package jsoftware.jbeerapp.forms;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaStatProdAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.Record;

public class FormProdotto extends AppCompatActivity {


    private String datafine;
    private String datainizio;
    private String clicodice;
    private ListView lista;
    private ArrayList<Record> vart;

    private ListaStatProdAdapter ladapter;

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_prodotto);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        lista = findViewById(R.id.fprod_lista);


        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodice"))
            clicodice = getIntent().getExtras().getString("clicodice");

        Data dt = new Data();
        int m = 1;
        int a = dt.anno();
        datainizio = a + "-" + Formattazione.formIntero(m, 2, "0") + "-01";

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date newDate = new Date();
        String oggi = dateFormat.format(newDate);
        datafine = oggi;

/*        if (Env.termordini) {
            calcolaDatiOrdinato();
        } else {*/
            calcolaDati();
        //}

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.msg_menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.msg_action_ok) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    private void calcolaDati() {
        vart = FunzioniJBeerApp.vendutoArticoliStat(clicodice);
        ladapter = new ListaStatProdAdapter(this.getApplicationContext(), vart);
        lista.setAdapter(ladapter);
/*        double tv = 0;
        for (Record r : vart) {
            tv += r.leggiDouble("val");
        }*/

    }

    private void calcolaDatiOrdinato() {

        vart = FunzioniJBeerApp.ordinatoArticoli(datainizio, datafine, clicodice, 2);
        ladapter = new ListaStatProdAdapter(this.getApplicationContext(), vart);
        lista.setAdapter(ladapter);
        double tv = 0;
        for (Record r : vart) {
            tv += r.leggiDouble("val");
        }

    }
}
