package jsoftware.jbeerapp.forms;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaRetiWiFiAdapter;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.env.funzStringa;

public class FormElencoRetiWIFI extends AppCompatActivity {
    private ListView listareti;
    private ArrayList<Record> vreti;
    private ListaRetiWiFiAdapter lradapter;
    WifiManager mWifiManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_elenco_reti_wifi);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        listareti = findViewById(R.id.elencowifi_listareti);

        vreti = new ArrayList<Record>();

        final WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        //final WifiConfiguration conf = new WifiConfiguration();
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for (WifiConfiguration i : list) {
            Record r = new Record();
            r.insStringa("ssid", funzStringa.rimuovi(i.SSID, "\""));
            Log.v("jazztv", "ssid:" + i.SSID + " bssid:" + i.BSSID);
            vreti.add(r);
        }
        lradapter = new ListaRetiWiFiAdapter(this.getApplicationContext(), vreti);
        listareti.setAdapter(lradapter);

//        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
//        registerReceiver(mWifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
//        mWifiManager.startScan();


        listareti.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                String tmp = vreti.get(arg2).leggiStringa("ssid");
                Uri codselez = Uri.parse("content://elencowifi/" + tmp);
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                setResult(RESULT_OK, result);
                finish();
            }
        });

    }

//    private final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context c, Intent intent) {
//            if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
//                List<ScanResult> mScanResults = mWifiManager.getScanResults();
//                for (ScanResult scanResult : mScanResults) {
//                    Record r = new Record();
//                    r.insStringa("ssid", scanResult.SSID);
//                    int level = WifiManager.calculateSignalLevel(scanResult.level, 5);
//                    r.insIntero("segnale", level);
//                    vreti.add(r);
//                }
//            }
//            lradapter.notifyDataSetChanged();
//        }
//    };
}
