package jsoftware.jbeerapp.forms;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaFatturatoAdapter;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;

public class FormFatturato extends AppCompatActivity {

    private String datafine;
    private String datainizio;
    private String clicodice;
    private ArrayList vdoc;
    private ListaFatturatoAdapter ldocadapter;
    private ListView listadoc;
    private TextView tvtotvend;

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_fatturato);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        listadoc = findViewById(R.id.ffatt_lista);
        tvtotvend = findViewById(R.id.ffatt_totvend);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodice"))
            clicodice = getIntent().getExtras().getString("clicodice");

        registerForContextMenu(listadoc);

        vdoc = new ArrayList();
/*        ldocadapter = new ListaFatturatoAdapter(this.getApplication(), vdoc, this);
        listadoc.setAdapter(ldocadapter);*/
        calcolaDati();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.msg_menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.msg_action_ok) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }



    private void calcolaDati() {
        vdoc = FunzioniJBeerApp.vendutoArticoliStat(clicodice);
        double totval = 0;
        for (int i = 0; i < vdoc.size(); i++) {
            Record r = (Record) vdoc.get(i);
            totval += r.leggiDouble("artvalore");

        }

        tvtotvend.setText("" + OpValute.arrotondaMat(totval,2));

        ldocadapter = new ListaFatturatoAdapter(this.getApplication(), vdoc, this);
        listadoc.setAdapter(ldocadapter);
/*        double tv = 0;
        for (Record r : vart) {
            tv += r.leggiDouble("val");
        }*/

    }


}
