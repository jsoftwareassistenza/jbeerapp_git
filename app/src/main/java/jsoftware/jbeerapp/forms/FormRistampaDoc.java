package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaRistampaDocAdapter;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;

public class FormRistampaDoc extends AppCompatActivity {
    private ListView listadoc;
    private Spinner spdoc;
    //    public TextView tvdatainizio;
//    public TextView tvdatafine;
    // public TextView tvtotimp;
//    public ImageButton bdatainizio;
//    public ImageButton bdatafine;
    private ArrayList<Record> vdoc;
    private ListaRistampaDocAdapter lrdadapter;
    public int inddoc = -1;
    private String datainizio = "0000-00-00";
    private String datafine = "9999-99-99";
    private int posselez = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_ristampa_doc);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        listadoc = findViewById(R.id.ristampadoc_listadoc);
        spdoc = findViewById(R.id.ristampadoc_doc);

//        tvdatainizio = (TextView) findViewById(R.id.ristampadoc_datainizio);
//        tvdatafine = (TextView) findViewById(R.id.ristampadoc_datafine);
        //tvtotimp = (TextView) findViewById(R.id.ristampadoc_totimp);
//        bdatainizio = (ImageButton) findViewById(R.id.ristampadoc_buttonDataInizio);
//        bdatafine = (ImageButton) findViewById(R.id.ristampadoc_buttonDataFine);

        //View header = this.getLayoutInflater().inflate(R.layout.ristampadoc_header, null);
        //listadoc.addHeaderView(header);
        //tvtotimp.setText("");

        ArrayList<String> vd = new ArrayList();
        vd.add("...");
        vd.add("DDT VENDITA");
        vd.add("DDT A QUANTITA");
        vd.add("FATTURE");
        vd.add("CORRISPETTIVI");
        vd.add("DDT DI RESO");
        vd.add("ORDINE PRODOTTI");
        vd.add("SCARICHI A SEDE");
        vd.add("TRASF.VS ALTRO AUTOMEZZO");
        vd.add("CARICHI DA ALTRO AUTOMEZZO");
        vd.add("INTEGRAZIONI DA TERMINALE");
        vd.add("INTEGRAZIONI DA SEDE");
        vd.add("ORDINI CLIENTI");
        SpinnerAdapter ldadapter = new SpinnerAdapter(FormRistampaDoc.this.getApplicationContext(), R.layout.spinner_item, vd);
        spdoc.setAdapter(ldadapter);

        if (Env.termordini || Env.tipoterm == Env.TIPOTERM_RACCOLTAORDINI) {
            spdoc.setSelection(12);
            spdoc.setEnabled(false);
        }
        vdoc = new ArrayList();
        lrdadapter = new ListaRistampaDocAdapter(this.getApplicationContext(), vdoc, this);
        listadoc.setAdapter(lrdadapter);

        spdoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                aggiornaLista();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });


        listadoc.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
                posselez = (int) arg3;
                if (posselez != -1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(FormRistampaDoc.this);
                    builder.setMessage("Documento " + vdoc.get(posselez).leggiStringa("doc") + " - " + vdoc.get(posselez).leggiIntero("numdoc"))
                            .setPositiveButton("ELIMINARE",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {

                                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                                    FormRistampaDoc.this);
                                            builder.setMessage("Confermi Eliminazione Documento?")
                                                    .setPositiveButton("SI",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog,
                                                                                    int id) {
                                                                    if (vdoc.get(posselez).leggiStringa("trasf").equals("N")) {


                                                                        SQLiteStatement stmt = Env.db.compileStatement("DELETE FROM movimenti WHERE movdoc = ? AND movnum = ? ");
                                                                        stmt.clearBindings();
                                                                        stmt.bindString(1, vdoc.get(posselez).leggiStringa("doc"));
                                                                        stmt.bindLong(2, (int) vdoc.get(posselez).leggiIntero("numdoc"));
                                                                        stmt.execute();
                                                                        stmt.close();
                                                                        stmt = Env.db.compileStatement("DELETE FROM righemov WHERE rmmovdoc = ? AND rmmovnum = ? ");
                                                                        stmt.clearBindings();
                                                                        stmt.bindString(1, vdoc.get(posselez).leggiStringa("doc"));
                                                                        stmt.bindLong(2, (int) vdoc.get(posselez).leggiIntero("numdoc"));
                                                                        stmt.execute();
                                                                        stmt.close();
                                                                    /*    stmt = Env.db.compileStatement("DELETE FROM ivamov WHERE immovsez = ? AND immovnum = ? ");
                                                                        stmt.clearBindings();
                                                                        stmt.bindString(1, vdoc.get(posselez).leggiStringa("doc"));
                                                                        stmt.bindLong(2, (int) vdoc.get(posselez).leggiIntero("numdoc"));
                                                                        stmt.execute();
                                                                        stmt.close();
                                                                       stmt = Env.db.compileStatement("DELETE FROM cauzmov WHERE cmmovsez = ? AND cmmovnum = ?");
                                                                        stmt.clearBindings();
                                                                        stmt.bindString(1, vdoc.get(posselez).leggiStringa("doc"));
                                                                        stmt.bindLong(2, (int) vdoc.get(posselez).leggiIntero("numdoc"));
                                                                        stmt.execute();
                                                                        stmt.close();
                                                                        stmt = Env.db.compileStatement("DELETE FROM lottimov WHERE lmmovsez = ? AND lmmovnum = ?");
                                                                        stmt.clearBindings();
                                                                        stmt.bindString(1, vdoc.get(posselez).leggiStringa("doc"));
                                                                        stmt.bindLong(2, (int) vdoc.get(posselez).leggiIntero("numdoc"));
                                                                        stmt.execute();
                                                                        stmt.close();
                                                                        stmt = Env.db.compileStatement("DELETE FROM scadmov WHERE smmovsez = ? AND smmovnum = ? ");
                                                                        stmt.clearBindings();
                                                                        stmt.bindString(1, vdoc.get(posselez).leggiStringa("doc"));
                                                                        stmt.bindLong(2, (int) vdoc.get(posselez).leggiIntero("numdoc"));
                                                                        stmt.execute();
                                                                        stmt.close();*/


                                                                        aggiornaNumerazione(vdoc.get(posselez).leggiStringa("doc"), vdoc.get(posselez).leggiIntero("numdoc"));
                                                                        aggiornaLista();

                                                                    } else {
                                                                        Toast toast = Toast.makeText(getApplicationContext(), "Documento già inviato impossibile eliminarlo, contattare la sede!",
                                                                                Toast.LENGTH_LONG);
                                                                        toast.show();

                                                                    }
      /*                                                              values.remove(pos);
                                                                    notifyDataSetChanged();*/

                                                                }
                                                            })
                                                    .setNegativeButton("NO",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog,
                                                                                    int id) {
                                                                }
                                                            });
                                            AlertDialog ad = builder.create();
                                            ad.show();

                                        }
                                    })
                            .setNegativeButton("Dettaglio",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            Record rec = vdoc.get(posselez);
                                            Intent i = new Intent(FormRistampaDoc.this.getApplicationContext(), FormVisDettaglioDoc.class);
                                            Bundle mBundle = new Bundle();
                                            mBundle.putString("movdoc", rec.leggiStringa("doc"));
                                            mBundle.putString("movdata", rec.leggiStringa("datadoc"));
                                            mBundle.putInt("movnum", rec.leggiIntero("numdoc"));
                                            mBundle.putString("trasf", rec.leggiStringa("trasf"));
                                            mBundle.putInt("movtipo", rec.leggiIntero("tipodoc"));
                                            i.putExtras(mBundle);
                                            FormRistampaDoc.this.startActivity(i);
                                        }
                                    })
                    ;
                    AlertDialog ad = builder.create();
                    ad.show();

                }
                return false;
            }
        });


        aggiornaLista();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_form_gestdoc, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.doc_action_nuovo) {
            Intent i = new Intent(getApplicationContext(), FormDocumento.class);
            Bundle mBundle = new Bundle();
            mBundle.putInt("tipodoc", spdoc.getSelectedItemPosition() - 1);
            i.putExtras(mBundle);
            FormRistampaDoc.this.startActivityForResult(i, 1);
        }

        return super.onOptionsItemSelected(item);
    }

    private void aggiornaLista() {
        vdoc.clear();
        int td = spdoc.getSelectedItemPosition() - 1;
        if (td == 10) {
            SQLiteStatement str = Env.db.compileStatement(
                    "UPDATE movcaricosedestor SET mcprezzo=?,mcnetto=? WHERE mcdata=? AND mcnum=? AND mcartcod=? AND mclotto=?");
            double totprog = 0;
            String qry = "SELECT DISTINCT mcdata,mcnum FROM movcaricosedestor ORDER BY mcdata DESC,mcnum DESC";
            Cursor cursor = Env.db.rawQuery(qry, null);
            while (cursor.moveToNext()) {
                Record r = new Record();
                r.insStringa("trasf", "S");
                r.insIntero("tipodoc", td);
                r.insStringa("descrtipodoc", "INTEGRAZIONE DA SEDE");
                r.insStringa("doc", "XE");
                r.insIntero("numdoc", cursor.getInt(1));
                r.insStringa("datadoc", cursor.getString(0));
                r.insStringa("oradoc", "00:00");

                String[] parsd = new String[2];
                parsd[0] = cursor.getString(0);
                parsd[1] = "" + cursor.getInt(1);

                String cli = "sede";
                Cursor cs = Env.db.rawQuery("SELECT artcod,arttipoord FROM movcaricosedestor INNER JOIN articoli ON movcaricosedestor.mcartcod = articoli.artcod WHERE mcdata = ? AND mcnum= ? ORDER BY mcartcod", parsd);
                if (cs.moveToFirst()) {
                    if (cs.getInt(1) == 1)
                        cli = "sede";
                    else {
                        Cursor cfa = Env.db.rawQuery(
                                "SELECT forcodice,fornome FROM forart INNER JOIN fornitori ON forart.faforcod = fornitori.forcodice WHERE faartcod = '" + cs.getString(0) + "'", null);
                        if (cfa.moveToFirst()) {
                            cli = cfa.getString(1);
                        }
                        cfa.close();
                    }
                }
                cs.close();
                r.insStringa("cliente", cli);
                double imp = 0;
                boolean przzero = false;
                Cursor cd = Env.db.rawQuery("SELECT mcnetto FROM movcaricosedestor WHERE mcdata = ? AND mcnum= ? ORDER BY mcartcod", parsd);
                while (cd.moveToNext()) {
                    imp += cd.getDouble(0);
                    if (cd.getDouble(0) == 0)
                        przzero = true;
                }
                cd.close();
                if (imp == 0) {
                    cd = Env.db.rawQuery("SELECT mcartcod,mclotto,mcqta FROM movcaricosedestor WHERE mcdata = ? AND mcnum= ? ORDER BY mcartcod", parsd);
                    while (cd.moveToNext()) {


                        str.clearBindings();
                        str.bindDouble(1, 0);
                        str.bindDouble(2, 0);
                        str.bindString(3, cursor.getString(0));
                        str.bindLong(4, cursor.getInt(1));
                        str.bindString(5, cd.getString(0));
                        str.bindString(6, cd.getString(1));
                        str.execute();
                    }
                    cd.close();
                    imp = OpValute.arrotondaMat(imp, 2);
                }
                totprog += imp;
                imp = OpValute.arrotondaMat(imp, 2);
                r.insDouble("totale", imp);
                r.insDouble("acconto", 0);
                r.insDouble("imponibile", imp);
                r.insDouble("totprog", totprog);
                r.insStringa("przzero", (przzero ? "S" : "N"));
                vdoc.add(r);
            }
            cursor.close();
            str.close();
            //tvtotimp.setText(Formattazione.formValuta(totprog, 12, 2, Formattazione.SEGNO_SX_NEG));
        } else {
            SQLiteStatement str = Env.db.compileStatement(
                    "UPDATE righemov SET rmprz=?,rmlordo=?,rmnetto=?,rmnettoivato=? WHERE rmmovtipo=? AND rmmovdata=? AND rmmovnum=? AND rmriga=?");
            SQLiteStatement stm = Env.db.compileStatement(
                    "UPDATE movimenti SET movtotimp=?,movtotale=?,movnettoapag=? WHERE movtipo=? AND movdata=? AND movnum=?");
            int td2 = td;
            if (td2 == 5)
                td2 = 12;
            else if (td2 == 6)
                td2 = 6;
            else if (td2 == 7)
                td2 = 7;
            else if (td2 == 8)
                td2 = 8;
            else if (td2 == 9)
                td2 = 5;
            else if (td2 == 11)
                td2 = 20;
            double totprog = 0;
            String qry = "";
            if (td2 == -1) {
                qry = "SELECT movnum,movdata,movclicod,movtotale,movacconto,movdoc,movfor,movdocora,movtotimp,movnote1,movtrasf FROM movimenti " +
                        "WHERE movsospeso = 'N' AND movnote2 <> 'CARAUTO' " +
                        "ORDER BY movdata DESC,movnum DESC LIMIT 10";
            } else {
                qry = "SELECT movnum,movdata,movclicod,movtotale,movacconto,movdoc,movfor,movdocora,movtotimp,movnote1,movtrasf FROM movimenti " +
                        "WHERE movtipo = " + td2 + " AND movsospeso = 'N' AND movnote2 <> 'CARAUTO' " +
                        "ORDER BY movdata DESC,movnum DESC";
            }
            Cursor cursor = Env.db.rawQuery(qry, null);
            while (cursor.moveToNext()) {
                Record r = new Record();
                if (cursor.getString(10).equals("S"))
                    r.insStringa("trasf", "S");
                else
                    r.insStringa("trasf", "N");
                r.insIntero("tipodoc", td);
                if (td == 0) {
                    r.insStringa("descrtipodoc", "DDT DI VENDITA");
                } else if (td == 1) {
                    r.insStringa("descrtipodoc", "DDT QUANTITA'");
                } else if (td == 2) {
                    r.insStringa("descrtipodoc", "FATTURA");
                } else if (td == 3) {
                    r.insStringa("descrtipodoc", "CORRISPETTIVO");
                } else if (td == 4) {
                    r.insStringa("descrtipodoc", "DDT DI RESO");
                } else if (td == 5) {
                    r.insStringa("descrtipodoc", "ORDINE PRODOTTI");
                } else if (td == 6) {
                    r.insStringa("descrtipodoc", "SCARICO A SEDE");
                } else if (td == 7) {
                    r.insStringa("descrtipodoc", "TRASF.VS ALTRO AUTOMEZZO");
                } else if (td == 8) {
                    r.insStringa("descrtipodoc", "CARICO DA ALTRO AUTOMEZZO");
                } else if (td == 9) {
                    r.insStringa("descrtipodoc", "INTEGRAZIONE DA TERMINALE");
                } else if (td == 11) {
                    r.insStringa("descrtipodoc", "ORDINE CLIENTE");
                }
                r.insStringa("doc", cursor.getString(5));
                r.insIntero("numdoc", cursor.getInt(0));
                r.insStringa("datadoc", cursor.getString(1));
                r.insStringa("oradoc", cursor.getString(7));
                String sogg = "";
                if (td == 7 || td == 8) {
                    sogg = cursor.getString(9);
                } else if (!cursor.getString(6).equals("")) {
                    String[] parsf = new String[1];
                    parsf[0] = cursor.getString(6);
                    final Cursor cfor = Env.db.rawQuery(
                            "SELECT prcodice,prdescr,prindir,prloc,prprov FROM provenienze WHERE prcodice = ?", parsf);
                    if (cfor.moveToFirst()) {
                        //sogg += cfor.getString(0) + " " + cfor.getString(1);
                        sogg += cfor.getString(1);
                    }
                    cfor.close();
                } else if (!cursor.getString(2).equals("")) {
                    String[] pars = new String[1];
                    pars[0] = cursor.getString(2);
                    Cursor cc = Env.db.rawQuery("SELECT clinome FROM clienti WHERE clicodice = ?", pars);
                    if (cc.moveToFirst())
                        //sogg = cursor.getString(2) + cc.getString(0);
                        sogg = cc.getString(0);
                    else
                        sogg = cursor.getString(2);
                    cc.close();
                }
                r.insStringa("cliente", sogg);
                double imp = cursor.getDouble(8);
                double tot = cursor.getDouble(3);
                if (imp == 0 && tot == 0) {
                    String[] parsd = new String[3];
                    parsd[0] = cursor.getString(5);
                    parsd[1] = cursor.getString(1);
                    parsd[2] = "" + cursor.getInt(0);
                    Cursor cd = Env.db.rawQuery("SELECT rmnetto FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? ORDER BY rmriga", parsd);
                    while (cd.moveToNext()) {
                        imp += cd.getDouble(0);
                    }
                    cd.close();
                    imp = OpValute.arrotondaMat(imp, 2);
                    tot = imp;
                    if (imp == 0) {
                        cd = Env.db.rawQuery("SELECT rmriga,rmartcod,rmlotto,rmqta FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? AND rmtiporiga='A' ORDER BY rmriga", parsd);
                        while (cd.moveToNext()) {

                            str.clearBindings();
                            str.bindDouble(1, 0);
                            str.bindDouble(2, 0);
                            str.bindDouble(3, 0);
                            str.bindDouble(4, 0);
                            str.bindLong(5, td2);
                            str.bindString(6, cursor.getString(1));
                            str.bindLong(7, cursor.getInt(0));
                            str.bindLong(8, cd.getInt(0));
                            str.execute();
                            imp += 0;
                        }
                        cd.close();
                        imp = OpValute.arrotondaMat(imp, 2);
                        stm.clearBindings();
                        stm.bindDouble(1, imp);
                        stm.bindDouble(2, imp);
                        stm.bindDouble(3, imp);
                        stm.bindLong(4, td2);
                        stm.bindString(5, cursor.getString(1));
                        stm.bindLong(6, cursor.getInt(0));
                        stm.execute();
                    }
                }
                r.insDouble("totale", tot);
                r.insDouble("acconto", cursor.getDouble(4));
                r.insDouble("imponibile", imp);
                totprog += imp;
                r.insDouble("totprog", totprog);
                r.insStringa("przzero", "N");
                vdoc.add(r);
            }
            cursor.close();
            str.close();
            stm.close();
            //tvtotimp.setText(Formattazione.formValuta(totprog, 12, 2, Formattazione.SEGNO_SX_NEG));
        }

        lrdadapter.notifyDataSetChanged();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                aggiornaLista();
                break;
            }
        }
    }

    private void aggiornaNumerazione(String doc, int num) {

        // bolla valorizzata
        if (doc.equals(Env.docbollaval)) {
            if (num == Env.numbollaval--) {
                int numReale = sistemaBuchi("numbollaval", num);
                SQLiteStatement stpar = Env.db.compileStatement("UPDATE datiterm SET numbollaval=?");
                stpar.clearBindings();
                stpar.bindLong(1, numReale);
                stpar.execute();
                stpar.close();
                Env.numbollaval = numReale;
                if (Env.sezbollaqta.toLowerCase().equals(Env.sezbollaval.toLowerCase())) {
                    stpar = Env.db.compileStatement("UPDATE datiterm SET numbollaqta=?");
                    stpar.clearBindings();
                    stpar.bindLong(1, Env.numbollaqta--);
                    stpar.execute();
                    stpar.close();
                    Env.numbollaqta--;
                }
            }
        } else if (doc.equals(Env.docbollaqta)) {
            // bolla qta
            if (num == Env.numbollaqta--) {
                int numReale = sistemaBuchi("numbollaqta", num);
                SQLiteStatement stpar = Env.db.compileStatement("UPDATE datiterm SET numbollaqta=?");
                stpar.clearBindings();
                stpar.bindLong(1, numReale);
                stpar.execute();
                stpar.close();
                Env.numbollaqta = numReale;
                if (Env.sezbollaval.toLowerCase().equals(Env.sezbollaqta.toLowerCase())) {
                    stpar = Env.db.compileStatement("UPDATE datiterm SET numbollaval=?");
                    stpar.clearBindings();
                    stpar.bindLong(1, Env.numbollaval--);
                    stpar.execute();
                    stpar.close();
                    Env.numbollaval--;
                }
            }
        } else if (doc.equals(Env.docfattura)) {
            // fattura
            if (num == Env.numfattura--) {
                int numReale = sistemaBuchi("numfattura", num);
                SQLiteStatement stpar = Env.db.compileStatement("UPDATE datiterm SET numfattura=?");
                stpar.clearBindings();
                stpar.bindLong(1, numReale);
                stpar.execute();
                stpar.close();
                Env.numfattura = numReale;
            }
        } else if (doc.equals(Env.doccorrisp)) {
            // corrispettivo
            if (num == Env.numcorrisp--) {
                int numReale = sistemaBuchi("numcorrisp", num);
                SQLiteStatement stpar = Env.db.compileStatement("UPDATE datiterm SET numcorrisp=?");
                stpar.clearBindings();
                stpar.bindLong(1, numReale);
                stpar.execute();
                stpar.close();
                Env.numcorrisp = numReale;

            }
        } else if (doc.equals(Env.docddtreso)) {
            // ddt reso
            if (num == Env.numddtreso--) {
                int numReale = sistemaBuchi("numddtreso", num);
                SQLiteStatement stpar = Env.db.compileStatement("UPDATE datiterm SET numddtreso=?");
                stpar.clearBindings();
                stpar.bindLong(1, numReale);
                stpar.execute();
                stpar.close();
                Env.numddtreso = numReale;
            }
        } else if (doc.equals(Env.docordinecli)) {
            // ordine cliente
            int env = Env.numordinecli - 1;
            if (num == env) {
                int numReale = sistemaBuchi("numordinecli", num);

                SQLiteStatement stpar = Env.db.compileStatement("UPDATE datiterm SET numordinecli=?");
                stpar.clearBindings();
                stpar.bindLong(1, numReale);
                stpar.execute();
                stpar.close();
                Env.numordinecli = numReale;

            }
        }


    }

    private int sistemaBuchi(String campo, int numAttuale) {
        //riallinea l'ultimo numero disponibile per una cancellazione massiva
        String qry = "SELECT movnum FROM movimenti ORDER BY movnum DESC";
        Cursor cursorpar = Env.db.rawQuery(qry, null);
        if (cursorpar.moveToNext()) {
            int numNew = cursorpar.getInt(0);
            if (numAttuale > numNew) {

                numAttuale = numNew + 1;

            }
        } else {
            numAttuale = 1;
        }
        cursorpar.close();
        return numAttuale;
    }

}
