package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaVendQtaTotAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;

public class FormDocListaVendQtaTot extends AppCompatActivity {
    private Button bok;
    private Button bannulla;
    private EditText edcod;
    private EditText eddescr;
    private ListView listaart;
    private ListaVendQtaTotAdapter lvendqtadapter;
    private ArrayList<Record> vart;
    public ArrayList<Record> righecrp = null;
    public boolean controlloGiac = true;
    public boolean controlloLotto = false;
    public String cliordlotto = "A";
    public String clicod = "";
    public String clicodpadre = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_doc_lista_vend_qta_tot);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("controlloGiac"))
            controlloGiac = getIntent().getExtras().getBoolean("controlloGiac");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("controlloLotto"))
            controlloLotto = getIntent().getExtras().getBoolean("controlloLotto");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cliordlotto"))
            cliordlotto = getIntent().getExtras().getString("cliordlotto");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicod"))
            clicod = getIntent().getExtras().getString("clicod");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodpadre"))
            clicodpadre = getIntent().getExtras().getString("clicodpadre");

        bok = this.findViewById(R.id.listavendqt_buttonOk);
        bannulla = this.findViewById(R.id.listavendqt_buttonAnnulla);
        listaart = this.findViewById(R.id.listavendqt_listaart);
        edcod = this.findViewById(R.id.listavendqt_riccod);
        eddescr = this.findViewById(R.id.listavendqt_ricdescr);
        if (Env.codartnum)
            edcod.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);

/*        View header = this.getLayoutInflater().inflate(R.layout.listavend_qtatot_listaart_header, null);
        listaart.addHeaderView(header);*/

        vart = new ArrayList<Record>();
        lvendqtadapter = new ListaVendQtaTotAdapter(this.getApplicationContext(), vart);
        listaart.setAdapter(lvendqtadapter);

        aggiornaLista();

        boolean modprz = true;
        if (!clicod.equals("")) {
            String[] parsc = new String[1];
            parsc[0] = clicod;
            final Cursor cc = Env.db.rawQuery(
                    "SELECT climodprz FROM clienti WHERE clicodice = ?", parsc);
            if (cc.moveToFirst()) {
                modprz = (cc.getString(0).equals("S"));
            }
            cc.close();
        }

        edcod.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                aggiornaLista();
                return false;
            }
        });

        eddescr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                aggiornaLista();
                return false;
            }
        });

        edcod.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                aggiornaLista();
            }
        });

        eddescr.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                aggiornaLista();
            }
        });

        bok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ArrayList<HashMap> vris = new ArrayList();
/*
                if (controlloGiac && !Env.lottineg)
                {
                    // controllo quantita
                    for (int i = 0; i < vart.size(); i++)
                    {
                        Record r = vart.get(i);
                        if (r.leggiDouble("qtavend") > 0 || r.leggiDouble("qtasm") > 0 || r.leggiDouble("qtaomimp") > 0 ||
                                r.leggiDouble("qtaomtot") > 0)
                        {
                            double diff = OpValute.arrotondaMat(r.leggiDouble("giac") - r.leggiDouble("qtavend") - r.leggiDouble("qtasm") - r.leggiDouble("qtaomimp") - r.leggiDouble("qtaomtot"), 2);
                            if ((r.leggiDouble("qtavend") != 0 || r.leggiDouble("qtasm") != 0 || r.leggiDouble("qtaomimp") != 0 ||
                                    r.leggiDouble("qtaomtot") != 0) && diff < 0)
                            {
                                AlertDialog.Builder builder = new AlertDialog.Builder(
                                        FormDocListaVendQtaTot.this);
                                builder.setMessage("Giacenza non sufficiente per articolo " + r.leggiStringa("artcod"))
                                        .setPositiveButton("Ok",
                                                new DialogInterface.OnClickListener()
                                                {
                                                    public void onClick(DialogInterface dialog,
                                                                        int id)
                                                    {
                                                    }
                                                });
                                AlertDialog ad = builder.create();
                                ad.setCancelable(false);
                                ad.show();
                                return;
                            }
                        }
                    }
                }
*/
                for (int i = 0; i < vart.size(); i++) {
                    Record r = vart.get(i);
                    if (r.leggiDouble("qtavend") != 0 || r.leggiDouble("qtasm") != 0 || r.leggiDouble("qtaomimp") != 0 ||
                            r.leggiDouble("qtaomtot") != 0) {
                        HashMap h = new HashMap();
                        h.put("artcod", r.leggiStringa("artcod"));
                        h.put("artdescr", r.leggiStringa("artdescr"));
                        h.put("codiva", r.leggiStringa("codiva"));
                        h.put("qtavend", r.leggiDouble("qtavend"));
                        h.put("qtasm", r.leggiDouble("qtasm"));
                        h.put("qtaomimp", r.leggiDouble("qtaomimp"));
                        h.put("qtaomtot", r.leggiDouble("qtaomtot"));
                        h.put("prezzo", r.leggiDouble("prezzo"));
                        h.put("sc1", r.leggiDouble("sc1"));
                        h.put("sc2", r.leggiDouble("sc2"));
                        h.put("sc3", r.leggiDouble("sc3"));
                        vris.add(h);
                    }
                }
                Uri codselez = Uri.parse("content://listavendqt/OK");
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                result.putExtra("vris", vris);
                setResult(RESULT_OK, result);
                finish();
            }
        });

        bannulla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uri codselez = Uri.parse("content://listavendqt/ANNULLA");
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                setResult(RESULT_CANCELED, result);
                finish();
            }
        });

        final boolean modprz2 = modprz;
        listaart.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                //Log.v("jorders", "CLICK PREF " + arg0 + " " + arg1 + " " + arg2 + " " + arg3);
                if ((int) arg3 >= 0) {
                    final Record rec = vart.get((int) arg3);
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            FormDocListaVendQtaTot.this);
                    final AlertDialog optionDialog = alert.create();
                    optionDialog.setMessage("Dati articolo");
                    LinearLayout ll = new LinearLayout(FormDocListaVendQtaTot.this.getApplicationContext());
                    ll.setOrientation(LinearLayout.VERTICAL);

                    final TextView tvlabelart = new TextView(FormDocListaVendQtaTot.this);
                    tvlabelart.setText(rec.leggiStringa("artcod") + "-" + rec.leggiStringa("artdescr") + " (" + rec.leggiStringa("artum") + ")");
                    tvlabelart.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                    tvlabelart.setTextColor(Color.BLACK);
                    tvlabelart.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    tvlabelart.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    tvlabelart.setBackgroundDrawable(getResources().getDrawable(R.drawable.textview1));
                    ll.addView(tvlabelart);

                    if (!rec.leggiStringa("artschedaweb").equals("")) {
                        final Button bweb = new Button(FormDocListaVendQtaTot.this);
                        bweb.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_public_white_48dp, 0, 0, 0);
                        bweb.setText("Scheda articolo WEB");
                        bweb.setTextColor(Color.WHITE);
                        bweb.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                        ll.addView(bweb);
                        bweb.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                Intent i = new Intent(FormDocListaVendQtaTot.this.getApplicationContext(), FormWeb.class);
                                Bundle mBundle = new Bundle();
                                mBundle.putString("URL", rec.leggiStringa("artschedaweb"));
                                mBundle.putString("TITOLO", "Scheda art." + rec.leggiStringa("artcod"));
                                i.putExtras(mBundle);
                                FormDocListaVendQtaTot.this.startActivity(i);
                            }
                        });
                    }

                    LinearLayout lqtavend = new LinearLayout(FormDocListaVendQtaTot.this.getApplicationContext());
                    lqtavend.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelqtavend = new TextView(FormDocListaVendQtaTot.this);
                    tvlabelqtavend.setText("Qtà vendita:");
                    tvlabelqtavend.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelqtavend.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelqtavend.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    lqtavend.addView(tvlabelqtavend);
                    final EditText inputqvend = new EditText(FormDocListaVendQtaTot.this);
                    inputqvend.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputqvend.setHint("quantità vendita");
                    if (!FunzioniJBeerApp.umDecimale(rec.leggiStringa("artcod")))
                        inputqvend.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    else
                        inputqvend.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputqvend.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputqvend.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtavend.addView(inputqvend);
                    boolean cbloccvend = FunzioniJBeerApp.causaleBloccata(0, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                    if (!cbloccvend) {
                        ll.addView(lqtavend);
                        LinearLayout.LayoutParams lParams_lqtavend = (LinearLayout.LayoutParams) tvlabelqtavend.getLayoutParams();
                        lParams_lqtavend.weight = 0.4f;
                        lParams_lqtavend.width = 0;
                        LinearLayout.LayoutParams lParams_qtavend = (LinearLayout.LayoutParams) inputqvend.getLayoutParams();
                        lParams_qtavend.weight = 0.6f;
                        lParams_qtavend.width = 0;
                    }
                    LinearLayout lqtasm = new LinearLayout(FormDocListaVendQtaTot.this.getApplicationContext());
                    lqtasm.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelqtasm = new TextView(FormDocListaVendQtaTot.this);
                    tvlabelqtasm.setText("Qtà sconto merce:");
                    tvlabelqtasm.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelqtasm.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelqtasm.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    lqtasm.addView(tvlabelqtasm);
                    final EditText inputqsm = new EditText(FormDocListaVendQtaTot.this);
                    inputqsm.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputqsm.setHint("quantità sconto merce");
                    if (!FunzioniJBeerApp.umDecimale(rec.leggiStringa("artcod")))
                        inputqsm.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    else
                        inputqsm.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputqsm.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputqsm.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtasm.addView(inputqsm);
                    boolean cbloccsm = FunzioniJBeerApp.causaleBloccata(4, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                    if (!cbloccsm) {
                        ll.addView(lqtasm);
                        LinearLayout.LayoutParams lParams_lqtasm = (LinearLayout.LayoutParams) tvlabelqtasm.getLayoutParams();
                        lParams_lqtasm.weight = 0.4f;
                        lParams_lqtasm.width = 0;
                        LinearLayout.LayoutParams lParams_qtasm = (LinearLayout.LayoutParams) inputqsm.getLayoutParams();
                        lParams_qtasm.weight = 0.6f;
                        lParams_qtasm.width = 0;
                    }
                    LinearLayout lqtaomimp = new LinearLayout(FormDocListaVendQtaTot.this.getApplicationContext());
                    lqtaomimp.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelqtaomimp = new TextView(FormDocListaVendQtaTot.this);
                    tvlabelqtaomimp.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelqtaomimp.setText("Qtà omaggio imponibile:");
                    tvlabelqtaomimp.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelqtaomimp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    lqtaomimp.addView(tvlabelqtaomimp);
                    final EditText inputqomimp = new EditText(FormDocListaVendQtaTot.this);
                    inputqomimp.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputqomimp.setHint("quantità omaggio imponibile");
                    if (!FunzioniJBeerApp.umDecimale(rec.leggiStringa("artcod")))
                        inputqomimp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    else
                        inputqomimp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputqomimp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputqomimp.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtaomimp.addView(inputqomimp);
                    boolean cbloccomimp = FunzioniJBeerApp.causaleBloccata(5, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                    if (!cbloccomimp) {
                        ll.addView(lqtaomimp);
                        LinearLayout.LayoutParams lParams_lqtaomimp = (LinearLayout.LayoutParams) tvlabelqtaomimp.getLayoutParams();
                        lParams_lqtaomimp.weight = 0.4f;
                        lParams_lqtaomimp.width = 0;
                        LinearLayout.LayoutParams lParams_qtaomimp = (LinearLayout.LayoutParams) inputqomimp.getLayoutParams();
                        lParams_qtaomimp.weight = 0.6f;
                        lParams_qtaomimp.width = 0;
                    }

                    LinearLayout lqtaomtot = new LinearLayout(FormDocListaVendQtaTot.this.getApplicationContext());
                    lqtaomtot.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelqtaomtot = new TextView(FormDocListaVendQtaTot.this);
                    tvlabelqtaomtot.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelqtaomtot.setText("Qtà omaggio totale:");
                    tvlabelqtaomtot.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelqtaomtot.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    lqtaomtot.addView(tvlabelqtaomtot);
                    final EditText inputqomtot = new EditText(FormDocListaVendQtaTot.this);
                    inputqomtot.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputqomtot.setHint("quantità omaggio totale");
                    if (!FunzioniJBeerApp.umDecimale(rec.leggiStringa("artcod")))
                        inputqomtot.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    else
                        inputqomtot.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputqomtot.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputqomtot.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtaomtot.addView(inputqomtot);
                    boolean cbloccomtot = FunzioniJBeerApp.causaleBloccata(6, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                    if (!cbloccomtot) {
                        ll.addView(lqtaomtot);
                        LinearLayout.LayoutParams lParams_lqtaomtot = (LinearLayout.LayoutParams) tvlabelqtaomtot.getLayoutParams();
                        lParams_lqtaomtot.weight = 0.4f;
                        lParams_lqtaomtot.width = 0;
                        LinearLayout.LayoutParams lParams_qtaomtot = (LinearLayout.LayoutParams) inputqomtot.getLayoutParams();
                        lParams_qtaomtot.weight = 0.6f;
                        lParams_qtaomtot.width = 0;
                    }

                    LinearLayout lprz = new LinearLayout(FormDocListaVendQtaTot.this.getApplicationContext());
                    lprz.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelprz = new TextView(FormDocListaVendQtaTot.this);
                    tvlabelprz.setText("Prezzo:");
                    tvlabelprz.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelprz.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelprz.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    lprz.addView(tvlabelprz);
                    final EditText inputp = new EditText(FormDocListaVendQtaTot.this);
                    inputp.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputp.setHint("prezzo");
                    inputp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputp.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    if (rec.leggiDouble("prezzo") != 0)
                        inputp.setText(Formattazione.formatta(rec.leggiDouble("prezzo"), "#####0.000", Formattazione.NO_SEGNO));
                    if (!modprz2)
                        inputp.setEnabled(false);
                    lprz.addView(inputp);
                    LinearLayout.LayoutParams lParams_lprz = (LinearLayout.LayoutParams) tvlabelprz.getLayoutParams();
                    lParams_lprz.weight = 0.4f;
                    lParams_lprz.width = 0;
                    LinearLayout.LayoutParams lParams_prz = (LinearLayout.LayoutParams) inputp.getLayoutParams();
                    lParams_prz.weight = 0.6f;
                    lParams_prz.width = 0;
                    ll.addView(lprz);
                    double minprzrif = 999999;
                    if (Env.visprzrif) {
                        LinearLayout lpr = new LinearLayout(FormDocListaVendQtaTot.this.getApplicationContext());
                        lpr.setOrientation(LinearLayout.HORIZONTAL);
                        lpr.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        final TextView tvlabelprzrif = new TextView(FormDocListaVendQtaTot.this);
                        tvlabelprzrif.setText("Prezzo base: ");
                        tvlabelprzrif.setTextColor(Color.rgb(5, 50, 73));
                        tvlabelprzrif.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                        tvlabelprzrif.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                        lpr.addView(tvlabelprzrif);
                        final TextView tvprzrif = new TextView(FormDocListaVendQtaTot.this);
                        tvprzrif.setTypeface(tvprzrif.getTypeface(), Typeface.BOLD);
                        tvprzrif.setTextColor(Color.BLUE);
                        tvprzrif.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                        tvprzrif.setBackgroundDrawable(getResources().getDrawable(R.drawable.textview1));
                        tvprzrif.setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL);
                        tvprzrif.setVisibility(View.INVISIBLE);
                        String sprzrif = "";
                        int nprzrif = 0;
                        String[] parsl = new String[1];
                        parsl[0] = rec.leggiStringa("artcod");
                        Cursor cl = Env.db.rawQuery("SELECT lotcod,lotgiacenza FROM lotti LEFT JOIN datelotti ON (lotti.lotcod = datelotti.dllotcod AND lotti.lotartcod = datelotti.dlartcod) WHERE lotgiacenza > 0 AND lotartcod = ? ORDER BY dldata DESC,lotcod DESC", parsl);
                        while (cl.moveToNext() && nprzrif < 10) {
                            double pb = FunzioniJBeerApp.leggiPrezzoBaseArticolo(rec.leggiStringa("artcod"), cl.getString(0),true);
                            sprzrif += "LOTTO:" + cl.getString(0) + " prz.:" + Formattazione.formValuta(pb, 12, 3, 0) + "\n";
                            nprzrif++;
                            if (pb < minprzrif)
                                minprzrif = pb;
                        }
                        if (sprzrif.length() > 0)
                            sprzrif = sprzrif.substring(0, sprzrif.length() - 1);
                        cl.close();
                        tvprzrif.setText(sprzrif);
/*
                        if (rec.leggiDouble("przrif") > 0)
                            tvprzrif.setText(Formattazione.formatta(rec.leggiDouble("przrif"), "#####0.000", Formattazione.NO_SEGNO));
                        else
                            tvprzrif.setText("");
*/
                        final ImageButton bvisprz = new ImageButton(FormDocListaVendQtaTot.this);
                        bvisprz.setImageResource(R.drawable.ic_visibility_white_48dp);
                        bvisprz.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                        lpr.addView(bvisprz);
                        bvisprz.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                if (bvisprz.getTag() == null || (bvisprz.getTag() != null && bvisprz.getTag().equals("NOVIS"))) {
                                    tvprzrif.setVisibility(View.VISIBLE);
                                    bvisprz.setTag("VIS");
                                } else {
                                    tvprzrif.setVisibility(View.INVISIBLE);
                                    bvisprz.setTag("NOVIS");
                                }
                            }
                        });
                        lpr.addView(tvprzrif);
                        LinearLayout.LayoutParams lParams_lprzrif = (LinearLayout.LayoutParams) tvlabelprzrif.getLayoutParams();
                        lParams_lprzrif.weight = 0.2f;
                        lParams_lprzrif.width = 0;
                        lParams_lprzrif.height = LinearLayout.LayoutParams.MATCH_PARENT;
                        LinearLayout.LayoutParams lParams_przrif = (LinearLayout.LayoutParams) tvprzrif.getLayoutParams();
                        lParams_przrif.weight = 0.6f;
                        lParams_przrif.width = 0;
                        lParams_przrif.height = LinearLayout.LayoutParams.MATCH_PARENT;
                        LinearLayout.LayoutParams lParams_bvisprz = (LinearLayout.LayoutParams) bvisprz.getLayoutParams();
                        lParams_bvisprz.weight = 0.2f;
                        lParams_bvisprz.width = 0;
                        lParams_bvisprz.height = LinearLayout.LayoutParams.MATCH_PARENT;
                        ll.addView(lpr);
                    }

                    final TextView tvinfo = new TextView(FormDocListaVendQtaTot.this);
                    tvinfo.setTextColor(Color.RED);
                    tvinfo.setTypeface(tvinfo.getTypeface(), Typeface.BOLD);
                    tvinfo.setText("");
                    ll.addView(tvinfo);
                    LinearLayout lb = new LinearLayout(FormDocListaVendQtaTot.this.getApplicationContext());
                    lb.setOrientation(LinearLayout.HORIZONTAL);
                    Button bdok = new Button(FormDocListaVendQtaTot.this);
                    bdok.setText("OK");
                    bdok.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                    bdok.setTextColor(Color.WHITE);
                    lb.addView(bdok);
                    Button bdann = new Button(FormDocListaVendQtaTot.this);
                    bdann.setText("Annulla");
                    bdann.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                    bdann.setTextColor(Color.WHITE);
                    lb.addView(bdann);
                    LinearLayout.LayoutParams lParams_bdok = (LinearLayout.LayoutParams) bdok.getLayoutParams();
                    lParams_bdok.weight = 0.5f;
                    lParams_bdok.width = 0;
                    LinearLayout.LayoutParams lParams_bdann = (LinearLayout.LayoutParams) bdann.getLayoutParams();
                    lParams_bdann.weight = 0.5f;
                    lParams_bdann.width = 0;
                    ll.addView(lb);

                    final double mpr = minprzrif;
                    bdok.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            String valueqvend = inputqvend.getText().toString();
                            double qvend = OpValute.arrotondaMat(Formattazione.estraiDouble(valueqvend.replace(".", ",")), 3);
                            String valueqsm = inputqsm.getText().toString();
                            double qsm = OpValute.arrotondaMat(Formattazione.estraiDouble(valueqsm.replace(".", ",")), 3);
                            String valueqomimp = inputqomimp.getText().toString();
                            double qomimp = OpValute.arrotondaMat(Formattazione.estraiDouble(valueqomimp.replace(".", ",")), 3);
                            String valueqomtot = inputqomtot.getText().toString();
                            double qomtot = OpValute.arrotondaMat(Formattazione.estraiDouble(valueqomtot.replace(".", ",")), 3);

                            String valuep = inputp.getText().toString();
                            double p = OpValute.arrotondaMat(Formattazione.estraiDouble(valuep.replace(".", ",")), 3);

                            if (qvend > 0) {
                                boolean cbloccvend = FunzioniJBeerApp.causaleBloccata(0, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                                if (cbloccvend) {
                                    tvinfo.setText("Vendita bloccata dalla sede");
                                    return;
                                }
                            }
                            if (qsm > 0) {
                                boolean cbloccsm = FunzioniJBeerApp.causaleBloccata(4, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                                if (cbloccsm) {
                                    tvinfo.setText("Sconto merce bloccata dalla sede");
                                    return;
                                }
                            }
                            if (qomimp > 0) {
                                boolean cbloccomimp = FunzioniJBeerApp.causaleBloccata(5, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                                if (cbloccomimp) {
                                    tvinfo.setText("Omaggio imponibile bloccato dalla sede");
                                    return;
                                }
                            }
                            if (qomtot > 0) {
                                boolean cbloccomtot = FunzioniJBeerApp.causaleBloccata(6, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                                if (cbloccomtot) {
                                    tvinfo.setText("Omaggio totale bloccato dalla sede");
                                    return;
                                }
                            }

                            if (Env.ctrprzrif && Env.visprzrif && mpr > 0 && mpr != 999999 && p < mpr) {
                                tvinfo.setText("ERRORE: Prezzo di vendita minore del prezzo di riferimento");
                                return;
                            } else {
                                rec.eliminaCampo("prezzo");
                                rec.insDouble("prezzo", p);
                                rec.eliminaCampo("qtavend");
                                rec.insDouble("qtavend", qvend);
                                rec.eliminaCampo("qtasm");
                                rec.insDouble("qtasm", qsm);
                                rec.eliminaCampo("qtaomimp");
                                rec.insDouble("qtaomimp", qomimp);
                                rec.eliminaCampo("qtaomtot");
                                rec.insDouble("qtaomtot", qomtot);
                                lvendqtadapter.notifyDataSetChanged();
                                optionDialog.dismiss();
                            }
                        }
                    });

                    bdann.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            optionDialog.dismiss();
                        }
                    });
                    optionDialog.setView(ll);
                    optionDialog.setCancelable(false);
                    optionDialog.show();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        Uri codselez = Uri.parse("content://listavendqt/ANNULLA");
        Intent result = new Intent(Intent.ACTION_PICK, codselez);
        setResult(RESULT_CANCELED, result);
        finish();
    }

    private void aggiornaLista() {
        // mem.qta già inserite in lista
        HashMap<String, Double> hqtavendold = new HashMap();
        HashMap<String, Double> hqtasmold = new HashMap();
        HashMap<String, Double> hqtaomimpold = new HashMap();
        HashMap<String, Double> hqtaomtotold = new HashMap();
        for (int i = 0; i < vart.size(); i++) {
            Record r = vart.get(i);
            if (r.leggiDouble("qtavend") > 0) {
                hqtavendold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtavend")));
            }
            if (r.leggiDouble("qtasm") > 0) {
                hqtasmold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtasm")));
            }
            if (r.leggiDouble("qtaomimp") > 0) {
                hqtaomimpold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtaomimp")));
            }
            if (r.leggiDouble("qtaomtot") > 0) {
                hqtaomtotold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtaomtot")));
            }
        }
        vart.clear();
        // caricamento prodotti
        String c = edcod.getText().toString().trim();
        String d = eddescr.getText().toString().trim();
        c.replace("'", "''");
        d.replace("'", "''");
        Cursor cursor = Env.db.rawQuery(
                "SELECT artcod,artdescr,artum,s,artcodiva,artflagum,artweb_linkscheda FROM articoli WHERE artcod <> '' " +
                        //" AND artgiacenza > 0 " +
                        (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                        (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                        " ORDER BY artcod", null);
        while (cursor.moveToNext()) {
            Record rx = new Record();
            rx.insStringa("artcod", cursor.getString(0));
            rx.insStringa("artdescr", cursor.getString(1));
            rx.insStringa("artum", cursor.getString(2));
            rx.insIntero("artflagum", cursor.getInt(5));
            rx.insStringa("artschedaweb", cursor.getString(6));
            rx.insStringa("codiva", cursor.getString(4));
            double vargiac = 0;
            for (int j = 0; j < Env.trasfdoc_righecrp.size(); j++) {
                Record rr = Env.trasfdoc_righecrp.get(j);
                String acod = rr.leggiStringa("rmartcod");
                String cau = rr.leggiStringa("rmcaumag");
                double qta = rr.leggiDouble("rmqta");
                if (acod.equals(cursor.getString(0)) && !cau.equals("RN")) {
                    if (cau.equals("RV"))
                        vargiac += qta;
                    else
                        vargiac -= qta;
                }
            }
            rx.insDouble("giac", OpValute.arrotondaMat(cursor.getDouble(3) + vargiac, 3));
            double przrif = FunzioniJBeerApp.leggiPrezzoListinoBaseArticolo(cursor.getString(0));
            if (!clicod.equals("")) {
                String cc = clicod;
                if (!clicodpadre.equals(""))
                    cc = clicodpadre;
                double przvend = 0;
                double prznetto = 0;
                double sc1 = 0, sc2 = 0, sc3 = 0;
//                if (Env.prezzopref)
//                {
//                    przvend = FunzioniJBeerApp.leggiPrezzoDaPreferiti(cursor.getString(0), cc);
//                    prznetto = przvend;
//                }
                //if (przvend == 0)
                if (Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
                    double[] przsc = FunzioniJBeerApp.leggiPrezzoScontiArticolo(cursor.getString(0), clicod, clicodpadre, (new Data()).formatta(Data.AAAA_MM_GG, "-"), true);
                    przvend = przsc[0];
                    prznetto = przvend;
                    sc1 = przsc[1];
                    sc2 = przsc[2];
                    sc3 = przsc[3];
                    if (przsc[1] > 0)
                        prznetto = prznetto - (prznetto * przsc[1] / 100);
                    if (przsc[2] > 0)
                        prznetto = prznetto - (prznetto * przsc[2] / 100);
                    if (przsc[3] > 0)
                        prznetto = prznetto - (prznetto * przsc[3] / 100);
                    prznetto = OpValute.arrotondaMat(prznetto, 3);
                }
                rx.insDouble("sc1", sc1);
                rx.insDouble("sc2", sc2);
                rx.insDouble("sc3", sc3);
                rx.insDouble("prezzo", przvend);
                rx.insDouble("prezzonetto", prznetto);
            } else {
                rx.insDouble("sc1", 0);
                rx.insDouble("sc2", 0);
                rx.insDouble("sc3", 0);
                rx.insDouble("prezzo", 0);
                rx.insDouble("prezzonetto", 0);
            }
            rx.insDouble("przrif", przrif);
            double qvend = 0;
            if (hqtavendold.get(cursor.getString(0)) != null)
                qvend = hqtavendold.get(cursor.getString(0)).doubleValue();
            rx.insDouble("qtavend", qvend);
            double qsm = 0;
            if (hqtasmold.get(cursor.getString(0)) != null)
                qsm = hqtasmold.get(cursor.getString(0)).doubleValue();
            rx.insDouble("qtasm", qsm);
            double qomimp = 0;
            if (hqtaomimpold.get(cursor.getString(0)) != null)
                qomimp = hqtaomimpold.get(cursor.getString(0)).doubleValue();
            rx.insDouble("qtaomimp", qomimp);
            double qomtot = 0;
            if (hqtaomtotold.get(cursor.getString(0)) != null)
                qomtot = hqtaomtotold.get(cursor.getString(0)).doubleValue();
            rx.insDouble("qtaomtot", qomtot);
            vart.add(rx);
        }
        cursor.close();
        lvendqtadapter.notifyDataSetChanged();
    }
}
