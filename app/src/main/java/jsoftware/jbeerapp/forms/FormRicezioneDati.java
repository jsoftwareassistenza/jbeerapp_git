package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniHTTP;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.env.funzStringa;


public class FormRicezioneDati extends AppCompatActivity {
    ProgressBar progress = null;
    ProgressBar progress2 = null;
    TextView labelinfo = null;
    TextView labelinfo2 = null;
    ImageView icona = null;
    ProcAggiornamentoDati proc = null;
    boolean filepresente = false;
    boolean aggincorso = false;
    boolean aggiornamento = false;
    boolean aggterminato = false;
    String canonicalPath = "";

    @Override
    public void onBackPressed() {
        if (!aggincorso) {
            Uri ris = Uri.parse("content://aggiornamento/OK");
            Intent result = new Intent(Intent.ACTION_PICK, ris);
            result.putExtra("aggiornamento", aggiornamento);
            setResult(RESULT_CANCELED, result);
            finish();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_ricezione_dati);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        progress = findViewById(R.id.aggdati_progress);
        progress2 = findViewById(R.id.aggdati_progress2);
        labelinfo = findViewById(R.id.aggdati_labelinfo);
        labelinfo2 = findViewById(R.id.aggdati_labelinfo2);
        icona = findViewById(R.id.aggdati_icona);

        if (!FunzioniJBeerApp.dispositivoOnline(getApplicationContext())) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormRicezioneDati.this);
            builder.setMessage("Connessione ad Internet assente")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
        } else {
            proc = new ProcAggiornamentoDati();
            proc.execute();
        }

    }

    private class ProcAggiornamentoDati extends AsyncTask<String, HashMap, String> {
        @Override
        protected String doInBackground(String... arg) {
            aggincorso = true;
            Env.agg_dati_incorso = true;
            aggterminato = false;
            boolean connok = false;
            String err = "";
            if (Env.tiposcambiodati == 0) {
                // sim dati
                if (FunzioniJBeerApp.dispositivoOnline(FormRicezioneDati.this.getApplicationContext())) {
                    connok = true;
                } else {
                    // se attivata WIFI la disabilita
                    if (FunzioniJBeerApp.tipoConnessione(FormRicezioneDati.this.getApplicationContext()).equals("WIFI")) {
                        HashMap h = new HashMap();
                        h.put("label", "Disattivazione Wi-FI...");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);
                        FunzioniJBeerApp.disattivaWiFi(FormRicezioneDati.this.getApplicationContext());
                        int ntent = 0;
                        while (FunzioniJBeerApp.tipoConnessione(FormRicezioneDati.this.getApplicationContext()).equals("WIFI") && ntent < 20) {
                            try {
                                Thread.currentThread().sleep(1000);
                            } catch (Exception esleep) {
                            }
                            ntent++;
                        }
                    }
                    int ntentonline = 0;
                    while (!FunzioniJBeerApp.dispositivoOnline(getApplicationContext()) && ntentonline < 10) {
                        try {
                            Thread.currentThread().sleep(1000);
                        } catch (Exception esleep) {
                        }
                        ntentonline++;
                    }
                    if (FunzioniJBeerApp.dispositivoOnline(getApplicationContext())) {
                        connok = true;
                    } else {
                        err = "Dispositivo non collegato ad Internet\nImpossibile inviare i dati ora";
                        HashMap h = new HashMap();
                        h.put("label", "");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "N");
                        h.put("indeterminate", "N");
                        h.put("error", err);
                        this.publishProgress(h);
                        aggincorso = false;
                        Env.agg_dati_incorso = false;
                    }
                }
            } else {
                // WIFI
                if (FunzioniJBeerApp.dispositivoOnline(FormRicezioneDati.this.getApplicationContext())) {
                    connok = true;
                } else {
                    boolean wifion = false;
                    if (FunzioniJBeerApp.tipoConnessione(FormRicezioneDati.this.getApplicationContext()).equals("WIFI")) {
                        wifion = true;
                    } else {
                        HashMap h = new HashMap();
                        h.put("label", "Attivazione Wi-FI...");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);
                        FunzioniJBeerApp.attivaWiFi(FormRicezioneDati.this.getApplicationContext());
                        int ntent = 0;

                        while (!FunzioniJBeerApp.tipoConnessione(FormRicezioneDati.this.getApplicationContext()).equals("WIFI") && ntent < 20) {
                            Log.v("JBEERAPP", "controllo " + ntent + " attivazione wifi");
                            try {
                                Thread.currentThread().sleep(1000);
                            } catch (Exception esleep) {
                            }
                            ntent++;
                        }
                        if (!FunzioniJBeerApp.tipoConnessione(FormRicezioneDati.this.getApplicationContext()).equals("WIFI")) {
                            h = new HashMap();
                            h.put("label", "");
                            h.put("label2", "");
                            h.put("labelris1", "");
                            h.put("labelris2", "");
                            h.put("labelris3", "");
                            h.put("val", "0");
                            h.put("max", "0");
                            h.put("visible", "N");
                            h.put("indeterminate", "N");
                            h.put("error", "Attivazione Wi-FI fallita");
                            this.publishProgress(h);
                            aggincorso = false;
                            Env.agg_dati_incorso = false;
                        } else {
                            wifion = true;
                        }
                    }
                    if (wifion) {
                        // controlla rete
                        String ssid = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(FormRicezioneDati.this.getApplicationContext());
                        if (ssid.equals(Env.ssidscambiodati)) {
                            connok = true;
                        } else {
                            HashMap h = new HashMap();
                            h.put("label", "Connessione a Wi-FI scambio dati...");
                            h.put("label2", "");
                            h.put("labelris1", "");
                            h.put("labelris2", "");
                            h.put("labelris3", "");
                            h.put("val", "0");
                            h.put("max", "0");
                            h.put("visible", "S");
                            h.put("indeterminate", "S");
                            h.put("error", "");
                            this.publishProgress(h);
                            FunzioniJBeerApp.connettiReteWiFi(getApplicationContext(), Env.ssidscambiodati);
                            String rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                            if (rc.equals(Env.ssidscambiodati))
                                connok = true;
                            else {
                                int ntent = 0;
                                while (!rc.equals(Env.ssidscambiodati) && ntent < 10 && !FunzioniJBeerApp.dispositivoOnline(getApplicationContext())) {
                                    Log.v("JBEERAPP", "tentativo " + ntent + " connessione " + Env.ssidscambiodati);
                                    try {
                                        Thread.currentThread().sleep(1000);
                                    } catch (Exception esleep) {
                                    }
                                    rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                                    if (rc.equals(Env.ssidscambiodati))
                                        connok = true;
                                    ntent++;
                                }
                                if (!connok) {
                                    h = new HashMap();
                                    h.put("label", "");
                                    h.put("label2", "");
                                    h.put("labelris1", "");
                                    h.put("labelris2", "");
                                    h.put("labelris3", "");
                                    h.put("val", "0");
                                    h.put("max", "0");
                                    h.put("visible", "N");
                                    h.put("indeterminate", "N");
                                    h.put("error", "Connessione Wi-FI a rete " + Env.ssidscambiodati + " fallita");
                                    this.publishProgress(h);
                                    aggincorso = false;
                                    Env.agg_dati_incorso = false;
                                }
                            }
                        }
                    }
                }
            }

            if (connok) {
                Cursor cursor = Env.db.rawQuery(
                        "SELECT pedcod,user,pwd,serverjcloud FROM datiterm",
                        null);
                cursor.moveToFirst();
                String tcod = cursor.getString(0);
                String user = cursor.getString(1);
                String pwd = cursor.getString(2);
                String server = cursor.getString(3);
                // pAX
                //server = "http://95.110.213.50:8080/J-Cloud";
                server = Env.ipServerJCloud;
                // legge da server elenco files da ricevere
                HashMap h = new HashMap();
                h.put("label", "Controllo aggiornamenti...");
                h.put("label2", "");
                h.put("val", "0");
                h.put("max", "0");
                h.put("val2", "0");
                h.put("max2", "0");
                h.put("indeterminate", "S");
                this.publishProgress(h);
                ArrayList<HashMap> lf = FunzioniJBeerApp.filesDaRicevere(user, pwd, tcod, server);
                cursor.close();
                for (int i = 0; i < lf.size(); i++) {
                    HashMap elem = lf.get(i);
                }
                if (lf.size() == 0) {
                    h = new HashMap();
                    h.put("label", "Nessun aggiornamento dati presente");
                    h.put("label2", "");
                    h.put("val", "0");
                    h.put("max", "0");
                    h.put("val2", "0");
                    h.put("max2", "0");
                    h.put("indeterminate", "N");
                    this.publishProgress(h);
                    aggincorso = false;
                    Env.agg_dati_incorso = false;
                    aggiornamento = false;
                } else {
                    aggiornamento = true;
                    // svuota cartella cache carico
                    File fcardir = new File(getApplicationContext().getCacheDir() + File.separator + "carico");
                    if (!fcardir.exists())
                        fcardir.mkdir();
                    else {
                        File[] lfs = fcardir.listFiles();
                        for (int i = 0; i < lfs.length; i++) {
                            lfs[i].delete();
                        }
                    }
                    // svuota cartella cache filescarico
                    File fcardir2 = new File(getApplicationContext().getCacheDir() + File.separator + "filescarico");
                    if (!fcardir2.exists())
                        fcardir2.mkdir();
                    else {
                        File[] lfs = fcardir2.listFiles();
                        for (int i = 0; i < lfs.length; i++) {
                            lfs[i].delete();
                        }
                    }
                    // legge tutti i files
                    ArrayList<String> vfileselab = new ArrayList();
                    for (int nf = 0; nf < lf.size(); nf++) {
                        int ntent = 0;
                        boolean downloadok = false;
                        while (!downloadok && ntent < 5) {
                            ntent++;
                            String nomefile = (String) lf.get(nf).get("file");
                            int dimfile = ((Integer) lf.get(nf).get("dimfile")).intValue();
                            h = new HashMap();
                            h.put("label", "Download file " + nomefile);
                            h.put("label2", "");
                            h.put("val", "0");
                            h.put("max", "" + dimfile);
                            h.put("val2", "0");
                            h.put("max2", "0");
                            h.put("indeterminate", "N");
                            this.publishProgress(h);
                            FunzioniJBeerApp.salvaLog("Ricezione file " + nomefile + " dim:" + dimfile + " tentativo " + ntent);
                            try {
                                String content = URLEncoder.encode("user", "UTF-8")
                                        + "="
                                        + URLEncoder.encode(user, "UTF-8")
                                        + "&"
                                        + URLEncoder.encode("pwd", "UTF-8")
                                        + "="
                                        + URLEncoder.encode(pwd, "UTF-8")
                                        + "&"
                                        + URLEncoder.encode("nomefile", "UTF-8")
                                        + "="
                                        + URLEncoder.encode(
                                        nomefile, "UTF-8");
                                BufferedInputStream in = FunzioniHTTP
                                        .inviaPostHttp2(
                                                server + "/service/downloadtrasf.do",
                                                content,
                                                "application/x-www-form-urlencoded", null,
                                                new Hashtable(), false, false, false, "",
                                                "", 10000);
                                boolean fileok = true;
                                String errdownload = "";
                                FileOutputStream fos = new FileOutputStream(getApplicationContext().getCacheDir() + File.separator + "filescarico" + File.separator + nomefile);
                                try {
                                    byte[] data = new byte[1024];
                                    int count;
                                    int tot = 0;
                                    while ((count = in.read(data, 0, 1024)) != -1) {
                                        fos.write(data, 0, count);
                                        tot += count;
                                        h = new HashMap();
                                        h.put("label", "Download file " + nomefile);
                                        h.put("label2", "");
                                        h.put("val", "" + tot);
                                        h.put("max", "" + dimfile);
                                        h.put("val2", "0");
                                        h.put("max2", "0");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //Thread.currentThread().sleep(500);
                                        //System.out.println("COUNT: " + count + " TOT:" + dimfile);
                                    }
                                    in.close();
                                    fos.close();
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    fileok = false;
                                    downloadok = false;
                                    errdownload = ex.getLocalizedMessage();
                                    for (int ne = 0; ne < ex.getStackTrace().length; ne++) {
                                        errdownload += ex.getStackTrace()[ne].toString();
                                    }
                                    Vector verr = funzStringa.creaRighe(errdownload, 255);
                                    for (int ne = 0; ne < verr.size(); ne++)
                                        FunzioniJBeerApp.salvaLog("Errore ricezione file " + nomefile + " tentativo " + ntent + ":" + verr.elementAt(ne));
                                }
                                if (fileok) {
                                    // controllo integrita file carico
                                    File fcheck = new File(getApplicationContext().getCacheDir() + File.separator + "filescarico" + File.separator + nomefile);
                                    if (fcheck.length() == dimfile) {
                                        h = new HashMap();
                                        h.put("label", "Download file " + nomefile + " OK");
                                        h.put("label2", "");
                                        h.put("val", "100");
                                        h.put("max", "100");
                                        h.put("val2", "0");
                                        h.put("max2", "0");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);

                                        vfileselab.add(nomefile);
                                        FunzioniJBeerApp.salvaLog("Ricezione file " + nomefile + " OK");
                                        downloadok = true;
                                    } else {
                                        FunzioniJBeerApp.salvaLog("Errore ricezione file " + nomefile + ": dimensione " + fcheck.length() + " invece di " + dimfile);
                                        downloadok = false;
                                    }
                                } else {
                                    downloadok = false;
                                }
                            } catch (Exception ef) {
                                ef.printStackTrace();
                                downloadok = false;
                            }
                            if (!downloadok) {
                                try {
                                    h = new HashMap();
                                    h.put("label", "Errore su download file " + nomefile + " tentativo " + ntent);
                                    h.put("label2", "");
                                    h.put("val", "100");
                                    h.put("max", "100");
                                    h.put("val2", "0");
                                    h.put("max2", "0");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    Thread.currentThread().sleep(2000);
                                } catch (Exception esleep) {
                                }
                            }
                        }
                    }


                    Env.db.execSQL("DELETE FROM movcaricosede");

                    boolean datitermpres = false;

                    for (int ind = 0; ind < vfileselab.size(); ind++) {
                        String nomefile = vfileselab.get(ind);
                        Log.v("JBEERAPP", "ELAB FILE " + nomefile);
                        FunzioniJBeerApp.salvaLog("Elaborazione carico " + nomefile);
                        try {
                            // svuota cartella cache carico
                            File fx = new File(getApplicationContext().getCacheDir() + File.separator + "carico");
                            if (!fx.exists())
                                fx.mkdir();
                            else {
                                File[] lfs = fx.listFiles();
                                for (int i = 0; i < lfs.length; i++) {
                                    lfs[i].delete();
                                }
                            }

                            // unzip file di carico
                            h = new HashMap();
                            h.put("label", "Decompressione file " + nomefile + "...");
                            h.put("label2", "");
                            h.put("val", "100");
                            h.put("max", "100");
                            h.put("val2", "0");
                            h.put("max2", "0");
                            h.put("indeterminate", "S");
                            this.publishProgress(h);
                            File f = new File(getApplicationContext().getCacheDir() + File.separator + "filescarico" + File.separator + nomefile);
                            FileInputStream fis = new FileInputStream(new File(getApplicationContext().getCacheDir() + File.separator + "filescarico" + File.separator + nomefile));
                            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
                            ZipEntry entry;
                            while ((entry = zis.getNextEntry()) != null) {
                                canonicalPath = f.getCanonicalPath();
                                int count;
                                int BUFFER = 2048;
                                byte[] data = new byte[BUFFER];
                                FileOutputStream fosunzip = new FileOutputStream(
                                        getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + entry.getName());
                                BufferedOutputStream dest = new BufferedOutputStream(fosunzip, BUFFER);
                                while ((count = zis.read(data, 0, BUFFER)) != -1) {
                                    dest.write(data, 0, count);
                                    File fout = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + entry.getName());
                                    try {
                                        String destDirectory = getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + entry.getName();
                                        ensureZipPathSafety(fout, destDirectory);
                                    } catch (Exception e) {
                                        e.printStackTrace();

                                    }
                                }
                                dest.flush();
                                dest.close();
                            }
                            zis.close();
                            fis.close();

                            // aggiornamento database
                            RandomAccessFile fin = null;

                            // dati term
                            File fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "datiterm.txt");
                            if (fb.exists()) {
                                if (fb.exists()) {
                                    datitermpres = true;
                                    try {
                                        SQLiteStatement stmt = Env.db.compileStatement("UPDATE datiterm SET " +
                                                "depcod=?,targamezzo=?,pedcod=?,agecod=?,agenome=?," +
                                                "docbollaval=?,sezbollaval=?,docbollaqta=?,sezbollaqta=?," +
                                                "docfattura=?,sezfattura=?,doccorrisp=?,sezcorrisp=?," +
                                                "docddtcarico=?,sezddtcarico=?," +
                                                "docscaricosede=?,sezscaricosede=?," +
                                                "doctrasfamezzo=?,seztrasfamezzo=?," +
                                                "doctrasfdamezzo=?,seztrasfdamezzo=?," +
                                                "docddtreso=?,sezddtreso=?," +
                                                "docordinesede=?,sezordinesede=?," +
                                                "docrimfs=?,sezrimfs=?," +
                                                "cmvend=?,cmresov=?,cmresonv=?,cmsost=?,cmomaggi=?,cmsm=?," +
                                                "cmddtcarico=?,cmscaricosede=?,cmtrasfamezzo=?,cmtrasfdamezzo=?," +
                                                "cmdistrmerce=?,cmordinesede=?,cmrprom=?,cmscaduto=?," +
                                                "depsede=?," +
                                                "codivaomaggiimp20=?,codivaomaggiimp10=?,codivaomaggiimp04=?," +
                                                "codivaomaggitot20=?,codivaomaggitot10=?,codivaomaggitot04=?," +
                                                "clinuovocod=?,clinuovolist=?,clinuovoposprz=?,clinuovopag=?,cliordine=?,autcod=?,autdescr=?,auttarga=?,tiponuovicli=?,tipogestlotti=?," +
                                                "intazriga1=?,intazriga2=?,intazriga3=?,intazriga4=?,intazriga5=?,visprzrif=?,ctrprzrif=?,bloccosconti=?,obbligoincpagtv=?,pwdcambiovend=?,tipoannullamentodoc=?," +
                                                "nscadblocco=?,ggtollblocco=?,impblocco=?,nrighelogo=?,stampasedesec=?,stampaddtcar_classi=?,stampaord=?,lottineg=?,listbase=?,datainizioblocco=?,intazriga6=?," +
                                                "intazriga7=?,trasmdopofg=?,inviotrasfvsmezzo=?,noxeterm=?,ddtstampatimbro=?,ddtdatiage=?,noscarsede=?,notrasfda=?,notrasfvs=?,noanndoc=?," +
                                                "azioneblocco=?,vendqtatot=?,prezzopref=?,ddtstamparaggrlotti=?,ddtstampascop=?,ddtqtanocess=?,finegstampadatiart=?,finegstampaelencodoc=?,backupauto=?,tipobackupauto=?,docordinecli=?,sezordinecli=?," +
                                                "stampascasede=?,stampatrasfvs=?,stampatrasfda=?,scasedenovalvend=?,scasedenovalrot=?,scasedenovalscad=?,stampaordtotali=?,maxscvend=?,visprzacq=?,incattivanote=?,incsceglicassa=?," +
                                                "noordsede=?,nosostlotti=?,nogestlist=?,nocambiovend=?,nocambiogirovisita=?,noprztrasp=?,noinv=?,nomenustampe=?,incnostampa=?,tipoterm=?,depgiac=?," +
                                                "sceltaemail=?,emailfirmariga1=?,emailfirmariga2=?,emailfirmariga3=?,abbuoniattivi=?,backupperiodico=?,calcaccisa=?,tipocalcaccisa=?,voceaccisa=?," +
                                                "pwdanndoc=?,pwdpar=?,passcodetrasf=?,copieddt=?,copieddtqta=?,finegstampasospdainc=?,noannstampe=?,colli1=?,usalistinonuovo=?,finegdistintadenaro=?,pwdutilita=?,invioordineauto=?," +
                                                "ordinicligiacterm=?,orddepsede=?,moddoc =?");
                                        fin = new RandomAccessFile(fb, "r");
                                        h = new HashMap();
                                        h.put("label", "Importazione dati term...");
                                        h.put("label2", "");
                                        h.put("val", "100");
                                        h.put("max", "100");
                                        h.put("val2", "1");
                                        h.put("max2", "26");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        int prog = 0;
                                        String linea = fin.readLine();
                                        if (linea != null) {
                                            String[] campi = linea.split("\\|");
                                            stmt.clearBindings();
                                            stmt.bindString(1, campi[1].trim());
                                            Env.depcod = campi[1].trim();
                                            stmt.bindString(2, campi[2].trim());
                                            Env.targamezzo = campi[2].trim();
                                            stmt.bindString(3, campi[3].trim());
                                            Env.pedcod = campi[3].trim();
                                            stmt.bindString(4, campi[5].trim());
                                            stmt.bindString(5, campi[6].trim());
                                            Env.agecod = campi[5].trim();
                                            Env.agenome = campi[6].trim();
                                            stmt.bindString(6, campi[7].trim());
                                            stmt.bindString(7, campi[8].trim());
                                            Env.docbollaval = campi[7].trim();
                                            Env.sezbollaval = campi[8].trim();
                                            stmt.bindString(8, campi[9].trim());
                                            stmt.bindString(9, campi[10].trim());
                                            Env.docbollaqta = campi[9].trim();
                                            Env.sezbollaqta = campi[10].trim();
                                            stmt.bindString(10, campi[11].trim());
                                            stmt.bindString(11, campi[12].trim());
                                            Env.docfattura = campi[11].trim();
                                            Env.sezfattura = campi[12].trim();
                                            stmt.bindString(12, campi[13].trim());
                                            stmt.bindString(13, campi[14].trim());
                                            Env.doccorrisp = campi[13].trim();
                                            Env.sezcorrisp = campi[14].trim();
                                            stmt.bindString(14, campi[15].trim());
                                            stmt.bindString(15, campi[16].trim());
                                            Env.docddtcarico = campi[15].trim();
                                            Env.sezddtcarico = campi[16].trim();
                                            stmt.bindString(16, campi[17].trim());
                                            stmt.bindString(17, campi[18].trim());
                                            Env.docscaricosede = campi[17].trim();
                                            Env.sezscaricosede = campi[18].trim();
                                            stmt.bindString(18, campi[19].trim());
                                            stmt.bindString(19, campi[20].trim());
                                            Env.doctrasfamezzo = campi[19].trim();
                                            Env.seztrasfamezzo = campi[20].trim();
                                            stmt.bindString(20, campi[21].trim());
                                            stmt.bindString(21, campi[22].trim());
                                            Env.doctrasfdamezzo = campi[21].trim();
                                            Env.seztrasfdamezzo = campi[22].trim();
                                            stmt.bindString(22, campi[23].trim());
                                            stmt.bindString(23, campi[24].trim());
                                            Env.docddtreso = campi[23].trim();
                                            Env.sezddtreso = campi[24].trim();
                                            stmt.bindString(24, campi[51].trim());
                                            stmt.bindString(25, campi[52].trim());
                                            Env.docordinesede = campi[51].trim();
                                            Env.sezordinesede = campi[52].trim();
                                            stmt.bindString(26, campi[57].trim());
                                            stmt.bindString(27, campi[58].trim());
                                            Env.docrimfs = campi[57].trim();
                                            Env.sezrimfs = campi[58].trim();
                                            stmt.bindString(28, campi[31].trim());
                                            Env.cmvend = campi[31].trim();
                                            stmt.bindString(29, campi[32].trim());
                                            Env.cmresov = campi[32].trim();
                                            stmt.bindString(30, campi[33].trim());
                                            Env.cmresonv = campi[33].trim();
                                            stmt.bindString(31, campi[34].trim());
                                            Env.cmann = campi[34].trim();
                                            stmt.bindString(32, campi[35].trim());
                                            Env.cmomaggi = campi[35].trim();
                                            stmt.bindString(33, campi[36].trim());
                                            Env.cmsm = campi[36].trim();
                                            stmt.bindString(34, campi[37].trim());
                                            Env.cmddtcarico = campi[37].trim();
                                            stmt.bindString(35, campi[38].trim());
                                            Env.cmscaricosede = campi[38].trim();
                                            stmt.bindString(36, campi[39].trim());
                                            Env.cmtrasfamezzo = campi[39].trim();
                                            stmt.bindString(37, campi[40].trim());
                                            Env.cmtrasfdamezzo = campi[40].trim();
                                            stmt.bindString(38, campi[41].trim());
                                            Env.cmdistrmerce = campi[41].trim();
                                            stmt.bindString(39, campi[53].trim());
                                            Env.cmordinesede = campi[53].trim();
                                            stmt.bindString(40, campi[68].trim());
                                            Env.cmrprom = campi[68].trim();
                                            stmt.bindString(41, campi[71].trim());
                                            Env.cmscaduto = campi[71].trim();
                                            stmt.bindString(42, campi[25].trim());
                                            Env.depsede = campi[25].trim();
                                            // codici iva omaggi
                                            stmt.bindString(43, campi[42].trim());
                                            stmt.bindString(44, campi[43].trim());
                                            stmt.bindString(45, campi[44].trim());
                                            stmt.bindString(46, campi[45].trim());
                                            stmt.bindString(47, campi[46].trim());
                                            stmt.bindString(48, campi[47].trim());
                                            Env.codivaomaggiimp20 = campi[42].trim();
                                            Env.codivaomaggiimp10 = campi[43].trim();
                                            Env.codivaomaggiimp04 = campi[44].trim();
                                            Env.codivaomaggitot20 = campi[45].trim();
                                            Env.codivaomaggitot10 = campi[46].trim();
                                            Env.codivaomaggitot04 = campi[47].trim();
                                            // cliente nuovo
                                            stmt.bindString(49, campi[54].trim());
                                            Env.clinuovocod = campi[54].trim();
                                            stmt.bindString(50, campi[48].trim());
                                            Env.clinuovolist = campi[48].trim();
                                            stmt.bindLong(51, Formattazione.estraiIntero(campi[49].trim()));
                                            Env.clinuovoposprz = Formattazione.estraiIntero(campi[49].trim());
                                            stmt.bindString(52, campi[50].trim());
                                            Env.clinuovopag = campi[50].trim();
                                            stmt.bindString(53, campi[55].trim());
                                            Env.cliordine = campi[55].trim();
                                            stmt.bindString(54, "");
                                            Env.autcod = "";
                                            stmt.bindString(55, "");
                                            Env.autdescr = "";
                                            stmt.bindString(56, "");
                                            Env.auttarga = "";
                                            Env.targamezzo = campi[2].trim();
                                            stmt.bindLong(57, Formattazione.estraiIntero(campi[76].trim()));
                                            Env.tiponuovicli = Formattazione.estraiIntero(campi[76].trim());
                                            stmt.bindLong(58, Formattazione.estraiIntero(campi[77].trim()));
                                            Env.tipogestlotti = Formattazione.estraiIntero(campi[77].trim());
                                            stmt.bindString(59, campi[78].trim());
                                            stmt.bindString(60, campi[79].trim());
                                            stmt.bindString(61, campi[80].trim());
                                            stmt.bindString(62, campi[81].trim());
                                            stmt.bindString(63, campi[82].trim());
                                            Env.intazriga1 = campi[78].trim();
                                            Env.intazriga2 = campi[79].trim();
                                            Env.intazriga3 = campi[80].trim();
                                            Env.intazriga4 = campi[81].trim();
                                            Env.intazriga5 = campi[82].trim();
                                            if (campi.length >= 84) {
                                                stmt.bindString(64, campi[83].trim());
                                                Env.visprzrif = (campi[83].trim().equals("S"));
                                            } else {
                                                stmt.bindString(64, "N");
                                                Env.visprzrif = false;
                                            }
                                            if (campi.length >= 85) {
                                                stmt.bindString(65, campi[84].trim());
                                                Env.ctrprzrif = (campi[84].trim().equals("S"));
                                            } else {
                                                stmt.bindString(65, "N");
                                                Env.ctrprzrif = false;
                                            }
                                            Log.v("JBEERAPP", "CTRPRZRIF:" + Env.ctrprzrif);
                                            if (campi.length >= 86) {
                                                stmt.bindString(66, campi[85].trim());
                                                Env.bloccosconti = (campi[85].trim().equals("S"));
                                            } else {
                                                stmt.bindString(66, "N");
                                                Env.bloccosconti = false;
                                            }
                                            if (campi.length >= 87) {
                                                stmt.bindString(67, campi[86].trim());
                                                Env.obbligoincpagtv = (campi[86].trim().equals("S"));
                                            } else {
                                                stmt.bindString(67, "N");
                                                Env.obbligoincpagtv = false;
                                            }
                                            if (campi.length >= 88) {
                                                stmt.bindString(68, campi[87].trim());
                                                Env.pwdcambiovend = (campi[87].trim().equals("S"));
                                            } else {
                                                stmt.bindString(68, "N");
                                                Env.pwdcambiovend = false;
                                            }
                                            if (campi.length >= 89) {
                                                stmt.bindString(69, campi[88].trim());
                                                Env.tipoannullamentodoc = Formattazione.estraiIntero(campi[88].trim());
                                            } else {
                                                stmt.bindString(69, "N");
                                                Env.tipoannullamentodoc = 0;
                                            }
                                            stmt.bindLong(70, Formattazione.estraiIntero(campi[74].trim()));
                                            Env.nscadblocco = Formattazione.estraiIntero(campi[74].trim());
                                            if (campi.length >= 90) {
                                                stmt.bindLong(71, Formattazione.estraiIntero(campi[89].trim()));
                                                Env.ggtollblocco = Formattazione.estraiIntero(campi[89].trim());
                                            } else {
                                                stmt.bindLong(71, 10);
                                                Env.ggtollblocco = 10;
                                            }
                                            stmt.bindDouble(72, Formattazione.estraiDouble(campi[75].trim()));
                                            Env.impblocco = Formattazione.estraiDouble(campi[75].trim());
                                            if (campi.length >= 91) {
                                                stmt.bindLong(73, Formattazione.estraiIntero(campi[90].trim()));
                                                Env.nrighelogo = Formattazione.estraiIntero(campi[90].trim());
                                            } else {
                                                stmt.bindLong(73, 7);
                                                Env.nrighelogo = 7;
                                            }
                                            if (campi.length >= 92) {
                                                stmt.bindString(74, campi[91].trim());
                                                Env.stampasedesec = (campi[91].trim().equals("S"));
                                            } else {
                                                stmt.bindString(74, "N");
                                                Env.stampasedesec = false;
                                            }
                                            if (campi.length >= 93) {
                                                stmt.bindString(75, campi[92].trim());
                                                Env.stampaddtcar_classi = (campi[92].trim().equals("S"));
                                            } else {
                                                stmt.bindString(75, "N");
                                                Env.stampaddtcar_classi = false;
                                            }
                                            if (campi.length >= 94) {
                                                stmt.bindString(76, campi[93].trim());
                                                Env.stampaord = (campi[93].trim().equals("S"));
                                            } else {
                                                stmt.bindString(76, "N");
                                                Env.stampaord = false;
                                            }
                                            if (campi.length >= 95) {
                                                stmt.bindString(77, campi[94].trim());
                                                Env.lottineg = (campi[94].trim().equals("S"));
                                            } else {
                                                stmt.bindString(77, "N");
                                                Env.lottineg = false;
                                            }
                                            if (campi.length >= 96) {
                                                stmt.bindString(78, campi[95].trim());
                                                Env.listbase = campi[95].trim();
                                            } else {
                                                stmt.bindString(78, "");
                                                Env.listbase = "";
                                            }
                                            if (campi.length >= 97) {
                                                stmt.bindString(79, campi[96].trim());
                                                Env.datainizioblocco = campi[96].trim();
                                            } else {
                                                stmt.bindString(79, "");
                                                Env.datainizioblocco = "";
                                            }
                                            if (campi.length >= 98) {
                                                stmt.bindString(80, campi[97].trim());
                                                Env.intazriga6 = campi[97].trim();
                                            } else {
                                                stmt.bindString(80, "");
                                                Env.intazriga6 = "";
                                            }
                                            if (campi.length >= 99) {
                                                stmt.bindString(81, campi[98].trim());
                                                Env.intazriga7 = campi[98].trim();
                                            } else {
                                                stmt.bindString(81, "");
                                                Env.intazriga7 = "";
                                            }
                                            if (campi.length >= 100) {
                                                stmt.bindString(82, campi[99].trim());
                                                Env.inviodopofineg = (campi[99].trim().equals("S"));
                                            } else {
                                                stmt.bindString(82, "N");
                                                Env.inviodopofineg = false;
                                            }
                                            if (campi.length >= 101) {
                                                stmt.bindString(83, campi[100].trim());
                                                Env.inviotrasfvsmezzo = (campi[100].trim().equals("S"));
                                            } else {
                                                stmt.bindString(83, "N");
                                                Env.inviotrasfvsmezzo = false;
                                            }
                                            if (campi.length >= 102) {
                                                stmt.bindString(84, campi[101].trim());
                                                Env.noxeterm = (campi[101].trim().equals("S"));
                                            } else {
                                                stmt.bindString(84, "N");
                                                Env.noxeterm = false;
                                            }
                                            if (campi.length >= 103) {
                                                stmt.bindString(85, campi[102].trim());
                                                Env.ddtstampatimbro = (campi[102].trim().equals("S"));
                                            } else {
                                                stmt.bindString(85, "N");
                                                Env.ddtstampatimbro = false;
                                            }
                                            if (campi.length >= 104) {
                                                stmt.bindLong(86, Formattazione.estraiIntero(campi[103].trim()));
                                                Env.ddtdatiage = Formattazione.estraiIntero(campi[103].trim());
                                            } else {
                                                stmt.bindLong(86, 0);
                                                Env.ddtdatiage = 0;
                                            }
                                            if (campi.length >= 105) {
                                                stmt.bindString(87, campi[104].trim());
                                                Env.noscarsede = (campi[104].trim().equals("S"));
                                            } else {
                                                stmt.bindString(87, "N");
                                                Env.noscarsede = false;
                                            }
                                            if (campi.length >= 106) {
                                                stmt.bindString(88, campi[105].trim());
                                                Env.notrasfvs = (campi[105].trim().equals("S"));
                                            } else {
                                                stmt.bindString(88, "N");
                                                Env.notrasfvs = false;
                                            }
                                            if (campi.length >= 107) {
                                                stmt.bindString(89, campi[106].trim());
                                                Env.notrasfda = (campi[106].trim().equals("S"));
                                            } else {
                                                stmt.bindString(89, "N");
                                                Env.notrasfda = false;
                                            }
                                            if (campi.length >= 108) {
                                                stmt.bindString(90, campi[107].trim());
                                                Env.noanndoc = (campi[107].trim().equals("S"));
                                            } else {
                                                stmt.bindString(90, "N");
                                                Env.noanndoc = false;
                                            }
                                            if (campi.length >= 109) {
                                                stmt.bindLong(91, Formattazione.estraiIntero(campi[108].trim()));
                                                Env.azioneblocco = Formattazione.estraiIntero(campi[108].trim());
                                            } else {
                                                stmt.bindLong(91, 0);
                                                Env.azioneblocco = 0;
                                            }
                                            if (campi.length >= 110) {
                                                stmt.bindString(92, campi[109].trim());
                                                Env.vendqtatot = (campi[109].trim().equals("S"));
                                            } else {
                                                stmt.bindString(92, "N");
                                                Env.vendqtatot = false;
                                            }
                                            if (campi.length >= 111) {
                                                stmt.bindString(93, campi[110].trim());
                                                Env.prezzopref = (campi[110].trim().equals("S"));
                                            } else {
                                                stmt.bindString(93, "N");
                                                Env.prezzopref = false;
                                            }
                                            if (campi.length >= 112) {
                                                stmt.bindString(94, campi[111].trim());
                                                Env.ddtstamparaggrlotti = (campi[111].trim().equals("S"));
                                            } else {
                                                stmt.bindString(94, "N");
                                                Env.ddtstamparaggrlotti = false;
                                            }
                                            if (campi.length >= 113) {
                                                stmt.bindString(95, campi[112].trim());
                                                Env.ddtstampascop = (campi[112].trim().equals("S"));
                                            } else {
                                                stmt.bindString(95, "N");
                                                Env.ddtstampascop = false;
                                            }
                                            if (campi.length >= 114) {
                                                stmt.bindString(96, campi[113].trim());
                                                Env.ddtqtanocess = (campi[113].trim().equals("S"));
                                            } else {
                                                stmt.bindString(96, "N");
                                                Env.ddtqtanocess = false;
                                            }
                                            if (campi.length >= 115) {
                                                stmt.bindString(97, campi[114].trim());
                                                Env.finegstampadatiart = (campi[114].trim().equals("S"));
                                            } else {
                                                stmt.bindString(97, "N");
                                                Env.finegstampadatiart = false;
                                            }
                                            if (campi.length >= 116) {
                                                stmt.bindString(98, campi[115].trim());
                                                Env.finegstampaelencodoc = (campi[115].trim().equals("S"));
                                            } else {
                                                stmt.bindString(98, "N");
                                                Env.finegstampaelencodoc = false;
                                            }
                                            if (campi.length >= 117) {
                                                stmt.bindString(99, campi[116].trim());
                                                Env.backupauto = (campi[116].trim().equals("S"));
                                            } else {
                                                stmt.bindString(99, "N");
                                                Env.backupauto = false;
                                            }
                                            if (campi.length >= 118) {
                                                stmt.bindLong(100, Formattazione.estraiIntero(campi[117].trim()));
                                                Env.tipobackupauto = Formattazione.estraiIntero(campi[117].trim());
                                            } else {
                                                stmt.bindLong(100, 0);
                                                Env.tipobackupauto = 0;
                                            }
                                            if (campi.length >= 120) {
                                                stmt.bindString(101, campi[118].trim());
                                                stmt.bindString(102, campi[119].trim());
                                                Env.docordinecli = campi[118].trim();
                                                Env.sezordinecli = campi[119].trim();
                                            } else {
                                                Env.docordinecli = "";
                                                Env.sezordinecli = "";
                                            }
                                            if (campi.length >= 121) {
                                                stmt.bindString(103, campi[120].trim());
                                                Env.stampascasede = (campi[120].trim().equals("S"));
                                            } else {
                                                stmt.bindString(103, "N");
                                                Env.stampascasede = false;
                                            }
                                            if (campi.length >= 122) {
                                                stmt.bindString(104, campi[121].trim());
                                                Env.stampatrasfvs = (campi[121].trim().equals("S"));
                                            } else {
                                                stmt.bindString(104, "N");
                                                Env.stampatrasfvs = false;
                                            }
                                            if (campi.length >= 123) {
                                                stmt.bindString(105, campi[122].trim());
                                                Env.stampatrasfda = (campi[122].trim().equals("S"));
                                            } else {
                                                stmt.bindString(105, "N");
                                                Env.stampatrasfda = false;
                                            }
                                            if (campi.length >= 124) {
                                                stmt.bindString(106, campi[123].trim());
                                                Env.scasedenovalvend = (campi[123].trim().equals("S"));
                                            } else {
                                                stmt.bindString(106, "N");
                                                Env.scasedenovalvend = false;
                                            }
                                            if (campi.length >= 125) {
                                                stmt.bindString(107, campi[124].trim());
                                                Env.scasedenovalrot = (campi[124].trim().equals("S"));
                                            } else {
                                                stmt.bindString(107, "N");
                                                Env.scasedenovalrot = false;
                                            }
                                            if (campi.length >= 126) {
                                                stmt.bindString(108, campi[125].trim());
                                                Env.scasedenovalscad = (campi[125].trim().equals("S"));
                                            } else {
                                                stmt.bindString(108, "N");
                                                Env.scasedenovalscad = false;
                                            }
                                            if (campi.length >= 127) {
                                                stmt.bindString(109, campi[126].trim());
                                                Env.stampaordtotali = (campi[126].trim().equals("S"));
                                            } else {
                                                stmt.bindString(109, "N");
                                                Env.stampaordtotali = false;
                                            }
                                            if (campi.length >= 128) {
                                                stmt.bindDouble(110, Formattazione.estraiDouble(campi[127].trim()));
                                                Env.maxscvend = Formattazione.estraiDouble(campi[127].trim());
                                            } else {
                                                stmt.bindDouble(110, 0);
                                                Env.maxscvend = 0;
                                            }
                                            if (campi.length >= 129) {
                                                stmt.bindString(111, campi[128].trim());
                                                Env.visprzacq = (campi[128].trim().equals("S"));
                                            } else {
                                                stmt.bindString(111, "N");
                                                Env.visprzacq = false;
                                            }
                                            if (campi.length >= 130) {
                                                stmt.bindString(112, campi[129].trim());
                                                Env.incattivanote = (campi[129].trim().equals("S"));
                                            } else {
                                                stmt.bindString(112, "N");
                                                Env.incattivanote = false;
                                            }
                                            if (campi.length >= 131) {
                                                stmt.bindString(113, campi[130].trim());
                                                Env.incsceglicassa = (campi[130].trim().equals("S"));
                                            } else {
                                                stmt.bindString(113, "N");
                                                Env.incsceglicassa = false;
                                            }
                                            if (campi.length >= 132) {
                                                stmt.bindString(114, campi[131].trim());
                                                Env.noordsede = (campi[131].trim().equals("S"));
                                            } else {
                                                stmt.bindString(114, "N");
                                                Env.noordsede = false;
                                            }
                                            if (campi.length >= 133) {
                                                stmt.bindString(115, campi[132].trim());
                                                Env.nosostlotti = (campi[132].trim().equals("S"));
                                            } else {
                                                stmt.bindString(115, "N");
                                                Env.nosostlotti = false;
                                            }
                                            if (campi.length >= 134) {
                                                stmt.bindString(116, campi[133].trim());
                                                Env.nogestlist = (campi[133].trim().equals("S"));
                                            } else {
                                                stmt.bindString(116, "N");
                                                Env.nogestlist = false;
                                            }
                                            if (campi.length >= 135) {
                                                stmt.bindString(117, campi[134].trim());
                                                Env.nocambiovend = (campi[134].trim().equals("S"));
                                            } else {
                                                stmt.bindString(117, "N");
                                                Env.nocambiovend = false;
                                            }
                                            if (campi.length >= 136) {
                                                stmt.bindString(118, campi[135].trim());
                                                Env.nocambiogirovisita = (campi[135].trim().equals("S"));
                                            } else {
                                                stmt.bindString(118, "N");
                                                Env.nocambiogirovisita = false;
                                            }
                                            if (campi.length >= 137) {
                                                stmt.bindString(119, campi[136].trim());
                                                Env.noprztrasp = (campi[136].trim().equals("S"));
                                            } else {
                                                stmt.bindString(119, "N");
                                                Env.noprztrasp = false;
                                            }
                                            if (campi.length >= 138) {
                                                stmt.bindString(120, campi[137].trim());
                                                Env.noinv = (campi[137].trim().equals("S"));
                                            } else {
                                                stmt.bindString(120, "N");
                                                Env.noinv = false;
                                            }
                                            if (campi.length >= 139) {
                                                stmt.bindString(121, campi[138].trim());
                                                Env.nomenustampe = (campi[138].trim().equals("S"));
                                            } else {
                                                stmt.bindString(121, "N");
                                                Env.nomenustampe = false;
                                            }
                                            if (campi.length >= 140) {
                                                stmt.bindString(122, campi[139].trim());
                                                Env.incnostampa = (campi[139].trim().equals("S"));
                                            } else {
                                                stmt.bindString(122, "N");
                                                Env.incnostampa = false;
                                            }
                                            if (campi.length >= 141) {
                                                stmt.bindLong(123, Formattazione.estraiIntero(campi[140].trim()));
                                                Env.tipoterm = Formattazione.estraiIntero(campi[140].trim());
                                            } else {
                                                stmt.bindLong(123, 0);
                                                Env.tipoterm = Env.TIPOTERM_TENTATAVENDITA;
                                            }
                                            if (campi.length >= 142) {
                                                stmt.bindString(124, campi[141].trim());
                                                Env.depgiac = campi[141].trim();
                                            } else {
                                                stmt.bindString(124, "");
                                                Env.depgiac = "";
                                            }
                                            if (campi.length >= 143) {
                                                stmt.bindString(125, campi[142].trim());
                                                Env.sceltaemail = (campi[142].trim().equals("S"));
                                            } else {
                                                stmt.bindString(125, "N");
                                                Env.sceltaemail = false;
                                            }
                                            if (campi.length >= 144) {
                                                stmt.bindString(126, campi[143].trim());
                                                Env.emailfirmariga1 = campi[143].trim();
                                            } else {
                                                stmt.bindString(126, "");
                                                Env.emailfirmariga1 = "";
                                            }
                                            if (campi.length >= 145) {
                                                stmt.bindString(127, campi[144].trim());
                                                Env.emailfirmariga2 = campi[144].trim();
                                            } else {
                                                stmt.bindString(127, "");
                                                Env.emailfirmariga2 = "";
                                            }
                                            if (campi.length >= 146) {
                                                stmt.bindString(128, campi[145].trim());
                                                Env.emailfirmariga3 = campi[145].trim();
                                            } else {
                                                stmt.bindString(128, "");
                                                Env.emailfirmariga3 = "";
                                            }
                                            if (campi.length >= 147) {
                                                stmt.bindString(129, campi[146].trim());
                                                Env.abbuoniattivi = (campi[146].trim().equals("S"));
                                            } else {
                                                stmt.bindString(129, "N");
                                                Env.abbuoniattivi = false;
                                            }
                                            if (campi.length >= 148) {
                                                stmt.bindLong(130, Formattazione.estraiIntero(campi[147].trim()));
                                                Env.backupperiodico = Formattazione.estraiIntero(campi[147].trim());
                                            } else {
                                                stmt.bindLong(130, 0);
                                                Env.backupperiodico = 0;
                                            }
                                            if (campi.length >= 150) {
                                                stmt.bindString(131, campi[149].trim());
                                                Env.calcaccisa = (campi[149].trim().equals("S"));
                                            } else {
                                                stmt.bindString(131, "N");
                                                Env.calcaccisa = false;
                                            }
                                            if (campi.length >= 151) {
                                                stmt.bindLong(132, Formattazione.estraiIntero(campi[150].trim()));
                                                Env.tipocalcaccisa = Formattazione.estraiIntero(campi[150].trim());
                                            } else {
                                                stmt.bindLong(132, 0);
                                                Env.tipocalcaccisa = 0;
                                            }
                                            if (campi.length >= 152) {
                                                stmt.bindString(133, campi[151].trim());
                                                Env.voceaccisa = campi[151].trim();
                                            } else {
                                                stmt.bindString(133, "");
                                                Env.voceaccisa = "";
                                            }
                                            //parte del contrassegno saltata
                                            if (campi.length >= 154) {
                                                stmt.bindString(134, campi[153].trim());
                                                Env.pwdanndoc = (campi[153].trim().equals("S"));
                                            } else {
                                                stmt.bindString(134, "N");
                                                Env.pwdanndoc = false;
                                            }
                                            if (campi.length >= 155) {
                                                stmt.bindString(135, campi[154].trim());
                                                Env.pwdpar = (campi[154].trim().equals("S"));
                                            } else {
                                                stmt.bindString(135, "N");
                                                Env.pwdpar = false;
                                            }
                                            if (campi.length >= 156) {
                                                stmt.bindString(136, campi[155].trim());
                                                Env.passcodetrasf = (campi[155].trim().equals("S"));
                                            } else {
                                                stmt.bindString(136, "N");
                                                Env.passcodetrasf = false;
                                            }
                                            if (campi.length >= 157) {
                                                stmt.bindLong(137, Formattazione.estraiIntero(campi[156].trim()));
                                                Env.copieddt = Formattazione.estraiIntero(campi[156].trim());
                                            } else {
                                                stmt.bindLong(137, 0);
                                                Env.copieddt = 0;
                                            }
                                            if (campi.length >= 158) {
                                                stmt.bindLong(138, Formattazione.estraiIntero(campi[157].trim()));
                                                Env.copieddtqta = Formattazione.estraiIntero(campi[157].trim());
                                            } else {
                                                stmt.bindLong(138, 0);
                                                Env.copieddtqta = 0;
                                            }
                                            if (campi.length >= 159) {
                                                stmt.bindString(139, campi[158].trim());
                                                Env.finegstampasospdainc = (campi[158].trim().equals("S"));
                                            } else {
                                                stmt.bindString(139, "S");
                                                Env.finegstampasospdainc = true;
                                            }
                                            if (campi.length >= 160) {
                                                stmt.bindString(140, campi[159].trim());
                                                Env.noannstampe = (campi[159].trim().equals("S"));
                                            } else {
                                                stmt.bindString(140, "N");
                                                Env.noannstampe = false;
                                            }
                                            if (campi.length >= 161) {
                                                stmt.bindString(141, campi[160].trim());
                                                Env.colli1 = (campi[160].trim().equals("S"));
                                            } else {
                                                stmt.bindString(141, "N");
                                                Env.colli1 = false;
                                            }
                                            if (campi.length >= 162) {
                                                stmt.bindString(142, campi[161].trim());
                                                Env.usalistinonuovo = (campi[161].trim().equals("S"));
                                            } else {
                                                stmt.bindString(142, "N");
                                                Env.usalistinonuovo = false;
                                            }
                                            if (campi.length >= 163) {
                                                stmt.bindString(143, campi[162].trim());
                                                Env.finegdistintadenaro = (campi[162].trim().equals("S"));
                                            } else {
                                                stmt.bindString(143, "N");
                                                Env.finegdistintadenaro = false;
                                            }
                                            if (campi.length >= 164) {
                                                stmt.bindString(144, campi[163].trim());
                                                Env.pwdutilita = (campi[163].trim().equals("S"));
                                            } else {
                                                stmt.bindString(144, "N");
                                                Env.pwdutilita = false;
                                            }
                                            if (campi.length >= 165) {
                                                stmt.bindString(145, campi[164].trim());
                                                Env.invioordineauto = (campi[164].trim().equals("S"));
                                            } else {
                                                stmt.bindString(145, "N");
                                                Env.invioordineauto = false;
                                            }
                                            if (campi.length >= 166) {
                                                stmt.bindString(146, campi[165].trim());
                                                Env.ordinicligiacterm = (campi[165].trim().equals("S"));
                                            } else {
                                                stmt.bindString(146, "N");
                                                Env.ordinicligiacterm = false;
                                            }
                                            if (campi.length >= 167) {
                                                stmt.bindString(147, campi[166].trim());
                                                Env.orddepsede = (campi[166].trim().equals("S"));
                                            } else {
                                                stmt.bindString(147, "N");
                                                Env.orddepsede = false;
                                            }
                                            if (campi.length >= 171) {
                                                stmt.bindString(148, campi[170].trim());
                                                Env.moddoc = (campi[170].trim().equals("S"));
                                            } else {
                                                stmt.bindString(148, "N");
                                                Env.moddoc = false;
                                            }
                                            stmt.execute();
                                        }
                                        stmt.close();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } finally {
                                        try {
                                            fin.close();
                                        } catch (Exception e2) {
                                        }
                                    }
                                }
                            }

                            // banche
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "banche.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM banche");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO banche (bcod,bdescr,babi) VALUES (?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione banche...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "2");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    int nr = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        nr++;
                                        prog += linea.length();
                                        if (nr % 10 == 0) {
                                            h = new HashMap();
                                            h.put("label", "Importazione banche...");
                                            h.put("label2", "");
                                            h.put("val", "" + prog);
                                            h.put("max", "" + fin.length());
                                            h.put("val2", "2");
                                            h.put("max2", "30");
                                            h.put("indeterminate", "N");
                                            this.publishProgress(h);
                                        }
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.execute();
                                        linea = fin.readLine();

                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // codiva
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "codiva.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM codiva");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO codiva (ivacod,ivadescr,ivagruppo,ivaaliq,ivapercind,ivagest,ivacodivagest,ivaomaggiotot) "
                                                    + "VALUES (?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione codici IVA...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "3");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione codici IVA...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "3");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindLong(3, Formattazione.estraiIntero(campi[2]));
                                        stmt.bindDouble(4, Formattazione.estraiDouble(campi[3]));
                                        stmt.bindDouble(5, Formattazione.estraiDouble(campi[7]));
                                        stmt.bindLong(6, Formattazione.estraiIntero(campi[9]));
                                        stmt.bindString(7, campi[10].trim());
                                        stmt.bindString(8, "N");
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // depositi
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "depositi.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM depositi");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO depositi (depcod,depdescr,depubic1,depubic2,depeventi,depdatai,depdataf) VALUES (?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione depositi...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "4");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione depositi...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "4");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[3].trim());
                                        stmt.bindString(4, campi[4].trim());
                                        String depeventi = campi[7].trim();
                                        stmt.bindLong(5, depeventi.equals("S") ? 1 : 0);
                                        stmt.bindString(6, campi[8].trim());
                                        stmt.bindString(7, campi[9].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // automezzi
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "automezzi.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM automezzi");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO automezzi (amcod,amdescr,amtarga) VALUES (?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione automezzi...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "5");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione automezzi...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "5");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // agenti
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "agenti.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM agenti");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO agenti (agecod,agedescr,agemaxsc,ageindirizzo,agelocalita,agepartitaiva,agecodicefiscale,agenregalbo,agetel,agefax,agemobile,ageemail) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione agenti...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "6");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione agenti...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "6");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        if (campi.length == 3) {
                                            stmt.clearBindings();
                                            stmt.bindString(1, campi[0].trim());
                                            stmt.bindString(2, campi[1].trim());
                                            stmt.bindDouble(3, 100);
                                            stmt.bindString(4, "");
                                            stmt.bindString(5, "");
                                            stmt.bindString(6, "");
                                            stmt.bindString(7, "");
                                            stmt.bindString(8, "");
                                            stmt.bindString(9, "");
                                            stmt.bindString(10, "");
                                            stmt.bindString(11, "");
                                            stmt.bindString(12, "");
                                            stmt.execute();
                                        } else {
                                            stmt.clearBindings();
                                            stmt.bindString(1, campi[0].trim());
                                            stmt.bindString(2, campi[1].trim());
                                            stmt.bindDouble(3, 100);
                                            stmt.bindString(4, campi[3].trim());
                                            stmt.bindString(5, campi[4].trim());
                                            stmt.bindString(6, campi[5].trim());
                                            stmt.bindString(7, campi[6].trim());
                                            stmt.bindString(8, campi[7].trim());
                                            if (campi.length >= 9) {
                                                stmt.bindString(9, campi[8].trim());
                                                stmt.bindString(10, campi[9].trim());
                                                stmt.bindString(11, campi[10].trim());
                                                stmt.bindString(12, campi[11].trim());
                                            } else {
                                                stmt.bindString(9, "");
                                                stmt.bindString(10, "");
                                                stmt.bindString(11, "");
                                                stmt.bindString(12, "");
                                            }
                                            stmt.execute();
                                        }
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }

                            }

                            // marche
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "marche.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM marche");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO marche (marcacod,marcadescr) VALUES (?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione marche...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "7");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione marche...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "7");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // sottoclassi
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "sottoclassi.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM sottoclassi");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO sottoclassi (clcod,sccod,scdescr) VALUES (?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione sottoclassi...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "8");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione sottoclassi...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "8");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // raggrart
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "raggrart.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM raggrart");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO raggrart (raggrcod,raggrdescr) VALUES (?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione raggruppamenti...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "8");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione raggruppamenti...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "8");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // aggregazioni
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "aggregazioni.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM scontilist");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO scontilist (sllistcod,slcodsc,slsc1_1,slsc1_2,slsc2_1,slsc2_2,"
                                                    + "slsc3_1,slsc3_2,slsc4_1,slsc4_2,slsc5_1,slsc5_2,slsc6_1,slsc6_2,"
                                                    + "slsc7_1,slsc7_2,slsc8_1,slsc8_2,slsc9_1,slsc9_2,slescltesta) "
                                                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione aggregazioni sconto...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "9");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione aggregazioni sconto...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "9");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[1].trim());
                                        stmt.bindString(2, campi[0].trim());
                                        stmt.bindDouble(3, -Formattazione.estraiDouble(campi[8]));
                                        stmt.bindDouble(4, -Formattazione.estraiDouble(campi[17]));
                                        stmt.bindDouble(5, -Formattazione.estraiDouble(campi[9]));
                                        stmt.bindDouble(6, -Formattazione.estraiDouble(campi[18]));
                                        stmt.bindDouble(7, -Formattazione.estraiDouble(campi[10]));
                                        stmt.bindDouble(8, -Formattazione.estraiDouble(campi[19]));
                                        stmt.bindDouble(9, -Formattazione.estraiDouble(campi[11]));
                                        stmt.bindDouble(10, -Formattazione.estraiDouble(campi[20]));
                                        stmt.bindDouble(11, -Formattazione.estraiDouble(campi[12]));
                                        stmt.bindDouble(12, -Formattazione.estraiDouble(campi[21]));
                                        stmt.bindDouble(13, -Formattazione.estraiDouble(campi[13]));
                                        stmt.bindDouble(14, -Formattazione.estraiDouble(campi[22]));
                                        stmt.bindDouble(15, -Formattazione.estraiDouble(campi[14]));
                                        stmt.bindDouble(16, -Formattazione.estraiDouble(campi[23]));
                                        stmt.bindDouble(17, -Formattazione.estraiDouble(campi[15]));
                                        stmt.bindDouble(18, -Formattazione.estraiDouble(campi[24]));
                                        stmt.bindDouble(19, -Formattazione.estraiDouble(campi[16]));
                                        stmt.bindDouble(20, -Formattazione.estraiDouble(campi[25]));
                                        if (campi.length >= 38)
                                            stmt.bindLong(21, Formattazione.estraiIntero(campi[37]));
                                        else
                                            stmt.bindLong(21, 0);
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // giorni pagamenti
                            HashMap hgg = new HashMap();
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "ggfissi.txt");
                            if (fb.exists()) {
                                try {
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione giorni pagamenti...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "10");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione giorni pagamenti...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "10");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        String[] campi = linea.split("\\|");
                                        if (hgg.get(campi[0].trim()) == null) {
                                            ArrayList<Integer> v = new ArrayList();
                                            v.add(Formattazione.estraiIntero(campi[1].trim()));
                                            hgg.put(campi[0].trim(), v);
                                        } else {
                                            ArrayList<Integer> v = (ArrayList<Integer>) hgg.get(campi[0].trim());
                                            v.add(Formattazione.estraiIntero(campi[1].trim()));
                                        }
                                        linea = fin.readLine();
                                    }
                                } catch (Exception e) {
                                    hgg = new HashMap();
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // pagamenti
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "pagamenti.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM pagamenti");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO pagamenti (pagcod,pagdescr,pagtipo,pagspeseinc,"
                                                    + "pagcodivaspeseinc,pagnrate,pagbancacod,pagbancadescr,pagabi,"
                                                    + "pagagenzia,pagcab,pagtipocalc,paggg1,paggg2,paggg3,paggg4,paggg5,paggg6,pagsconto) "
                                                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione pagamenti...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "11");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione pagamenti...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "11");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindLong(3, Formattazione.estraiIntero(campi[3]));
                                        stmt.bindLong(4, Formattazione.estraiIntero(campi[4]));
                                        stmt.bindString(5, campi[5].trim());
                                        stmt.bindLong(6, Formattazione.estraiIntero(campi[11]));
                                        stmt.bindString(7, campi[25].trim());
                                        stmt.bindString(8, campi[26].trim());
                                        stmt.bindString(9, campi[27].trim());
                                        stmt.bindString(10, campi[28].trim());
                                        stmt.bindString(11, campi[29].trim());
                                        if (Formattazione.estraiIntero(campi[22].trim()) == 0)
                                            stmt.bindString(12, "DF");
                                        else
                                            stmt.bindString(12, "FM");
                                        if (Formattazione.estraiIntero(campi[21].trim()) > 0) {
                                            int gg = Formattazione.estraiIntero(campi[21].trim());
                                            for (int i = 1; i <= 6; i++) {
                                                if (i <= Formattazione.estraiIntero(campi[11].trim()))
                                                    stmt.bindLong((12 + i), gg * i);
                                                else
                                                    stmt.bindLong((12 + i), 0);
                                            }
                                        } else {
                                            // cerca in file ggfissi
                                            ArrayList<Integer> v = (ArrayList<Integer>) hgg.get(campi[0].trim());
                                            if (v != null) {
                                                if (v.size() >= 1)
                                                    stmt.bindLong(13, v.get(0));
                                                else
                                                    stmt.bindLong(13, 0);
                                                if (v.size() >= 2)
                                                    stmt.bindLong(14, v.get(1));
                                                else
                                                    stmt.bindLong(14, 0);
                                                if (v.size() >= 3)
                                                    stmt.bindLong(15, v.get(2));
                                                else
                                                    stmt.bindLong(15, 0);
                                                if (v.size() >= 4)
                                                    stmt.bindLong(16, v.get(3));
                                                else
                                                    stmt.bindLong(16, 0);
                                                if (v.size() >= 5)
                                                    stmt.bindLong(17, v.get(4));
                                                else
                                                    stmt.bindLong(17, 0);
                                                if (v.size() >= 6)
                                                    stmt.bindLong(18, v.get(5));
                                                else
                                                    stmt.bindLong(18, 0);
                                            } else {
                                                stmt.bindLong(13, 0);
                                                stmt.bindLong(14, 0);
                                                stmt.bindLong(15, 0);
                                                stmt.bindLong(16, 0);
                                                stmt.bindLong(17, 0);
                                                stmt.bindLong(18, 0);
                                            }
                                        }
                                        if (campi.length >= 32)
                                            stmt.bindDouble(19, Formattazione.estraiDouble(campi[31]));
                                        else
                                            stmt.bindDouble(19, 0);
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // articoli
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "articoli.txt");
                            if (fb.exists()) {
                                try {
                                    // memorizza giacenze articoli
                                    Env.db.beginTransaction();
                                    HashMap<String, Double> hgiac = new HashMap();
                                    Cursor cart = Env.db.rawQuery("SELECT artcod,artgiacenza FROM articoli", null);
                                    while (cart.moveToNext()) {
                                        hgiac.put(cart.getString(0), cart.getDouble(1));
                                    }
                                    cart.close();
                                    ArrayList<String> vartnuovi = new ArrayList();

                                    Env.db.execSQL("DELETE FROM articoli");
                                    SQLiteStatement stmt = Env.db.compileStatement("INSERT INTO articoli ("
                                            + "artcod," + "artdescr," + "artum," + "artcodiva,"
                                            + "artpezzi," + "artscprom," + "artclassecod,"
                                            + "artsottoclassecod," + "artmarcacod,"
                                            + "artscdatainizio," + "artscdatafine," + "artcpesclaltri,"
                                            + "artcauvend," + "artcausm," + "artcauom," + "artcaureso,"
                                            + "artcausost," + "artgiacenza," + "artcodsc," + "artpeso,"
                                            + "artcauzcod,artggscad,artart62,artstato,arttipoord,artnotificaterm,artraggrcod,artflagum,"
                                            + "artnoord,artcodbarre,artweb_linkscheda,artprzacq) "
                                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione articoli...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "12");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int ndis = 0;
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione articoli...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "12");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[2].trim());
                                        stmt.bindString(3, campi[4].trim());
                                        stmt.bindString(4, campi[3].trim());
                                        stmt.bindDouble(5, Formattazione.estraiDouble(campi[7].trim()));
                                        stmt.bindDouble(6, Formattazione.estraiDouble(campi[12].trim()));
                                        stmt.bindString(7, campi[20].trim());
                                        stmt.bindString(8, campi[21].trim());
                                        stmt.bindString(9, campi[24].trim());
                                        stmt.bindString(10, campi[13].trim());
                                        stmt.bindString(11, campi[14].trim());
                                        stmt.bindString(12, "N");
                                        stmt.bindString(13, "S");
                                        stmt.bindString(14, "S");
                                        stmt.bindString(15, "S");
                                        stmt.bindString(16, "S");
                                        stmt.bindString(17, "S");
                                        if (hgiac.get(campi[0].trim()) != null)
                                            stmt.bindDouble(18, hgiac.get(campi[0].trim()));
                                        else {
                                            vartnuovi.add(campi[0].trim());
                                            stmt.bindDouble(18, 0);
                                        }
                                        stmt.bindString(19, campi[23].trim());
                                        stmt.bindDouble(20, Formattazione.estraiDouble(campi[8].trim()));
                                        stmt.bindString(21, campi[16].trim());
                                        stmt.bindLong(22, Formattazione.estraiIntero(campi[71].trim()));
                                        stmt.bindLong(23, Formattazione.estraiIntero(campi[72].trim()));
                                        String stato = "A";
                                        if (Formattazione.estraiIntero(campi[64].trim()) == 3) {
                                            stato = "E";
                                            ndis++;
                                        }
                                        stmt.bindString(24, stato);
                                        if (campi.length >= 75)
                                            stmt.bindLong(25, Formattazione.estraiIntero(campi[74].trim()));
                                        else
                                            stmt.bindLong(25, 0);
                                        if (campi.length >= 76)
                                            stmt.bindLong(26, Formattazione.estraiIntero(campi[75].trim()));
                                        else
                                            stmt.bindLong(26, 0);
                                        stmt.bindString(27, campi[25].trim());
                                        stmt.bindLong(28, Formattazione.estraiIntero(campi[5].trim()));
                                        if (campi.length >= 77) {
                                            int noord = Formattazione.estraiIntero(campi[76].trim());
                                            if (noord == 1)
                                                stmt.bindString(29, "S");
                                            else
                                                stmt.bindString(29, "N");
                                        } else
                                            stmt.bindString(29, "N");
                                        stmt.bindString(30, campi[1].trim());
                                        if (campi.length >= 78) {
                                            stmt.bindString(31, campi[77].trim());
                                        } else
                                            stmt.bindString(31, "");
                                        if (campi.length >= 74)
                                            stmt.bindDouble(32, Formattazione.estraiDouble(campi[73].trim()));
                                        else
                                            stmt.bindDouble(32, 0);
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();

                                    // articoli nuovi -> inserisce lotti da xe già arrivate da sede
                                    for (int nn = 0; nn < vartnuovi.size(); nn++) {
                                        String codartnuovo = vartnuovi.get(nn);
                                        System.out.println("ART.NUOVO:" + codartnuovo);
                                        String[] parsxe = new String[1];
                                        parsxe[0] = codartnuovo;
                                        double g = 0;
                                        Cursor cxe = Env.db.rawQuery("SELECT mcqta,mclotto FROM movcaricosedestor WHERE mcartcod = ?", parsxe);
                                        while (cxe.moveToNext()) {
                                            System.out.println("- TROVATO LOTTO:" + cxe.getString(1) + " QTA:" + cxe.getDouble(0));
                                            FunzioniJBeerApp.aggiornaGiacenzaLotto(cxe.getString(1), codartnuovo, cxe.getDouble(0));
                                            g += cxe.getDouble(0);
                                        }
                                        cxe.close();
                                        g = OpValute.arrotondaMat(g, 3);
                                        FunzioniJBeerApp.impostaGiacenzaArticolo(codartnuovo, g);
                                        double gg = FunzioniJBeerApp.leggiGiacenzaArticolo(codartnuovo);
                                        System.out.println("- GIAC.FINALE:" + gg);
                                    }

                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();

                                    //FunzioniJazzTv.creaArtBis();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }
                            // artaccisa
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "artaccisa.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM artaccisa");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO artaccisa (artaartcod,artarea6,artagrado,artata16,artaaccisa,artatipoacc,artacontrassegno,artacn,artanc,artaum,artaplato) "
                                                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione articoli-accisa...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "13");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione articoli-accisa...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "13");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        try {
                                            stmt.clearBindings();
                                            stmt.bindString(1, campi[0].trim());
                                            stmt.bindLong(2, Formattazione.estraiIntero(campi[1].trim()));
                                            stmt.bindDouble(3, Formattazione.estraiDouble(campi[2].trim()));
                                            stmt.bindString(4, campi[4].trim());
                                            stmt.bindDouble(5, Formattazione.estraiDouble(campi[5].trim()));
                                            stmt.bindLong(6, Formattazione.estraiIntero(campi[6].trim()));
                                            stmt.bindDouble(7, Formattazione.estraiDouble(campi[7].trim()));
                                            stmt.bindLong(8, Formattazione.estraiIntero(campi[8].trim()));
                                            stmt.bindString(9, campi[9].trim());
                                            stmt.bindString(10, campi[10].trim());
                                            stmt.bindDouble(11, Formattazione.estraiDouble(campi[3].trim()));
                                            stmt.execute();
                                        } catch (Exception erigalist) {
                                        }
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }

                            }

                            // valaccisa
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "valaccisa.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM valaccisa");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO valaccisa (vaartcod,vanazione,vadataval,vaaccisa,vagradiiniz1,vagradifine1,vaaccgradi1," +
                                                    "vagradiiniz2,vagradifine2,vaaccgradi2,vagradiiniz3,vagradifine3,vaaccgradi3,vagradiiniz4,vagradifine4,vaaccgradi4," +
                                                    "vagradiiniz5,vagradifine5,vaaccgradi5,vagradiiniz6,vagradifine6,vaaccgradi6,vatipocalc) " +
                                                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione valori accisa...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "13");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione valori accisa...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "13");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        try {
                                            stmt.clearBindings();
                                            stmt.bindString(1, campi[0].trim());
                                            stmt.bindString(2, campi[1].trim());
                                            stmt.bindString(3, campi[2].trim());
                                            stmt.bindDouble(4, Formattazione.estraiDouble(campi[3].trim()));
                                            stmt.bindDouble(5, Formattazione.estraiDouble(campi[4].trim()));
                                            stmt.bindDouble(6, Formattazione.estraiDouble(campi[5].trim()));
                                            stmt.bindDouble(7, Formattazione.estraiDouble(campi[6].trim()));
                                            stmt.bindDouble(8, Formattazione.estraiDouble(campi[7].trim()));
                                            stmt.bindDouble(9, Formattazione.estraiDouble(campi[8].trim()));
                                            stmt.bindDouble(10, Formattazione.estraiDouble(campi[9].trim()));
                                            stmt.bindDouble(11, Formattazione.estraiDouble(campi[10].trim()));
                                            stmt.bindDouble(12, Formattazione.estraiDouble(campi[11].trim()));
                                            stmt.bindDouble(13, Formattazione.estraiDouble(campi[12].trim()));
                                            stmt.bindDouble(14, Formattazione.estraiDouble(campi[13].trim()));
                                            stmt.bindDouble(15, Formattazione.estraiDouble(campi[14].trim()));
                                            stmt.bindDouble(16, Formattazione.estraiDouble(campi[15].trim()));
                                            stmt.bindDouble(17, Formattazione.estraiDouble(campi[16].trim()));
                                            stmt.bindDouble(18, Formattazione.estraiDouble(campi[17].trim()));
                                            stmt.bindDouble(19, Formattazione.estraiDouble(campi[18].trim()));
                                            stmt.bindDouble(20, Formattazione.estraiDouble(campi[19].trim()));
                                            stmt.bindDouble(21, Formattazione.estraiDouble(campi[20].trim()));
                                            stmt.bindDouble(22, Formattazione.estraiDouble(campi[21].trim()));
                                            stmt.execute();
                                        } catch (Exception erigalist) {
                                        }
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }

                            }

                            // listini
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "listini.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM listini");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO listini (lcod,lartcod,ldataval,lprezzo1,lprezzo2,lprezzo3,lprezzo4,lprezzo5,lprezzo6,lprezzo7,lprezzo8,lprezzo9) "
                                                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione listini...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "13");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione listini...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "13");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        try {
                                            stmt.clearBindings();
                                            stmt.bindString(1, campi[0].trim());
                                            stmt.bindString(2, campi[2].trim());
                                            stmt.bindString(3, campi[3].trim());
                                            stmt.bindDouble(4, Formattazione.estraiDouble(campi[4].trim()));
                                            stmt.bindDouble(5, Formattazione.estraiDouble(campi[5].trim()));
                                            stmt.bindDouble(6, Formattazione.estraiDouble(campi[6].trim()));
                                            stmt.bindDouble(7, Formattazione.estraiDouble(campi[7].trim()));
                                            stmt.bindDouble(8, Formattazione.estraiDouble(campi[8].trim()));
                                            stmt.bindDouble(9, Formattazione.estraiDouble(campi[9].trim()));
                                            stmt.bindDouble(10, Formattazione.estraiDouble(campi[10].trim()));
                                            stmt.bindDouble(11, Formattazione.estraiDouble(campi[11].trim()));
                                            stmt.bindDouble(12, Formattazione.estraiDouble(campi[12].trim()));
                                            stmt.execute();
                                        } catch (Exception erigalist) {
                                        }
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // catalogo
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "catalogo.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM catalogo");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO catalogo (catartcod,catstile,catcolore,catlistcod,catgrado,catnomeart,catnote) "
                                                    + "VALUES (?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione catalogo...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "13");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione catalogo...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "13");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        try {
                                            stmt.clearBindings();
                                            String artcod = campi[0].trim();
                                            double grado = Formattazione.estraiDouble(campi[1].trim());
                                            String listcod = campi[2].trim();
                                            String stile = campi[3].trim();
                                            stile = funzStringa.sostituisci(stile, '#', '\r');
                                            stile = funzStringa.sostituisci(stile, '§', '\n');
                                            if (stile.length() > 255)
                                                stile = stile.substring(0, 255);
                                            String colore = campi[4].trim();
                                            colore = funzStringa.sostituisci(colore, '#', '\r');
                                            colore = funzStringa.sostituisci(colore, '§', '\n');
                                            if (colore.length() > 255)
                                                colore = colore.substring(0, 255);
                                            String note = campi[5].trim();
                                            note = funzStringa.sostituisci(note, '#', '\r');
                                            note = funzStringa.sostituisci(note, '§', '\n');
                                            String descrart = campi[6].trim();
                                            descrart = funzStringa.sostituisci(descrart, '#', '\r');
                                            descrart = funzStringa.sostituisci(descrart, '§', '\n');
                                            if (descrart.length() > 255)
                                                descrart = descrart.substring(0, 255);
                                            stmt.bindString(1, artcod);
                                            stmt.bindString(2, stile);
                                            stmt.bindString(3, colore);
                                            stmt.bindString(4, listcod);
                                            stmt.bindDouble(5, grado);
                                            stmt.bindString(6, descrart);
                                            stmt.bindString(7, note);
                                            stmt.execute();
                                        } catch (Exception erigacatalogo) {
                                        }
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }


                            // cauzioni
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "cauzioni.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM cauzioni");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO cauzioni (cauzcod,cauzdescr,cauzdescrbreve,cauztipo,cauztipocalc,cauzc1cod,cauzc2cod,cauzc3cod,cauzc1qta,cauzc2qta,cauzc3qta,cauzprezzo,cauzposdoc,cauzcapacita) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione cauzioni...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "14");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione cauzioni...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "14");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.bindLong(4, Formattazione.estraiIntero(campi[3].trim()));
                                        stmt.bindLong(5, Formattazione.estraiIntero(campi[7].trim()));
                                        stmt.bindString(6, campi[10].trim());
                                        stmt.bindString(7, campi[11].trim());
                                        stmt.bindString(8, campi[12].trim());
                                        stmt.bindDouble(9, Formattazione.estraiDouble(campi[13].trim()));
                                        stmt.bindDouble(10, Formattazione.estraiDouble(campi[14].trim()));
                                        stmt.bindDouble(11, Formattazione.estraiDouble(campi[15].trim()));
                                        stmt.bindDouble(12, Formattazione.estraiDouble(campi[16].trim()));
                                        stmt.bindLong(13, Formattazione.estraiIntero(campi[8].trim()));
                                        stmt.bindDouble(14, Formattazione.estraiDouble(campi[5].trim()));
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "condpers.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM condpers");
                                    SQLiteStatement stmt = Env.db.compileStatement("INSERT INTO condpers ("
                                            + "cpclicod," + "cpartcod," + "cpdatainizio,"
                                            + "cpdatafine," + "cpsc1," + "cpsc2," + "cpsc3,"
                                            + "cplistcod," + "cpprezzo," + "cpesclaltri,cpescltestata) "
                                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione condizioni personalizzate...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "15");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione condizioni personalizzate...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "15");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        if (!campi[0].trim().equals(""))
                                            stmt.bindString(1, campi[0].trim());
                                        else
                                            stmt.bindString(1, campi[18].trim());
                                        stmt.bindString(2, campi[2].trim());
                                        stmt.bindString(3, campi[8].trim());
                                        stmt.bindString(4, campi[9].trim());
                                        stmt.bindDouble(5, Formattazione.estraiDouble(campi[11].trim()));
                                        stmt.bindDouble(6, Formattazione.estraiDouble(campi[12].trim()));
                                        stmt.bindDouble(7, Formattazione.estraiDouble(campi[13].trim()));
                                        stmt.bindString(8, campi[14].trim());
                                        stmt.bindDouble(9, Formattazione.estraiDouble(campi[17].trim()));
                                        if (campi.length >= 20) {
                                            if (Formattazione.estraiIntero(campi[19].trim()) == 1)
                                                stmt.bindString(10, "S");
                                            else
                                                stmt.bindString(10, "N");
                                        } else
                                            stmt.bindString(10, "N");
                                        if (campi.length >= 21) {
                                            if (Formattazione.estraiIntero(campi[20].trim()) == 1)
                                                stmt.bindString(11, "S");
                                            else
                                                stmt.bindString(11, "N");
                                        } else
                                            stmt.bindString(11, "N");
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // clienti
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "clienti.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM clienti WHERE clinuovo = 0");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO clienti ("
                                                    + "clicodice,"
                                                    + "clinome,"
                                                    + "cliindir,"
                                                    + "clicap,"
                                                    + "cliloc,"
                                                    + "cliprov,"
                                                    + "clipiva,"
                                                    + "clicodpadre,"
                                                    + "clicodpag,"
                                                    + "clicodiva,"
                                                    + "clinumtel,"
                                                    + "clinumcell,"
                                                    + "clinumfax,"
                                                    + "clicodlist,"
                                                    + "clinprz,"
                                                    + "climodprz,"
                                                    + "clisc1,"
                                                    + "clisc2,"
                                                    + "clisc3,"
                                                    + "climag,"
                                                    + "clifido,"
                                                    + "clibloccofido,"
                                                    + "climaxscvend,"
                                                    + "clispeseinc,"
                                                    + "clispesebol,"
                                                    + "clispesetrasp,"
                                                    + "clispesecauz,"
                                                    + "clibancacod,"
                                                    + "clibancadescr,"
                                                    + "cliabi,"
                                                    + "cliagenzia,"
                                                    + "clicab,"
                                                    + "climodpag,"
                                                    + "climastro,"
                                                    + "cliconto,"
                                                    + "clisottoconto,"
                                                    + "cliblocco,"
                                                    + "clinomeemergenze,"
                                                    + "clitelemergenze,"
                                                    + "clitelammin,"
                                                    + "clitelcomm,"
                                                    + "clitelabitaz,"
                                                    + "clitelsedeop,"
                                                    + "cliemail1,"
                                                    + "cliemail2,"
                                                    + "cliemailpec,"
                                                    + "clisitoweb,"
                                                    + "cliordine,"
                                                    + "cligpsnordsud,"
                                                    + "cligpsestovest,"
                                                    + "cligpsnordgradi,"
                                                    + "cligpsnordmin,"
                                                    + "cligpsnordsec,"
                                                    + "cligpsestgradi,"
                                                    + "cligpsestmin,"
                                                    + "cligpsestsec,"
                                                    + "clicanalecod,"
                                                    + "clicanaledescr,"
                                                    + "clicategcod,"
                                                    + "clicategdescr,"
                                                    + "clizonacod,"
                                                    + "clizonadescr,"
                                                    + "clinuovo,"
                                                    + "clivalidato,"
                                                    + "cliraggrddt,"
                                                    + "clistampaprz,"
                                                    + "clicausm,"
                                                    + "clicauom,"
                                                    + "clicaureso,"
                                                    + "clicausost,"
                                                    + "clibollaval,"
                                                    + "clibollaqta,"
                                                    + "clifattura,"
                                                    + "clicorrisp,"
                                                    + "cliddtreso,"
                                                    + "clinumcontr,"
                                                    + "clidatacontr,"
                                                    + "cliordlotto,"
                                                    + "clinscadblocco,"
                                                    + "cliimpblocco,"
                                                    + "cliggtollblocco,"
                                                    + "clidtblocco,"
                                                    + "cliazioneblocco,"
                                                    + "clidocpref,"
                                                    + "clitipo,"
                                                    + "clinomepri,"
                                                    + "clicognomepri,"
                                                    + "cliinsegna,"
                                                    + "clicodsdinuovo"
                                                    + ") "
                                                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione clienti...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "16");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione clienti...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "16");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        String cod = campi[0].trim();
                                        String piva = campi[6].trim();
                                        String[] pars = new String[1];
                                        pars[0] = cod;
                                        boolean clipres = false;
                                        Cursor cc = Env.db.rawQuery("SELECT clicodice FROM clienti WHERE clicodice = ?", pars);
                                        if (cc.moveToFirst())
                                            clipres = true;
                                        cc.close();
                                        if (!clipres) {
                                            // controllo clienti nuovi
                                            String[] parscn = new String[1];
                                            parscn[0] = piva;
                                            Cursor ccn = Env.db.rawQuery("SELECT clicodice FROM clienti WHERE clipiva = ? AND clinuovo = 1", parscn);
                                            if (ccn.moveToFirst()) {
                                                String codcn = ccn.getString(0);
                                                SQLiteStatement stdc = Env.db
                                                        .compileStatement("DELETE FROM clienti WHERE clicodice=?");
                                                stdc.clearBindings();
                                                stdc.bindString(1, codcn);
                                                stdc.execute();
                                                stdc.close();
                                                SQLiteStatement stum = Env.db
                                                        .compileStatement("UPDATE movimenti SET movclicod=? WHERE movclicod=?");
                                                stum.clearBindings();
                                                stum.bindString(1, cod);
                                                stum.bindString(2, codcn);
                                                stum.execute();
                                                stum.close();
                                                SQLiteStatement stusc = Env.db
                                                        .compileStatement("UPDATE scoperti SET scclicod=? WHERE scclicod=?");
                                                stusc.clearBindings();
                                                stusc.bindString(1, cod);
                                                stusc.bindString(2, codcn);
                                                stusc.execute();
                                                stusc.close();
                                                SQLiteStatement stui = Env.db
                                                        .compileStatement("UPDATE incassi SET iscclicod=? WHERE iscclicod=?");
                                                stui.clearBindings();
                                                stui.bindString(1, cod);
                                                stui.bindString(2, codcn);
                                                stui.execute();
                                                stui.close();
                                            }
                                            ccn.close();

                                            stmt.clearBindings();
                                            stmt.bindString(1, campi[0].trim());
                                            stmt.bindString(2, campi[1].trim());
                                            stmt.bindString(3, campi[2].trim());
                                            stmt.bindString(4, campi[3].trim());
                                            stmt.bindString(5, campi[4].trim());
                                            stmt.bindString(6, campi[5].trim());
                                            stmt.bindString(7, campi[6].trim()); // piva
                                            stmt.bindString(8, campi[7].trim()); // codpadre
                                            stmt.bindString(9, campi[8].trim()); // codpag
                                            stmt.bindString(10, campi[9].trim()); // codiva
                                            stmt.bindString(11, campi[10].trim()); // num tel
                                            stmt.bindString(12, campi[11].trim()); // mobile
                                            stmt.bindString(13, campi[12].trim()); // fax
                                            stmt.bindString(14, campi[13].trim()); // listino
                                            stmt.bindLong(15, FunzioniJBeerApp.estraiIntero(campi[14].trim())); // fascia
                                            // prezzo
                                            stmt.bindString(16, campi[16].trim()); // mod prz
                                            stmt.bindDouble(17, FunzioniJBeerApp.estraiDouble(campi[18].trim())); // sconti
                                            stmt.bindDouble(18, FunzioniJBeerApp.estraiDouble(campi[19].trim())); // sconti
                                            stmt.bindDouble(19, FunzioniJBeerApp.estraiDouble(campi[20].trim())); // sconti
                                            stmt.bindDouble(20, FunzioniJBeerApp.estraiDouble(campi[21].trim())); // maggiorazione
                                            stmt.bindDouble(21, FunzioniJBeerApp.estraiDouble(campi[22].trim())); // fido
                                            stmt.bindString(22, campi[23].trim()); // blocco fido
                                            stmt.bindDouble(23, FunzioniJBeerApp.estraiDouble(campi[28].trim())); // maxscvend
                                            stmt.bindString(24, campi[29].trim()); // spese incasso
                                            stmt.bindString(25, campi[30].trim()); // spese bolli
                                            stmt.bindString(26, campi[31].trim()); // spese sped
                                            stmt.bindString(27, campi[32].trim()); // spese cauz
                                            stmt.bindString(28, campi[33].trim()); // banca cod
                                            stmt.bindString(29, campi[34].trim()); // banca descr
                                            stmt.bindString(30, campi[35].trim()); // banca abi
                                            stmt.bindString(31, campi[36].trim()); // agenzia
                                            stmt.bindString(32, campi[37].trim()); // cab
                                            stmt.bindString(33, campi[38].trim()); // modpag
                                            stmt.bindString(34, campi[40].trim()); // mastro conto
                                            // sottoconto
                                            stmt.bindString(35, campi[41].trim());
                                            stmt.bindString(36, campi[42].trim());
                                            stmt.bindString(37, campi[48].trim()); // blocco

                                            stmt.bindString(38, campi[55].trim()); // nome emergenze
                                            stmt.bindString(39, campi[56].trim()); // tel emergenze
                                            stmt.bindString(40, campi[57].trim()); // tel amministrativo
                                            stmt.bindString(41, campi[58].trim()); // tel commerciale
                                            stmt.bindString(42, campi[59].trim()); // tel abitaz
                                            stmt.bindString(43, campi[60].trim()); // tel sede op
                                            stmt.bindString(44, campi[61].trim()); // email 1
                                            stmt.bindString(45, campi[62].trim()); // email 2
                                            stmt.bindString(46, campi[63].trim()); // email pec
                                            stmt.bindString(47, campi[64].trim()); // sito web
                                            if (campi.length >= 66)
                                                stmt.bindString(48, campi[65].trim()); // flag ordine
                                            else
                                                stmt.bindString(48, "S");
                                            if (campi.length >= 74) {
                                                // gps
                                                stmt.bindString(49, campi[66].trim());
                                                stmt.bindString(50, campi[67].trim());
                                                stmt.bindDouble(51, FunzioniJBeerApp.estraiDouble(campi[68].trim()));
                                                stmt.bindDouble(52, FunzioniJBeerApp.estraiDouble(campi[69].trim()));
                                                stmt.bindDouble(53, FunzioniJBeerApp.estraiDouble(campi[70].trim()));
                                                stmt.bindDouble(54, FunzioniJBeerApp.estraiDouble(campi[71].trim()));
                                                stmt.bindDouble(55, FunzioniJBeerApp.estraiDouble(campi[72].trim()));
                                                stmt.bindDouble(56, FunzioniJBeerApp.estraiDouble(campi[73].trim()));
                                            } else {
                                                stmt.bindString(49, "N");
                                                stmt.bindString(50, "E");
                                                stmt.bindDouble(51, 0);
                                                stmt.bindDouble(52, 0);
                                                stmt.bindDouble(53, 0);
                                                stmt.bindDouble(54, 0);
                                                stmt.bindDouble(55, 0);
                                                stmt.bindDouble(56, 0);
                                            }
                                            if (campi.length >= 80) {
                                                // canale categ zona
                                                stmt.bindString(57, campi[74].trim());
                                                stmt.bindString(58, campi[75].trim());
                                                stmt.bindString(59, campi[76].trim());
                                                stmt.bindString(60, campi[77].trim());
                                                stmt.bindString(61, campi[78].trim());
                                                stmt.bindString(62, campi[79].trim());
                                            } else {
                                                stmt.bindString(57, "");
                                                stmt.bindString(58, "");
                                                stmt.bindString(59, "");
                                                stmt.bindString(60, "");
                                                stmt.bindString(61, "");
                                                stmt.bindString(62, "");
                                            }
                                            stmt.bindLong(63, 0); // nuovo
                                            stmt.bindString(64, campi[80].trim()); // validato
                                            stmt.bindString(65, campi[15].trim()); // raggr ddt
                                            //System.out.println("cliente " + campi[0].trim() + " raggrddt:" + campi[15].trim());
                                            stmt.bindString(66, campi[17].trim()); // stampa prz
                                            //System.out.println("cliente " + campi[0].trim() + " stamparz:" + campi[17].trim());
                                            stmt.bindString(67, campi[24].trim());
                                            stmt.bindString(68, campi[25].trim());
                                            stmt.bindString(69, campi[26].trim());
                                            stmt.bindString(70, campi[27].trim());
                                            stmt.bindString(71, campi[43].trim());
                                            stmt.bindString(72, campi[44].trim());
                                            stmt.bindString(73, campi[45].trim());
                                            stmt.bindString(74, campi[46].trim());
                                            stmt.bindString(75, campi[47].trim());
                                            stmt.bindString(76, campi[51].trim()); // contratto art62
                                            stmt.bindString(77, campi[52].trim());
                                            stmt.bindString(78, campi[39].trim()); // ord lotto
                                            // dati blocco
                                            if (campi.length >= 82) {
                                                stmt.bindLong(79, FunzioniJBeerApp.estraiIntero(campi[82].trim()));
                                                stmt.bindDouble(80, FunzioniJBeerApp.estraiDouble(campi[83].trim()));
                                                stmt.bindLong(81, FunzioniJBeerApp.estraiIntero(campi[84].trim()));
                                                stmt.bindString(82, campi[85].trim());
                                                stmt.bindLong(83, FunzioniJBeerApp.estraiIntero(campi[86].trim()));
                                            } else {
                                                stmt.bindLong(79, 0);
                                                stmt.bindDouble(80, 0);
                                                stmt.bindLong(81, 0);
                                                stmt.bindString(82, "0000-00-00");
                                                stmt.bindLong(83, 0);
                                            }
                                            // doc.preferito
                                            if (campi.length >= 88) {
                                                stmt.bindLong(84, FunzioniJBeerApp.estraiIntero(campi[87].trim()));
                                            } else {
                                                stmt.bindLong(84, 0);
                                            }
                                            if (campi.length >= 89) {
                                                stmt.bindDouble(85, FunzioniJBeerApp.estraiIntero(campi[88].trim()));
                                                if (!campi[89].trim().equals("")) {
                                                    stmt.bindString(86, campi[89].trim());
                                                } else {
                                                    stmt.bindString(86, "");
                                                }
                                            }
                                            if (campi.length >= 91) {
                                                if (!campi[90].trim().equals("")) {
                                                    stmt.bindString(87, campi[90].trim());
                                                } else {
                                                    stmt.bindString(87, "");
                                                }
                                            }
                                            // insegna
                                            if (campi.length >= 92) {
                                                stmt.bindString(88, campi[91].trim());
                                            } else {
                                                stmt.bindString(88, "");
                                            }
                                            // codice sdi
                                            if (campi.length >= 93) {
                                                stmt.bindString(89, campi[92].trim());
                                            } else {
                                                stmt.bindString(89, "");
                                            }

                                            stmt.execute();
                                        }
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "girivisita.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM girivisita");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO girivisita (" + "gvcod,"
                                                    + "gvprog," + "gvcodcli," + "gvcoddest,"
                                                    + "gvcodric) " + "VALUES (?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione giri visita...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "17");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione giri visita...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "17");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindLong(2, Formattazione.estraiIntero(campi[1].trim()));
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.bindString(4, campi[3].trim());
                                        if (!campi[2].trim().equals(""))
                                            stmt.bindString(5, campi[2].trim());
                                        else
                                            stmt.bindString(5, campi[3].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "scoperti.txt");
                            if (fb.exists()) {
                                try {
                                    //Env.db.execSQL("DELETE FROM incassi");
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM scoperti");
                                    SQLiteStatement stmt = Env.db.compileStatement("INSERT INTO scoperti ("
                                            + "sctipo," + "scsezdoc," + "scdatadoc," + "scnumdoc,"
                                            + "sctipoop," + "scnote," + "scclicod," + "scdestcod,"
                                            + "scscadid," + "scdatascad," + "sctiposcad,"
                                            + "scimportoscad," + "scresiduoscad," + "scrateizzata,"
                                            + "scdataconsfat) "
                                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione scoperti...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "18");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione scoperti...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "18");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        String clicod = campi[7].trim();
                                        if (clicod.equals(""))
                                            clicod = campi[6].trim();
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.bindLong(4, Formattazione.estraiIntero(campi[3].trim()));
                                        stmt.bindString(5, campi[4].trim());
                                        stmt.bindString(6, campi[5].trim());
                                        stmt.bindString(7, campi[6].trim());
                                        stmt.bindString(8, campi[7].trim());
                                        stmt.bindLong(9, Formattazione.estraiIntero(campi[8].trim()));
                                        stmt.bindString(10, campi[9].trim());
                                        stmt.bindLong(11, Formattazione.estraiIntero(campi[11].trim()));
                                        stmt.bindDouble(12, Formattazione.estraiDouble(campi[12].trim()));
                                        stmt.bindDouble(13, Formattazione.estraiDouble(campi[13].trim()));
                                        stmt.bindLong(14, Formattazione.estraiIntero(campi[15].trim()));
                                        stmt.bindString(15, campi[14].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "password.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM password");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO password (pwdoperatore,pwdpassword) VALUES (?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione password...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "19");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione password...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "19");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] token = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, token[0].trim());
                                        stmt.bindString(2, token[1].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "proprieta.txt");
                            if (fb.exists()) {
                                try {
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione proprieta...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "20");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione proprieta...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "20");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] token = linea.split("\\|");
                                        FunzioniJBeerApp.impostaProprieta(token[0].trim(), token[1].trim());
                                        linea = fin.readLine();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "blocchicau.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM blocchicau");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO blocchicau (causale,agecod,clicod,artcod,gita,blocco,destcod) VALUES (?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione blocchi causali...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "21");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione blocchi causali...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "21");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        String[] token = linea.split("\\|");
                                        stmt.clearBindings();
                                        // causale
                                        stmt.bindLong(1, Formattazione.estraiIntero(token[0].trim()));
                                        // agecod
                                        stmt.bindString(2, token[1].trim());
                                        // clicod
                                        stmt.bindString(3, token[2].trim());
                                        // artcod
                                        stmt.bindString(4, token[3].trim());
                                        // gita
                                        stmt.bindString(5, token[4].trim());
                                        // blocco
                                        stmt.bindString(6, token[5].trim());
                                        // dest
                                        stmt.bindString(7, token[6].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "ddtfatt.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM ddtfatt");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO ddtfatt (suffatt,dtfatt,numfatt,sufddt,dtddt,numddt) VALUES (?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione collegamenti ddt-fatture...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "22");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione collegamenti ddt-fatture...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "22");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        String[] token = linea.split("\\|");
                                        stmt.clearBindings();
                                        // sez.fattura
                                        stmt.bindString(1, token[0].trim());
                                        // data fattura
                                        stmt.bindString(2, token[1].trim());
                                        // numero fattura
                                        stmt.bindLong(3, Formattazione.estraiIntero(token[2].trim()));
                                        // sez.ddt
                                        stmt.bindString(4, token[3].trim());
                                        // data ddt
                                        stmt.bindString(5, token[4].trim());
                                        // numero ddt
                                        stmt.bindLong(6, Formattazione.estraiIntero(token[5].trim()));
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "preferiti.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM preferiti");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO preferiti (prefclicod,prefartcod,prefprezzo) "
                                                    + "VALUES (?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione art.preferiti...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "23");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione art.preferiti...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "23");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindDouble(3, Formattazione.estraiDouble(campi[3].trim()));
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "giacart.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM giacart");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO giacart (giacartcod,giaccolli,giacqta,giacdata,giacora) "
                                                    + "VALUES (?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione giacenze articoli...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "24");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione giacenze articoli...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "24");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindLong(2, Formattazione.estraiIntero(campi[1].trim()));
                                        stmt.bindDouble(3, Formattazione.estraiDouble(campi[2].trim()));
                                        stmt.bindString(4, campi[3].trim());
                                        stmt.bindString(5, campi[4].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "dispart.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM dispart");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO dispart (dispartcod,dispcolli,dispqta,dispdata) "
                                                    + "VALUES (?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione disponibilta' articoli...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "24");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione disponibilta' articoli...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "24");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindLong(2, Formattazione.estraiIntero(campi[1].trim()));
                                        stmt.bindDouble(3, Formattazione.estraiDouble(campi[2].trim()));
                                        stmt.bindString(4, campi[3].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "giacartlotti.txt");
                            if (fb.exists())
                            {
                                try
                                {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM giacartlotti");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO giacartlotti (giaclartcod,giacllotto,giaclcolli,giaclqta,giacldata,giaclora,giaclplato,giaclettogradi) "
                                                    + "VALUES (?,?,?,?,?,?,?,?)");

                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione giacenze articoli/lotti...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "24");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null)
                                    {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione giacenze articoli/lotti...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "24");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        try
                                        {
                                            stmt.clearBindings();
                                            stmt.bindString(1, campi[0].trim());
                                            stmt.bindString(2, campi[1].trim());
                                            stmt.bindLong(3, Formattazione.estraiIntero(campi[2].trim()));
                                            stmt.bindDouble(4, Formattazione.estraiDouble(campi[3].trim()));
                                            stmt.bindString(5, campi[4].trim());
                                            stmt.bindString(6, campi[5].trim());
                                            if (campi.length >= 7) {
                                                stmt.bindDouble(7, Formattazione.estraiDouble(campi[6].trim()));
                                            } else {
                                                stmt.bindDouble(7, 0);
                                            }
                                            if (campi.length >= 8) {
                                                stmt.bindDouble(8, Formattazione.estraiDouble(campi[7].trim()));
                                            } else {
                                                stmt.bindDouble(8, 0);
                                            }

                                            stmt.execute();
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e)
                                {
                                    e.printStackTrace();
                                } finally
                                {
                                    try
                                    {
                                        fin.close();
                                    } catch (Exception e2)
                                    {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "dispartlotti.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM dispartlotti");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO dispartlotti (dlartcod,dllotto,dlcolli,dlqta,dldata) "
                                                    + "VALUES (?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione disponibilta' lotto articoli...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "24");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione disponibilta' lotto articoli...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "24");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindLong(3, Formattazione.estraiIntero(campi[2].trim()));
                                        stmt.bindDouble(4, Formattazione.estraiDouble(campi[3].trim()));
                                        stmt.bindString(5, campi[4].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "daticessionari.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM daticessionari");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO daticessionari (dcclicod,dcartcod,dcconscod,dcartcodext) "
                                                    + "VALUES (?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione dati cessionari...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "25");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione dati cessionari...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "25");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        String[] campi = linea.split("\\|");
                                        //System.out.println("LINEA:" + campi.length);
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        if (campi.length >= 4)
                                            stmt.bindString(4, campi[3].trim());
                                        else
                                            stmt.bindString(4, "");
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "cliartiva.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM cliartiva");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO cliartiva (clicod,artcod,ivacod) "
                                                    + "VALUES (?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione dati eccezioni IVA...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "26");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione dati eccezioni IVA...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "26");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // fornitori
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "fornitori.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM fornitori");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO fornitori ("
                                                    + "forcodice,"
                                                    + "fornome,"
                                                    + "forindir,"
                                                    + "forcap,"
                                                    + "forloc,"
                                                    + "forprov,"
                                                    + "forpiva,"
                                                    + "forcodpag,"
                                                    + "forcodiva,"
                                                    + "fornumtel,"
                                                    + "fornumcell,"
                                                    + "fornumfax,"
                                                    + "forsc1,"
                                                    + "forsc2,"
                                                    + "forsc3,"
                                                    + "forcodlist,"
                                                    + "forric1,"
                                                    + "forric2"
                                                    + ") "
                                                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione fornitori...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "27");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione fornitori...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "27");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        String cod = campi[0].trim();
                                        String[] pars = new String[1];
                                        pars[0] = cod;
                                        boolean clipres = false;
                                        Cursor cc = Env.db.rawQuery("SELECT forcodice FROM fornitori WHERE forcodice = ?", pars);
                                        if (cc.moveToFirst())
                                            clipres = true;
                                        cc.close();
                                        if (!clipres) {
                                            stmt.clearBindings();
                                            stmt.bindString(1, campi[0].trim());
                                            stmt.bindString(2, campi[1].trim());
                                            stmt.bindString(3, campi[2].trim());
                                            stmt.bindString(4, campi[3].trim());
                                            stmt.bindString(5, campi[4].trim());
                                            stmt.bindString(6, campi[5].trim());
                                            stmt.bindString(7, campi[6].trim()); // piva
                                            stmt.bindString(8, campi[8].trim()); // codpag
                                            stmt.bindString(9, campi[9].trim()); // codiva
                                            stmt.bindString(10, campi[10].trim()); // num tel
                                            stmt.bindString(11, campi[11].trim()); // mobile
                                            stmt.bindString(12, campi[12].trim()); // fax
                                            stmt.bindDouble(13, Formattazione.estraiDouble(campi[14].trim())); // sc1
                                            stmt.bindDouble(14, Formattazione.estraiDouble(campi[15].trim())); // sc2
                                            stmt.bindDouble(15, Formattazione.estraiDouble(campi[16].trim())); // sc3
                                            stmt.bindString(16, campi[13].trim()); // listino
                                            if (campi.length >= 30) {
                                                stmt.bindDouble(17, Formattazione.estraiDouble(campi[29].trim())); //  % ric.1
                                                stmt.bindDouble(18, Formattazione.estraiDouble(campi[30].trim())); //  % ric.2
                                            } else {
                                                stmt.bindDouble(17, 3);
                                                stmt.bindDouble(18, 40);
                                            }
                                            stmt.execute();
                                        }
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // provenienze
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "provenienze.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM provenienze");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO provenienze ("
                                                    + "prcodice,"
                                                    + "prforcod,"
                                                    + "prdescr,"
                                                    + "prindir,"
                                                    + "prcap,"
                                                    + "prloc,"
                                                    + "prprov"
                                                    + ") "
                                                    + "VALUES (?,?,?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione provenienze...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "28");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione provenienze...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "28");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.bindString(4, campi[3].trim());
                                        stmt.bindString(5, campi[4].trim());
                                        stmt.bindString(6, campi[5].trim());
                                        stmt.bindString(7, campi[6].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // forart
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "forart.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM forart");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO forart ("
                                                    + "faforcod,"
                                                    + "faartcod,"
                                                    + "facodext,"
                                                    + "faqtaordmin,"
                                                    + "faqtaordmax,"
                                                    + "faqtaordmult"
                                                    + ") "
                                                    + "VALUES (?,?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione dati fornitori articoli...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "29");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione dati fornitori articoli...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "29");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.bindDouble(4, Formattazione.estraiDouble(campi[3].trim()));
                                        stmt.bindDouble(5, Formattazione.estraiDouble(campi[4].trim()));
                                        stmt.bindDouble(6, Formattazione.estraiDouble(campi[5].trim()));
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }
                            // forord
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "forord.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM forord");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO forord ("
                                                    + "foforcod,"
                                                    + "fopvcod,"
                                                    + "foggord1,"
                                                    + "foggord2,"
                                                    + "foggord3,"
                                                    + "foggord4,"
                                                    + "foggord5,"
                                                    + "foggord6,"
                                                    + "foggord7,"
                                                    + "foqtamin,"
                                                    + "foggcons1,"
                                                    + "foggcons2,"
                                                    + "foggcons3,"
                                                    + "foggcons4,"
                                                    + "foggcons5,"
                                                    + "foggcons6,"
                                                    + "foggcons7,"
                                                    + "foggappr1,"
                                                    + "foggappr2,"
                                                    + "foggappr3,"
                                                    + "foggappr4,"
                                                    + "foggappr5,"
                                                    + "foggappr6,"
                                                    + "foggappr7"
                                                    + ") "
                                                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione dati fornitori articoli...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "29");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione dati fornitori articoli...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "29");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindLong(3, Formattazione.estraiIntero(campi[2].trim()));
                                        stmt.bindLong(4, Formattazione.estraiIntero(campi[3].trim()));
                                        stmt.bindLong(5, Formattazione.estraiIntero(campi[4].trim()));
                                        stmt.bindLong(6, Formattazione.estraiIntero(campi[5].trim()));
                                        stmt.bindLong(7, Formattazione.estraiIntero(campi[6].trim()));
                                        stmt.bindLong(8, Formattazione.estraiIntero(campi[7].trim()));
                                        stmt.bindLong(9, Formattazione.estraiIntero(campi[8].trim()));
                                        stmt.bindDouble(10, Formattazione.estraiDouble(campi[9].trim()));
                                        stmt.bindLong(11, Formattazione.estraiIntero(campi[10].trim()));
                                        stmt.bindLong(12, Formattazione.estraiIntero(campi[11].trim()));
                                        stmt.bindLong(13, Formattazione.estraiIntero(campi[12].trim()));
                                        stmt.bindLong(14, Formattazione.estraiIntero(campi[13].trim()));
                                        stmt.bindLong(15, Formattazione.estraiIntero(campi[14].trim()));
                                        stmt.bindLong(16, Formattazione.estraiIntero(campi[15].trim()));
                                        stmt.bindLong(17, Formattazione.estraiIntero(campi[16].trim()));
                                        stmt.bindLong(18, Formattazione.estraiIntero(campi[17].trim()));
                                        stmt.bindLong(19, Formattazione.estraiIntero(campi[18].trim()));
                                        stmt.bindLong(20, Formattazione.estraiIntero(campi[19].trim()));
                                        stmt.bindLong(21, Formattazione.estraiIntero(campi[20].trim()));
                                        stmt.bindLong(22, Formattazione.estraiIntero(campi[21].trim()));
                                        stmt.bindLong(23, Formattazione.estraiIntero(campi[22].trim()));
                                        stmt.bindLong(24, Formattazione.estraiIntero(campi[23].trim()));
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }
                            // forartpv
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "forartpv.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM forartpv");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO forartpv ("
                                                    + "fapvforcod,"
                                                    + "fapvpvcod,"
                                                    + "fapvartcod"
                                                    + ") "
                                                    + "VALUES (?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione dati fornitori articoli...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "29");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione dati fornitori articoli...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "29");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // listini fornitori
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "listinifor.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM listinifor");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO listinifor (lfcod,lfartcod,lfdataval,lfprezzo1,lfprezzo2,lfprezzo3,lfprezzo4,lfprezzo5,lfprezzo6,lfprezzo7,lfprezzo8,lfprezzo9) "
                                                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione listini fornitori...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "29");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione listini fornitori...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "29");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        try {
                                            stmt.clearBindings();
                                            stmt.bindString(1, campi[0].trim());
                                            stmt.bindString(2, campi[2].trim());
                                            stmt.bindString(3, campi[3].trim());
                                            stmt.bindDouble(4, Formattazione.estraiDouble(campi[4].trim()));
                                            stmt.bindDouble(5, Formattazione.estraiDouble(campi[5].trim()));
                                            stmt.bindDouble(6, Formattazione.estraiDouble(campi[6].trim()));
                                            stmt.bindDouble(7, Formattazione.estraiDouble(campi[7].trim()));
                                            stmt.bindDouble(8, Formattazione.estraiDouble(campi[8].trim()));
                                            stmt.bindDouble(9, Formattazione.estraiDouble(campi[9].trim()));
                                            stmt.bindDouble(10, Formattazione.estraiDouble(campi[10].trim()));
                                            stmt.bindDouble(11, Formattazione.estraiDouble(campi[11].trim()));
                                            stmt.bindDouble(12, Formattazione.estraiDouble(campi[12].trim()));
                                            stmt.execute();
                                        } catch (Exception erigalist) {
                                        }
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // condizioni fornitori
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "condpersfor.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM condpersfor");
                                    SQLiteStatement stmt = Env.db.compileStatement("INSERT INTO condpersfor ("
                                            + "cpfforcod," + "cpfartcod," + "cpfdatainizio,"
                                            + "cpfdatafine," + "cpfsc1," + "cpfsc2," + "cpfsc3,"
                                            + "cpflistcod," + "cpfprezzo," + "cpfesclaltri,cpfescltestata) "
                                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione condizioni personalizzate fornitori...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "29");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione condizioni personalizzate fornitori...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "29");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.bindString(4, campi[3].trim());
                                        stmt.bindDouble(5, Formattazione.estraiDouble(campi[4].trim()));
                                        stmt.bindDouble(6, Formattazione.estraiDouble(campi[5].trim()));
                                        stmt.bindDouble(7, Formattazione.estraiDouble(campi[6].trim()));
                                        stmt.bindString(8, campi[7].trim());
                                        stmt.bindDouble(9, Formattazione.estraiDouble(campi[10].trim()));
                                        stmt.bindString(10, "N");
                                        stmt.bindString(11, "N");
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // posmag
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "posmag.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM posmag");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO posmag ("
                                                    + "pmartcod,"
                                                    + "pmdepcod,"
                                                    + "pmpos1,"
                                                    + "pmpos2,"
                                                    + "pmpos3,"
                                                    + "pmpos4,"
                                                    + "pmscortamin,"
                                                    + "pmscortamax"
                                                    + ") "
                                                    + "VALUES (?,?,?,?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione dati posizioni a magazzino...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "29");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione dati posizioni a magazzino...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "29");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.bindString(4, campi[3].trim());
                                        stmt.bindString(5, campi[4].trim());
                                        stmt.bindString(6, campi[5].trim());
                                        stmt.bindDouble(7, Formattazione.estraiDouble(campi[6].trim()));
                                        stmt.bindDouble(8, Formattazione.estraiDouble(campi[7].trim()));
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // prezzi base
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "prezzilotto.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    SQLiteStatement stmd = Env.db
                                            .compileStatement("DELETE FROM prezzibase WHERE pbartcod=? AND pblotcod=?");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO prezzibase ("
                                                    + "pbartcod,"
                                                    + "pblotcod,"
                                                    + "pbprezzo,"
                                                    + "pbprezzoacq,"
                                                    + "pbprezzoven,"
                                                    + "pbdata"
                                                    + ") "
                                                    + "VALUES (?,?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione dati prezzi base...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "29");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione dati prezzi base...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "29");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        String[] campi = linea.split("\\|");
                                        String artcod = campi[0].trim();
                                        String lotcod = campi[1].trim();
                                        stmd.clearBindings();
                                        stmd.bindString(1, artcod);
                                        stmd.bindString(2, lotcod);
                                        stmd.execute();
                                        stmt.clearBindings();
                                        stmt.bindString(1, artcod);
                                        stmt.bindString(2, lotcod);
                                        stmt.bindDouble(3, Formattazione.estraiDouble(campi[3].trim()));
                                        stmt.bindDouble(4, Formattazione.estraiDouble(campi[2].trim()));
                                        stmt.bindDouble(5, Formattazione.estraiDouble(campi[4].trim()));
                                        if (campi.length >= 6)
                                            stmt.bindString(6, campi[5].trim());
                                        else
                                            stmt.bindString(6, "0000-00-00");
                                        stmt.execute();
                                        linea = fin.readLine();

                                    }
                                    stmt.close();
                                    stmd.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // date lotti
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "datelotti.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM datelotti");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO datelotti ("
                                                    + "dlartcod,"
                                                    + "dllotcod,"
                                                    + "dldata"
                                                    + ") "
                                                    + "VALUES (?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione date carico lotti...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "29");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione date carico lotti...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "29");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        String[] campi = linea.split("\\|");
                                        String artcod = campi[0].trim();
                                        String lotcod = campi[1].trim();
                                        String data = campi[2].trim();
                                        stmt.clearBindings();
                                        stmt.bindString(1, artcod);
                                        stmt.bindString(2, lotcod);
                                        stmt.bindString(3, data);
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // messaggi da sede
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "messaggi.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM messaggi");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO messaggi ("
                                                    + "msgtesto,"
                                                    + "msgdatainizio,"
                                                    + "msgdatafine,"
                                                    + "msgora"
                                                    + ") "
                                                    + "VALUES (?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione messaggi da sede...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "29");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione messaggi da sede...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "29");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        String[] campi = linea.split("\\|");
                                        String dti = campi[0].trim();
                                        String dtf = campi[1].trim();
                                        String ora = campi[2].trim();
                                        String testo = campi[3].trim();
                                        testo = funzStringa.sostituisci(testo, '#', '\r');
                                        testo = funzStringa.sostituisci(testo, '§', '\n');
                                        if (testo.length() > 255)
                                            testo = testo.substring(0, 255);
                                        stmt.clearBindings();
                                        stmt.bindString(1, testo);
                                        stmt.bindString(2, dti);
                                        stmt.bindString(3, dtf);
                                        stmt.bindString(4, ora);
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "giacart.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM giacart");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO giacart (giacartcod,giaccolli,giacqta,giacdata,giacora) "
                                                    + "VALUES (?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione giacenze articoli da sede...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "18");
                                    h.put("max2", "18");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione giacenze articoli da sede...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "18");
                                        h.put("max2", "18");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindLong(2, Formattazione.estraiIntero(campi[1].trim()));
                                        stmt.bindDouble(3, Formattazione.estraiDouble(campi[2].trim()));
                                        stmt.bindString(4, campi[3].trim());
                                        stmt.bindString(5, campi[4].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            File fcatpdf = new File(getApplicationContext().getFilesDir() + File.separator + "docs" + File.separator);
                            if (!fcatpdf.exists())
                                fcatpdf.mkdir();

                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "logoaz.png");
                            if (fb.exists()) {
                                try {
                                    FunzioniJBeerApp.copiaFile(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "logoaz.png",
                                            getApplicationContext().getFilesDir() + File.separator + "logoaz.png");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            //catologo App
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "catalogo_app.pdf");
                            if (fb.exists()) {
                                try {
                                    FunzioniJBeerApp.copiaFile(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "catalogo_app.pdf",
                                            getApplicationContext().getFilesDir() + File.separator + "docs" + File.separator + "catalogo_app.pdf");

                                    Date lastModDate = new Date(fb.lastModified());
                                    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                                    String dataOggi = date.format(lastModDate);
                                    cursor = Env.db.rawQuery("SELECT catpdfnome FROM catalogopdf"
                                            + " WHERE catpdfnome = 'catalogo_app'", null);
                                    if (cursor.moveToNext()) {
                                        // aggiorna la data del catalogo
                                        SQLiteStatement sta = Env.db.compileStatement(
                                                "UPDATE catalogopdf SET catpdfdata = ? WHERE catpdfnome = ?");
                                        sta.clearBindings();
                                        sta.bindString(1, dataOggi);
                                        sta.bindString(2, "catalogo_app");
                                        sta.execute();
                                        sta.close();
                                    } else {
                                        SQLiteStatement sti = Env.db.compileStatement(
                                                "insert into catalogopdf (catpdfnome,catpdfdata) " +
                                                        "VALUES (?,?)");
                                        sti.clearBindings();
                                        sti.bindString(1, "catalogo_app");
                                        sti.bindString(2, dataOggi);
                                        sti.execute();
                                        sti.close();
                                    }
                                    cursor.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            //inserire nel db insert o update nel catologopdf con i campi del db
                            //dopo andare sulla parte dello share che va a leggere su db e levare la parte del test


                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "logoazddtqta.png");
                            if (fb.exists()) {
                                try {
                                    FunzioniJBeerApp.copiaFile(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "logoazddtqta.png",
                                            getApplicationContext().getFilesDir() + File.separator + "logoazddtqta.png");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // movimenti da sede
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "movimenti.txt");
                            if (fb.exists()) {
                                try {
                                    // pulizia movcaricati
                                    Data dt = new Data();
                                    dt.decrementaGiorni(30);
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM movcaricati WHERE mcardata < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                                    HashMap<String, Record> hmcar = new HashMap();
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO movcaricosede (mcsez,mcdata,mcnum,mcartcod,mcqta,mclotto,mcprezzo,mcnetto) VALUES (?,?,?,?,?,?,?,?)");
                                    SQLiteStatement stmts = Env.db
                                            .compileStatement("INSERT INTO movcaricosedestor (mcsez,mcdata,mcnum,mcartcod,mcqta,mclotto,mcprezzo,mcnetto) VALUES (?,?,?,?,?,?,?,?)");
                                    SQLiteStatement stmt2 = Env.db
                                            .compileStatement("INSERT INTO prezzibase (pbartcod,pblotcod,pbprezzo,pbdata) VALUES (?,?,?,?)");
                                    SQLiteStatement stmt3 = Env.db
                                            .compileStatement("DELETE FROM prezzibase WHERE pbartcod=? AND pblotcod=?");
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione movimenti...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "30");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    int nriga = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione movimenti...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "30");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);

                                        if (linea.length() > 10 && linea.startsWith("MG")) {
                                            Log.v("JBEERAPP", linea);
                                            String sez = linea.substring(2, 12).trim();
                                            String data = linea.substring(12, 22).trim();
                                            int numdoc = Formattazione.estraiIntero(linea.substring(22, 30).trim());
                                            Log.v("JBEERAPP", "sez:" + sez + " data:" + data + " num:" + numdoc);
                                            boolean rigaok = true;
                                            if (!sez.toLowerCase().startsWith("nxe")) {
                                                rigaok = false;
                                            }
                                            Cursor c = Env.db.rawQuery(
                                                    "SELECT mcarnum FROM movcaricati WHERE mcarsez = '" + sez + "' AND mcardata = '" + data + "' AND mcarnum = " + numdoc, null);
                                            if (c.moveToFirst()) {
                                                rigaok = false;
                                            }
                                            c.close();
                                            Log.v("JBEERAPP", "rigaok:" + rigaok);
                                            if (rigaok) {
                                                String artcod = linea.substring(78, 98).trim();
                                                String lotto = linea.substring(108, 128).trim();
                                                if (!artcod.equals("")) {
                                                    String descr = linea.substring(138, 188).trim();
                                                    double qta = Formattazione.estraiDouble(linea.substring(242, 257).trim());
                                                    double nettoriga = Formattazione.estraiDouble(linea.substring(487, 502).trim());
                                                    double prz = OpValute.arrotondaMat(nettoriga / qta, 3);
                                                    Log.v("JBEERAPP", "artcod:" + artcod + " lotto:" + lotto + " qta:" + qta + " prz:" + prz);
                                                    if (FunzioniJBeerApp.articoloEsistente(artcod)) {
                                                        // aggiorna giac.articolo
                                                        double giac = FunzioniJBeerApp.leggiGiacenzaArticolo(artcod);
                                                        FunzioniJBeerApp.impostaGiacenzaArticolo(artcod, giac + qta);
                                                        // gestione lotto
                                                        if (!lotto.equals("") && !artcod.equals("")) {
                                                            double ng = FunzioniJBeerApp.aggiornaGiacenzaLotto(lotto, artcod, qta);
                                                        }
                                                    } else {
                                                        Log.v("JBEERAPP", "artcod:" + artcod + " inesistente");
                                                    }
                                                    stmt.clearBindings();
                                                    stmt.bindString(1, sez);
                                                    stmt.bindString(2, data);
                                                    stmt.bindLong(3, numdoc);
                                                    stmt.bindString(4, artcod);
                                                    stmt.bindDouble(5, qta);
                                                    stmt.bindString(6, lotto);
                                                    stmt.bindDouble(7, prz);
                                                    stmt.bindDouble(8, nettoriga);
                                                    stmt.execute();
                                                    stmts.clearBindings();
                                                    stmts.bindString(1, sez);
                                                    stmts.bindString(2, data);
                                                    stmts.bindLong(3, numdoc);
                                                    stmts.bindString(4, artcod);
                                                    stmts.bindDouble(5, qta);
                                                    stmts.bindString(6, lotto);
                                                    stmts.bindDouble(7, prz);
                                                    stmts.bindDouble(8, nettoriga);
                                                    stmts.execute();

                                                    stmt3.clearBindings();
                                                    stmt3.bindString(1, artcod);
                                                    stmt3.bindString(2, lotto);
                                                    stmt3.execute();

                                                    stmt2.clearBindings();
                                                    stmt2.bindString(1, artcod);
                                                    stmt2.bindString(2, lotto);
                                                    stmt2.bindDouble(3, prz);
                                                    stmt2.bindString(4, data);
                                                    stmt2.execute();
                                                }
                                            } else {
                                                Log.v("JBEERAPP", "riga già caricata in precedenza");
                                            }
                                            Record rec = new Record();
                                            rec.insStringa("sez", sez);
                                            rec.insStringa("data", data);
                                            rec.insIntero("num", numdoc);
                                            hmcar.put(("" + (nriga++)), rec);
                                        }
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    stmt2.close();
                                    stmt3.close();

                                    Iterator it = hmcar.keySet().iterator();
                                    while (it.hasNext()) {
                                        String k = (String) it.next();
                                        Record rec = hmcar.get(k);
                                        String ss = rec.leggiStringa("sez");
                                        String dd = rec.leggiStringa("data");
                                        int nn = rec.leggiIntero("num");
                                        Env.db.execSQL("DELETE FROM movcaricati WHERE mcarsez = '" + ss + "' AND mcardata = '" + dd + "' AND mcarnum = " + nn);
                                        stmt2 = Env.db
                                                .compileStatement("INSERT INTO movcaricati (mcarsez,mcardata,mcarnum) VALUES (?,?,?)");
                                        stmt2.clearBindings();
                                        stmt2.bindString(1, ss);
                                        stmt2.bindString(2, dd);
                                        stmt2.bindLong(3, nn);
                                        stmt2.execute();
                                        stmt2.close();
                                    }
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                                FunzioniJBeerApp.impostaProprieta("MOVINTEGR", "S");
                            }

                            // movimenti da altro terminale
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "movtrasfterm.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    fin = new RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione trasferimenti da altro term...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "30");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    int nriga = 0;
                                    String termorig = "";
                                    ArrayList<Record> vrt = new ArrayList();
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione trasferimenti da altro term...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "30");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        if (linea.length() > 10 && linea.startsWith("MV")) {
                                            termorig = linea.substring(160, 170).trim();
                                        }
                                        if (linea.length() > 10 && linea.startsWith("MG")) {
                                            Log.v("JBEERAPP", linea);
                                            String artcod = linea.substring(78, 98).trim();
                                            String lotto = linea.substring(108, 128).trim();
                                            if (!artcod.equals("")) {
                                                String[] parsa = new String[1];
                                                parsa[0] = artcod;
                                                Cursor c = Env.db.rawQuery("SELECT artcodiva,ivaaliq FROM articoli LEFT JOIN codiva ON articoli.artcodiva = codiva.ivacod WHERE artcod = ?", parsa);
                                                if (c.moveToFirst()) {
                                                    String descr = linea.substring(138, 188).trim();
                                                    double qta = Formattazione.estraiDouble(linea.substring(242, 257).trim());
                                                    double nettoriga = Formattazione.estraiDouble(linea.substring(487, 502).trim());
                                                    double prz = OpValute.arrotondaMat(nettoriga / qta, 3);
                                                    Log.v("JBEERAPP", "artcod:" + artcod + " lotto:" + lotto + " qta:" + qta + " prz:" + prz);
                                                    // aggiorna giac.articolo
                                                    double giac = FunzioniJBeerApp.leggiGiacenzaArticolo(artcod);
                                                    FunzioniJBeerApp.impostaGiacenzaArticolo(artcod, giac + qta);
                                                    // gestione lotto
                                                    if (!lotto.equals("") && !artcod.equals("")) {
                                                        double ng = FunzioniJBeerApp.aggiornaGiacenzaLotto(lotto, artcod, qta);
                                                    }
                                                    Record rx = new Record();
                                                    rx.insStringa("artcod", artcod);
                                                    rx.insStringa("lotto", lotto);
                                                    rx.insDouble("qta", qta);
                                                    rx.insStringa("codiva", c.getString(0));
                                                    rx.insDouble("aliq", c.getDouble(1));
                                                    rx.insDouble("prezzo", prz);
                                                    rx.insDouble("netto", nettoriga);
                                                    vrt.add(rx);
                                                }
                                                c.close();
                                            }
                                        }
                                        linea = fin.readLine();
                                    }
                                    // salva doc.trasf da altro automezzo
                                    int numt = Env.numtrasfdamezzo;
                                    String sezt = Env.seztrasfdamezzo;
                                    String doct = Env.doctrasfdamezzo;
                                    Env.numtrasfdamezzo++;
                                    String datadoc = (new Data()).formatta(Data.AAAA_MM_GG, "-");
                                    SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
                                    String oradoc = dh.format(new Date());
                                    SQLiteStatement stmt = Env.db.compileStatement(
                                            "insert into movimenti (movtipo,movdoc,movsez,movdata,movnum,movdocora," +
                                                    "movtrasf,movsospeso,movclicod,movdestcod,movspeseinc," +
                                                    "movspesebolli,movspesetrasp,movspesecauz,movcodlist," +
                                                    "movnprz,movbancacod,movagenzia,movcab,movpagcod,movaspbeni," +
                                                    "movnumcolli,movpeso,movnote1,movnote2,movnote3,movnote4,movnote5,movnote6," +
                                                    "movtotlordomerce,movtotsconticli,movtotscontiart,movomaggi,movtotmerce," +
                                                    "movtotnetti,movtotspeseinc,movtotspesetrasp,movtotspesebol,movtotimp," +
                                                    "movtotiva,movtotale,movaddeb,movaccred,movnettoapag,movacconto,movnrif,movoperatore,movevaso,movart62,movkey) " +
                                                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                    stmt.clearBindings();
                                    stmt.bindLong(1, 8);
                                    stmt.bindString(2, doct);
                                    stmt.bindString(3, sezt);
                                    stmt.bindString(4, datadoc);
                                    stmt.bindLong(5, numt);
                                    stmt.bindString(6, oradoc);
                                    stmt.bindString(7, "S");
                                    stmt.bindString(8, "N");
                                    stmt.bindString(9, "");
                                    stmt.bindString(10, "");
                                    stmt.bindString(11, "N");
                                    stmt.bindString(12, "N");
                                    stmt.bindString(13, "N");
                                    stmt.bindString(14, "N");
                                    stmt.bindString(15, "");
                                    stmt.bindLong(16, 1);
                                    stmt.bindString(17, "");
                                    stmt.bindString(18, "");
                                    stmt.bindString(19, "");
                                    stmt.bindString(20, "");
                                    stmt.bindString(21, "A Vista");
                                    stmt.bindLong(22, 0);
                                    stmt.bindLong(23, 0);
                                    stmt.bindString(24, termorig);
                                    stmt.bindString(25, "");
                                    stmt.bindString(26, "");
                                    stmt.bindString(27, "");
                                    stmt.bindString(28, "");
                                    stmt.bindString(29, "");
                                    stmt.bindDouble(30, 0);
                                    stmt.bindDouble(31, 0);
                                    stmt.bindDouble(32, 0);
                                    stmt.bindDouble(33, 0);
                                    stmt.bindDouble(34, 0);
                                    stmt.bindDouble(35, 0);
                                    stmt.bindDouble(36, 0);
                                    stmt.bindDouble(37, 0);
                                    stmt.bindDouble(38, 0);
                                    stmt.bindDouble(39, 0);
                                    stmt.bindDouble(40, 0);
                                    stmt.bindDouble(41, 0);
                                    stmt.bindDouble(42, 0);
                                    stmt.bindDouble(43, 0);
                                    stmt.bindDouble(44, 0);
                                    stmt.bindDouble(45, 0);
                                    stmt.bindString(46, termorig);
                                    stmt.bindString(47, "");
                                    stmt.bindLong(48, 0);
                                    stmt.bindLong(49, 0);
                                    stmt.bindString(50, "");
                                    stmt.execute();
                                    stmt.close();
                                    // righe articoli
                                    SQLiteStatement str = Env.db.compileStatement(
                                            "insert into righemov (rmmovtipo,rmmovdoc,rmmovsez,rmmovdata,rmmovnum,rmriga," +
                                                    "rmtiporiga,rmartcod,rmlotto,rmcaumag,rmcolli,rmpezzi,rmcontenuto,rmqta,rmprz," +
                                                    "rmartsc1,rmartsc2,rmscvend,rmcodiva,rmaliq,rmclisc1,rmclisc2,rmclisc3," +
                                                    "rmlordo,rmnetto,rmnettoivato) " +
                                                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                    for (int i = 0; i < vrt.size(); i++) {
                                        Record riga = vrt.get(i);
                                        str.clearBindings();
                                        str.bindLong(1, 8);
                                        str.bindString(2, doct);
                                        str.bindString(3, sezt);
                                        str.bindString(4, datadoc);
                                        str.bindLong(5, numt);
                                        str.bindLong(6, i);
                                        str.bindString(7, "A");
                                        str.bindString(8, riga.leggiStringa("artcod"));
                                        str.bindString(9, riga.leggiStringa("lotto"));
                                        str.bindString(10, "TE");
                                        str.bindLong(11, 0);
                                        str.bindDouble(12, 0);
                                        str.bindDouble(13, 0);
                                        str.bindDouble(14, riga.leggiDouble("qta"));
                                        str.bindDouble(15, riga.leggiDouble("prezzo"));
                                        str.bindDouble(16, 0);
                                        str.bindDouble(17, 0);
                                        str.bindDouble(18, 0);
                                        str.bindString(19, riga.leggiStringa("codiva"));
                                        str.bindDouble(20, riga.leggiDouble("aliq"));
                                        str.bindDouble(21, 0);
                                        str.bindDouble(22, 0);
                                        str.bindDouble(23, 0);
                                        str.bindDouble(24, riga.leggiDouble("netto"));
                                        str.bindDouble(25, riga.leggiDouble("netto"));
                                        str.bindDouble(26, riga.leggiDouble("netto"));
                                        str.execute();
                                    }
                                    str.close();

                                    // movimenti lotti
                                    SQLiteStatement stlm = Env.db.compileStatement(
                                            "insert into lottimov (lmmovtipo,lmmovdoc,lmmovsez,lmmovdata,lmmovnum,lmriga," +
                                                    "lmlotcod,lmlotartcod,lmtipomov,lmpezzi,lmqta) " +
                                                    "VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                                    int nr = 0;
                                    for (int i = 0; i < vrt.size(); i++) {
                                        Record riga = vrt.get(i);
                                        String lc = riga.leggiStringa("lotto");
                                        String art = riga.leggiStringa("artcod");
                                        if (!lc.equals("") && !art.equals("")) {
                                            // ins.riga mov.lotto
                                            stlm.clearBindings();
                                            stlm.bindLong(1, 8);
                                            stlm.bindString(2, doct);
                                            stlm.bindString(3, sezt);
                                            stlm.bindString(4, datadoc);
                                            stlm.bindLong(5, numt);
                                            stlm.bindLong(6, nr);
                                            stlm.bindString(7, lc);
                                            stlm.bindString(8, art);
                                            stlm.bindString(9, "C");
                                            stlm.bindLong(10, 0);
                                            stlm.bindDouble(11, riga.leggiDouble("qta"));
                                            stlm.execute();
                                            nr++;
                                        }
                                    }
                                    stlm.close();
                                    // aggiorna sezionali su db
                                    SQLiteStatement stsez = Env.db.compileStatement(
                                            "UPDATE datiterm SET numtrasfdamezzo=?");
                                    stsez.clearBindings();
                                    stsez.bindLong(1, Env.numtrasfdamezzo);
                                    stsez.execute();
                                    stsez.close();
                                    // salva su log
                                    FunzioniJBeerApp.salvaLog("Trasferimento da altro automezzo " + termorig);
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                                //FunzioniJazzTv.impostaProprieta("MOVINTEGR", "S");
                            }

                            // immagini articoli dal file di carico
                            File fbizip = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "immagini.zip");
                            File fbi = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "artimg.txt");
                            if (fbizip.exists() && fbi.exists()) {
                                try {
                                    h = new HashMap();
                                    h.put("label", "Aggiornamento immagini articoli...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "0");
                                    h.put("val2", "30");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "S");
                                    this.publishProgress(h);
                                    // unzip immagini
                                    FileInputStream fisimg = new FileInputStream(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "immagini.zip");
                                    ZipInputStream zisimg = new ZipInputStream(new BufferedInputStream(fisimg));
                                    ZipEntry entryimg;
                                    while ((entryimg = zisimg.getNextEntry()) != null) {
                                        int count;
                                        int BUFFER = 2048;
                                        byte[] data = new byte[BUFFER];
                                        FileOutputStream fosunzip = openFileOutput(entryimg.getName(), MODE_PRIVATE);
                                        BufferedOutputStream dest = new BufferedOutputStream(fosunzip, BUFFER);
                                        while ((count = zisimg.read(data, 0, BUFFER)) != -1) {
                                            dest.write(data, 0, count);
                                        }
                                        dest.flush();
                                        dest.close();
                                    }
                                    zisimg.close();
                                    fisimg.close();

                                    // aggiornamento collegamenti articolo immagine
                                    SQLiteStatement stdimg = Env.db
                                            .compileStatement("DELETE FROM imgart WHERE iartcod=?");
                                    SQLiteStatement stimg = Env.db
                                            .compileStatement("INSERT INTO imgart (iartcod,ifileimg) VALUES (?,?)");
                                    RandomAccessFile finimg = new java.io.RandomAccessFile(fbi, "r");
                                    int prog = 0;
                                    String linea = finimg.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stdimg.clearBindings();
                                        stdimg.bindString(1, campi[0].trim());
                                        stdimg.execute();
                                        stimg.clearBindings();
                                        stimg.bindString(1, campi[0].trim());
                                        stimg.bindString(2, campi[1].trim());
                                        stimg.execute();
                                        linea = finimg.readLine();
                                    }
                                    stimg.close();
                                    stdimg.close();
                                    finimg.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            // eventi venduto
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "eventi_venduto.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM eventi_venduto");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO eventi_venduto (evvevento,evvartcod,evvartdescr,evvartum,evvqta,evvvalore) "
                                                    + "VALUES (?,?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione venduto storico eventi...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "30");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione venduto storico eventi...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "30");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.bindString(4, campi[3].trim());
                                        stmt.bindDouble(5, Formattazione.estraiDouble(campi[4].trim()));
                                        stmt.bindDouble(6, Formattazione.estraiDouble(campi[5].trim()));
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // eventi_merceinviata
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "eventi_merceinviata.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM eventi_merceinviata");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO eventi_merceinviata (evievento,eviartcod,eviartdescr,eviartum,eviqta) "
                                                    + "VALUES (?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione merce inviata storico eventi...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "30");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione merce inviata storico eventi...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "30");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.bindString(4, campi[3].trim());
                                        stmt.bindDouble(5, Formattazione.estraiDouble(campi[4].trim()));
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // eventi_mercetornata
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "eventi_mercetornata.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM eventi_mercetornata");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO eventi_mercetornata (evtevento,evtartcod,evtartdescr,evtartum,evtqta) "
                                                    + "VALUES (?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione merce tornata storico eventi...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "30");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione merce tornata storico eventi...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "30");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.bindString(4, campi[3].trim());
                                        stmt.bindDouble(5, Formattazione.estraiDouble(campi[4].trim()));
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            // eventi_contatti
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "eventi_contatti.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM eventi_contatti");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO eventi_contatti (evcevento,evcnome,evccognome,evcemail,evctel,evcmobile,"
                                                    + "evcaziendaragsoc,evcaziendaindir,evcaziendaloc,evcaziendaprov,evcaziendaemail,evcaziendasitoweb,"
                                                    + "evccod,evcnote,evctrasfcliente) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione contatti storico eventi...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "30");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione contatti storico eventi...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "30");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.bindString(4, campi[3].trim());
                                        stmt.bindString(5, campi[4].trim());
                                        stmt.bindString(6, campi[5].trim());
                                        stmt.bindString(7, campi[6].trim());
                                        stmt.bindString(8, campi[7].trim());
                                        stmt.bindString(9, campi[8].trim());
                                        stmt.bindString(10, campi[9].trim());
                                        stmt.bindString(11, campi[10].trim());
                                        stmt.bindString(12, campi[11].trim());
                                        stmt.bindString(13, campi[12].trim());
                                        stmt.bindString(14, campi[13].trim());
                                        stmt.bindString(15, campi[14].trim());
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }
                            // Statistiche di Vendita
                            fb = new File(getApplicationContext().getCacheDir() + File.separator + "carico" + File.separator + "statistiche.txt");
                            if (fb.exists()) {
                                try {
                                    Env.db.beginTransaction();
                                    Env.db.execSQL("DELETE FROM statistiche");
                                    SQLiteStatement stmt = Env.db
                                            .compileStatement("INSERT INTO statistiche (stclicod,startcod,startdescr,stcolli,stqta,stvalore,stprezzom,stlitritot,sttotld2,sttotld3) " +
                                                    "VALUES (?,?,?,?,?,?,?,?,?,?)");
                                    fin = new java.io.RandomAccessFile(fb, "r");
                                    h = new HashMap();
                                    h.put("label", "Importazione statistiche di vendita...");
                                    h.put("label2", "");
                                    h.put("val", "0");
                                    h.put("max", "" + fin.length());
                                    h.put("val2", "30");
                                    h.put("max2", "30");
                                    h.put("indeterminate", "N");
                                    this.publishProgress(h);
                                    int prog = 0;
                                    String linea = fin.readLine();
                                    while (linea != null) {
                                        prog += linea.length();
                                        h = new HashMap();
                                        h.put("label", "Importazione statistiche di vendita...");
                                        h.put("label2", "");
                                        h.put("val", "" + prog);
                                        h.put("max", "" + fin.length());
                                        h.put("val2", "30");
                                        h.put("max2", "30");
                                        h.put("indeterminate", "N");
                                        this.publishProgress(h);
                                        //System.out.println("LINEA:" + linea);
                                        String[] campi = linea.split("\\|");
                                        stmt.clearBindings();
                                        stmt.bindString(1, campi[0].trim());
                                        stmt.bindString(2, campi[1].trim());
                                        stmt.bindString(3, campi[2].trim());
                                        stmt.bindLong(4, Formattazione.estraiIntero(campi[3].trim()));
                                        stmt.bindDouble(5, Formattazione.estraiDouble(campi[4].trim()));
                                        stmt.bindDouble(6, Formattazione.estraiDouble(campi[5].trim()));
                                        stmt.bindDouble(7, Formattazione.estraiDouble(campi[6].trim()));
                                        stmt.bindDouble(8, Formattazione.estraiDouble(campi[7].trim()));
                                        stmt.bindDouble(9, Formattazione.estraiDouble(campi[8].trim()));
                                        stmt.bindDouble(10, Formattazione.estraiDouble(campi[9].trim()));
                                        stmt.execute();
                                        linea = fin.readLine();
                                    }
                                    stmt.close();
                                    Env.db.setTransactionSuccessful();
                                    Env.db.endTransaction();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        fin.close();
                                    } catch (Exception e2) {
                                    }
                                }
                            }

                            FunzioniJBeerApp.salvaLog("Elaborazione carico " + nomefile + " OK");
                        } catch (Exception eelabf) {
                            eelabf.printStackTrace();
                            FunzioniJBeerApp.salvaLog("Errore su elaborazione carico " + nomefile + ":" + eelabf.getLocalizedMessage());
                        }
                    } // file loop elab.files

                    FunzioniJBeerApp.caricaParametri();

                    if (datitermpres)
                        FunzioniJBeerApp.impostaProprieta("stato", "INIZIOSESSIONE");

                    // cancellazione movimenti + vecchi di 120 gg
                    Data dt = new Data();
                    dt.decrementaGiorni(120);
                    Env.db.execSQL("DELETE FROM movimenti WHERE movdata < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM righemov WHERE rmmovdata < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM cauzmov WHERE cmmovdata < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM ivamov WHERE immovdata < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM lottimov WHERE lmmovdata < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM scadmov WHERE smmovdata < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM storico_ddtcarico WHERE stdata < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM storico_incassi WHERE siscdatainc < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM storico_fineg WHERE fgdata < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM storico_fineg_ddtann WHERE fg1data < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM storico_fineg_ddtom WHERE fg2data < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM storico_fineg_incassigiorno WHERE fg3data < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM storico_fineg_incassisospesi WHERE fg4data < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM storico_fineg_incassiftnc WHERE fg5data < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM storico_fineg_sospesidainc WHERE fg6data < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");
                    Env.db.execSQL("DELETE FROM storico_fineg_costivendite WHERE fg7data < '" + dt.formatta(Data.AAAA_MM_GG, "-") + "'");


                    //aggiorna documenti da non considerare in fase di calcolo giacenza
                    Env.db.execSQL("UPDATE movimenti SET movcalcgiac = 'N' WHERE movtrasf = 'S'");

                    // svuota directory carico
                    // svuota cartella cache carico
                    fcardir = new File(getApplicationContext().getCacheDir() + File.separator + "carico");
                    File[] lfs = fcardir.listFiles();
                    for (int i = 0; i < lfs.length; i++) {
                        lfs[i].delete();
                    }
                    // svuota cartella cache filescarico
                    fcardir = new File(getApplicationContext().getCacheDir() + File.separator + "filescarico");
                    lfs = fcardir.listFiles();
                    for (int i = 0; i < lfs.length; i++) {
                        lfs[i].delete();
                    }

                    // cancella lotti senza giacenza
                    Env.db.execSQL("delete from lotti where lotgiacenza = 0");

                    // carica tabelle
                    FunzioniJBeerApp.caricaCodIva();
                    FunzioniJBeerApp.caricaCodPag();
                    FunzioniJBeerApp.caricaBlocchiCau();

                    // pulizia log
                    FunzioniJBeerApp.puliziaLog();

                    Env.db.execSQL("DELETE FROM varanag WHERE vatrasf = 'S'");

                    // inizializzazione contatore per backup periodico
                    FunzioniJBeerApp.impostaProprieta("contatore_backupperiodico", "0");

                    h = new HashMap();
                    h.put("label", "Aggiornamento dati terminato");
                    h.put("label2", "");
                    h.put("val", "100");
                    h.put("max", "100");
                    h.put("val2", "30");
                    h.put("max2", "30");
                    h.put("indeterminate", "N");
                    this.publishProgress(h);
                    aggincorso = false;
                    Env.agg_dati_incorso = false;
                    aggterminato = true;
                    FunzioniJBeerApp.impostaProprieta("ultimaric_data", (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                    SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                    FunzioniJBeerApp.impostaProprieta("ultimaric_ora", df.format(new Date()));
                }
                FunzioniJBeerApp.impostaProprieta("ULTIMADATACHECKRIC", (new Data()).formatta(Data.AAAA_MM_GG, "-"));
//                {
//                    h = new HashMap();
//                    h.put("label", "Errore download file");
//                    h.put("label2", "");
//                    h.put("val", "0");
//                    h.put("max", "0");
//                    h.put("val2", "0");
//                    h.put("max2", "0");
//                    h.put("indeterminate", "N");
//                    this.publishProgress(h);
//                    aggincorso = false;
//                    Env.agg_dati_incorso = false;
//                }
            }

            return "";
            //onBackPressed();
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(String value) {
        }

        @Override
        protected void onProgressUpdate(HashMap... values) {
            if (values[0].get("label") != null)
                labelinfo.setText((String) values[0].get("label"));
            if (values[0].get("label2") != null)
                labelinfo2.setText((String) values[0].get("label2"));
            if (values[0].get("max") != null)
                progress.setMax(Formattazione.estraiIntero((String) values[0].get("max")));
            if (values[0].get("val") != null)
                progress.setProgress(Formattazione.estraiIntero((String) values[0].get("val")));
            if (values[0].get("max2") != null)
                progress2.setMax(Formattazione.estraiIntero((String) values[0].get("max2")));
            if (values[0].get("val2") != null)
                progress2.setProgress(Formattazione.estraiIntero((String) values[0].get("val2")));
            if (values[0].get("indeterminate") != null)
                progress.setIndeterminate(values[0].get("indeterminate").equals("S"));
            if (values[0].get("error") != null) {
                String err = (String) values[0].get("error");
                if (!err.equals("")) {
                    labelinfo.setText(err);
                    icona.setImageResource(R.drawable.ic_error_black_48dp);
                }
            }
        }
    }

    private void ensureZipPathSafety(final File outputFile, final String destDirectory) throws Exception {
        String destDirCanonicalPath = (new File(destDirectory)).getCanonicalPath();
        String outputFilecanonicalPath = outputFile.getCanonicalPath();
        if (!outputFilecanonicalPath.startsWith(destDirCanonicalPath)) {
            throw new Exception(String.format("Found Zip Path Traversal Vulnerability with %s", canonicalPath));
        }
    }
}


