package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaRicArtAdapter;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Record;


public class FormRicercaArticoli extends AppCompatActivity {

    private EditText riccod;
    private EditText ricdescr;
    private EditText riccodbarre;
    private ListView listaart;
    private Spinner ricsottoclasse;
    private Spinner ricmarca;
    private Spinner ricstato;
    private ImageButton bvoice;
    private ArrayList<Record> vart;
    private ListaRicArtAdapter lraadapter;
    private SpinnerAdapter lscadapter;
    private SpinnerAdapter lmaadapter;
    private SpinnerAdapter lstadapter;
    private ArrayList<Record> vsc;
    private ArrayList<String> lsc;
    private ArrayList<Record> vma;
    private ArrayList<String> lma;
    private ArrayList<String> lst;
    private String ordric;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_ricerca_articoli);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        riccod = findViewById(R.id.ricart_riccod);
        ricdescr = findViewById(R.id.ricart_ricdescr);
        riccodbarre = findViewById(R.id.ricart_riccodbarre);
        listaart = findViewById(R.id.ricart_listaart);
        ricsottoclasse = findViewById(R.id.ricart_ricsottoclasse);
        ricmarca = findViewById(R.id.ricart_ricmarca);
        ricstato = findViewById(R.id.ricart_ricstato);
        bvoice = findViewById(R.id.ricart_buttonVoice);

        if (getIntent().getExtras() != null && getIntent().getExtras().getString("CODICE") != null) {
            riccod.setText(getIntent().getExtras().getString("CODICE"));
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().getString("DESCRIZIONE") != null) {
            ricdescr.setText(getIntent().getExtras().getString("DESCRIZIONE"));
        }

        bvoice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Descrizione articolo...");
                startActivityForResult(intent, 1);
            }
        });

        ordric = "per codice";

        // carica sottoclassi nella combo
        lsc = new ArrayList();
        lsc.add("TUTTI");
        vsc = new ArrayList();
        Record h1 = new Record();
        h1.insStringa("clcod", "TUTTI");
        h1.insStringa("sccod", "");
        h1.insStringa("scdescr", "");
        vsc.add(h1);
        Cursor cursor = Env.db.rawQuery(
                "SELECT clcod,sccod,scdescr FROM sottoclassi ORDER BY scdescr", null);
        while (cursor.moveToNext()) {
            lsc.add(cursor.getString(2));
            Record h = new Record();
            h.insStringa("clcod", cursor.getString(0));
            h.insStringa("sccod", cursor.getString(1));
            h.insStringa("scdescr", cursor.getString(2));
            vsc.add(h);
        }
        cursor.close();
        lscadapter = new SpinnerAdapter(FormRicercaArticoli.this.getApplicationContext(), R.layout.spinner_item, lsc);
        ricsottoclasse.setAdapter(lscadapter);

        // carica marche nella combo
        lma = new ArrayList();
        lma.add("TUTTI");
        vma = new ArrayList();
        h1 = new Record();
        h1.insStringa("marcacod", "TUTTI");
        h1.insStringa("marcadescr", "");
        vma.add(h1);
        cursor = Env.db.rawQuery(
                "SELECT marcacod,marcadescr FROM marche ORDER BY marcadescr", null);
        while (cursor.moveToNext()) {
            lma.add(cursor.getString(1));
            Record h = new Record();
            h.insStringa("marcacod", cursor.getString(0));
            h.insStringa("marcadescr", cursor.getString(1));
            vma.add(h);
        }
        cursor.close();
        lmaadapter = new SpinnerAdapter(FormRicercaArticoli.this.getApplicationContext(), R.layout.spinner_item, lma);
        ricmarca.setAdapter(lmaadapter);

        // carica stato
        lst = new ArrayList();
        lst.add("...");
        lst.add("NUOVI");
        lst.add("IN OFFERTA");
        lst.add("PREZZO AUMENTATO");
        lst.add("PREZZO DIMINUITO");
        lstadapter = new SpinnerAdapter(FormRicercaArticoli.this.getApplicationContext(), R.layout.spinner_item, lst);
        ricstato.setAdapter(lstadapter);

        ricsottoclasse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                aggiornaLista();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        ricmarca.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                aggiornaLista();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        ricstato.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                aggiornaLista();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

/*		riccod.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				aggiornaLista();
				return false;
			}
		});*/

/*        riccodbarre.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                aggiornaLista();
                return false;
            }
        });*/

/*
		ricdescr.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				aggiornaLista();
				return false;
			}
		});
*/

        riccod.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                aggiornaLista();
            }
        });

        riccodbarre.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                aggiornaLista();
            }
        });

        ricdescr.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                aggiornaLista();
            }
        });

        listaart.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                String tmp = vart.get(arg2).leggiStringa("codice");
                Uri codselez = Uri.parse("content://ricart/" + tmp);
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                setResult(RESULT_OK, result);
                finish();
            }
        });

        vart = new ArrayList<Record>();
        lraadapter = new ListaRicArtAdapter(this.getApplicationContext(), vart);
        listaart.setAdapter(lraadapter);

        aggiornaLista();
    }

    public void impostaFiltri(String descr, String marcacod, String clcod, String sccod) {
        if (!descr.equals(""))
            ricdescr.setText(descr);
        if (!marcacod.equals("")) {
            for (int i = 0; i < lma.size(); i++) {
                if (marcacod.equals(lma.get(i))) {
                    ricmarca.setSelection(i);
                    break;
                }
            }
        }
        if (!clcod.equals("") && !sccod.equals("")) {
            for (int i = 0; i < vsc.size(); i++) {
                Record elem = vsc.get(i);
                if (clcod.equals(elem.leggiStringa("clcod")) && sccod.equals(elem.leggiStringa("sccod"))) {
                    ricsottoclasse.setSelection(i);
                    break;
                }
            }
        }

        aggiornaLista();
    }

    private void aggiornaLista() {
        vart.clear();
        String c = riccod.getText().toString().trim();
        String d = ricdescr.getText().toString().trim();
        String cb = riccodbarre.getText().toString().trim();
        c.replace("'", "''");
        d.replace("'", "''");
        cb.replace("'", "''");
        String qry = "SELECT artcod,artdescr,artum,artnotificaterm FROM articoli WHERE artcod <> '' ";
        if (!c.equals(""))
            qry += " AND artcod LIKE \"%" + c + "%\"";
        if (!d.equals(""))
            qry += " AND artdescr LIKE \"%" + d + "%\"";
        if (!cb.equals(""))
            qry += " AND artcodbarre LIKE \"%" + cb + "%\"";
        if (ricsottoclasse.getSelectedItemPosition() > 0) {
            Record h = vsc.get(ricsottoclasse.getSelectedItemPosition());
            if (!h.leggiStringa("sccod").equals(""))
                qry += " AND artclassecod = '" + h.leggiStringa("clcod") + "' and artsottoclassecod = '" + h.leggiStringa("sccod") + "'";
            else
                qry += " AND artclassecod = '" + h.leggiStringa("clcod") + "'";
        }
        if (ricmarca.getSelectedItemPosition() > 0) {
            Record h = vma.get(ricmarca.getSelectedItemPosition());
            qry += " AND artmarcacod = '" + h.leggiStringa("marcacod") + "'";
        }
        if (ricstato.getSelectedItemPosition() > 0) {
            qry += " AND artnotificaterm = " + ricstato.getSelectedItemPosition();
        }
        qry += " AND artstato = 'A'";
        if (ordric.equals("per codice"))
            qry += " ORDER BY artcod";
        else
            qry += " ORDER BY artdescr";
        Cursor cursor = Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            Record r = new Record();
            r.insStringa("codice", cursor.getString(0));
            r.insStringa("descr", cursor.getString(1));
            r.insStringa("um", cursor.getString(2));
            r.insIntero("notifica", cursor.getInt(3));
            vart.add(r);
        }
        cursor.close();
        lraadapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                if (resultCode == Activity.RESULT_OK) {
                    ArrayList<String> matches = data.getStringArrayListExtra(
                            RecognizerIntent.EXTRA_RESULTS);
                    //String voice = ris.getLastPathSegment();
                    if (matches != null && matches.size() > 0)
                        ricdescr.setText(matches.get(0).toLowerCase());
                }
                break;
            }
        }

    }

}
