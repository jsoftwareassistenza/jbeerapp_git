package jsoftware.jbeerapp.forms;

import android.database.Cursor;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaScopertiAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;

public class FormScoperti extends AppCompatActivity {


    private String clicodice;
    private String ccod = "";
    private String cnome = "";
    private String cind = "";
    private String civa = "";
    private String cemail = "";
    private ArrayList<Record> vscop = null;
    private ListaScopertiAdapter lscopadapter;
    private ListView listascop;

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_scoperti);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        listascop = findViewById(R.id.fsco_lista);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodice"))
            clicodice = getIntent().getExtras().getString("clicodice");

        registerForContextMenu(listascop);

        vscop = new ArrayList();
        lscopadapter = new ListaScopertiAdapter(this.getApplication(), vscop, this);
        listascop.setAdapter(lscopadapter);


        visualizzaDatiCliente();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.msg_menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.msg_action_ok) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    public void visualizzaDatiCliente() {
        HashMap hgiainc = new HashMap();
        String[] pars = new String[1];
        pars[0] = clicodice;
        Cursor cursor = Env.db.rawQuery(
                "SELECT clicodice,clinome,cliindir," +
                        "cliloc,cliprov,clipiva,cliemail1,cliemail2,cliemailpec FROM clienti WHERE clicodice = ?", pars);
        cursor.moveToFirst();
        ccod = cursor.getString(0);
        cnome = cursor.getString(1);
        cind = cursor.getString(2) + " " + cursor.getString(3) + " " + cursor.getString(4);
        civa = cursor.getString(5);
        if (!cursor.getString(6).equals(""))
            cemail = cursor.getString(6);
        else if (!cursor.getString(7).equals(""))
            cemail = cursor.getString(7);
        else if (!cursor.getString(8).equals(""))
            cemail = cursor.getString(8);
        String infocli = cursor.getString(0) + " " + cursor.getString(1) + "\r\n"
                + cursor.getString(2) + "\r\n"
                + cursor.getString(3) + " " + cursor.getString(4) + "\r\n"
                + "p.IVA:" + cursor.getString(5) + "\r\n";
        //tvcliente.setText(infocli);
        cursor.close();


        vscop.clear();
        cursor = Env.db.rawQuery(
                "SELECT sctipo,scsezdoc,scdatadoc,scnumdoc,sctipoop,scnote,scclicod,scdestcod," +
                        "scdatascad,sctiposcad,scimportoscad,scresiduoscad,scscadid,scrateizzata FROM scoperti WHERE scclicod=? " +
                        " AND scresiduoscad > 0 ORDER BY scdatadoc ASC,scnumdoc ASC", pars);
        while (cursor.moveToNext()) {
            Boolean scopok = true;
            // se bolla sospesa e presente fattura di fine mese, non stampa la bolla
            if (cursor.getString(0).equals("B")) {
                int anno = Formattazione.estraiIntero(cursor.getString(2).substring(0, 4));
                int mese = Formattazione.estraiIntero(cursor.getString(2).substring(5, 7));
                mese++;
                if (mese > 12) {
                    anno++;
                    mese = 1;
                }
                Data dt = new Data(1, mese, anno);
                dt.decrementaGiorni(1);
                String dtf = dt.formatta(Data.AAAA_MM_GG, "-");
                String[] pars2 = new String[2];
                pars2[0] = clicodice;
                pars2[1] = dtf;
                Cursor cursor2 = Env.db.rawQuery(
                        "SELECT * FROM scoperti WHERE scclicod=? AND scresiduoscad > 0 AND sctipo = 'F' AND scdatadoc=?", pars2);
                if (cursor2.moveToFirst())
                    scopok = false;
                cursor2.close();
            }
            // prepara riga
            Record r = new Record();
            r.insStringa("tipo", cursor.getString(0));
            r.insStringa("sezdoc", cursor.getString(1));
            r.insStringa("datadoc", cursor.getString(2));
            r.insIntero("numdoc", cursor.getInt(3));
            r.insStringa("tipoop", cursor.getString(4));
            r.insStringa("note", cursor.getString(5));
            r.insIntero("scadid", cursor.getInt(12));
            r.insStringa("datascad", cursor.getString(8));
            r.insIntero("tiposcad", cursor.getInt(9));
            r.insDouble("importoscad", cursor.getDouble(10));
            r.insDouble("residuoscad", cursor.getDouble(11));
            r.insDouble("saldo", 0);
            r.insIntero("rateizzata", cursor.getInt(13));
            r.insIntero("calctot", scopok ? 1 : 0);
            vscop.add(r);
        }
        cursor.close();
        lscopadapter.notifyDataSetChanged();
        //aggiornaTotale();
    }
}
