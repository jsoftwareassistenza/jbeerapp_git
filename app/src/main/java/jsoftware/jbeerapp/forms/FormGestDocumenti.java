package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaCatalogoAdapter;
import jsoftware.jbeerapp.adapters.SpinnerAdapterYellow;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.Record;

public class FormGestDocumenti extends AppCompatActivity {

    private TextView etCliente;
    private boolean prezzo = true;

    private ImageButton bvoice;
    private ImageButton sendpdf;
    private CheckBox sent;
    private ListView listadoc;
    private ArrayList<Record> vcli = new ArrayList();
    private ListaCatalogoAdapter lartadapter;
    private Spinner spTipoDoc;
    private ArrayList<String> lTipoDoc;
    private ArrayList<Record> vTipoDoc;
    private SpinnerAdapterYellow tipoDocadapter;
    private boolean prezzi = true;
    private boolean checkati = false;
    private String tDoc = "Tipo Doc.";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_gestdocumenti);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        etCliente = findViewById(R.id.gestdoc_riccli);
        bvoice = findViewById(R.id.gestdoc_ricclivoice);
        sendpdf = findViewById(R.id.gestdoc_sendpdf);
        listadoc = findViewById(R.id.gestdoc_listadoc);
        spTipoDoc = findViewById(R.id.gestdoc_tipodoc);


        // colore
        lTipoDoc = new ArrayList();
        vTipoDoc = new ArrayList();
        lTipoDoc.add("DDT");
        Record rg = new Record();
        rg.insStringa("cod", "...");

        tipoDocadapter = new SpinnerAdapterYellow(FormGestDocumenti.this.getApplicationContext(), R.layout.spinner_item_white, lTipoDoc);
        spTipoDoc.setAdapter(tipoDocadapter);
        spTipoDoc.setSelection(0);


        registerForContextMenu(listadoc);


//        vart = new ArrayList<Record>();
//        lartadapter = new ListaCatalogoAdapter(this.getApplicationContext(), vart, this);
//        listaart.setAdapter(lartadapter);
//        aggiornaLista(true,false);
//
//
//        artdescr.requestFocus();
//
//
//        spcolore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
//                col=(String) spcolore.getSelectedItem();
//                aggiornaLista(prezzi,false);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parentView) {
//                // your code here
//            }
//
//        });
//
//        spstile.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
//                sty=(String) spstile.getSelectedItem();
//                aggiornaLista(prezzi,false);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parentView) {
//                // your code here
//            }
//
//        });
//
//        spgrado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
//                grad=(String) spgrado.getSelectedItem();
//                aggiornaLista(prezzi,false);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parentView) {
//                // your code here
//            }
//
//        });

        bvoice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Articolo...");
                startActivityForResult(intent, 1);
            }
        });

        sendpdf.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ArrayList<Record> a = preparaDati();
                //boolean pdfok = FunzioniJBeerApp.generaPDFDocumentoCatalogo(a, getApplicationContext(), "/storage/emulated/0/Android/data/jsoftware.jbeer/Documento.pdf");
                boolean pdfok = FunzioniJBeerApp.generaPDFDocumentoCatalogo(a, getApplicationContext());
                if (pdfok) {
                    String soggettomail = "";
                    String testomail = "";
                    soggettomail = "Invio Catalogo";
                    testomail = "In allegato il catalogo delle nostre birre";

                    String[] maildest = new String[1];
                    maildest[0] = "serena.cecchetto@jsoftware.it";

                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, maildest);
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, soggettomail);
                    emailIntent.putExtra(Intent.EXTRA_TEXT, testomail);
                    //emailIntent.setType("image/jpeg");
                    emailIntent.setType("application/pdf");
                    //&emailIntent.putExtra(Intent.EXTRA_SUBJECT, "PDF Allegato");
                    emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///storage/emulated/0/Android/data/jsoftware.jbeer/Documento.pdf"));
                    emailIntent.setType("message/rfc822");
                    startActivity(emailIntent);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormGestDocumenti.this);
                    builder.setMessage("Errore durante la generazione del PDF")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            finish();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
            }
        });


//        prz.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (buttonView.isChecked()) {
//                    prezzi=true;
//                } else {
//                    prezzi=false;
//                }
//                aggiornaLista(prezzi,false);
//            }
//        });

        etCliente.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                //aggiornaLista(true,false);
            }
        });

        listadoc.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                String tmp = vcli.get(arg2).leggiStringa("descrizione");
                Uri codselez = Uri.parse("content://ricart/" + tmp.replace('/', '='));
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                result.putExtra("bloccato", vcli.get(arg2).leggiStringa("bloccato"));
                setResult(RESULT_OK, result);
                finish();
            }
        });


    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(null);
        menu.add(0, v.getId(), 0, "Seleziona articolo");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_form_documento, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.doc_action_salva) {
            // controlli

        } else if (id == R.id.doc_action_nuovo) {
            Intent i = new Intent(getApplicationContext(), FormDocumento.class);
            FormGestDocumenti.this.startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                if (resultCode == Activity.RESULT_OK) {
                    ArrayList<String> matches = data.getStringArrayListExtra(
                            RecognizerIntent.EXTRA_RESULTS);
                    if (matches != null && matches.size() > 0)
                        etCliente.setText(matches.get(0).toLowerCase());
                }
                break;
            }
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals("Seleziona articolo")) {
//            String tmp = (String) vart.get(posselez).leggiStringa("artdescr");
//            Uri codselez = Uri.parse("content://ricart/" + tmp);
//            Intent result = new Intent(Intent.ACTION_PICK, codselez);
//            result.putExtra("bloccato", (String) vart.get(posselez).leggiStringa("bloccato"));
//            setResult(RESULT_OK, result);
//            finish();
        } else {
            return false;
        }
        return true;
    }


    private void aggiornaLista(boolean prezzi, boolean checkati) {
        vcli.clear();
        String c = etCliente.getText().toString().trim();
        c.replace("'", "''");
        String qry = "SELECT * FROM movimenti LEFT JOIN clienti ON movclicod = clicodice WHERE clicodice <> \"\" ";
        if (!c.equals("")) {
            qry += " AND clinome LIKE \"%" + c + "%\"";
        }

        qry += " ORDER BY clicodice";
        Cursor cursor =
                Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            boolean artok = true;
            Record h = new Record();
            String cc = "";
            if (!cursor.getString(4).equals("")) {
                String qry1 = "SELECT lprezzo1 FROM listini WHERE lartcod='" + cursor.getString(6) + "' ORDER BY ldataval DESC LIMIT 1";
                Cursor cursor1 = Env.db.rawQuery(qry1, null);
                while (cursor1.moveToNext()) {
                    cc = Double.toString(cursor1.getDouble(0));
                }
                cursor1.close();
            } else {
                cc = "No Prezzo";
            }
            h.insStringa("artcauzcod", cursor.getString(8));
            h.insStringa("artcod", cursor.getString(6));
            h.insStringa("catnote", cursor.getString(7));
            h.insStringa("catnomeart", cursor.getString(0));
            h.insStringa("catstile", "Stile: " + cursor.getString(1));
            h.insStringa("catcolore", "Colore: " + cursor.getString(2));
            h.insStringa("catgrado", "Grado alcolico: " + cursor.getString(3));
            if (prezzi) {
                h.insStringa("prz", "€ " + cc);
            } else {
                h.insStringa("prz", "");
            }
            h.insStringa("giac", "Disponibilità: " + cursor.getDouble(5));
            if (checkati) {
                h.insIntero("ch", 1);
            } else {
                h.insIntero("ch", 0);
            }
            if (artok) {

            }
//                vart.add(h);
        }
        cursor.close();
        lartadapter.notifyDataSetChanged();


    }

    private ArrayList<Record> preparaDati() {
        ArrayList<Record> array = new ArrayList();
//        if(vart.size()>0){
//            for(int i=0;i<vart.size();i++){
//                Record r= vart.get(i);
//                if(r.leggiIntero("ch")==1){
//
//                    Record r1= new Record();
//                    r1.insStringa("artcod",(String) r.leggiStringa("artcod"));
//                    r1.insStringa("catnomeart",(String) r.leggiStringa("catnomeart"));
//                    r1.insStringa("catnote",(String) r.leggiStringa("catnote"));
//                    r1.insStringa("catstile",(String) r.leggiStringa("catstile"));
//                    r1.insStringa("catcolore",(String) r.leggiStringa("catcolore"));
//                    r1.insStringa("catgrado",(String) r.leggiStringa("catgrado"));
//                    r1.insStringa("prz",(String) r.leggiStringa("prz"));
//                    String cauz = "";
//                if(!r.leggiStringa("artcauzcod").equals("")) {
//                    String qry = "SELECT cauzdescr FROM cauzioni WHERE cauzcod='" + (String) r.leggiStringa("artcauzcod") + "'";
//                    Cursor cursor = Env.db.rawQuery(qry, null);
//
//
//                    while (cursor.moveToNext()) {
//                        cauz = cursor.getString(0);
//                    }
//                    cursor.close();
//                }
//                    r1.insStringa("cauz",cauz);
//                    array.add(r1);
//                }
//            }
//        }
        return array;

    }


}
