package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.StringTokenizer;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.funzStringa;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentDocumentoCliente.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDocumentoCliente#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDocumentoCliente extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FormDocumento fdoc = null;

    public TextView tvcliente;
    public ImageButton briccli;
    //public ImageButton bnuovocli;
    public Button breginc;
    public EditText edsc1;
    public EditText edsc2;
    public EditText edsc3;
    public Spinner sptipodoc;
    public EditText ednrif;
    public LinearLayout llsconti;
    public TextView tvscopcli;
    public TextView ldatacons;
    public TextView tvdatacons;
    public ImageButton bdatacons;
    public View viewdettaglio;
    private ImageButton butcell;
    private ImageButton butfisso;
    private ImageButton butemail;
    public TextView tvemail;
    public TextView tvindiaz;
    public TextView tvtel;
    public TextView tvcell;
    public TextView tvpiva;
    public TextView tvnotecli;
    public TextView tvcomune;
    public TextView tvclientefatt;
    public View viewclientefatt;
    public View viewdataconsegna;
    //private String clicodice;
    public LinearLayout llnrif;
    public TextView tvsdi;
    public TextView llnote;
    public TextView tvinsegna;
    public ImageButton bnuovocli;


    public FragmentDocumentoCliente() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentDocumentoCliente.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDocumentoCliente newInstance(String param1, String param2) {
        FragmentDocumentoCliente fragment = new FragmentDocumentoCliente();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_documento_cliente, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
/*
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        } else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

        tvcliente = this.getActivity().findViewById(R.id.doc_cli_cliente);
        tvclientefatt = this.getActivity().findViewById(R.id.doc_cli_clientefatt);
        briccli = this.getActivity().findViewById(R.id.doc_cli_buttonRicCli);
        bnuovocli = this.getActivity().findViewById(R.id.doc_cli_buttonNewCli);
        breginc = this.getActivity().findViewById(R.id.doc_cli_buttonRegInc);
        edsc1 = this.getActivity().findViewById(R.id.doc_cli_sc1);
        edsc2 = this.getActivity().findViewById(R.id.doc_cli_sc2);
        edsc3 = this.getActivity().findViewById(R.id.doc_cli_sc3);
        sptipodoc = this.getActivity().findViewById(R.id.doc_cli_tipodoc);
        ednrif = this.getActivity().findViewById(R.id.doc_cli_nrif);
        llsconti = this.getActivity().findViewById(R.id.doc_cli_psconti);
        tvscopcli = this.getActivity().findViewById(R.id.doc_cli_label_scopcli);
        ldatacons = this.getActivity().findViewById(R.id.doc_cli_label_datacons);
        tvdatacons = this.getActivity().findViewById(R.id.doc_cli_datacons);
        llnrif = this.getActivity().findViewById(R.id.doc_ll_nrif);
        bdatacons = this.getActivity().findViewById(R.id.doc_cli_buttonDataCons);
        viewdettaglio = this.getActivity().findViewById(R.id.doc_cli_dettcli);
        viewdettaglio.setVisibility(View.INVISIBLE);
        viewclientefatt = this.getActivity().findViewById(R.id.doc_cli_fatt);
        viewclientefatt.setVisibility(View.GONE);
        viewdataconsegna = this.getActivity().findViewById(R.id.doc_cli_data);
        tvemail = this.getActivity().findViewById(R.id.doc_cli_email);
        tvindiaz = this.getActivity().findViewById(R.id.doc_cli_indAz);
        tvcomune = this.getActivity().findViewById(R.id.doc_cli_comuneAz);
        tvtel = this.getActivity().findViewById(R.id.doc_cli_tel);
        tvcell = this.getActivity().findViewById(R.id.doc_cli_cell);
        tvpiva = this.getActivity().findViewById(R.id.doc_cli_piva);
        tvnotecli = this.getActivity().findViewById(R.id.doc_cli_note);
        tvsdi = this.getActivity().findViewById(R.id.doc_cli_sdi);
        tvinsegna = this.getActivity().findViewById(R.id.doc_cli_insegna);
        llnote = this.getActivity().findViewById(R.id.doc_cli_ll_note);

        butcell = this.getActivity().findViewById(R.id.doc_cli_bcallcel);
        butfisso = this.getActivity().findViewById(R.id.doc_cli_bcalltel);
        butemail = this.getActivity().findViewById(R.id.doc_cli_bemail);


        if (Env.bloccosconti) {
            llsconti.setVisibility(View.INVISIBLE);
        }

        briccli.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (fdoc.righecrp.size() > 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDocumentoCliente.this.getActivity());
                    builder.setMessage("Non si può cambiare il cliente con articoli inseriti")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                    return;
                }
                if (fdoc.fcli.sptipodoc.getSelectedItemPosition() == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDocumentoCliente.this.getActivity());
                    builder.setMessage("Selezionare prima il Tipo Documento")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                    return;
                } else {
                    Intent i = new Intent(FragmentDocumentoCliente.this.getActivity().getApplicationContext(), FormRicercaClienti.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putBoolean("solodest", Env.ricsolodest);
                    i.putExtras(mBundle);
                    FragmentDocumentoCliente.this.startActivityForResult(i, 1);
                }
            }
        });


        bnuovocli.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (fdoc.righecrp.size() > 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDocumentoCliente.this.getActivity());
                    builder.setMessage("Non si può cambiare il cliente con articoli inseriti")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                    return;
                } else {
                    fdoc.clicod = "";
                    fdoc.svuotaDatiCliente();
                    Intent i = new Intent(FragmentDocumentoCliente.this.getActivity().getApplicationContext(), FormNuovoCliente.class);
                    FragmentDocumentoCliente.this.startActivityForResult(i, 3);
                }
            }
        });

        sptipodoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (!fdoc.modificadoc)
                    fdoc.visualizzaDoc();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        breginc.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /*if (Env.tipostampante == 0 && (Env.ssidstampante.equals("") || Env.ipstampante.equals("")))
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDocumentoCliente.this.getActivity());
                    builder.setMessage("Stampante WI-FI non configurata")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog,
                                                            int id)
                                        {
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                }
                else if ((Env.tipostampante == 1 || Env.tipostampante == 2) && Env.nomebtstampante.equals(""))
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDocumentoCliente.this.getActivity());
                    builder.setMessage("Stampante Bluetooth non configurata")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog,
                                                            int id)
                                        {
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                }
                else
                {*/
                Intent i = new Intent(FragmentDocumentoCliente.this.getActivity().getApplicationContext(), FormRegIncassi.class);
                if (!fdoc.clicod.equals("")) {
                    String cc = fdoc.clicod;
                    if (!fdoc.clicodpadre.equals(""))
                        cc = fdoc.clicodpadre;
                    Bundle mBundle = new Bundle();
                    mBundle.putString("cod", cc);
                    i.putExtras(mBundle);
                }
                FragmentDocumentoCliente.this.startActivityForResult(i, 4);
            }

            //}
        });

        bdatacons.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DatePickerForm dpf = new DatePickerForm();
                dpf.activity = FragmentDocumentoCliente.this.getActivity();
                dpf.callback = new DatePickerForm.DPCallback() {
                    @Override
                    public void onDialogResult(int year, int monthOfYear, int dayOfMonth) {
                        Data dt = new Data(dayOfMonth, monthOfYear + 1, year);
                        tvdatacons.setText(dt.formatta(Data.GG_MM_AAAA, "/") + "\n" + dt.giornoSettimana());
                        fdoc.datacons = dt.formatta(Data.AAAA_MM_GG, "-");
                    }
                };
                dpf.show(FragmentDocumentoCliente.this.getActivity().getSupportFragmentManager(), "datePicker");
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                // ricerca clienti
                if (resultCode == Activity.RESULT_OK) {
                    Env.quantitaCarrello = 0;
                    ris = data.getData();
                    String bloccato = (String) data.getExtras().get("bloccato");
                    String cx = ris.getLastPathSegment();
                    if (!cx.equals("")) {
                        viewdettaglio.setVisibility(View.VISIBLE);
                        cx = cx.replace('=', '/');
                        fdoc.clicod = cx;
                        fdoc.visualizzaDatiCliente();

                    }
                }
                break;
            }
            case (2): {
                // ricerca clienti del giro
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    //String bloccato = (String) data.getExtras().get("bloccato");
                    String cx = ris.getLastPathSegment();
                    if (!cx.equals("")) {
                        fdoc.clicod = cx;
                        fdoc.visualizzaDatiCliente();
                    }
                }
                break;
            }
            case (3): {
                // nuovo cliente
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    //String bloccato = (String) data.getExtras().get("bloccato");
                    String cx = ris.getLastPathSegment();
                    if (!cx.equals("")) {
                        Env.quantitaCarrello = 0;
                        fdoc.clicod = (String) data.getExtras().get("clicodice");
                        fdoc.visualizzaDatiCliente();
                    }
                }
                break;
            }
            case (5): {
                // nuovo cliente
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    String dnc = ris.getLastPathSegment();
                    if (!dnc.equals("")) {
                        StringTokenizer stok = new StringTokenizer(dnc, "|");
                        String sok = stok.nextToken();
                        String nuovook = stok.nextToken().trim();
                        String codesistente = stok.nextToken().trim();
                        String nome = stok.nextToken().trim();
                        String piva = stok.nextToken().trim();
                        String ind = stok.nextToken().trim();
                        String tel = stok.nextToken().trim();
                        String mobile = stok.nextToken().trim();
                        String pag = stok.nextToken().trim();
                        if (nuovook.equals("N") && !codesistente.equals("")) {
                            // cliente esistente
                            fdoc.clicod = codesistente;
                            fdoc.visualizzaDatiCliente();
                        } else if (nuovook.equals("S")) {
                            // nuovo cliente
                            // ins.cliente
                            SQLiteStatement stc = Env.db.compileStatement(
                                    "INSERT INTO clienti (" +
                                            "clicodice," +
                                            "clinome," +
                                            "cliindir," +
                                            "cliloc," +
                                            "cliprov," +
                                            "clipiva," +
                                            "clicodpadre," +
                                            "clicodpag," +
                                            "clicodiva," +
                                            "clinumtel," +
                                            "clinumcell," +
                                            "clinumfax," +
                                            "clicodlist," +
                                            "clinprz," +
                                            "cliraggrddt," +
                                            "climodprz," +
                                            "clistampaprz," +
                                            "clisc1," +
                                            "clisc2," +
                                            "clisc3," +
                                            "climag," +
                                            "clifido," +
                                            "clibloccofido," +
                                            "clicausm," +
                                            "clicauom," +
                                            "clicaureso," +
                                            "clicausost," +
                                            "climaxscvend," +
                                            "clispeseinc," +
                                            "clispesebol," +
                                            "clispesetrasp," +
                                            "clispesecauz," +
                                            "clibancacod," +
                                            "clibancadescr," +
                                            "cliabi," +
                                            "cliagenzia," +
                                            "clicab," +
                                            "climodpag," +
                                            "cliordlotto," +
                                            "climastro," +
                                            "cliconto," +
                                            "clisottoconto," +
                                            "clibollaval," +
                                            "clibollaqta," +
                                            "clifattura," +
                                            "clicorrisp," +
                                            "cliddtreso," +
                                            "cliblocco," +
                                            "clinuovo," +
                                            "clivalidato" +
                                            ") " +
                                            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                            int progcli = 1;
                            Boolean trovato = false;
                            String codnew = "";
                            while (!trovato) {
                                codnew = Env.pedcod + funzStringa.riempiStringa("" + progcli, 3, funzStringa.DX, '0');
                                String[] pars = new String[1];
                                pars[0] = codnew;
                                Cursor cursorp = Env.db.rawQuery("SELECT clicodice FROM clienti WHERE clicodice=?", pars);
                                if (!cursorp.moveToFirst()) {
                                    trovato = true;
                                } else {
                                    progcli++;
                                }
                                cursorp.close();
                            }
                            stc.clearBindings();
                            // codice
                            stc.bindString(1, codnew);
                            // nome
                            stc.bindString(2, nome);
                            // indirizzo
                            stc.bindString(3, ind);
                            // localita
                            stc.bindString(4, "");
                            // prov
                            stc.bindString(5, "");
                            // p.iva
                            stc.bindString(6, piva);
                            // codpadre
                            stc.bindString(7, "");
                            // codpag
                            stc.bindString(8, pag);
                            // codiva
                            stc.bindString(9, "");
                            // tel
                            stc.bindString(10, tel);
                            // cell
                            stc.bindString(11, mobile);
                            // fax
                            stc.bindString(12, "");
                            // codlist
                            stc.bindString(13, Env.clinuovolist);
                            // posprezzo
                            if (Env.clinuovoposprz > 0)
                                stc.bindLong(14, Env.clinuovoposprz);
                            else
                                stc.bindLong(14, 1);
                            // raggr.ddt
                            stc.bindLong(15, 1);
                            // mod.prz.
                            stc.bindString(16, "S");
                            // stampa prz.
                            stc.bindString(17, "S");
                            // sconti
                            stc.bindDouble(18, 0);
                            stc.bindDouble(19, 0);
                            stc.bindDouble(20, 0);
                            // mag
                            stc.bindDouble(21, 0);
                            // fido
                            stc.bindDouble(22, 0);
                            // blocco fido
                            stc.bindString(23, "N");
                            // causali
                            stc.bindString(24, "S");
                            stc.bindString(25, "S");
                            stc.bindString(26, "S");
                            stc.bindString(27, "S");
                            // max.sc.vend.%
                            stc.bindDouble(28, 100);
                            // speseinc
                            stc.bindString(29, "S");
                            // spesebol
                            stc.bindString(30, "S");
                            // spesetrasp
                            stc.bindString(31, "S");
                            // spesecauz
                            stc.bindString(32, "S");
                            // banca
                            stc.bindString(33, "");
                            stc.bindString(34, "");
                            stc.bindString(35, "");
                            stc.bindString(36, "");
                            stc.bindString(37, "");
                            // modpag
                            stc.bindString(38, "N");
                            // ord.lotto
                            stc.bindString(39, "A");
                            // sottoconto
                            stc.bindString(40, "");
                            stc.bindString(41, "");
                            stc.bindString(42, codnew);
                            // documenti ammessi
                            stc.bindString(43, "S");
                            stc.bindString(44, "S");
                            stc.bindString(45, "S");
                            stc.bindString(46, "S");
                            stc.bindString(47, "N");
                            // blocco
                            stc.bindString(48, "N");
                            // nuovo
                            stc.bindLong(49, 1);
                            // validato
                            stc.bindString(50, "N");
                            stc.execute();
                            stc.close();

                            fdoc.nuovoins = true;
                            fdoc.clicod = codnew;
                            fdoc.visualizzaDatiCliente();
                            briccli.setEnabled(false);
                            //bnuovocli.setEnabled(false);
                        }
                    }
                }
                break;
            }
            case (4): {
                // ritorno da reg.incassi
                fdoc.visualizzaDatiCliente();
                break;
            }
        }
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        fdoc.nfrag++;
        if (fdoc.nfrag == 3)
            fdoc.inizializza();
    }
}
