package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaClientiGiroAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;

public class FormClientiGiro extends AppCompatActivity {
    public String cli = "";
    private ListView listacli;
    private ArrayList<HashMap> vcli;
    private ListaClientiGiroAdapter lcliadapter;
    private int posselez = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_clienti_giro);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        listacli = findViewById(R.id.cligiro_listacli);

        registerForContextMenu(listacli);

        listacli.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                String tmp = (String) vcli.get(arg2).get("codice");
                Uri codselez = Uri.parse("content://cligiro/" + tmp);
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                result.putExtra("bloccato", (String) vcli.get(arg2).get("bloccato"));
                setResult(RESULT_OK, result);
                finish();
            }
        });

        listacli.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
                posselez = arg2;
                return false;
            }
        });

        vcli = new ArrayList<HashMap>();
        lcliadapter = new ListaClientiGiroAdapter(this.getApplicationContext(), vcli, this);
        listacli.setAdapter(lcliadapter);

        String cliatt = FunzioniJBeerApp.leggiProprieta("cligiro");

        aggiornaLista();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(null);
        //menu.add(0, v.getId(), 0, "Annulla sospeso");
        menu.add(0, v.getId(), 0, "Seleziona");
        menu.add(0, v.getId(), 1, "Apri posizione GPS");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.form_ricerca_clienti, menu);
        return true;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals("Seleziona cliente")) {
            String tmp = (String) vcli.get(posselez).get("codice");
            Uri codselez = Uri.parse("content://cligiro/" + tmp);
            Intent result = new Intent(Intent.ACTION_PICK, codselez);
            result.putExtra("bloccato", (String) vcli.get(posselez).get("bloccato"));
            setResult(RESULT_OK, result);
            finish();
        } else if (item.getTitle().equals("Apri posizione GPS")) {
            Intent geointent = new Intent(Intent.ACTION_VIEW);
            geointent.setData(Uri.parse((String) vcli.get(posselez).get("geouri")));
            if (geointent.resolveActivity(this.getPackageManager()) != null) {
                this.startActivity(geointent);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        this);
                builder.setMessage("Dati posizione non visualizzabili")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
            }
        } else {
            return false;
        }
        return true;
    }

    private void aggiornaLista() {
        String cliatt = FunzioniJBeerApp.leggiProprieta("cligiro");
        int pos = -1;
        vcli.clear();
        String qry = "SELECT clicodice,clinome,cliindir,cliloc,clicodpadre,cligpsnordsud,cligpsestovest,cligpsnordgradi,cligpsnordmin,cligpsnordsec,cligpsestgradi,cligpsestmin,cligpsestsec " +
                "FROM girivisita LEFT JOIN clienti ON girivisita.gvcodric = clienti.clicodice WHERE gvcod = '" + Env.gvcod + "' ORDER BY gvprog";
        Cursor cursor = Env.db.rawQuery(qry, null);
        int i = 0;
        while (cursor.moveToNext()) {
            HashMap h = new HashMap();
            String cc = cursor.getString(0);
            if (!cursor.getString(5).equals(""))
                cc = cursor.getString(5);
            h.put("codice", cursor.getString(0));
            h.put("nome", cursor.getString(1));
            h.put("indirizzo", cursor.getString(2));
            h.put("localita", cursor.getString(3));
            h.put("scoperti", FunzioniJBeerApp.totaleScopertiCliente(cc));
            if (cursor.getDouble(7) > 0 || cursor.getDouble(8) > 0 || cursor.getDouble(9) > 0 || cursor.getDouble(10) > 0 || cursor.getDouble(11) > 0 || cursor.getDouble(12) > 0) {
                double gpsn = cursor.getDouble(7) + (cursor.getDouble(8) / 60) + (cursor.getDouble(9) / 3600);
                double gpse = cursor.getDouble(10) + (cursor.getDouble(11) / 60) + (cursor.getDouble(12) / 3600);
                String geouri = "geo:" + Formattazione.formatta(gpsn, "####0.000000", 1).replaceAll(",", ".") + "," +
                        Formattazione.formatta(gpse, "####0.000000", 1).replaceAll(",", ".");
                h.put("geouri", geouri);
                h.put("gpsn", gpsn);
                h.put("gpse", gpse);
            } else {
                h.put("geouri", "");
                h.put("gpsn", new Double(0));
                h.put("gpse", new Double(0));
            }
            boolean bloccato = false;
            double impscadscadute = 0;
            if (Env.nscadblocco > 0) {
                double[] bloc = FunzioniJBeerApp.scadenzeScadute(cc, (new Data()), Env.ggtollblocco);
                bloccato = (bloc[1] > 0 && bloc[0] > 0 && bloc[0] > Env.impblocco);
                impscadscadute = bloc[0];
                h.put("bloccato", bloccato ? "S" : "N");
                if (bloccato)
                    h.put("impscaduto", impscadscadute);
                else
                    h.put("impscaduto", (double) 0);
            } else {
                h.put("bloccato", "N");
                h.put("impscaduto", (double) 0);
            }
            h.put("visitato", FunzioniJBeerApp.clienteVisitato(cursor.getString(0)) ? "S" : "N");
            vcli.add(h);
            if (cliatt.equals(cursor.getString(0)))
                pos = i;
            i++;
        }
        cursor.close();
        lcliadapter.notifyDataSetChanged();
        if (pos >= 0)
            listacli.setSelection(pos);
    }
}
