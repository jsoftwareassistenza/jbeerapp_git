package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaElencoBackupOnlineAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniHTTP;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.Record;

public class FormElencoBackupOnline extends AppCompatActivity {
    ProgressBar progress = null;
    TextView labelinfo = null;
    ProcElencoBackup proc = null;
    private ListView listafiles;
    private Button bok;
    private Button bannulla;
    private ArrayList<Record> vfiles;
    private ListaElencoBackupOnlineAdapter lboadapter;
    private int posselez = -1;
    private boolean checkincorso = false;
    private boolean downloadincorso = false;
    private String filesel = "";
    private int dimfilesel = 0;

    @Override
    public void onBackPressed() {
        if (!checkincorso && !downloadincorso)
            super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_elenco_backup_online);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        progress = findViewById(R.id.elbackup_progress);
        labelinfo = findViewById(R.id.elbackup_labelinfo);

        listafiles = findViewById(R.id.elbackup_listafiles);
/*        View header = this.getLayoutInflater().inflate(R.layout.listafileselbackuponline_header, null);
        listafiles.addHeaderView(header);*/

        vfiles = new ArrayList<Record>();
        lboadapter = new ListaElencoBackupOnlineAdapter(this.getApplicationContext(), vfiles, this);
        listafiles.setAdapter(lboadapter);

        listafiles.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (vfiles.size() > 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormElencoBackupOnline.this);
                    builder.setMessage("Confermi ripristino di questo backup?")
                            .setPositiveButton("SI",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            Record rec = vfiles.get(position);
                                            filesel = rec.leggiStringa("nomefile");
                                            dimfilesel = rec.leggiIntero("dimbytes");
                                            ProcDownloadBackup pdownload = new ProcDownloadBackup();
                                            pdownload.execute();
                                        }
                                    })
                            .setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
            }
        });

        proc = new ProcElencoBackup();
        proc.execute();
    }


    private class ProcElencoBackup extends AsyncTask<String, HashMap, String> {
        @Override
        protected String doInBackground(String... arg) {
            checkincorso = true;
            boolean connok = false;
            String err = "";
            if (Env.tiposcambiodati == 0) {
                // sim dati
                if (FunzioniJBeerApp.dispositivoOnline(FormElencoBackupOnline.this.getApplicationContext())) {
                    connok = true;
                } else {
                    // se attivata WIFI la disabilita
                    if (FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()).equals("WIFI")) {
                        HashMap h = new HashMap();
                        h.put("label", "Disattivazione Wi-FI...");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);
                        FunzioniJBeerApp.disattivaWiFi(FormElencoBackupOnline.this.getApplicationContext());
                        int ntent = 0;
                        System.out.println("tipo conn:" + FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()));
                        while (FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()).equals("WIFI") && ntent < 20) {
                            System.out.println("tipo conn:" + FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()));
                            try {
                                Thread.currentThread().sleep(1000);
                            } catch (Exception esleep) {
                            }
                            ntent++;
                        }
                    }
                    int ntentonline = 0;
                    while (!FunzioniJBeerApp.dispositivoOnline(getApplicationContext()) && ntentonline < 10) {
                        try {
                            Thread.currentThread().sleep(1000);
                        } catch (Exception esleep) {
                        }
                        ntentonline++;
                    }
                    if (FunzioniJBeerApp.dispositivoOnline(getApplicationContext())) {
                        connok = true;
                    } else {
                        err = "Dispositivo non collegato ad Internet\nImpossibile inviare i dati ora";
                        HashMap h = new HashMap();
                        h.put("label", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "N");
                        h.put("indeterminate", "N");
                        h.put("error", err);
                        this.publishProgress(h);
                        checkincorso = false;
                    }
                }
            } else {
                // WIFI
                if (FunzioniJBeerApp.dispositivoOnline(FormElencoBackupOnline.this.getApplicationContext())) {
                    connok = true;
                } else {
                    boolean wifion = false;
                    if (FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()).equals("WIFI")) {
                        wifion = true;
                    } else {
                        HashMap h = new HashMap();
                        h.put("label", "Attivazione Wi-FI...");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);
                        FunzioniJBeerApp.attivaWiFi(FormElencoBackupOnline.this.getApplicationContext());
                        int ntent = 0;

                        while (!FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()).equals("WIFI") && ntent < 20) {
                            Log.v("JBEERAPP", "controllo " + ntent + " attivazione wifi");
                            try {
                                Thread.currentThread().sleep(1000);
                            } catch (Exception esleep) {
                            }
                            ntent++;
                        }
                        if (!FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()).equals("WIFI")) {
                            h = new HashMap();
                            h.put("label", "");
                            h.put("val", "0");
                            h.put("max", "0");
                            h.put("visible", "N");
                            h.put("indeterminate", "N");
                            h.put("error", "Attivazione Wi-FI fallita");
                            this.publishProgress(h);
                            checkincorso = false;
                        } else {
                            wifion = true;
                        }
                    }
                    if (wifion) {
                        // controlla rete
                        String ssid = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(FormElencoBackupOnline.this.getApplicationContext());
                        if (ssid.equals(Env.ssidscambiodati)) {
                            connok = true;
                        } else {
                            HashMap h = new HashMap();
                            h.put("label", "Connessione a Wi-FI scambio dati...");
                            h.put("val", "0");
                            h.put("max", "0");
                            h.put("visible", "S");
                            h.put("indeterminate", "S");
                            h.put("error", "");
                            this.publishProgress(h);
                            FunzioniJBeerApp.connettiReteWiFi(getApplicationContext(), Env.ssidscambiodati);
                            String rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                            int ntent = 0;
                            while (!rc.equals(Env.ssidscambiodati) && ntent < 30) {
                                Log.v("JBEERAPP", "tentativo " + ntent + " connessione " + Env.ssidscambiodati);
                                FunzioniJBeerApp.connettiReteWiFi(getApplicationContext(), Env.ssidscambiodati);
                                try {
                                    Thread.currentThread().sleep(1000);
                                } catch (Exception esleep) {
                                }
                                rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                                if (rc.equals(Env.ssidscambiodati))
                                    connok = true;
                                ntent++;
                            }
                            if (!connok) {
                                h = new HashMap();
                                h.put("label", "");
                                h.put("val", "0");
                                h.put("max", "0");
                                h.put("visible", "N");
                                h.put("indeterminate", "N");
                                h.put("error", "Connessione Wi-FI a rete " + Env.ssidscambiodati + " fallita");
                                this.publishProgress(h);
                                checkincorso = false;
                            }
                        }
                    }
                }
            }

            if (connok) {
                Cursor cursor = Env.db.rawQuery(
                        "SELECT pedcod,user,pwd,serverjcloud FROM datiterm",
                        null);
                cursor.moveToFirst();
                String tcod = cursor.getString(0);
                String user = cursor.getString(1);
                String pwd = cursor.getString(2);
                String server = cursor.getString(3);
                HashMap h = new HashMap();
                h.put("label", "Controllo in corso...");
                h.put("val", "0");
                h.put("max", "0");
                h.put("visible", "S");
                h.put("indeterminate", "S");
                h.put("error", "");
                this.publishProgress(h);
                ArrayList<HashMap> vbk = FunzioniJBeerApp.elencoBackupOnline(user, pwd, tcod, server);
                cursor.close();
                if (vbk.size() == 0) {
                    h = new HashMap();
                    h.put("label", "Nessun backup presente online");
                    h.put("val", "0");
                    h.put("max", "0");
                    h.put("visible", "N");
                    h.put("indeterminate", "N");
                    h.put("error", "");
                    this.publishProgress(h);
                    checkincorso = false;
                } else {
                    vfiles.clear();
                    for (int i = 0; i < vbk.size(); i++) {
                        HashMap tmp = vbk.get(i);
                        Record rx = new Record();
                        rx.insStringa("nomefile", (String) tmp.get("nomefile"));
                        rx.insStringa("dataarrivo", (String) tmp.get("dataarrivo"));
                        rx.insStringa("oraarrivo", (String) tmp.get("oraarrivo"));
                        rx.insIntero("dimbytes", ((Integer) tmp.get("dimbytes")).intValue());
                        vfiles.add(rx);
                    }
                    h = new HashMap();
                    h.put("label", "Controllo terminato");
                    h.put("val", "0");
                    h.put("max", "0");
                    h.put("visible", "N");
                    h.put("indeterminate", "N");
                    h.put("error", "");
                    this.publishProgress(h);
                    checkincorso = false;
                }
            }
            return "";
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(String value) {
            lboadapter.notifyDataSetChanged();
        }

        @Override
        protected void onProgressUpdate(HashMap... values) {

            labelinfo.setText((String) values[0].get("label"));
            progress.setMax(Formattazione.estraiIntero((String) values[0].get("max")));
            progress.setProgress(Formattazione.estraiIntero((String) values[0].get("val")));
            progress.setVisibility(values[0].get("visible").equals("S") ? View.VISIBLE : View.INVISIBLE);
            progress.setIndeterminate(values[0].get("indeterminate").equals("S"));
            String err = (String) values[0].get("error");
            if (!err.equals("")) {
                labelinfo.setText(err);
            }
        }
    }

    private class ProcDownloadBackup extends AsyncTask<String, HashMap, String> {
        @Override
        protected String doInBackground(String... arg) {
            downloadincorso = true;
            boolean connok = false;
            String err = "";
            if (Env.tiposcambiodati == 0) {
                // sim dati
                if (FunzioniJBeerApp.dispositivoOnline(FormElencoBackupOnline.this.getApplicationContext())) {
                    connok = true;
                } else {
                    // se attivata WIFI la disabilita
                    if (FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()).equals("WIFI")) {
                        HashMap h = new HashMap();
                        h.put("label", "Disattivazione Wi-FI...");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);
                        FunzioniJBeerApp.disattivaWiFi(FormElencoBackupOnline.this.getApplicationContext());
                        int ntent = 0;
                        System.out.println("tipo conn:" + FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()));
                        while (FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()).equals("WIFI") && ntent < 20) {
                            System.out.println("tipo conn:" + FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()));
                            try {
                                Thread.currentThread().sleep(1000);
                            } catch (Exception esleep) {
                            }
                            ntent++;
                        }
                    }
                    int ntentonline = 0;
                    while (!FunzioniJBeerApp.dispositivoOnline(getApplicationContext()) && ntentonline < 10) {
                        try {
                            Thread.currentThread().sleep(1000);
                        } catch (Exception esleep) {
                        }
                        ntentonline++;
                    }
                    if (FunzioniJBeerApp.dispositivoOnline(getApplicationContext())) {
                        connok = true;
                    } else {
                        err = "Dispositivo non collegato ad Internet\nImpossibile inviare i dati ora";
                        HashMap h = new HashMap();
                        h.put("label", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "N");
                        h.put("indeterminate", "N");
                        h.put("error", err);
                        this.publishProgress(h);
                        downloadincorso = false;
                    }
                }
            } else {
                // WIFI
                if (FunzioniJBeerApp.dispositivoOnline(FormElencoBackupOnline.this.getApplicationContext())) {
                    connok = true;
                } else {
                    boolean wifion = false;
                    if (FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()).equals("WIFI")) {
                        wifion = true;
                    } else {
                        HashMap h = new HashMap();
                        h.put("label", "Attivazione Wi-FI...");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);
                        FunzioniJBeerApp.attivaWiFi(FormElencoBackupOnline.this.getApplicationContext());
                        int ntent = 0;

                        while (!FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()).equals("WIFI") && ntent < 20) {
                            Log.v("JBEERAPP", "controllo " + ntent + " attivazione wifi");
                            try {
                                Thread.currentThread().sleep(1000);
                            } catch (Exception esleep) {
                            }
                            ntent++;
                        }
                        if (!FunzioniJBeerApp.tipoConnessione(FormElencoBackupOnline.this.getApplicationContext()).equals("WIFI")) {
                            h = new HashMap();
                            h.put("label", "");
                            h.put("val", "0");
                            h.put("max", "0");
                            h.put("visible", "N");
                            h.put("indeterminate", "N");
                            h.put("error", "Attivazione Wi-FI fallita");
                            this.publishProgress(h);
                            downloadincorso = false;
                        } else {
                            wifion = true;
                        }
                    }
                    if (wifion) {
                        // controlla rete
                        String ssid = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(FormElencoBackupOnline.this.getApplicationContext());
                        if (ssid.equals(Env.ssidscambiodati)) {
                            connok = true;
                        } else {
                            HashMap h = new HashMap();
                            h.put("label", "Connessione a Wi-FI scambio dati...");
                            h.put("val", "0");
                            h.put("max", "0");
                            h.put("visible", "S");
                            h.put("indeterminate", "S");
                            h.put("error", "");
                            this.publishProgress(h);
                            FunzioniJBeerApp.connettiReteWiFi(getApplicationContext(), Env.ssidscambiodati);
                            String rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                            int ntent = 0;
                            while (!rc.equals(Env.ssidscambiodati) && ntent < 30) {
                                Log.v("JBEERAPP", "tentativo " + ntent + " connessione " + Env.ssidscambiodati);
                                FunzioniJBeerApp.connettiReteWiFi(getApplicationContext(), Env.ssidscambiodati);
                                try {
                                    Thread.currentThread().sleep(1000);
                                } catch (Exception esleep) {
                                }
                                rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                                if (rc.equals(Env.ssidscambiodati))
                                    connok = true;
                                ntent++;
                            }
                            if (!connok) {
                                h = new HashMap();
                                h.put("label", "");
                                h.put("val", "0");
                                h.put("max", "0");
                                h.put("visible", "N");
                                h.put("indeterminate", "N");
                                h.put("error", "Connessione Wi-FI a rete " + Env.ssidscambiodati + " fallita");
                                this.publishProgress(h);
                                downloadincorso = false;
                            }
                        }
                    }
                }
            }

            if (connok) {
                // svuota cartella cache
                File fcardir = getApplicationContext().getCacheDir();
                if (!fcardir.exists())
                    fcardir.mkdir();
                else {
                    File[] lfs = fcardir.listFiles();
                    for (int i = 0; i < lfs.length; i++) {
                        lfs[i].delete();
                    }
                }
                Cursor cursor = Env.db.rawQuery(
                        "SELECT pedcod,user,pwd,serverjcloud FROM datiterm",
                        null);
                cursor.moveToFirst();
                String tcod = cursor.getString(0);
                String user = cursor.getString(1);
                String pwd = cursor.getString(2);
                String server = cursor.getString(3);
                HashMap h = new HashMap();
                h.put("label", "Download " + filesel + " in corso...");
                h.put("val", "0");
                h.put("max", "0");
                h.put("visible", "S");
                h.put("indeterminate", "S");
                h.put("error", "");
                this.publishProgress(h);
                try {
                    String content = URLEncoder.encode("user", "UTF-8")
                            + "="
                            + URLEncoder.encode(user, "UTF-8")
                            + "&"
                            + URLEncoder.encode("pwd", "UTF-8")
                            + "="
                            + URLEncoder.encode(pwd, "UTF-8")
                            + "&"
                            + URLEncoder.encode("nomefile", "UTF-8")
                            + "="
                            + URLEncoder.encode(
                            filesel, "UTF-8");
                    BufferedInputStream in = FunzioniHTTP
                            .inviaPostHttp2(
                                    server + "/service/downloadbackup.do",
                                    content,
                                    "application/x-www-form-urlencoded", null,
                                    new Hashtable(), false, false, false, "",
                                    "", 10000);
                    boolean fileok = true;
                    FileOutputStream fos = new FileOutputStream(getApplicationContext().getCacheDir() + File.separator + filesel);
                    try {
                        byte[] data = new byte[1024];
                        int count;
                        int tot = 0;
                        while ((count = in.read(data, 0, 1024)) != -1) {
                            fos.write(data, 0, count);
                            tot += count;
                            h = new HashMap();
                            h.put("label", "Download file " + filesel);
                            h.put("val", "" + tot);
                            h.put("max", "" + dimfilesel);
                            h.put("visible", "S");
                            h.put("indeterminate", "N");
                            h.put("error", "");
                            this.publishProgress(h);
                        }
                        in.close();
                        fos.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        fileok = false;
                    }
                    if (fileok) {
                        // controllo integrita file zip
                        File fcheck = new File(getApplicationContext().getCacheDir() + File.separator + filesel);
                        if (fcheck.length() == dimfilesel) {
                            h = new HashMap();
                            h.put("label", "Download file " + filesel + " OK");
                            h.put("val", "100");
                            h.put("max", "100");
                            h.put("visible", "N");
                            h.put("indeterminate", "N");
                            h.put("error", "");
                            this.publishProgress(h);
                            // installazione db
                            // unzip file di carico
                            h = new HashMap();
                            h.put("label", "Decompressione file " + filesel + "...");
                            h.put("val", "100");
                            h.put("max", "100");
                            h.put("visible", "S");
                            h.put("indeterminate", "S");
                            h.put("error", "");
                            this.publishProgress(h);
                            FileInputStream fis = new FileInputStream(new File(getApplicationContext().getCacheDir() + File.separator + filesel));
                            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
                            ZipEntry entry;
                            while ((entry = zis.getNextEntry()) != null) {
                                int count;
                                int BUFFER = 2048;
                                byte[] data = new byte[BUFFER];
                                FileOutputStream fosunzip = new FileOutputStream(
                                        getApplicationContext().getCacheDir() + File.separator + "jbeerappbd.db");
                                BufferedOutputStream dest = new BufferedOutputStream(fosunzip, BUFFER);
                                while ((count = zis.read(data, 0, BUFFER)) != -1) {
                                    dest.write(data, 0, count);
                                }
                                dest.flush();
                                dest.close();
                            }
                            zis.close();
                            fis.close();

                            File fcache = getApplicationContext().getCacheDir();
                            File fdata = Environment.getDataDirectory();
                            String dbFilePath = "/data/" + getApplicationContext().getPackageName() + "/databases/jbeerappbd.db";
                            String backupPath = "jbeerappbd.db";
                            File currentDB = new File(fdata, dbFilePath);
                            File backupDB = new File(fcache, backupPath);
                            FileChannel src = null;
                            FileChannel dest = null;
                            try {
                                src = new FileInputStream(backupDB).getChannel();
                                dest = new FileOutputStream(currentDB).getChannel();
                                dest.transferFrom(src, 0, src.size());
                                src.close();
                                dest.close();
                                h = new HashMap();
                                h.put("label", "Ripristino database terminato\nRiavviare l'app");
                                h.put("val", "100");
                                h.put("max", "100");
                                h.put("visible", "N");
                                h.put("indeterminate", "N");
                                h.put("error", "");
                                this.publishProgress(h);
                            } catch (Exception e) {
                                h = new HashMap();
                                h.put("label", "");
                                h.put("val", "100");
                                h.put("max", "100");
                                h.put("visible", "N");
                                h.put("indeterminate", "N");
                                h.put("error", "Errore durante il ripristino del database:" + e.getLocalizedMessage());
                                this.publishProgress(h);
                            }
                            downloadincorso = false;
                        } else {
                            h = new HashMap();
                            h.put("label", "");
                            h.put("val", "100");
                            h.put("max", "100");
                            h.put("visible", "N");
                            h.put("indeterminate", "N");
                            h.put("error", "File corrotto");
                            this.publishProgress(h);
                            downloadincorso = false;
                        }
                    } else {
                        h = new HashMap();
                        h.put("label", "");
                        h.put("val", "100");
                        h.put("max", "100");
                        h.put("visible", "N");
                        h.put("indeterminate", "N");
                        h.put("error", "File corrotto");
                        this.publishProgress(h);
                        downloadincorso = false;
                    }
                } catch (Exception ef) {
                    ef.printStackTrace();
                }
            }
            return "";
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(String value) {
            lboadapter.notifyDataSetChanged();
        }

        @Override
        protected void onProgressUpdate(HashMap... values) {

            labelinfo.setText((String) values[0].get("label"));
            progress.setMax(Formattazione.estraiIntero((String) values[0].get("max")));
            progress.setProgress(Formattazione.estraiIntero((String) values[0].get("val")));
            progress.setVisibility(values[0].get("visible").equals("S") ? View.VISIBLE : View.INVISIBLE);
            progress.setIndeterminate(values[0].get("indeterminate").equals("S"));
            String err = (String) values[0].get("error");
            if (!err.equals("")) {
                labelinfo.setText(err);
            }
        }
    }

}
