package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaPrefOrdineClienteAdapter;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;

public class FormDocPreferitiOrdineCliente extends AppCompatActivity {
    private Button bok;
    private Button bannulla;
    private EditText edcod;
    private EditText eddescr;
    private Spinner ricstato;
    private ListView listaart;
    private ListaPrefOrdineClienteAdapter lprefadapter;
    private ArrayList<Record> vart;
    public ArrayList<Record> righecrp = null;
    private String clicod = "";
    public String clicodpadre = "";
    private ArrayList<String> lst;
    private SpinnerAdapter lstadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_doc_preferiti_ordinecliente);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicod"))
            clicod = getIntent().getExtras().getString("clicod");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodpadre"))
            clicodpadre = getIntent().getExtras().getString("clicodpadre");

        bok = this.findViewById(R.id.prefoc_buttonOk);
        bannulla = this.findViewById(R.id.prefoc_buttonAnnulla);
        ricstato = this.findViewById(R.id.prefoc_ricstato);
        listaart = this.findViewById(R.id.prefoc_listaart);
        edcod = this.findViewById(R.id.prefoc_riccod);
        eddescr = this.findViewById(R.id.prefoc_ricdescr);
        if (Env.codartnum)
            edcod.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);

        // carica stato
        lst = new ArrayList();
        lst.add("PREFERITI");
        lst.add("NUOVI");
        lst.add("IN OFFERTA");
        lst.add("PREZZO AUMENTATO");
        lst.add("PREZZO DIMINUITO");
        lstadapter = new SpinnerAdapter(FormDocPreferitiOrdineCliente.this.getApplicationContext(), R.layout.spinner_item, lst);
        ricstato.setAdapter(lstadapter);

/*        View header = this.getLayoutInflater().inflate(R.layout.prefoc_listaart_header, null);
        listaart.addHeaderView(header);*/

        vart = new ArrayList<Record>();
        lprefadapter = new ListaPrefOrdineClienteAdapter(this.getApplicationContext(), vart);
        listaart.setAdapter(lprefadapter);

        if (Env.depeventocod.equals("")) {
            aggiornaLista();
        } else {
            aggiornaListaEvento();
        }

        bok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ArrayList<HashMap> vris = new ArrayList();
                for (int i = 0; i < vart.size(); i++) {
                    Record r = vart.get(i);
                    if (r.leggiDouble("qtavend") != 0 || r.leggiDouble("qtasm") != 0 || r.leggiDouble("qtaomimp") != 0 ||
                            r.leggiDouble("qtaomtot") != 0) {
                        HashMap h = new HashMap();
                        h.put("artcod", r.leggiStringa("artcod"));
                        h.put("artdescr", r.leggiStringa("artdescr"));
                        h.put("codiva", r.leggiStringa("codiva"));
                        h.put("colli", r.leggiIntero("collivend"));
                        h.put("qtavend", r.leggiDouble("qtavend"));
                        h.put("collism", r.leggiIntero("collism"));
                        h.put("qtasm", r.leggiDouble("qtasm"));
                        h.put("colliomimp", r.leggiIntero("colliomimp"));
                        h.put("qtaomimp", r.leggiDouble("qtaomimp"));
                        h.put("colliomtot", r.leggiIntero("colliomtot"));
                        h.put("qtaomtot", r.leggiDouble("qtaomtot"));
                        h.put("prezzo", r.leggiDouble("prezzo"));
                        h.put("sc1", r.leggiDouble("sc1"));
                        h.put("sc2", r.leggiDouble("sc2"));
                        h.put("sc3", r.leggiDouble("sc3"));
                        vris.add(h);
                    }
                }
                Uri codselez = Uri.parse("content://listaprefoc/OK");
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                result.putExtra("vris", vris);
                setResult(RESULT_OK, result);
                finish();
            }
        });

        bannulla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uri codselez = Uri.parse("content://listaprefoc/ANNULLA");
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                setResult(RESULT_CANCELED, result);
                finish();
            }
        });

        ricstato.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

/*
        edcod.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
                return false;
            }
        });

        eddescr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
                return false;
            }
        });
*/

        edcod.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
            }
        });

        eddescr.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
            }
        });

        listaart.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                //Log.v("jorders", "CLICK PREF " + arg0 + " " + arg1 + " " + arg2 + " " + arg3);
                if (arg3 >= 0) {
                    final Record rec = vart.get((int) arg3);
                    final double npezzi = rec.leggiDouble("artpezzi");
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            FormDocPreferitiOrdineCliente.this);
                    final AlertDialog optionDialog = alert.create();
                    optionDialog.setMessage("Dati articolo");
                    LinearLayout ll = new LinearLayout(FormDocPreferitiOrdineCliente.this.getApplicationContext());
                    ll.setOrientation(LinearLayout.VERTICAL);

                    final TextView tvlabelart = new TextView(FormDocPreferitiOrdineCliente.this);
                    tvlabelart.setText(rec.leggiStringa("artcod") + "-" + rec.leggiStringa("artdescr") + " (" + rec.leggiStringa("artum") + ")");
                    tvlabelart.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                    tvlabelart.setTextColor(Color.BLACK);
                    tvlabelart.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    tvlabelart.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    tvlabelart.setBackgroundDrawable(getResources().getDrawable(R.drawable.textview1));
                    ll.addView(tvlabelart);

                    if (!rec.leggiStringa("artschedaweb").equals("")) {
                        final Button bweb = new Button(FormDocPreferitiOrdineCliente.this);
                        bweb.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_public_white_48dp, 0, 0, 0);
                        bweb.setText("Scheda articolo WEB");
                        bweb.setTextColor(Color.WHITE);
                        bweb.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                        ll.addView(bweb);
                        bweb.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                Intent i = new Intent(FormDocPreferitiOrdineCliente.this.getApplicationContext(), FormWeb.class);
                                Bundle mBundle = new Bundle();
                                mBundle.putString("URL", rec.leggiStringa("artschedaweb"));
                                mBundle.putString("TITOLO", "Scheda art." + rec.leggiStringa("artcod"));
                                i.putExtras(mBundle);
                                FormDocPreferitiOrdineCliente.this.startActivity(i);
                            }
                        });
                    }

                    LinearLayout lqtavend = new LinearLayout(FormDocPreferitiOrdineCliente.this.getApplicationContext());
                    lqtavend.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelqtavend = new TextView(FormDocPreferitiOrdineCliente.this);
                    tvlabelqtavend.setText("Qtà vendita:");
                    tvlabelqtavend.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelqtavend.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelqtavend.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    lqtavend.addView(tvlabelqtavend);
                    final EditText inputcvend = new EditText(FormDocPreferitiOrdineCliente.this);
                    inputcvend.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputcvend.setHint("colli");
                    inputcvend.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    inputcvend.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputcvend.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtavend.addView(inputcvend);
                    final EditText inputqvend = new EditText(FormDocPreferitiOrdineCliente.this);
                    inputqvend.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputqvend.setHint("quantità");
                    if (!FunzioniJBeerApp.umDecimale(rec.leggiStringa("artcod")))
                        inputqvend.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    else
                        inputqvend.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputqvend.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputqvend.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtavend.addView(inputqvend);
                    inputcvend.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (!hasFocus) {
                                double col = Formattazione.estraiDouble(inputcvend.getText().toString());
                                if (npezzi > 0) {
                                    double q = OpValute.arrotondaMat(col * npezzi, 2);
                                    inputqvend.setText(Formattazione.formatta(q, "#####0.000", 0)
                                            .replace(",", "."));
                                }
                            }
                        }
                    });
                    boolean cbloccvend = FunzioniJBeerApp.causaleBloccata(0, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                    if (!cbloccvend) {
                        ll.addView(lqtavend);
                        LinearLayout.LayoutParams lParams_lqtavend = (LinearLayout.LayoutParams) tvlabelqtavend.getLayoutParams();
                        lParams_lqtavend.weight = 0.4f;
                        lParams_lqtavend.width = 0;
                        LinearLayout.LayoutParams lParams_cvend = (LinearLayout.LayoutParams) inputcvend.getLayoutParams();
                        lParams_cvend.weight = 0.2f;
                        lParams_cvend.width = 0;
                        LinearLayout.LayoutParams lParams_qtavend = (LinearLayout.LayoutParams) inputqvend.getLayoutParams();
                        lParams_qtavend.weight = 0.4f;
                        lParams_qtavend.width = 0;
                    }

                    LinearLayout lqtasm = new LinearLayout(FormDocPreferitiOrdineCliente.this.getApplicationContext());
                    lqtasm.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelqtasm = new TextView(FormDocPreferitiOrdineCliente.this);
                    tvlabelqtasm.setText("Qtà sconto merce:");
                    tvlabelqtasm.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelqtasm.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelqtasm.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    lqtasm.addView(tvlabelqtasm);
                    final EditText inputcsm = new EditText(FormDocPreferitiOrdineCliente.this);
                    inputcsm.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputcsm.setHint("colli");
                    inputcsm.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    inputcsm.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputcsm.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtasm.addView(inputcsm);
                    final EditText inputqsm = new EditText(FormDocPreferitiOrdineCliente.this);
                    inputqsm.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputqsm.setHint("quantità");
                    if (!FunzioniJBeerApp.umDecimale(rec.leggiStringa("artcod")))
                        inputqsm.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    else
                        inputqsm.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputqsm.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputqsm.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtasm.addView(inputqsm);
                    inputcsm.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (!hasFocus) {
                                double col = Formattazione.estraiDouble(inputcsm.getText().toString());
                                if (npezzi > 0) {
                                    double q = OpValute.arrotondaMat(col * npezzi, 2);
                                    inputqsm.setText(Formattazione.formatta(q, "#####0.000", 0)
                                            .replace(",", "."));
                                }
                            }
                        }
                    });
                    boolean cbloccsm = FunzioniJBeerApp.causaleBloccata(4, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                    if (!cbloccsm) {
                        ll.addView(lqtasm);
                        LinearLayout.LayoutParams lParams_lqtasm = (LinearLayout.LayoutParams) tvlabelqtasm.getLayoutParams();
                        lParams_lqtasm.weight = 0.4f;
                        lParams_lqtasm.width = 0;
                        LinearLayout.LayoutParams lParams_csm = (LinearLayout.LayoutParams) inputcsm.getLayoutParams();
                        lParams_csm.weight = 0.2f;
                        lParams_csm.width = 0;
                        LinearLayout.LayoutParams lParams_qtasm = (LinearLayout.LayoutParams) inputqsm.getLayoutParams();
                        lParams_qtasm.weight = 0.4f;
                        lParams_qtasm.width = 0;
                    }

                    LinearLayout lqtaomimp = new LinearLayout(FormDocPreferitiOrdineCliente.this.getApplicationContext());
                    lqtaomimp.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelqtaomimp = new TextView(FormDocPreferitiOrdineCliente.this);
                    tvlabelqtaomimp.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelqtaomimp.setText("Qtà omaggio imponibile:");
                    tvlabelqtaomimp.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelqtaomimp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    lqtaomimp.addView(tvlabelqtaomimp);
                    final EditText inputcomimp = new EditText(FormDocPreferitiOrdineCliente.this);
                    inputcomimp.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputcomimp.setHint("colli");
                    inputcomimp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    inputcomimp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputcomimp.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtaomimp.addView(inputcomimp);
                    final EditText inputqomimp = new EditText(FormDocPreferitiOrdineCliente.this);
                    inputqomimp.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputqomimp.setHint("quantità");
                    if (!FunzioniJBeerApp.umDecimale(rec.leggiStringa("artcod")))
                        inputqomimp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    else
                        inputqomimp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputqomimp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputqomimp.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtaomimp.addView(inputqomimp);
                    inputcomimp.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (!hasFocus) {
                                double col = Formattazione.estraiDouble(inputcomimp.getText().toString());
                                if (npezzi > 0) {
                                    double q = OpValute.arrotondaMat(col * npezzi, 2);
                                    inputqomimp.setText(Formattazione.formatta(q, "#####0.000", 0)
                                            .replace(",", "."));
                                }
                            }
                        }
                    });
                    boolean cbloccomimp = FunzioniJBeerApp.causaleBloccata(5, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                    if (!cbloccomimp) {
                        ll.addView(lqtaomimp);
                        LinearLayout.LayoutParams lParams_lqtaomimp = (LinearLayout.LayoutParams) tvlabelqtaomimp.getLayoutParams();
                        lParams_lqtaomimp.weight = 0.4f;
                        lParams_lqtaomimp.width = 0;
                        LinearLayout.LayoutParams lParams_comimp = (LinearLayout.LayoutParams) inputcomimp.getLayoutParams();
                        lParams_comimp.weight = 0.2f;
                        lParams_comimp.width = 0;
                        LinearLayout.LayoutParams lParams_qtaomimp = (LinearLayout.LayoutParams) inputqomimp.getLayoutParams();
                        lParams_qtaomimp.weight = 0.4f;
                        lParams_qtaomimp.width = 0;
                    }

                    LinearLayout lqtaomtot = new LinearLayout(FormDocPreferitiOrdineCliente.this.getApplicationContext());
                    lqtaomtot.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelqtaomtot = new TextView(FormDocPreferitiOrdineCliente.this);
                    tvlabelqtaomtot.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelqtaomtot.setText("Qtà omaggio totale:");
                    tvlabelqtaomtot.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelqtaomtot.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    lqtaomtot.addView(tvlabelqtaomtot);
                    final EditText inputcomtot = new EditText(FormDocPreferitiOrdineCliente.this);
                    inputcomtot.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputcomtot.setHint("colli");
                    inputcomtot.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    inputcomtot.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputcomtot.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtaomtot.addView(inputcomtot);
                    final EditText inputqomtot = new EditText(FormDocPreferitiOrdineCliente.this);
                    inputqomtot.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputqomtot.setHint("quantità");
                    if (!FunzioniJBeerApp.umDecimale(rec.leggiStringa("artcod")))
                        inputqomtot.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    else
                        inputqomtot.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputqomtot.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputqomtot.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtaomtot.addView(inputqomtot);
                    inputcomtot.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (!hasFocus) {
                                double col = Formattazione.estraiDouble(inputcomtot.getText().toString());
                                if (npezzi > 0) {
                                    double q = OpValute.arrotondaMat(col * npezzi, 2);
                                    inputqomtot.setText(Formattazione.formatta(q, "#####0.000", 0)
                                            .replace(",", "."));
                                }
                            }
                        }
                    });
                    boolean cbloccomtot = FunzioniJBeerApp.causaleBloccata(6, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                    if (!cbloccomtot) {
                        ll.addView(lqtaomtot);
                        LinearLayout.LayoutParams lParams_lqtaomtot = (LinearLayout.LayoutParams) tvlabelqtaomtot.getLayoutParams();
                        lParams_lqtaomtot.weight = 0.4f;
                        lParams_lqtaomtot.width = 0;
                        LinearLayout.LayoutParams lParams_comtot = (LinearLayout.LayoutParams) inputcomtot.getLayoutParams();
                        lParams_comtot.weight = 0.2f;
                        lParams_comtot.width = 0;
                        LinearLayout.LayoutParams lParams_qtaomtot = (LinearLayout.LayoutParams) inputqomtot.getLayoutParams();
                        lParams_qtaomtot.weight = 0.4f;
                        lParams_qtaomtot.width = 0;
                    }

                    LinearLayout lprz = new LinearLayout(FormDocPreferitiOrdineCliente.this.getApplicationContext());
                    lprz.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelprz = new TextView(FormDocPreferitiOrdineCliente.this);
                    tvlabelprz.setText("Prezzo:");
                    tvlabelprz.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelprz.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelprz.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    lprz.addView(tvlabelprz);

                    final EditText inputp = new EditText(FormDocPreferitiOrdineCliente.this);
                    inputp.setHint("prezzo");
                    inputp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputp.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputp.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    double prz = rec.leggiDouble("prezzo");
                    double sc1 = rec.leggiDouble("sc1");
                    double sc2 = rec.leggiDouble("sc2");
                    double sc3 = rec.leggiDouble("sc3");
                    if (Env.prezzopref && rec.leggiDouble("prezzocli") > 0)
                        prz = rec.leggiDouble("prezzocli");
                    inputp.setText(Formattazione.formatta(Math.abs(prz), "######0.000", Formattazione.SEGNO_DX));
                    lprz.addView(inputp);
                    LinearLayout.LayoutParams lParams_lprz = (LinearLayout.LayoutParams) tvlabelprz.getLayoutParams();
                    lParams_lprz.weight = 0.4f;
                    lParams_lprz.width = 0;
                    LinearLayout.LayoutParams lParams_prz = (LinearLayout.LayoutParams) inputp.getLayoutParams();
                    lParams_prz.weight = 0.6f;
                    lParams_prz.width = 0;
                    ll.addView(lprz);

                    LinearLayout lsc = new LinearLayout(FormDocPreferitiOrdineCliente.this.getApplicationContext());
                    lsc.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelsc = new TextView(FormDocPreferitiOrdineCliente.this);
                    tvlabelsc.setText("%sconto:");
                    tvlabelsc.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelsc.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelsc.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    lsc.addView(tvlabelsc);
                    final EditText inputsc1 = new EditText(FormDocPreferitiOrdineCliente.this);
                    inputsc1.setHint("");
                    inputsc1.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputsc1.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputsc1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputsc1.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    if (sc1 > 0)
                        inputsc1.setText(Formattazione.formatta(sc1, "##0.00", Formattazione.SEGNO_DX));
                    else
                        inputsc1.setText("");
                    lsc.addView(inputsc1);
                    final EditText inputsc2 = new EditText(FormDocPreferitiOrdineCliente.this);
                    inputsc2.setHint("");
                    inputsc2.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputsc2.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputsc2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputsc2.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    if (sc2 > 0)
                        inputsc2.setText(Formattazione.formatta(sc2, "##0.00", Formattazione.SEGNO_DX));
                    else
                        inputsc2.setText("");
                    lsc.addView(inputsc2);
                    final EditText inputsc3 = new EditText(FormDocPreferitiOrdineCliente.this);
                    inputsc3.setHint("");
                    inputsc3.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputsc3.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputsc3.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputsc3.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    if (sc3 > 0)
                        inputsc3.setText(Formattazione.formatta(sc3, "##0.00", Formattazione.SEGNO_DX));
                    else
                        inputsc3.setText("");
                    lsc.addView(inputsc3);
                    LinearLayout.LayoutParams lParams_lsc = (LinearLayout.LayoutParams) tvlabelsc.getLayoutParams();
                    lParams_lsc.weight = 0.4f;
                    lParams_lsc.width = 0;
                    LinearLayout.LayoutParams lParams_sc1 = (LinearLayout.LayoutParams) inputsc1.getLayoutParams();
                    lParams_sc1.weight = 0.2f;
                    lParams_sc1.width = 0;
                    LinearLayout.LayoutParams lParams_sc2 = (LinearLayout.LayoutParams) inputsc2.getLayoutParams();
                    lParams_sc2.weight = 0.2f;
                    lParams_sc2.width = 0;
                    LinearLayout.LayoutParams lParams_sc3 = (LinearLayout.LayoutParams) inputsc3.getLayoutParams();
                    lParams_sc3.weight = 0.2f;
                    lParams_sc3.width = 0;
                    ll.addView(lsc);

                    if (rec.leggiDouble("artprzacq") > 0 && Env.visprzacq) {
                        LinearLayout lpra = new LinearLayout(FormDocPreferitiOrdineCliente.this.getApplicationContext());
                        lpra.setOrientation(LinearLayout.HORIZONTAL);
                        final TextView tvlabelprzacq = new TextView(FormDocPreferitiOrdineCliente.this);
                        tvlabelprzacq.setText("Prezzo acquisto: ");
                        tvlabelprzacq.setTextColor(Color.rgb(5, 50, 73));
                        tvlabelprzacq.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                        tvlabelprzacq.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                        lpra.addView(tvlabelprzacq);
                        final TextView tvprzacq = new TextView(FormDocPreferitiOrdineCliente.this);
                        tvprzacq.setTypeface(tvprzacq.getTypeface(), Typeface.BOLD);
                        tvprzacq.setTextColor(Color.BLUE);
                        tvprzacq.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                        tvprzacq.setBackgroundDrawable(getResources().getDrawable(R.drawable.textview1));
                        tvprzacq.setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL);
                        tvprzacq.setText(Formattazione.formatta(rec.leggiDouble("artprzacq"), "#####0.000", Formattazione.NO_SEGNO));
                        lpra.addView(tvprzacq);
                        LinearLayout.LayoutParams lParams_lprzacq = (LinearLayout.LayoutParams) tvlabelprzacq.getLayoutParams();
                        lParams_lprzacq.weight = 0.4f;
                        lParams_lprzacq.width = 0;
                        lParams_lprzacq.height = LinearLayout.LayoutParams.MATCH_PARENT;
                        LinearLayout.LayoutParams lParams_przacq = (LinearLayout.LayoutParams) tvprzacq.getLayoutParams();
                        lParams_przacq.weight = 0.6f;
                        lParams_przacq.width = 0;
                        lParams_przacq.height = LinearLayout.LayoutParams.MATCH_PARENT;
                        ll.addView(lpra);
                    }
                    if (Env.visprzrif) {
                        LinearLayout lpr = new LinearLayout(FormDocPreferitiOrdineCliente.this.getApplicationContext());
                        lpr.setOrientation(LinearLayout.HORIZONTAL);
                        final TextView tvlabelprzrif = new TextView(FormDocPreferitiOrdineCliente.this);
                        tvlabelprzrif.setText("Prezzo base: ");
                        tvlabelprzrif.setTextColor(Color.rgb(5, 50, 73));
                        tvlabelprzrif.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                        tvlabelprzrif.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                        lpr.addView(tvlabelprzrif);
                        final TextView tvprzrif = new TextView(FormDocPreferitiOrdineCliente.this);
                        tvprzrif.setTypeface(tvprzrif.getTypeface(), Typeface.BOLD);
                        tvprzrif.setTextColor(Color.BLUE);
                        tvprzrif.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                        tvprzrif.setBackgroundDrawable(getResources().getDrawable(R.drawable.textview1));
                        tvprzrif.setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL);
                        tvprzrif.setVisibility(View.INVISIBLE);
                        if (rec.leggiDouble("przrif") > 0)
                            tvprzrif.setText(Formattazione.formatta(rec.leggiDouble("przrif"), "#####0.000", Formattazione.NO_SEGNO));
                        else
                            tvprzrif.setText("");
                        final ImageButton bvisprz = new ImageButton(FormDocPreferitiOrdineCliente.this);
                        bvisprz.setImageResource(R.drawable.ic_visibility_white_48dp);
                        bvisprz.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                        lpr.addView(bvisprz);
                        bvisprz.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                if (bvisprz.getTag() == null || (bvisprz.getTag() != null && bvisprz.getTag().equals("NOVIS"))) {
                                    tvprzrif.setVisibility(View.VISIBLE);
                                    bvisprz.setTag("VIS");
                                } else {
                                    tvprzrif.setVisibility(View.INVISIBLE);
                                    bvisprz.setTag("NOVIS");
                                }
                            }
                        });
                        lpr.addView(tvprzrif);
                        LinearLayout.LayoutParams lParams_lprzrif = (LinearLayout.LayoutParams) tvlabelprzrif.getLayoutParams();
                        lParams_lprzrif.weight = 0.4f;
                        lParams_lprzrif.width = 0;
                        lParams_lprzrif.height = LinearLayout.LayoutParams.MATCH_PARENT;
                        LinearLayout.LayoutParams lParams_przrif = (LinearLayout.LayoutParams) tvprzrif.getLayoutParams();
                        lParams_przrif.weight = 0.4f;
                        lParams_przrif.width = 0;
                        lParams_przrif.height = LinearLayout.LayoutParams.MATCH_PARENT;
                        LinearLayout.LayoutParams lParams_bvisprz = (LinearLayout.LayoutParams) bvisprz.getLayoutParams();
                        lParams_bvisprz.weight = 0.2f;
                        lParams_bvisprz.width = 0;
                        ll.addView(lpr);
                    }

                    final TextView tvinfo = new TextView(FormDocPreferitiOrdineCliente.this);
                    tvinfo.setTextColor(Color.RED);
                    tvinfo.setTypeface(tvinfo.getTypeface(), Typeface.BOLD);
                    tvinfo.setText("");
                    ll.addView(tvinfo);
                    LinearLayout lb = new LinearLayout(FormDocPreferitiOrdineCliente.this.getApplicationContext());
                    lb.setOrientation(LinearLayout.HORIZONTAL);
                    Button bdok = new Button(FormDocPreferitiOrdineCliente.this);
                    bdok.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                    bdok.setTextColor(Color.WHITE);
                    bdok.setText("OK");
                    lb.addView(bdok);
                    Button bdann = new Button(FormDocPreferitiOrdineCliente.this);
                    bdann.setText("Annulla");
                    bdann.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                    bdann.setTextColor(Color.WHITE);
                    lb.addView(bdann);
                    LinearLayout.LayoutParams lParams_bdok = (LinearLayout.LayoutParams) bdok.getLayoutParams();
                    lParams_bdok.weight = 0.5f;
                    lParams_bdok.width = 0;
                    LinearLayout.LayoutParams lParams_bdann = (LinearLayout.LayoutParams) bdann.getLayoutParams();
                    lParams_bdann.weight = 0.5f;
                    lParams_bdann.width = 0;
                    ll.addView(lb);

                    bdok.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            String valuecvend = inputcvend.getText().toString();
                            int cvend = Formattazione.estraiIntero(valuecvend);
                            String valueqvend = inputqvend.getText().toString();
                            double qvend = OpValute.arrotondaMat(Formattazione.estraiDouble(valueqvend.replace(".", ",")), 3);
                            String valuecsm = inputcsm.getText().toString();
                            int csm = Formattazione.estraiIntero(valuecsm);
                            String valueqsm = inputqsm.getText().toString();
                            double qsm = OpValute.arrotondaMat(Formattazione.estraiDouble(valueqsm.replace(".", ",")), 3);
                            String valuecomimp = inputcomimp.getText().toString();
                            int comimp = Formattazione.estraiIntero(valuecomimp);
                            String valueqomimp = inputqomimp.getText().toString();
                            double qomimp = OpValute.arrotondaMat(Formattazione.estraiDouble(valueqomimp.replace(".", ",")), 3);
                            String valuecomtot = inputcomtot.getText().toString();
                            int comtot = Formattazione.estraiIntero(valuecomtot);
                            String valueqomtot = inputqomtot.getText().toString();
                            double qomtot = OpValute.arrotondaMat(Formattazione.estraiDouble(valueqomtot.replace(".", ",")), 3);
                            if (cvend > 0 && qvend == 0 && npezzi > 0) {
                                qvend = OpValute.arrotondaMat(cvend * npezzi, 2);
                            }
                            if (csm > 0 && qsm == 0 && npezzi > 0) {
                                qsm = OpValute.arrotondaMat(csm * npezzi, 2);
                            }
                            if (comimp > 0 && qomimp == 0 && npezzi > 0) {
                                qomimp = OpValute.arrotondaMat(comimp * npezzi, 2);
                            }
                            if (comtot > 0 && qomtot == 0 && npezzi > 0) {
                                qomtot = OpValute.arrotondaMat(comtot * npezzi, 2);
                            }

                            String valuep = inputp.getText().toString();
                            double p = OpValute.arrotondaMat(Formattazione.estraiDouble(valuep.replace(".", ",")), 3);

                            String valuesc1 = inputsc1.getText().toString();
                            double s1 = OpValute.arrotondaMat(Formattazione.estraiDouble(valuesc1.replace(".", ",")), 3);
                            String valuesc2 = inputsc2.getText().toString();
                            double s2 = OpValute.arrotondaMat(Formattazione.estraiDouble(valuesc2.replace(".", ",")), 3);
                            String valuesc3 = inputsc3.getText().toString();
                            double s3 = OpValute.arrotondaMat(Formattazione.estraiDouble(valuesc3.replace(".", ",")), 3);

                            if (Env.maxscvend > 0) {
                                double prznet = p;
                                if (s1 > 0)
                                    prznet -= prznet * s1 / 100;
                                if (s2 > 0)
                                    prznet -= prznet * s2 / 100;
                                if (s3 > 0)
                                    prznet -= prznet * s3 / 100;
                                double diffprz = rec.leggiDouble("prezzonetto") - prznet;
                                if (diffprz > 0) {
                                    double percdiff = diffprz / rec.leggiDouble("prezzonetto") * 100;
                                    if (percdiff > Env.maxscvend) {
                                        tvinfo.setText("Il prezzo assegnato eccede lo sconto massimo del " + Formattazione.formValuta(Env.maxscvend, 3, 2, 0) + "% rispetto al prezzo di listino impostato dalla sede");
                                        return;
                                    }
                                }
                            }
                            if (qvend > 0) {
                                boolean cbloccvend = FunzioniJBeerApp.causaleBloccata(0, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                                if (cbloccvend) {
                                    tvinfo.setText("Vendita bloccata dalla sede");
                                    return;
                                }
                            }
                            if (qsm > 0) {
                                boolean cbloccsm = FunzioniJBeerApp.causaleBloccata(4, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                                if (cbloccsm) {
                                    tvinfo.setText("Sconto merce bloccata dalla sede");
                                    return;
                                }
                            }
                            if (qomimp > 0) {
                                boolean cbloccomimp = FunzioniJBeerApp.causaleBloccata(5, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                                if (cbloccomimp) {
                                    tvinfo.setText("Omaggio imponibile bloccato dalla sede");
                                    return;
                                }
                            }
                            if (qomtot > 0) {
                                boolean cbloccomtot = FunzioniJBeerApp.causaleBloccata(6, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                                if (cbloccomtot) {
                                    tvinfo.setText("Omaggio totale bloccato dalla sede");
                                    return;
                                }
                            }

                            if (Env.ctrprzrif && rec.leggiDouble("przrif") > 0 && p < rec.leggiDouble("przrif")) {
                                tvinfo.setText("ERRORE: Prezzo di vendita minore del prezzo di riferimento");
                                return;
                            } else {
                                rec.eliminaCampo("prezzo");
                                rec.insDouble("prezzo", p);
                                rec.eliminaCampo("sc1");
                                rec.insDouble("sc1", s1);
                                rec.eliminaCampo("sc2");
                                rec.insDouble("sc2", s2);
                                rec.eliminaCampo("sc3");
                                rec.insDouble("sc3", s3);
                                rec.eliminaCampo("collivend");
                                rec.insIntero("collivend", cvend);
                                rec.eliminaCampo("qtavend");
                                rec.insDouble("qtavend", qvend);
                                rec.eliminaCampo("collism");
                                rec.insIntero("collism", csm);
                                rec.eliminaCampo("qtasm");
                                rec.insDouble("qtasm", qsm);
                                rec.eliminaCampo("colliomimp");
                                rec.insIntero("colliomimp", comimp);
                                rec.eliminaCampo("qtaomimp");
                                rec.insDouble("qtaomimp", qomimp);
                                rec.eliminaCampo("colliomtot");
                                rec.insIntero("colliomtot", comtot);
                                rec.eliminaCampo("qtaomtot");
                                rec.insDouble("qtaomtot", qomtot);
                                rec.eliminaCampo("lotto");
                                rec.insStringa("lotto", "");
                                lprefadapter.notifyDataSetChanged();
                                optionDialog.dismiss();

                            }
                        }
                    });

                    bdann.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            optionDialog.dismiss();
                        }
                    });
                    optionDialog.setView(ll);
                    optionDialog.setCancelable(false);
                    optionDialog.show();
                }
            }
        });
    }

    private void aggiornaListaEvento() {
        // mem.qta già inserite in lista
        HashMap<String, Double> hqtavendold = new HashMap();
        HashMap<String, Double> hqtasmold = new HashMap();
        HashMap<String, Double> hqtaomimpold = new HashMap();
        HashMap<String, Double> hqtaomtotold = new HashMap();
        HashMap<String, Integer> hcollivendold = new HashMap();
        HashMap<String, Integer> hcollismold = new HashMap();
        HashMap<String, Integer> hcolliomimpold = new HashMap();
        HashMap<String, Integer> hcolliomtotold = new HashMap();
        for (int i = 0; i < vart.size(); i++) {
            Record r = vart.get(i);
            if (r.leggiIntero("collivend") > 0) {
                hcollivendold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("collivend")));
            }
            if (r.leggiDouble("qtavend") > 0) {
                hqtavendold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtavend")));
            }
            if (r.leggiIntero("collism") > 0) {
                hcollismold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("collism")));
            }
            if (r.leggiDouble("qtasm") > 0) {
                hqtasmold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtasm")));
            }
            if (r.leggiIntero("colliomimp") > 0) {
                hcolliomimpold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("colliomimp")));
            }
            if (r.leggiDouble("qtaomimp") > 0) {
                hqtaomimpold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtaomimp")));
            }
            if (r.leggiIntero("colliomtot") > 0) {
                hcolliomtotold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("colliomtot")));
            }
            if (r.leggiDouble("qtaomtot") > 0) {
                hqtaomtotold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtaomtot")));
            }
        }
        vart.clear();

        String c = edcod.getText().toString().trim();
        String d = eddescr.getText().toString().trim();
        c.replace("'", "''");
        d.replace("'", "''");
        if (ricstato.getSelectedItemPosition() == 0) {
            // caricamento preferiti
            String cc = clicod;
            if (!clicodpadre.equals(""))
                cc = clicodpadre;
            HashMap<String, String> hart = new HashMap();
            Cursor cursor = Env.db.rawQuery(
                    "SELECT artcod,artdescr,artum,artgiacenza,artcodiva,prefprezzo,artnotificaterm,artflagum,artpezzi,artweb_linkscheda,artprzacq,giacqta FROM articoli " +
                            "INNER JOIN preferiti " +
                            "ON articoli.artcod = preferiti.prefartcod LEFT JOIN giacart ON articoli.artcod = giacart.giacartcod " +
                            "LEFT JOIN catalago ON catartcod=artcod WHERE prefclicod = '" + cc + "' AND " +
                            "artcod <> ''  AND artstato = 'A' " +
                            (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                            (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                            " ORDER BY artcod", null);
            while (cursor.moveToNext()) {
                Record rx = new Record();
                hart.put(cursor.getString(0), "");
                rx.insStringa("artcod", cursor.getString(0));
                rx.insStringa("artdescr", cursor.getString(1));
                rx.insStringa("artum", cursor.getString(2));
                rx.insIntero("artflagum", cursor.getInt(7));
                rx.insStringa("artschedaweb", cursor.getString(9));
                rx.insDouble("artprzacq", cursor.getDouble(10));
                rx.insDouble("artpezzi", cursor.getDouble(8));
                rx.insStringa("codiva", cursor.getString(4));
                rx.insDouble("giacsede", cursor.getDouble(11));
                if (!clicod.equals("")) {
                    double przvend = 0;
                    double prznetto = 0;
                    double sc1 = 0, sc2 = 0, sc3 = 0;
//                    if (Env.prezzopref)
//                    {
//                        przvend = cursor.getDouble(5);
//                        prznetto = przvend;
//                    }
//                    if (przvend == 0)
                    {
                        double[] prz = FunzioniJBeerApp.calcoloPrezzoArticolo(cursor.getString(0), cc, false);
                        przvend = prz[3];
                        prznetto = przvend;
                        sc1 = prz[0];
                        sc2 = prz[1];
                        sc3 = prz[2];
                        if (prz[0] > 0)
                            prznetto = prznetto - (prznetto * prz[0] / 100);
                        if (prz[1] > 0)
                            prznetto = prznetto - (prznetto * prz[1] / 100);
                        if (prz[2] > 0)
                            prznetto = prznetto - (prznetto * prz[2] / 100);
                        prznetto = OpValute.arrotondaMat(prznetto, 3);
                    }
                    rx.insDouble("sc1", sc1);
                    rx.insDouble("sc2", sc2);
                    rx.insDouble("sc3", sc3);
                    rx.insDouble("prezzo", przvend);
                    rx.insDouble("prezzonetto", prznetto);
                    rx.insDouble("prezzocli", cursor.getDouble(5));
                } else {
                    rx.insDouble("sc1", 0);
                    rx.insDouble("sc2", 0);
                    rx.insDouble("sc3", 0);
                    rx.insDouble("prezzo", 0);
                    rx.insDouble("prezzonetto", 0);
                    rx.insDouble("prezzocli", 0);
                }
                double przrif = FunzioniJBeerApp.leggiPrezzoListinoBaseArticolo(cursor.getString(0));
                rx.insDouble("przrif", przrif);

                int cvend = 0;
                if (hcollivendold.get(cursor.getString(0)) != null)
                    cvend = hcollivendold.get(cursor.getString(0)).intValue();
                rx.insIntero("collivend", cvend);
                double qvend = 0;
                if (hqtavendold.get(cursor.getString(0)) != null)
                    qvend = hqtavendold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtavend", qvend);

                int csm = 0;
                if (hcollismold.get(cursor.getString(0)) != null)
                    csm = hcollismold.get(cursor.getString(0)).intValue();
                rx.insIntero("collism", csm);
                double qsm = 0;
                if (hqtasmold.get(cursor.getString(0)) != null)
                    qsm = hqtasmold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtasm", qsm);

                int comimp = 0;
                if (hcolliomimpold.get(cursor.getString(0)) != null)
                    comimp = hcolliomimpold.get(cursor.getString(0)).intValue();
                rx.insIntero("colliomimp", comimp);
                double qomimp = 0;
                if (hqtaomimpold.get(cursor.getString(0)) != null)
                    qomimp = hqtaomimpold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtaomimp", qomimp);

                int comtot = 0;
                if (hcolliomtotold.get(cursor.getString(0)) != null)
                    comtot = hcolliomtotold.get(cursor.getString(0)).intValue();
                rx.insIntero("colliomtot", comtot);
                double qomtot = 0;
                if (hqtaomtotold.get(cursor.getString(0)) != null)
                    qomtot = hqtaomtotold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtaomtot", qomtot);

                rx.insIntero("notifica", cursor.getInt(6));
                vart.add(rx);
            }
            cursor.close();
        } else {
            // nuovi, in offerta, prezzo variato ecc.
            String qry = "SELECT artcod,artdescr,artum,artgiacenza,artcodiva,artnotificaterm,artweb_linkscheda,artprzacq,giacqta " +
                    "FROM articoli LEFT JOIN giacart ON articoli.artcod = giacart.giacartcod " +
                    "LEFT JOIN catalago ON catartcod=artcod " +
                    " WHERE artcod <> ''  AND artstato = 'A' ";
            qry += " AND artnotificaterm = " + ricstato.getSelectedItemPosition();
            if (!c.equals("")) {
                qry += " AND artcod LIKE \"%" + c + "%\"";
            }
            if (!d.equals("")) {
                qry += " AND artdescr LIKE \"%" + d + "%\"";
            }
            qry += " ORDER BY artcod";
            Cursor ca = Env.db.rawQuery(qry, null);
            while (ca.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", ca.getString(0));
                rx.insStringa("artdescr", ca.getString(1));
                rx.insStringa("artum", ca.getString(2));
                rx.insStringa("artschedaweb", ca.getString(6));
                rx.insDouble("artprzacq", ca.getDouble(7));
                rx.insStringa("codiva", ca.getString(4));
                rx.insDouble("giacsede", ca.getDouble(8));
                if (!clicod.equals("")) {
                    String cc = clicod;
                    if (!clicodpadre.equals(""))
                        cc = clicodpadre;
                    double przvend = 0;
                    double prznetto = 0;
                    double sc1 = 0, sc2 = 0, sc3 = 0;
//                    if (Env.prezzopref)
//                    {
//                        przvend = FunzioniJBeerApp.leggiPrezzoDaPreferiti(ca.getString(0), cc);
//                        prznetto = przvend;
//                    }
//                    if (przvend == 0)
                    {
                        double[] prz = FunzioniJBeerApp.calcoloPrezzoArticolo(ca.getString(0), cc, false);
                        przvend = prz[3];
                        prznetto = przvend;
                        sc1 = prz[0];
                        sc2 = prz[1];
                        sc3 = prz[2];
                        if (prz[0] > 0)
                            prznetto = prznetto - (prznetto * prz[0] / 100);
                        if (prz[1] > 0)
                            prznetto = prznetto - (prznetto * prz[1] / 100);
                        if (prz[2] > 0)
                            prznetto = prznetto - (prznetto * prz[2] / 100);
                        prznetto = OpValute.arrotondaMat(prznetto, 3);
                    }
                    rx.insDouble("sc1", sc1);
                    rx.insDouble("sc2", sc2);
                    rx.insDouble("sc3", sc3);
                    rx.insDouble("prezzo", przvend);
                    rx.insDouble("prezzonetto", prznetto);
                    rx.insDouble("prezzocli", przvend);
                } else {
                    rx.insDouble("sc1", 0);
                    rx.insDouble("sc2", 0);
                    rx.insDouble("sc3", 0);
                    rx.insDouble("prezzo", 0);
                    rx.insDouble("prezzonetto", 0);
                    rx.insDouble("prezzocli", 0);
                }
                double przrif = FunzioniJBeerApp.leggiPrezzoListinoBaseArticolo(ca.getString(0));
                rx.insDouble("przrif", przrif);

                int cvend = 0;
                if (hcollivendold.get(ca.getString(0)) != null)
                    cvend = hcollivendold.get(ca.getString(0)).intValue();
                rx.insIntero("collivend", cvend);
                double qvend = 0;
                if (hqtavendold.get(ca.getString(0)) != null)
                    qvend = hqtavendold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtavend", qvend);

                int csm = 0;
                if (hcollismold.get(ca.getString(0)) != null)
                    csm = hcollismold.get(ca.getString(0)).intValue();
                rx.insIntero("collism", csm);
                double qsm = 0;
                if (hqtasmold.get(ca.getString(0)) != null)
                    qsm = hqtasmold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtasm", qsm);

                int comimp = 0;
                if (hcolliomimpold.get(ca.getString(0)) != null)
                    comimp = hcolliomimpold.get(ca.getString(0)).intValue();
                rx.insIntero("colliomimp", comimp);
                double qomimp = 0;
                if (hqtaomimpold.get(ca.getString(0)) != null)
                    qomimp = hqtaomimpold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtaomimp", qomimp);

                int comtot = 0;
                if (hcolliomtotold.get(ca.getString(0)) != null)
                    comtot = hcolliomtotold.get(ca.getString(0)).intValue();
                rx.insIntero("colliomtot", comtot);
                double qomtot = 0;
                if (hqtaomtotold.get(ca.getString(0)) != null)
                    qomtot = hqtaomtotold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtaomtot", qomtot);

                rx.insIntero("notifica", ca.getInt(5));
                vart.add(rx);
            }
            ca.close();
        }
        lprefadapter.notifyDataSetChanged();

    }

    @Override
    public void onBackPressed() {
        Uri codselez = Uri.parse("content://listaprefoc/ANNULLA");
        Intent result = new Intent(Intent.ACTION_PICK, codselez);
        setResult(RESULT_CANCELED, result);
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v("JAZZTV", "*************RESUME");
    }

    private void aggiornaLista() {
        // mem.qta già inserite in lista
        HashMap<String, Double> hqtavendold = new HashMap();
        HashMap<String, Double> hqtasmold = new HashMap();
        HashMap<String, Double> hqtaomimpold = new HashMap();
        HashMap<String, Double> hqtaomtotold = new HashMap();
        HashMap<String, Integer> hcollivendold = new HashMap();
        HashMap<String, Integer> hcollismold = new HashMap();
        HashMap<String, Integer> hcolliomimpold = new HashMap();
        HashMap<String, Integer> hcolliomtotold = new HashMap();
        for (int i = 0; i < vart.size(); i++) {
            Record r = vart.get(i);
            if (r.leggiIntero("collivend") > 0) {
                hcollivendold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("collivend")));
            }
            if (r.leggiDouble("qtavend") > 0) {
                hqtavendold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtavend")));
            }
            if (r.leggiIntero("collism") > 0) {
                hcollismold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("collism")));
            }
            if (r.leggiDouble("qtasm") > 0) {
                hqtasmold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtasm")));
            }
            if (r.leggiIntero("colliomimp") > 0) {
                hcolliomimpold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("colliomimp")));
            }
            if (r.leggiDouble("qtaomimp") > 0) {
                hqtaomimpold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtaomimp")));
            }
            if (r.leggiIntero("colliomtot") > 0) {
                hcolliomtotold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("colliomtot")));
            }
            if (r.leggiDouble("qtaomtot") > 0) {
                hqtaomtotold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtaomtot")));
            }
        }
        vart.clear();

        String c = edcod.getText().toString().trim();
        String d = eddescr.getText().toString().trim();
        c.replace("'", "''");
        d.replace("'", "''");
        if (ricstato.getSelectedItemPosition() == 0) {
            // caricamento preferiti
            String cc = clicod;
            if (!clicodpadre.equals(""))
                cc = clicodpadre;
            HashMap<String, String> hart = new HashMap();
            String qry = "";
            if (Env.dispart) {
                qry = "SELECT artcod,artdescr,artum,dispqta,artcodiva,prefprezzo,artnotificaterm,artflagum,artpezzi,artweb_linkscheda,artprzacq FROM articoli " +
                        "INNER JOIN preferiti " +
                        "ON articoli.artcod = preferiti.prefartcod LEFT JOIN dispart ON artcod=dispartcod " +
                        "LEFT JOIN catalogo ON catartcod=artcod WHERE prefclicod = '" + cc + "' AND " +
                        "artcod <> ''  AND artstato = 'A' " +
                        (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                        (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                        " ORDER BY artcod";

            } else {
                qry = "SELECT artcod,artdescr,artum,giacqta,artcodiva,prefprezzo,artnotificaterm,artflagum,artpezzi,artweb_linkscheda,artprzacq FROM articoli " +
                        "INNER JOIN preferiti " +
                        "ON articoli.artcod = preferiti.prefartcod LEFT JOIN giacart ON articoli.artcod = giacart.giacartcod " +
                        "LEFT JOIN catalogo ON catartcod=artcod WHERE prefclicod = '" + cc + "' AND " +
                        "artcod <> ''  AND artstato = 'A' " +
                        (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                        (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                        " ORDER BY artcod";
            }
            Cursor cursor = Env.db.rawQuery(qry, null);
            while (cursor.moveToNext()) {
                double vargiac = 0;
                Record rx = new Record();
                hart.put(cursor.getString(0), "");
                rx.insStringa("artcod", cursor.getString(0));
                rx.insStringa("artdescr", cursor.getString(1));
                rx.insStringa("artum", cursor.getString(2));
                rx.insIntero("artflagum", cursor.getInt(7));
                rx.insStringa("artschedaweb", cursor.getString(9));
                rx.insDouble("artprzacq", cursor.getDouble(10));
                rx.insDouble("artpezzi", cursor.getDouble(8));
                rx.insStringa("codiva", cursor.getString(4));
                String qrygiac = "SELECT movtipo,movdoc,movsez,movdata,movnum" +
                        " FROM movimenti WHERE movtrasf <> 'S' ORDER BY movtipo,movdata,movnum";
                Cursor cursorgiac = Env.db.rawQuery(qrygiac, null);
                while (cursorgiac.moveToNext()) {
                    String[] parsd = new String[6];
                    parsd[0] = "" + cursorgiac.getInt(0);
                    parsd[1] = cursorgiac.getString(1);
                    parsd[2] = cursorgiac.getString(2);
                    parsd[3] = cursorgiac.getString(3);
                    parsd[4] = "" + cursorgiac.getInt(4);
                    parsd[5] = cursor.getString(0);
                    int nriga = 1;
                    Cursor cd = Env.db.rawQuery(
                            "SELECT rmqta " +
                                    "FROM righemov WHERE rmmovtipo=? AND rmmovdoc=? AND rmmovsez=? AND rmmovdata=? AND rmmovnum=? AND rmartcod=? ORDER BY rmriga",
                            parsd);
                    while (cd.moveToNext()) {
                        vargiac = vargiac - cd.getDouble(0);
                    }
                    cd.close();
                }
                cursorgiac.close();
                rx.insDouble("giacsede", cursor.getDouble(3) + vargiac);
                rx.insDouble("giac", cursor.getDouble(3) + vargiac);
                if (!clicod.equals("")) {
                    double przvend = 0;
                    double prznetto = 0;
                    double sc1 = 0, sc2 = 0, sc3 = 0;
//                    if (Env.prezzopref)
//                    {
//                        przvend = cursor.getDouble(5);
//                        prznetto = przvend;
//                    }
//                    if (przvend == 0)
                    {
                        double[] prz = FunzioniJBeerApp.calcoloPrezzoArticolo(cursor.getString(0), cc, false);
                        przvend = prz[3];
                        prznetto = przvend;
                        sc1 = prz[0];
                        sc2 = prz[1];
                        sc3 = prz[2];
                        if (prz[0] > 0)
                            prznetto = prznetto - (prznetto * prz[0] / 100);
                        if (prz[1] > 0)
                            prznetto = prznetto - (prznetto * prz[1] / 100);
                        if (prz[2] > 0)
                            prznetto = prznetto - (prznetto * prz[2] / 100);
                        prznetto = OpValute.arrotondaMat(prznetto, 3);
                    }
                    rx.insDouble("sc1", sc1);
                    rx.insDouble("sc2", sc2);
                    rx.insDouble("sc3", sc3);
                    rx.insDouble("prezzo", przvend);
                    rx.insDouble("prezzonetto", prznetto);
                    rx.insDouble("prezzocli", cursor.getDouble(5));
                } else {
                    rx.insDouble("sc1", 0);
                    rx.insDouble("sc2", 0);
                    rx.insDouble("sc3", 0);
                    rx.insDouble("prezzo", 0);
                    rx.insDouble("prezzonetto", 0);
                    rx.insDouble("prezzocli", 0);
                }
                double przrif = FunzioniJBeerApp.leggiPrezzoListinoBaseArticolo(cursor.getString(0));
                rx.insDouble("przrif", przrif);
                int cvend = 0;
                if (hcollivendold.get(cursor.getString(0)) != null)
                    cvend = hcollivendold.get(cursor.getString(0)).intValue();
                rx.insIntero("collivend", cvend);
                double qvend = 0;
                if (hqtavendold.get(cursor.getString(0)) != null)
                    qvend = hqtavendold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtavend", qvend);

                int csm = 0;
                if (hcollismold.get(cursor.getString(0)) != null)
                    csm = hcollismold.get(cursor.getString(0)).intValue();
                rx.insIntero("collism", csm);
                double qsm = 0;
                if (hqtasmold.get(cursor.getString(0)) != null)
                    qsm = hqtasmold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtasm", qsm);

                int comimp = 0;
                if (hcolliomimpold.get(cursor.getString(0)) != null)
                    comimp = hcolliomimpold.get(cursor.getString(0)).intValue();
                rx.insIntero("colliomimp", comimp);
                double qomimp = 0;
                if (hqtaomimpold.get(cursor.getString(0)) != null)
                    qomimp = hqtaomimpold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtaomimp", qomimp);

                int comtot = 0;
                if (hcolliomtotold.get(cursor.getString(0)) != null)
                    comtot = hcolliomtotold.get(cursor.getString(0)).intValue();
                rx.insIntero("colliomtot", comtot);
                double qomtot = 0;
                if (hqtaomtotold.get(cursor.getString(0)) != null)
                    qomtot = hqtaomtotold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtaomtot", qomtot);

                rx.insIntero("notifica", cursor.getInt(6));

                vart.add(rx);
            }
            cursor.close();
        } else {
            // nuovi, in offerta, prezzo variato ecc.
            String qry = "SELECT artcod,artdescr,artum,giacqta,artcodiva,artnotificaterm,artweb_linkscheda,artprzacq " +
                    "FROM articoli LEFT JOIN giacart ON articoli.artcod = giacart.giacartcod " +
                    "LEFT JOIN catalogo ON catartcod=artcod " +
                    " WHERE artcod <> ''  AND artstato = 'A' ";
            qry += " AND artnotificaterm = " + ricstato.getSelectedItemPosition();
            if (!c.equals("")) {
                qry += " AND artcod LIKE \"%" + c + "%\"";
            }
            if (!d.equals("")) {
                qry += " AND artdescr LIKE \"%" + d + "%\"";
            }
            qry += " ORDER BY artcod";
            Cursor ca = Env.db.rawQuery(qry, null);
            while (ca.moveToNext()) {
                double vargiac = 0;
                Record rx = new Record();
                rx.insStringa("artcod", ca.getString(0));
                rx.insStringa("artdescr", ca.getString(1));
                rx.insStringa("artum", ca.getString(2));
                rx.insStringa("artschedaweb", ca.getString(6));
                rx.insDouble("artprzacq", ca.getDouble(7));
                rx.insStringa("codiva", ca.getString(4));
                String qrygiac = "SELECT movtipo,movdoc,movsez,movdata,movnum" +
                        " FROM movimenti WHERE movtrasf <> 'S' ORDER BY movtipo,movdata,movnum";
                Cursor cursorgiac = Env.db.rawQuery(qrygiac, null);
                while (cursorgiac.moveToNext()) {
                    String[] parsd = new String[6];
                    parsd[0] = "" + cursorgiac.getInt(0);
                    parsd[1] = cursorgiac.getString(1);
                    parsd[2] = cursorgiac.getString(2);
                    parsd[3] = cursorgiac.getString(3);
                    parsd[4] = "" + cursorgiac.getInt(4);
                    parsd[5] = ca.getString(0);
                    int nriga = 1;
                    Cursor cd = Env.db.rawQuery(
                            "SELECT rmqta " +
                                    "FROM righemov WHERE rmmovtipo=? AND rmmovdoc=? AND rmmovsez=? AND rmmovdata=? AND rmmovnum=? AND rmartcod=? ORDER BY rmriga",
                            parsd);
                    while (cd.moveToNext()) {
                        vargiac = vargiac - cd.getDouble(0);
                    }
                    cd.close();
                }
                cursorgiac.close();

                rx.insDouble("giacsede", ca.getDouble(3) + vargiac);
                if (!clicod.equals("")) {
                    String cc = clicod;
                    if (!clicodpadre.equals(""))
                        cc = clicodpadre;
                    double przvend = 0;
                    double prznetto = 0;
                    double sc1 = 0, sc2 = 0, sc3 = 0;
//                    if (Env.prezzopref)
//                    {
//                        przvend = FunzioniJBeerApp.leggiPrezzoDaPreferiti(ca.getString(0), cc);
//                        prznetto = przvend;
//                    }
//                    if (przvend == 0)
                    {
                        double[] prz = FunzioniJBeerApp.calcoloPrezzoArticolo(ca.getString(0), cc, false);
                        przvend = prz[3];
                        prznetto = przvend;
                        sc1 = prz[0];
                        sc2 = prz[1];
                        sc3 = prz[2];
                        if (prz[0] > 0)
                            prznetto = prznetto - (prznetto * prz[0] / 100);
                        if (prz[1] > 0)
                            prznetto = prznetto - (prznetto * prz[1] / 100);
                        if (prz[2] > 0)
                            prznetto = prznetto - (prznetto * prz[2] / 100);
                        prznetto = OpValute.arrotondaMat(prznetto, 3);
                    }
                    rx.insDouble("sc1", sc1);
                    rx.insDouble("sc2", sc2);
                    rx.insDouble("sc3", sc3);
                    rx.insDouble("prezzo", przvend);
                    rx.insDouble("prezzonetto", prznetto);
                    rx.insDouble("prezzocli", przvend);
                } else {
                    rx.insDouble("sc1", 0);
                    rx.insDouble("sc2", 0);
                    rx.insDouble("sc3", 0);
                    rx.insDouble("prezzo", 0);
                    rx.insDouble("prezzonetto", 0);
                    rx.insDouble("prezzocli", 0);
                }

                int cvend = 0;
                if (hcollivendold.get(ca.getString(0)) != null)
                    cvend = hcollivendold.get(ca.getString(0)).intValue();
                rx.insIntero("collivend", cvend);
                double qvend = 0;
                if (hqtavendold.get(ca.getString(0)) != null)
                    qvend = hqtavendold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtavend", qvend);

                int csm = 0;
                if (hcollismold.get(ca.getString(0)) != null)
                    csm = hcollismold.get(ca.getString(0)).intValue();
                rx.insIntero("collism", csm);
                double qsm = 0;
                if (hqtasmold.get(ca.getString(0)) != null)
                    qsm = hqtasmold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtasm", qsm);

                int comimp = 0;
                if (hcolliomimpold.get(ca.getString(0)) != null)
                    comimp = hcolliomimpold.get(ca.getString(0)).intValue();
                rx.insIntero("colliomimp", comimp);
                double qomimp = 0;
                if (hqtaomimpold.get(ca.getString(0)) != null)
                    qomimp = hqtaomimpold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtaomimp", qomimp);

                int comtot = 0;
                if (hcolliomtotold.get(ca.getString(0)) != null)
                    comtot = hcolliomtotold.get(ca.getString(0)).intValue();
                rx.insIntero("colliomtot", comtot);
                double qomtot = 0;
                if (hqtaomtotold.get(ca.getString(0)) != null)
                    qomtot = hqtaomtotold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtaomtot", qomtot);

                rx.insIntero("notifica", ca.getInt(5));
                vart.add(rx);
            }
            ca.close();
        }
        lprefadapter.notifyDataSetChanged();

    }

}
