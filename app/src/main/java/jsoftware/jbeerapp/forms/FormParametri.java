package jsoftware.jbeerapp.forms;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;


public class FormParametri extends AppCompatActivity {
    TextView tvversione;
    EditText edcodterm;
    EditText eduserjcloud;
    EditText edpwdjcloud;
    EditText edserverjcloud;
    EditText ednddtval;
    EditText ednddtqta;
    EditText ednfatt;
    EditText edncorrisp;
    EditText ednddtreso;
    EditText ednordcli;
    EditText edssidstamp;
    EditText edipstamp;
    EditText edssidscambiodati;
    EditText edipscambiodati;
    CheckBox cbcodartnum;
    CheckBox cbnoriccarichi;
    CheckBox cbricsolodest;
    //CheckBox cbnostampadocvend;
    //CheckBox cbnostampaddtcarico;
    //CheckBox cbtermordini;
    private EditText eddepsede;
    Button bok;

    ImageButton bwifistampante;
    ImageButton bwifiscambiodati;
    private Spinner sptiposcambiodati;
    private Spinner sptipostampante;
    EditText ednomebtstampante;
    private ArrayList<String> lsd;
    private ArrayList<String> lts;
    private CheckBox cbdispart;

    //LinearLayout llgestlotto;
    LinearLayout llddtreso;
    LinearLayout llddtqta;
    LinearLayout llddtval;
    LinearLayout llnumfatt;
    LinearLayout llnumcorr;

    ArrayList<String> vtl = new ArrayList();
    SpinnerAdapter ltdadapter = null;
    private Spinner spGestLotto;
    private CheckBox cbstampeveloci;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_parametri);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        tvversione = findViewById(R.id.par_versione);
        edcodterm = findViewById(R.id.par_codterm);
        eduserjcloud = findViewById(R.id.par_userjcloud);
        edpwdjcloud = findViewById(R.id.par_pwdjcloud);
        edserverjcloud = findViewById(R.id.par_serverjcloud);
        ednddtval = findViewById(R.id.par_nddtval);
        ednddtqta = findViewById(R.id.par_nddtqta);
        ednfatt = findViewById(R.id.par_nfatt);
        edncorrisp = findViewById(R.id.par_ncorrisp);
        ednddtreso = findViewById(R.id.par_nddtreso);
        //ednprenot = (EditText) findViewById(R.id.par_nprenot);
        ednordcli = findViewById(R.id.par_nordcli);
        edssidstamp = findViewById(R.id.par_ssidstampante);
        edipstamp = findViewById(R.id.par_ipstampante);
        eddepsede = findViewById(R.id.par_depsede);
        edssidscambiodati = findViewById(R.id.par_ssidscambiodati);
        edipscambiodati = findViewById(R.id.par_ipscambiodati);
        sptiposcambiodati = findViewById(R.id.par_tipoconnscambiodati);
        cbcodartnum = findViewById(R.id.par_codartnum);
        cbstampeveloci = (CheckBox) findViewById(R.id.par_stampeveloci);
        bok = findViewById(R.id.par_buttonOk);
        bwifistampante = findViewById(R.id.par_buttonRicercaReteWiFiStampante);
        bwifiscambiodati = findViewById(R.id.par_buttonRicercaReteWiFiScambioDati);
        sptipostampante = findViewById(R.id.par_tipostampante);
        ednomebtstampante = findViewById(R.id.par_nomebtstampante);
        cbnoriccarichi = findViewById(R.id.par_noriccarichi);
        cbricsolodest = findViewById(R.id.par_ricsolodest);
        llddtqta = findViewById(R.id.par_ll_ddt_qta);
        llddtqta.setVisibility(View.GONE);
        llddtreso = findViewById(R.id.par_ll_ddt_reso);
        llddtreso.setVisibility(View.GONE);
        llddtval = findViewById(R.id.par_ll_ddt_val);
        llddtval.setVisibility(View.GONE);
        llnumcorr = findViewById(R.id.par_ll_num_corr);
        llnumcorr.setVisibility(View.GONE);
        llnumfatt = findViewById(R.id.par_ll_num_fatt);
        llnumfatt.setVisibility(View.GONE);


        cbdispart = findViewById(R.id.par_visdispart);
        //llgestlotto = findViewById(R.id.par_gestlotto);
        spGestLotto = findViewById(R.id.par_lottogest);
        tvversione.setText("JBeerGO v." + Env.versione);

        lsd = new ArrayList();
        lsd.add("SIM Dati");
        lsd.add("WI-FI");
        sptiposcambiodati.setAdapter(new SpinnerAdapter(FormParametri.this.getApplicationContext(), R.layout.spinner_item, lsd));

        lts = new ArrayList();
        lts.add("SEWOO LK-P30 3p WI-FI");
        lts.add("SEWOO LK-P41 4p BLUETOOTH");
        lts.add("SEWOO LK-P30 3p BLUETOOTH");
        sptipostampante.setAdapter(new SpinnerAdapter(FormParametri.this.getApplicationContext(), R.layout.spinner_item, lts));

/*        if (Env.dispart) {
            llgestlotto.setVisibility(View.GONE);
        } else {
            llgestlotto.setVisibility(View.VISIBLE);
        }*/

        vtl = new ArrayList();
        vtl.add("No");
        vtl.add("Si");
        ltdadapter = new SpinnerAdapter(FormParametri.this.getApplicationContext(), R.layout.spinner_item, vtl);
        spGestLotto.setAdapter(ltdadapter);

/*
        cbdispart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    llgestlotto.setVisibility(View.GONE);
                } else {
                    llgestlotto.setVisibility(View.VISIBLE);
                }
            }
        });
*/

        edcodterm.setText(Env.pedcod);
        Cursor cursorpar = Env.db
                .rawQuery(
                        "SELECT user,pwd,serverjcloud,gprs_postserver,pedcod FROM datiterm",
                        null);
        cursorpar.moveToFirst();
        String user = cursorpar.getString(0);
        String pwd = cursorpar.getString(1);
        String server = cursorpar.getString(2);
        //String depsede = cursorpar.getString(3);
        String termcod = cursorpar.getString(4);
        cursorpar.close();
        edcodterm.setText(termcod);
        eduserjcloud.setText(user);
        edpwdjcloud.setText(pwd);
        edserverjcloud.setText(server);
        ednddtval.setText("" + Env.numbollaval);
        ednddtqta.setText("" + Env.numbollaqta);
        ednfatt.setText("" + Env.numfattura);
        edncorrisp.setText("" + Env.numcorrisp);
        ednddtreso.setText("" + Env.numddtreso);
        ednordcli.setText("" + Env.numordinecli);
        edssidstamp.setText("" + Env.ssidstampante);
        edipstamp.setText("" + Env.ipstampante);
        sptiposcambiodati.setSelection(Env.tiposcambiodati);
        edssidscambiodati.setText("" + Env.ssidscambiodati);
        edipscambiodati.setText("" + Env.ipscambiodati);
        eddepsede.setText("" + Env.gprs_postserver);
        cbcodartnum.setChecked(Env.codartnum);
        sptipostampante.setSelection(Env.tipostampante);
        ednomebtstampante.setText("" + Env.nomebtstampante);
        cbstampeveloci.setChecked(FunzioniJBeerApp.leggiProprieta("stampeveloci").equals("S"));
        cbnoriccarichi.setChecked(Env.noriccarichi);
        cbricsolodest.setChecked(Env.ricsolodest);
        //cbnostampadocvend.setChecked(Env.nostampadocvend);
        //cbnostampaddtcarico.setChecked(Env.nostampaddtcarico);
        //cbtermordini.setChecked(Env.termordini);
        cbdispart.setChecked(Env.dispart);
        //if (!cbdispart.isChecked()) {
            spGestLotto.setSelection(Env.gestlotti ? 1 : 0);
/*        } else {
            spGestLotto.setSelection(0);
        }*/
        bok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (edcodterm.getText().toString().equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Indicare il codice terminale")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            edcodterm.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (eduserjcloud.getText().toString().equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Indicare nome utente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            eduserjcloud.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (edpwdjcloud.getText().toString().equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Indicare password utente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            edpwdjcloud.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (edserverjcloud.getText().toString().equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Indicare URL server J-Cloud")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            edserverjcloud.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (eddepsede.getText().toString().equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Indicare il codice postazione sede")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            eddepsede.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (Formattazione.estraiIntero(ednddtval.getText().toString()) == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Indicare numero progressivo ddt valorizzati")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            ednddtval.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (Formattazione.estraiIntero(ednddtqta.getText().toString()) == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Indicare numero progressivo ddt a quantità")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            ednddtqta.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (Formattazione.estraiIntero(ednfatt.getText().toString()) == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Indicare numero progressivo fatture")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            ednfatt.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (Formattazione.estraiIntero(edncorrisp.getText().toString()) == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Indicare numero progressivo corrispettivi/ric.fiscali")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            edncorrisp.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (Formattazione.estraiIntero(ednddtreso.getText().toString()) == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Indicare numero progressivo ddt di reso")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            ednddtreso.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (Formattazione.estraiIntero(ednordcli.getText().toString()) == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Indicare numero progressivo ordini clienti")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            ednordcli.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (sptiposcambiodati.getSelectedItemPosition() == 1 &&
                        edssidscambiodati.getText().toString().trim().equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Indicare i dati dell'SSID scambio dati")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else {
                    SQLiteStatement stpar = Env.db.compileStatement("UPDATE datiterm SET pedcod=?,user=?,pwd=?,serverjcloud=?,numbollaval=?,numbollaqta=?,numfattura=?,numcorrisp=?,numddtreso=?,numddtcarico=?," +
                            "numscaricosede=?,numtrasfamezzo=?,numtrasfdamezzo=?,numordinesede=?,gprs_postserver=?,ssidstampante=?,ipstampante=?," +
                            "tiposcambiodati=?,ssidscambiodati=?,ipscambiodati=?,tipostampante=?,nomebtstampante=?,numinv=?,noriccarichi=?,numordinecli=?,dispart=?,gestlotti=?");
                    stpar.clearBindings();
                    stpar.bindString(1, edcodterm.getText().toString().trim());
                    stpar.bindString(2, eduserjcloud.getText().toString().trim());
                    stpar.bindString(3, edpwdjcloud.getText().toString().trim());
                    stpar.bindString(4, edserverjcloud.getText().toString().trim());
                    stpar.bindLong(5, Formattazione.estraiIntero(ednddtval.getText().toString().trim()));
                    stpar.bindLong(6, Formattazione.estraiIntero(ednddtqta.getText().toString().trim()));
                    stpar.bindLong(7, Formattazione.estraiIntero(ednfatt.getText().toString().trim()));
                    stpar.bindLong(8, Formattazione.estraiIntero(edncorrisp.getText().toString().trim()));
                    stpar.bindLong(9, Formattazione.estraiIntero(ednddtreso.getText().toString().trim()));
                    stpar.bindLong(10, 1);
                    stpar.bindLong(11, 1);
                    stpar.bindLong(12, 1);
                    stpar.bindLong(13, 1);
                    stpar.bindLong(14, 1);
                    stpar.bindString(15, eddepsede.getText().toString().trim());
                    stpar.bindString(16, edssidstamp.getText().toString().trim());
                    stpar.bindString(17, edipstamp.getText().toString().trim());
                    stpar.bindLong(18, sptiposcambiodati.getSelectedItemPosition());
                    stpar.bindString(19, edssidscambiodati.getText().toString().trim());
                    stpar.bindString(20, edipscambiodati.getText().toString().trim());
                    stpar.bindLong(21, sptipostampante.getSelectedItemPosition());
                    stpar.bindString(22, ednomebtstampante.getText().toString().trim());
                    stpar.bindLong(23, 1);
                    stpar.bindString(24, cbnoriccarichi.isChecked() ? "S" : "N");
                    stpar.bindLong(25, Formattazione.estraiIntero(ednordcli.getText().toString().trim()));
                    stpar.bindString(26, cbdispart.isChecked() ? "S" : "N");
                    //if (!cbdispart.isChecked()) {
                        stpar.bindLong(27, spGestLotto.getSelectedItemPosition());
                    //} else {
                    //    stpar.bindLong(27, 0);
                    //}
                    stpar.execute();
                    stpar.close();
                    Env.pedcod = edcodterm.getText().toString().trim();
                    Env.ipServerJCloud = edserverjcloud.getText().toString().trim();
                    Env.numbollaval = Formattazione.estraiIntero(ednddtval.getText().toString().trim());
                    Env.numbollaqta = Formattazione.estraiIntero(ednddtqta.getText().toString().trim());
                    Env.numfattura = Formattazione.estraiIntero(ednfatt.getText().toString().trim());
                    Env.numcorrisp = Formattazione.estraiIntero(edncorrisp.getText().toString().trim());
                    Env.numddtreso = Formattazione.estraiIntero(ednddtreso.getText().toString().trim());
                    Env.numordinecli = Formattazione.estraiIntero(ednordcli.getText().toString().trim());
                    Env.ssidstampante = edssidstamp.getText().toString().trim();
                    Env.ipstampante = edipstamp.getText().toString().trim();
                    Env.tiposcambiodati = sptiposcambiodati.getSelectedItemPosition();
                    Env.ssidscambiodati = edssidscambiodati.getText().toString().trim();
                    Env.ipscambiodati = edipscambiodati.getText().toString().trim();
                    Env.tipostampante = sptipostampante.getSelectedItemPosition();
                    Env.nomebtstampante = ednomebtstampante.getText().toString().trim();

                    FunzioniJBeerApp.impostaProprieta("codartnum", cbcodartnum.isChecked() ? "S" : "N");
                    Env.codartnum = cbcodartnum.isChecked();

                    FunzioniJBeerApp.impostaProprieta("ricsolodest", cbricsolodest.isChecked() ? "S" : "N");
                    Env.ricsolodest = cbricsolodest.isChecked();

                    FunzioniJBeerApp.impostaProprieta("nostampadocvend", "S");
                    Env.nostampadocvend = true;
                    //FunzioniJBeerApp.impostaProprieta("nostampadocvend", cbnostampadocvend.isChecked() ? "S" : "N");
                    //Env.nostampadocvend = cbnostampadocvend.isChecked();

                    FunzioniJBeerApp.impostaProprieta("nostampaddtcarico", "S");
                    Env.nostampaddtcarico = true;
                    //FunzioniJBeerApp.impostaProprieta("nostampaddtcarico", cbnostampaddtcarico.isChecked() ? "S" : "N");
                    //Env.nostampaddtcarico = cbnostampaddtcarico.isChecked();

                    //FunzioniJBeerApp.impostaProprieta("termordini", cbtermordini.isChecked() ? "S" : "N");
                    FunzioniJBeerApp.impostaProprieta("termordini", "S");

                    FunzioniJBeerApp.impostaProprieta("stampeveloci", cbstampeveloci.isChecked() ? "S" : "N");
                    if (cbstampeveloci.isChecked()) {
                        Env.stampe_par1 = 10;
                    } else {
                        Env.stampe_par1 = 4;
                    }

                    //Env.termordini = cbtermordini.isChecked();
                    Env.termordini = true;

                    Env.noriccarichi = (cbnoriccarichi.isChecked());
                    Env.dispart = (cbdispart.isChecked());
                    //if (!cbdispart.isChecked()) {
                        Env.gestlotti = spGestLotto.getSelectedItemPosition() == 0 ? false : true;
/*                    } else {
                        Env.gestlotti = false;
                    }*/
                    Toast toast = Toast.makeText(getApplicationContext(), "Parametri salvati", Toast.LENGTH_SHORT);
                    toast.show();
                    finish();
                }
            }
        });

        bwifistampante.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!FunzioniJBeerApp.tipoConnessione(getApplicationContext()).equals("WIFI")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Attivare la WI-FI")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            ednfatt.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                } else {
                    Intent i = new Intent(getApplicationContext(), FormElencoRetiWIFI.class);
                    startActivityForResult(i, 1);
                }
            }
        });

        bwifiscambiodati.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!FunzioniJBeerApp.tipoConnessione(getApplicationContext()).equals("WIFI")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormParametri.this);
                    builder.setMessage("Attivare la WI-FI")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            ednfatt.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                } else {
                    Intent i = new Intent(getApplicationContext(), FormElencoRetiWIFI.class);
                    startActivityForResult(i, 2);
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                // ricerca reti wifi stampante
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    String ssid = ris.getLastPathSegment();
                    if (!ssid.equals("")) {
                        edssidstamp.setText(ssid);
                    }
                }
                break;
            }
            case (2): {
                // ricerca reti wifi scambio dati
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    String ssid = ris.getLastPathSegment();
                    if (!ssid.equals("")) {
                        edssidscambiodati.setText(ssid);
                    }
                }
                break;
            }

        }
    }
}
