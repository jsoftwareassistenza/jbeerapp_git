package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaClientiDocAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;

public class FormRicercaClienti extends AppCompatActivity {
    private EditText riccod;
    private EditText ricnome;
    private ListView listacli;
    //private Spinner spgiri;
    private ArrayList<HashMap> vcli;
    private ListaClientiDocAdapter lcliadapter;
    private ImageButton bvoice;
    private boolean solodest = false;
    private boolean nodest = false;
    private int posselez = -1;
    private EditText ricinsegna;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_ricerca_clienti);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("solodest"))
            solodest = getIntent().getExtras().getBoolean("solodest");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("nodest"))
            nodest = getIntent().getExtras().getBoolean("nodest");

        riccod = findViewById(R.id.riccli_riccod);
        ricnome = findViewById(R.id.riccli_ricnome);
        listacli = findViewById(R.id.riccli_listacli);
        bvoice = findViewById(R.id.riccli_buttonVoice);
        ricinsegna = findViewById(R.id.riccli_ricinsegna);
        //spgiri = (Spinner) findViewById(R.id.riccli_giro);

        registerForContextMenu(listacli);

        bvoice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Nome cliente...");
                startActivityForResult(intent, 1);
            }
        });

/*		ricnome.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				aggiornaLista();
				return false;
			}
		});*/

        ricnome.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                aggiornaLista();
            }
        });

/*        riccod.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                aggiornaLista();
                return false;
            }
        });*/

        riccod.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                aggiornaLista();
            }
        });

        ricinsegna.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                aggiornaLista();
            }
        });

        listacli.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                String tmp = (String) vcli.get(arg2).get("codice");
                Uri codselez = Uri.parse("content://riccli/" + tmp.replace('/', '='));
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                result.putExtra("bloccato", (String) vcli.get(arg2).get("bloccato"));
                setResult(RESULT_OK, result);
                finish();
            }
        });

        listacli.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
                posselez = arg2;
                return false;
            }
        });


        vcli = new ArrayList<HashMap>();
        lcliadapter = new ListaClientiDocAdapter(this.getApplicationContext(), vcli, this);
        listacli.setAdapter(lcliadapter);

        aggiornaLista();

        ricnome.requestFocus();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(null);
        //menu.add(0, v.getId(), 0, "Annulla sospeso");
        menu.add(0, v.getId(), 0, "Seleziona cliente");
        menu.add(0, v.getId(), 1, "Apri posizione GPS");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.form_ricerca_clienti, menu);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                if (resultCode == Activity.RESULT_OK) {
                    ArrayList<String> matches = data.getStringArrayListExtra(
                            RecognizerIntent.EXTRA_RESULTS);
                    //String voice = ris.getLastPathSegment();
                    if (matches != null && matches.size() > 0)
                        ricnome.setText(matches.get(0).toLowerCase());
                }
                break;
            }
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals("Seleziona cliente")) {
            String tmp = (String) vcli.get(posselez).get("codice");
            Uri codselez = Uri.parse("content://riccli/" + tmp);
            Intent result = new Intent(Intent.ACTION_PICK, codselez);
            result.putExtra("bloccato", (String) vcli.get(posselez).get("bloccato"));
            setResult(RESULT_OK, result);
            finish();
        } else if (item.getTitle().equals("Apri posizione GPS")) {
            Intent geointent = new Intent(Intent.ACTION_VIEW);
            geointent.setData(Uri.parse((String) vcli.get(posselez).get("geouri")));
            if (geointent.resolveActivity(this.getPackageManager()) != null) {
                this.startActivity(geointent);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        this);
                builder.setMessage("Dati posizione non visualizzabili")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
            }
        } else {
            return false;
        }
        return true;
    }


    private void aggiornaLista() {
        vcli.clear();
        String c = riccod.getText().toString().trim();
        c.replace("'", "''");
        String n = ricnome.getText().toString().trim();
        n.replace("'", "''");
        String in = ricinsegna.getText().toString().trim();
        in.replace("'", "''");

        String qry = "SELECT clicodice,clinome,cliindir,cliloc,clicodpadre,cligpsnordsud,cligpsestovest,cligpsnordgradi,cligpsnordmin,cligpsnordsec,cligpsestgradi,cligpsestmin," +
                "cligpsestsec,clinomepri,clicognomepri,cliinsegna FROM clienti WHERE clicodice <> '' AND clicodpadre = ''";
        if (!c.equals(""))
            qry += " AND clicodice LIKE \"%" + c + "%\"";
        if (!n.equals(""))
            qry += " AND (clinome LIKE \"%" + n + "%\" OR clinomepri LIKE \"%" + n + "%\" OR clicognomepri LIKE \"%" + n + "%\")";
        if (!in.equals(""))
            qry += " AND cliinsegna LIKE \"%" + in + "%\"";
        qry += " ORDER BY clinome LIMIT 100";
        Cursor cursor = Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            boolean cliok = true;
            HashMap h = new HashMap();
            String cc = cursor.getString(0);
            if (!cursor.getString(4).equals(""))
                cc = cursor.getString(4);
            h.put("codice", cursor.getString(0));
            if (!cursor.getString(1).equals("")) {
                h.put("nome", cursor.getString(1));
            } else {
                h.put("nome", cursor.getString(13) + " " + cursor.getString(14));
            }
            h.put("indirizzo", cursor.getString(2));
            h.put("localita", cursor.getString(3));
            if (cursor.getDouble(7) > 0 || cursor.getDouble(8) > 0 || cursor.getDouble(9) > 0 || cursor.getDouble(10) > 0 || cursor.getDouble(11) > 0 || cursor.getDouble(12) > 0) {
                double gpsn = cursor.getDouble(7) + (cursor.getDouble(8) / 60) + (cursor.getDouble(9) / 3600);
                double gpse = cursor.getDouble(10) + (cursor.getDouble(11) / 60) + (cursor.getDouble(12) / 3600);
                String geouri = "geo:" + Formattazione.formatta(gpsn, "####0.000000", 1).replaceAll(",", ".") + "," +
                        Formattazione.formatta(gpse, "####0.000000", 1).replaceAll(",", ".");
                h.put("geouri", geouri);
                h.put("gpsn", gpsn);
                h.put("gpse", gpse);
            } else {
                h.put("geouri", "");
                h.put("gpsn", new Double(0));
                h.put("gpse", new Double(0));
            }
            h.put("scoperti", FunzioniJBeerApp.totaleScopertiCliente(cc));
            boolean bloccato = false;
            double impscadscadute = 0;
            if (Env.nscadblocco > 0) {
                double[] bloc = FunzioniJBeerApp.scadenzeScadute(cc, (new Data()), Env.ggtollblocco);
                bloccato = (bloc[1] > 0 && bloc[0] > 0 && bloc[0] > Env.impblocco);
                impscadscadute = bloc[0];
                h.put("bloccato", bloccato ? "S" : "N");
                if (bloccato)
                    h.put("impscaduto", impscadscadute);
                else
                    h.put("impscaduto", (double) 0);
            } else {
                h.put("bloccato", "N");
                h.put("impscaduto", (double) 0);
            }
            if (!cursor.getString(4).equals("")) {
                String[] parcp = new String[1];
                parcp[0] = cursor.getString(4);
                Cursor ccp = Env.db.rawQuery("SELECT clinome FROM clienti WHERE clicodice = ?", parcp);
                if (ccp.moveToFirst()) {
                    h.put("codpadre", cursor.getString(4));
                    h.put("descrpadre", ccp.getString(0));
                } else {
                    h.put("codpadre", "");
                    h.put("descrpadre", "");
                }
                ccp.close();
            } else {
                h.put("codpadre", "");
                h.put("descrpadre", "");
            }
            if (cliok)
                vcli.add(h);
        }
        cursor.close();
        lcliadapter.notifyDataSetChanged();
    }
}
