package jsoftware.jbeerapp.forms;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.Record;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentStatRiepilogo.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentStatRiepilogo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentStatRiepilogo extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private TextView tvultimoddt;
    private TextView tvultimoddtqta;
    private TextView tvultimafatt;
    private TextView tvultimoordcli;
    private TextView tvdocmax;
    private TextView tvtotvend;
    private TextView tvordcli;
    private TextView tartmax;

    private String datainizio = "";
    private String datafine = "";

    public FragmentStatRiepilogo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentStatRiepilogo.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentStatRiepilogo newInstance(String param1, String param2) {
        FragmentStatRiepilogo fragment = new FragmentStatRiepilogo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_stat_riepilogo, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String azione);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvultimoddt = view.findViewById(R.id.statriep_ultimoddt);
        tvultimoddtqta = view.findViewById(R.id.statriep_ultimoddtqta);
        tvultimafatt = view.findViewById(R.id.statriep_ultimafatt);
        tvultimoordcli = view.findViewById(R.id.statriep_ultimoordcli);
        tvdocmax = view.findViewById(R.id.statriep_docmax);
        tvtotvend = view.findViewById(R.id.statriep_totvend);
        tvordcli = view.findViewById(R.id.statriep_totordcli);
        tartmax = view.findViewById(R.id.statriep_artmax);
    }

    private void calcolaDati() {
        Record r = FunzioniJBeerApp.ultimoDDTVendita(datainizio, datafine);
        if (r != null) {
            tvultimoddt.setText("N." + r.leggiIntero("numero") + " del " + (new Data(r.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") +
                    " cliente:" + r.leggiStringa("clinome") + " tot.imp:" + Formattazione.formValuta(r.leggiDouble("imponibile"), 12, 2, 1));
        } else {
            tvultimoddt.setText("");
        }
        r = FunzioniJBeerApp.ultimoDDTQuantita(datainizio, datafine);
        if (r != null) {
            tvultimoddtqta.setText("N." + r.leggiIntero("numero") + " del " + (new Data(r.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") +
                    " cliente:" + r.leggiStringa("clinome") + " tot.imp:" + Formattazione.formValuta(r.leggiDouble("imponibile"), 12, 2, 1));
        } else {
            tvultimoddtqta.setText("");
        }
        r = FunzioniJBeerApp.ultimaFattura(datainizio, datafine);
        if (r != null) {
            tvultimafatt.setText("N." + r.leggiIntero("numero") + " del " + (new Data(r.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") +
                    " cliente:" + r.leggiStringa("clinome") + " tot.imp:" + Formattazione.formValuta(r.leggiDouble("imponibile"), 12, 2, 1));
        } else {
            tvultimafatt.setText("");
        }
        r = FunzioniJBeerApp.ultimoOrdineCliente(datainizio, datafine);
        if (r != null) {
            tvultimoordcli.setText("N." + r.leggiIntero("numero") + " del " + (new Data(r.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") +
                    " cliente:" + r.leggiStringa("clinome") + " tot.imp:" + Formattazione.formValuta(r.leggiDouble("imponibile"), 12, 2, 1));
        } else {
            tvultimoordcli.setText("");
        }
        r = FunzioniJBeerApp.documentoImportoMax(datainizio, datafine);
        if (r != null) {
            tvdocmax.setText(r.leggiStringa("tipo") + " N." + r.leggiIntero("numero") + " del " + (new Data(r.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") +
                    " cliente:" + r.leggiStringa("clinome") + " tot.imp:" + Formattazione.formValuta(r.leggiDouble("imponibile"), 12, 2, 1));
        } else {
            tvdocmax.setText("");
        }
        double tv = FunzioniJBeerApp.totaleVenduto(datainizio, datafine);
        tvtotvend.setText(Formattazione.formValuta(tv, 12, 2, 1));
        double to = FunzioniJBeerApp.totaleOrdiniClienti(datainizio, datafine);
        tvordcli.setText(Formattazione.formValuta(to, 12, 2, 1));
        ArrayList<Record> vart = FunzioniJBeerApp.vendutoArticoli(datainizio, datafine, "", 1);
        if (vart != null && vart.size() > 0) {
            Record ra = vart.get(0);
            tartmax.setText(ra.leggiStringa("artcod") + "-" + ra.leggiStringa("artdescr") + " valore:" + Formattazione.formValuta(ra.leggiDouble("val"), 12, 2, 1));
        } else {
            tartmax.setText("");
        }
    }

    public void impostaFiltri(String dti, String dtf) {
        datainizio = dti;
        datafine = dtf;
    }

    @Override
    public void onStart() {
        super.onStart();
        System.out.println("RIEP START " + datainizio + " " + datafine);
        calcolaDati();
    }
}
