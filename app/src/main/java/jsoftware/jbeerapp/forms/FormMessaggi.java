package jsoftware.jbeerapp.forms;

import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaMsgAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.Record;

public class FormMessaggi extends AppCompatActivity {
    private TextView tvinfo;
    private ListView listamsg;
    private ListaMsgAdapter ladapter;
    private ArrayList<Record> vmsg = null;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_messaggi);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        tvinfo = findViewById(R.id.msg_info);
        listamsg = findViewById(R.id.msg_lista);

        vmsg = FunzioniJBeerApp.leggiMessaggi();
        if (vmsg.size() == 1)
            tvinfo.setText("C'è un messaggio da leggere");
        else
            tvinfo.setText("Ci sono " + vmsg.size() + " messaggi da leggere");
        ladapter = new ListaMsgAdapter(this.getApplicationContext(), vmsg, this);
        listamsg.setAdapter(ladapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.msg_menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.msg_action_ok) {
            // imposta messaggi come letti
            Data oggi = new Data();
            String dtoggi = oggi.formatta(Data.AAAA_MM_GG, "-");
            SQLiteStatement stmt = Env.db.compileStatement("INSERT INTO vismessaggi ("
                    + "vmsgdata,"
                    + "vmsgora"
                    + ") "
                    + "VALUES (?,?)");
            for (int i = 0; i < vmsg.size(); i++) {
                Record rmsg = vmsg.get(i);
                stmt.clearBindings();
                stmt.bindString(1, dtoggi);
                stmt.bindString(2, rmsg.leggiStringa("ora"));
                stmt.execute();
            }
            stmt.close();
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }
}
