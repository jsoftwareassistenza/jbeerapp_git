package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaVisGiacAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.Record;

public class FormVisGiac extends AppCompatActivity {
    private ArrayList<Record> vgiac;
    private ListaVisGiacAdapter ladapter;
    private ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_vis_giac);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        lista = findViewById(R.id.visgiac_lista);

        vgiac = new ArrayList<Record>();
        ladapter = new ListaVisGiacAdapter(this.getApplicationContext(), vgiac);
        lista.setAdapter(ladapter);

        aggiornaLista();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.visgiac_menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.visgiac_action_stampa) {
            if (Env.tipostampante == 0 && (Env.ssidstampante.equals("") || Env.ipstampante.equals(""))) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormVisGiac.this);
                builder.setMessage("Stampante WI-FI non configurata")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
                return false;
            } else if ((Env.tipostampante == 1 || Env.tipostampante == 2) && Env.nomebtstampante.equals("")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormVisGiac.this);
                builder.setMessage("Stampante Bluetooth non configurata")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
                return false;
            } else {
                Intent i = new Intent(FormVisGiac.this.getApplicationContext(), FormInfoStampa.class);
                Bundle mBundle = new Bundle();
                mBundle.putString("titolo", "Stampa giacenze articoli");
                i.putExtras(mBundle);
                startActivityForResult(i, 2);
                return true;
            }
        } else
            return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (2): {
                // stampa giacenze
                if (resultCode == Activity.RESULT_OK) {
                    FunzioniJBeerApp.stampaGiacenze(getApplicationContext());
                    try {
                        Env.wifiPort.disconnect();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    try {
                        Env.btPort.disconnect();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            }
        }

    }


    private void aggiornaLista() {
        vgiac.clear();
        Cursor cursor = Env.db.rawQuery("SELECT artcod,artdescr,artum,artgiacenza FROM articoli WHERE artgiacenza <> 0 ORDER BY artcod", null);
        while (cursor.moveToNext()) {
            Record r = new Record();
            r.insStringa("artcod", cursor.getString(0));
            r.insStringa("artdescr", cursor.getString(1));
            r.insStringa("artum", cursor.getString(2));
            r.insDouble("giac", cursor.getDouble(3));
            vgiac.add(r);
        }
        cursor.close();
        ladapter.notifyDataSetChanged();
    }
}

