package jsoftware.jbeerapp.forms;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaStatEventiMerceInviataAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Record;

public class FragmentStatEventiMerceInviata extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ListView lista;
    private TextView tvtotvend;

    private String eventocod = "";

    private ArrayList<Record> vmi;

    private ListaStatEventiMerceInviataAdapter ladapter;

    public FragmentStatEventiMerceInviata() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentStatArticoli.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentStatEventiMerceInviata newInstance(String param1, String param2) {
        FragmentStatEventiMerceInviata fragment = new FragmentStatEventiMerceInviata();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_stat_eventi_merceinviata, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String azione);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lista = view.findViewById(R.id.statmi_lista);
    }

    private void calcolaDati() {
        vmi = new ArrayList();
        String[] pars = new String[1];
        pars[0] = eventocod;
        Cursor c = Env.db.rawQuery("SELECT eviartcod,eviartdescr,eviartum,eviqta FROM eventi_merceinviata WHERE evievento=? ORDER BY eviartcod", pars);
        while (c.moveToNext()) {
            Record r = new Record();
            r.insStringa("artcod", c.getString(0));
            r.insStringa("artdescr", c.getString(1));
            r.insStringa("artum", c.getString(2));
            r.insDouble("qta", c.getDouble(3));
            vmi.add(r);
        }
        c.close();
        ladapter = new ListaStatEventiMerceInviataAdapter(this.getActivity().getApplicationContext(), vmi);
        lista.setAdapter(ladapter);
    }

    public void impostaFiltri(String eventocod) {
        this.eventocod = eventocod;
    }

    @Override
    public void onStart() {
        super.onStart();
        System.out.println("STATMI START " + eventocod);
        calcolaDati();
    }
}
