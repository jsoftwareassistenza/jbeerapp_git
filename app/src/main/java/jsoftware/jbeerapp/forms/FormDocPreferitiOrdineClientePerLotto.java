package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaPrefOrdineClienteAdapter;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;

public class FormDocPreferitiOrdineClientePerLotto extends AppCompatActivity {
    private Button bok;
    private Button bannulla;
    private EditText edcod;
    private EditText eddescr;
    private Spinner ricstato;
    private ListView listaart;
    private ListaPrefOrdineClienteAdapter lprefadapter;
    private ArrayList<Record> vart;
    private ArrayList<Record> vins;
    public ArrayList<Record> righecrp = null;
    private String clicod = "";
    public String clicodpadre = "";
    private ArrayList<String> lst;
    private SpinnerAdapter lstadapter;
    private int posselez;
    private String codiva;
    private String artdescr;
    private int tipodoc;
    private double clisc1;
    private double clisc2;
    private double clisc3;
    private int caucorr;
    private double giacenza;
    private Double quantita;
    public String clicodiva = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_doc_preferiti_ordinecliente);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicod"))
            clicod = getIntent().getExtras().getString("clicod");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodpadre"))
            clicodpadre = getIntent().getExtras().getString("clicodpadre");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("tipodoc"))
            tipodoc = getIntent().getExtras().getInt("tipodoc");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clisc1"))
            clisc1 = getIntent().getExtras().getDouble("clisc1");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clisc2"))
            clisc2 = getIntent().getExtras().getDouble("clisc2");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clisc3"))
            clisc3 = getIntent().getExtras().getDouble("clisc3");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("caucorr"))
            caucorr = getIntent().getExtras().getInt("caucorr");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodiva"))
            clicodiva = getIntent().getExtras().getString("clicodiva");

        bok = this.findViewById(R.id.prefoc_buttonOk);
        bannulla = this.findViewById(R.id.prefoc_buttonAnnulla);
        ricstato = this.findViewById(R.id.prefoc_ricstato);
        listaart = this.findViewById(R.id.prefoc_listaart);
        edcod = this.findViewById(R.id.prefoc_riccod);
        eddescr = this.findViewById(R.id.prefoc_ricdescr);
        if (Env.codartnum)
            edcod.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);

        // carica stato
        lst = new ArrayList();
        lst.add("PREFERITI");
        lst.add("NUOVI");
        lst.add("IN OFFERTA");
        lst.add("PREZZO AUMENTATO");
        lst.add("PREZZO DIMINUITO");
        lstadapter = new SpinnerAdapter(FormDocPreferitiOrdineClientePerLotto.this.getApplicationContext(), R.layout.spinner_item, lst);
        ricstato.setAdapter(lstadapter);

/*        View header = this.getLayoutInflater().inflate(R.layout.prefoc_listaart_header, null);
        listaart.addHeaderView(header);*/

        vart = new ArrayList<Record>();
        vins = new ArrayList<Record>();
        lprefadapter = new ListaPrefOrdineClienteAdapter(this.getApplicationContext(), vart);
        listaart.setAdapter(lprefadapter);

        if (Env.depeventocod.equals("")) {
            aggiornaLista();
        } else {
            aggiornaListaEvento();
        }

        bok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ArrayList<HashMap> vris = new ArrayList();
                for (int i = 0; i < vins.size(); i++) {
                    Record r = vins.get(i);
                    double a = r.leggiDouble("qtavend");
                    if (r.leggiDouble("qtavend") != 0 || r.leggiDouble("qtasm") != 0 || r.leggiDouble("qtaomimp") != 0 ||
                            r.leggiDouble("qtaomtot") != 0) {
                        HashMap h = new HashMap();
                        h.put("artcod", r.leggiStringa("rmartcod"));
                        h.put("artdescr", r.leggiStringa("artdescr"));
                        h.put("codiva", r.leggiStringa("codiva"));
                        h.put("lotto", r.leggiStringa("lotto"));
                        h.put("qtavend", r.leggiDouble("qtavend"));
                        h.put("qtasm", r.leggiDouble("qtasm"));
                        h.put("qtaomimp", r.leggiDouble("qtaomimp"));
                        h.put("qtaomtot", r.leggiDouble("qtaomtot"));
                        h.put("prezzo", r.leggiDouble("prezzo"));
                        h.put("sc1", r.leggiDouble("sc1"));
                        h.put("sc2", r.leggiDouble("sc2"));
                        h.put("sc3", r.leggiDouble("sc3"));
                        h.put("colli", r.leggiIntero("colli"));
                        vris.add(h);
                    }
                }
                Uri codselez = Uri.parse("content://listavend/OK");
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                result.putExtra("vris", vris);
                setResult(RESULT_OK, result);
                finish();
            }
        });

        bannulla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uri codselez = Uri.parse("content://listaprefoc/ANNULLA");
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                setResult(RESULT_CANCELED, result);
                finish();
            }
        });

        ricstato.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

/*
        edcod.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
                return false;
            }
        });

        eddescr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
                return false;
            }
        });
*/

        edcod.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
            }
        });

        eddescr.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
            }
        });

        listaart.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                posselez = (int) arg3;
                if (posselez != -1) {
                    Record rec = vart.get(posselez);
                    Env.trasfdoc_rriga = null;
                    Env.trasfdoc_rrigavar = null;
                    //Env.trasfdoc_righecrp = fdoc.righecrp;
                    Intent i = new Intent(FormDocPreferitiOrdineClientePerLotto.this.getApplicationContext(), FormRigaArt.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("artcod", rec.leggiStringa("artcod"));
                    //mBundle.putString("lotto", rec.leggiStringa("lotto"));

                    giacenza = rec.leggiDouble("giac");
                    quantita = rec.leggiDouble("qtavend");
                    codiva = rec.leggiStringa("codiva");
                    artdescr = rec.leggiStringa("artdescr");
                    mBundle.putDouble("giac", rec.leggiDouble("giac"));
                    mBundle.putInt("tipodoc", tipodoc);
                    mBundle.putString("clicod", clicod);
                    mBundle.putDouble("clisc1", clisc1);
                    mBundle.putDouble("clisc2", clisc2);
                    mBundle.putDouble("clisc3", clisc3);
                    mBundle.putInt("caucorr", caucorr);
                    mBundle.putDouble("prezzo", rec.leggiDouble("prezzo"));
                    i.putExtras(mBundle);
                    FormDocPreferitiOrdineClientePerLotto.this.startActivityForResult(i, 1);

                }
            }
        });

    }

    private void aggiornaListaEvento() {
        // mem.qta già inserite in lista
        HashMap<String, Double> hqtavendold = new HashMap();
        HashMap<String, Double> hqtasmold = new HashMap();
        HashMap<String, Double> hqtaomimpold = new HashMap();
        HashMap<String, Double> hqtaomtotold = new HashMap();
        HashMap<String, Integer> hcollivendold = new HashMap();
        HashMap<String, Integer> hcollismold = new HashMap();
        HashMap<String, Integer> hcolliomimpold = new HashMap();
        HashMap<String, Integer> hcolliomtotold = new HashMap();
        for (int i = 0; i < vart.size(); i++) {
            Record r = vart.get(i);
            if (r.leggiIntero("collivend") > 0) {
                hcollivendold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("collivend")));
            }
            if (r.leggiDouble("qtavend") > 0) {
                hqtavendold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtavend")));
            }
            if (r.leggiIntero("collism") > 0) {
                hcollismold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("collism")));
            }
            if (r.leggiDouble("qtasm") > 0) {
                hqtasmold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtasm")));
            }
            if (r.leggiIntero("colliomimp") > 0) {
                hcolliomimpold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("colliomimp")));
            }
            if (r.leggiDouble("qtaomimp") > 0) {
                hqtaomimpold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtaomimp")));
            }
            if (r.leggiIntero("colliomtot") > 0) {
                hcolliomtotold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("colliomtot")));
            }
            if (r.leggiDouble("qtaomtot") > 0) {
                hqtaomtotold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtaomtot")));
            }
        }
        vart.clear();

        String c = edcod.getText().toString().trim();
        String d = eddescr.getText().toString().trim();
        c.replace("'", "''");
        d.replace("'", "''");
        if (ricstato.getSelectedItemPosition() == 0) {
            // caricamento preferiti
            String cc = clicod;
            if (!clicodpadre.equals(""))
                cc = clicodpadre;
            HashMap<String, String> hart = new HashMap();
            Cursor cursor = Env.db.rawQuery(
                    "SELECT artcod,artdescr,artum,artgiacenza,artcodiva,prefprezzo,artnotificaterm,artflagum,artpezzi,artweb_linkscheda,artprzacq,giacqta FROM articoli " +
                            "INNER JOIN preferiti " +
                            "ON articoli.artcod = preferiti.prefartcod LEFT JOIN giacart ON articoli.artcod = giacart.giacartcod " +
                            "LEFT JOIN catalago ON catartcod=artcod WHERE prefclicod = '" + cc + "' AND " +
                            "artcod <> ''  AND artstato = 'A' " +
                            (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                            (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                            " ORDER BY artcod", null);
            while (cursor.moveToNext()) {
                Record rx = new Record();
                hart.put(cursor.getString(0), "");
                rx.insStringa("artcod", cursor.getString(0));
                rx.insStringa("artdescr", cursor.getString(1));
                rx.insStringa("artum", cursor.getString(2));
                rx.insIntero("artflagum", cursor.getInt(7));
                rx.insStringa("artschedaweb", cursor.getString(9));
                rx.insDouble("artprzacq", cursor.getDouble(10));
                rx.insDouble("artpezzi", cursor.getDouble(8));
                rx.insStringa("codiva", cursor.getString(4));
                rx.insDouble("giacsede", cursor.getDouble(11));
                if (!clicod.equals("")) {
                    double przvend = 0;
                    double prznetto = 0;
                    double sc1 = 0, sc2 = 0, sc3 = 0;
//                    if (Env.prezzopref)
//                    {
//                        przvend = cursor.getDouble(5);
//                        prznetto = przvend;
//                    }
//                    if (przvend == 0)
                    {
                        double[] prz = FunzioniJBeerApp.calcoloPrezzoArticolo(cursor.getString(0), cc, false);
                        przvend = prz[3];
                        prznetto = przvend;
                        sc1 = prz[0];
                        sc2 = prz[1];
                        sc3 = prz[2];
                        if (prz[0] > 0)
                            prznetto = prznetto - (prznetto * prz[0] / 100);
                        if (prz[1] > 0)
                            prznetto = prznetto - (prznetto * prz[1] / 100);
                        if (prz[2] > 0)
                            prznetto = prznetto - (prznetto * prz[2] / 100);
                        prznetto = OpValute.arrotondaMat(prznetto, 3);
                    }
                    rx.insDouble("sc1", sc1);
                    rx.insDouble("sc2", sc2);
                    rx.insDouble("sc3", sc3);
                    rx.insDouble("prezzo", przvend);
                    rx.insDouble("prezzonetto", prznetto);
                    rx.insDouble("prezzocli", cursor.getDouble(5));
                } else {
                    rx.insDouble("sc1", 0);
                    rx.insDouble("sc2", 0);
                    rx.insDouble("sc3", 0);
                    rx.insDouble("prezzo", 0);
                    rx.insDouble("prezzonetto", 0);
                    rx.insDouble("prezzocli", 0);
                }
                double przrif = FunzioniJBeerApp.leggiPrezzoListinoBaseArticolo(cursor.getString(0));
                rx.insDouble("przrif", przrif);

                int cvend = 0;
                if (hcollivendold.get(cursor.getString(0)) != null)
                    cvend = hcollivendold.get(cursor.getString(0)).intValue();
                rx.insIntero("collivend", cvend);
                double qvend = 0;
                if (hqtavendold.get(cursor.getString(0)) != null)
                    qvend = hqtavendold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtavend", qvend);

                int csm = 0;
                if (hcollismold.get(cursor.getString(0)) != null)
                    csm = hcollismold.get(cursor.getString(0)).intValue();
                rx.insIntero("collism", csm);
                double qsm = 0;
                if (hqtasmold.get(cursor.getString(0)) != null)
                    qsm = hqtasmold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtasm", qsm);

                int comimp = 0;
                if (hcolliomimpold.get(cursor.getString(0)) != null)
                    comimp = hcolliomimpold.get(cursor.getString(0)).intValue();
                rx.insIntero("colliomimp", comimp);
                double qomimp = 0;
                if (hqtaomimpold.get(cursor.getString(0)) != null)
                    qomimp = hqtaomimpold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtaomimp", qomimp);

                int comtot = 0;
                if (hcolliomtotold.get(cursor.getString(0)) != null)
                    comtot = hcolliomtotold.get(cursor.getString(0)).intValue();
                rx.insIntero("colliomtot", comtot);
                double qomtot = 0;
                if (hqtaomtotold.get(cursor.getString(0)) != null)
                    qomtot = hqtaomtotold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtaomtot", qomtot);

                rx.insIntero("notifica", cursor.getInt(6));
                vart.add(rx);
            }
            cursor.close();
        } else {
            // nuovi, in offerta, prezzo variato ecc.
            String qry = "SELECT artcod,artdescr,artum,artgiacenza,artcodiva,artnotificaterm,artweb_linkscheda,artprzacq,giacqta " +
                    "FROM articoli LEFT JOIN giacart ON articoli.artcod = giacart.giacartcod " +
                    "LEFT JOIN catalago ON catartcod=artcod " +
                    " WHERE artcod <> ''  AND artstato = 'A' ";
            qry += " AND artnotificaterm = " + ricstato.getSelectedItemPosition();
            if (!c.equals("")) {
                qry += " AND artcod LIKE \"%" + c + "%\"";
            }
            if (!d.equals("")) {
                qry += " AND artdescr LIKE \"%" + d + "%\"";
            }
            qry += " ORDER BY artcod";
            Cursor ca = Env.db.rawQuery(qry, null);
            while (ca.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", ca.getString(0));
                rx.insStringa("artdescr", ca.getString(1));
                rx.insStringa("artum", ca.getString(2));
                rx.insStringa("artschedaweb", ca.getString(6));
                rx.insDouble("artprzacq", ca.getDouble(7));
                rx.insStringa("codiva", ca.getString(4));
                rx.insDouble("giacsede", ca.getDouble(8));
                if (!clicod.equals("")) {
                    String cc = clicod;
                    if (!clicodpadre.equals(""))
                        cc = clicodpadre;
                    double przvend = 0;
                    double prznetto = 0;
                    double sc1 = 0, sc2 = 0, sc3 = 0;
//                    if (Env.prezzopref)
//                    {
//                        przvend = FunzioniJBeerApp.leggiPrezzoDaPreferiti(ca.getString(0), cc);
//                        prznetto = przvend;
//                    }
//                    if (przvend == 0)
                    {
                        double[] prz = FunzioniJBeerApp.calcoloPrezzoArticolo(ca.getString(0), cc, false);
                        przvend = prz[3];
                        prznetto = przvend;
                        sc1 = prz[0];
                        sc2 = prz[1];
                        sc3 = prz[2];
                        if (prz[0] > 0)
                            prznetto = prznetto - (prznetto * prz[0] / 100);
                        if (prz[1] > 0)
                            prznetto = prznetto - (prznetto * prz[1] / 100);
                        if (prz[2] > 0)
                            prznetto = prznetto - (prznetto * prz[2] / 100);
                        prznetto = OpValute.arrotondaMat(prznetto, 3);
                    }
                    rx.insDouble("sc1", sc1);
                    rx.insDouble("sc2", sc2);
                    rx.insDouble("sc3", sc3);
                    rx.insDouble("prezzo", przvend);
                    rx.insDouble("prezzonetto", prznetto);
                    rx.insDouble("prezzocli", przvend);
                } else {
                    rx.insDouble("sc1", 0);
                    rx.insDouble("sc2", 0);
                    rx.insDouble("sc3", 0);
                    rx.insDouble("prezzo", 0);
                    rx.insDouble("prezzonetto", 0);
                    rx.insDouble("prezzocli", 0);
                }
                double przrif = FunzioniJBeerApp.leggiPrezzoListinoBaseArticolo(ca.getString(0));
                rx.insDouble("przrif", przrif);

                int cvend = 0;
                if (hcollivendold.get(ca.getString(0)) != null)
                    cvend = hcollivendold.get(ca.getString(0)).intValue();
                rx.insIntero("collivend", cvend);
                double qvend = 0;
                if (hqtavendold.get(ca.getString(0)) != null)
                    qvend = hqtavendold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtavend", qvend);

                int csm = 0;
                if (hcollismold.get(ca.getString(0)) != null)
                    csm = hcollismold.get(ca.getString(0)).intValue();
                rx.insIntero("collism", csm);
                double qsm = 0;
                if (hqtasmold.get(ca.getString(0)) != null)
                    qsm = hqtasmold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtasm", qsm);

                int comimp = 0;
                if (hcolliomimpold.get(ca.getString(0)) != null)
                    comimp = hcolliomimpold.get(ca.getString(0)).intValue();
                rx.insIntero("colliomimp", comimp);
                double qomimp = 0;
                if (hqtaomimpold.get(ca.getString(0)) != null)
                    qomimp = hqtaomimpold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtaomimp", qomimp);

                int comtot = 0;
                if (hcolliomtotold.get(ca.getString(0)) != null)
                    comtot = hcolliomtotold.get(ca.getString(0)).intValue();
                rx.insIntero("colliomtot", comtot);
                double qomtot = 0;
                if (hqtaomtotold.get(ca.getString(0)) != null)
                    qomtot = hqtaomtotold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtaomtot", qomtot);

                rx.insIntero("notifica", ca.getInt(5));
                vart.add(rx);
            }
            ca.close();
        }
        lprefadapter.notifyDataSetChanged();

    }

    @Override
    public void onBackPressed() {
        Uri codselez = Uri.parse("content://listaprefoc/ANNULLA");
        Intent result = new Intent(Intent.ACTION_PICK, codselez);
        setResult(RESULT_CANCELED, result);
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v("JAZZTV", "*************RESUME");
    }

    private void aggiornaLista() {
        // mem.qta già inserite in lista
        HashMap<String, Double> hqtavendold = new HashMap();
        HashMap<String, Double> hqtasmold = new HashMap();
        HashMap<String, Double> hqtaomimpold = new HashMap();
        HashMap<String, Double> hqtaomtotold = new HashMap();
        HashMap<String, Integer> hcollivendold = new HashMap();
        HashMap<String, Integer> hcollismold = new HashMap();
        HashMap<String, Integer> hcolliomimpold = new HashMap();
        HashMap<String, Integer> hcolliomtotold = new HashMap();
        for (int i = 0; i < vart.size(); i++) {
            Record r = vart.get(i);
            if (r.leggiIntero("collivend") > 0) {
                hcollivendold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("collivend")));
            }
            if (r.leggiDouble("qtavend") > 0) {
                hqtavendold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtavend")));
            }
            if (r.leggiIntero("collism") > 0) {
                hcollismold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("collism")));
            }
            if (r.leggiDouble("qtasm") > 0) {
                hqtasmold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtasm")));
            }
            if (r.leggiIntero("colliomimp") > 0) {
                hcolliomimpold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("colliomimp")));
            }
            if (r.leggiDouble("qtaomimp") > 0) {
                hqtaomimpold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtaomimp")));
            }
            if (r.leggiIntero("colliomtot") > 0) {
                hcolliomtotold.put(r.leggiStringa("artcod"), new Integer(r.leggiIntero("colliomtot")));
            }
            if (r.leggiDouble("qtaomtot") > 0) {
                hqtaomtotold.put(r.leggiStringa("artcod"), new Double(r.leggiDouble("qtaomtot")));
            }
        }
        vart.clear();

        String c = edcod.getText().toString().trim();
        String d = eddescr.getText().toString().trim();
        c.replace("'", "''");
        d.replace("'", "''");
        if (ricstato.getSelectedItemPosition() == 0) {
            // caricamento preferiti
            String cc = clicod;
            if (!clicodpadre.equals(""))
                cc = clicodpadre;
            HashMap<String, String> hart = new HashMap();
            String qry = "";
            if (Env.dispart) {
                qry = "SELECT artcod,artdescr,artum,dispqta,artcodiva,prefprezzo,artnotificaterm,artflagum,artpezzi,artweb_linkscheda,artprzacq FROM articoli " +
                        "INNER JOIN preferiti " +
                        "ON articoli.artcod = preferiti.prefartcod LEFT JOIN dispart ON artcod=dispartcod " +
                        "LEFT JOIN catalogo ON catartcod=artcod WHERE prefclicod = '" + cc + "' AND " +
                        "artcod <> ''  AND artstato = 'A' " +
                        (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                        (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                        " ORDER BY artcod";

            } else {
                qry = "SELECT artcod,artdescr,artum,giacqta,artcodiva,prefprezzo,artnotificaterm,artflagum,artpezzi,artweb_linkscheda,artprzacq FROM articoli " +
                        "INNER JOIN preferiti " +
                        "ON articoli.artcod = preferiti.prefartcod LEFT JOIN giacart ON articoli.artcod = giacart.giacartcod " +
                        "LEFT JOIN catalogo ON catartcod=artcod WHERE prefclicod = '" + cc + "' AND " +
                        "artcod <> ''  AND artstato = 'A' " +
                        (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                        (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                        " ORDER BY artcod";
            }
            Cursor cursor = Env.db.rawQuery(qry, null);
            while (cursor.moveToNext()) {
                double vargiac = 0;
                Record rx = new Record();
                hart.put(cursor.getString(0), "");
                rx.insStringa("artcod", cursor.getString(0));
                rx.insStringa("artdescr", cursor.getString(1));
                rx.insStringa("artum", cursor.getString(2));
                rx.insIntero("artflagum", cursor.getInt(7));
                rx.insStringa("artschedaweb", cursor.getString(9));
                rx.insDouble("artprzacq", cursor.getDouble(10));
                rx.insDouble("artpezzi", cursor.getDouble(8));
                rx.insStringa("codiva", cursor.getString(4));
                for (int j = 0; j < Env.trasfdoc_righecrp.size(); j++) {
                    Record rr = Env.trasfdoc_righecrp.get(j);
                    String acod = rr.leggiStringa("rmartcod");
                    String lcod = rr.leggiStringa("rmlotto");
                    String cau = rr.leggiStringa("rmcaumag");
                    double qta = rr.leggiDouble("rmqta");
                    if (acod.equals(cursor.getString(0)) && !cau.equals("RN")) {
                        if (cau.equals("RV"))
                            vargiac += qta;
                        else
                            vargiac -= qta;
                    }
                }
                //controlla i movimenti - documenti effettuati ma non ancora inviati
                String qrygiac = "SELECT movtipo,movdoc,movsez,movdata,movnum" +
                        " FROM movimenti WHERE movtrasf <> 'S' ORDER BY movtipo,movdata,movnum";
                Cursor cursorgiac = Env.db.rawQuery(qrygiac, null);
                while (cursorgiac.moveToNext()) {
                    String[] parsd = new String[6];
                    parsd[0] = "" + cursorgiac.getInt(0);
                    parsd[1] = cursorgiac.getString(1);
                    parsd[2] = cursorgiac.getString(2);
                    parsd[3] = cursorgiac.getString(3);
                    parsd[4] = "" + cursorgiac.getInt(4);
                    parsd[5] = cursor.getString(0);
                    int nriga = 1;
                    Cursor cd = Env.db.rawQuery(
                            "SELECT rmqta " +
                                    "FROM righemov WHERE rmmovtipo=? AND rmmovdoc=? AND rmmovsez=? AND rmmovdata=? AND rmmovnum=? AND rmartcod=? ORDER BY rmriga",
                            parsd);
                    while (cd.moveToNext()) {
                        vargiac = vargiac - cd.getDouble(0);
                    }
                    cd.close();
                }
                cursorgiac.close();
                rx.insDouble("giac", cursor.getDouble(3) + vargiac);
                if (!clicod.equals("")) {
                    double przvend = 0;
                    double prznetto = 0;
                    double sc1 = 0, sc2 = 0, sc3 = 0;
//                    if (Env.prezzopref)
//                    {
//                        przvend = cursor.getDouble(5);
//                        prznetto = przvend;
//                    }
//                    if (przvend == 0)
                    {
                        double[] prz = FunzioniJBeerApp.calcoloPrezzoArticolo(cursor.getString(0), cc, false);
                        przvend = prz[3];
                        prznetto = przvend;
                        sc1 = prz[0];
                        sc2 = prz[1];
                        sc3 = prz[2];
                        if (prz[0] > 0)
                            prznetto = prznetto - (prznetto * prz[0] / 100);
                        if (prz[1] > 0)
                            prznetto = prznetto - (prznetto * prz[1] / 100);
                        if (prz[2] > 0)
                            prznetto = prznetto - (prznetto * prz[2] / 100);
                        prznetto = OpValute.arrotondaMat(prznetto, 3);
                    }
                    rx.insDouble("sc1", sc1);
                    rx.insDouble("sc2", sc2);
                    rx.insDouble("sc3", sc3);
                    rx.insDouble("prezzo", przvend);
                    rx.insDouble("prezzonetto", prznetto);
                    rx.insDouble("prezzocli", cursor.getDouble(5));
                } else {
                    rx.insDouble("sc1", 0);
                    rx.insDouble("sc2", 0);
                    rx.insDouble("sc3", 0);
                    rx.insDouble("prezzo", 0);
                    rx.insDouble("prezzonetto", 0);
                    rx.insDouble("prezzocli", 0);
                }
                double przrif = FunzioniJBeerApp.leggiPrezzoListinoBaseArticolo(cursor.getString(0));
                rx.insDouble("przrif", przrif);
                int cvend = 0;
                if (hcollivendold.get(cursor.getString(0)) != null)
                    cvend = hcollivendold.get(cursor.getString(0)).intValue();
                rx.insIntero("collivend", cvend);
                double qvend = 0;
                if (hqtavendold.get(cursor.getString(0)) != null)
                    qvend = hqtavendold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtavend", qvend);

                int csm = 0;
                if (hcollismold.get(cursor.getString(0)) != null)
                    csm = hcollismold.get(cursor.getString(0)).intValue();
                rx.insIntero("collism", csm);
                double qsm = 0;
                if (hqtasmold.get(cursor.getString(0)) != null)
                    qsm = hqtasmold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtasm", qsm);

                int comimp = 0;
                if (hcolliomimpold.get(cursor.getString(0)) != null)
                    comimp = hcolliomimpold.get(cursor.getString(0)).intValue();
                rx.insIntero("colliomimp", comimp);
                double qomimp = 0;
                if (hqtaomimpold.get(cursor.getString(0)) != null)
                    qomimp = hqtaomimpold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtaomimp", qomimp);

                int comtot = 0;
                if (hcolliomtotold.get(cursor.getString(0)) != null)
                    comtot = hcolliomtotold.get(cursor.getString(0)).intValue();
                rx.insIntero("colliomtot", comtot);
                double qomtot = 0;
                if (hqtaomtotold.get(cursor.getString(0)) != null)
                    qomtot = hqtaomtotold.get(cursor.getString(0)).doubleValue();
                rx.insDouble("qtaomtot", qomtot);

                rx.insIntero("notifica", cursor.getInt(6));
                vart.add(rx);
            }
            cursor.close();
        } else {
            // nuovi, in offerta, prezzo variato ecc.
            String qry = "SELECT artcod,artdescr,artum,giacqta,artcodiva,artnotificaterm,artweb_linkscheda,artprzacq " +
                    "FROM articoli LEFT JOIN giacart ON articoli.artcod = giacart.giacartcod " +
                    "LEFT JOIN catalogo ON catartcod=artcod " +
                    " WHERE artcod <> ''  AND artstato = 'A' ";
            qry += " AND artnotificaterm = " + ricstato.getSelectedItemPosition();
            if (!c.equals("")) {
                qry += " AND artcod LIKE \"%" + c + "%\"";
            }
            if (!d.equals("")) {
                qry += " AND artdescr LIKE \"%" + d + "%\"";
            }
            qry += " ORDER BY artcod";
            Cursor ca = Env.db.rawQuery(qry, null);
            while (ca.moveToNext()) {
                double vargiac = 0;
                Record rx = new Record();
                rx.insStringa("artcod", ca.getString(0));
                rx.insStringa("artdescr", ca.getString(1));
                rx.insStringa("artum", ca.getString(2));
                rx.insStringa("artschedaweb", ca.getString(6));
                rx.insDouble("artprzacq", ca.getDouble(7));
                rx.insStringa("codiva", ca.getString(4));
                for (int j = 0; j < Env.trasfdoc_righecrp.size(); j++) {
                    Record rr = Env.trasfdoc_righecrp.get(j);
                    String acod = rr.leggiStringa("rmartcod");
                    String lcod = rr.leggiStringa("rmlotto");
                    String cau = rr.leggiStringa("rmcaumag");
                    double qta = rr.leggiDouble("rmqta");
                    if (acod.equals(ca.getString(0)) && !cau.equals("RN")) {
                        if (cau.equals("RV"))
                            vargiac += qta;
                        else
                            vargiac -= qta;
                    }
                }
                String qrygiac = "SELECT movtipo,movdoc,movsez,movdata,movnum" +
                        " FROM movimenti WHERE movtrasf <> 'S' ORDER BY movtipo,movdata,movnum";
                Cursor cursorgiac = Env.db.rawQuery(qrygiac, null);
                while (cursorgiac.moveToNext()) {
                    String[] parsd = new String[6];
                    parsd[0] = "" + cursorgiac.getInt(0);
                    parsd[1] = cursorgiac.getString(1);
                    parsd[2] = cursorgiac.getString(2);
                    parsd[3] = cursorgiac.getString(3);
                    parsd[4] = "" + cursorgiac.getInt(4);
                    parsd[5] = ca.getString(0);
                    int nriga = 1;
                    Cursor cd = Env.db.rawQuery(
                            "SELECT rmqta " +
                                    "FROM righemov WHERE rmmovtipo=? AND rmmovdoc=? AND rmmovsez=? AND rmmovdata=? AND rmmovnum=? AND rmartcod=? ORDER BY rmriga",
                            parsd);
                    while (cd.moveToNext()) {
                        vargiac = vargiac - cd.getDouble(0);
                    }
                    cd.close();
                }
                cursorgiac.close();

                rx.insDouble("giac", ca.getDouble(3) + vargiac);
                if (!clicod.equals("")) {
                    String cc = clicod;
                    if (!clicodpadre.equals(""))
                        cc = clicodpadre;
                    double przvend = 0;
                    double prznetto = 0;
                    double sc1 = 0, sc2 = 0, sc3 = 0;
//                    if (Env.prezzopref)
//                    {
//                        przvend = FunzioniJBeerApp.leggiPrezzoDaPreferiti(ca.getString(0), cc);
//                        prznetto = przvend;
//                    }
//                    if (przvend == 0)
                    {
                        double[] prz = FunzioniJBeerApp.calcoloPrezzoArticolo(ca.getString(0), cc, false);
                        przvend = prz[3];
                        prznetto = przvend;
                        sc1 = prz[0];
                        sc2 = prz[1];
                        sc3 = prz[2];
                        if (prz[0] > 0)
                            prznetto = prznetto - (prznetto * prz[0] / 100);
                        if (prz[1] > 0)
                            prznetto = prznetto - (prznetto * prz[1] / 100);
                        if (prz[2] > 0)
                            prznetto = prznetto - (prznetto * prz[2] / 100);
                        prznetto = OpValute.arrotondaMat(prznetto, 3);
                    }
                    rx.insDouble("sc1", sc1);
                    rx.insDouble("sc2", sc2);
                    rx.insDouble("sc3", sc3);
                    rx.insDouble("prezzo", przvend);
                    rx.insDouble("prezzonetto", prznetto);
                    rx.insDouble("prezzocli", przvend);
                } else {
                    rx.insDouble("sc1", 0);
                    rx.insDouble("sc2", 0);
                    rx.insDouble("sc3", 0);
                    rx.insDouble("prezzo", 0);
                    rx.insDouble("prezzonetto", 0);
                    rx.insDouble("prezzocli", 0);
                }

                int cvend = 0;
                if (hcollivendold.get(ca.getString(0)) != null)
                    cvend = hcollivendold.get(ca.getString(0)).intValue();
                rx.insIntero("collivend", cvend);
                double qvend = 0;
                if (hqtavendold.get(ca.getString(0)) != null)
                    qvend = hqtavendold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtavend", qvend);

                int csm = 0;
                if (hcollismold.get(ca.getString(0)) != null)
                    csm = hcollismold.get(ca.getString(0)).intValue();
                rx.insIntero("collism", csm);
                double qsm = 0;
                if (hqtasmold.get(ca.getString(0)) != null)
                    qsm = hqtasmold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtasm", qsm);

                int comimp = 0;
                if (hcolliomimpold.get(ca.getString(0)) != null)
                    comimp = hcolliomimpold.get(ca.getString(0)).intValue();
                rx.insIntero("colliomimp", comimp);
                double qomimp = 0;
                if (hqtaomimpold.get(ca.getString(0)) != null)
                    qomimp = hqtaomimpold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtaomimp", qomimp);

                int comtot = 0;
                if (hcolliomtotold.get(ca.getString(0)) != null)
                    comtot = hcolliomtotold.get(ca.getString(0)).intValue();
                rx.insIntero("colliomtot", comtot);
                double qomtot = 0;
                if (hqtaomtotold.get(ca.getString(0)) != null)
                    qomtot = hqtaomtotold.get(ca.getString(0)).doubleValue();
                rx.insDouble("qtaomtot", qomtot);

                rx.insIntero("notifica", ca.getInt(5));
                vart.add(rx);
            }
            ca.close();
        }
        lprefadapter.notifyDataSetChanged();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                // ritorno da inserimento riga art
                if (resultCode == Activity.RESULT_OK) {
                    Record rart = vart.get(posselez);
                    //ris = data.getData();
                    ArrayList<HashMap> vris = (ArrayList<HashMap>) data.getExtras().get("vris");
                    for (int i = 0; i < vris.size(); i++) {
                        HashMap riga = vris.get(i);
                        boolean trovato = false;
                        //controlla se è già stato inserito lo stesso articolo con eventualmente lo stesso lotto
                        for (int j = 0; j < vins.size(); j++) {
                            Record r2 = vins.get(j);
                            if (((String) riga.get("artcod")).equals(r2.leggiStringa("rmartcod")) &&
                                    ((String) riga.get("rmcaumag")).equals(r2.leggiStringa("rmcaumag")) &&
                                    (Double) riga.get("prezzo") == r2.leggiDouble("prezzo") &&
                                    (Double) riga.get("sc1") == r2.leggiDouble("sc1") &&
                                    (Double) riga.get("sc2") == r2.leggiDouble("sc2") &&
                                    (Double) riga.get("sc3") == r2.leggiDouble("sc3") &&
                                    ((String) riga.get("lotto")).equals(r2.leggiStringa("lotto"))) {
                                Env.righecrptmp.remove(j);
                                double qtaomimp = (Double) riga.get("qtaomimp") + r2.leggiDouble("qtaomimp");
                                double qtasm = (Double) riga.get("qtasm") + r2.leggiDouble("qtasm");
                                double qtavend = (Double) riga.get("qtavend") + r2.leggiDouble("qtavend");
                                double qtaomtot = (Double) riga.get("qtaomtot") + r2.leggiDouble("qtaomtot");
                                giacenza -= (Double) riga.get("quantita");
                                rart.eliminaCampo("giac");
                                rart.insDouble("giac", giacenza);
                                rart.eliminaCampo("qtaomimp");
                                rart.insDouble("qtaomimp", qtaomimp);
                                rart.eliminaCampo("qtasm");
                                rart.insDouble("qtasm", qtasm);
                                rart.eliminaCampo("qtavend");
                                rart.insDouble("qtavend", qtavend);
                                rart.eliminaCampo("qtaomtot");
                                rart.insDouble("qtaomtot", qtaomtot);
                                int colli = (Integer) riga.get("colli") + r2.leggiIntero("colli");
                                double quantita = (Double) riga.get("quantita") + r2.leggiDouble("rmqta");
                                r2.eliminaCampo("qtaomimp");
                                r2.insDouble("qtaomimp", qtaomimp);
                                r2.eliminaCampo("qtasm");
                                r2.insDouble("qtasm", qtasm);
                                r2.eliminaCampo("qtavend");
                                r2.insDouble("qtavend", qtavend);
                                r2.eliminaCampo("qtaomtot");
                                r2.insDouble("qtaomtot", qtaomtot);
                                r2.eliminaCampo("quantita");
                                r2.insDouble("rmqta", quantita);
                                r2.eliminaCampo("colli");
                                r2.insIntero("colli", colli);
                                trovato = true;
                                Env.righecrptmp.add(r2);

                                break;
                            }
                        }
                        if (!trovato) {
                            Record rec = new Record();
                            rec.insDouble("prezzo", (Double) riga.get("prezzo"));
                            rec.insDouble("sc1", (Double) riga.get("sc1"));
                            rec.insDouble("sc2", (Double) riga.get("sc2"));
                            rec.insDouble("sc3", (Double) riga.get("sc3"));
                            rec.insDouble("qtaomimp", (Double) riga.get("qtaomimp"));
                            rec.insDouble("qtasm", (Double) riga.get("qtasm"));
                            rec.insDouble("qtavend", (Double) riga.get("qtavend"));
                            rec.insDouble("qtaomtot", (Double) riga.get("qtaomtot"));
                            rec.insIntero("colli", (Integer) riga.get("colli"));
                            rec.insStringa("lotto", (String) riga.get("lotto"));
                            giacenza -= (Double) riga.get("quantita");
                            rart.eliminaCampo("giac");
                            rart.insDouble("giac", giacenza);
                            rart.eliminaCampo("qtaomimp");
                            rart.insDouble("qtaomimp", (Double) riga.get("qtaomimp"));
                            rart.eliminaCampo("qtasm");
                            rart.insDouble("qtasm", (Double) riga.get("qtasm"));
                            rart.eliminaCampo("qtavend");
                            rart.insDouble("qtavend", (Double) riga.get("qtavend"));
                            rart.eliminaCampo("qtaomtot");
                            rart.insDouble("qtaomtot", (Double) riga.get("qtaomtot"));
                            rec.insDouble("giac", giacenza);
                            rec.insStringa("rmcaumag", (String) riga.get("rmcaumag"));
                            rec.insStringa("rmartcod", (String) riga.get("artcod"));
                            rec.insStringa("rmlotto", (String) riga.get("lotto"));
                            rec.insStringa("codiva", codiva);
                            rec.insStringa("artdescr", artdescr);
                            rec.insDouble("rmqta", (Double) riga.get("quantita"));
                            vins.add(rec);
                            Env.righecrptmp.add(rec);
                        }

                        Env.quantitaCarrello += (Integer) riga.get("colli");
                    }
                    //lvendadapter.notifyDataSetChanged();
                    //mCountTv.setText("" + (int) Env.quantitaCarrello);

                    Toast toast = Toast.makeText(getApplicationContext(), "Aggiunto al carrello!",
                            Toast.LENGTH_SHORT);
                    toast.show();

                }
                break;
            }
        }
    }

}
