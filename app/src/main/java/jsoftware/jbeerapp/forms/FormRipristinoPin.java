package jsoftware.jbeerapp.forms;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;


public class FormRipristinoPin extends AppCompatActivity {


    private Button bconferma;
    private Button bsalva;
    private Spinner spdom;
    private LinearLayout llpin;
    private boolean ripristino = false;
    private EditText edrisposta;
    private EditText ednewpin;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_ripristino_pin);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        bconferma = findViewById(R.id.ripr_pin_buttonconf);
        bsalva = findViewById(R.id.ripr_pin_buttonOk);
        spdom = findViewById(R.id.ripr_pin_spinner);
        llpin = findViewById(R.id.ripr_pin_llnuovopin);
        edrisposta = findViewById(R.id.ripr_pin_rispedit);
        ednewpin = findViewById(R.id.ripr_pin_newpin);


        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("ripristino")) {
            ripristino = this.getIntent().getExtras().getBoolean("ripristino");
        }


        llpin.setVisibility(View.GONE);


        ArrayList<String> vd = new ArrayList();
        vd.add("...");
        vd.add("Qual'è la tua birra preferita?");
        vd.add("Qual'è il nome del tuo miglior amico d'infanzia?");
        vd.add("Qual'è la città di nascita di tua  madre?");
        vd.add("Qual'è la tua squadra del cuore?");
        vd.add("Qual'è il nome del tuo animale domestico?");
        SpinnerAdapter ldadapter = new SpinnerAdapter(FormRipristinoPin.this.getApplicationContext(), R.layout.spinner_item, vd);
        spdom.setAdapter(ldadapter);


        bconferma.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ripristino) {
                    String risposta = FunzioniJBeerApp.leggiProprieta("" + spdom.getSelectedItemPosition());
                    if (risposta.equals(edrisposta.getText().toString().toUpperCase())) {
                        llpin.setVisibility(View.VISIBLE);
                    }
                } else {
                    FunzioniJBeerApp.impostaProprieta("" + spdom.getSelectedItemPosition(), edrisposta.getText().toString().toUpperCase());
                    Toast toast = Toast.makeText(FormRipristinoPin.this.getApplicationContext(), "Domanda Segreta Impostata!",
                            Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
            }
        });

        bsalva.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String tmp = (String) ednewpin.getText().toString();
                Uri codselez = Uri.parse("content://riprpin/" + tmp);
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                result.putExtra("newpin", (String) tmp);
                setResult(RESULT_OK, result);
                finish();
            }
        });


    }


}
