package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.funzStringa;

public class FormNuovoCliente extends AppCompatActivity {

    private EditText ednome;
    private EditText edcognomepri;
    private EditText ednomepri;
    private EditText edindirizzo;
    private EditText edindnum;
    private EditText edcomune;
    private EditText edmail;
    private EditText edtel;
    private EditText edcell;
    private EditText ednote;
    private EditText edpiva;
    private EditText edcf;
    private boolean opmodifica = false;
    private String clicognomepri;
    private String clinomepri;
    private String clinumtel;
    private String clinumcell;
    private String cliemail1;
    private String cliindir;
    private String cliloc;
    private String clicap;
    private String clipiva;
    private String clicodfisc;
    private String clinumciv;
    private String clinote;
    private boolean optrasforma = false;
    private String clinome;
    private String clicodice;
    private EditText edcap;
    private EditText edprov;
    private String cliprov;
    private Spinner sppagamento;
    private ArrayList vpag = null;
    private ArrayList vpagtipo = null;
    public int pagtvpos = -1;
    private String clicodpag;
    private RadioButton ckprivato;
    private RadioButton ckazienda;
    private int clitipo;
    private EditText edinsegna;
    private EditText edsdi;
    private String clisdi;
    private String cliinsegna;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_nuovo_cliente);

        ednomepri = findViewById(R.id.editnewCli_Nome);
        edcognomepri = findViewById(R.id.editnewCli_Cognome);
        ednome = findViewById(R.id.editnewCli_Az);
        edindirizzo = findViewById(R.id.editnewCli_ind);
        edindnum = findViewById(R.id.editnewCli_num);
        edcomune = findViewById(R.id.editnewCli_Comune);
        edmail = findViewById(R.id.editnewCli_Mail);
        edtel = findViewById(R.id.editnewCli_Tel);
        edcell = findViewById(R.id.editnewCli_Cell);
        ednote = findViewById(R.id.editnewCli_Note);
        edpiva = findViewById(R.id.editnewCli_PIva);
        edcf = findViewById(R.id.editnewCli_CF);
        edcap = findViewById(R.id.editnewCli_cap);
        edprov = findViewById(R.id.editnewCli_pr);
        edinsegna = findViewById(R.id.editnewCli_Insegna);
        edsdi = findViewById(R.id.editnewCli_SDI);
        sppagamento = findViewById(R.id.editnewCli_pagamento);
        ckprivato = findViewById(R.id.rB_privato);
        ckazienda = findViewById(R.id.rB_azienda);
        ckazienda.setChecked(true);

        // caricamento lista pagamenti
        vpag = new ArrayList();
        vpagtipo = new ArrayList();
        ArrayList<String> vtmp = new ArrayList();
        int posp = 0;
        Cursor cursor = Env.db.rawQuery("SELECT pagcod,pagdescr,pagtipo FROM pagamenti ORDER BY pagcod", null);
        while (cursor.moveToNext()) {
            vtmp.add(cursor.getString(1));
            vpag.add(cursor.getString(0));
            vpagtipo.add(cursor.getInt(2));
            if (cursor.getInt(2) == 8)
                pagtvpos = posp;
            posp++;
        }
        cursor.close();
        SpinnerAdapter lpagadapter = new SpinnerAdapter(FormNuovoCliente.this.getApplicationContext(), R.layout.spinner_item, vtmp);
        sppagamento.setAdapter(lpagadapter);
        sppagamento.setSelection(0);


        if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean("opmodifica") != false) {
            opmodifica = true;
            edcomune.setEnabled(false);
            if (edcf.length() > 13) {
                edcf.setEnabled(false);
            } else {
                edcf.setEnabled(true);
            }
            edprov.setEnabled(false);
            edpiva.setEnabled(false);
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicognomepri"))
                clicognomepri = getIntent().getExtras().getString("clicognomepri");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clinome"))
                clinome = getIntent().getExtras().getString("clinome");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clinomepri"))
                clinomepri = getIntent().getExtras().getString("clinomepri");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clinumtel"))
                clinumtel = getIntent().getExtras().getString("clinumtel");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clinumcell"))
                clinumcell = getIntent().getExtras().getString("clinumcell");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cliemail1"))
                cliemail1 = getIntent().getExtras().getString("cliemail1");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cliindir"))
                cliindir = getIntent().getExtras().getString("cliindir");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodfisc"))
                clicodfisc = getIntent().getExtras().getString("clicodfisc");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clinumciv"))
                clinumciv = getIntent().getExtras().getString("clinumciv");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cliloc"))
                cliloc = getIntent().getExtras().getString("cliloc");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicap"))
                clicap = getIntent().getExtras().getString("clicap");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clipiva"))
                clipiva = getIntent().getExtras().getString("clipiva");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cliprov"))
                cliprov = getIntent().getExtras().getString("cliprov");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodice"))
                clicodice = getIntent().getExtras().getString("clicodice");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodpag"))
                clicodpag = getIntent().getExtras().getString("clicodpag");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clitipo"))
                clitipo = getIntent().getExtras().getInt("clitipo");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodsdinuovo"))
                clisdi = getIntent().getExtras().getString("clicodsdinuovo");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cliinsegna"))
                cliinsegna = getIntent().getExtras().getString("cliinsegna");
            if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean("optrasforma") != false)
                optrasforma = true;
            if (optrasforma) {
                if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cnote"))
                    clinote = getIntent().getExtras().getString("cnote");
                FormNuovoCliente.this.setTitle("Trasforma Contatto in Cliente");
                // bsalva.setText("TRASFORMA");

            } else {
                if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clinote"))
                    clinote = getIntent().getExtras().getString("clinote");
                FormNuovoCliente.this.setTitle("Modifica Contatto Cliente");
                //  bsalva.setText("MODIFICA");
            }

            // codice pagamento
            if (!clicodpag.equals("")) {
                for (int i = 0; i < vpag.size(); i++) {
                    if (clicodpag.equals(vpag.get(i))) {
                        sppagamento.setSelection(i);
                       /* if (cursor.getString(32).equals("S"))
                            sppagamento.setEnabled(true);
                        else
                            sppagamento.setEnabled(false);*/
                        break;
                    }
                }
            }


            edcognomepri.setText(clicognomepri);
            ednomepri.setText(clinomepri);
            ednome.setText(clinome);
            if (!cliindir.equals("")) {
                if (cliindir.contains(",")) {
                    edindirizzo.setText(cliindir.substring(0, cliindir.indexOf(",")));
                    String numciv = cliindir.substring(cliindir.indexOf(",") + 1);
                    edindnum.setText(numciv.trim());
                } else {
                    edindirizzo.setText(cliindir);
                    edindnum.setText(clinumciv.trim());
                }
            }

            edcomune.setText(cliloc);
            edmail.setText(cliemail1);
            edtel.setText(clinumtel);
            edcell.setText(clinumcell);
            edpiva.setText(clipiva);
            ednote.setText(clinote);
            edcf.setText(clicodfisc);
            edcap.setText(clicap);
            if (optrasforma) {
                edsdi.setEnabled(true);
            } else {
                if (clisdi.length() < 2 || clisdi.equals("0000000")) {
                    edsdi.setEnabled(true);
                } else {
                    edsdi.setEnabled(false);
                }
                edsdi.setText(clisdi);
            }

            edinsegna.setText(cliinsegna);
            edprov.setText(cliprov.toUpperCase());
            if (clitipo == 0) {
                ckprivato.setChecked(true);
                ckazienda.setChecked(false);
                ednomepri.setHint("Nome*");
                edcognomepri.setHint("Cognome*");
                ednome.setVisibility(View.GONE);
                ckazienda.setSelected(false);
                edpiva.setVisibility(View.GONE);
                edinsegna.setVisibility(View.GONE);
                edcf.setHint("C.F.*");
            } else {
                ckprivato.setChecked(false);
                ckazienda.setChecked(true);
                ednomepri.setHint("Nome");
                edcognomepri.setHint("Cognome");
                ednome.setHint("Azienda*");
                ckprivato.setSelected(false);
                edpiva.setHint("P.Iva*");
                edcf.setHint("C.F.");
                ednome.setVisibility(View.VISIBLE);
                edpiva.setVisibility(View.VISIBLE);
                edinsegna.setVisibility(View.VISIBLE);
            }
        }

        ckprivato.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (ckprivato.isChecked()) {
                    ednomepri.setHint("Nome*");
                    edcognomepri.setHint("Cognome*");
                    //ednome.setHint("Azienda");
                    ckazienda.setSelected(false);
                    //edpiva.setHint("P.Iva");
                    edcf.setHint("C.F.*");
                    ednome.setVisibility(View.GONE);
                    edpiva.setVisibility(View.GONE);
                    edinsegna.setVisibility(View.GONE);
                }
            }
        });

        ckazienda.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (ckazienda.isChecked()) {
                    ednomepri.setHint("Nome");
                    edcognomepri.setHint("Cognome");
                    ednome.setHint("Azienda*");
                    ckprivato.setSelected(false);
                    edpiva.setHint("P.Iva*");
                    edcf.setHint("C.F. Azienda");
                    ednome.setVisibility(View.VISIBLE);
                    edpiva.setVisibility(View.VISIBLE);
                    edinsegna.setVisibility(View.VISIBLE);
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_form_salva_contatto, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //item.setEnabled(false);
        if (ckprivato.isChecked() && ednomepri.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoCliente.this);
            builder.setMessage("Inserire il Nome")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }
        if (ckprivato.isChecked() && edcognomepri.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoCliente.this);
            builder.setMessage("Inserire il Cognome")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }
        if (ckazienda.isChecked() && ednome.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoCliente.this);
            builder.setMessage("Inserire la Ragione sociale")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }
        if (edmail.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoCliente.this);
            builder.setMessage("Inserire l'Email")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }
/*        if (edcell.getText().toString().equals("") || edtel.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoCliente.this);
            builder.setMessage("Inserire almeno un numero telefonico")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }*/
        if (ckazienda.isChecked() && edpiva.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoCliente.this);
            builder.setMessage("Inserire la Partita IVA")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }
        if (ckprivato.isChecked() && edcf.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoCliente.this);
            builder.setMessage("Inserire il Codice Fiscale")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }


        if (opmodifica && !optrasforma) {


            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoCliente.this);
            builder.setMessage("Confermi Modifica Cliente?")
                    .setPositiveButton("SI",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    String qry = "UPDATE clienti SET clinome=?, cliindir=?, cliloc=?,cliprov=?,clipiva=?,clinumtel=?," + //6
                                            "clinumcell=?, cliemail1=? ,clicognomepri=?,clicodfisc=?, clinote=? ,clinumciv=?, " + //12
                                            "clinomepri = ? , clicap = ? , clicodpag = ? , clitipo= ? , clicodsdinuovo = ? , cliinsegna = ? " + //18
                                            "WHERE clicodice= '" + clicodice + "'";
                                    SQLiteStatement stup = Env.db.compileStatement(qry);
                                    stup.clearBindings();
                                    stup.bindString(1, ednome.getText().toString().trim());
                                    stup.bindString(2, edindirizzo.getText().toString().trim());
                                    stup.bindString(3, edcomune.getText().toString().trim());
                                    stup.bindString(4, edprov.equals("") ? "" : edprov.getText().toString().trim().toUpperCase());
                                    stup.bindString(5, edpiva.getText().toString().trim());
                                    //stup.bindDouble(6, Formattazione.estraiIntero(edtel.getText().toString().trim()));
                                    stup.bindString(6, edtel.getText().toString().trim());
                                    stup.bindString(7, edcell.getText().toString().trim());
                                    stup.bindString(8, edmail.getText().toString().trim());
                                    stup.bindString(9, edcognomepri.getText().toString().trim());
                                    stup.bindString(10, edcf.getText().toString().trim());
                                    stup.bindString(11, ednote.getText().toString().trim());
                                    stup.bindString(12, edindnum.getText().toString().trim());
                                    stup.bindString(13, ednomepri.getText().toString().trim());
                                    stup.bindString(14, edcap.getText().toString().trim());
                                    String a = String.valueOf(sppagamento.getSelectedItem());
                                    Cursor cursorp = Env.db.rawQuery("SELECT pagcod FROM pagamenti WHERE pagdescr ='" + a + "'", null);
                                    while (cursorp.moveToNext()) {
                                        stup.bindString(15, cursorp.getString(0));
                                    }
                                    cursorp.close();
                                    //stup.bindString(15, a.substring(0, a.lastIndexOf("-")));
                                    if (ckprivato.isChecked()) {
                                        stup.bindDouble(16, 0);
                                    } else {
                                        stup.bindDouble(16, 1);
                                    }
                                    stup.bindString(17, edsdi.getText().toString().trim());
                                    stup.bindString(18, edinsegna.getText().toString().trim());

                                    stup.execute();
                                    stup.close();
                                    azioneInsertvaranag();
                                    HashMap h = new HashMap();
                                    h.put("clinome", ednome.getText().toString().trim());
                                    h.put("cliindir", edindirizzo.getText().toString().trim());
                                    h.put("cliloc", edcomune.getText().toString().trim());
                                    h.put("cliprov", edprov.equals("") ? "" : edprov.getText().toString().trim());
                                    h.put("clipiva", edpiva.getText().toString().trim());
                                    h.put("clinumtel", edtel.getText().toString().trim());
                                    h.put("clinumcell", edcell.getText().toString().trim());
                    /*h.put("clinumtel", Formattazione.estraiIntero(edtel.getText().toString().trim()));
                    h.put("clinumcell",  Formattazione.estraiIntero(edcell.getText().toString().trim()));*/
                                    h.put("cliemail1", edmail.getText().toString().trim());
                                    h.put("clicognomepri", edcognomepri.getText().toString().trim());
                                    h.put("clicodfisc", edcf.getText().toString().trim());
                                    h.put("clinote", ednote.getText().toString().trim());
                                    h.put("clinumciv", edindnum.getText().toString().trim());
                                    /*h.put("clinumciv", Formattazione.estraiIntero(edindnum.getText().toString().trim()));*/
                                    h.put("clinomepri", ednomepri.getText().toString().trim());
                                    h.put("clicap", edcap.getText().toString().trim());
                                    h.put("clitipo", ckprivato.isChecked() ? 0 : 1);
                                    //non so' se serve passare il cliinsegna e il clicodsdi
                                    ArrayList<HashMap> vris = new ArrayList();
                                    vris.add(h);
                                    Uri esito = Uri.parse("content://modificacontatto/OK1");
                                    Intent result = new Intent(Intent.ACTION_PICK, esito);
                                    result.putExtra("vris", vris);
                                    setResult(RESULT_OK, result);
                                    finish();


                                    // FormDettaglioCliente.this.finish();
                                    //Intent inent = new Intent(FormDettaglioCliente.this, FormRubrica.class);

                                    // calling an activity using <intent-filter> action name
                                    //  Intent inent = new Intent("com.hmkcode.android.ANOTHER_ACTIVITY");

                                    //startActivity(inent);


                                }
                            })
                    .setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.show();


        } else {

            String piva = edpiva.getText().toString().trim();
            String clicodfisc = edcf.getText().toString().trim();
            boolean esiste = false;
            if (ckazienda.isChecked()) {
                Cursor ccli = Env.db.rawQuery("SELECT clipiva FROM clienti WHERE clipiva = '" + piva + "'", null);
                if (ccli.moveToFirst()) {
                    esiste = true;
                }
                ccli.close();
            } else {
                Cursor ccli = Env.db.rawQuery("SELECT clicodfisc FROM clienti WHERE clicodfisc = '" + clicodfisc + "'", null);
                if (ccli.moveToFirst()) {
                    esiste = true;
                }
                ccli.close();
            }
            if (!esiste) {
                SQLiteStatement sti = Env.db
                        .compileStatement("INSERT INTO clienti (clicodice,clinome,cliindir,clicap,cliloc,cliprov," + //6
                                "clipiva, clinumtel,clinumcell,cliemail1,clicognomepri,clicodfisc, " +
                                "clinote, clinumciv, clinomepri,clinuovo,clivalidato,clicodpag, " +
                                "clitipo,clicodsdinuovo,cliinsegna,clicodlist) "
                                + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                String codnew = "";
                boolean trovato = false;
                int progcli = 1;
                String codlist = "";
                while (!trovato) {
                    codnew = Env.pedcod + funzStringa.riempiStringa("" + progcli, 3, funzStringa.DX, '0');
                    String[] pars = new String[1];
                    pars[0] = codnew;
                    Cursor cursorp = Env.db.rawQuery("SELECT clicodice FROM clienti WHERE clicodice=?", pars);
                    if (!cursorp.moveToFirst()) {
                        trovato = true;
                        codlist = Env.clinuovolist;
                    } else {
                        progcli++;
                    }
                    cursorp.close();
                }
                sti.clearBindings();
                sti.bindString(1, codnew);
                String tel = edtel.getText().toString().trim();
                String cliNome = "";
                if (ckprivato.isChecked()) {
                    cliNome = edcognomepri.getText().toString().trim() + " " + ednomepri.getText().toString().trim();
                } else {
                    cliNome = ednome.getText().toString().trim();
                }
                sti.bindString(2, cliNome);
                sti.bindString(3, edindirizzo.getText().toString().trim());
                sti.bindString(4, edcap.getText().toString().trim());
                sti.bindString(5, edcomune.getText().toString().trim());
                sti.bindString(6, edprov.equals("") ? "" : edprov.getText().toString().trim());
                sti.bindString(7, edpiva.getText().toString().trim());
                sti.bindString(8, tel);
                sti.bindString(9, edcell.getText().toString().trim());
                sti.bindString(10, edmail.getText().toString().trim());
                sti.bindString(11, edcognomepri.getText().toString().trim());
                sti.bindString(12, edcf.getText().toString().trim());
                sti.bindString(13, ednote.getText().toString().trim());
                sti.bindString(14, (edindnum.getText().toString().trim()));
                sti.bindString(15, ednomepri.getText().toString().trim());
                // nuovo
                sti.bindLong(16, 1);
                // validato
                sti.bindString(17, "N");
                String a = String.valueOf(sppagamento.getSelectedItem());
                Cursor cursorp = Env.db.rawQuery("SELECT pagcod FROM pagamenti WHERE pagdescr ='" + a + "'", null);
                while (cursorp.moveToNext()) {
                    sti.bindString(18, cursorp.getString(0));
                }
                cursorp.close();
                //sti.bindString(18, a.substring(0, a.lastIndexOf("-")));
                sti.bindDouble(19, ckprivato.isChecked() ? 0 : 1);
                sti.bindString(20, edsdi.getText().toString().trim());
                sti.bindString(21, edinsegna.getText().toString().trim());
                sti.bindString(22, codlist);
                sti.execute();
                sti.close();
                Uri esito = Uri.parse("content://nuovocontatto/OK1");
                Intent result = new Intent(Intent.ACTION_PICK, esito);
                result.putExtra("clicodice", codnew);
                result.putExtra("clinome", cliNome);
                result.putExtra("clicognomepri", edcognomepri.getText().toString().trim());
                result.putExtra("clinomepri", ednomepri.getText().toString().trim());
                setResult(RESULT_OK, result);
                finish();
            } else {
                String text = "";
                if (ckazienda.isChecked()) {
                    text = "Attenzione cliente con Partita Iva " + piva + " gia' esistente!";
                } else {
                    text = "Attenzione cliente con Codice Fiscale " + clicodfisc + " gia' esistente!";
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormNuovoCliente.this);
                builder.setMessage(text)
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();

            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void azioneInsertvaranag() {
        SQLiteStatement std = Env.db
                .compileStatement("DELETE FROM varanag WHERE vacodice=? AND vatipo=?");
        SQLiteStatement sti = Env.db
                .compileStatement("INSERT INTO varanag (vacodice,vatipo,vavalore,vatrasf,vadata,vaora) "
                        + "VALUES (?,?,?,?,?,?)");
        SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
        String ora = dh.format(new Date());
        String oggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");

        std.clearBindings();
        std.bindString(1, clicodice);
        std.bindLong(2, 0);
        std.execute();
        sti.clearBindings();
        sti.bindString(1, clicodice);
        sti.bindLong(2, 0);
        sti.bindString(3, edtel.getText().toString().trim());
        sti.bindString(4, "N");
        sti.bindString(5, oggi);
        sti.bindString(6, ora);
        sti.execute();


        std.clearBindings();
        std.bindString(1, clicodice);
        std.bindLong(2, 1);
        std.execute();
        sti.clearBindings();
        sti.bindString(1, clicodice);
        sti.bindLong(2, 1);
        sti.bindString(3, edcell.getText().toString().trim());
        sti.bindString(4, "N");
        sti.bindString(5, oggi);
        sti.bindString(6, ora);
        sti.execute();

/*            std.clearBindings();
            std.bindString(1, clicodice);
            std.bindLong(2, 2);
            std.execute();
            sti.clearBindings();
            sti.bindString(1, clicodice);
            sti.bindLong(2, 2);
            sti.bindString(3, edfax.getText().toString().trim());
            sti.bindString(4, "N");
            sti.bindString(5, oggi);
            sti.bindString(6, ora);
            sti.execute();*/


/*
            std.clearBindings();
            std.bindString(1, clicodice);
            std.bindLong(2, 3);
            std.execute();
            sti.clearBindings();
            sti.bindString(1, clicodice);
            sti.bindLong(2, 3);
            sti.bindString(3, edtelamm.getText().toString().trim());
            sti.bindString(4, "N");
            sti.bindString(5, oggi);
            sti.bindString(6, ora);
            sti.execute();
*/

/*            std.clearBindings();
            std.bindString(1, clicodice);
            std.bindLong(2, 4);
            std.execute();
            sti.clearBindings();
            sti.bindString(1, clicodice);
            sti.bindLong(2, 4);
            sti.bindString(3, edtelcomm.getText().toString().trim());
            sti.bindString(4, "N");
            sti.bindString(5, oggi);
            sti.bindString(6, ora);
            sti.execute();*/

/*            std.clearBindings();
            std.bindString(1, clicodice);
            std.bindLong(2, 5);
            std.execute();
            sti.clearBindings();
            sti.bindString(1, clicodice);
            sti.bindLong(2, 5);
            sti.bindString(3, edtelabitaz.getText().toString().trim());
            sti.bindString(4, "N");
            sti.bindString(5, oggi);
            sti.bindString(6, ora);
            sti.execute();

            std.clearBindings();
            std.bindString(1, clicodice);
            std.bindLong(2, 6);
            std.execute();
            sti.clearBindings();
            sti.bindString(1, clicodice);
            sti.bindLong(2, 6);
            sti.bindString(3, edtelsedeop.getText().toString().trim());
            sti.bindString(4, "N");
            sti.bindString(5, oggi);
            sti.bindString(6, ora);
            sti.execute();*/


/*        if (ckemergenze.isChecked())
        {
            std.clearBindings();
            std.bindString(1, clicodice);
            std.bindLong(2, 7);
            std.execute();
            sti.clearBindings();
            sti.bindString(1, clicodice);
            sti.bindLong(2, 7);
            sti.bindString(3, edemergenze.getText().toString().trim());
            sti.bindString(4, "N");
            sti.bindString(5, oggi);
            sti.bindString(6, ora);
            sti.execute();
        }*/

/*        if (cktelemergenze.isChecked())
        {
            std.clearBindings();
            std.bindString(1, clicodice);
            std.bindLong(2, 8);
            std.execute();
            sti.clearBindings();
            sti.bindString(1, clicodice);
            sti.bindLong(2, 8);
            sti.bindString(3, edtelemergenze.getText().toString().trim());
            sti.bindString(4, "N");
            sti.bindString(5, oggi);
            sti.bindString(6, ora);
            sti.execute();
        }*/


        std.clearBindings();
        std.bindString(1, clicodice);
        std.bindLong(2, 9);
        std.execute();
        sti.clearBindings();
        sti.bindString(1, clicodice);
        sti.bindLong(2, 9);
        sti.bindString(3, edmail.getText().toString().trim());
        sti.bindString(4, "N");
        sti.bindString(5, oggi);
        sti.bindString(6, ora);
        sti.execute();

        std.clearBindings();
        std.bindString(1, clicodice);
        std.bindLong(2, 19);
        std.execute();
        sti.clearBindings();
        sti.bindString(1, clicodice);
        sti.bindLong(2, 19);
        sti.bindString(3, ednomepri.getText().toString().trim());
        sti.bindString(4, "N");
        sti.bindString(5, oggi);
        sti.bindString(6, ora);
        sti.execute();

        std.clearBindings();
        std.bindString(1, clicodice);
        std.bindLong(2, 20);
        std.execute();
        sti.clearBindings();
        sti.bindString(1, clicodice);
        sti.bindLong(2, 20);
        sti.bindString(3, edcognomepri.getText().toString().trim());
        sti.bindString(4, "N");
        sti.bindString(5, oggi);
        sti.bindString(6, ora);
        sti.execute();

        std.clearBindings();
        std.bindString(1, clicodice);
        std.bindLong(2, 21);
        std.execute();
        sti.clearBindings();
        sti.bindString(1, clicodice);
        sti.bindLong(2, 21);
        sti.bindString(3, ednome.getText().toString().trim());
        sti.bindString(4, "N");
        sti.bindString(5, oggi);
        sti.bindString(6, ora);
        sti.execute();

        std.clearBindings();
        std.bindString(1, clicodice);
        std.bindLong(2, 22);
        std.execute();
        sti.clearBindings();
        sti.bindString(1, clicodice);
        sti.bindLong(2, 22);
        sti.bindString(3, edindirizzo.getText().toString().trim());
        sti.bindString(4, "N");
        sti.bindString(5, oggi);
        sti.bindString(6, ora);
        sti.execute();

        std.clearBindings();
        std.bindString(1, clicodice);
        std.bindLong(2, 23);
        std.execute();
        sti.clearBindings();
        sti.bindString(1, clicodice);
        sti.bindLong(2, 23);
        sti.bindString(3, edcap.getText().toString().trim());
        sti.bindString(4, "N");
        sti.bindString(5, oggi);
        sti.bindString(6, ora);
        sti.execute();

        std.clearBindings();
        std.bindString(1, clicodice);
        std.bindLong(2, 24);
        std.execute();
        sti.clearBindings();
        sti.bindString(1, clicodice);
        sti.bindLong(2, 24);
        sti.bindString(3, edinsegna.getText().toString().trim());
        sti.bindString(4, "N");
        sti.bindString(5, oggi);
        sti.bindString(6, ora);
        sti.execute();

        std.clearBindings();
        std.bindString(1, clicodice);
        std.bindLong(2, 25);
        std.execute();
        sti.clearBindings();
        sti.bindString(1, clicodice);
        sti.bindLong(2, 25);
        sti.bindString(3, edsdi.getText().toString().trim());
        sti.bindString(4, "N");
        sti.bindString(5, oggi);
        sti.bindString(6, ora);
        sti.execute();

        std.clearBindings();
        std.bindString(1, clicodice);
        std.bindLong(2, 26);
        std.execute();
        sti.clearBindings();
        sti.bindString(1, clicodice);
        sti.bindLong(2, 26);
        sti.bindString(3, ednote.getText().toString());
        sti.bindString(4, "N");
        sti.bindString(5, oggi);
        sti.bindString(6, ora);
        sti.execute();

        std.clearBindings();
        std.bindString(1, clicodice);
        std.bindLong(2, 27);
        std.execute();
        sti.clearBindings();
        sti.bindString(1, clicodice);
        sti.bindLong(2, 27);
        sti.bindString(3, edindnum.getText().toString());
        sti.bindString(4, "N");
        sti.bindString(5, oggi);
        sti.bindString(6, ora);
        sti.execute();
/*        if (ckemailpec.isChecked())
        {
            std.clearBindings();
            std.bindString(1, clicodice);
            std.bindLong(2, 11);
            std.execute();
            sti.clearBindings();
            sti.bindString(1, clicodice);
            sti.bindLong(2, 11);
            sti.bindString(3, ed.getText().toString().trim());
            sti.bindString(4, "N");
            sti.bindString(5, oggi);
            sti.bindString(6, ora);
            sti.execute();
        }*/

/*        if (cksitoweb.isChecked())
        {
            std.clearBindings();
            std.bindString(1, clicodice);
            std.bindLong(2, 12);
            std.execute();
            sti.clearBindings();
            sti.bindString(1, clicodice);
            sti.bindLong(2, 12);
            sti.bindString(3, edsitoweb.getText().toString().trim());
            sti.bindString(4, "N");
            sti.bindString(5, oggi);
            sti.bindString(6, ora);
            sti.execute();
        }*/

        std.close();
        sti.close();


    }

}
