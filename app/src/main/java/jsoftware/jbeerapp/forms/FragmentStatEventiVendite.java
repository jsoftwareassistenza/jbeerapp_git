package jsoftware.jbeerapp.forms;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaStatEventiVenditeAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;

public class FragmentStatEventiVendite extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ListView lista;
    private TextView tvtotvend;

    private String eventocod = "";

    private ArrayList<Record> vvend;

    private ListaStatEventiVenditeAdapter ladapter;

    public FragmentStatEventiVendite() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentStatArticoli.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentStatEventiVendite newInstance(String param1, String param2) {
        FragmentStatEventiVendite fragment = new FragmentStatEventiVendite();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_stat_eventi_vendite, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String azione);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lista = view.findViewById(R.id.statvend_lista);
        tvtotvend = view.findViewById(R.id.statvend_totvend);
    }

    private void calcolaDati() {
        vvend = new ArrayList();
        double tv = 0;
        String[] pars = new String[1];
        pars[0] = eventocod;
        Cursor c = Env.db.rawQuery("SELECT evvartcod,evvartdescr,evvartum,evvqta,evvvalore FROM eventi_venduto WHERE evvevento=? ORDER BY evvvalore DESC", pars);
        while (c.moveToNext()) {
            Record r = new Record();
            r.insStringa("artcod", c.getString(0));
            r.insStringa("artdescr", c.getString(1));
            r.insStringa("artum", c.getString(2));
            r.insDouble("qta", c.getDouble(3));
            r.insDouble("valore", c.getDouble(4));
            vvend.add(r);
            tv += c.getDouble(4);
        }
        c.close();
        tv = OpValute.arrotondaMat(tv, 2);
        for (Record r : vvend) {
            double perc = 0;
            if (tv != 0)
                perc = r.leggiDouble("valore") / tv * 100.0;
            r.insDouble("perctot", OpValute.arrotondaMat(perc, 2));
        }

        ladapter = new ListaStatEventiVenditeAdapter(this.getActivity().getApplicationContext(), vvend);
        lista.setAdapter(ladapter);
        tvtotvend.setText(Formattazione.formValuta(OpValute.arrotondaMat(tv, 2), 12, 2, Formattazione.SEGNO_SX_NEG));
    }

    public void impostaFiltri(String eventocod) {
        this.eventocod = eventocod;
    }

    @Override
    public void onStart() {
        super.onStart();
        System.out.println("STATVEND START " + eventocod);
        calcolaDati();
    }
}
