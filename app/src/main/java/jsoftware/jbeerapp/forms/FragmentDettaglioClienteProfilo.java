package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteStatement;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.GPSTracker;
import jsoftware.jbeerapp.env.funzStringa;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentDettaglioClienteProfilo.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDettaglioClienteProfilo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDettaglioClienteProfilo extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FormDettaglioCliente fdcli = null;

    //public Button blista;

    //private int posselez = -1;
    private ImageButton butcell;
    private ImageButton butfisso;
    private ImageButton butemail;
    public TextView tvemail;
    public TextView tvindiaz;
    public TextView tvnomepri;
    public TextView tvtel;
    public TextView tvcell;
    public TextView tvsitoaz;
    public String clinome;
    public String clinumtel;
    public String clinumcell;
    public String cliemail1;
    public String cliindir;
    public String cliloc;
    public String clipiva;
    public String clicodfisc;
    public String clinumciv;
    public String clinote;
    public TextView tvcomune;
    public String clicodice;
    public String clisdi;
    public String cliinsegna;
    public String clicognomepri;
    public String clinomepri;
    public String cliprov;
    public TextView tvpiva;
    public TextView tvnotecli;
    public String clicap;
    public String clicodpadre;
    public String clicodpag;
    public ImageButton breggps;
    public ImageButton bnav;
    public TextView tvgps;
    public String gps;
    public TextView tvsdi;
    public TextView tvinsegna;
    public TextView llnote;

    public FragmentDettaglioClienteProfilo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentDocumentoCarrello.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDettaglioClienteProfilo newInstance(String param1, String param2) {
        FragmentDettaglioClienteProfilo fragment = new FragmentDettaglioClienteProfilo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_dettaglio_cliente_profilo, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
/*
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        } else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);


        tvemail = this.getActivity().findViewById(R.id.detcli_email);
        tvindiaz = this.getActivity().findViewById(R.id.detcli_indAz);
        tvnomepri = this.getActivity().findViewById(R.id.detcli_cognomepri);
        tvcomune = this.getActivity().findViewById(R.id.detcli_comuneAz);
        tvsitoaz = this.getActivity().findViewById(R.id.detcli_sitoAz);
        tvtel = this.getActivity().findViewById(R.id.detcli_tel);
        tvcell = this.getActivity().findViewById(R.id.detcli_cell);
        tvpiva = this.getActivity().findViewById(R.id.detcli_piva);
        tvsdi = this.getActivity().findViewById(R.id.detcli_SdiAz);
        tvinsegna = this.getActivity().findViewById(R.id.detcli_insegnaAz);
        tvnotecli = this.getActivity().findViewById(R.id.detcli_note);
        tvgps = this.getActivity().findViewById(R.id.detcli_gps);
        llnote = this.getActivity().findViewById(R.id.decli_ll_note);
        FragmentDettaglioClienteProfilo.this.getActivity().setTitle("Dettaglio Cliente");


        butcell = this.getActivity().findViewById(R.id.detcli_bcallcel);
        butfisso = this.getActivity().findViewById(R.id.detcli_bcalltel);
        butemail = this.getActivity().findViewById(R.id.detcli_bemail);
        breggps = this.getActivity().findViewById(R.id.detcli_buttongps);
        bnav = this.getActivity().findViewById(R.id.detcli_buttonnav);

        if (this.getActivity().getIntent().getExtras() != null && this.getActivity().getIntent().getExtras().containsKey("clinome"))
            clinome = this.getActivity().getIntent().getExtras().getString("clinome");
        if (this.getActivity().getIntent().getExtras() != null && this.getActivity().getIntent().getExtras().containsKey("clicodice"))
            clicodice = this.getActivity().getIntent().getExtras().getString("clicodice");
        if (this.getActivity().getIntent().getExtras() != null && this.getActivity().getIntent().getExtras().containsKey("clicognome"))
            clicognomepri = this.getActivity().getIntent().getExtras().getString("clicognome");
        if (this.getActivity().getIntent().getExtras() != null && this.getActivity().getIntent().getExtras().containsKey("cnote"))
            clinote = this.getActivity().getIntent().getExtras().getString("cnote");

        //leggiCliente();
        visualizzaTextView();


        butcell.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + tvcell.getText().toString()));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDettaglioClienteProfilo.this.getActivity());
                    builder.setMessage("Errore su apertura telefono")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
            }
        });

        butfisso.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + tvtel.getText().toString()));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDettaglioClienteProfilo.this.getActivity());
                    builder.setMessage("Errore su apertura telefono")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
            }
        });

        butemail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("mailto:?subject=" + tvemail.getText().toString()));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDettaglioClienteProfilo.this.getActivity());
                    builder.setMessage("Errore su apertura client email")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
            }
        });


        breggps.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (fdcli.clicodice.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDettaglioClienteProfilo.this.getActivity());
                    builder.setMessage("Indicare un cliente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                } else {
                    LocationManager locationManager = (LocationManager) FragmentDettaglioClienteProfilo.this.getActivity().getSystemService(LOCATION_SERVICE);
                    boolean gpsAttivo = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (!gpsAttivo) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FragmentDettaglioClienteProfilo.this.getActivity());
                        builder.setMessage("Modulo GPS non attivo")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FragmentDettaglioClienteProfilo.this.getActivity());
                        builder.setMessage("Vuoi acquisire la posizione satellitare attuale?")
                                .setPositiveButton("SI",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                GPSTracker gps = new GPSTracker(FragmentDettaglioClienteProfilo.this.getActivity());
                                                if (gps.canGetLocation()) {
                                                    double latitude = gps.getLatitude();
                                                    double longitude = gps.getLongitude();
                                                    if (latitude > 0 || longitude > 0) {
                                                        fdcli.cligpsn = latitude;
                                                        fdcli.cligpse = longitude;
                                                        int nd = (int) fdcli.cligpsn;
                                                        double nt1 = (fdcli.cligpsn - nd) * 60;
                                                        int nm = (int) nt1;
                                                        double ns = (nt1 - nm) * 60;
                                                        int ed = (int) fdcli.cligpse;
                                                        double et1 = (fdcli.cligpse - ed) * 60;
                                                        int em = (int) et1;
                                                        double es = (et1 - em) * 60;
                                                        System.out.println("N:" + nd + " " + nm + " " + ns);
                                                        System.out.println("E:" + ed + " " + em + " " + es);
                                                        SQLiteStatement std = Env.db
                                                                .compileStatement("DELETE FROM varanag WHERE vacodice=? AND vatipo=?");
                                                        SQLiteStatement sti = Env.db
                                                                .compileStatement("INSERT INTO varanag (vacodice,vatipo,vavalore,vatrasf,vadata,vaora) "
                                                                        + "VALUES (?,?,?,?,?,?)");
                                                        SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
                                                        String ora = dh.format(new Date());
                                                        String oggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
                                                        std.clearBindings();
                                                        std.bindString(1, fdcli.clicodice);
                                                        std.bindLong(2, 13);
                                                        std.execute();
                                                        std.clearBindings();
                                                        std.bindString(1, fdcli.clicodice);
                                                        std.bindLong(2, 14);
                                                        std.execute();
                                                        std.clearBindings();
                                                        std.bindString(1, fdcli.clicodice);
                                                        std.bindLong(2, 15);
                                                        std.execute();
                                                        std.clearBindings();
                                                        std.bindString(1, fdcli.clicodice);
                                                        std.bindLong(2, 16);
                                                        std.execute();
                                                        std.clearBindings();
                                                        std.bindString(1, fdcli.clicodice);
                                                        std.bindLong(2, 17);
                                                        std.execute();
                                                        std.clearBindings();
                                                        std.bindString(1, fdcli.clicodice);
                                                        std.bindLong(2, 18);
                                                        std.execute();
                                                        sti.clearBindings();
                                                        sti.bindString(1, fdcli.clicodice);
                                                        sti.bindLong(2, 13);
                                                        sti.bindString(3, "" + nd);
                                                        sti.bindString(4, "N");
                                                        sti.bindString(5, oggi);
                                                        sti.bindString(6, ora);
                                                        sti.execute();
                                                        sti.clearBindings();
                                                        sti.bindString(1, fdcli.clicodice);
                                                        sti.bindLong(2, 14);
                                                        sti.bindString(3, "" + nm);
                                                        sti.bindString(4, "N");
                                                        sti.bindString(5, oggi);
                                                        sti.bindString(6, ora);
                                                        sti.execute();
                                                        sti.clearBindings();
                                                        sti.bindString(1, fdcli.clicodice);
                                                        sti.bindLong(2, 15);
                                                        sti.bindString(3, Formattazione.formatta(ns, "######0.000000", 0));
                                                        sti.bindString(4, "N");
                                                        sti.bindString(5, oggi);
                                                        sti.bindString(6, ora);
                                                        sti.execute();
                                                        sti.clearBindings();
                                                        sti.bindString(1, fdcli.clicodice);
                                                        sti.bindLong(2, 16);
                                                        sti.bindString(3, "" + ed);
                                                        sti.bindString(4, "N");
                                                        sti.bindString(5, oggi);
                                                        sti.bindString(6, ora);
                                                        sti.execute();
                                                        sti.clearBindings();
                                                        sti.bindString(1, fdcli.clicodice);
                                                        sti.bindLong(2, 17);
                                                        sti.bindString(3, "" + em);
                                                        sti.bindString(4, "N");
                                                        sti.bindString(5, oggi);
                                                        sti.bindString(6, ora);
                                                        sti.execute();
                                                        sti.clearBindings();
                                                        sti.bindString(1, fdcli.clicodice);
                                                        sti.bindLong(2, 18);
                                                        sti.bindString(3, Formattazione.formatta(es, "######0.000000", 0));
                                                        sti.bindString(4, "N");
                                                        sti.bindString(5, oggi);
                                                        sti.bindString(6, ora);
                                                        sti.execute();
                                                        std.close();
                                                        sti.close();
                                                        // aggiorna cliente
                                                        SQLiteStatement staggcli = Env.db
                                                                .compileStatement("UPDATE clienti SET cligpsnordsud='N',cligpsnordgradi=?,cligpsnordmin=?,cligpsnordsec=?," +
                                                                        "cligpsestovest='E',cligpsestgradi=?,cligpsestmin=?,cligpsestsec=? WHERE clicodice=?");
                                                        staggcli.clearBindings();
                                                        staggcli.bindDouble(1, nd);
                                                        staggcli.bindDouble(2, nm);
                                                        staggcli.bindDouble(3, ns);
                                                        staggcli.bindDouble(4, ed);
                                                        staggcli.bindDouble(5, em);
                                                        staggcli.bindDouble(6, es);
                                                        staggcli.bindString(7, fdcli.clicodice);
                                                        staggcli.execute();
                                                        staggcli.close();
                                                        tvgps.setText("N:" + Formattazione.formatta(fdcli.cligpsn, "########0.000000", 0) + " E:" + Formattazione.formatta(fdcli.cligpse, "########0.000000", 0));
                                                        Toast toast = Toast.makeText(FragmentDettaglioClienteProfilo.this.getActivity().getApplicationContext(), "Posizione satellitare acquisita",
                                                                Toast.LENGTH_SHORT);
                                                        toast.show();
                                                    }
                                                } else {
                                                    AlertDialog.Builder builder = new AlertDialog.Builder(
                                                            FragmentDettaglioClienteProfilo.this.getActivity());
                                                    builder.setMessage("Posizione non acquisita")
                                                            .setPositiveButton("Ok",
                                                                    new DialogInterface.OnClickListener() {
                                                                        public void onClick(DialogInterface dialog,
                                                                                            int id) {
                                                                        }
                                                                    });
                                                    AlertDialog ad = builder.create();
                                                    ad.setCancelable(false);
                                                    ad.show();
                                                }
                                            }
                                        })
                                .setNegativeButton("NO",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                    }

                }
            }
        });

        bnav.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (fdcli.clicodice.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDettaglioClienteProfilo.this.getActivity());
                    builder.setMessage("Indicare un cliente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                } else if (fdcli.cligpsn > 0 || fdcli.cligpse > 0) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + funzStringa.sostituisci(Formattazione.formatta(fdcli.cligpsn, "#######0.000000", 0), ',', '.') + "," +
                            funzStringa.sostituisci(Formattazione.formatta(fdcli.cligpse, "#######0.000000", 0), ',', '.'));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDettaglioClienteProfilo.this.getActivity());
                    builder.setMessage("Coordinate satellitari non registrate")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
            }
        });


        //registerForContextMenu(listaart);

    }

    /*    @Override
        public void onResume() {
            // TODO Auto-generated method stub
           super.onResume();
            fdoc.nfrag++;
            if (fdoc.nfrag == 3)
                fdoc.inizializza();*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                // Modifica contatto
                if (resultCode == Activity.RESULT_OK) {
                    Toast toast = Toast.makeText(this.getActivity().getApplicationContext(), "Contatto modificato",
                            Toast.LENGTH_LONG);
                    toast.show();
                    //finish();
                    ris = data.getData();
                    ArrayList<HashMap> vris = (ArrayList<HashMap>) data.getExtras().get("vris");
                    for (int i = 0; i < vris.size(); i++) {
                        HashMap riga = vris.get(i);
                        clinome = (String) riga.get("clinome");
                        cliindir = (String) riga.get("cliindir");
                        cliloc = (String) riga.get("cliloc");
                        //cmobile = String.valueOf((int) riga.get("cmobile"));
                        clipiva = (String) riga.get("clipiva");
                        clinumtel = (String) riga.get("clinumtel");
                        //caziendanumciv = String.valueOf((int) riga.get("caziendanumciv"));
                        clinumcell = (String) riga.get("clinumcell");
                        cliemail1 = (String) riga.get("cliemail1");
                        clicognomepri = (String) riga.get("clicognomepri");
                        clinomepri = (String) riga.get("clinomepri");
                        clicodfisc = (String) riga.get("clicodfisc");
                        clinote = (String) riga.get("clinote");
                        clinumciv = (String) riga.get("clinumciv");
                        clicap = (String) riga.get("clicap");
                        cliprov = (String) riga.get("cliprov");
                        fdcli.tvnominativo.setText(clinome);
                        visualizzaTextView();


                    }
                }
                break;
            }
        }
    }

    private void visualizzaTextView() {

        if (clicognomepri.equals("")) {
            tvnomepri.setText(clinome);
        } else {
            tvnomepri.setText(clicognomepri + " " + clinomepri);
        }
        tvemail.setText(cliemail1);
        String indirizzo = "";
        if (!cliindir.equals("")) {
            indirizzo += cliindir;
        }
        if (!clinumciv.equals("")) {
            indirizzo += ", " + clinumciv;
        }
        tvindiaz.setText(indirizzo);
        String comune = "";
        if (!clicap.equals("")) {
            comune += clicap;
        }
        if (!cliloc.equals("")) {
            comune += ", " + cliloc;
        }
        if (!cliprov.equals("")) {
            comune += " (" + cliprov + ")";
        }
        tvcomune.setText(comune);
        if (!clipiva.equals("")) {
            tvpiva.setText("P.Iva " + clipiva);
        } else {
            tvpiva.setText("C.F. " + clicodfisc);
        }
        tvsitoaz.setText("");
        tvtel.setText(clinumtel);
        tvcell.setText(clinumcell);

        if (!clinote.equals("")) {
            tvnotecli.setText(clinote);
        } else {
            llnote.setVisibility(View.GONE);
        }
        tvgps.setText(gps);
        if (!clisdi.equals("")) {
            tvsdi.setText("Codice SDI " + clisdi);
        } else {
            tvsdi.setVisibility(View.GONE);
        }
        if (!cliinsegna.equals("")) {
            tvinsegna.setText("Insegna " + cliinsegna);
        } else {
            tvinsegna.setVisibility(View.GONE);
        }

    }

}



