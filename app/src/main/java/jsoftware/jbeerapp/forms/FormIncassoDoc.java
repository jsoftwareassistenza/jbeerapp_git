package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.OpValute;

public class FormIncassoDoc extends AppCompatActivity {
    public double impdoc = 0;
    public double impscad = 0;
    public double totdoc = 0;
    public double totscaduto = 0;
    public double impinc = 0;
    public String clicod = "";
    private boolean blocco_ddtpagtv = false;
    public TextView tvtotdoc;
    public TextView tvimpscaduto;
    public TextView tvtotgen;
    public EditText edimp;
    public Button bok;
    public Button bnopag;
    public CheckBox cbabbuonaresto;


    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_incasso_doc);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        tvtotdoc = this.findViewById(R.id.incdoc_totdoc);
        tvimpscaduto = this.findViewById(R.id.incdoc_impscaduto);
        tvtotgen = this.findViewById(R.id.incdoc_totgen);
        edimp = this.findViewById(R.id.incdoc_imp);
        bok = this.findViewById(R.id.incdoc_buttonOk);
        bnopag = this.findViewById(R.id.incdoc_buttonNoPag);
        cbabbuonaresto = this.findViewById(R.id.incdoc_abbuonaresto);
        if (Env.obbligoincpagtv || totscaduto != 0 || !Env.abbuoniattivi) {
            cbabbuonaresto.setVisibility(View.INVISIBLE);
        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicod"))
            clicod = getIntent().getExtras().getString("clicod");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("totdoc"))
            totdoc = getIntent().getExtras().getDouble("totdoc");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("totscaduto"))
            totscaduto = getIntent().getExtras().getDouble("totscaduto");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("blocco_ddtpagtv"))
            blocco_ddtpagtv = getIntent().getExtras().getBoolean("blocco_ddtpagtv");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("impinc"))
            impinc = getIntent().getExtras().getDouble("impinc");

        double totg = OpValute.arrotondaMat(totdoc + totscaduto, 2);
        tvtotdoc.setText(Formattazione.formatta(totdoc, "#########0.00", Formattazione.SEGNO_SX_NEG).replaceAll(",", "."));
        tvimpscaduto.setText(Formattazione.formatta(totscaduto, "#########0.00", Formattazione.SEGNO_SX_NEG).replaceAll(",", "."));
        tvtotgen.setText(Formattazione.formatta(totg, "#########0.00", Formattazione.SEGNO_SX_NEG).replaceAll(",", "."));
        if (impinc != 0) {
            edimp.setText(Formattazione.formatta(impinc, "#########0.00", Formattazione.SEGNO_SX_NEG).replaceAll(",", "."));
        }

        bok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                double valincasso = Formattazione.estraiDouble(edimp.getText().toString().replace(".", ","));
                double totg = OpValute.arrotondaMat(totdoc + totscaduto, 2);
                if (totg > 0 && valincasso > totg) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormIncassoDoc.this);
                    builder.setMessage("Importo maggiore del totale")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            edimp.requestFocus();
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (totg < 0 && valincasso < totg) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormIncassoDoc.this);
                    builder.setMessage("Importo maggiore del totale")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            edimp.requestFocus();
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (totg < 0 && valincasso > 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormIncassoDoc.this);
                    builder.setMessage("L'importo incassato deve essere negativo!")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            edimp.requestFocus();
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (totg > 0 && valincasso < 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormIncassoDoc.this);
                    builder.setMessage("L'importo incassato deve essere positivo!")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            edimp.requestFocus();
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if ((totscaduto > 0 || Env.obbligoincpagtv) && valincasso != totg) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormIncassoDoc.this);
                    builder.setMessage("L'importo incassato deve essere uguale al totale documento!")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            edimp.requestFocus();
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else {
                    double abbuono = 0;
                    if (totscaduto != 0) {
                        impdoc = totdoc;
                        impscad = totscaduto;
                        abbuono = 0;
                    } else {
                        if (cbabbuonaresto.isChecked()) {
                            impdoc = valincasso;
                            abbuono = OpValute.arrotondaMat(totdoc - valincasso, 2);
                        } else {
                            impdoc = valincasso;
                            abbuono = 0;
                        }
                        impscad = 0;
                    }
                    Uri codselez = Uri.parse("content://incdoc/OK|" + Formattazione.formatta(impdoc, "#########0.00", Formattazione.SEGNO_DX) + "|" +
                            Formattazione.formatta(impscad, "#########0.00", Formattazione.SEGNO_DX) + "|" + Formattazione.formatta(abbuono, "#########0.00", Formattazione.SEGNO_DX) + "|");
                    Intent result = new Intent(Intent.ACTION_PICK, codselez);
                    setResult(RESULT_OK, result);
                    finish();
                }
            }
        });

        bnopag.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormIncassoDoc.this);
                builder.setMessage("Confermi annullamento documento?")
                        .setPositiveButton("SI",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        Uri codselez = Uri.parse("content://incdoc/ANNULLADOC");
                                        Intent result = new Intent(Intent.ACTION_PICK, codselez);
                                        setResult(RESULT_CANCELED, result);
                                        finish();

                                    }
                                })
                        .setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
            }
        });
    }
}
