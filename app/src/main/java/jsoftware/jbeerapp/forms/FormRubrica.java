package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaShareCatalogoAdapter;
import jsoftware.jbeerapp.adapters.ListaTuttiAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Record;


public class FormRubrica extends AppCompatActivity implements OnFragmentInteractionListener {


    public FragmentRubricaContatti frcon;
    public FragmentRubricaClienti frcli;
    public FragmentRubricaTutti frtut;
    public String ricnome = ""; // ricerca contatto attuale
    public EditText edricnome;
    private ArrayList<Record> vrubrica = new ArrayList<>();


    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */

    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    public ViewPager mViewPager;
    public ArrayList<Record> righetutti = null;
    private ListaTuttiAdapter ltuttiadapter;
    private boolean fragtutti = false;
    private boolean fragcontatti = false;
    private boolean fragclienti = false;
    public ArrayList listaCli;
    private ImageButton bnuovocli;
    private ImageButton bvoice;
    private ImageButton bshare;
    private boolean contatto;
    private ListaShareCatalogoAdapter lshcatagadapter;
    private ListView lvcatalogo;
    private ArrayList<Record> vcatalogo = null;
    private String[] destMail = null;
    private Record[] vscop;
    private boolean cliente;
    // private EditText edricnome;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_rubrica);

/*		Toolbar toolbar = (Toolbar) findViewById(R.id.docfor_toolbar);
		setSupportActionBar(toolbar);*/

        frcon = new FragmentRubricaContatti();
        frcon.frub = this;
        frcli = new FragmentRubricaClienti();
        frcli.frub = this;
        frtut = new FragmentRubricaTutti();
        frtut.frub = this;
        edricnome = findViewById(R.id.rub_riccont);
        ricnome = edricnome.getText().toString().trim();
        bnuovocli = findViewById(R.id.rub_badd);
        bvoice = findViewById(R.id.rub_bvoice);
        bshare = findViewById(R.id.rub_bshare);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
/*        int col = R.color.colorButtonShare;
        bnuovocli.setColorFilter(col);*/
        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.rub_container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(4);
        TabLayout tabLayout = findViewById(R.id.rub_tabs);
        tabLayout.setupWithViewPager(mViewPager);
/*        TabLayout tabLayout =  findViewById(R.id.rub_tabs);
        tabLayout.setupWithViewPager(mViewPager);*/
        mViewPager.setCurrentItem(0);

        bvoice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Nome cliente...");
                startActivityForResult(intent, 1);
            }
        });
        // listaCompleta();

/*        edricnome.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                frtut.listaCompleta();
                frcli.listaclienti();
                frcon.listacontatti();
                return false;
            }
        });*/


        edricnome.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                frtut.listaCompleta();
                frcli.listaclienti();
                frcon.listacontatti();
            }
        });

        bnuovocli.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showPopUp();
            }
        });

        bshare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ArrayList<Record> vscop1 = frtut.vrubrica;
                int c = 0;
                for (int i = 0; i < vscop1.size(); i++) {
                    Record r = vscop1.get(i);
                    if (r.esisteCampo("sel")) {
                        c++;
                    }
                }
                if (c == 0) {
                    Toast toast = Toast.makeText(FormRubrica.this.getApplicationContext(), "Selezionare un contatto!",
                            Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }
                azioneShare();
            }
        });

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


/*    public void uscita() {
        super.onBackPressed();
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* AlertDialog.Builder builder = new AlertDialog.Builder(
                FormRubrica.this);
        builder.setMessage("Uscire dalla rubrica?")
                .setPositiveButton("SI",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                uscita();
                                //finish();
                            }
                        })
                .setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                            }
                        });
        AlertDialog ad = builder.create();
        ad.show();*/
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return frtut;
                }
                case 1: {
                    return frcon;
                }
                case 2: {
                    return frcli;
                }

            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Tutti";
                case 1:
                    return "Contatti";
                case 2:
                    return "Clienti";

            }
            return null;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                if (resultCode == Activity.RESULT_OK) {
                    ArrayList<String> matches = data.getStringArrayListExtra(
                            RecognizerIntent.EXTRA_RESULTS);
                    //String voice = ris.getLastPathSegment();
                    if (matches != null && matches.size() > 0)
                        edricnome.setText(matches.get(0).toLowerCase());
                }
                break;
            }
            case (2): {
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    String esito = ris.getLastPathSegment();
                    if (esito.equals("OK1")) {
                        //esito positivoinserimento nuovo contatto
                        Toast toast = Toast.makeText(FormRubrica.this.getApplicationContext(), "Salvataggio eseguito correttamente!",
                                Toast.LENGTH_SHORT);
                        toast.show();

                        if (contatto) {
                            frtut.listaCompleta();
                            frcon.listacontatti();
                            mViewPager.setCurrentItem(1);
                        } else if (cliente) {
                            frtut.listaCompleta();
                            frcli.listaclienti();
                            mViewPager.setCurrentItem(2);
                        } else {
                            frtut.listaCompleta();
                            mViewPager.setCurrentItem(0);
                        }
                    }

                }
            }
        }
        if (mViewPager.getCurrentItem() == 0) {
            frcon.listacontatti();
            frcli.listaclienti();
            frtut.listaCompleta();
        }
    }


    private void showPopUp() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(FormRubrica.this);
        helpBuilder.setTitle("Nuovo");
        //helpBuilder.setMessage("Aggiungere un nuovo contatto");

        LayoutInflater inflater = getLayoutInflater();
        View checkboxLayout = inflater.inflate(R.layout.activity_nuovo_contatto_cliente, null);
        helpBuilder.setView(checkboxLayout);
        final RadioButton checkcontatto = checkboxLayout.findViewById(R.id.cB_contatto);
        final RadioButton checkcliente = checkboxLayout.findViewById(R.id.cB_cliente);
        checkcontatto.setChecked(true);
        helpBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        if (checkcontatto.isChecked()) {
                            Intent i = new Intent(FormRubrica.this.getApplicationContext(), FormNuovoContatto.class);
                            contatto = true;
                            cliente = false;
/*                    Bundle mBundle = new Bundle();
                    mBundle.putString("barcode", barCode);
                    i.putExtras(mBundle);*/
                            FormRubrica.this.startActivityForResult(i, 2);
                        } else if (checkcliente.isChecked()) {
                            Intent i = new Intent(FormRubrica.this.getApplicationContext(), FormNuovoCliente.class);
                            contatto = false;
                            cliente = true;
/*                    Bundle mBundle = new Bundle();
                    mBundle.putString("barcode", barCode);
                    i.putExtras(mBundle);*/
                            FormRubrica.this.startActivityForResult(i, 2);

                        }
                    }
                });
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();

    }

    private void azioneWhatsapp() {
        int c = 0;
        String[] catalogoSelezionato = new String[c];
        //final ArrayList<Record> vsel = new ArrayList();
        for (int i = 0; i < vcatalogo.size(); i++) {
            Record r = vcatalogo.get(i);
            if (r.esisteCampo("sel")) {
                c++;
            }
        }
        if (c != 0) {
            catalogoSelezionato = new String[c];
            c = 0;
            for (int i = 0; i < vcatalogo.size(); i++) {
                Record r = vcatalogo.get(i);
                if (r.esisteCampo("sel")) {
                    catalogoSelezionato[c] = r.leggiStringa("catpdfnome");
                    c++;
                }
            }
        } else {
            Toast toast = Toast.makeText(FormRubrica.this.getApplicationContext(), "Selezionare almeno un catalogo!",
                    Toast.LENGTH_LONG);
            toast.show();
            return;
        }
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("application/pdf");

        whatsappIntent.setPackage("com.whatsapp");
        String nomepdf = "";
        for (int i = 0; i < catalogoSelezionato.length; i++) {
            nomepdf = catalogoSelezionato[i];
            String a = getApplication().getFilesDir() + File.separator + "docs" + File.separator + nomepdf + ".pdf";
            File newFile = new File(getApplication().getFilesDir() + File.separator + "docs" + File.separator + nomepdf + ".pdf");
            if (newFile.isFile()) {
                Uri contentUri = FileProvider.getUriForFile(getApplicationContext(), "jsoftware.jbeerapp.fileprovider", newFile);
                whatsappIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
            } else {
                return;
            }
        }
        try {
            FormRubrica.this.startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast toast = Toast.makeText(FormRubrica.this.getApplicationContext(), "Whatsapp have not been installed.",
                    Toast.LENGTH_LONG);
            toast.show();
        }

    }

    private void azioneShare() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(FormRubrica.this);
        helpBuilder.setTitle("Cataloghi");
        LayoutInflater inflater = getLayoutInflater();
        View checkboxLayout = inflater.inflate(R.layout.activity_share_catalogo, null);
        helpBuilder.setView(checkboxLayout);

        lvcatalogo = checkboxLayout.findViewById(R.id.sharecatag_lista);
        // vrubrica = new ArrayList<>();
        registerForContextMenu(lvcatalogo);
        vcatalogo = new ArrayList<Record>();
        lshcatagadapter = new ListaShareCatalogoAdapter(getApplicationContext(), vcatalogo, this);
        lvcatalogo.setAdapter(lshcatagadapter);
        listaCatalogoPdf();
        CheckBox cbsel = checkboxLayout.findViewById(R.id.checkBox_sharecatag);
        ImageButton bsharetmp = checkboxLayout.findViewById(R.id.sharecatag_mail);
        //ImageButton bshareWhat = (ImageButton) checkboxLayout.findViewById(R.id.sharecatag_whatsapp);
        bsharetmp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int index = mViewPager.getCurrentItem();
                if (index == 0) {
                    ArrayList<Record> vscop1 = frtut.vrubrica;
                    int c = 0;
                    boolean emailVuota = false;
                    //final ArrayList<Record> vsel = new ArrayList();
                    for (int i = 0; i < vscop1.size(); i++) {
                        Record r = vscop1.get(i);
                        if (r.esisteCampo("sel")) {
                            c++;
                        }
                    }
                    if (c != 0) {
                        String[] destMail = new String[c];
                        c = 0;
                        for (int i = 0; i < vscop1.size(); i++) {
                            Record r = vscop1.get(i);
                            if (r.esisteCampo("sel")) {
                                if (r.leggiStringa("tipo") == "contatto") {
                                    destMail[c] = r.leggiStringa("cmail");
                                    c++;
                                } else {
                                    destMail[c] = r.leggiStringa("cliemail");
                                    c++;
                                }
                            }
                        }
                        if (!emailVuota) {
                            invioMail(destMail);
                        } else {
                            Toast toast = Toast.makeText(FormRubrica.this.getApplicationContext(), "Email non presente!",
                                    Toast.LENGTH_LONG);
                            toast.show();
                            return;
                        }

                    }
                } else if (index == 1) {
                    ArrayList<Record> vscop1 = frcon.vrubrica;
                    int c = 0;
                    boolean emailVuota = false;
                    //final ArrayList<Record> vsel = new ArrayList();
                    for (int i = 0; i < vscop1.size(); i++) {
                        Record r = vscop1.get(i);
                        if (r.esisteCampo("sel")) {
                            c++;
                        }
                    }
                    if (c != 0) {
                        String[] destMail = new String[c];
                        c = 0;
                        for (int i = 0; i < vscop1.size(); i++) {
                            Record r = vscop1.get(i);
                            if (r.esisteCampo("sel")) {

                                destMail[c] = r.leggiStringa("cmail");
                                c++;
                            }
                        }
                    }
                    if (!emailVuota) {
                        invioMail(destMail);
                    } else {
                        Toast toast = Toast.makeText(FormRubrica.this.getApplicationContext(), "Email non presente!",
                                Toast.LENGTH_LONG);
                        toast.show();
                        return;
                    }
                } else {
                    ArrayList<Record> vscop1 = frcli.vrubrica;
                    int c = 0;
                    boolean emailVuota = false;
                    //final ArrayList<Record> vsel = new ArrayList();
                    for (int i = 0; i < vscop1.size(); i++) {
                        Record r = vscop1.get(i);
                        if (r.esisteCampo("sel")) {
                            c++;
                        }
                    }
                    if (c != 0) {
                        String[] destMail = new String[c];
                        c = 0;
                        for (int i = 0; i < vscop1.size(); i++) {
                            Record r = vscop1.get(i);
                            if (r.esisteCampo("sel")) {
                                destMail[c] = r.leggiStringa("cliemail");
                                c++;
                            }
                        }
                        if (!emailVuota) {
                            invioMail(destMail);
                        } else {
                            Toast toast = Toast.makeText(FormRubrica.this.getApplicationContext(), "Email non presente!",
                                    Toast.LENGTH_LONG);
                            toast.show();
                            return;
                        }

                    }
                }
            }
        });

/*
        bshareWhat.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneWhatsapp();
            }
        });
*/


        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();

    }


    private void invioMail(String[] destinatari) {


        int c = 0;
        String[] catalogoSelezionato = new String[c];
        //final ArrayList<Record> vsel = new ArrayList();
        for (int i = 0; i < vcatalogo.size(); i++) {
            Record r = vcatalogo.get(i);
            if (r.esisteCampo("sel")) {
                c++;
            }
        }
        if (c != 0) {
            catalogoSelezionato = new String[c];
            c = 0;
            for (int i = 0; i < vcatalogo.size(); i++) {
                Record r = vcatalogo.get(i);
                if (r.esisteCampo("sel")) {
                    catalogoSelezionato[c] = r.leggiStringa("catpdfnome");
                    c++;
                }
            }
        } else {
            Toast toast = Toast.makeText(FormRubrica.this.getApplicationContext(), "Selezionare almeno un catalogo!",
                    Toast.LENGTH_LONG);
            toast.show();
            return;
        }

        String soggettomail = "";
        String testomail = "";
        soggettomail = "Invio Catalogo";
        testomail = "In allegato il catalogo delle nostre birre";

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        //emailIntent.putExtra(Intent.EXTRA_EMAIL, destinatari);
        emailIntent.putExtra(Intent.EXTRA_BCC, destinatari);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, soggettomail);
        emailIntent.putExtra(Intent.EXTRA_TEXT, testomail);
        //emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        // emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.setType("application/pdf");
        String nomepdf = "";
        for (int i = 0; i < catalogoSelezionato.length; i++) {
            nomepdf = catalogoSelezionato[i];
            File newFile = new File(getApplication().getFilesDir() + File.separator + "docs" + File.separator + nomepdf + ".pdf");
            //File newFile = new File(getApplication().getFilesDir() + File.separator + nomepdf + ".pdf");
            if (newFile.isFile()) {

                Uri contentUri = FileProvider.getUriForFile(getApplicationContext(), "jsoftware.jbeerapp.fileprovider", newFile);
                emailIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
            } else {
                return;
            }
        }

        emailIntent.setType("message/rfc822");
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //startActivity(emailIntent);
        try {
            startActivity(emailIntent);
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        //startActivity(Intent.createChooser(emailIntent, "Condividi con"));/data/data/jsoftware.jbeerapp/files/docs/catalogoBirre.pdf

    }

    private void listaCatalogoPdf() {

        vcatalogo.clear();
        Cursor cursor = Env.db.rawQuery("SELECT catpdfevento,catpdfnome,catpdfdata FROM catalogopdf"
                //+ " WHERE ccognome LIKE '%" + n + "%'"
                , null);

        while (cursor.moveToNext()) {
            Record r = new Record();
            r.insStringa("catpdfevento", cursor.getString(0));
            r.insStringa("catpdfnome", cursor.getString(1));
            r.insStringa("catpdfdata", cursor.getString(2));
            vcatalogo.add(r);
        }
        cursor.close();
        lshcatagadapter.notifyDataSetChanged();
    }

}
