package jsoftware.jbeerapp.forms;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniHTTP;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.env.funzStringa;

public class FormInvioDati extends AppCompatActivity {

    ProgressBar progress = null;
    TextView labelinfo = null;
    TextView labelinfo2 = null;
    TextView labelris1 = null;
    TextView labelris2 = null;
    TextView labelris3 = null;
    ImageView icona = null;
    ProcInvioDati proc = null;
    boolean invioincorso = false;
    String nomefilestorico = "";
    boolean soloordini = false;
    int tipodocsingolo = 0;
    int numdocsingolo = 0;
    String datadocsingolo = "";
    String postdestdocsingolo = "";

    @Override
    public void onBackPressed() {
        if (!invioincorso)
            super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_invio_dati);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("soloordini"))
            soloordini = getIntent().getExtras().getBoolean("soloordini");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("nomefilestorico"))
            nomefilestorico = getIntent().getExtras().getString("nomefilestorico");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("datadocsingolo"))
            datadocsingolo = getIntent().getExtras().getString("datadocsingolo");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("tipodocsingolo"))
            tipodocsingolo = getIntent().getExtras().getInt("tipodocsingolo");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("numdocsingolo"))
            numdocsingolo = getIntent().getExtras().getInt("numdocsingolo");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("postdestdocsingolo"))
            postdestdocsingolo = getIntent().getExtras().getString("postdestdocsingolo");

        progress = findViewById(R.id.inviodati_progress);
        labelinfo = findViewById(R.id.inviodati_labelinfo);
        labelinfo2 = findViewById(R.id.inviodati_labelinfo2);
        labelris1 = findViewById(R.id.inviodati_labelris1);
        labelris2 = findViewById(R.id.inviodati_labelris2);
        labelris3 = findViewById(R.id.inviodati_labelris3);
        icona = findViewById(R.id.inviodati_icona);

        proc = new ProcInvioDati();
        proc.execute();

/*
		if (!FunzioniJazzTv.dispositivoOnline(getApplicationContext()))
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(
					FormInvioDati.this);
			builder.setMessage("Connessione ad Internet assente")
					.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
								}
							});
			AlertDialog ad = builder.create();
			ad.setCancelable(false);
			ad.show();
		}
		else
		{
			proc = new ProcInvioDati();
			proc.execute();
		}
*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.form_invio_dati, menu);
        return true;
    }

    private class ProcInvioDati extends AsyncTask<String, HashMap, String> {
        @Override
        protected String doInBackground(String... arg) {
            invioincorso = true;
            boolean connok = false;
            String err = "";
            if (Env.tiposcambiodati == 0) {
                // sim dati
                if (FunzioniJBeerApp.dispositivoOnline(FormInvioDati.this.getApplicationContext())) {
                    connok = true;
                } else {
                    // se attivata WIFI la disabilita
                    if (FunzioniJBeerApp.tipoConnessione(FormInvioDati.this.getApplicationContext()).equals("WIFI")) {
                        HashMap h = new HashMap();
                        h.put("label", "Disattivazione Wi-FI...");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);
                        FunzioniJBeerApp.disattivaWiFi(FormInvioDati.this.getApplicationContext());
                        int ntent = 0;
                        System.out.println("tipo conn:" + FunzioniJBeerApp.tipoConnessione(FormInvioDati.this.getApplicationContext()));
                        while (FunzioniJBeerApp.tipoConnessione(FormInvioDati.this.getApplicationContext()).equals("WIFI") && ntent < 20) {
                            System.out.println("tipo conn:" + FunzioniJBeerApp.tipoConnessione(FormInvioDati.this.getApplicationContext()));
                            try {
                                Thread.currentThread().sleep(1000);
                            } catch (Exception esleep) {
                            }
                            ntent++;
                        }
                    }
                    int ntentonline = 0;
                    while (!FunzioniJBeerApp.dispositivoOnline(getApplicationContext()) && ntentonline < 10) {
                        try {
                            Thread.currentThread().sleep(1000);
                        } catch (Exception esleep) {
                        }
                        ntentonline++;
                    }
                    if (FunzioniJBeerApp.dispositivoOnline(getApplicationContext())) {
                        connok = true;
                    } else {
                        err = "Dispositivo non collegato ad Internet\nImpossibile inviare i dati ora";
                        HashMap h = new HashMap();
                        h.put("label", "");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "N");
                        h.put("indeterminate", "N");
                        h.put("error", err);
                        this.publishProgress(h);
                        invioincorso = false;
                    }
                }
            } else {
                // WIFI
                if (FunzioniJBeerApp.dispositivoOnline(FormInvioDati.this.getApplicationContext())) {
                    connok = true;
                } else {
                    boolean wifion = false;
                    if (FunzioniJBeerApp.tipoConnessione(FormInvioDati.this.getApplicationContext()).equals("WIFI")) {
                        wifion = true;
                    } else {
                        HashMap h = new HashMap();
                        h.put("label", "Attivazione Wi-FI...");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);
                        FunzioniJBeerApp.attivaWiFi(FormInvioDati.this.getApplicationContext());
                        int ntent = 0;

                        while (!FunzioniJBeerApp.tipoConnessione(FormInvioDati.this.getApplicationContext()).equals("WIFI") && ntent < 20) {
                            Log.v("JBEERAPP", "controllo " + ntent + " attivazione wifi");
                            try {
                                Thread.currentThread().sleep(1000);
                            } catch (Exception esleep) {
                            }
                            ntent++;
                        }
                        if (!FunzioniJBeerApp.tipoConnessione(FormInvioDati.this.getApplicationContext()).equals("WIFI")) {
                            h = new HashMap();
                            h.put("label", "");
                            h.put("label2", "");
                            h.put("labelris1", "");
                            h.put("labelris2", "");
                            h.put("labelris3", "");
                            h.put("val", "0");
                            h.put("max", "0");
                            h.put("visible", "N");
                            h.put("indeterminate", "N");
                            h.put("error", "Attivazione Wi-FI fallita");
                            this.publishProgress(h);
                            invioincorso = false;
                        } else {
                            wifion = true;
                        }
                    }
                    if (wifion) {
                        // controlla rete
                        String ssid = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(FormInvioDati.this.getApplicationContext());
                        if (ssid.equals(Env.ssidscambiodati)) {
                            connok = true;
                        } else {
                            HashMap h = new HashMap();
                            h.put("label", "Connessione a Wi-FI scambio dati...");
                            h.put("label2", "");
                            h.put("labelris1", "");
                            h.put("labelris2", "");
                            h.put("labelris3", "");
                            h.put("val", "0");
                            h.put("max", "0");
                            h.put("visible", "S");
                            h.put("indeterminate", "S");
                            h.put("error", "");
                            this.publishProgress(h);
                            FunzioniJBeerApp.connettiReteWiFi(getApplicationContext(), Env.ssidscambiodati);
                            String rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                            int ntent = 0;
                            while (!rc.equals(Env.ssidscambiodati) && ntent < 30) {
                                Log.v("JBEERAPP", "tentativo " + ntent + " connessione " + Env.ssidscambiodati);
                                FunzioniJBeerApp.connettiReteWiFi(getApplicationContext(), Env.ssidscambiodati);
                                try {
                                    Thread.currentThread().sleep(1000);
                                } catch (Exception esleep) {
                                }
                                rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                                if (rc.equals(Env.ssidscambiodati))
                                    connok = true;
                                ntent++;
                            }
                            if (!connok) {
                                h = new HashMap();
                                h.put("label", "");
                                h.put("label2", "");
                                h.put("labelris1", "");
                                h.put("labelris2", "");
                                h.put("labelris3", "");
                                h.put("val", "0");
                                h.put("max", "0");
                                h.put("visible", "N");
                                h.put("indeterminate", "N");
                                h.put("error", "Connessione Wi-FI a rete " + Env.ssidscambiodati + " fallita");
                                this.publishProgress(h);
                                invioincorso = false;
                            }
                        }
                    }
                }
            }

            if (connok) {
                try {
                    Thread.currentThread().sleep(2000);
                } catch (Exception esleep) {
                }

                if (soloordini)
                    FunzioniJBeerApp.salvaLog("Trasmissione dati solo ordini");
                else if (numdocsingolo > 0)
                    FunzioniJBeerApp.salvaLog("Trasmissione dati trasferimento tra automezzi");
                else
                    FunzioniJBeerApp.salvaLog("Trasmissione dati completa");

                int ninc = 0;
                int nddt = 0;
                int nfatt = 0;
                int nordcli = 0;
                int nordsede = 0;
                int nxe = 0;
                int ntrasfvs = 0;
                int ntrasfda = 0;
                int nscasede = 0;
                int ncontatti = 0;
                int nclinuovi = 0;
                boolean climodificati = false;

                Cursor cursorpar = Env.db
                        .rawQuery(
                                "SELECT pedcod,user,pwd,serverjcloud FROM datiterm",
                                null);
                cursorpar.moveToFirst();
                String tcod = cursorpar.getString(0);
                String user = cursorpar.getString(1);
                String pwd = cursorpar.getString(2);
                String server = cursorpar.getString(3);
                cursorpar.close();

                // pulizia cache
                File fscadir = new File(getApplicationContext().getCacheDir() + File.separator + "scarico");
                if (!fscadir.exists())
                    fscadir.mkdir();
                else {
                    File[] lfs = fscadir.listFiles();
                    for (int i = 0; i < lfs.length; i++) {
                        lfs[i].delete();
                    }
                }

                // creazione directory storico scarichi
                File fssdir = new File(getApplicationContext().getFilesDir() + File.separator + "storico_scarico");
                if (!fssdir.exists())
                    fssdir.mkdir();

                if (nomefilestorico.equals("")) {
                    // pulizia directory storico_scarico files piu vecchi di 30 giorni
                    String[] lfs = fssdir.list();
                    for (int i = 0; i < lfs.length; i++) {
                        File fs = new File(getApplicationContext().getFilesDir() + File.separator + "storico_scarico" + File.separator + lfs[i]);
                        long tsf = fs.lastModified();
                        long tsc = System.currentTimeMillis() - 2592000000L; // timestamp 30 giorni fa
                        if (tsf < tsc) {
                            fs.delete();
                        }
                    }
                }

                String dtoggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
                SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
                SimpleDateFormat dh2 = new SimpleDateFormat("HH:mm:ss");
                String ora = dh.format(new Date());

                boolean fileok = true;

                // prepara file di scarico
                String nf = "scarico_" + (new Data()).formatta(Data.AAAAMMGG) + "_" + dh.format(new Date()).replace(":", "") + ".txt";
                if (numdocsingolo > 0)
                    nf = "movtrasfterm.txt";
                String nfzip = "dati_" + tcod + "_" + (new Data()).formatta(Data.AAAAMMGG) + "_" + dh2.format(new Date()).replace(":", "") + ".zip";
                HashMap<String, String> hclinuovi = new HashMap();
                if (nomefilestorico.equals("")) {
                    // lettura documenti emessi da inviare
                    int ndocs = 0;
                    if (soloordini) {
                        Cursor cursor = Env.db.rawQuery(
                                "SELECT COUNT(*) FROM movimenti WHERE movtipo IN (12) AND movtrasf<>'S'", null);
                        if (cursor.moveToFirst())
                            ndocs = cursor.getInt(0);
                        cursor.close();
                    } else if (numdocsingolo > 0) {
                        ndocs = 1;
                    } else {
                        Cursor cursor = Env.db.rawQuery(
                                "SELECT COUNT(*) FROM movimenti WHERE movtrasf<>'S'", null);
                        if (cursor.moveToFirst())
                            ndocs = cursor.getInt(0);
                        cursor.close();
                    }

                    HashMap h = new HashMap();
                    h.put("label", "Esportazione movimenti...");
                    h.put("label2", "");
                    h.put("labelris1", "");
                    h.put("labelris2", "");
                    h.put("labelris3", "");
                    h.put("val", "0");
                    h.put("max", "" + ndocs);
                    h.put("visible", "S");
                    h.put("indeterminate", "N");
                    h.put("error", err);
                    this.publishProgress(h);

                    FileOutputStream fos = null;

                    try {
                        fos = new FileOutputStream(getApplicationContext().getCacheDir() + File.separator + "scarico" +
                                File.separator + nf);
                        int prog = 0;
                        String qry = "SELECT movtipo,movdoc,movsez,movdata,movnum,movsospeso," +
                                "movclicod,movdestcod,movspeseinc,movspesebolli,movspesetrasp,movspesecauz,movcodlist,movnprz," + // 13
                                "movbancacod,movagenzia,movcab,movpagcod,movaspbeni,movnumcolli,movpeso," +
                                "movnote1,movnote2,movnote3,movnote4,movnote5,movnote6,movtotlordomerce,movtotsconticli," +
                                "movtotscontiart,movomaggi,movtotmerce,movtotnetti,movtotspeseinc,movtotspesetrasp," +
                                "movtotspesebol,movtotimp,movtotiva,movtotale,movaddeb,movaccred,movnettoapag,movacconto,movnrif,movart62,movkey,movdocora,movcesscod,movdatacons,movfor,movabbuoni,movdep " + // 51
                                " FROM movimenti WHERE movtrasf <> 'S' " + (soloordini ? "AND movtipo = 12" : "") + " ORDER BY movtipo,movdata,movnum";
                        if (numdocsingolo > 0) {
                            qry = "SELECT movtipo,movdoc,movsez,movdata,movnum,movsospeso," +
                                    "movclicod,movdestcod,movspeseinc,movspesebolli,movspesetrasp,movspesecauz,movcodlist,movnprz," + // 13
                                    "movbancacod,movagenzia,movcab,movpagcod,movaspbeni,movnumcolli,movpeso," +
                                    "movnote1,movnote2,movnote3,movnote4,movnote5,movnote6,movtotlordomerce,movtotsconticli," +
                                    "movtotscontiart,movomaggi,movtotmerce,movtotnetti,movtotspeseinc,movtotspesetrasp," +
                                    "movtotspesebol,movtotimp,movtotiva,movtotale,movaddeb,movaccred,movnettoapag,movacconto,movnrif,movart62,movkey,movdocora,movcesscod,movdatacons,movfor,movabbuoni,movdep " +
                                    " FROM movimenti WHERE movtipo = " + tipodocsingolo + " AND movnum = " + numdocsingolo + " AND movdata = '" + datadocsingolo + "'";
                        }
                        Log.v("JBEERAPP", "QUERY INVIO:" + qry);
                        Cursor cursor = Env.db.rawQuery(qry, null);
                        while (cursor.moveToNext()) {
                            if (cursor.getInt(0) == 0 || cursor.getInt(0) == 1 || cursor.getInt(0) == 4)
                                nddt++;
                            else if (cursor.getInt(0) == 2)
                                nfatt++;
                            else if (cursor.getInt(0) == 12)
                                nordsede++;
                            else if (cursor.getInt(0) == 20)
                                nordcli++;
                            else if (cursor.getInt(0) == 6)
                                nscasede++;
                            else if (cursor.getInt(0) == 5)
                                nxe++;
                            else if (cursor.getInt(0) == 5)
                                ntrasfvs++;
                            else if (cursor.getInt(0) == 7)
                                ntrasfda++;

                            // record movimento
                            String rmv = "MV";
                            rmv += "0       ";
                            rmv += "0000-00-00";
                            // rif.esterni
                            if (!cursor.isNull(43) && !cursor.getString(43).equals("")) {
                                rmv += funzStringa.riempiStringa(cursor.getString(43), 8, funzStringa.SX, ' ');
                                rmv += cursor.getString(3);
                            } else {
                                rmv += "0       ";
                                rmv += "0000-00-00";
                            }
                            // numerazione
                            if (cursor.getInt(0) == 6) {
                                // scarico a sede
                                rmv += funzStringa.riempiStringa(cursor.getString(2), 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("" + cursor.getInt(4), 8, funzStringa.SX, ' ');
                                rmv += cursor.getString(3);
                                rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                rmv += "0000-00-00";
                                rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                rmv += "0000-00-00";
                                rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                rmv += "0000-00-00";
                            } else if (cursor.getInt(0) == 5 || cursor.getInt(0) == 7 || cursor.getInt(0) == 8 ||
                                    cursor.getInt(0) == 9 || cursor.getInt(0) == 10 || cursor.getInt(0) == 11) {
                                // ddt carico, trasf.,ordine, rett.car
                                if (cursor.getString(22).equals("CARAUTO")) {
                                    rmv += funzStringa.riempiStringa(cursor.getString(24), 10, funzStringa.SX, ' ');
                                    rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                    rmv += cursor.getString(3);
                                    rmv += funzStringa.riempiStringa(cursor.getString(24), 10, funzStringa.SX, ' ');
                                    rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                    rmv += cursor.getString(3);
                                    rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                    rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                    rmv += "0000-00-00";
                                    rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                    rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                    rmv += "0000-00-00";
                                } else {
                                    rmv += funzStringa.riempiStringa(cursor.getString(2), 10, funzStringa.SX, ' ');
                                    rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                    rmv += cursor.getString(3);
                                    rmv += funzStringa.riempiStringa(cursor.getString(2), 10, funzStringa.SX, ' ');
                                    rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                    rmv += cursor.getString(3);
                                    rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                    rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                    rmv += "0000-00-00";
                                    rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                    rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                    rmv += "0000-00-00";
                                }
                            } else if (cursor.getInt(0) == 0 || cursor.getInt(0) == 4) {
                                // ddt val.o qta o ddt reso
                                rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                rmv += "0000-00-00";
                                rmv += funzStringa.riempiStringa(cursor.getString(2), 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("" + cursor.getInt(4), 8, funzStringa.SX, ' ');
                                rmv += cursor.getString(3);
                                rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                rmv += "0000-00-00";
                                rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                rmv += "0000-00-00";
                            } else if (cursor.getInt(0) == 1 || cursor.getInt(0) == 12 || cursor.getInt(0) == 20) {
                                // ordine cliente, ordine a sede
                                rmv += funzStringa.riempiStringa(cursor.getString(2), 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("" + cursor.getInt(4), 8, funzStringa.SX, ' ');
                                rmv += cursor.getString(3);
                                rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                rmv += "0000-00-00";
                                rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                rmv += "0000-00-00";
                                rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                rmv += "0000-00-00";
                            } else {
                                // fattura o corrisp.
                                rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                rmv += "0000-00-00";
                                rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                rmv += "0000-00-00";
                                rmv += funzStringa.riempiStringa(cursor.getString(2), 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("" + cursor.getInt(4), 8, funzStringa.SX, ' ');
                                rmv += cursor.getString(3);
                                rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                rmv += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                rmv += "0000-00-00";
                            }
                            if (cursor.getString(22).equals("CARAUTO")) {
                                // carico da altro automezzo del terminale di destinazione generato in automatico
                                // deposito
                                rmv += funzStringa.riempiStringa("T" + cursor.getString(25), 10, funzStringa.SX, ' ');
                                // punto em.doc.
                                rmv += funzStringa.riempiStringa("T" + cursor.getString(25), 10, funzStringa.SX, ' ');
                            } else {
                                // deposito
                                if (Env.orddepsede && (cursor.getInt(0) == 12 || cursor.getInt(0) == 20))
                                    rmv += funzStringa.riempiStringa(Env.depsede, 10, funzStringa.SX, ' ');
                                else
                                    rmv += funzStringa.riempiStringa(Env.depcod, 10, funzStringa.SX, ' ');
                                // punto em.doc.
                                rmv += funzStringa.riempiStringa(Env.pedcod, 10, funzStringa.SX, ' ');
                            }
                            // tipo doc.
                            if (cursor.getString(22).equals("CARAUTO")) {
                                rmv += funzStringa.riempiStringa(cursor.getString(23), 10, funzStringa.SX, ' ');
                            } else {
                                rmv += funzStringa.riempiStringa(cursor.getString(1), 10, funzStringa.SX, ' ');
                            }
                            // operatore
                            rmv += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                            // stampa boll.
                            rmv += "N";
                            // stato
                            if (cursor.getInt(0) == 9 || cursor.getInt(0) == 12 || cursor.getInt(0) == 20)
                                rmv += "2";
                            else
                                rmv += "0";
                            // sospeso
                            if (cursor.getString(5).equals("S"))
                                rmv += "1";
                            else
                                rmv += "0";
                            rmv += "\r\n";
                            fos.write(rmv.getBytes());

                            // record testata
                            String rts = "TS";
                            // codice cliente
                            rts += funzStringa.riempiStringa(cursor.getString(6), 10, funzStringa.SX, ' ');
                            // addebito spese inc
                            rts += cursor.getString(8);
                            // addebito spese bolli
                            rts += cursor.getString(9);
                            // addebito spese trasp.
                            rts += cursor.getString(10);
                            // addebito spese cauz.
                            rts += cursor.getString(11);
                            // codice e pos.listino
                            rts += funzStringa.riempiStringa(cursor.getString(12), 10, funzStringa.SX, ' ');
                            rts += "" + cursor.getInt(13);
                            // valuta e cambio
                            rts += "Euro      ";

                            rts += funzStringa.riempiStringa("1,000000", 15, funzStringa.DX, ' ');
                            // codice agente
                            rts += funzStringa.riempiStringa(Env.agecod, 10, funzStringa.SX, ' ');
                            // lingua
                            rts += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                            // banca
                            rts += funzStringa.riempiStringa(cursor.getString(14), 10, funzStringa.SX, ' ');
                            rts += funzStringa.riempiStringa(cursor.getString(15), 50, funzStringa.SX, ' ');
                            rts += funzStringa.riempiStringa(cursor.getString(16), 7, funzStringa.SX, ' ');
                            String codiva = "";
                            String raggrddt = "S";
                            String mastro = "", conto = "", sottoconto = "";
                            Boolean clinuovo = false;
                            String[] pars = new String[1];
                            pars[0] = cursor.getString(6);
                            Cursor cc = Env.db.rawQuery("SELECT cliraggrddt,clicodiva,climastro,cliconto,clisottoconto,clinuovo FROM clienti WHERE clicodice = @p1", pars);
                            if (cc.moveToFirst()) {
                                codiva = cc.getString(1);
                                if (cc.getString(0).equals("N"))
                                    raggrddt = "N";
                                mastro = cc.getString(2);
                                conto = cc.getString(3);
                                sottoconto = cc.getString(4);
                                clinuovo = (cc.getInt(5) == 1);
                            }
                            cc.close();
                            // codiva
                            rts += funzStringa.riempiStringa(codiva, 3, funzStringa.SX, ' ');
                            // cod.pagamento
                            rts += funzStringa.riempiStringa(cursor.getString(17), 10, funzStringa.SX, ' ');
                            // cod.destinazione
                            rts += funzStringa.riempiStringa(cursor.getString(7), 10, funzStringa.SX, ' ');
                            // raggr.bolle
                            rts += raggrddt;
                            rts += "\r\n";
                            fos.write(rts.getBytes());

                            if (!cursor.getString(47).equals("")) {
                                // record cessionario
                                String rcs = "CS";
                                rcs += funzStringa.riempiStringa(cursor.getString(47), 10, funzStringa.SX, ' ');
                                rcs += "\r\n";
                                fos.write(rcs.getBytes());
                            }

                            // records righemov
                            String[] parsd = new String[5];
                            parsd[0] = "" + cursor.getInt(0);
                            parsd[1] = cursor.getString(1);
                            parsd[2] = cursor.getString(2);
                            parsd[3] = cursor.getString(3);
                            parsd[4] = "" + cursor.getInt(4);
                            int nriga = 1;
                            ArrayList<Record> vdescrlunghe = new ArrayList();
                            Cursor cd = Env.db.rawQuery(
                                    "SELECT rmriga,rmtiporiga,rmartcod,rmlotto," +
                                            "rmcaumag,rmcolli,rmpezzi,rmcontenuto,rmqta,rmprz,rmartsc1,rmartsc2,rmscvend,rmcodiva," + //14
                                            "rmaliq,rmclisc1,rmclisc2,rmclisc3,rmlordo,rmnetto,rmnettoivato,rmaccisa,rmgrado,rmplato " +
                                            "FROM righemov WHERE rmmovtipo=? AND rmmovdoc=? AND rmmovsez=? AND rmmovdata=? AND rmmovnum=? ORDER BY rmriga",
                                    parsd);
                            while (cd.moveToNext()) {
                                int nrigaatt = nriga;
                                String rm = "MG";
                                // sezionale
                                rm += funzStringa.riempiStringa(cursor.getString(2), 10, funzStringa.SX, ' ');
                                // data
                                rm += cursor.getString(3);
                                // numero
                                if (cursor.getInt(0) == 5 || cursor.getInt(0) == 6 || cursor.getInt(0) == 7 || cursor.getInt(0) == 8 || cursor.getInt(0) == 9 || cursor.getInt(0) == 11)
                                    rm += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                else
                                    rm += funzStringa.riempiStringa("" + cursor.getInt(4), 8, funzStringa.SX, ' ');
                                // tipo sogg
                                if (!cursor.getString(6).equals(""))
                                    rm += "1";
                                else
                                    rm += "7";
                                // codice cli
                                rm += funzStringa.riempiStringa(cursor.getString(6), 10, funzStringa.SX, ' ');
                                // n.riga (5)
                                rm += funzStringa.riempiStringa("" + (nriga++), 5, funzStringa.SX, ' ');
                                // cod.esecutore
                                rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                // tipo riga (2)
                                rm += " 1";
                                // cod.art.
                                rm += funzStringa.riempiStringa(cd.getString(2), 20, funzStringa.SX, ' ');
                                String descr = "";
                                String um = "";
                                double peso = 0;
                                String[] parsa = new String[1];
                                parsa[0] = cd.getString(2);
                                Cursor cart = Env.db.rawQuery(
                                        "SELECT artdescr,artum,artpeso FROM articoli WHERE artcod = ?", parsa);
                                if (cart.moveToFirst()) {
                                    descr = cart.getString(0);
                                    um = cart.getString(1);
                                    peso = cart.getDouble(2);
                                }
                                cart.close();
                                // cod.art mov.
                                String artmov = cd.getString(2);
//								if (artmov.length() == 4)
//								{
//									String acodparz = artmov.substring(0, 3);
//									if (acodparz.compareTo("110") >= 0 && acodparz.compareTo("180") <= 0)
//									{
//										artmov = acodparz;
//									}
//								}
                                rm += funzStringa.riempiStringa(artmov, 20, funzStringa.SX, ' ');
                                // codice voce
                                rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                // lotto
                                rm += funzStringa.riempiStringa(cd.getString(3), 20, funzStringa.SX, ' ');
                                // cau.mag.
                                String cm = Env.cmvend;
                                if (cd.getString(4).equals("VE"))//vendita
                                    cm = Env.cmvend;
                                else if (cd.getString(4).equals("RV"))// reso vendibile
                                    cm = Env.cmresov;
                                else if (cd.getString(4).equals("RN"))    //reso non vendibile
                                    cm = Env.cmresonv;
                                else if (cd.getString(4).equals("RC"))// scarico sede per merce scaduta
                                    cm = Env.cmscaduto;
/*                                else if (cd.getString(4).equals("CP"))// calo peso
                                    cm = Env.cmcalipeso;*/
                                else if (cd.getString(4).equals("AN"))// annullamento documento
                                    cm = Env.cmann;
                                else if (cd.getString(4).equals("OM")) // omaggi
                                    cm = Env.cmomaggi;
                                else if (cd.getString(4).equals("OT")) // omaggi
                                    cm = Env.cmomaggi;
                                else if (cd.getString(4).equals("SM")) // sconto merce
                                    cm = Env.cmsm;
                                else if (cd.getString(4).equals("TU"))// trasferimento a mezzo
                                    cm = Env.cmtrasfamezzo;
                                else if (cd.getString(4).equals("TE"))// trasferimento da mezzo
                                    cm = Env.cmtrasfdamezzo;
                                else if (cd.getString(4).equals("SS")) // scarico sede
                                    cm = Env.cmscaricosede;
                                else if (cd.getString(4).equals("PE")) // scarico a sede per rotture danneggiamenti
                                    cm = Env.cmdistrmerce;
                                else if (cd.getString(4).equals("SR"))// scarico a sede
                                    cm = Env.cmscaricosede;
                                else if (cd.getString(4).equals("DC")) // ddt di carico
                                    cm = Env.cmddtcarico;
                                else if (cd.getString(4).equals("OS")) // ordini a sede
                                    cm = Env.cmordinesede;
                                else if (cd.getString(4).equals("OC")) // ordini clienti
                                    cm = Env.cmordinesede;
                                else if (cd.getString(4).equals("CO")) // conferme ordini a sede
                                    cm = Env.cmordinesede;
                                else if (cd.getString(4).equals("VQ")) // ddt quantità
                                {
                                    cm = Env.cmddtqta;
                                    if (Env.cmddtqta.equals(""))
                                        cm = Env.cmvend;
                                } else if (cd.getString(4).equals("XV")) // rettifica vendita
                                    cm = "RETVEND";
                                else if (cd.getString(4).equals("XR")) // rettifica reso vendibile
                                    cm = "RETRESOV";
                                else if (cd.getString(4).equals("XN")) // rettifica reso non vendibile
                                    cm = "RETRESONV";
                                // RETTDDT
                                rm += funzStringa.riempiStringa(cm, 10, funzStringa.SX, ' ');
                                // descr.
                                rm += funzStringa.riempiStringa(descr, 50, funzStringa.SX, ' ');
                                // um (4)
                                rm += funzStringa.riempiStringa(um, 4, funzStringa.SX, ' ');
                                // colli (5)
                                rm += funzStringa.riempiStringa("" + cd.getInt(5), 5, funzStringa.DX, ' ');
                                // pezzi (15)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(6), "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');//Utilita.riempi(Utilita.formatta(rsr.GetFloat(6), "#######0.00", Utilita.SEGNO_DX), 15, " ", 1);
                                // contenuto (15)
                                double cont = cd.getDouble(7);
                                double qta = cd.getDouble(8);
                                if (cont == 0) {
                                    if (peso != 0)
                                        cont = OpValute.arrotondaMat(peso * qta, 3);
                                    else
                                        cont = qta;
                                }
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cont, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // qta (15)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(qta, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // qta evasa (15)
                                if (cursor.getInt(0) == 9 || cursor.getInt(0) == 12 || cursor.getInt(0) == 20)
                                    rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                else
                                    rm += funzStringa.riempiStringa(Formattazione.formatta(qta, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // cod.listino
                                rm += funzStringa.riempiStringa(cursor.getString(12), 10, funzStringa.SX, ' ');
                                // posprezzo (1)
                                rm += "" + cursor.getInt(13);
                                // prezzo (15)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(9), "#######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // prezzo (15)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(9), "#######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // %sconto art.1 (8)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(10), "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // %sconto art.2 (8)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(11), "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // %sconto art.3 (8)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(12), "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // %magg.(8)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // %provv (8)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // codiva
                                rm += funzStringa.riempiStringa(cd.getString(13), 3, funzStringa.SX, ' ');
                                // sconto in valore
                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // sconto in valore
                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // %sconto cli.1 (8)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(15), "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // %sconto cli.2 (8)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(16), "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // %sconto cli.3 (8)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(17), "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // %sconto prom. (8)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // cod.agente 1
                                rm += funzStringa.riempiStringa(Env.agecod, 10, funzStringa.SX, ' ');
                                // %provv.1 (8)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // cod.agente 2
                                rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                // %provv.2 (8)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // cod.agente 3
                                rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                // %provv.3 (8)
                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // lordo
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(18), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // lordo
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(18), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // netto
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(19), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // netto
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(19), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // netto ivato
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(19), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // colli evasi (5)
                                rm += funzStringa.riempiStringa("" + cd.getInt(5), 5, funzStringa.DX, ' ');
                                //grado
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(22), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        //plato
                                rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(23), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                rm += "\r\n";
                                fos.write(rm.getBytes());
/*                                if (cursor.getInt(0) == 20 && Env.orddescragg) {
                                    // descrizioni aggiuntive articolo
                                    String[] parsda = new String[1];
                                    parsda[0] = cd.getString(2);
                                    Cursor cda = Env.db.rawQuery(
                                            "SELECT * FROM descrspecif WHERE dsartcod = ?", parsda);
                                    if (cda.moveToFirst()) {
                                        for (int nd = 2; nd <= 14; nd++) {
                                            if (!cda.getString(cda.getColumnIndex("dsdescr" + nd)).trim().equals("")) {
                                                int nrigaatt2 = nriga;
                                                rm = "MG";
                                                // sezionale
                                                rm += funzStringa.riempiStringa(cursor.getString(2), 10, funzStringa.SX, ' ');
                                                // data
                                                rm += cursor.getString(3);
                                                // numero
                                                if (cursor.getInt(0) == 5 || cursor.getInt(0) == 6 || cursor.getInt(0) == 7 || cursor.getInt(0) == 8 || cursor.getInt(0) == 9 || cursor.getInt(0) == 11)
                                                    rm += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                                else
                                                    rm += funzStringa.riempiStringa("" + cursor.getInt(4), 8, funzStringa.SX, ' ');
                                                // tipo sogg
                                                if (!cursor.getString(6).equals(""))
                                                    rm += "1";
                                                else
                                                    rm += "7";
                                                // codice cli
                                                rm += funzStringa.riempiStringa(cursor.getString(6), 10, funzStringa.SX, ' ');
                                                // n.riga (5)
                                                rm += funzStringa.riempiStringa("" + (nriga++), 5, funzStringa.SX, ' ');
                                                // cod.esecutore
                                                rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                                // tipo riga (2)
                                                rm += "-2";
                                                // cod.art.
                                                rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                                rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                                // codice voce
                                                rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                                // lotto
                                                rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                                // cau.mag.
                                                cm = Env.cmordinesede;
                                                rm += funzStringa.riempiStringa(cm, 10, funzStringa.SX, ' ');
                                                // descr.
                                                rm += funzStringa.riempiStringa(cda.getString(cda.getColumnIndex("dsdescr" + nd)), 50, funzStringa.SX, ' ');
                                                if (cda.getString(cda.getColumnIndex("dsdescr" + nd)).length() > 50)
                                                {
                                                    Record rdl = new Record();
                                                    rdl.insIntero("riga", nrigaatt2);
                                                    rdl.insStringa("descr", cda.getString(cda.getColumnIndex("dsdescr" + nd)));
                                                    vdescrlunghe.add(rdl);
                                                }

                                                // um (4)
                                                rm += funzStringa.riempiStringa("", 4, funzStringa.SX, ' ');
                                                // colli (5)
                                                rm += funzStringa.riempiStringa("0", 5, funzStringa.DX, ' ');
                                                // pezzi (15)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');//Utilita.riempi(Utilita.formatta(rsr.GetFloat(6), "#######0.00", Utilita.SEGNO_DX), 15, " ", 1);
                                                // contenuto (15)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                                // qta (15)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                                // qta evasa (15)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                                // cod.listino
                                                rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                                // posprezzo (1)
                                                rm += "" + cursor.getInt(13);
                                                // prezzo (15)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                                // prezzo (15)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                                // %sconto art.1 (8)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                                // %sconto art.2 (8)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                                // %sconto art.3 (8)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                                // %magg.(8)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                                // %provv (8)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                                // codiva
                                                rm += funzStringa.riempiStringa("", 3, funzStringa.SX, ' ');
                                                // sconto in valore
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                                // sconto in valore
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                                // %sconto cli.1 (8)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                                // %sconto cli.2 (8)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                                // %sconto cli.3 (8)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                                // %sconto prom. (8)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                                // cod.agente 1
                                                rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                                // %provv.1 (8)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                                // cod.agente 2
                                                rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                                // %provv.2 (8)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                                // cod.agente 3
                                                rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                                // %provv.3 (8)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                                // lordo
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                                // lordo
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                                // netto
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                                // netto
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                                // netto ivato
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                                // colli evasi (5)
                                                rm += funzStringa.riempiStringa("0", 5, funzStringa.DX, ' ');
                                                rm += "\r\n";
                                                fos.write(rm.getBytes());
                                            }
                                        }
                                    }
                                    cda.close();
                                }*/
                                // accisa
                                if (Env.calcaccisa && cd.getDouble(21) != 0) {
                                    if (Env.tipocalcaccisa == 0) {
                                        if (!Env.voceaccisa.equals("")) {
                                            // voce
                                            rm = "MG";
                                            // sezionale
                                            rm += funzStringa.riempiStringa(cursor.getString(2), 10, funzStringa.SX, ' ');
                                            // data
                                            rm += cursor.getString(3);
                                            // numero
                                            if (cursor.getInt(0) == 5 || cursor.getInt(0) == 6 || cursor.getInt(0) == 7 || cursor.getInt(0) == 8 || cursor.getInt(0) == 9 || cursor.getInt(0) == 11)
                                                rm += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                            else
                                                rm += funzStringa.riempiStringa("" + cursor.getInt(4), 8, funzStringa.SX, ' ');
                                            // tipo sogg
                                            if (!cursor.getString(6).equals(""))
                                                rm += "1";
                                            else
                                                rm += "7";
                                            // codice cli
                                            rm += funzStringa.riempiStringa(cursor.getString(6), 10, funzStringa.SX, ' ');
                                            // n.riga (5)
                                            rm += funzStringa.riempiStringa("" + (nriga++), 5, funzStringa.SX, ' ');
                                            // cod.esecutore
                                            rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                            // tipo riga (2)
                                            rm += " 0";
                                            // cod.art.
                                            rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                            rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                            // codice voce
                                            rm += funzStringa.riempiStringa(Env.voceaccisa, 10, funzStringa.SX, ' ');
                                            // lotto
                                            rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                            // cau.mag.
                                            cm = Env.cmvend;
                                            if (cd.getString(4).equals("VE"))//vendita
                                                cm = Env.cmvend;
                                            else if (cd.getString(4).equals("RV"))// reso vendibile
                                                cm = Env.cmresov;
                                            else if (cd.getString(4).equals("RN"))    //reso non vendibile
                                                cm = Env.cmresonv;
                                            else if (cd.getString(4).equals("RC"))// scarico sede per merce scaduta
                                                cm = Env.cmscaduto;
                                            else if (cd.getString(4).equals("AN"))// annullamento documento
                                                cm = Env.cmann;
                                            else if (cd.getString(4).equals("OM")) // omaggi
                                                cm = Env.cmomaggi;
                                            else if (cd.getString(4).equals("OT")) // omaggi
                                                cm = Env.cmomaggi;
                                            else if (cd.getString(4).equals("SM")) // sconto merce
                                                cm = Env.cmsm;
                                            else if (cd.getString(4).equals("TU"))// trasferimento a mezzo
                                                cm = Env.cmtrasfamezzo;
                                            else if (cd.getString(4).equals("TE"))// trasferimento da mezzo
                                                cm = Env.cmtrasfdamezzo;
                                            else if (cd.getString(4).equals("SS")) // scarico sede
                                                cm = Env.cmscaricosede;
                                            else if (cd.getString(4).equals("PE")) // scarico a sede per rotture danneggiamenti
                                                cm = Env.cmdistrmerce;
                                            else if (cd.getString(4).equals("SR"))// scarico a sede
                                                cm = Env.cmscaricosede;
                                            else if (cd.getString(4).equals("DC")) // ddt di carico
                                                cm = Env.cmddtcarico;
                                            else if (cd.getString(4).equals("OS")) // ordini a sede
                                                cm = Env.cmordinesede;
                                            else if (cd.getString(4).equals("OC")) // ordini clienti
                                                cm = Env.cmordinesede;
                                            else if (cd.getString(4).equals("CO")) // conferme ordini a sede
                                                cm = Env.cmordinesede;
                                            else if (cd.getString(4).equals("VQ")) // ddt quantità
                                            {
                                                cm = Env.cmddtqta;
                                                if (Env.cmddtqta.equals(""))
                                                    cm = Env.cmvend;
                                            } else if (cd.getString(4).equals("XV")) // rettifica vendita
                                                cm = "RETVEND";
                                            else if (cd.getString(4).equals("XR")) // rettifica reso vendibile
                                                cm = "RETRESOV";
                                            else if (cd.getString(4).equals("XN")) // rettifica reso non vendibile
                                                cm = "RETRESONV";
                                            // RETTDDT
                                            rm += funzStringa.riempiStringa(cm, 10, funzStringa.SX, ' ');
                                            // descr.
                                            rm += funzStringa.riempiStringa("ACCISA", 50, funzStringa.SX, ' ');
                                            // um (4)
                                            rm += funzStringa.riempiStringa("", 4, funzStringa.SX, ' ');
                                            // colli (5)
                                            rm += funzStringa.riempiStringa("0", 5, funzStringa.DX, ' ');
                                            // pezzi (15)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');//Utilita.riempi(Utilita.formatta(rsr.GetFloat(6), "#######0.00", Utilita.SEGNO_DX), 15, " ", 1);
                                            // contenuto (15)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // qta (15)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(1, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // qta evasa (15)
                                            if (cursor.getInt(0) == 9 || cursor.getInt(0) == 12 || cursor.getInt(0) == 20)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            else
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(1, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // cod.listino
                                            rm += funzStringa.riempiStringa(cursor.getString(12), 10, funzStringa.SX, ' ');
                                            // posprezzo (1)
                                            rm += "" + cursor.getInt(13);
                                            // prezzo (15)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "#######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // prezzo (15)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "#######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // %sconto art.1 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %sconto art.2 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %sconto art.3 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %magg.(8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %provv (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // codiva
                                            rm += funzStringa.riempiStringa(cd.getString(13), 3, funzStringa.SX, ' ');
                                            // sconto in valore
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // sconto in valore
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // %sconto cli.1 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %sconto cli.2 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %sconto cli.3 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %sconto prom. (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // cod.agente 1
                                            rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                            // %provv.1 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // cod.agente 2
                                            rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                            // %provv.2 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // cod.agente 3
                                            rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                            // %provv.3 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // lordo
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // lordo
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // netto
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // netto
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // netto ivato
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // colli evasi (5)
                                            rm += funzStringa.riempiStringa("0", 5, funzStringa.DX, ' ');
                                            rm += "\r\n";
                                            fos.write(rm.getBytes());
                                        } else {
                                            // non codificato
                                            rm = "MG";
                                            // sezionale
                                            rm += funzStringa.riempiStringa(cursor.getString(2), 10, funzStringa.SX, ' ');
                                            // data
                                            rm += cursor.getString(3);
                                            // numero
                                            if (cursor.getInt(0) == 5 || cursor.getInt(0) == 6 || cursor.getInt(0) == 7 || cursor.getInt(0) == 8 || cursor.getInt(0) == 9 || cursor.getInt(0) == 11)
                                                rm += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                            else
                                                rm += funzStringa.riempiStringa("" + cursor.getInt(4), 8, funzStringa.SX, ' ');
                                            // tipo sogg
                                            if (!cursor.getString(6).equals(""))
                                                rm += "1";
                                            else
                                                rm += "7";
                                            // codice cli
                                            rm += funzStringa.riempiStringa(cursor.getString(6), 10, funzStringa.SX, ' ');
                                            // n.riga (5)
                                            rm += funzStringa.riempiStringa("" + (nriga++), 5, funzStringa.SX, ' ');
                                            // cod.esecutore
                                            rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                            // tipo riga (2)
                                            rm += "-1";
                                            // cod.art.
                                            rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                            rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                            // codice voce
                                            rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                            // lotto
                                            rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                            // cau.mag.
                                            cm = Env.cmvend;
                                            if (cd.getString(4).equals("VE"))//vendita
                                                cm = Env.cmvend;
                                            else if (cd.getString(4).equals("RV"))// reso vendibile
                                                cm = Env.cmresov;
                                            else if (cd.getString(4).equals("RN"))    //reso non vendibile
                                                cm = Env.cmresonv;
                                            else if (cd.getString(4).equals("RC"))// scarico sede per merce scaduta
                                                cm = Env.cmscaduto;
                                            else if (cd.getString(4).equals("AN"))// annullamento documento
                                                cm = Env.cmann;
                                            else if (cd.getString(4).equals("OM")) // omaggi
                                                cm = Env.cmomaggi;
                                            else if (cd.getString(4).equals("OT")) // omaggi
                                                cm = Env.cmomaggi;
                                            else if (cd.getString(4).equals("SM")) // sconto merce
                                                cm = Env.cmsm;
                                            else if (cd.getString(4).equals("TU"))// trasferimento a mezzo
                                                cm = Env.cmtrasfamezzo;
                                            else if (cd.getString(4).equals("TE"))// trasferimento da mezzo
                                                cm = Env.cmtrasfdamezzo;
                                            else if (cd.getString(4).equals("SS")) // scarico sede
                                                cm = Env.cmscaricosede;
                                            else if (cd.getString(4).equals("PE")) // scarico a sede per rotture danneggiamenti
                                                cm = Env.cmdistrmerce;
                                            else if (cd.getString(4).equals("SR"))// scarico a sede
                                                cm = Env.cmscaricosede;
                                            else if (cd.getString(4).equals("DC")) // ddt di carico
                                                cm = Env.cmddtcarico;
                                            else if (cd.getString(4).equals("OS")) // ordini a sede
                                                cm = Env.cmordinesede;
                                            else if (cd.getString(4).equals("OC")) // ordini clienti
                                                cm = Env.cmordinesede;
                                            else if (cd.getString(4).equals("CO")) // conferme ordini a sede
                                                cm = Env.cmordinesede;
                                            else if (cd.getString(4).equals("VQ")) // ddt quantità
                                            {
                                                cm = Env.cmddtqta;
                                                if (Env.cmddtqta.equals(""))
                                                    cm = Env.cmvend;
                                            } else if (cd.getString(4).equals("XV")) // rettifica vendita
                                                cm = "RETVEND";
                                            else if (cd.getString(4).equals("XR")) // rettifica reso vendibile
                                                cm = "RETRESOV";
                                            else if (cd.getString(4).equals("XN")) // rettifica reso non vendibile
                                                cm = "RETRESONV";
                                            // RETTDDT
                                            rm += funzStringa.riempiStringa(cm, 10, funzStringa.SX, ' ');
                                            // descr.
                                            rm += funzStringa.riempiStringa("ACCISA", 50, funzStringa.SX, ' ');
                                            // um (4)
                                            rm += funzStringa.riempiStringa("", 4, funzStringa.SX, ' ');
                                            // colli (5)
                                            rm += funzStringa.riempiStringa("0", 5, funzStringa.DX, ' ');
                                            // pezzi (15)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');//Utilita.riempi(Utilita.formatta(rsr.GetFloat(6), "#######0.00", Utilita.SEGNO_DX), 15, " ", 1);
                                            // contenuto (15)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // qta (15)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(1, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // qta evasa (15)
                                            if (cursor.getInt(0) == 9 || cursor.getInt(0) == 12 || cursor.getInt(0) == 20)
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            else
                                                rm += funzStringa.riempiStringa(Formattazione.formatta(1, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // cod.listino
                                            rm += funzStringa.riempiStringa(cursor.getString(12), 10, funzStringa.SX, ' ');
                                            // posprezzo (1)
                                            rm += "" + cursor.getInt(13);
                                            // prezzo (15)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "#######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // prezzo (15)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "#######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // %sconto art.1 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %sconto art.2 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %sconto art.3 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %magg.(8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %provv (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // codiva
                                            rm += funzStringa.riempiStringa(cd.getString(13), 3, funzStringa.SX, ' ');
                                            // sconto in valore
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // sconto in valore
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // %sconto cli.1 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %sconto cli.2 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %sconto cli.3 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // %sconto prom. (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // cod.agente 1
                                            rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                            // %provv.1 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // cod.agente 2
                                            rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                            // %provv.2 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // cod.agente 3
                                            rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                            // %provv.3 (8)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                            // lordo
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // lordo
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // netto
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // netto
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // netto ivato
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                            // colli evasi (5)
                                            rm += funzStringa.riempiStringa("0", 5, funzStringa.DX, ' ');
                                            rm += "\r\n";
                                            fos.write(rm.getBytes());
                                        }
                                    } else if (Env.tipocalcaccisa == 1) {
                                        // non codificato
                                        rm = "MG";
                                        // sezionale
                                        rm += funzStringa.riempiStringa(cursor.getString(2), 10, funzStringa.SX, ' ');
                                        // data
                                        rm += cursor.getString(3);
                                        // numero
                                        if (cursor.getInt(0) == 5 || cursor.getInt(0) == 6 || cursor.getInt(0) == 7 || cursor.getInt(0) == 8 || cursor.getInt(0) == 9 || cursor.getInt(0) == 11)
                                            rm += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                        else
                                            rm += funzStringa.riempiStringa("" + cursor.getInt(4), 8, funzStringa.SX, ' ');
                                        // tipo sogg
                                        if (!cursor.getString(6).equals(""))
                                            rm += "1";
                                        else
                                            rm += "7";
                                        // codice cli
                                        rm += funzStringa.riempiStringa(cursor.getString(6), 10, funzStringa.SX, ' ');
                                        // n.riga (5)
                                        rm += funzStringa.riempiStringa("" + (nriga++), 5, funzStringa.SX, ' ');
                                        // cod.esecutore
                                        rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                        // tipo riga (2)
                                        rm += "-1";
                                        // cod.art.
                                        rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                        rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                        // codice voce
                                        rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                        // lotto
                                        rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                        // cau.mag.
                                        cm = Env.cmvend;
                                        if (cd.getString(4).equals("VE"))//vendita
                                            cm = Env.cmvend;
                                        else if (cd.getString(4).equals("RV"))// reso vendibile
                                            cm = Env.cmresov;
                                        else if (cd.getString(4).equals("RN"))    //reso non vendibile
                                            cm = Env.cmresonv;
                                        else if (cd.getString(4).equals("RC"))// scarico sede per merce scaduta
                                            cm = Env.cmscaduto;
                                        else if (cd.getString(4).equals("AN"))// annullamento documento
                                            cm = Env.cmann;
                                        else if (cd.getString(4).equals("OM")) // omaggi
                                            cm = Env.cmomaggi;
                                        else if (cd.getString(4).equals("OT")) // omaggi
                                            cm = Env.cmomaggi;
                                        else if (cd.getString(4).equals("SM")) // sconto merce
                                            cm = Env.cmsm;
                                        else if (cd.getString(4).equals("TU"))// trasferimento a mezzo
                                            cm = Env.cmtrasfamezzo;
                                        else if (cd.getString(4).equals("TE"))// trasferimento da mezzo
                                            cm = Env.cmtrasfdamezzo;
                                        else if (cd.getString(4).equals("SS")) // scarico sede
                                            cm = Env.cmscaricosede;
                                        else if (cd.getString(4).equals("PE")) // scarico a sede per rotture danneggiamenti
                                            cm = Env.cmdistrmerce;
                                        else if (cd.getString(4).equals("SR"))// scarico a sede
                                            cm = Env.cmscaricosede;
                                        else if (cd.getString(4).equals("DC")) // ddt di carico
                                            cm = Env.cmddtcarico;
                                        else if (cd.getString(4).equals("OS")) // ordini a sede
                                            cm = Env.cmordinesede;
                                        else if (cd.getString(4).equals("OC")) // ordini clienti
                                            cm = Env.cmordinesede;
                                        else if (cd.getString(4).equals("CO")) // conferme ordini a sede
                                            cm = Env.cmordinesede;
                                        else if (cd.getString(4).equals("VQ")) // ddt quantità
                                        {
                                            cm = Env.cmddtqta;
                                            if (Env.cmddtqta.equals(""))
                                                cm = Env.cmvend;
                                        } else if (cd.getString(4).equals("XV")) // rettifica vendita
                                            cm = "RETVEND";
                                        else if (cd.getString(4).equals("XR")) // rettifica reso vendibile
                                            cm = "RETRESOV";
                                        else if (cd.getString(4).equals("XN")) // rettifica reso non vendibile
                                            cm = "RETRESONV";
                                        // RETTDDT
                                        rm += funzStringa.riempiStringa(cm, 10, funzStringa.SX, ' ');
                                        // descr.
                                        rm += funzStringa.riempiStringa("ACCISA", 50, funzStringa.SX, ' ');
                                        // um (4)
                                        rm += funzStringa.riempiStringa("", 4, funzStringa.SX, ' ');
                                        // colli (5)
                                        rm += funzStringa.riempiStringa("0", 5, funzStringa.DX, ' ');
                                        // pezzi (15)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');//Utilita.riempi(Utilita.formatta(rsr.GetFloat(6), "#######0.00", Utilita.SEGNO_DX), 15, " ", 1);
                                        // contenuto (15)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // qta (15)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(1, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // qta evasa (15)
                                        if (cursor.getInt(0) == 9 || cursor.getInt(0) == 12 || cursor.getInt(0) == 20)
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        else
                                            rm += funzStringa.riempiStringa(Formattazione.formatta(1, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // cod.listino
                                        rm += funzStringa.riempiStringa(cursor.getString(12), 10, funzStringa.SX, ' ');
                                        // posprezzo (1)
                                        rm += "" + cursor.getInt(13);
                                        // prezzo (15)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "#######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // prezzo (15)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "#######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // %sconto art.1 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %sconto art.2 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %sconto art.3 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %magg.(8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %provv (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // codiva
                                        rm += funzStringa.riempiStringa(cd.getString(13), 3, funzStringa.SX, ' ');
                                        // sconto in valore
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // sconto in valore
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // %sconto cli.1 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %sconto cli.2 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %sconto cli.3 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %sconto prom. (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // cod.agente 1
                                        rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                        // %provv.1 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // cod.agente 2
                                        rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                        // %provv.2 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // cod.agente 3
                                        rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                        // %provv.3 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // lordo
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // lordo
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // netto
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // netto
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // netto ivato
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(cd.getDouble(21), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // colli evasi (5)
                                        rm += funzStringa.riempiStringa("0", 5, funzStringa.DX, ' ');
                                        rm += "\r\n";
                                        fos.write(rm.getBytes());
                                    } else if (Env.tipocalcaccisa == 2) {
                                        // descrittivo
                                        rm = "MG";
                                        // sezionale
                                        rm += funzStringa.riempiStringa(cursor.getString(2), 10, funzStringa.SX, ' ');
                                        // data
                                        rm += cursor.getString(3);
                                        // numero
                                        if (cursor.getInt(0) == 5 || cursor.getInt(0) == 6 || cursor.getInt(0) == 7 || cursor.getInt(0) == 8 || cursor.getInt(0) == 9 || cursor.getInt(0) == 11)
                                            rm += funzStringa.riempiStringa("0", 8, funzStringa.SX, ' ');
                                        else
                                            rm += funzStringa.riempiStringa("" + cursor.getInt(4), 8, funzStringa.SX, ' ');
                                        // tipo sogg
                                        if (!cursor.getString(6).equals(""))
                                            rm += "1";
                                        else
                                            rm += "7";
                                        // codice cli
                                        rm += funzStringa.riempiStringa(cursor.getString(6), 10, funzStringa.SX, ' ');
                                        // n.riga (5)
                                        rm += funzStringa.riempiStringa("" + (nriga++), 5, funzStringa.SX, ' ');
                                        // cod.esecutore
                                        rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                        // tipo riga (2)
                                        rm += "-2";
                                        // cod.art.
                                        rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                        rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                        // codice voce
                                        rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                        // lotto
                                        rm += funzStringa.riempiStringa("", 20, funzStringa.SX, ' ');
                                        // cau.mag.
                                        cm = Env.cmvend;
                                        if (cd.getString(4).equals("VE"))//vendita
                                            cm = Env.cmvend;
                                        else if (cd.getString(4).equals("RV"))// reso vendibile
                                            cm = Env.cmresov;
                                        else if (cd.getString(4).equals("RN"))    //reso non vendibile
                                            cm = Env.cmresonv;
                                        else if (cd.getString(4).equals("RC"))// scarico sede per merce scaduta
                                            cm = Env.cmscaduto;
                                        else if (cd.getString(4).equals("AN"))// annullamento documento
                                            cm = Env.cmann;
                                        else if (cd.getString(4).equals("OM")) // omaggi
                                            cm = Env.cmomaggi;
                                        else if (cd.getString(4).equals("OT")) // omaggi
                                            cm = Env.cmomaggi;
                                        else if (cd.getString(4).equals("SM")) // sconto merce
                                            cm = Env.cmsm;
                                        else if (cd.getString(4).equals("TU"))// trasferimento a mezzo
                                            cm = Env.cmtrasfamezzo;
                                        else if (cd.getString(4).equals("TE"))// trasferimento da mezzo
                                            cm = Env.cmtrasfdamezzo;
                                        else if (cd.getString(4).equals("SS")) // scarico sede
                                            cm = Env.cmscaricosede;
                                        else if (cd.getString(4).equals("PE")) // scarico a sede per rotture danneggiamenti
                                            cm = Env.cmdistrmerce;
                                        else if (cd.getString(4).equals("SR"))// scarico a sede
                                            cm = Env.cmscaricosede;
                                        else if (cd.getString(4).equals("DC")) // ddt di carico
                                            cm = Env.cmddtcarico;
                                        else if (cd.getString(4).equals("OS")) // ordini a sede
                                            cm = Env.cmordinesede;
                                        else if (cd.getString(4).equals("OC")) // ordini clienti
                                            cm = Env.cmordinesede;
                                        else if (cd.getString(4).equals("CO")) // conferme ordini a sede
                                            cm = Env.cmordinesede;
                                        else if (cd.getString(4).equals("VQ")) // ddt quantità
                                        {
                                            cm = Env.cmddtqta;
                                            if (Env.cmddtqta.equals(""))
                                                cm = Env.cmvend;
                                        } else if (cd.getString(4).equals("XV")) // rettifica vendita
                                            cm = "RETVEND";
                                        else if (cd.getString(4).equals("XR")) // rettifica reso vendibile
                                            cm = "RETRESOV";
                                        else if (cd.getString(4).equals("XN")) // rettifica reso non vendibile
                                            cm = "RETRESONV";
                                        // RETTDDT
                                        rm += funzStringa.riempiStringa(cm, 10, funzStringa.SX, ' ');
                                        // descr.
                                        rm += funzStringa.riempiStringa("di cui di accisa all'aliquota vigente E." + Formattazione.formValuta(cd.getDouble(21), 12, 2, 0), 50, funzStringa.SX, ' ');
                                        // um (4)
                                        rm += funzStringa.riempiStringa("", 4, funzStringa.SX, ' ');
                                        // colli (5)
                                        rm += funzStringa.riempiStringa("0", 5, funzStringa.DX, ' ');
                                        // pezzi (15)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');//Utilita.riempi(Utilita.formatta(rsr.GetFloat(6), "#######0.00", Utilita.SEGNO_DX), 15, " ", 1);
                                        // contenuto (15)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // qta (15)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // qta evasa (15)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // cod.listino
                                        rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                        // posprezzo (1)
                                        rm += "" + cursor.getInt(13);
                                        // prezzo (15)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // prezzo (15)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // %sconto art.1 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %sconto art.2 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %sconto art.3 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %magg.(8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %provv (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // codiva
                                        rm += funzStringa.riempiStringa("", 3, funzStringa.SX, ' ');
                                        // sconto in valore
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // sconto in valore
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // %sconto cli.1 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %sconto cli.2 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %sconto cli.3 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // %sconto prom. (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // cod.agente 1
                                        rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                        // %provv.1 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // cod.agente 2
                                        rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                        // %provv.2 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // cod.agente 3
                                        rm += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                        // %provv.3 (8)
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                        // lordo
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // lordo
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // netto
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // netto
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // netto ivato
                                        rm += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                        // colli evasi (5)
                                        rm += funzStringa.riempiStringa("0", 5, funzStringa.DX, ' ');
                                        rm += "\r\n";
                                        fos.write(rm.getBytes());
                                    }
                                }

                            }
                            cd.close();

                            // tipo bolla (2=fresco; 1  o 0 =non deperibile) //AGOCF
                            String tb = "TB";
                            if (!cursor.isNull(44)) {
                                if (cursor.getInt(44) == 1)
                                    tb += "1";
                                else if (cursor.getInt(44) == 2)
                                    tb += "2";
                                else
                                    tb += "0";
                            } else
                                tb += "0";
                            tb += "\r\n";
                            fos.write(tb.getBytes());

                            // record piede
                            String rpd = "PD";
                            // codice destinazione
                            rpd += funzStringa.riempiStringa(cursor.getString(7), 10, funzStringa.SX, ' ');
                            // dest.in chiaro (50)
                            rpd += funzStringa.riempiStringa("", 50, funzStringa.SX, ' ');
                            // ind.in chiaro (50)
                            rpd += funzStringa.riempiStringa("", 50, funzStringa.SX, ' ');
                            // tipo trasp (1) = 1
                            rpd += "1";
                            // causale trasporto
                            if (cursor.getInt(0) == 0 || cursor.getInt(0) == 1 || cursor.getInt(0) == 2 || cursor.getInt(0) == 3)
                                rpd += funzStringa.riempiStringa("Vendita", 50, funzStringa.SX, ' ');
                            else if (cursor.getInt(0) == 5 || cursor.getInt(0) == 6 || cursor.getInt(0) == 7 || cursor.getInt(0) == 8)
                                rpd += funzStringa.riempiStringa("Trasferimento", 50, funzStringa.SX, ' ');
                            else
                                rpd += funzStringa.riempiStringa("Varie", 50, funzStringa.SX, ' ');
                            // aspetto beni
                            rpd += funzStringa.riempiStringa(cursor.getString(18), 50, funzStringa.SX, ' ');
                            // vettore 1
                            rpd += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                            rpd += funzStringa.riempiStringa("", 50, funzStringa.SX, ' ');
                            rpd += "0000-00-00";
                            rpd += "00:00";
                            // vettore 2
                            rpd += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                            rpd += funzStringa.riempiStringa("", 50, funzStringa.SX, ' ');
                            rpd += "0000-00-00";
                            rpd += "00:00";
                            // totale colli (5)
                            rpd += funzStringa.riempiStringa("" + cursor.getInt(19), 5, funzStringa.DX, ' ');
                            // totale peso (15)
                            rpd += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(20), "########0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // data e ora ritiro
                            rpd += cursor.getString(3);
                            //V120
                            rpd += cursor.getString(46).replace(".", ":");
                            rpd += "\r\n";
                            fos.write(rpd.getBytes());

                            // record ND note piede
                            if (!cursor.getString(21).equals("")) {
                                String rnd = "ND";
                                rnd += funzStringa.riempiStringa(cursor.getString(21), 255, funzStringa.SX, ' ');
                                rnd += funzStringa.riempiStringa(cursor.getString(22), 255, funzStringa.SX, ' ');
                                rnd += funzStringa.riempiStringa(cursor.getString(23), 255, funzStringa.SX, ' ');
                                rnd += funzStringa.riempiStringa(cursor.getString(24), 255, funzStringa.SX, ' ');
                                rnd += funzStringa.riempiStringa(cursor.getString(25), 255, funzStringa.SX, ' ');
                                rnd += funzStringa.riempiStringa(cursor.getString(26), 255, funzStringa.SX, ' ');
                                rnd += "\r\n";
                                fos.write(rnd.getBytes());
                            }

                            if (!cursor.getString(49).equals("")) {
                                // record provenienza
                                String rcs = "PR";
                                rcs += funzStringa.riempiStringa(cursor.getString(49), 10, funzStringa.SX, ' ');
                                rcs += "\r\n";
                                fos.write(rcs.getBytes());
                            }

                            // record riepilogo
                            String rrp = "RP";
                            // codice pagamento
                            rrp += funzStringa.riempiStringa(cursor.getString(17), 10, funzStringa.SX, ' ');
                            // mezzo pag. (1)
                            rrp += "0";
                            // totale lordo merce
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(27), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // totale lordo voci
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // tot.sconti cliente
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(28), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // tot.sconti articoli
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(29), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // tot.sconti prom.
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // tot.righe omaggi
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(30), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // lordo merce - sconti cliente
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(31), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // netti righe
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(32), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // % spese trasp.
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(0, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                            // spese trasp.
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(34), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // sconto in val.
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // spese inc.
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(33), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // spese bolli
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(35), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // spese imb.
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // tot.imp.
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(36), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // totiva
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(37), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // tot.doc.
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(38), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // addebiti
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(39), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // accrediti
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(40), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // abbuoni
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(50), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // netto a pagare
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(41), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            // mese escluso 1 (2)
                            rrp += "00";
                            // mese escluso 2 (2)
                            rrp += "00";
                            // gg pref.(2)
                            rrp += "00";
                            // acconto consegna
                            rrp += funzStringa.riempiStringa(Formattazione.formatta(cursor.getDouble(42), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                            rrp += "\r\n";
                            fos.write(rrp.getBytes());

                            // records ivamov
                            Cursor ci = Env.db.rawQuery(
                                    "SELECT imriga,imcodiva,imtipo,imaliq,imimp,imiva,imivaindetr " +
                                            "FROM ivamov WHERE immovtipo=? AND immovdoc=? AND immovsez=? AND immovdata=? AND immovnum=? ORDER BY imriga",
                                    parsd);
                            while (ci.moveToNext()) {
                                String ri = "IP";
                                // sottoconto iva
                                ri += funzStringa.riempiStringa("", 4, funzStringa.SX, ' ');
                                ri += funzStringa.riempiStringa("", 4, funzStringa.SX, ' ');
                                ri += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                // codiva
                                ri += funzStringa.riempiStringa(ci.getString(1), 3, funzStringa.SX, ' ');
                                // gruppoiva
                                ri += funzStringa.riempiStringa("" + ci.getInt(2), 2, funzStringa.DX, ' ');
                                // aliquota
                                ri += funzStringa.riempiStringa(Formattazione.formatta(ci.getDouble(3), "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // % indetr.
                                double pind = 0;
                                String[] parsi = new String[1];
                                parsi[0] = ci.getString(1);
                                Cursor cci = Env.db.rawQuery("SELECT ivapercind FROM codiva WHERE ivacod = ?", parsi);
                                if (cci.moveToFirst())
                                    pind = cci.getDouble(0);
                                cci.close();
                                ri += funzStringa.riempiStringa(Formattazione.formatta(pind, "##0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                                // imponibile
                                ri += funzStringa.riempiStringa(Formattazione.formatta(ci.getDouble(4), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // imponibile
                                ri += funzStringa.riempiStringa(Formattazione.formatta(ci.getDouble(4), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // imposta
                                ri += funzStringa.riempiStringa(Formattazione.formatta(ci.getDouble(5), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // imposta
                                ri += funzStringa.riempiStringa(Formattazione.formatta(ci.getDouble(5), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // tot.indetr.
                                ri += funzStringa.riempiStringa(Formattazione.formatta(ci.getDouble(6), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // tot.indetr.
                                ri += funzStringa.riempiStringa(Formattazione.formatta(ci.getDouble(6), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // valuta
                                ri += "Euro      ";
                                ri += "\r\n";
                                fos.write(ri.getBytes());
                            }
                            ci.close();

                            int tipopag = 6;
                            String[] parspag = new String[1];
                            parspag[0] = cursor.getString(17);
                            Cursor cpag = Env.db.rawQuery("SELECT pagtipo FROM pagamenti WHERE pagcod = ?", parspag);
                            if (cpag.moveToFirst())
                                tipopag = cpag.getInt(0);
                            cpag.close();

                            // records scadmov
                            Cursor cscad = Env.db.rawQuery(
                                    "SELECT smdata,smimp " +
                                            "FROM scadmov WHERE smmovtipo=? AND smmovdoc=? AND smmovsez=? AND smmovdata=? AND smmovnum=? ORDER BY smriga",
                                    parsd);
                            while (cscad.moveToNext()) {
                                String rsca = "SC";
                                // segno
                                rsca += "0";
                                // modo
                                rsca += "" + tipopag;
                                // data scad.
                                rsca += cscad.getString(0);
                                // importo
                                rsca += funzStringa.riempiStringa(Formattazione.formatta(cscad.getDouble(1), "########0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                // descrizione
                                rsca += funzStringa.riempiStringa("", 50, funzStringa.SX, ' ');
                                // n.doc (8)
                                rsca += funzStringa.riempiStringa("" + cursor.getInt(4), 8, funzStringa.SX, ' ');
                                // data doc
                                rsca += cursor.getString(3);
                                // sottoconto sogg.
                                rsca += funzStringa.riempiStringa(mastro, 4, funzStringa.SX, ' ');
                                rsca += funzStringa.riempiStringa(conto, 4, funzStringa.SX, ' ');
                                rsca += funzStringa.riempiStringa(sottoconto, 10, funzStringa.SX, ' ');
                                // n.riga pn
                                rsca += "00001";
                                // clifor
                                rsca += "8";
                                // agente 1
                                rsca += funzStringa.riempiStringa(Env.agecod, 10, funzStringa.SX, ' ');
                                // agente 2
                                rsca += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                // agente 3
                                rsca += funzStringa.riempiStringa("", 10, funzStringa.SX, ' ');
                                // valuta
                                rsca += "Euro      ";
                                // cambio
                                rsca += funzStringa.riempiStringa(Formattazione.formatta(1, "####0.000000", Formattazione.NO_SEGNO), 15, funzStringa.DX, ' ');
                                // tipo scad.
                                rsca += "0";
                                // saldi scad.
                                rsca += funzStringa.riempiStringa(Formattazione.formatta(0, "########0.00", Formattazione.NO_SEGNO), 15, funzStringa.DX, ' ');
                                rsca += "\r\n";
                                fos.write(rsca.getBytes());
                            }
                            cscad.close();

                            // records lottimov
                            Cursor clot = Env.db.rawQuery(
                                    "SELECT lmlotcod,lmlotartcod,lmtipomov,lmpezzi,lmqta " +
                                            "FROM lottimov WHERE lmmovtipo=? AND lmmovdoc=? AND lmmovsez=? AND lmmovdata=? AND lmmovnum=? ORDER BY lmriga",
                                    parsd);
                            while (clot.moveToNext()) {
                                String rl = "LT";
                                // codice lotto
                                rl += funzStringa.riempiStringa(clot.getString(0), 20, funzStringa.SX, ' ');
                                // codice articolo
                                rl += funzStringa.riempiStringa(clot.getString(1), 20, funzStringa.SX, ' ');
                                // tipo mov.
                                if (clot.getString(2).equals("C"))
                                    rl += "0 ";
                                else
                                    rl += "1 ";
                                // pezzi
                                rl += funzStringa.riempiStringa("" + clot.getInt(3), 8, funzStringa.SX, ' ');
                                // quantita
                                rl += funzStringa.riempiStringa(Formattazione.formatta(clot.getDouble(4), "########0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                rl += "\r\n";
                                fos.write(rl.getBytes());
                            }
                            clot.close();

                            // record nuovo cliente
/*                            if (clinuovo) {
                                String[] parscn = new String[1];
                                parscn[0] = cursor.getString(6);
                                Cursor ccn = Env.db.rawQuery(
                                        "SELECT clinome,cliindir,clipiva,clicodpag,clinumtel,clinumcell,clitiponuovo,clinomepfnuovo,clicognomepfnuovo,clizonacod,clicodsdinuovo,clinote " +
                                                "FROM clienti WHERE clicodice=?", parscn);
                                if (ccn.moveToNext()) {
                                    String rnc = "CN";
                                    // nome
                                    rnc += funzStringa.riempiStringa(ccn.getString(0), 100, funzStringa.SX, ' ');
                                    // indirizzo
                                    rnc += funzStringa.riempiStringa(ccn.getString(1), 50, funzStringa.SX, ' ');
                                    // p.iva
                                    rnc += funzStringa.riempiStringa(ccn.getString(2), 16, funzStringa.SX, ' ');
                                    // cod.pag.
                                    rnc += funzStringa.riempiStringa(ccn.getString(3), 10, funzStringa.SX, ' ');
                                    // tel
                                    rnc += funzStringa.riempiStringa(ccn.getString(4), 20, funzStringa.SX, ' ');
                                    // cell
                                    rnc += funzStringa.riempiStringa(ccn.getString(5), 20, funzStringa.SX, ' ');
                                    //Aggiunto dalla tentata vendita da testare
                                    // tipo cliente
                                    rnc += "" + ccn.getInt(6);
                                    // nome pf
                                    rnc += funzStringa.riempiStringa(ccn.getString(7), 40, funzStringa.SX, ' ');
                                    // cognome pf
                                    rnc += funzStringa.riempiStringa(ccn.getString(8), 40, funzStringa.SX, ' ');
                                    // zona commerciale
                                    rnc += funzStringa.riempiStringa(ccn.getString(9), 10, funzStringa.SX, ' ');
                                    // codice sdi
                                    rnc += funzStringa.riempiStringa(ccn.getString(10), 7, funzStringa.SX, ' ');
                                    //note cliente
                                    rnc += funzStringa.riempiStringa(ccn.getString(11), 255, funzStringa.SX, ' ');
                                    rnc += "\r\n";
                                    fos.write(rnc.getBytes());
                                    nclinuovi++;
                                }
                                ccn.close();
                                hclinuovi.put(cursor.getString(6), "");
                            }*/

                            // record messaggio
                            Cursor cmsg = Env.db.rawQuery(
                                    "SELECT msgtipo,msgtesto FROM msgmov WHERE msgmovtipo=? AND msgmovdoc=? AND msgmovsez=? AND msgmovdata=? AND msgmovnum=?",
                                    parsd);
                            while (cmsg.moveToNext()) {
                                String rl = "MS";
                                // tipo
                                rl += funzStringa.riempiStringa("" + cmsg.getInt(0), 2, funzStringa.SX, ' ');
                                // testo
                                rl += funzStringa.riempiStringa(cmsg.getString(1), 255, funzStringa.SX, ' ');
                                rl += "\r\n";
                                fos.write(rl.getBytes());
                            }
                            cmsg.close();

                            // chiave
                            String key = cursor.getString(45);
                            if (!key.equals("")) {
                                String kk = "KK";
                                kk += key;
                                kk += "\r\n";
                                fos.write(kk.getBytes());
                            }

                            // data consegna
                            if (cursor.getInt(0) == 12 || cursor.getInt(0) == 20) {
                                String rdc = "DC";
                                if (cursor.getString(48).equals(""))
                                    rdc += "0000-00-00";
                                else
                                    rdc += funzStringa.riempiStringa(cursor.getString(48), 10, funzStringa.SX, ' ');
                                rdc += "\r\n";
                                fos.write(rdc.getBytes());
                            }

                            // aggiornamento flag trasferito (1a fase)
                            if (numdocsingolo == 0) {
                                SQLiteStatement ststato = Env.db.compileStatement(
                                        "UPDATE movimenti SET movtrasf='T' WHERE movtipo=? AND movdoc=? AND movsez=? AND movdata=? AND movnum=?");
                                ststato.clearBindings();
                                ststato.bindLong(1, cursor.getInt(0));
                                ststato.bindString(2, cursor.getString(1));
                                ststato.bindString(3, cursor.getString(2));
                                ststato.bindString(4, cursor.getString(3));
                                ststato.bindLong(5, cursor.getInt(4));
                                ststato.execute();
                                ststato.close();
                            }

                            prog++;
                            h = new HashMap();
                            h.put("label", "Esportazione movimenti...");
                            String dt = cursor.getString(3).substring(8, 10) + "/" + cursor.getString(3).substring(5, 7) + "/" + cursor.getString(3).substring(0, 4);
                            h.put("label2", "Esportazione " + cursor.getString(2) + "/" + cursor.getInt(4) + " del " + dt);
                            h.put("labelris1", "");
                            h.put("labelris2", "");
                            h.put("labelris3", "");
                            h.put("val", "" + prog);
                            h.put("max", "" + ndocs);
                            h.put("visible", "S");
                            h.put("indeterminate", "N");
                            h.put("error", "");
                            this.publishProgress(h);
                        }
                        cursor.close();

                        if (!soloordini && numdocsingolo == 0) {
                            //fatture consegnate
                            Cursor cfc = Env.db.rawQuery(
                                    "SELECT scscadid,scdataconsfat FROM scoperti WHERE scdataconsfat <> '0000-00-00'", null);
                            while (cfc.moveToNext()) {
                                if (!cfc.isNull(1)) {
                                    h = new HashMap();
                                    h.put("label", "Esportazione consegna fatture...");
                                    h.put("label2", "");
                                    h.put("labelris1", "");
                                    h.put("labelris2", "");
                                    h.put("labelris3", "");
                                    h.put("val", "0");
                                    h.put("max", "0");
                                    h.put("visible", "S");
                                    h.put("indeterminate", "S");
                                    h.put("error", "");
                                    this.publishProgress(h);
                                    String ric = "CF";
                                    ric += funzStringa.riempiStringa("" + cfc.getInt(0), 10, funzStringa.SX, ' ');
                                    ric += funzStringa.riempiStringa(cfc.getString(1), 10, funzStringa.SX, ' ');
                                    ric += "\r\n";
                                    fos.write(ric.getBytes());
                                }
                            }
                            cfc.close();

                            // incassi
                            Cursor cinc = Env.db.rawQuery(
                                    "SELECT isctipo,iscsezdoc,iscdatadoc,iscnumdoc," +
                                            "isctipoop,iscclicod,iscdestcod,iscscadid,iscdatascad,isctiposcad,iscresiduoscad,isaldo,iscdatainc,iscorainc,iscdescr,iscdescr2,isctipocassa " +
                                            "FROM incassi", null);
                            while (cinc.moveToNext()) {
                                h = new HashMap();
                                h.put("label", "Esportazione incassi...");
                                h.put("label2", "");
                                h.put("labelris1", "");
                                h.put("labelris2", "");
                                h.put("labelris3", "");
                                h.put("val", "0");
                                h.put("max", "0");
                                h.put("visible", "S");
                                h.put("indeterminate", "S");
                                h.put("error", "");
                                this.publishProgress(h);
                                String ric = "";
                                if (Env.incattivanote || Env.incsceglicassa)
                                    ric = "IX";
                                else
                                    ric = "IC";
                                ric += cinc.getString(0);
                                ric += funzStringa.riempiStringa(cinc.getString(1), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cinc.getString(2), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa("" + cinc.getInt(3), 8, funzStringa.SX, ' ');
                                ric += cinc.getString(4);
                                ric += funzStringa.riempiStringa(cinc.getString(5), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cinc.getString(6), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa("" + cinc.getInt(7), 8, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cinc.getString(8), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa("" + cinc.getInt(9), 1, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(Formattazione.formatta(cinc.getDouble(11), "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                ric += funzStringa.riempiStringa(Env.agecod, 10, funzStringa.SX, ' ');
                                if (cinc.getString(12).equals("00:00"))
                                    ric += "0000-00-00";
                                else
                                    ric += funzStringa.riempiStringa(cinc.getString(12), 10, funzStringa.SX, ' ');
                                if (cinc.isNull(13))
                                    ric += "00:00";
                                else
                                    ric += funzStringa.riempiStringa(cinc.getString(13), 5, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cinc.getString(14), 255, funzStringa.SX, ' ');
                                if (Env.incattivanote || Env.incsceglicassa) {
                                    ric += funzStringa.riempiStringa(cinc.getString(15), 255, funzStringa.SX, ' ');
                                    ric += "" + cinc.getInt(16);
                                }
                                ric += "\r\n";
                                fos.write(ric.getBytes());
                                ninc++;
                            }
                            cinc.close();

                        }
                        // log operazioni
                        if (numdocsingolo == 0) {
                            try {
                                Cursor clog = Env.db.rawQuery("SELECT logdata,logora,logdescr FROM logop ORDER BY logdata,logora", null);
                                while (clog.moveToNext()) {
                                    h = new HashMap();
                                    h.put("label", "Esportazione log operazioni...");
                                    h.put("label2", "");
                                    h.put("labelris1", "");
                                    h.put("labelris2", "");
                                    h.put("labelris3", "");
                                    h.put("val", "0");
                                    h.put("max", "0");
                                    h.put("visible", "S");
                                    h.put("indeterminate", "S");
                                    h.put("error", "");
                                    this.publishProgress(h);
                                    String ric = "LO";
                                    ric += funzStringa.riempiStringa(Env.pedcod, 10, funzStringa.SX, ' ');
                                    ric += " ";
                                    ric += funzStringa.riempiStringa(clog.getString(0).substring(8, 10) + "/" + clog.getString(0).substring(5, 7) + "/" + clog.getString(0).substring(0, 4), 10, funzStringa.SX, ' ');
                                    ric += " ";
                                    ric += funzStringa.riempiStringa(clog.getString(1), 5, funzStringa.SX, ' ');
                                    ric += " ";
                                    ric += clog.getString(2);
                                    ric += "\r\n";
                                    fos.write(ric.getBytes());
                                }
                                clog.close();
                            } catch (Exception elogop) {
                            }
                        }

                        if (!soloordini && numdocsingolo == 0) {
                            // tabella scoperti ddt sospesi
                            Cursor csosp = Env.db.rawQuery(
                                    "SELECT scsezdoc,scdatadoc,scnumdoc,sctipoop,scclicod,scdestcod,scresiduoscad " +
                                            "FROM scoperti WHERE sctipo = 'B' AND scresiduoscad <> 0", null);
                            while (csosp.moveToNext()) {
                                h = new HashMap();
                                h.put("label", "Esportazione ddt sospesi...");
                                h.put("label2", "");
                                h.put("labelris1", "");
                                h.put("labelris2", "");
                                h.put("labelris3", "");
                                h.put("val", "0");
                                h.put("max", "0");
                                h.put("visible", "S");
                                h.put("indeterminate", "S");
                                h.put("error", "");
                                this.publishProgress(h);
                                String ric = "SB";
                                ric += funzStringa.riempiStringa(csosp.getString(0), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(csosp.getString(1), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa("" + csosp.getInt(2), 8, funzStringa.SX, ' ');
                                ric += csosp.getString(3);
                                ric += funzStringa.riempiStringa(csosp.getString(4), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(csosp.getString(5), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(Formattazione.formatta(csosp.getDouble(6), "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                ric += "\r\n";
                                fos.write(ric.getBytes());
                            }
                            csosp.close();
                        }

                        if (!soloordini && numdocsingolo == 0) {
                            // tabella inventario
                            Cursor cinv = Env.db.rawQuery(
                                    "SELECT invdata,invora,invartcod,invqta,invval,invnum FROM inventario WHERE invtrasf <> 'S' ORDER BY invdata,invora", null);
                            while (cinv.moveToNext()) {
                                h = new HashMap();
                                h.put("label", "Esportazione inventari...");
                                h.put("label2", "");
                                h.put("labelris1", "");
                                h.put("labelris2", "");
                                h.put("labelris3", "");
                                h.put("val", "0");
                                h.put("max", "0");
                                h.put("visible", "S");
                                h.put("indeterminate", "S");
                                h.put("error", "");
                                this.publishProgress(h);
                                String ric = "IG";
                                ric += funzStringa.riempiStringa(Env.pedcod, 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cinv.getString(0), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cinv.getString(1), 8, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cinv.getString(2), 20, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(Formattazione.formatta(cinv.getDouble(3), "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                ric += funzStringa.riempiStringa(Formattazione.formatta(cinv.getDouble(4), "#######0.00", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                ric += funzStringa.riempiStringa("" + cinv.getInt(5), 8, funzStringa.SX, ' ');
                                ric += "\r\n";
                                fos.write(ric.getBytes());
                            }
                            cinv.close();
                            Env.db.execSQL("UPDATE inventario SET invtrasf='T' WHERE invtrasf <> 'S'");
                        }

                        if (!soloordini && numdocsingolo == 0) {
                            // listini term
                            Cursor cl = Env.db.rawQuery(
                                    "SELECT ltcod,ltdescr,ltdata,ltora FROM listini_term WHERE lttrasf <> 'S' ORDER BY ltcod", null);
                            while (cl.moveToNext()) {
                                h = new HashMap();
                                h.put("label", "Esportazione listini...");
                                h.put("label2", "");
                                h.put("labelris1", "");
                                h.put("labelris2", "");
                                h.put("labelris3", "");
                                h.put("val", "0");
                                h.put("max", "0");
                                h.put("visible", "S");
                                h.put("indeterminate", "S");
                                h.put("error", "");
                                this.publishProgress(h);
                                String ric = "LI";
                                ric += funzStringa.riempiStringa(Env.pedcod, 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cl.getString(0), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cl.getString(1), 255, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cl.getString(2), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cl.getString(3), 8, funzStringa.SX, ' ');
                                ric += "\r\n";
                                fos.write(ric.getBytes());
                            }
                            cl.close();
                            Cursor cld = Env.db.rawQuery(
                                    "SELECT ltcod,ltdartcod,ltdprezzo FROM listini_term_dett INNER JOIN listini_term " +
                                            " ON ltdcodlist = ltcod WHERE lttrasf <> 'S' ORDER BY ltcod,ltdartcod", null);
                            while (cld.moveToNext()) {
                                h = new HashMap();
                                h.put("label", "Esportazione listini...");
                                h.put("label2", "");
                                h.put("labelris1", "");
                                h.put("labelris2", "");
                                h.put("labelris3", "");
                                h.put("val", "0");
                                h.put("max", "0");
                                h.put("visible", "S");
                                h.put("indeterminate", "S");
                                h.put("error", "");
                                this.publishProgress(h);
                                String ric = "RL";
                                ric += funzStringa.riempiStringa(Env.pedcod, 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cld.getString(0), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(cld.getString(1), 20, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(Formattazione.formatta(cld.getDouble(2), "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                ric += "\r\n";
                                fos.write(ric.getBytes());
                            }
                            cl.close();

                            Env.db.execSQL("UPDATE listini_term SET lttrasf='T' WHERE lttrasf <> 'S'");
                        }

                        if (!soloordini && numdocsingolo == 0) {
                            // storico ddt carico
                            Cursor csd = Env.db.rawQuery(
                                    "SELECT stdata,stora,stnum,startcod,startdescr,stum,stlotto,straggrcod,straggrdescr,stqta,stprezzo FROM storico_ddtcarico WHERE sttrasf <> 'S' ORDER BY stdata,stora,stnum", null);
                            while (csd.moveToNext()) {
                                h = new HashMap();
                                h.put("label", "Esportazione storico DDT di carico...");
                                h.put("label2", "");
                                h.put("labelris1", "");
                                h.put("labelris2", "");
                                h.put("labelris3", "");
                                h.put("val", "0");
                                h.put("max", "0");
                                h.put("visible", "S");
                                h.put("indeterminate", "S");
                                h.put("error", "");
                                this.publishProgress(h);
                                String ric = "DL";
                                ric += funzStringa.riempiStringa(Env.pedcod, 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(csd.getString(0), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(csd.getString(1), 8, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa("" + csd.getInt(2), 10, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(csd.getString(3), 20, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(csd.getString(4), 255, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(csd.getString(5), 4, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(csd.getString(6), 20, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(csd.getString(7), 20, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(csd.getString(8), 255, funzStringa.SX, ' ');
                                ric += funzStringa.riempiStringa(Formattazione.formatta(csd.getDouble(9), "#######0.000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                ric += funzStringa.riempiStringa(Formattazione.formatta(csd.getDouble(10), "######0.0000", Formattazione.SEGNO_DX), 15, funzStringa.DX, ' ');
                                ric += "\r\n";
                                fos.write(ric.getBytes());
                            }
                            csd.close();
                            Env.db.execSQL("UPDATE storico_ddtcarico SET sttrasf='T' WHERE sttrasf <> 'S'");
                        }

                        if (numdocsingolo == 0) {
                            // contatti
                            Cursor cco = Env.db.rawQuery(
                                    "SELECT ccodice,ccognome,cnome,cdatacont,ceventocont,ctitolo,cmansione,cemail,ctel,cmobile,cfax,ctipologia,ctipo," +
                                            "caziendaragsoc,caziendaindir,caziendanumciv,caziendaloc,caziendacap,caziendaprov,caziendanaz,caziendaemail,caziendasitoweb," +
                                            "ctrasformatocliente,cnote,ctermcod FROM contatti WHERE ctrasferito <> 'S' ORDER BY ccodice", null);
                            while (cco.moveToNext()) {
                                h = new HashMap();
                                h.put("label", "Esportazione contatti...");
                                h.put("label2", "");
                                h.put("labelris1", "");
                                h.put("labelris2", "");
                                h.put("labelris3", "");
                                h.put("val", "0");
                                h.put("max", "0");
                                h.put("visible", "S");
                                h.put("indeterminate", "S");
                                h.put("error", "");
                                this.publishProgress(h);
                                String ric = "CO";
                                ric += Env.pedcod + " |"; // terminale
                                ric += cco.getString(0) + " |"; // codice
                                ric += cco.getString(1) + " |"; // cognome
                                ric += cco.getString(2) + " |"; // nome
                                ric += cco.getString(3) + " |"; // data contatto
                                ric += cco.getString(4) + " |"; // evento
                                ric += cco.getString(5) + " |"; // titolo
                                ric += cco.getString(6) + " |"; // mansione
                                ric += cco.getString(7) + " |"; // email
                                ric += cco.getString(8) + " |"; // tel
                                ric += "" + cco.getInt(9) + " |"; // mobile
                                ric += cco.getString(10) + " |"; // fax
                                ric += "" + cco.getInt(11) + " |"; // tipologia
                                ric += "" + cco.getInt(12) + " |";
                                // tipo
                                ric += cco.getString(13) + " |";
                                // azienda rag soc
                                ric += cco.getString(14) + " |";
                                // azienda indirizzo
                                ric += "" + cco.getInt(15) + " |";
                                // num civico
                                ric += cco.getString(16) + " |";
                                // azienda localita
                                ric += "" + cco.getInt(17) + " |";
                                // cap
                                ric += cco.getString(18) + " |";
                                // azienda provincia
                                ric += cco.getString(19) + " |";
                                // azienda nazione
                                ric += cco.getString(20) + " |";
                                // azienda email
                                ric += cco.getString(21) + " |";
                                // azienda sitoweb
                                ric += cco.getString(22) + " |";
                                // trasformato cliente
                                ric += cco.getString(23) + " |";
                                // note
                                ric += cco.getString(24) + " |";
                                // codice terminale
                                ric += "\r\n";
                                fos.write(ric.getBytes());
                                ncontatti++;
                            }
                            cco.close();
                            Env.db.execSQL("UPDATE contatti SET ctrasferito='T' WHERE ctrasferito <> 'S'");
                        }


                        // nuovi clienti
                        String cncond = "";
                        Iterator it = hclinuovi.keySet().iterator();
                        while (it.hasNext()) {
                            cncond += "'" + it.next() + "',";
                        }
                        if (cncond.length() > 0)
                            cncond = cncond.substring(0, cncond.length() - 1);
                        Cursor ccn = Env.db.rawQuery(
                                "SELECT clicodice,clinome,cliindir,clinumciv,clicap,cliloc,cliprov,clipiva,clicodfisc,clinumtel,clinumcell,cliemail1,clicognomepri,clinomepri,clinote,clicodpag,clitipo,clicodsdinuovo,cliinsegna " +
                                        "FROM clienti WHERE clinuovo = 1 AND clitrasferito <> 'S' " +
                                        (!cncond.equals("") ? "AND NOT (clicodice IN (" + cncond + "))" : "") +
                                        " ORDER BY clicodice", null);
                        while (ccn.moveToNext()) {
                            h = new HashMap();
                            h.put("label", "Esportazione nuovi clienti...");
                            h.put("label2", "");
                            h.put("labelris1", "");
                            h.put("labelris2", "");
                            h.put("labelris3", "");
                            h.put("val", "0");
                            h.put("max", "0");
                            h.put("visible", "S");
                            h.put("indeterminate", "S");
                            h.put("error", "");
                            this.publishProgress(h);
                            String rnc = "CK";
                            rnc += Env.pedcod + " |"; // terminale
                            rnc += ccn.getString(0) + " |"; //codice
                            rnc += ccn.getString(1) + " |"; //nome
                            rnc += ccn.getString(2) + " |"; //indir
                            rnc += ccn.getString(3) + " |";//numCiv
                            rnc += ccn.getString(4) + " |"; //cap
                            rnc += ccn.getString(5) + " |"; //loc
                            rnc += ccn.getString(6) + " |"; //prov
                            rnc += ccn.getString(7) + " |"; //P.iva
                            rnc += ccn.getString(8) + " |"; //cF
                            rnc += ccn.getString(9) + " |"; //numtel
                            rnc += ccn.getString(10) + " |"; //numcell
                            rnc += ccn.getString(11) + " |"; //email
                            rnc += ccn.getString(12) + " |"; //cgnomepri
                            rnc += ccn.getString(13) + " |"; //nomepri
                            rnc += ccn.getString(14) + " |"; //note
                            rnc += ccn.getString(15) + " |"; // cod.pag.
                            rnc += ccn.getString(16) + " |"; //clitipo
                            rnc += ccn.getString(17) + " |"; //codice sdi
                            rnc += ccn.getString(18) + " |"; //insegna
                            rnc += "\r\n";
                            fos.write(rnc.getBytes());
                            nclinuovi++;
                        }
                        ccn.close();
                        Env.db.execSQL("UPDATE clienti SET clitrasferito='T' WHERE clitrasferito <> 'S' AND clinuovo = 1");


                        h = new HashMap();
                        h.put("label", "Esportazione variazioni anagrafiche...");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);
                        prog = 0;
                        cursor = Env.db.rawQuery(
                                "SELECT vacodice,vatipo,vavalore FROM varanag WHERE vatrasf='N'", null);
                        while (cursor.moveToNext()) {
                            String ric = "VA";
                            ric += funzStringa.riempiStringa(cursor.getString(0), 10, funzStringa.SX, ' ');
                            ric += funzStringa.riempiStringa("" + cursor.getInt(1), 2, funzStringa.SX, ' ');
                            ric += funzStringa.riempiStringa(cursor.getString(2), 255, funzStringa.SX, ' ');
                            ric += "\r\n";
                            fos.write(ric.getBytes());
                            climodificati = true;
                            prog++;
                            h = new HashMap();
                            h.put("label", "Esportazione variazioni anagrafiche...");
                            h.put("label2", "");
                            h.put("labelris1", "");
                            h.put("labelris2", "");
                            h.put("labelris3", "");
                            h.put("val", "" + prog);
                            h.put("max", "0");
                            h.put("visible", "S");
                            h.put("indeterminate", "S");
                            h.put("error", "");
                            this.publishProgress(h);
                        }
                        cursor.close();

                        fos.write("GESTTERM\r\n".getBytes());
                        fos.close();

                        h = new HashMap();
                        h.put("label", "Creazione file scarico dati terminata");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "N");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);

                        // cancella la tabella temporanea artbis se esiste
                        try {
                            Env.db.execSQL("DELETE FROM artbis");
                        } catch (Exception ebis) {
                        }
                    } catch (Exception emovs) {
                        emovs.printStackTrace();
                        try {
                            fos.close();
                        } catch (Exception efos) {
                        }
                        fileok = false;
                        h = new HashMap();
                        h.put("label", "Errore su generazione file");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "S");
                        h.put("indeterminate", "N");
                        h.put("error", "");
                        this.publishProgress(h);
                        invioincorso = false;
                    }
                }
                if (fileok) {
                    // zip file
                    boolean zipok = true;
                    if (nomefilestorico.equals("")) {
                        HashMap h = new HashMap();
                        h.put("label", "Compressione file...");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "100");
                        h.put("max", "100");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);

                        try {
                            FileOutputStream fosz = new FileOutputStream(getApplicationContext().getCacheDir() + File.separator + "scarico" + File.separator + nfzip);
                            ZipOutputStream zip = new ZipOutputStream(fosz);
                            zip.putNextEntry(new ZipEntry(nf));
                            FileInputStream fis = new FileInputStream(getApplicationContext().getCacheDir() + File.separator + "scarico" + File.separator + nf);
                            while (fis.available() > 0) {
                                if (fis.available() >= 512) {
                                    byte[] b = new byte[512];
                                    fis.read(b);
                                    zip.write(b);
                                } else {
                                    byte[] b = new byte[fis.available()];
                                    fis.read(b);
                                    zip.write(b);
                                }
                            }
                            zip.flush();
                            fis.close();
                            zip.close();
                            fosz.close();
                        } catch (Exception ezip) {
                            System.out.println("errore su creazione zip: " + ezip.getMessage());
                            zipok = false;
                        }
                    }

                    if (zipok) {
                        // copia file zip su storico
                        if (nomefilestorico.equals("") && numdocsingolo == 0) {
                            HashMap h = new HashMap();
                            h.put("label", "Copia file zip su storico scarichi...");
                            h.put("label2", "");
                            h.put("labelris1", "");
                            h.put("labelris2", "");
                            h.put("labelris3", "");
                            h.put("val", "100");
                            h.put("max", "100");
                            h.put("visible", "S");
                            h.put("indeterminate", "S");
                            h.put("error", "");
                            this.publishProgress(h);
                            FunzioniJBeerApp.copiaFile(getApplicationContext().getCacheDir() + File.separator + "scarico" + File.separator + nfzip,
                                    getApplicationContext().getFilesDir() + File.separator + "storico_scarico" + File.separator + nfzip);
                        }

                        // invio file
                        String nfx = nfzip;
                        if (!nomefilestorico.equals(""))
                            nfx = nomefilestorico;

                        HashMap h = new HashMap();
                        h.put("label", "Invio file " + nfx + "...");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "100");
                        h.put("max", "100");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);

                        boolean inviook = false;

                        if (!nomefilestorico.equals("")) {
                            FunzioniJBeerApp.copiaFile(getApplicationContext().getFilesDir() + File.separator + "storico_scarico" + File.separator + nomefilestorico,
                                    getApplicationContext().getCacheDir() + File.separator + "scarico" + File.separator + nomefilestorico);
                        }
                        try {
                            ArrayList<HashMap> campiForm = new ArrayList();
                            ArrayList<HashMap> filesUpload = new ArrayList();
                            HashMap r = new HashMap();
                            r.put("nome", "user");
                            r.put("valore", user);
                            campiForm.add(r);
                            r = new HashMap();
                            r.put("nome", "pwd");
                            r.put("valore", pwd);
                            campiForm.add(r);
                            r = new HashMap();
                            r.put("nome", "codpostorig");
                            r.put("valore", tcod);
                            campiForm.add(r);
                            r = new HashMap();
                            r.put("nome", "codpostdest");
                            if (numdocsingolo > 0)
                                r.put("valore", postdestdocsingolo);
                            else if (!Env.gprs_postserver.equals(""))
                                r.put("valore", Env.gprs_postserver);
                            else
                                r.put("valore", "sede");
                            campiForm.add(r);
                            r = new HashMap();
                            r.put("nome", "cancprec");
                            r.put("valore", "N");
                            campiForm.add(r);
                            r = new HashMap();
                            r.put("nome", "fileUpload");
                            r.put("nomefile", getApplicationContext().getCacheDir() + File.separator + "scarico" + File.separator + nfx);
                            filesUpload.add(r);

                            BufferedReader bufline = FunzioniHTTP.inviaPostMultipartHttp(server + "/service/uploadtrasf.do", campiForm, filesUpload, null, new Hashtable(), false, false,
                                    false, "", "", 10000);
                            if (bufline != null) {
                                String s = bufline.readLine();
                                if (s != null && s.startsWith("OK"))
                                    inviook = true;
                                bufline.close();
                            }
                        } catch (Exception einvio) {
                            einvio.printStackTrace();
                            inviook = false;
                        }

                        if (inviook) {
                            if (nomefilestorico.equals("") && numdocsingolo == 0) {
                                // aggiornamento flag trasferito definitivo
                                if (soloordini) {
                                    Env.db.execSQL("UPDATE movimenti SET movtrasf = 'S' WHERE movtrasf = 'T' AND movtipo = 12");
                                } else {
                                    Env.db.execSQL("UPDATE movimenti SET movtrasf = 'S' WHERE movtrasf = 'T'");

                                    Env.db.execSQL("UPDATE inventario SET invtrasf = 'S' WHERE invtrasf = 'T'");

                                    Env.db.execSQL("UPDATE listini_term SET lttrasf = 'S' WHERE lttrasf = 'T'");

                                    Env.db.execSQL("UPDATE storico_ddtcarico SET sttrasf = 'S' WHERE sttrasf = 'T'");

                                    // pulizia incassi
                                    Env.db.execSQL("DELETE FROM incassi");
                                }
                                // imposta contatti come trasferiti
                                Env.db.execSQL("UPDATE contatti SET ctrasferito = 'S' WHERE ctrasferito = 'T'");
                                // imposta variazioni anagr inviate come trasferite
                                Env.db.execSQL("UPDATE varanag SET vatrasf='S' WHERE vatrasf='N'");
                                // imposta client nuovi passati da fuori dei documenti come trasferiti
                                Env.db.execSQL("UPDATE clienti SET clitrasferito = 'S' WHERE clitrasferito = 'T'");
                            }

                            h = new HashMap();
                            h.put("label", "Invio dati " + (soloordini ? "ordini " : "") + "terminato");
                            String infoinvio = "";
                            if (nddt > 0)
                                infoinvio += "DDT inviati:" + nddt + "\n";
                            if (nfatt > 0)
                                infoinvio += "Fatture inviate:" + nfatt + "\n";
                            if (nordcli > 0)
                                infoinvio += "Ordini clienti inviati:" + nordcli + "\n";
                            if (nordsede > 0)
                                infoinvio += "Ordini a sede inviati:" + nordsede + "\n";
                            if (nxe > 0)
                                infoinvio += "Integrazioni inviate:" + nxe + "\n";
                            if (nscasede > 0)
                                infoinvio += "Scarichi a sede inviati:" + nscasede + "\n";
                            if (ntrasfvs > 0)
                                infoinvio += "Trasf.verso altro automezzo inviati:" + ntrasfvs + "\n";
                            if (ntrasfda > 0)
                                infoinvio += "Carichi da altro automezzo inviati:" + ntrasfda + "\n";
                            if (ninc > 0)
                                infoinvio += "Incassi inviati:" + ninc + "\n";
                            if (ncontatti > 0)
                                infoinvio += "Nuovi contatti inviati:" + ncontatti + "\n";
                            if (nclinuovi > 0)
                                infoinvio += "Nuovi clienti inviati:" + nclinuovi + "\n";
                            if (climodificati)
                                infoinvio += "Clienti modificati inviati" + "\n";
                            h.put("label2", infoinvio);
                            h.put("labelris1", "");
                            h.put("labelris2", "");
                            h.put("labelris3", "");
                            h.put("val", "100");
                            h.put("max", "100");
                            h.put("visible", "N");
                            h.put("indeterminate", "N");
                            h.put("error", "");
                            this.publishProgress(h);
                            invioincorso = false;

                            if (nomefilestorico.equals("") && numdocsingolo == 0) {
                                FunzioniJBeerApp.impostaProprieta("ultimoinv_data", (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                                FunzioniJBeerApp.impostaProprieta("ultimoinv_ora", df.format(new Date()));

                                if (!soloordini) {
                                    FunzioniJBeerApp.impostaProprieta("DATIDAINVIARE", "N");
                                    if (Env.backupauto && Env.tipobackupauto == 0) {
                                        Intent i = new Intent(FormInvioDati.this.getApplicationContext(), FormBackupOnline.class);
                                        FormInvioDati.this.startActivity(i);
                                    }
                                }
                            }
                        } else {
                            h = new HashMap();
                            h.put("label", "Errore su invio file");
                            h.put("label2", "");
                            h.put("labelris1", "");
                            h.put("labelris2", "");
                            h.put("labelris3", "");
                            h.put("val", "100");
                            h.put("max", "100");
                            h.put("visible", "N");
                            h.put("indeterminate", "N");
                            h.put("error", "");
                            this.publishProgress(h);
                            invioincorso = false;
                        }
                    } else {
                        HashMap h = new HashMap();
                        h.put("label", "Errore su compressione file");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "100");
                        h.put("max", "100");
                        h.put("visible", "N");
                        h.put("indeterminate", "N");
                        h.put("error", "");
                        this.publishProgress(h);
                        invioincorso = false;
                    }
                }
            }
            return "";
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(String value) {

        }

        @Override
        protected void onProgressUpdate(HashMap... values) {

            labelinfo.setText((String) values[0].get("label"));
            labelinfo2.setText((String) values[0].get("label2"));
            labelris1.setText((String) values[0].get("labelris1"));
            labelris2.setText((String) values[0].get("labelris2"));
            labelris3.setText((String) values[0].get("labelris3"));
            progress.setMax(Formattazione.estraiIntero((String) values[0].get("max")));
            progress.setProgress(Formattazione.estraiIntero((String) values[0].get("val")));
            progress.setVisibility(values[0].get("visible").equals("S") ? View.VISIBLE : View.INVISIBLE);
            progress.setIndeterminate(values[0].get("indeterminate").equals("S"));
            String err = (String) values[0].get("error");
            if (!err.equals("")) {
                labelinfo.setText(err);
                icona.setImageResource(R.drawable.ic_error_black_48dp);
            }
        }
    }

}
