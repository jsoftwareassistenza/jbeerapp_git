package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaVisDocDettAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;

public class FormVisDettaglioDoc extends AppCompatActivity {
    private TextView tvdoc;
    private TextView tvsogg;
    private TextView tvtotimp;
    private TextView tvtotdoc;
    private TextView tvtotinc;
    private ListView listaart;
    private ListaVisDocDettAdapter ladapter;
    private ArrayList<Record> vart = null;
    private String movdoc = "";
    private String movdata = "";
    private String trasf = "";
    private int movnum = 0;
    private int movtipo = 0;
    private double acconto = 0;
    private String emailcliente = "";
    private TextView tvcli;
    private TextView tvtotmerce;
    private TextView tvtotaccisa;
    private LinearLayout lltotmerce;
    private LinearLayout lltotaccisa;
    private LinearLayout lltotincassato;
    private TextView tvtotpagare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_vis_dettaglio_doc);

        if (getIntent().getExtras() != null && getIntent().getExtras().getString("movdoc") != null) {
            movdoc = getIntent().getExtras().getString("movdoc");
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().getString("movdata") != null) {
            movdata = getIntent().getExtras().getString("movdata");
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().getInt("movnum") != 0) {
            movnum = getIntent().getExtras().getInt("movnum");
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().getString("trasf") != null) {
            trasf = getIntent().getExtras().getString("trasf");
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        tvdoc = findViewById(R.id.visdoc_doc);
        tvcli = findViewById(R.id.visdoc_cliente);
        tvsogg = findViewById(R.id.visdoc_cliente_dett);
        tvtotimp = findViewById(R.id.visdoc_riep_totimp);
        tvtotdoc = findViewById(R.id.visdoc_riep_totdoc);
        tvtotinc = findViewById(R.id.visdoc_riep_totinc);
        listaart = findViewById(R.id.visdoc_listaart);
        tvtotmerce = findViewById(R.id.visdoc_riep_totmerce);
        tvtotaccisa = findViewById(R.id.visdoc_riep_totaccisa);
        tvtotpagare = findViewById(R.id.visdoc_riep_nettopagare);
        lltotaccisa = findViewById(R.id.visdoc_lltotaccisa);
        lltotmerce = findViewById(R.id.visdoc_lltotmerce);
        lltotincassato = findViewById(R.id.visdoc_lltotincassato);

        if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
            lltotmerce.setVisibility(View.VISIBLE);
            lltotaccisa.setVisibility(View.VISIBLE);
        } else {
            lltotmerce.setVisibility(View.GONE);
            lltotaccisa.setVisibility(View.GONE);
        }


        if (movdoc.equals("XE")) {
            String[] pars = new String[2];
            pars[0] = movdata;
            pars[1] = "" + movnum;
            tvdoc.setText("XE n." + movnum + " del " + (new Data(movdata, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"));
            String cli = "sede";
            Cursor cs = Env.db.rawQuery("SELECT artcod,arttipoord FROM movcaricosedestor INNER JOIN articoli ON movcaricosedestor.mcartcod = articoli.artcod WHERE mcdata = ? AND mcnum= ? ORDER BY mcartcod", pars);
            if (cs.moveToFirst()) {
                if (cs.getInt(1) == 1)
                    cli = "sede";
                else {
                    Cursor cfa = Env.db.rawQuery(
                            "SELECT forcodice,fornome FROM forart INNER JOIN fornitori ON forart.faforcod = fornitori.forcodice WHERE faartcod = '" + cs.getString(0) + "'", null);
                    if (cfa.moveToFirst()) {
                        cli = cfa.getString(1);
                    }
                    cfa.close();
                }
            }
            cs.close();
            tvcli.setText(cli);
            //tvsogg.setText(cli);
            double totimp = 0;
            acconto = 0;
            vart = new ArrayList();
            boolean przzero = false;
            Cursor c = Env.db.rawQuery("SELECT mcartcod,mclotto,mcqta,mcprezzo,mcnetto FROM movcaricosedestor WHERE mcdata = ? AND mcnum= ? ORDER BY mcartcod", pars);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", c.getString(0));
                String[] parsa = new String[1];
                parsa[0] = c.getString(0);
                Cursor ca = Env.db.rawQuery("SELECT artdescr,artum FROM articoli WHERE artcod = ?", parsa);
                if (ca.moveToFirst()) {
                    rx.insStringa("artdescr", ca.getString(0));
                    rx.insStringa("um", ca.getString(1));
                } else {
                    rx.insStringa("artdescr", "");
                    rx.insStringa("um", "");
                }
                ca.close();
                rx.insStringa("lotto", c.getString(1));
                rx.insStringa("causale", "XE");
                rx.insIntero("colli", 0);
                rx.insDouble("qta", c.getDouble(2));
                rx.insDouble("prezzo", c.getDouble(3));
                rx.insDouble("sc1", 0);
                rx.insDouble("sc2", 0);
                rx.insDouble("sc3", 0);
                rx.insStringa("codiva", "");
                rx.insDouble("aliquota", 0);
                rx.insDouble("clisc1", 0);
                rx.insDouble("clisc2", 0);
                rx.insDouble("clisc3", 0);
                rx.insDouble("lordo", c.getDouble(4));
                rx.insDouble("netto", c.getDouble(4));
                vart.add(rx);
                totimp += c.getDouble(4);
                if (c.getDouble(3) == 0)
                    przzero = true;
            }
            c.close();
            totimp = OpValute.arrotondaMat(totimp, 2);
            tvtotimp.setText(Formattazione.formValuta(totimp, 12, 2, Formattazione.SEGNO_SX_NEG));
            tvtotdoc.setText(Formattazione.formValuta(totimp, 12, 2, Formattazione.SEGNO_SX_NEG));
            tvtotinc.setText("");
            if (przzero) {
                tvtotimp.setTextColor(Color.RED);
                tvtotdoc.setTextColor(Color.RED);
            } else {
                tvtotimp.setTextColor(Color.BLACK);
                tvtotdoc.setTextColor(Color.BLACK);
            }
        } else {
            String[] pars = new String[3];
            pars[0] = movdoc;
            pars[1] = movdata;
            pars[2] = "" + movnum;

            Cursor cm = Env.db.rawQuery("SELECT movtipo,movdoc,movsez,movdata,movnum,movdocora,movclicod,movdestcod,movpagcod," +
                    "movnumcolli,movtotimp,movtotiva,movtotale,movacconto,movnrif,movcesscod,movfor,movdatacons,movnote1,movacconto,movabbuoni FROM movimenti WHERE movdoc = ? AND movdata = ? AND movnum = ?", pars);
            cm.moveToFirst();

            String tipodoc = "";
            if (cm.getInt(0) == 0)
                tipodoc = "DDT";
            else if (cm.getInt(0) == 1)
                tipodoc = "DDT qtà";
            else if (cm.getInt(0) == 2)
                tipodoc = "Fattura";
            else if (cm.getInt(0) == 3)
                tipodoc = "Ric.fiscale";
            else if (cm.getInt(0) == 4)
                tipodoc = "DDT reso";
            else if (cm.getInt(0) == 5)
                tipodoc = "Integrazione da terminale";
            else if (cm.getInt(0) == 6)
                tipodoc = "Scarico a sede";
            else if (cm.getInt(0) == 7)
                tipodoc = "Trasf.VS automezzo";
            else if (cm.getInt(0) == 8)
                tipodoc = "Carico da altro automezzo";
            else if (cm.getInt(0) == 12)
                tipodoc = "Ordine a sede";
            else if (cm.getInt(0) == 20)
                tipodoc = "Ordine cliente";
            movtipo = cm.getInt(0);
            tipodoc += " n." + movnum + " del " + (new Data(movdata, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            if (!cm.getString(17).equals(""))
                tipodoc += "\nData consegna:" + (new Data(cm.getString(17), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            tvdoc.setText(tipodoc);
            String cli = "";
            String ssogg = "";
            if (cm.getInt(0) == 7) {
                ssogg = "VERSO: " + cm.getString(18);
            } else if (cm.getInt(0) == 8) {
                ssogg = "DA: " + cm.getString(18);
            } else if (!cm.getString(16).equals("")) {
                String[] parsf = new String[1];
                parsf[0] = cm.getString(16);
                final Cursor cfor = Env.db.rawQuery(
                        "SELECT prcodice,prdescr,prindir,prloc,prprov FROM provenienze WHERE prcodice = ?", parsf);
                if (cfor.moveToFirst()) {
                    cli += cfor.getString(0) + " " + cfor.getString(1);
                    ssogg += cfor.getString(2) + " " + cfor.getString(3) + " " + cfor.getString(4) + "\r\n";
                }
                cfor.close();
            } else if (!cm.getString(6).equals("")) {
                String[] parscli = new String[1];
                parscli[0] = cm.getString(6);
                Cursor ccli = Env.db.rawQuery(
                        "SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva,cliemail1,cliemail2,cliemailpec FROM clienti WHERE clicodice = ?", parscli);
                if (ccli.moveToFirst()) {
                    cli += ccli.getString(0) + " " + ccli.getString(1);
                    ssogg += ccli.getString(2) + " " + ccli.getString(3) + " " + ccli.getString(4) +
                            "\r\np.IVA:" + ccli.getString(5) + "\r\n";
                    if (!ccli.getString(6).equals(""))
                        emailcliente = ccli.getString(6);
                    else if (!ccli.getString(7).equals(""))
                        emailcliente = ccli.getString(7);
                    else if (!ccli.getString(8).equals(""))
                        emailcliente = ccli.getString(8);
                }
                ccli.close();
                if (!cm.getString(7).equals("")) {
                    String[] parsdest = new String[1];
                    parsdest[0] = cm.getString(7);
                    Cursor cdest = Env.db.rawQuery(
                            "SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva FROM clienti WHERE clicodice = ?", parsdest);
                    if (cdest.moveToFirst()) {
                        ssogg += "Consegna:\r\n" + cdest.getString(0) + cdest.getString(1) + cdest.getString(2) + " " + cdest.getString(3) +
                                " " + cdest.getString(4) + "\r\n";
                    }
                    cdest.close();
                }
                if (!cm.getString(8).equals("")) {
                    String pagdescr = "";
                    String[] parsp = new String[1];
                    parsp[0] = cm.getString(8);
                    Cursor cpag = Env.db.rawQuery(
                            "SELECT pagdescr,pagtipo FROM pagamenti WHERE pagcod = ?", parsp);
                    if (cpag.moveToFirst()) {
                        pagdescr = cpag.getString(0);
                    }
                    cpag.close();
                    ssogg += "Pagamento:\r\n" + cm.getString(8).toLowerCase() + "-" + pagdescr.toLowerCase();
                }
            }
            if (ssogg.equals("")) {
                if (cm.getInt(0) == 12)
                    ssogg = "Sede";
                else
                    ssogg = "";
            }

            tvcli.setText(cli);
            tvsogg.setText(ssogg);
            acconto = cm.getDouble(13);

            double totimp = 0;
            double totmerce = 0;
            double totaccisa = 0;
            vart = new ArrayList();
            Cursor c = Env.db.rawQuery("SELECT rmartcod,rmlotto,rmcaumag,rmcolli,rmpezzi,rmcontenuto,rmqta,rmprz,rmartsc1,rmartsc2," +
                    "rmscvend,rmcodiva,rmaliq,rmclisc1,rmclisc2,rmclisc3,rmlordo,rmnetto,rmnettoivato,rmaccisa FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? ORDER BY rmriga", pars);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", c.getString(0));
                String[] parsa = new String[1];
                parsa[0] = c.getString(0);
                Cursor ca = Env.db.rawQuery("SELECT artdescr,artum FROM articoli WHERE artcod = ?", parsa);
                if (ca.moveToFirst()) {
                    rx.insStringa("artdescr", ca.getString(0));
                    rx.insStringa("um", ca.getString(1));
                } else {
                    rx.insStringa("artdescr", "");
                    rx.insStringa("um", "");
                }
                ca.close();
                rx.insStringa("lotto", c.getString(1));
                rx.insStringa("causale", c.getString(2));
                rx.insIntero("colli", c.getInt(3));
                rx.insDouble("qta", c.getDouble(6));
                rx.insDouble("prezzo", c.getDouble(7));
                rx.insDouble("sc1", c.getDouble(8));
                rx.insDouble("sc2", c.getDouble(9));
                rx.insDouble("sc3", c.getDouble(10));
                rx.insStringa("codiva", c.getString(11));
                rx.insDouble("aliquota", c.getDouble(12));
                rx.insDouble("clisc1", c.getDouble(13));
                rx.insDouble("clisc2", c.getDouble(14));
                rx.insDouble("clisc3", c.getDouble(15));
                rx.insDouble("lordo", c.getDouble(16));
                rx.insDouble("netto", c.getDouble(17));
                totimp += c.getDouble(17) + c.getDouble(19);
                totmerce += c.getDouble(17);
                totaccisa += c.getDouble(19);
                vart.add(rx);
            }
            c.close();
            totimp = OpValute.arrotondaMat(totimp, 2);
            totaccisa = OpValute.arrotondaMat(totaccisa, 2);
            if (cm.getDouble(10) == 0) {
                tvtotimp.setText(Formattazione.formValuta(totimp, 12, 2, Formattazione.SEGNO_SX_NEG));
                tvtotdoc.setText(Formattazione.formValuta(totimp, 12, 2, Formattazione.SEGNO_SX_NEG));
                tvtotpagare.setText(Formattazione.formValuta(totimp + totaccisa, 12, 2, Formattazione.SEGNO_SX_NEG));
            } else {
                tvtotimp.setText(Formattazione.formValuta(cm.getDouble(10), 12, 2, Formattazione.SEGNO_SX_NEG));
                tvtotdoc.setText(Formattazione.formValuta(cm.getDouble(12), 12, 2, Formattazione.SEGNO_SX_NEG));
                tvtotpagare.setText(Formattazione.formValuta(cm.getDouble(12), 12, 2, Formattazione.SEGNO_SX_NEG));
            }
            tvtotmerce.setText(Formattazione.formValuta(totmerce, 12, 2, Formattazione.SEGNO_SX_NEG));
            tvtotaccisa.setText(Formattazione.formValuta(totaccisa, 12, 2, Formattazione.SEGNO_SX_NEG));

            double totinc = cm.getDouble(19);
            if (totinc != 0) {
                tvtotinc.setText(Formattazione.formValuta(totinc, 12, 2, Formattazione.SEGNO_SX_NEG));
            } else {
                lltotincassato.setVisibility(View.GONE);
            }
            cm.close();
        }
        ladapter = new ListaVisDocDettAdapter(this.getApplicationContext(), vart, this);
        listaart.setAdapter(ladapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.visdoc_menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.visdoc_action_stampa) {
            if (Env.tipostampante == 0 && (Env.ssidstampante.equals("") || Env.ipstampante.equals(""))) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormVisDettaglioDoc.this);
                builder.setTitle("Errore");
                builder.setMessage("Stampante WI-FI non configurata")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
                return true;

            } else if ((Env.tipostampante == 1 || Env.tipostampante == 2) && Env.nomebtstampante.equals("")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormVisDettaglioDoc.this);
                builder.setTitle("Errore");
                builder.setMessage("Stampante Bluethoot non configurata")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
                return true;
            }


            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormVisDettaglioDoc.this);
            builder.setMessage("Confermi ristampa di questo documento?")
                    .setPositiveButton("SI",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    Intent i = new Intent(FormVisDettaglioDoc.this.getApplicationContext(), FormInfoStampa.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("titolo", "Stampa Documento");
                                    i.putExtras(mBundle);
                                    startActivityForResult(i, 1);
                                }
                            })
                    .setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return true;
        } else if (item.getItemId() == R.id.visdoc_action_email) {
            if (!movdoc.equals(Env.docbollaval) && !movdoc.equals(Env.docbollaqta) && !movdoc.equals(Env.doccorrisp) && !movdoc.equals(Env.docddtreso) &&
                    !movdoc.equals(Env.docfattura) && !movdoc.equals(Env.docordinecli)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormVisDettaglioDoc.this);
                builder.setTitle("Errore");
                builder.setMessage("Invio documento per E-Mail previsto solo per documenti a clienti")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        finish();
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
            } else {
                ArrayList<Record> vdocs = new ArrayList();
                Record rdoc = new Record();
                rdoc.insStringa("doc", movdoc);
                rdoc.insIntero("num", movnum);
                rdoc.insStringa("data", movdata);
                vdocs.add(rdoc);
                FunzioniJBeerApp.invioEMailDocumento(FormVisDettaglioDoc.this, getApplicationContext(), vdocs, emailcliente);
/*
                File fx = new File("/storage/emulated/0/Android/data/jsoftware.jazztv");
                if (!fx.exists())
                    fx.mkdir();

                String nomefile = movdoc + "_" + movnum + "_" + movdata + ".pdf";
                nomefile = nomefile.replace("-", "_");
                boolean pdfok = FunzioniJazzTv.generaPDFDocumento(movdoc, movnum, movdata, getApplicationContext(), "/storage/emulated/0/Android/data/jsoftware.jazztv/" + nomefile);
                if (pdfok)
                {
                    String soggettomail = "";
                    String testomail = "";
                    if (movdoc.equals(Env.docbollaval))
                    {
                        soggettomail = "Invio DDT";
                        testomail = "In allegato il DDT n." + movnum + " del " + (new Data(movdata, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    } else if (movdoc.equals(Env.docbollaqta))
                    {
                        soggettomail = "Invio DDT";
                        testomail = "In allegato il DDT n." + movnum + " del " + (new Data(movdata, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    } else if (movdoc.equals(Env.docordinecli))
                    {
                        soggettomail = "Invio Ordine cliente";
                        testomail = "In allegato l'ordine cliente n." + movnum + " del " + (new Data(movdata, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    } else if (movdoc.equals(Env.docfattura))
                    {
                        soggettomail = "Invio Fattura";
                        testomail = "In allegato la fattura n." + movnum + " del " + (new Data(movdata, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    } else if (movdoc.equals(Env.doccorrisp))
                    {
                        soggettomail = "Invio Ricevuta fiscale";
                        testomail = "In allegato la ricevuta fiscale n." + movnum + " del " + (new Data(movdata, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    } else if (movdoc.equals(Env.docddtreso))
                    {
                        soggettomail = "Invio DDT di reso";
                        testomail = "In allegato il DDT di reso n." + movnum + " del " + (new Data(movdata, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    }
                    String[] maildest = new String[1];
                    maildest[0] = emailcliente;

                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, maildest);
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, soggettomail);
                    emailIntent.putExtra(Intent.EXTRA_TEXT, testomail);
                    //emailIntent.setType("image/jpeg");
                    emailIntent.setType("application/pdf");
                    //&emailIntent.putExtra(Intent.EXTRA_SUBJECT, "PDF Allegato");
                    emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///storage/emulated/0/Android/data/jsoftware.jazztv/" + nomefile));
                    emailIntent.setType("message/rfc822");
                    startActivity(emailIntent);
                } else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormVisDettaglioDoc.this);
                    builder.setMessage("Errore durante la generazione del PDF")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog,
                                                            int id)
                                        {
                                            finish();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
*/
            }
            return true;
        } else if (item.getItemId() == R.id.visdoc_action_pdf) {
            if (!movdoc.equals(Env.docbollaval) && !movdoc.equals(Env.docbollaqta) && !movdoc.equals(Env.doccorrisp) && !movdoc.equals(Env.docddtreso) &&
                    !movdoc.equals(Env.docfattura) && !movdoc.equals(Env.docordinecli)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormVisDettaglioDoc.this);
                builder.setTitle("Errore");
                builder.setMessage("Documento in PDF previsto solo per documenti a clienti")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        finish();
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
            } else {

                File fx = new File(getApplicationContext().getFilesDir() + File.separator + "tmp");
                //File fx = new File("/storage/emulated/0/Android/data/jsoftware.jbeerapp");
                if (!fx.exists())
                    fx.mkdir();

                String nomefile = movdoc + "_" + movnum + "_" + movdata + ".pdf";
                nomefile = nomefile.replace("-", "_");
                boolean pdfok = FunzioniJBeerApp.generaPDFDocumento(movdoc, movnum, movdata, getApplicationContext(), getApplicationContext().getFilesDir() + File.separator + "tmp" + File.separator + nomefile);

                if (pdfok) {
                    try {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        File newFile = new File(getApplicationContext().getFilesDir() + File.separator + "tmp" + File.separator + nomefile);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            Uri contentUri = FileProvider.getUriForFile(getApplicationContext(), "jsoftware.jbeerapp.fileprovider", newFile);
                            intent.setDataAndType(contentUri, "application/pdf");
                        } else {
                            intent.setDataAndType(Uri.fromFile(newFile), "application/pdf");
                        }
                        startActivity(intent);
                    } catch (Exception epdf) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormVisDettaglioDoc.this);
                        builder.setTitle("Errore");
                        builder.setMessage("Nessuna APP trovata per l'apertura di documenti PDF")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                finish();
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormVisDettaglioDoc.this);
                    builder.setTitle("Errore");
                    builder.setMessage("Errore durante la generazione del PDF")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
            }
            return true;
        } else if (item.getItemId() == R.id.visdoc_action_modifica) {
            if (Env.moddoc) {
                if (trasf.equals("S")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormVisDettaglioDoc.this);
                    builder.setMessage("Documento già inviato alla sede")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            return;
                                        }
                                    }).setTitle("Errore");
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                } else {
                    Intent i = new Intent(FormVisDettaglioDoc.this.getApplicationContext(), FormDocumento.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putBoolean("modificadoc", true);
                    mBundle.putInt("movtipo", movtipo);
                    mBundle.putString("movdoc", movdoc);
                    mBundle.putString("movdata", movdata);
                    mBundle.putInt("movnum", movnum);
                    i.putExtras(mBundle);
                    FormVisDettaglioDoc.this.startActivityForResult(i, 2);
                }
            } else {
                Toast toast = Toast.makeText(FormVisDettaglioDoc.this.getApplicationContext(), "Operazione non consentita!",
                        Toast.LENGTH_LONG);
                toast.show();
            }
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                // stampa documento
                try {
                    Thread.currentThread().sleep(4000);
                } catch (Exception ex) {
                }
                // stampa documento
//                while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//                {
//                    Log.v("JAZZTV", "BUSY");
//                    try
//                    {
//                        Thread.currentThread().sleep(500);
//                    }
//                    catch (Exception ex)
//                    {
//                    }
//                }
                if (movdoc.equals(Env.docordinesede)) {
                    FunzioniJBeerApp.stampaDocOrdineSede(movnum, movdata,
                            getApplicationContext());
                } else if (movdoc.equals(Env.docfattura)) {
                    FunzioniJBeerApp.stampaFatturaNew(movdoc, movnum, movdata,
                            acconto, 0, false, new ArrayList(), false, getApplicationContext());
                } else if (movdoc.equals(Env.docbollaval) || movdoc.equals(Env.docbollaqta) || movdoc.equals(Env.docddtreso)) {
                    FunzioniJBeerApp.stampaDDT(movdoc, movnum, movdata,
                            acconto, 0, false, new ArrayList(), false, getApplicationContext());
                } else if (movdoc.equals(Env.docscaricosede)) {
                    FunzioniJBeerApp.stampaDocScaricoSede(movnum, movdata, getApplicationContext());
                } else if (movdoc.equals(Env.doctrasfamezzo)) {
                    FunzioniJBeerApp.stampaDocTrasfVsMezzo(movnum, movdata, getApplicationContext());
                } else if (movdoc.equals(Env.doctrasfdamezzo)) {
                    FunzioniJBeerApp.stampaDocTrasfDaMezzo(movnum, movdata, getApplicationContext());
                } else if (movdoc.equals(Env.docordinesede)) {
                    FunzioniJBeerApp.stampaDocOrdineSede(movnum, movdata, getApplicationContext());
                } else if (movdoc.equals(Env.docddtcarico)) {
                    FunzioniJBeerApp.stampaIntegrazione(movnum, movdata, getApplicationContext(), false);
                } else if (movdoc.equals(Env.docordinecli)) {
                    FunzioniJBeerApp.stampaOrdineCliente(movdoc, movnum, movdata, getApplicationContext());
                } else if (movdoc.equals("XE")) {
                    FunzioniJBeerApp.stampaXeSede(movdata, movnum, true, getApplicationContext());
                }
                try {
                    Env.wifiPort.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                try {
                    Env.btPort.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            }
            case (2): {

                if (resultCode == Activity.RESULT_OK) {

                    ArrayList<HashMap> vris = (ArrayList<HashMap>) data.getExtras().get("vris");
                    for (int i = 0; i < vris.size(); i++) {
                        HashMap riga = vris.get(i);
                        movdoc = (String) riga.get("movdoc");
                        movdata = (String) riga.get("movdata");
                        movnum = (int) riga.get("movnum");
                        trasf = (String) riga.get("trasf");
                        movtipo = (int) riga.get("movtipo");

                    }
/*                    Intent i = new Intent(FormVisDettaglioDoc.this.getApplicationContext(), FormVisDettaglioDoc.class);  //your class
                    Bundle mBundle = new Bundle();
                    mBundle.putString("movdoc", movdoc);
                    mBundle.putString("movdata", movdata);
                    mBundle.putInt("movnum", movnum);
                    mBundle.putString("trasf", trasf);
                    mBundle.putInt("movtipo", movtipo);
                    i.putExtras(mBundle);
                    startActivity(i);*/
                    onBackPressed();

                }
                break;
            }
        }
    }
}
