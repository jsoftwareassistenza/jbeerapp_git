package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sewoo.jpos.command.CPCLConst;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaRegIncScopAdapter;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.env.funzStringa;

public class FormRegIncassi extends AppCompatActivity {
    public String cod = "";
    public String codric = "";
    private int posselez = -1;

    private TextView tvcliente;
    private ListView listascop;
    private ListaRegIncScopAdapter lscopadapter;
    private ArrayList<Record> vscop = null;
    private ArrayList<Record> vsld = null;
    private ImageButton briccli;
    private Button bec;
    private Button besci;
    private Button bristampa;
    private Button bincmult;
    private TextView tvtotscop;
    private TextView tvtotincmult;
    private String ccod = "";
    private String cnome = "";
    private String cind = "";
    private String civa = "";
    private boolean flagStampa = false;
    private double totale = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_reg_incassi);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        tvcliente = findViewById(R.id.reginc_cliente);
        briccli = findViewById(R.id.reginc_buttonRicCli);
        bec = findViewById(R.id.reginc_buttonEc);
        besci = findViewById(R.id.reginc_buttonRicevutaEsci);
        bristampa = findViewById(R.id.reginc_buttonRistampaIncassi);
        bincmult = findViewById(R.id.reginc_buttonIncassoMultiplo);
        listascop = findViewById(R.id.reginc_listascop);
        tvtotincmult = findViewById(R.id.reginc_totincmult);

        if (Env.incnostampa) {
            bec.setVisibility(View.INVISIBLE);
            LinearLayout.LayoutParams lParams = (LinearLayout.LayoutParams) bec.getLayoutParams();
            lParams.width = 0;
            lParams.height = 0;
            besci.setText("Esci");
        }


        registerForContextMenu(listascop);

        View header = this.getLayoutInflater().inflate(R.layout.listareginc_listascop_header, null);
        listascop.addHeaderView(header);
        final View footer = this.getLayoutInflater().inflate(R.layout.listareginc_listascop_footer, null);
        listascop.addFooterView(footer);

        vsld = new ArrayList();
        vscop = new ArrayList();
        lscopadapter = new ListaRegIncScopAdapter(this.getApplicationContext(), vscop, this);
        listascop.setAdapter(lscopadapter);

        tvtotscop = findViewById(R.id.listarecinc_footer_totscop);
        aggiornaTotale();
        aggiornaTotaleIncassoMultiplo();

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cod"))
            cod = getIntent().getExtras().getString("cod");

        if (!cod.equals("")) {
            flagStampa = true;
            codric = cod;
            // cerca cliente padre
            String[] pars = new String[1];
            pars[0] = codric;
            Cursor cursor = Env.db.rawQuery(
                    "SELECT clicodpadre FROM clienti WHERE clicodice=?", pars);
            cursor.moveToFirst();
            if (!cursor.getString(0).equals(""))
                codric = cursor.getString(0);
            cursor.close();
            this.visualizzaDatiCliente();
            briccli.setEnabled(false);
        }

        Env.db.execSQL("DELETE FROM ddtsospchiusi");

        besci.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!flagStampa && vsld.size() > 0 && !Env.termordini && !Env.incnostampa) {
                    if (Env.tipostampante == 0 && (Env.ssidstampante.equals("") || Env.ipstampante.equals(""))) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormRegIncassi.this);
                        builder.setMessage("Stampante WI-FI non configurata")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                finish();
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return;
                    } else if ((Env.tipostampante == 1 || Env.tipostampante == 2) && Env.nomebtstampante.equals("")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormRegIncassi.this);
                        builder.setMessage("Stampante Bluetooth non configurata")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                finish();
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return;
                    } else {
                        Intent i = new Intent(FormRegIncassi.this.getApplicationContext(), FormInfoStampa.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString("titolo", "Stampa ricevuta incassi");
                        i.putExtras(mBundle);
                        startActivityForResult(i, 5);
                    }
                } else {
                    finish();
                }
            }
        });

        bec.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!Env.termordini && !Env.incnostampa) {
                    if (codric.equals("")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormRegIncassi.this);
                        builder.setMessage("Selezionare un cliente")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                return;
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return;
                    } else if ((Env.tipostampante == 1 || Env.tipostampante == 2) && Env.nomebtstampante.equals("")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormRegIncassi.this);
                        builder.setMessage("Stampante WI-FI non configurata")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                return;
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return;
                    } else if (Env.tipostampante == 1 && Env.nomebtstampante.equals("")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormRegIncassi.this);
                        builder.setMessage("Stampante Bluetooth non configurata")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                return;
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return;
                    } else {
                        Intent i = new Intent(FormRegIncassi.this.getApplicationContext(), FormInfoStampa.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString("titolo", "Stampa estratto conto");
                        i.putExtras(mBundle);
                        startActivityForResult(i, 4);
                    }
                }
            }
        });

        briccli.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(FormRegIncassi.this.getApplicationContext(), FormRicercaClienti.class);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean("solodest", false);
                mBundle.putBoolean("nodest", true);
                i.putExtras(mBundle);
                FormRegIncassi.this.startActivityForResult(i, 1);
            }
        });

        bincmult.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int nsel = 0;
                double totscopsel = 0;
                final ArrayList<Record> vsel = new ArrayList();
                for (Record r : vscop) {
                    if (r.esisteCampo("sel")) {
                        vsel.add(r);
                        if (r.leggiStringa("tipoop").equals("V"))
                            totscopsel += r.leggiDouble("residuoscad");
                        else
                            totscopsel -= r.leggiDouble("residuoscad");
                        nsel++;
                    }
                }
                totscopsel = OpValute.arrotondaMat(totscopsel, 2);
                if (nsel == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRegIncassi.this);
                    builder.setMessage("Selezionare almeno uno scoperto da chiudere")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                }

                final double tss = totscopsel;
                {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            FormRegIncassi.this);
                    final AlertDialog optionDialog = alert.create();
                    optionDialog.setMessage("Registrazione incasso multiplo");
                    LinearLayout ll = new LinearLayout(FormRegIncassi.this.getApplicationContext());
                    ll.setOrientation(LinearLayout.VERTICAL);
                    final EditText inputimp = new EditText(FormRegIncassi.this);
                    inputimp.setHint("importo incassato totale");
                    inputimp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                    inputimp.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputimp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputimp.setTypeface(inputimp.getTypeface(), Typeface.BOLD);
                    inputimp.setText(Formattazione.formatta(totale, "######0.00", 0));
                    ll.addView(inputimp);
                    EditText inputdescr = null;
                    if (Env.incattivanote) {
                        inputdescr = new EditText(FormRegIncassi.this);
                        inputdescr.setHint("descrizione libera");
                        inputdescr.setInputType(InputType.TYPE_CLASS_TEXT);
                        inputdescr.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                        inputdescr.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                        inputdescr.setTypeface(inputdescr.getTypeface(), Typeface.BOLD);
                        ll.addView(inputdescr);
                    }
                    Spinner spcassa = null;
                    if (Env.incsceglicassa) {
                        spcassa = new Spinner(FormRegIncassi.this);
                        ArrayList lsp = new ArrayList();
                        lsp.add("CASSA CONTANTI");
                        lsp.add("CASSA NON CONTANTI");
                        SpinnerAdapter lspadapter = new SpinnerAdapter(FormRegIncassi.this.getApplicationContext(), R.layout.spinner_item, lsp);
                        spcassa.setAdapter(lspadapter);
                        ll.addView(spcassa);
                    }
                    final TextView tvinfo = new TextView(FormRegIncassi.this);
                    tvinfo.setTextAppearance(FormRegIncassi.this, R.style.TextViewLabel1);
                    tvinfo.setTextColor(Color.RED);
                    tvinfo.setTypeface(tvinfo.getTypeface(), Typeface.BOLD);
                    tvinfo.setText("");
                    ll.addView(tvinfo);
                    LinearLayout lb = new LinearLayout(FormRegIncassi.this.getApplicationContext());
                    lb.setOrientation(LinearLayout.HORIZONTAL);
                    ll.addView(lb);
                    Button bdok = new Button(FormRegIncassi.this);
                    bdok.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                    bdok.setTextColor(Color.WHITE);
                    bdok.setText("OK");
                    lb.addView(bdok);
                    Button bdann = new Button(FormRegIncassi.this);
                    bdann.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                    bdann.setTextColor(Color.WHITE);
                    bdann.setText("Annulla");
                    lb.addView(bdann);
                    LinearLayout.LayoutParams lParams1 = (LinearLayout.LayoutParams) bdok.getLayoutParams();
                    lParams1.weight = 0.5f;
                    lParams1.width = 0;
                    LinearLayout.LayoutParams lParams2 = (LinearLayout.LayoutParams) bdann.getLayoutParams();
                    lParams2.weight = 0.5f;
                    lParams2.width = 0;

                    final EditText edtmp = inputdescr;
                    final Spinner sptmp = spcassa;

                    bdok.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            String value = inputimp.getText().toString();
                            String descr = "";
                            int tipocassa = 0;
                            if (Env.incattivanote) {
                                descr = edtmp.getText().toString();
                                if (descr.length() > 255)
                                    descr = descr.substring(0, 255);
                            }
                            if (Env.incsceglicassa) {
                                tipocassa = sptmp.getSelectedItemPosition();
                            }
                            double imp = Formattazione.estraiDouble(value.replace(".", ","));
//                            if (tss < imp)
//                            {
//                                tvinfo.setText("ERRORE: il totale degli scoperti selezionati (" + Formattazione.formValuta(tss, 12, 2, 1) + ") è minore dell'importo totale incassato");
//                                return;
//                            }
//                            else
                            {
                                double residuodainc = imp;
                                for (Record rs : vsel) {
                                    if (residuodainc != 0) {
                                        double scop = 0;
                                        double impinc = 0;
                                        if (rs.leggiStringa("tipoop").equals("V"))
                                            scop = rs.leggiDouble("residuoscad");
                                        else
                                            scop = -rs.leggiDouble("residuoscad");
                                        if (Math.abs(residuodainc) > Math.abs(scop)) {
                                            impinc = scop;
                                            residuodainc = OpValute.arrotondaMat(residuodainc - scop, 2);
                                        } else {
                                            impinc = residuodainc;
                                            residuodainc = 0;
                                        }
                                        effettuaIncasso(rs, impinc, descr, tipocassa);
                                    }
                                }

                                for (Record rs : vsel) {
                                    if (rs.esisteCampo("sel"))
                                        rs.eliminaCampo("sel");
                                }

                                String descrop = "REG.INCASSO CLIENTE " + codric + " IMP." + Formattazione.formValuta(imp, 12, 2, 2);
                                FunzioniJBeerApp.salvaLog(descrop);
                                FunzioniJBeerApp.impostaProprieta("stato", "SESSIONE");
                                optionDialog.dismiss();
                            }
                        }

                    });

                    bdann.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            optionDialog.dismiss();
                        }
                    });

                    optionDialog.setView(ll);
                    optionDialog.setCancelable(false);
                    optionDialog.show();

                }
            }
        });

        bristampa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
/*
                if (codric.equals(""))
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRegIncassi.this);
                    builder.setMessage("Selezionare un cliente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog,
                                                            int id)
                                        {
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                }
*/
                Intent i = new Intent(FormRegIncassi.this.getApplicationContext(), FormRistampaIncassi.class);
                Bundle mBundle = new Bundle();
                mBundle.putString("clicod", codric);
                i.putExtras(mBundle);
                FormRegIncassi.this.startActivity(i);
            }
        });

        listascop.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
                posselez = (int) arg3;
                return false;
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                // ricerca clienti
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    String cx = ris.getLastPathSegment();
                    if (!cx.equals("")) {
                        cx = cx.replace('=', '/');
                        codric = cx;
                        visualizzaDatiCliente();
                    }
                }
                break;
            }
            case (4): {
                // stampa estratto conto
                if (resultCode == Activity.RESULT_OK) {
                    stampaEstrattoConto(getApplicationContext());
                    try {
                        Env.wifiPort.disconnect();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    try {
                        Env.btPort.disconnect();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            }
            case (5): {
                // stampa ricevuta
                if (resultCode == Activity.RESULT_OK) {
                    stampaRicevuta(getApplicationContext());
                    try {
                        Env.wifiPort.disconnect();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    try {
                        Env.btPort.disconnect();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRegIncassi.this);
                    builder.setMessage("Effettuare un'altra stampa?")
                            .setPositiveButton("SI",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            Intent i = new Intent(FormRegIncassi.this.getApplicationContext(), FormInfoStampa.class);
                                            Bundle mBundle = new Bundle();
                                            mBundle.putString("titolo", "Stampa ricevuta incassi");
                                            i.putExtras(mBundle);
                                            startActivityForResult(i, 5);
                                        }
                                    })
                            .setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            finish();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                } else {
                    finish();
                }
                break;
            }
        }
    }

    public void visualizzaDatiCliente() {
        String[] pars = new String[1];
        pars[0] = codric;
        Cursor cursor = Env.db.rawQuery(
                "SELECT clicodice,clinome,cliindir," +
                        "cliloc,cliprov,clipiva FROM clienti WHERE clicodice = ?", pars);
        cursor.moveToFirst();
        ccod = cursor.getString(0);
        cnome = cursor.getString(1);
        cind = cursor.getString(2) + " " + cursor.getString(3) + " " + cursor.getString(4);
        civa = cursor.getString(5);
        String infocli = cursor.getString(0) + " " + cursor.getString(1) + "\r\n"
                + cursor.getString(2) + "\r\n"
                + cursor.getString(3) + " " + cursor.getString(4) + "\r\n"
                + "p.IVA:" + cursor.getString(5) + "\r\n";
        tvcliente.setText(infocli);
        cursor.close();

        vscop.clear();
        cursor = Env.db.rawQuery(
                "SELECT sctipo,scsezdoc,scdatadoc,scnumdoc,sctipoop,scnote,scclicod,scdestcod," +
                        "scdatascad,sctiposcad,scimportoscad,scresiduoscad,scscadid,scrateizzata FROM scoperti WHERE scclicod=? " +
                        " AND scresiduoscad > 0 ORDER BY scdatadoc DESC,scnumdoc ASC", pars);
        while (cursor.moveToNext()) {
            Boolean scopok = true;
            // se bolla sospesa e presente fattura di fine mese, non stampa la bolla
            if (cursor.getString(0).equals("B")) {
                int anno = Formattazione.estraiIntero(cursor.getString(2).substring(0, 4));
                int mese = Formattazione.estraiIntero(cursor.getString(2).substring(5, 7));
                mese++;
                if (mese > 12) {
                    anno++;
                    mese = 1;
                }
                Data dt = new Data(1, mese, anno);
                dt.decrementaGiorni(1);
                String dtf = dt.formatta(Data.AAAA_MM_GG, "-");
                String[] pars2 = new String[2];
                pars2[0] = codric;
                pars2[1] = dtf;
                Cursor cursor2 = Env.db.rawQuery(
                        "SELECT * FROM scoperti WHERE scclicod=? AND scresiduoscad > 0 AND sctipo = 'F' AND scdatadoc=?", pars2);
                if (cursor2.moveToFirst())
                    scopok = false;
                cursor2.close();
            }
            // prepara riga
            Record r = new Record();
            r.insStringa("tipo", cursor.getString(0));
            r.insStringa("sezdoc", cursor.getString(1));
            r.insStringa("datadoc", cursor.getString(2));
            r.insIntero("numdoc", cursor.getInt(3));
            r.insStringa("tipoop", cursor.getString(4));
            r.insStringa("note", cursor.getString(5));
            r.insIntero("scadid", cursor.getInt(12));
            r.insStringa("datascad", cursor.getString(8));
            r.insIntero("tiposcad", cursor.getInt(9));
            r.insDouble("importoscad", cursor.getDouble(10));
            r.insDouble("residuoscad", cursor.getDouble(11));
            r.insDouble("saldo", 0);
            r.insIntero("rateizzata", cursor.getInt(13));
            r.insIntero("calctot", scopok ? 1 : 0);
            vscop.add(r);
        }
        cursor.close();
        lscopadapter.notifyDataSetChanged();
        aggiornaTotale();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(null);
        //menu.add(0, v.getId(), 0, "Annulla sospeso");
        menu.add(0, v.getId(), 0, "Incassa sospeso");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals("Annulla sospeso")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormRegIncassi.this);
            builder.setMessage("Questa operazione cancella lo scoperto selezionato (da usare in caso di scoperto errato).Confermi eliminazione scoperto?")
                    .setPositiveButton("SI",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    // elimina record del sospeso
                                    Record rec = vscop.get(posselez);
                                    SQLiteStatement std = Env.db.compileStatement(
                                            "DELETE FROM scoperti WHERE sctipo=? AND scsezdoc=? AND scdatadoc=? AND " +
                                                    "scnumdoc=? AND scclicod=? AND scscadid=?");
                                    std.clearBindings();
                                    std.bindString(1, rec.leggiStringa("tipo"));
                                    std.bindString(2, rec.leggiStringa("sezdoc"));
                                    std.bindString(3, rec.leggiStringa("datadoc"));
                                    std.bindLong(4, rec.leggiIntero("numdoc"));
                                    std.bindString(5, codric);
                                    std.bindLong(6, rec.leggiIntero("scadid"));
                                    std.execute();
                                    std.close();

                                    std = Env.db.compileStatement(
                                            "DELETE FROM incassi WHERE isctipo=? AND iscsezdoc=? AND iscdatadoc=? AND " +
                                                    "iscnumdoc=? AND iscclicod=? AND iscscadid=?");
                                    std.clearBindings();
                                    std.bindString(1, rec.leggiStringa("tipo"));
                                    std.bindString(2, rec.leggiStringa("sezdoc"));
                                    std.bindString(3, rec.leggiStringa("datadoc"));
                                    std.bindLong(4, rec.leggiIntero("numdoc"));
                                    std.bindString(5, codric);
                                    std.bindLong(6, rec.leggiIntero("scadid"));
                                    std.execute();
                                    std.close();
                                    vscop.remove(posselez);
                                    lscopadapter.notifyDataSetChanged();
                                    aggiornaTotale();
                                }
                            })
                    .setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
        } else if (item.getTitle().equals("Incassa sospeso")) {
            final Record rec = vscop.get(posselez);
            if (rec != null) {
                final String tipoop = rec.leggiStringa("tipoop");
                if (!rec.leggiStringa("datadoc").equals("") && !rec.leggiStringa("datadoc").equals("0000-00-00") &&
                        FunzioniJBeerApp.clienteContanti(codric) && rec.leggiStringa("tipo").equals("B")) {
                    String[] pars = new String[3];
                    pars[0] = rec.leggiStringa("sezdoc");
                    pars[1] = rec.leggiStringa("datadoc");
                    pars[2] = "" + rec.leggiIntero("numdoc");
                    Cursor cursor = Env.db.rawQuery(
                            "SELECT * FROM ddtfatt WHERE sufddt=? AND dtddt=? AND numddt=?", pars);
                    Boolean fattpres = false;
                    if (cursor.moveToFirst())
                        fattpres = true;
                    cursor.close();
                    if (fattpres) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormRegIncassi.this);
                        builder.setMessage("Fattura di fine mese presente, obbligatorio incassare la fattura di fine mese")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                return;
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return false;
                    }
                }
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        FormRegIncassi.this);
                final AlertDialog optionDialog = alert.create();
                optionDialog.setMessage("Registrazione incasso");
                LinearLayout ll = new LinearLayout(FormRegIncassi.this.getApplicationContext());
                ll.setOrientation(LinearLayout.VERTICAL);
                final TextView tvscop = new TextView(FormRegIncassi.this);
                tvscop.setText("Importo: " + Formattazione.formValuta(rec.leggiDouble("residuoscad"), 12, 2, 1));
                tvscop.setTypeface(tvscop.getTypeface(), Typeface.BOLD);
                tvscop.setTextColor(Color.BLACK);
                tvscop.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                ll.addView(tvscop);
                final EditText inputimp = new EditText(FormRegIncassi.this);
                inputimp.setHint("importo incassato");
                inputimp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                inputimp.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                ll.addView(inputimp);
                EditText inputdescr = null;
                if (Env.incattivanote) {
                    inputdescr = new EditText(FormRegIncassi.this);
                    inputdescr.setHint("descrizione libera");
                    inputdescr.setInputType(InputType.TYPE_CLASS_TEXT);
                    inputdescr.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    ll.addView(inputdescr);
                }
                Spinner spcassa = null;
                if (Env.incsceglicassa) {
                    spcassa = new Spinner(FormRegIncassi.this);
                    ArrayList lsp = new ArrayList();
                    lsp.add("CASSA CONTANTI");
                    lsp.add("CASSA NON CONTANTI");
                    SpinnerAdapter lspadapter = new SpinnerAdapter(FormRegIncassi.this.getApplicationContext(), R.layout.spinner_item, lsp);
                    spcassa.setAdapter(lspadapter);
                    ll.addView(spcassa);
                }
                final TextView tvinfo = new TextView(FormRegIncassi.this);
                tvinfo.setTextColor(Color.RED);
                tvinfo.setTypeface(tvinfo.getTypeface(), Typeface.BOLD);
                tvinfo.setText("");
                tvinfo.setTextAppearance(this, R.style.TextViewLabel1);
                ll.addView(tvinfo);

                LinearLayout lb = new LinearLayout(FormRegIncassi.this.getApplicationContext());
                lb.setOrientation(LinearLayout.HORIZONTAL);
                ll.addView(lb);
                Button bdok = new Button(FormRegIncassi.this);
                bdok.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                bdok.setTextColor(Color.WHITE);
                bdok.setText("OK");
                //bdok.setTextAppearance(this, R.style.Button1);
                lb.addView(bdok);
                Button bdann = new Button(FormRegIncassi.this);
                bdann.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                bdann.setTextColor(Color.WHITE);
                bdann.setText("Annulla");
                //bdann.setTextAppearance(this, R.style.Button1);
                lb.addView(bdann);
                LinearLayout.LayoutParams lParams1 = (LinearLayout.LayoutParams) bdok.getLayoutParams();
                lParams1.weight = 0.5f;
                lParams1.width = 0;
                LinearLayout.LayoutParams lParams2 = (LinearLayout.LayoutParams) bdann.getLayoutParams();
                lParams2.weight = 0.5f;
                lParams2.width = 0;

                final EditText edtmp = inputdescr;
                final Spinner sptmp = spcassa;

                bdok.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        String value = inputimp.getText().toString();
                        String descr = "";
                        int tipocassa = 0;
                        if (Env.incattivanote) {
                            descr = edtmp.getText().toString();
                            if (descr.length() > 255)
                                descr = descr.substring(0, 255);
                        }
                        if (Env.incsceglicassa) {
                            tipocassa = sptmp.getSelectedItemPosition();
                        }
                        double imp = Formattazione.estraiDouble(value.replace(".", ","));
                        double tot = OpValute.arrotondaMat(rec.leggiDouble("residuoscad") - rec.leggiDouble("saldo"), 2);
                        if (tipoop.equals("V") && imp < 0) {
                            tvinfo.setText("ERRORE: Inserire importo positivo");
                            return;
                        } else if (tipoop.equals("R") && imp > 0) {
                            tvinfo.setText("ERRORE: Inserire importo negativo");
                            return;
                        }
                        if (imp > tot) {
                            tvinfo.setText("ERRORE: Importo a saldo maggiore di quello da saldare");
                            return;
                        }
                        // controllo cliente pagtv con sospesi mese precedente
                        if (FunzioniJBeerApp.clienteContanti(codric) && FunzioniJBeerApp.DDTScopertiMesePrecedente(codric, new Data())) {
                            if (Math.abs(tot) != Math.abs(imp)) {
                                tvinfo.setText("ERRORE: Saldo ammesso solo per l'intero importo dello scoperto");
                                return;
                            }
                        }
                        // ins.saldo
                        if (imp != 0) {
                            effettuaIncasso(rec, imp, descr, tipocassa);

                            lscopadapter.notifyDataSetChanged();
                            aggiornaTotale();

                            String descrop = "REG.INCASSO CLIENTE " + codric + " IMP." + Formattazione.formValuta(imp, 12, 2, 2);
                            FunzioniJBeerApp.salvaLog(descrop);

                            FunzioniJBeerApp.impostaProprieta("stato", "SESSIONE");

                            Toast toast = Toast.makeText(getApplicationContext(), "Incasso registrato",
                                    Toast.LENGTH_SHORT);
                            toast.show();
                        } else {
                            Toast toast = Toast.makeText(getApplicationContext(), "Saldo zero, Incasso non registrato",
                                    Toast.LENGTH_SHORT);
                            toast.show();
                        }
                        optionDialog.dismiss();
                    }
                });

                bdann.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        optionDialog.dismiss();
                    }
                });

                optionDialog.setView(ll);
                optionDialog.setCancelable(false);
                optionDialog.show();
            }
        } else {
            return false;
        }
        return true;
    }

    private void effettuaIncasso(Record rec, double imp, String descr, int tipocassa) {
        if (descr.length() > 255)
            descr = descr.substring(0, 255);
        // eventuale aggiornamento documento non trasferito
        double movacc = 0;
        boolean doces = false;
        int tipomovim = -1;
        if (rec.leggiStringa("tipo").equals("B"))
            tipomovim = 0;
        if (rec.leggiStringa("tipo").equals("F"))
            tipomovim = 2;
        if (rec.leggiStringa("tipo").equals("N"))
            tipomovim = 4;
        String[] pars = new String[3];
        pars[0] = "" + tipomovim;
        pars[1] = rec.leggiStringa("datadoc");
        pars[2] = "" + rec.leggiIntero("numdoc");
        Cursor cursor = Env.db.rawQuery(
                "SELECT movacconto FROM movimenti WHERE movtipo = ? AND movdata = ? AND movnum = ? AND movtrasf='N'", pars);
        if (cursor.moveToFirst()) {
            movacc = cursor.getDouble(0);
            doces = true;
        }
        cursor.close();
        if (doces && tipomovim != 2) {
            // documento esistente e non trasferito -> aggiorna acconto doc.
            SQLiteStatement sta = Env.db.compileStatement(
                    "UPDATE movimenti SET movacconto = ? WHERE movtipo = ? AND movdata = ? AND movnum = ? AND movtrasf='N'");
            sta.clearBindings();
            sta.bindDouble(1, OpValute.arrotondaMat(movacc + imp, 2));
            sta.bindLong(2, tipomovim);
            sta.bindString(3, rec.leggiStringa("datadoc"));
            sta.bindLong(4, rec.leggiIntero("numdoc"));
            sta.execute();
            sta.close();
        } else {
            // documento non esistente o già trasferito o fattura emessa nella sessione ->
            // ins.saldo
            SQLiteStatement sti = Env.db.compileStatement(
                    "INSERT INTO incassi (isctipo,iscsezdoc,iscdatadoc,iscnumdoc," +
                            "isctipoop,iscclicod,iscdestcod,iscscadid,iscdatascad,isctiposcad,iscresiduoscad,isaldo,iscdatainc,iscorainc,iscdescr,iscdescr2,isctipocassa) " +
                            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            sti.clearBindings();
            sti.bindString(1, rec.leggiStringa("tipo"));
            sti.bindString(2, rec.leggiStringa("sezdoc"));
            sti.bindString(3, rec.leggiStringa("datadoc"));
            sti.bindLong(4, rec.leggiIntero("numdoc"));
            sti.bindString(5, rec.leggiStringa("tipoop"));
            sti.bindString(6, codric);
            sti.bindString(7, "");
            sti.bindLong(8, rec.leggiIntero("scadid"));
            sti.bindString(9, rec.leggiStringa("datascad"));
            sti.bindLong(10, rec.leggiIntero("tiposcad"));
            sti.bindDouble(11, rec.leggiDouble("residuoscad"));
            if (!rec.leggiStringa("tipoop").equals("R"))
                sti.bindDouble(12, imp);
            else
                sti.bindDouble(12, -imp);
            String datainc = (new Data()).formatta(Data.AAAA_MM_GG, "-");
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String orainc = dh.format(new Date());
            sti.bindString(13, datainc);
            sti.bindString(14, orainc);
            sti.bindString(15, "");
            sti.bindString(16, descr);
            sti.bindLong(17, tipocassa);
            sti.execute();
            sti.close();

            SQLiteStatement stsi = Env.db.compileStatement(
                    "INSERT INTO storico_incassi (sisctipo,siscsezdoc,siscdatadoc,siscnumdoc," +
                            "sisctipoop,siscclicod,siscdestcod,siscscadid,siscdatascad,sisctiposcad,siscresiduoscad,sisaldo,siscdatainc,siscorainc,siscdescr,siscdescr2,sisctipocassa) " +
                            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            stsi.clearBindings();
            stsi.bindString(1, rec.leggiStringa("tipo"));
            stsi.bindString(2, rec.leggiStringa("sezdoc"));
            stsi.bindString(3, rec.leggiStringa("datadoc"));
            stsi.bindLong(4, rec.leggiIntero("numdoc"));
            stsi.bindString(5, rec.leggiStringa("tipoop"));
            stsi.bindString(6, codric);
            stsi.bindString(7, "");
            stsi.bindLong(8, rec.leggiIntero("scadid"));
            stsi.bindString(9, rec.leggiStringa("datascad"));
            stsi.bindLong(10, rec.leggiIntero("tiposcad"));
            stsi.bindDouble(11, rec.leggiDouble("residuoscad"));
            if (!rec.leggiStringa("tipoop").equals("R"))
                stsi.bindDouble(12, imp);
            else
                stsi.bindDouble(12, -imp);
            stsi.bindString(13, datainc);
            stsi.bindString(14, orainc);
            stsi.bindString(15, "");
            stsi.bindString(16, descr);
            stsi.bindLong(17, tipocassa);
            stsi.execute();
            stsi.close();
        }

        // aggiornamento scoperto su db
        double res = 0;
        if (!rec.leggiStringa("tipoop").equals("R")) // se non è reso ...  Nota di credito
            res = OpValute.arrotondaMat(rec.leggiDouble("residuoscad") - rec.leggiDouble("saldo") - imp, 2);
        else
            res = OpValute.arrotondaMat(rec.leggiDouble("residuoscad") - rec.leggiDouble("saldo") + imp, 2);
        SQLiteStatement sta = Env.db.compileStatement(
                "UPDATE scoperti SET scresiduoscad=? WHERE sctipo=? AND scsezdoc=? AND scdatadoc=? AND " +
                        "scnumdoc=? AND scclicod=? AND scscadid=?");
        sta.clearBindings();
        sta.bindDouble(1, res);
        sta.bindString(2, rec.leggiStringa("tipo"));
        sta.bindString(3, rec.leggiStringa("sezdoc"));
        sta.bindString(4, rec.leggiStringa("datadoc"));
        sta.bindLong(5, rec.leggiIntero("numdoc"));
        sta.bindString(6, codric);
        sta.bindLong(7, rec.leggiIntero("scadid"));
        sta.execute();
        sta.close();

        if (!rec.leggiStringa("tipoop").equals("R")) {
            double sold = rec.leggiDouble("saldo");
            rec.eliminaCampo("saldo");
            rec.insDouble("saldo", OpValute.arrotondaMat(sold + imp, 2));
        } else {
            double sold = rec.leggiDouble("saldo");
            rec.eliminaCampo("saldo");
            rec.insDouble("saldo", -OpValute.arrotondaMat(sold + imp, 2));
        }
        vsld.add(rec);

        // se fattura cliente cancella eventuali ddt sospesi della fattura
        if (!rec.leggiStringa("datadoc").equals("") && !rec.leggiStringa("datadoc").equals("0000-00-00") &&
                rec.leggiStringa("tipo").equals("F")) {
            String[] parsd = new String[3];
            parsd[0] = rec.leggiStringa("sezdoc");
            parsd[1] = rec.leggiStringa("datadoc");
            parsd[2] = "" + rec.leggiIntero("numdoc");
            Cursor cd = Env.db.rawQuery(
                    "SELECT sufddt,dtddt,numddt FROM ddtfatt WHERE suffatt=? AND dtfatt=? AND numfatt=?", parsd);
            while (cd.moveToNext()) {
                String[] parsd2 = new String[3];
                parsd2[0] = cd.getString(0);
                parsd2[1] = cd.getString(1);
                parsd2[2] = "" + cd.getInt(2);
                Cursor cd2 = Env.db.rawQuery(
                        "SELECT scsezdoc,scdatadoc,scnumdoc,scresiduoscad FROM scoperti WHERE sctipo = 'B' AND scsezdoc=? AND scdatadoc=? AND scnumdoc=?", parsd2);
                if (cd2.moveToFirst()) {
                    SQLiteStatement sti2 = Env.db.compileStatement(
                            "INSERT INTO ddtsospchiusi (dscsezdoc,dscdatadoc,dscnumdoc,dscimporto) VALUES (?,?,?,?)");
                    sti2.clearBindings();
                    sti2.bindString(1, cd2.getString(0));
                    sti2.bindString(2, cd2.getString(1));
                    sti2.bindLong(3, cd2.getInt(2));
                    sti2.bindDouble(4, cd2.getDouble(3));
                    sti2.execute();
                    sti2.close();
                    SQLiteStatement std2 = Env.db.compileStatement(
                            "DELETE FROM scoperti WHERE sctipo = 'B' AND scsezdoc=? AND scdatadoc=? AND scnumdoc=?");
                    std2.clearBindings();
                    std2.bindString(1, cd.getString(0));
                    std2.bindString(2, cd.getString(1));
                    std2.bindLong(3, cd.getInt(2));
                    std2.execute();
                    std2.close();
                }
                cd2.close();
            }
            cd.close();
        }
    }

    private void aggiornaTotale() {
        double totsosp = 0;
        for (int i = 0; i < vscop.size(); i++) {
            Record riga = vscop.get(i);
            if (riga.leggiIntero("calctot") == 1) {
                if (riga.leggiStringa("tipoop").equals("V"))
                    totsosp += OpValute.arrotondaMat(riga.leggiDouble("residuoscad") - riga.leggiDouble("saldo"), 2);
                else
                    totsosp -= OpValute.arrotondaMat(riga.leggiDouble("residuoscad") - riga.leggiDouble("saldo"), 2);
            }
        }
        tvtotscop.setText(Formattazione.formValuta(totsosp, 12, 2, Formattazione.SEGNO_DX));
    }

    private void stampaEstrattoConto(Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            ArrayList<Record> vrighe = new ArrayList();
            double totscop = 0;
            String[] pars = new String[1];
            pars[0] = codric;
            Cursor c = Env.db.rawQuery(
                    "SELECT sctipo,scsezdoc,scdatadoc,scnumdoc,sctipoop,scnote,scclicod,scdestcod," +
                            "scdatascad,sctiposcad,scimportoscad,scresiduoscad,scscadid,scrateizzata FROM scoperti WHERE scclicod=? " +
                            " AND scresiduoscad > 0 ORDER BY scdatadoc DESC,scnumdoc ASC", pars);
            while (c.moveToNext()) {
                Boolean scopok = true;
                // se bolla sospesa e presente fattura di fine mese, non stampa la bolla
                if (c.getString(0).equals("B")) {
                    int anno = Formattazione.estraiIntero(c.getString(2).substring(0, 4));
                    int mese = Formattazione.estraiIntero(c.getString(2).substring(5, 7));
                    mese++;
                    if (mese > 12) {
                        anno++;
                        mese = 1;
                    }
                    Data dt = new Data(1, mese, anno);
                    dt.decrementaGiorni(1);
                    String dtf = dt.formatta(Data.AAAA_MM_GG, "-");
                    String[] parsf = new String[2];
                    parsf[0] = codric;
                    parsf[1] = dtf;
                    Cursor cf = Env.db.rawQuery(
                            "SELECT * FROM scoperti WHERE scclicod=? AND scresiduoscad > 0 AND sctipo = 'F' AND scdatadoc=?", parsf);
                    if (cf.moveToFirst())
                        scopok = false;
                    cf.close();
                }
                if (scopok) {
                    Record rx = new Record();
                    String linea = "";
                    if (c.getString(0).equals("F"))
                        rx.insStringa("tipo", "FATT.");
                    else if (c.getString(0).equals("B"))
                        rx.insStringa("tipo", "DDT");
                    else if (c.getString(0).equals("C"))
                        rx.insStringa("tipo", "C.Gen.");
                    else if (c.getString(0).equals("N"))
                        rx.insStringa("tipo", "N.Cr.");
                    if (c.getInt(13) == 1)
                        rx.insStringa("rifdoc", "RATEIZZAZIONE");
                    else if (c.getInt(3) > 0)
                        rx.insStringa("rifdoc", c.getString(1) + "/" + c.getInt(3) + " del " + (new Data(c.getString(2), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/"));
                    else
                        rx.insStringa("rifdoc", "");
                    if (!c.getString(8).equals("0000-00-00") && !c.getString(8).equals(""))
                        rx.insStringa("datascad", (new Data(c.getString(8), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/"));
                    else
                        rx.insStringa("datascad", "");
                    double imp = c.getDouble(11);
                    if (!c.getString(4).equals("V"))
                        imp = -imp;
                    rx.insStringa("importo", Formattazione.formValuta(imp, 12, 2, Formattazione.SEGNO_DX));
                    vrighe.add(rx);
                    totscop += imp;
                }
            }
            c.close();

            int nrigheform = 24 + vrighe.size() + 4;
            if (Env.stampasedesec)
                nrigheform += 4;
            File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
            if (flogo.exists())
                nrigheform += Env.nrighelogo;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheform * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);

            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante("ESTRATTO CONTO", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "ESTRATTO CONTO", lf, 24);
            posy += 30;
            if (flogo.exists()) {
                try {
                    Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                    Env.cpclPrinter.printBitmap(bm, 0, posy);
                } catch (Exception elogo) {
                    elogo.printStackTrace();
                }
                posy += 30 * Env.nrighelogo;
            }
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga6 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga7 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, "CLIENTE:", lf, 24);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Nome: " + cnome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Indirizzo: " + cind, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Partita IVA: " + civa, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, "MOVIMENTI:", lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " TIPO  DOCUMENTO               DATA SCAD.    IMPORTO ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            for (int i = 0; i < vrighe.size(); i++) {
                Record rx = vrighe.get(i);
                stmp = funzStringa.tagliaStringa(rx.leggiStringa("tipo") + " ", 5);
                Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                stmp = funzStringa.tagliaStringa(rx.leggiStringa("rifdoc") + " ", 23);
                Env.cpclPrinter.printAndroidFont(76, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                stmp = funzStringa.tagliaStringa(rx.leggiStringa("datascad") + " ", 8);
                Env.cpclPrinter.printAndroidFont(337, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                stmp = funzStringa.riempiStringa(rx.leggiStringa("importo") + " ", 12, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(434, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                posy += 30;
            }
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, "TOTALE SCOPERTI: " + Formattazione.formValuta(totscop, 12, 2, 2), lf, 24);
            posy += 40;
            Env.cpclPrinter.printAndroidFont(4, posy, Typeface.MONOSPACE, true, false, false, "Firma conducente", lf, 20);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 119, 1);
            posy += 30 * 4;

            Env.cpclPrinter.printForm();

            c.close();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    private void stampaRicevuta(Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;
        try {
            ArrayList<Record> vrighe = new ArrayList();
            double totsaldo = 0;
            for (int i = 0; i < vsld.size(); i++) {
                Record rsld = vsld.get(i);
                Record rx = new Record();
                if (rsld.leggiStringa("tipo").equals("F"))
                    rx.insStringa("tipo", "Fat");
                else if (rsld.leggiStringa("tipo").equals("B"))
                    rx.insStringa("tipo", "DDT");
                else if (rsld.leggiStringa("tipo").equals("C"))
                    rx.insStringa("tipo", "CGe");
                else if (rsld.leggiStringa("tipo").equals("N"))
                    rx.insStringa("tipo", "Ncr");
                if (rsld.leggiIntero("rateizzata") == 1)
                    rx.insStringa("rifdoc", "RATEIZZAZIONE");
                else if (!rsld.leggiStringa("tipo").equals("C"))
                    rx.insStringa("rifdoc", rsld.leggiStringa("sezdoc") + "/" + rsld.leggiIntero("numdoc"));
                else
                    rx.insStringa("rifdoc", "");
                if (!rsld.leggiStringa("tipo").equals("C")) {
                    if (!rsld.leggiStringa("datadoc").equals("0000-00-00") && !rsld.leggiStringa("datadoc").equals(""))
                        rx.insStringa("datadoc", (new Data(rsld.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/"));
                    if (!rsld.leggiStringa("datascad").equals("0000-00-00") && !rsld.leggiStringa("datascad").equals(""))
                        rx.insStringa("datadoc", (new Data(rsld.leggiStringa("datascad"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/"));
                    else
                        rx.insStringa("datadoc", "");
                } else
                    rx.insStringa("datadoc", "");
                double imp = rsld.leggiDouble("residuoscad");
                if (!rsld.leggiStringa("tipoop").equals("V"))
                    imp = -imp;
                rx.insStringa("importo", Formattazione.formatta(imp, "###0.00", 2));
                double residuo = 0;
                Log.v("JAZZTV", "RESIDUOSCAD:" + rsld.leggiDouble("residuoscad"));
                Log.v("JAZZTV", "SALDO:" + rsld.leggiDouble("saldo"));
                if (rsld.leggiStringa("tipo").equals("N"))
                    residuo = OpValute.arrotondaMat(rsld.leggiDouble("residuoscad") - rsld.leggiDouble("saldo"), 2);
                else
                    residuo = OpValute.arrotondaMat(rsld.leggiDouble("residuoscad") - Math.abs(rsld.leggiDouble("saldo")), 2);
                if (!rsld.leggiStringa("tipoop").equals("V"))
                    residuo = -residuo;
                rx.insStringa("residuo", Formattazione.formatta(residuo, "###0.00", 2));
                rx.insStringa("saldo", Formattazione.formatta(rsld.leggiDouble("saldo"), "###0.00", 2));
                if (!rsld.leggiStringa("tipoop").equals("V"))
                    totsaldo -= rsld.leggiDouble("saldo");
                else
                    totsaldo += rsld.leggiDouble("saldo");
                vrighe.add(rx);
            }

            ArrayList<Record> vddtc = new ArrayList();
            Cursor c = Env.db.rawQuery("SELECT dscsezdoc,dscdatadoc,dscnumdoc,dscimporto FROM ddtsospchiusi ORDER BY dscsezdoc,dscdatadoc,dscnumdoc", null);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("sezdoc", c.getString(0));
                rx.insStringa("datadoc", (new Data(c.getString(1), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"));
                rx.insIntero("numdoc", c.getInt(2));
                rx.insStringa("importo", Formattazione.formatta(c.getDouble(3), "#######0.00", 2));
                vddtc.add(rx);
            }
            c.close();

            int nrigheform = 25 + vrighe.size() + 4;
            if (Env.stampasedesec)
                nrigheform += 4;
            File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
            if (flogo.exists())
                nrigheform += Env.nrighelogo;
            if (vddtc.size() > 0)
                nrigheform += vddtc.size() + 3;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheform * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);

            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante("RICEVUTA DI PAGAMENTO", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "RICEVUTA DI PAGAMENTO", lf, 24);
            posy += 30;
            if (flogo.exists()) {
                try {
                    Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                    Env.cpclPrinter.printBitmap(bm, 0, posy);
                } catch (Exception elogo) {
                    elogo.printStackTrace();
                }
                posy += 30 * Env.nrighelogo;
            }
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga6 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga7 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, "CLIENTE:", lf, 24);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Nome: " + cnome + " ", gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Indirizzo: " + cind + " ", gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Partita IVA: " + civa + " ", gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, "INCASSI:", lf, 24);
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " TIPO DOCUMENTO  DATA DOC IMPORTO  RIMANENZA   SALDO ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " TIPO DOCUMENTO     DATA DOC     IMPORTO      RIMANENZA       SALDO ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            for (int i = 0; i < vrighe.size(); i++) {
                Record rx = vrighe.get(i);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("tipo") + " ", 3);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("rifdoc") + " ", 11);
                    Env.cpclPrinter.printAndroidFont(54, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("datadoc") + " ", 8);
                    Env.cpclPrinter.printAndroidFont(185, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("importo") + " ", 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(282, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("residuo") + " ", 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(380, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("saldo") + " ", 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(478, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                } else {
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("tipo") + " ", 3);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("rifdoc") + " ", 14);
                    Env.cpclPrinter.printAndroidFont(54, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("datadoc") + " ", 8);
                    Env.cpclPrinter.printAndroidFont(217, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("importo") + " ", 11, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(315, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("residuo") + " ", 11, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(478, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("saldo") + " ", 11, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(608, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                }
                posy += 30;
            }
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, "TOTALE SALDO: " + Formattazione.formValuta(totsaldo, 12, 2, 2), lf, 24);
            posy += 30;
            posy += 30;
            if (vddtc.size() > 0) {
                Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
                Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, "DDT SOSPESI CHIUSI CON INCASSI FATTURE", lf, 24);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " SEZ.DOC.   N.DOC.      DATA DOC.    IMPORTO         ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                for (int i = 0; i < vrighe.size(); i++) {
                    Record rx = vrighe.get(i);
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("sezdoc") + " ", 10);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa("" + rx.leggiIntero("numdoc") + " ", 8);
                    Env.cpclPrinter.printAndroidFont(130, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("datadoc") + " ", 10);
                    Env.cpclPrinter.printAndroidFont(272, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("importo") + " ", 12, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(402, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    posy += 30;
                }
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
            }
            Env.cpclPrinter.printAndroidFont(4, posy, Typeface.MONOSPACE, true, false, false, "Firma conducente", lf, 20);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 119, 1);
            posy += 30 * 4;

            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    private void stampaRicevutaGenerica(Context context, String descr, double importo) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;
        try {
            int nrigheform = 25 + 2;
            if (Env.stampasedesec)
                nrigheform += 4;
            File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
            if (flogo.exists())
                nrigheform += Env.nrighelogo;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheform * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);

            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante("RICEVUTA DI PAGAMENTO GENERICO", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "RICEVUTA DI PAGAMENTO GENERICO", lf, 24);
            posy += 30;
            if (flogo.exists()) {
                try {
                    Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                    Env.cpclPrinter.printBitmap(bm, 0, posy);
                } catch (Exception elogo) {
                    elogo.printStackTrace();
                }
                posy += 30 * Env.nrighelogo;
            }
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga6 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga7 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2), gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0), gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1), gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, "DESCRIZIONE:", lf, 24);
            posy += 30;
            String riga1 = "", riga2 = "", riga3 = "", riga4 = "", riga5 = "", riga6 = "";
            String dtmp = descr;
            if (dtmp.length() <= 53) {
                riga1 = funzStringa.riempiStringa(dtmp, 53, funzStringa.SX, ' ');
                dtmp = "";
            } else {
                riga1 = dtmp.substring(0, 53);
                dtmp = dtmp.substring(53);
            }
            if (dtmp.length() <= 53) {
                riga2 = funzStringa.riempiStringa(dtmp, 53, funzStringa.SX, ' ');
                dtmp = "";
            } else {
                riga2 = dtmp.substring(0, 53);
                dtmp = dtmp.substring(53);
            }
            if (dtmp.length() <= 53) {
                riga3 = funzStringa.riempiStringa(dtmp, 53, funzStringa.SX, ' ');
                dtmp = "";
            } else {
                riga3 = dtmp.substring(0, 53);
                dtmp = dtmp.substring(53);
            }
            if (dtmp.length() <= 53) {
                riga4 = funzStringa.riempiStringa(dtmp, 53, funzStringa.SX, ' ');
                dtmp = "";
            } else {
                riga4 = dtmp.substring(0, 53);
                dtmp = dtmp.substring(53);
            }
            if (dtmp.length() <= 53) {
                riga5 = funzStringa.riempiStringa(dtmp, 53, funzStringa.SX, ' ');
                dtmp = "";
            } else {
                riga5 = dtmp.substring(0, 53);
                dtmp = dtmp.substring(53);
            }
            if (dtmp.length() <= 53) {
                riga6 = funzStringa.riempiStringa(dtmp, 53, funzStringa.SX, ' ');
                dtmp = "";
            } else {
                riga6 = dtmp.substring(0, 53);
                dtmp = dtmp.substring(53);
            }
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga1, lf, 18);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga2, lf, 18);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga3, lf, 18);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga4, lf, 18);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga5, lf, 18);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga6, lf, 18);
            posy += 30;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, "IMPORTO: " + Formattazione.formValuta(importo, 12, 2, 2), lf, 24);
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printAndroidFont(4, posy, Typeface.MONOSPACE, true, false, false, "Firma conducente", lf, 20);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 119, 1);
            posy += 30 * 4;

            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    public void aggiornaTotaleIncassoMultiplo() {
        totale = 0;
        for (Record rs : vscop) {
            if (rs.esisteCampo("sel")) {
                if (rs.leggiStringa("tipoop").equals("V"))
                    totale += rs.leggiDouble("residuoscad");
                else
                    totale -= rs.leggiDouble("residuoscad");
            }
        }
        totale = OpValute.arrotondaMat(totale, 2);
        tvtotincmult.setText(Formattazione.formValuta(totale, 12, 2, 1));
    }
}
