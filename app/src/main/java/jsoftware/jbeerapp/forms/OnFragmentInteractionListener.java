package jsoftware.jbeerapp.forms;

import android.net.Uri;

public interface OnFragmentInteractionListener {

    void onFragmentInteraction(Uri uri);
}
