package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Record;

public class FormDettaglioContatto extends AppCompatActivity {

    private TextView tvnominativo;
    private TextView tvemail;
    private TextView tvindiaz;
    private TextView tvnomeaz;
    private TextView tvtel;
    private TextView tvcell;
    private TextView tvsitoaz;
    private String ccognome = "";
    private String cnome = "";
    private String ctel = "";
    private String cmobile = "";
    private String cemail = "";
    private String caziendaindir = "";
    private String caziendanumciv = "";
    private String caziendaloc = "";
    private String caziendacap = "";
    private String caziendaragsoc = "";
    private ImageButton butdelete;
    private ImageButton butcambia;
    private ImageButton butshare;
    private ImageButton butmodif;
    private ImageButton butcell;
    private ImageButton butfisso;
    private ImageButton butemail;
    public FragmentRubricaContatti frcon;
    private TextView tvcomune;
    private ArrayList<Record> vrubrica;
    private String ccodice;
    private String ceventocont;
    private String ctitolo;
    private String cmansione;
    private String cfax;
    private int ctipologia;
    private int ctipo;
    private String caziendaprov;
    private String caziendanaz;
    private String caziendaemail;
    private String caziendasitoweb;
    private TextView tvnote;
    private String cnote;
    private boolean flagmodifica = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_dettaglio_contatto);

        frcon = new FragmentRubricaContatti();
        //frub = new FormRubrica();

        tvnominativo = findViewById(R.id.detcont_nome);
        tvemail = findViewById(R.id.detcont_email);
        tvindiaz = findViewById(R.id.detcont_indAz);
        tvnomeaz = findViewById(R.id.detcont_nomeAz);
        tvcomune = findViewById(R.id.detcont_comuneAz);
        tvsitoaz = findViewById(R.id.detcont_sitoAz);
        tvtel = findViewById(R.id.detcont_tel);
        tvcell = findViewById(R.id.detcont_cell);
        tvnote = findViewById(R.id.detcont_note);
        butmodif = findViewById(R.id.detcont_bmod);
        butshare = findViewById(R.id.detcont_bshare);
        butdelete = findViewById(R.id.detcont_bdel);
        butcambia = findViewById(R.id.detcont_bcambia);
        butcell = findViewById(R.id.detcont_bcallcel);
        butfisso = findViewById(R.id.detcont_bcalltel);
        butemail = findViewById(R.id.detcont_bemail);


        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cognome"))
            ccognome = getIntent().getExtras().getString("cognome");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("nome"))
            cnome = getIntent().getExtras().getString("nome");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("ragsoc"))
            caziendaragsoc = getIntent().getExtras().getString("ragsoc");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("codice"))
            ccodice = getIntent().getExtras().getString("codice");

        leggiContatto();
        visualizzaTextView();


        butcell.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + tvcell.getText().toString()));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDettaglioContatto.this);
                    builder.setMessage("Errore su apertura telefono")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
            }
        });

        butfisso.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + tvtel.getText().toString()));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDettaglioContatto.this);
                    builder.setMessage("Errore su apertura telefono")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
            }
        });

        butemail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("mailto:?subject=" + tvemail.getText().toString()));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDettaglioContatto.this);
                    builder.setMessage("Errore su apertura client email")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
            }
        });

        butmodif.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneModifica();
            }
        });

        butcambia.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //to prevent double click
                butcambia.setEnabled(false);
                azioneTrasformaContatto();
            }
        });

        butdelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneElimina();
            }
        });


    }

    @Override
    public void onBackPressed() {
        if (flagmodifica) {
            Uri ris = Uri.parse("content://modificacontatto/OK");
            Intent result = new Intent(Intent.ACTION_PICK, ris);
            setResult(RESULT_CANCELED, result);
            finish();

        } else {
            Uri ris = Uri.parse("content://modificacontatto/ANNULLA");
            Intent result = new Intent(Intent.ACTION_PICK, ris);
            setResult(RESULT_CANCELED, result);
            finish();
        }
/*        Intent refresh = new Intent(this, FormRubrica.class);
        startActivity(refresh);//Start the same Activity
        finish();*/
    }

    private void azioneModifica() {
        Intent i = new Intent(FormDettaglioContatto.this.getApplicationContext(), FormNuovoContatto.class);
        Bundle mBundle = new Bundle();
        mBundle.putString("ccognome", ccognome);
        mBundle.putString("ccodice", ccodice);
        mBundle.putString("cnome", cnome);
        mBundle.putString("ctel", ctel);
        mBundle.putString("cmobile", cmobile);
        mBundle.putString("cemail", cemail);
        mBundle.putString("caziendaindir", caziendaindir);
        mBundle.putString("caziendanumciv", caziendanumciv);
        mBundle.putString("caziendaloc", caziendaloc);
        mBundle.putString("caziendacap", caziendacap);
        mBundle.putString("caziendaragsoc", caziendaragsoc);
        mBundle.putString("caziendaprov", caziendaprov);
        mBundle.putString("cnote", cnote);
        mBundle.putBoolean("opmodifica", true);
        mBundle.putBoolean("optrasforma", false);
        mBundle.putInt("ctipo", ctipo);
        i.putExtras(mBundle);
        FormDettaglioContatto.this.startActivityForResult(i, 1);
    }

    private void azioneTrasformaContatto() {
        Intent i = new Intent(FormDettaglioContatto.this.getApplicationContext(), FormNuovoCliente.class);
        Bundle mBundle = new Bundle();
        mBundle.putString("clicognomepri", ccognome);
        mBundle.putString("clinomepri", cnome);
        mBundle.putString("clinumtel", ctel);
        mBundle.putString("clinumcell", cmobile);
        mBundle.putString("cliemail1", cemail);
        mBundle.putString("cliindir", caziendaindir);
        mBundle.putString("clinumciv", caziendanumciv);
        mBundle.putString("cliloc", caziendaloc);
        mBundle.putString("clicap", caziendacap);
        mBundle.putString("clinome", caziendaragsoc);
        mBundle.putString("cliprov", caziendaprov);
        mBundle.putString("cnote", cnote);
        mBundle.putBoolean("opmodifica", true);
        mBundle.putBoolean("optrasforma", true);
        mBundle.putString("clicodpag", "");
        i.putExtras(mBundle);
        FormDettaglioContatto.this.startActivityForResult(i, 2);
    }

    private void azioneElimina() {


        AlertDialog.Builder builder = new AlertDialog.Builder(
                FormDettaglioContatto.this);
        builder.setMessage("Confermi Eliminazione Contatto?")
                .setPositiveButton("SI",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                SQLiteStatement stmt = Env.db.compileStatement("DELETE FROM contatti WHERE ((ccognome=? AND cnome=?) OR caziendaragsoc=?)  AND ccodice=?");
                                stmt.clearBindings();
                                stmt.bindString(1, ccognome);
                                stmt.bindString(2, cnome);
                                stmt.bindString(3, caziendaragsoc);
                                stmt.bindString(4, ccodice);
                                stmt.execute();
                                stmt.close();
                                Toast toast = Toast.makeText(getApplicationContext(), "Contatto eliminato",
                                        Toast.LENGTH_LONG);
                                toast.show();
                                finish();
                                Intent inent = new Intent(FormDettaglioContatto.this, FormRubrica.class);

                                // calling an activity using <intent-filter> action name
                                //  Intent inent = new Intent("com.hmkcode.android.ANOTHER_ACTIVITY");

                                startActivity(inent);


                            }
                        })
                .setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                            }
                        });
        AlertDialog ad = builder.create();
        ad.show();


    }

    private void leggiContatto() {
        cnome = cnome.replaceAll("'", "\'");
        ccognome = ccognome.replaceAll("'", "\'");
        vrubrica = new ArrayList<Record>();
        String qry = "SELECT ccognome,cnome,ccodice,ceventocont,ctitolo,cmansione,cemail,ctel,cmobile," +
                "cfax,ctipologia,ctipo,caziendaragsoc,caziendaindir,caziendanumciv,caziendaloc, caziendacap" +
                ",caziendaprov,caziendanaz,caziendaemail,caziendasitoweb,cnote FROM contatti  WHERE ((cnome = '" + cnome + "' AND ccognome = '" + ccognome + "') OR caziendaragsoc='" + caziendaragsoc + "') AND ccodice = '" + ccodice + "' ";
        Cursor cursor = Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            Record r = new Record();
            ccognome = cursor.getString(0);
            cnome = cursor.getString(1);
            ccodice = cursor.getString(2);
            ceventocont = cursor.getString(3);
            ctitolo = cursor.getString(4);
            cmansione = cursor.getString(5);
            cemail = cursor.getString(6);
            ctel = cursor.getString(7);
            cmobile = cursor.getString(8);
            cfax = cursor.getString(9);
            ctipologia = cursor.getInt(10);
            ctipo = cursor.getInt(11);
            caziendaragsoc = cursor.getString(12);
            caziendaindir = cursor.getString(13);
            caziendanumciv = cursor.getString(14);
            caziendaloc = cursor.getString(15);
            caziendacap = cursor.getString(16);
            caziendaprov = cursor.getString(17);
            caziendanaz = cursor.getString(18);
            caziendaemail = cursor.getString(19);
            caziendasitoweb = cursor.getString(20);
            cnote = cursor.getString(21);
            r.insStringa("tipo", "cliente");
            vrubrica.add(r);
        }
        cursor.close();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                // Modifica contatto
                if (resultCode == Activity.RESULT_OK) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Contatto modificato",
                            Toast.LENGTH_LONG);
                    toast.show();
                    ArrayList<HashMap> vris = (ArrayList<HashMap>) data.getExtras().get("vris");
                    for (int i = 0; i < vris.size(); i++) {
                        HashMap riga = vris.get(i);
                        ccognome = (String) riga.get("ccognome");
                        cnome = (String) riga.get("cnome");
                        ctel = (String) riga.get("ctel");
                        cmobile = (String) riga.get("cmobile");
                        cemail = (String) riga.get("cemail");
                        caziendaindir = (String) riga.get("caziendaindir");
                        caziendanumciv = (String) riga.get("caziendanumciv");
                        caziendaloc = (String) riga.get("caziendaloc");
                        caziendacap = (String) riga.get("caziendacap");
                        caziendaragsoc = (String) riga.get("caziendaragsoc");
                        cnote = (String) riga.get("cnote");
                        ctipo = (int) riga.get("ctipo");
                        flagmodifica = true;
                        visualizzaTextView();

                    }
                }
                break;
            }
            case (2): {
                //contatto trasformato in cliente
                butcambia.setEnabled(true);
                if (resultCode == Activity.RESULT_OK) {
                    String qry = "UPDATE contatti SET ctrasformatocliente='S' WHERE ccodice= '" + ccodice + "'";
                    SQLiteStatement stup = Env.db.compileStatement(qry);
                    stup.execute();
                    stup.close();
                    Toast toast = Toast.makeText(getApplicationContext(), "Contatto trasformato in cliente",
                            Toast.LENGTH_LONG);
                    toast.show();
                    ris = data.getData();
                    //clicognomepri + " " + clinomepri
                    String clicodice = (String) data.getExtras().get("clicodice");
                    String clinome = (String) data.getExtras().get("clinome");
                    String clicognomepri = (String) data.getExtras().get("clicognomepri");
                    String clinomepri = (String) data.getExtras().get("clinomepri");

                    Intent i = new Intent(FormDettaglioContatto.this.getApplicationContext(), FormDettaglioCliente.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("clicodice", clicodice);
                    mBundle.putString("clinome", clinome);
                    mBundle.putString("clicognomepri", clicognomepri);
                    mBundle.putString("clinomepri", clinomepri);
                    mBundle.putBoolean("trasformato", true);
                    i.putExtras(mBundle);
                    FormDettaglioContatto.this.startActivityForResult(i, 3);
                }
                break;
            }
            case (3): {
                if (resultCode == Activity.RESULT_CANCELED) {
                    ris = Uri.parse("content://modificacontatto/OK");
                    Intent result = new Intent(Intent.ACTION_PICK, ris);
                    //setResult(RESULT_CANCELED, result);
                    setResult(RESULT_CANCELED, result);
                    finish();
                }
                break;
            }

        }

    }

    private void visualizzaTextView() {

        if (!ccognome.equals("")) {
            tvnominativo.setText(ccognome + " " + cnome);
        } else {
            tvnominativo.setText(caziendaragsoc);
        }
        tvemail.setText(cemail);
        if (caziendaindir.equals("")) {
            tvindiaz.setText("");
        } else {
            tvindiaz.setText(caziendaindir + ", " + caziendanumciv);
        }
        if (!caziendacap.equals("") || !caziendaloc.equals("")) {
            tvcomune.setText(caziendacap + ", " + caziendaloc + " (" + caziendaprov.toUpperCase() + ")");
        } else {
            tvcomune.setText("");
        }
        tvnomeaz.setText(caziendaragsoc);
        tvsitoaz.setText("");
        tvtel.setText(ctel);
        tvcell.setText(cmobile);

        if (cnote != null && !cnote.equals("")) {
            tvnote.setText(cnote);
        } else {
            tvnote.setText("");
        }

    }
}
