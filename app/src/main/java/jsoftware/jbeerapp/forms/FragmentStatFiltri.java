package jsoftware.jbeerapp.forms;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Formattazione;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentStatFiltri.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentStatFiltri#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentStatFiltri extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Spinner spperiodo = null;
    private Button briep;
    private Button bart;
    private Button bcli;
    private Button bclassi;
    private ImageButton bdatai;
    private ImageButton bdataf;
    //private TextView tvtitolo;
    private TextView tvdatai;
    private TextView tvdataf;
    private String datainizio = "", datafine = "";


    private OnFragmentInteractionListener mListener;

    public FragmentStatFiltri() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentStatFiltri.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentStatFiltri newInstance(String param1, String param2) {
        FragmentStatFiltri fragment = new FragmentStatFiltri();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_stat_filtri, container, false);

    }

    public void sceltaRiep() {
        if (mListener != null) {
            //tvtitolo.setText("Riepilogo");
            mListener.onFragmentInteraction("STATFILTRI|RIEP|" + datainizio + "|" + datafine);
        }
    }

    public void sceltaArticoli() {
        if (mListener != null) {
            //tvtitolo.setText("Dettaglio Articoli");
            mListener.onFragmentInteraction("STATFILTRI|ART|" + datainizio + "|" + datafine);
        }
    }

    public void sceltaClassi() {
        if (mListener != null) {
            //tvtitolo.setText("Dettaglio Categorie merce");
            mListener.onFragmentInteraction("STATFILTRI|CLASSI|" + datainizio + "|" + datafine);
        }
    }

    public void sceltaClienti() {
        if (mListener != null) {
            //tvtitolo.setText("Dettaglio Clienti");
            mListener.onFragmentInteraction("STATFILTRI|CLI|" + datainizio + "|" + datafine);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String azione);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spperiodo = view.findViewById(R.id.statfiltri_periodo);
        briep = view.findViewById(R.id.statfiltri_buttonRiep);
        bart = view.findViewById(R.id.statfiltri_buttonArt);
        bcli = view.findViewById(R.id.statfiltri_buttonClienti);
        bclassi = view.findViewById(R.id.statfiltri_buttonClasse);
        //tvtitolo = (TextView) view.findViewById(R.id.statfiltri_titolo);
        bdatai = view.findViewById(R.id.statfiltri_buttonDataInizio);
        bdataf = view.findViewById(R.id.statfiltri_buttonDataFine);
        tvdatai = view.findViewById(R.id.statfiltri_datainizio);
        tvdataf = view.findViewById(R.id.statfiltri_datafine);

        ArrayList<String> vp = new ArrayList();
        vp.add("Questa settimana");
        vp.add("Settimana precedente");
        vp.add("Questo mese");
        vp.add("Mese precedente");
        SpinnerAdapter ltdadapter = new SpinnerAdapter(this.getActivity().getApplicationContext(), R.layout.spinner_item, vp);
        spperiodo.setAdapter(ltdadapter);

        briep.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sceltaRiep();
            }
        });

        bart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sceltaArticoli();
            }
        });

        bcli.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sceltaClienti();
            }
        });

        bclassi.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sceltaClassi();
            }
        });

        bdatai.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DatePickerForm dpf = new DatePickerForm();
                dpf.activity = FragmentStatFiltri.this.getActivity();
                dpf.callback = new DatePickerForm.DPCallback() {
                    @Override
                    public void onDialogResult(int year, int monthOfYear, int dayOfMonth) {
                        Data dt = new Data(dayOfMonth, monthOfYear + 1, year);
                        //tvdatai.setText(dt.formatta(Data.GG_MM_AAAA, "/") + "\n" + dt.giornoSettimana());
                        tvdatai.setText(dt.formatta(Data.GG_MM_AAAA, "/"));
                        datainizio = dt.formatta(Data.AAAA_MM_GG, "-");
                    }
                };
                dpf.show(FragmentStatFiltri.this.getActivity().getSupportFragmentManager(), "datePicker");
            }
        });

        bdataf.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DatePickerForm dpf = new DatePickerForm();
                dpf.activity = FragmentStatFiltri.this.getActivity();
                dpf.callback = new DatePickerForm.DPCallback() {
                    @Override
                    public void onDialogResult(int year, int monthOfYear, int dayOfMonth) {
                        Data dt = new Data(dayOfMonth, monthOfYear + 1, year);
                        tvdataf.setText(dt.formatta(Data.GG_MM_AAAA, "/"));
                        //tvdataf.setText(dt.formatta(Data.GG_MM_AAAA, "/") + "\n" + dt.giornoSettimana());
                        datafine = dt.formatta(Data.AAAA_MM_GG, "-");
                    }
                };
                dpf.show(FragmentStatFiltri.this.getActivity().getSupportFragmentManager(), "datePicker");
            }
        });

        spperiodo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // aggiorna date
                if (arg2 == 0) {
                    Data dt = new Data();
                    Data dtf = new Data(dt.giorno(), dt.mese(), dt.anno());
                    datafine = dtf.formatta(Data.AAAA_MM_GG, "-");
                    while (!dt.giornoSettimana().toLowerCase().startsWith("lun"))
                        dt.decrementaGiorni(1);
                    datainizio = dt.formatta(Data.AAAA_MM_GG, "-");
                } else if (arg2 == 1) {
                    Data dt = new Data();
                    while (!dt.giornoSettimana().toLowerCase().startsWith("dom"))
                        dt.decrementaGiorni(1);
                    datafine = dt.formatta(Data.AAAA_MM_GG, "-");
                    while (!dt.giornoSettimana().toLowerCase().startsWith("lun"))
                        dt.decrementaGiorni(1);
                    datainizio = dt.formatta(Data.AAAA_MM_GG, "-");
                } else if (arg2 == 2) {
                    Data dt = new Data();
                    Data dtf = new Data(dt.giorno(), dt.mese(), dt.anno());
                    datafine = dtf.formatta(Data.AAAA_MM_GG, "-");
                    datainizio = (new Data(1, dt.mese(), dt.anno())).formatta(Data.AAAA_MM_GG, "-");
                } else if (arg2 == 3) {
                    Data dt = new Data();
                    int m = dt.mese();
                    int a = dt.anno();
                    m--;
                    if (m == 0) {
                        m = 12;
                        a--;
                    }
                    datafine = a + "-" + Formattazione.formIntero(m, 2, "0") + "-31";
                    datainizio = a + "-" + Formattazione.formIntero(m, 2, "0") + "-01";
                }
                tvdatai.setText((new Data(datainizio, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"));
                tvdataf.setText((new Data(datafine, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }
}
