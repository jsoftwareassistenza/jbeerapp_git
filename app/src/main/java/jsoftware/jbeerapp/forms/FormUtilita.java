package jsoftware.jbeerapp.forms;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.JBeerAppDatabaseHelper;
import jsoftware.jbeerapp.env.Record;

import static jsoftware.jbeerapp.env.Env.db;


public class FormUtilita extends AppCompatActivity {


    private Button bbackup;
    private Button beseguibackup;
    private Button bcancdb;
    private Button bazzeraanno;
    private Button bdomandasegreta;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_utilita);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        bcancdb = findViewById(R.id.util_buttonCancDb);
        bbackup = findViewById(R.id.util_buttonRipristinoDb);
        beseguibackup = findViewById(R.id.util_buttonBackupDb);
        bazzeraanno = findViewById(R.id.util_buttonAzzeraAnno);
        bdomandasegreta = findViewById(R.id.util_buttonAggiornaDomanda);

        bbackup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), FormElencoBackupOnline.class);
                startActivity(i);
            }
        });

        beseguibackup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(FormUtilita.this.getApplicationContext(), FormBackupOnline.class);
                FormUtilita.this.startActivity(i);
            }
        });

        bcancdb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(FormUtilita.this.getApplicationContext(), FormPasswordOpe.class);
                Bundle mBundle = new Bundle();
                mBundle.putString("DESCR", "reinizializzazione terminale");
                mBundle.putString("PWD", "xjbeergox");
                i.putExtras(mBundle);
                FormUtilita.this.startActivityForResult(i, 1);
            }
        });

        bdomandasegreta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i = new Intent(FormUtilita.this.getApplicationContext(), FormRipristinoPin.class);
                startActivity(i);
            }
        });

        bazzeraanno.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormUtilita.this);
                builder.setMessage("Attenzione: l'operazione riporta ad 1 i numeratori dei movimenti per la ripartenza con il nuovo anno di lavoro, continuare?")
                        .setPositiveButton("SI",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        //azzera numeratori
                                        db.execSQL("UPDATE datiterm SET numbollaval=1,numbollaqta=1,numfattura=1,numcorrisp=1,numddtreso=1,numddtcarico=1," +
                                                "numscaricosede=1,numtrasfamezzo=1,numtrasfdamezzo=1,numordinesede=1,numordinecli=1,numinv=1");
                                        Env.numbollaval = 1;
                                        Env.numbollaqta = 1;
                                        Env.numfattura = 1;
                                        Env.numcorrisp = 1;
                                        Env.numddtreso = 1;
                                        Env.numddtcarico = 1;
                                        Env.numscaricosede = 1;
                                        Env.numtrasfamezzo = 1;
                                        Env.numtrasfdamezzo = 1;
                                        Env.numordinesede = 1;
                                        Env.numordinecli = 1;
                                        Env.numinv = 1;
                                        Toast toast = Toast.makeText(getApplicationContext(), "Azzeramento anno eseguito",
                                                Toast.LENGTH_SHORT);
                                        toast.show();
                                    }
                                })
                        .setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                if (resultCode == Activity.RESULT_OK) {
                    // ritorno password canc.db
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormUtilita.this);
                    builder.setMessage("Attenzione: confermando si reinizializzano i dati del terminale e l'operazione è irreversibile, continuare?")
                            .setPositiveButton("SI",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            String pedcod = "";
                                            String user = "";
                                            String pwd = "";
                                            String serverjcloud = "";
                                            String activationcode = "";
                                            String pin = "";
                                            String versionedemo = "";
                                            Cursor cursor = db.rawQuery(
                                                    "SELECT pedcod,user,pwd,serverjcloud,activationcode,pin,versionedemo FROM datiterm",
                                                    null);
                                            if (cursor.moveToFirst()) {
                                                pedcod = cursor.getString(0);
                                                user = cursor.getString(1);
                                                pwd = cursor.getString(2);
                                                serverjcloud = cursor.getString(3);
                                                activationcode = cursor.getString(4);
                                                pin = cursor.getString(5);
                                                versionedemo = cursor.getString(6);
                                            }

                                            cursor.close();


                                            File fdata = Environment.getDataDirectory();
                                            String dbFilePath = "/data/" + getApplicationContext().getPackageName() + "/databases/jbeerappbd.db";
                                            File fcurrentDB = new File(fdata, dbFilePath);
                                            fcurrentDB.delete();

                                            try {
                                                db.execSQL("CREATE TABLE datitermreiniz ( " +
                                                        "numterm INTEGER DEFAULT 0," +
                                                        "user VARCHAR(20) DEFAULT ''," +
                                                        "pwd VARCHAR(20) DEFAULT ''," +
                                                        "activationcode VARCHAR(20) DEFAULT ''," +
                                                        "pin VARCHAR(255) DEFAULT ''," +
                                                        "serverjcloud VARCHAR(255) DEFAULT ''," +
                                                        "versionedemo VARCHAR(1) DEFAULT 'N'," +
                                                        "pedcod VARCHAR(10) DEFAULT ''" +
                                                        ")");
                                            } catch (Exception edb) {
                                                //edb.printStackTrace();
                                            }
                                            JBeerAppDatabaseHelper dbh = new JBeerAppDatabaseHelper(getApplicationContext(), "", null, Env.versionDatabase);
                                            Env.db = dbh.getWritableDatabase();
                                            //db.getWritableDatabase();
                                            String sql = "INSERT INTO datitermreiniz (pedcod,user,pwd,serverjcloud,activationcode,pin,versionedemo) VALUES (?,?,?,?,?,?,?)";
                                            SQLiteStatement stmt = db
                                                    .compileStatement(sql);
                                            stmt.clearBindings();
                                            stmt.bindString(1, pedcod);
                                            stmt.bindString(2, user);
                                            stmt.bindString(3, pwd);
                                            stmt.bindString(4, serverjcloud);
                                            stmt.bindString(5, activationcode);
                                            stmt.bindString(6, pin);
                                            stmt.bindString(7, versionedemo);

                                            stmt.execute();
                                            stmt.close();

                                            AlertDialog.Builder adb = new AlertDialog.Builder(
                                                    FormUtilita.this);
                                            adb.setMessage("Reinizializzazione effettuata, riavviare l'app")
                                                    .setPositiveButton("Ok",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog,
                                                                                    int id) {
                                                                    return;
                                                                }
                                                            });
                                            AlertDialog adx = adb.create();
                                            adx.setCancelable(false);
                                            adx.show();
                                            return;
                                        }
                                    })
                            .setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
                break;
            }
        }
    }

}
