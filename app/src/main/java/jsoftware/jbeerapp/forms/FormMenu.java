package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.SpinnerAdapterMenu;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;

public class FormMenu extends AppCompatActivity {
    private ImageButton bScambiodati;
    private ArrayList<String> veventi = new ArrayList();
    private ArrayList<String> veventiCod = new ArrayList();
    private SpinnerAdapterMenu veventiadapter;
    private Spinner speventi;
    private ImageButton bAppunti;
    private View alert;
    private TextView countDown;
    private String dataScadenza = "";
    private String demo = "";
    private View buttonContatti;
    private View buttonCatologo;
    private View buttonShop;
    private View buttonStatistiche;
    private View buttonSblocco;
    private View llcountdown;
    //private Button buttonContatti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_menu);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        buttonContatti = findViewById(R.id.main_bRubrica);
        buttonCatologo = findViewById(R.id.main_bCatalogo);
        buttonShop = findViewById(R.id.main_bShop);
        buttonStatistiche = findViewById(R.id.main_bStatistiche);
        buttonSblocco = findViewById(R.id.button_sblocco);
        countDown = findViewById(R.id.tv_countDown);
        llcountdown = findViewById(R.id.main_llcountdown);
        bScambiodati = findViewById(R.id.button_ScambioDati);
        bAppunti = findViewById(R.id.button_appunti);
        alert = findViewById(R.id.alert_menu);

        veventi = new ArrayList();
        veventi.add("Nessun Evento");
        veventiCod = new ArrayList();
        veventiCod.add("");
        //Env.depcod = "SEDE";
        alert.setVisibility(View.INVISIBLE);
        //alert.setEnabled(false);
        caricaeventi();
        //veventiadapter = new SpinnerAdapter(this, R.layout.spinner_item_white, veventi);
        veventiadapter = new SpinnerAdapterMenu(this, R.layout.spinner_item_background, veventi);
        speventi = findViewById(R.id.spinner_eventi);
        speventi.setAdapter(veventiadapter);

        ultimoeventoselezionato();
        controlloAttivazione();
        numeratoriDaAzzerare();

        controlloCarichi concar = new controlloCarichi();
        concar.start();


        //View footer = getLayoutInflater().inflate(R.layout.footer_form_menu, null);
        //lvlistaart.addFooterView(footer_form_menu);


        buttonContatti.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneRubrica();
            }
        });

        bScambiodati.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneTrasmissione();
            }
        });

        bAppunti.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneAppunti();
            }
        });

        buttonSblocco.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneSbloccaVersione();
            }
        });

        buttonCatologo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneCatalogo();
            }
        });

        buttonShop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneShop();
            }
        });

        buttonStatistiche.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneStatistiche();
            }
        });

        speventi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (speventi.getSelectedItemPosition() > 0) {
                    Env.depeventocod = veventiCod.get(speventi.getSelectedItemPosition());
                    ((TextView) speventi.getSelectedView()).setTextColor(getResources().getColor(R.color.colorWhite));
                    FunzioniJBeerApp.impostaProprieta("eventojbeer", Env.depeventocod);
                } else {
                    Env.depeventocod = "";
                    FunzioniJBeerApp.impostaProprieta("eventojbeer", "");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


    }

    public void caricaeventi() {

        String qry = "SELECT depcod,depdescr,depubic1,depeventi,depdatai,depdataf,depubic2 FROM depositi WHERE depeventi = 1";
        Cursor cursor = Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            veventiCod.add(cursor.getString(0));
            veventi.add(cursor.getString(1));

        }
        cursor.close();


    }

    private void azioneRubrica() {

        if (FunzioniJBeerApp.leggiMessaggi().size() > 0) {
            Intent i = new Intent(getApplicationContext(), FormMessaggi.class);
            jsoftware.jbeerapp.forms.FormMenu.this.startActivity(i);
        } else {
            Intent i = new Intent(getApplicationContext(), FormRubrica.class);
            //Ambiente.fd.ddtprom = false;
            //Ambiente.fd.ddtrientroprom = false;
            jsoftware.jbeerapp.forms.FormMenu.this.startActivity(i);
        }

    }

    private void azioneSbloccaVersione() {
        Intent i = new Intent(getApplicationContext(), FormPrimaConf.class);
        //Ambiente.fd.ddtprom = false;
        //Ambiente.fd.ddtrientroprom = false;
        jsoftware.jbeerapp.forms.FormMenu.this.startActivityForResult(i, 2);
    }

    private void azioneCatalogo() {
        if (FunzioniJBeerApp.leggiMessaggi().size() > 0) {
            Intent i = new Intent(getApplicationContext(), FormMessaggi.class);
            jsoftware.jbeerapp.forms.FormMenu.this.startActivity(i);
        } else {
            Intent i = new Intent(getApplicationContext(), FormCatalogo.class);
            //Ambiente.fd.ddtprom = false;
            //Ambiente.fd.ddtrientroprom = false;
            jsoftware.jbeerapp.forms.FormMenu.this.startActivity(i);
        }

    }

    private void azioneTrasmissione() {
        if (FunzioniJBeerApp.leggiMessaggi().size() > 0) {
            Intent i = new Intent(getApplicationContext(), FormMessaggi.class);
            jsoftware.jbeerapp.forms.FormMenu.this.startActivity(i);
        } else {
            Intent i = new Intent(getApplicationContext(), FormScambioDati.class);
            jsoftware.jbeerapp.forms.FormMenu.this.startActivityForResult(i, 1);
        }

    }

    private void azioneShop() {
        if (FunzioniJBeerApp.leggiMessaggi().size() > 0) {
            Intent i = new Intent(getApplicationContext(), FormMessaggi.class);
            jsoftware.jbeerapp.forms.FormMenu.this.startActivity(i);
        } else {
            Intent i = new Intent(getApplicationContext(), FormRistampaDoc.class);
            //Ambiente.fd.ddtprom = false;
            //Ambiente.fd.ddtrientroprom = false;
            jsoftware.jbeerapp.forms.FormMenu.this.startActivity(i);
        }

    }

    private void azioneAppunti() {


        Calendar calendarEvent = Calendar.getInstance();
        Intent i = new Intent(Intent.ACTION_EDIT);
        i.setType("vnd.android.cursor.item/event");
        i.putExtra("beginTime", calendarEvent.getTimeInMillis());
        i.putExtra("allDay", true);
        i.putExtra("rule", "FREQ=YEARLY");
        i.putExtra("endTime", calendarEvent.getTimeInMillis() + 60 * 60 * 1000);
        //i.putExtra("title", "Sample Calender Event Android Application");
        startActivity(i);

    }

    private void azioneStatistiche() {
        if (FunzioniJBeerApp.leggiMessaggi().size() > 0) {
            Intent i = new Intent(getApplicationContext(), FormMessaggi.class);
            jsoftware.jbeerapp.forms.FormMenu.this.startActivity(i);
        } else {
            //Intent i = new Intent(getApplicationContext(), FormStatisticheEventi.class);
            Intent i = new Intent(getApplicationContext(), FormStatistiche.class);
            jsoftware.jbeerapp.forms.FormMenu.this.startActivity(i);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            Intent i = new Intent(FormMenu.this.getApplicationContext(), FormParametri.class);
            FormMenu.this.startActivity(i);
            //FormMenu.this.startActivityForResult(i, 1);
            return true;
        } else if (item.getItemId() == R.id.action_utility) {
            Intent i = new Intent(getApplicationContext(), FormUtilita.class);
            FormMenu.this.startActivity(i);
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
        finish();
    }

    class controlloCarichi extends Thread {
        public String user = "", pwd = "", termcod = "", server = "";
        public boolean uscita = false;
        Handler handler = new Handler();

        public void run() {
            while (!uscita) {
                Log.v("jbeerapp", "controllo carichi...");

                if (!Env.agg_dati_incorso && FunzioniJBeerApp.terminaleConfigurato() && FunzioniJBeerApp.dispositivoOnline(getApplicationContext())) {

                    // test connessione
                    Cursor cursor = Env.db
                            .rawQuery(
                                    "SELECT pedcod,user,pwd,serverjcloud FROM datiterm",
                                    null);
                    cursor.moveToFirst();
                    termcod = cursor.getString(0);
                    user = cursor.getString(1);
                    pwd = cursor.getString(2);
                    server = cursor.getString(3);
                    cursor.close();
                    boolean testok = FunzioniJBeerApp.testUtente(user, pwd, termcod, server);
                    Log.v("jbeerapp", "test utente:" + testok);
                    if (testok) {


                        final ArrayList<HashMap> lf = FunzioniJBeerApp.filesDaRicevere(user, pwd, termcod, server);
                        Log.v("jbeerapp", "   n." + lf.size());
                        if (lf.size() > 0) {
                            handler.post(new Runnable() {
                                public void run() {
                                    alert.setVisibility(View.VISIBLE);
                                    Env.alertagg = true;
                                }
                            });
/*                        handler.post(new Runnable(){
                            public void run() {
                                if (lf.size() == 1)
                                    tvcarichisede.setText("C'è un nuovo carico da sede da ricevere");
                                else
                                    tvcarichisede.setText("Ci sono " + lf.size() + " nuovi carichi da sede da ricevere");
                            }
                        });*/

                        } else {
                            handler.post(new Runnable() {
                                public void run() {
                                    alert.setVisibility(View.INVISIBLE);
                                    Env.alertagg = false;
                                }
                            });

                        }
                    }
                }
                try {
                    //    alert.setVisibility(View.INVISIBLE);
                    Thread.sleep(120000); //2 minuto
                } catch (Exception e) {
                }
            }

        }

    }

    private void ultimoeventoselezionato() {
        // imposta ultimo evento selezionato
        String eventomem = FunzioniJBeerApp.leggiProprieta("eventojbeer");
        if (!eventomem.equals("")) {
            for (int i = 0; i < veventiCod.size(); i++) {
                if (veventiCod.get(i).equals(eventomem)) {
                    speventi.setSelection(i);
                    break;
                }
            }
        } else {
            speventi.setSelection(0);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                boolean aggiornamento = false;
                ris = data.getData();
                if (ris != null) {

                    aggiornamento = (boolean) data.getExtras().get("aggiornamento");
                }
                if (aggiornamento) {
                    veventi.clear();
                    veventiCod.clear();
                    veventi.add("Nessun Evento");
                    veventiCod.add("");
                    caricaeventi();
                    //veventiadapter = new SpinnerAdapterMenu(this, R.layout.spinner_item_background, veventi);
                    //speventi = (Spinner) findViewById(R.id.spinner_eventi);
                    speventi.setAdapter(veventiadapter);
                    alert.setVisibility(View.INVISIBLE);
                    ultimoeventoselezionato();

                }


                break;
            }
            case (2): {
                controlloAttivazione();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //this.aggiornaInformazioni();
    }

    private void showPopUp() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(FormMenu.this);
        helpBuilder.setTitle("ATTENZIONE!");
        //helpBuilder.setMessage("Aggiungere un nuovo contatto");

        LayoutInflater inflater = getLayoutInflater();
        View checkboxLayout = inflater.inflate(R.layout.activity_form_popup_scadenza, null);
        helpBuilder.setView(checkboxLayout);
        helpBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();

    }

    private void controlloAttivazione() {
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        Date newDate = new Date();
        String dataOggi = date.format(newDate);
        Cursor cursor = Env.db.rawQuery("SELECT datattivazione,versionedemo FROM datiterm", null);

        while (cursor.moveToNext()) {
            dataScadenza = cursor.getString(0);
            demo = cursor.getString(1);
        }
        cursor.close();
        if (demo.equals("S")) {
            countDown.setText("DATA SCADENZA PROVA:  " + (new Data(dataScadenza, Data.GG_MM_AAAA)).formatta(Data.GG_MM_AAAA, "/"));
            String scadenza = (new Data(dataScadenza, Data.GG_MM_AAAA)).formatta(Data.AAAA_MM_GG, "-");
            if (scadenza.compareTo(dataOggi) < 0 || scadenza.compareTo(dataOggi) == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(FormMenu.this);
                builder.setMessage("Versione di prova SCADUTA.\nAcquista JbeerGO su Carebox per continuare a lavorare")
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        buttonCatologo.setEnabled(false);
                                        buttonContatti.setEnabled(false);
                                        buttonShop.setEnabled(false);
                                        buttonStatistiche.setEnabled(false);
                                        bScambiodati.setEnabled(false);
                                        bAppunti.setEnabled(false);
                                        //onBackPressed();

                                    }
                                });
                AlertDialog ad = builder.create();
                ad.show();
            } else {
                Data sc = new Data(scadenza, Data.AAAA_MM_GG);
                Data og = new Data(dataOggi, Data.AAAA_MM_GG);
                long a = sc.differenza(og);
                if (sc.differenza(og) <= 5 && sc.differenza(og) >= 1) {
                    showPopUp();
                }
            }

        } else {
            buttonSblocco.setVisibility(View.INVISIBLE);
            llcountdown.setVisibility(View.GONE);
            buttonCatologo.setEnabled(true);
            buttonContatti.setEnabled(true);
            buttonShop.setEnabled(true);
            buttonStatistiche.setEnabled(true);
            bScambiodati.setEnabled(true);
            bAppunti.setEnabled(true);
        }

    }

    private void numeratoriDaAzzerare() {

        boolean ris = false;
        String dti = (new Data()).anno() + "-01-01";
        String dtprec = ((new Data()).anno() - 1) + "-12-31";
        boolean movprec = false;
        String[] parsprec = new String[1];
        parsprec[0] = dtprec;
        Cursor cmprec = Env.db.rawQuery("SELECT movnum FROM movimenti WHERE movdata <= ?", parsprec);
        if (cmprec.moveToFirst())
        {
            movprec = true;
        }
        cmprec.close();
        if (movprec) {
            if (Env.numbollaval > 1) {
                String[] pars = new String[2];
                pars[0] = "0";
                pars[1] = dti;
                Cursor cm = Env.db.rawQuery("SELECT movnum FROM movimenti WHERE movtipo = ? AND movdata >= ?", pars);
                if (!cm.moveToFirst()) {
                    ris = true;
                }
                cm.close();
            }
            if (!ris && Env.numbollaqta > 1) {
                String[] pars = new String[2];
                pars[0] = "1";
                pars[1] = dti;
                Cursor cm = Env.db.rawQuery("SELECT movnum FROM movimenti WHERE movtipo = ? AND movdata >= ?", pars);
                if (!cm.moveToFirst()) {
                    ris = true;
                }
                cm.close();
            }
            if (!ris && Env.numddtreso > 1) {
                String[] pars = new String[2];
                pars[0] = "4";
                pars[1] = dti;
                Cursor cm = Env.db.rawQuery("SELECT movnum FROM movimenti WHERE movtipo = ? AND movdata >= ?", pars);
                if (!cm.moveToFirst()) {
                    ris = true;
                }
                cm.close();
            }
            if (!ris && Env.numscaricosede > 1) {
                String[] pars = new String[2];
                pars[0] = "6";
                pars[1] = dti;
                Cursor cm = Env.db.rawQuery("SELECT movnum FROM movimenti WHERE movtipo = ? AND movdata >= ?", pars);
                if (!cm.moveToFirst()) {
                    ris = true;
                }
                cm.close();
            }
            if (!ris && Env.numtrasfamezzo > 1) {
                String[] pars = new String[2];
                pars[0] = "7";
                pars[1] = dti;
                Cursor cm = Env.db.rawQuery("SELECT movnum FROM movimenti WHERE movtipo = ? AND movdata >= ?", pars);
                if (!cm.moveToFirst()) {
                    ris = true;
                }
                cm.close();
            }
            if (!ris && Env.numtrasfdamezzo > 1) {
                String[] pars = new String[2];
                pars[0] = "8";
                pars[1] = dti;
                Cursor cm = Env.db.rawQuery("SELECT movnum FROM movimenti WHERE movtipo = ? AND movdata >= ?", pars);
                if (!cm.moveToFirst()) {
                    ris = true;
                }
                cm.close();
            }
            if (!ris && Env.numddtcarico > 1) {
                String[] pars = new String[2];
                pars[0] = "5";
                pars[1] = dti;
                Cursor cm = Env.db.rawQuery("SELECT movnum FROM movimenti WHERE movtipo = ? AND movdata >= ?", pars);
                if (!cm.moveToFirst()) {
                    ris = true;
                }
                cm.close();
            }
        }
        if (ris) {
            AlertDialog.Builder builder = new AlertDialog.Builder(FormMenu.this);
            builder.setMessage("ATTENZIONE\nAndare sulle utilita' e azzerare l'anno\n numeratori riferiti all'anno " + ((new Data()).anno() - 1))
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {

                                }
                            });
            AlertDialog ad = builder.create();
            ad.show();
        }
    }
}

