package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;

public class FormPrimaConf extends AppCompatActivity {

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        //
        try {
            if (c != null)
                c.cancel(true);
        } catch (Exception e) {

        }
        super.onBackPressed();
    }

    public boolean testok = false;
    public boolean aggKeyOk = false;
    ProgressBar wait = null;
    Button bok;
    EditText etKey;
    EditText etPin;
    Connection c = null;
    GestKey g = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_prima_conf);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        wait = findViewById(R.id.primaconf_wait);
        bok = findViewById(R.id.primaconf_buttonOk);
        etKey = findViewById(R.id.primaconf_key);
        etPin = findViewById(R.id.primaconf_pin);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        wait.setVisibility(View.INVISIBLE);

        bok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (etKey.getText().toString().equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormPrimaConf.this);
                    builder.setMessage("Inserire la chiave di attivazione")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (etPin.getText().toString().equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormPrimaConf.this);
                    builder.setMessage("Inserire il pin")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else {
                    // controllo dispositivo online
                    if (!FunzioniJBeerApp.dispositivoOnline(getApplicationContext())) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormPrimaConf.this);
                        builder.setMessage("Connessione ad Internet assente")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.show();
                    } else {

                        // controllo validazione utente
                        boolean utenteok = true;
                        c = new Connection();
                        c.key = etKey.getText().toString();
                        wait.setVisibility(View.VISIBLE);
                        bok.setEnabled(false);
                        etKey.setEnabled(false);
                        etPin.setEnabled(false);
                        c.execute();
                    }
                }
            }
        });

    }


    private class Connection extends AsyncTask {
        public String key = "";
        public HashMap risp = new HashMap();
        String codTerm = "";
        String username = "";
        String demo = "";
        String pwd = "";
        String pin = etPin.getText().toString();

        @Override
        protected Object doInBackground(Object... arg0) {
            // test connessione
            risp = FunzioniJBeerApp.testKey(key);
            testok = (boolean) risp.get("esito");
            // PAX
            /*            testok = true;*/
            return null;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Object result) {
            wait.setVisibility(View.INVISIBLE);
            bok.setEnabled(true);
            etKey.setEnabled(true);
            etPin.setEnabled(true);
            String decryptedKey = "";

            Log.v("testok", testok ? "OK" : "NOK");
            if (testok) {
                codTerm = (String) risp.get("terminale");
                username = (String) risp.get("utente");
                pwd = (String) risp.get("password");
                //tempo di prova
                //da inserire nella stringa json una variabile boolean impostata da jazz "segreta" per abilitare poi ogni cliente alla versione completa
                demo = (String) risp.get("versionedemo");
                int numberOfDays = 31;
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DAY_OF_YEAR, numberOfDays);
                dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date newDate = new Date(c.getTimeInMillis());
                String resultDate = dateFormat.format(newDate);
                String sql = "INSERT INTO datiterm (datattivazione) VALUES (?)";
                SQLiteStatement stmt = Env.db
                        .compileStatement(sql);
                stmt.clearBindings();
                stmt.bindString(1, resultDate);


                stmt.execute();
                stmt.close();
                System.out.println("FINE APP:->  " + resultDate);

                //SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");


                // PAX
/*                codTerm = "T80";
                username = "BAM02";
                pwd = "BAM02";*/


//				try {
//					StringEncrypter s = new StringEncrypter(StringEncrypter.DES_ENCRYPTION_SCHEME);
//					decryptedKey = s.decrypt(etKey.getText().toString());
//					Log.v("xd",decryptedKey);
//				} catch (StringEncrypter.EncryptionException e) {
//					e.printStackTrace();
//				}
//
//				if(!decryptedKey.equals(""))
//				{
//					String[] splitted = decryptedKey.split("-");
//					codTerm = splitted[0];
//					username = splitted[1];
//					pwd = splitted[2];
//				}
//				else
//				{
//					AlertDialog.Builder builder = new AlertDialog.Builder(
//							FormPrimaConf.this);
//					builder.setMessage("Errore nel decriptare la chiave")
//							.setPositiveButton("Ok",
//									new DialogInterface.OnClickListener() {
//										public void onClick(DialogInterface dialog,
//															int id) {
//										}
//									});
//					AlertDialog ad = builder.create();
//					ad.show();
//					return;
//				}

                // aggiorna tabella datiterm
                boolean datitermpres = false;
                Cursor cursor = Env.db.rawQuery(
                        "SELECT pedcod,user,pwd,serverjcloud,activationcode FROM datiterm",
                        null);
                if (cursor.moveToFirst())
                    datitermpres = true;
                cursor.close();
                if (!datitermpres) {
                    sql = "INSERT INTO datiterm (pedcod,user,pwd,serverjcloud,activationcode,pin,versionedemo) VALUES (?,?,?,?,?,?,?)";
                    stmt = Env.db
                            .compileStatement(sql);
                    stmt.clearBindings();
                    stmt.bindString(1, codTerm);
                    stmt.bindString(2, username);
                    stmt.bindString(3, pwd);
                    stmt.bindString(4, Env.ipServerJCloud);
                    stmt.bindString(5, etKey.getText().toString());
                    stmt.bindString(6, etPin.getText().toString());
                    stmt.bindString(7, demo);

                    stmt.execute();
                    stmt.close();
                } else {
                    sql = "UPDATE datiterm SET pedcod=?,user=?,pwd=?,serverjcloud=?,activationcode=?,pin=?,versionedemo = ?";
                    stmt = Env.db
                            .compileStatement(sql);
                    stmt.clearBindings();
                    stmt.bindString(1, codTerm);
                    stmt.bindString(2, username);
                    stmt.bindString(3, pwd);
                    stmt.bindString(4, Env.ipServerJCloud);
                    stmt.bindString(5, etKey.getText().toString());
                    stmt.bindString(6, etPin.getText().toString());
                    stmt.bindString(7, demo);
                    stmt.execute();
                    stmt.close();
                }

                g = new GestKey();
                g.key = etKey.getText().toString();
                g.execute();
                //terminale impostato come raccolta ordini
                FunzioniJBeerApp.impostaProprieta("termordini", "S");
                Env.termordini = true;


                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormPrimaConf.this);
                builder.setMessage("Terminale parametrizzato correttamente")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        FormPrimaConf.this.finish();
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
            } else {
/*                if (risp.get("message").toString().equals("Key già utilizzata")) {
                    backup = true;
                }*/
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormPrimaConf.this);
                builder.setMessage((risp.get("message").toString()))
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        if (risp.get("message").toString().equals("Key già utilizzata")) {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                                    FormPrimaConf.this);
                                            builder.setMessage("Ripristinare il dispositivo da un precedente backup?")
                                                    .setPositiveButton("SI",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog,
                                                                                    int id) {
                                                                    Intent i = new Intent(getApplicationContext(), FormRipristino.class);
                                                                    //jsoftware.jbeerapp.forms.FormPrimaConf.this.startActivity(i);
                                                                    FormPrimaConf.this.startActivityForResult(i, 1);
                                                                }
                                                            })
                                                    .setNegativeButton("NO",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog,
                                                                                    int id) {

/*                                                                    Intent i = new Intent(getApplicationContext(), FormRicezioneDati.class);
                                                                    FormPrimaConf.this.startActivityForResult(i, 1);
                                                                    */
                                                                    return;
                                                                }
                                                            });
                                            AlertDialog ad = builder.create();
                                            ad.setCancelable(false);
                                            ad.show();

                                        }
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.show();
            }
        }
    }

    private class GestKey extends AsyncTask {
        String key = "";
        public HashMap risp = new HashMap();

        @Override
        protected Object doInBackground(Object... arg0) {
            // test connessione
            risp = FunzioniJBeerApp.aggiornaKey(key);
            aggKeyOk = (boolean) risp.get("esito");
            return null;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Object result) {

            Log.v("aggKeyOk", aggKeyOk ? "OK" : "NOK");

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                onBackPressed();
            }
            break;

        }
    }

}
