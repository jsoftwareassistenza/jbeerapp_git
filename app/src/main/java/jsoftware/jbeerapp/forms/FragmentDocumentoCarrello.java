package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaDocDettAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentDocumentoCarrello.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDocumentoCarrello#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDocumentoCarrello extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FormDocumento fdoc = null;

    private Button bcatalog;
    public Button bpref;
    public ListView listaart;
    public TextView tvnarts;
    public ListaDocDettAdapter lvdocdettadapter;

    private int posselez = -1;
    // private int qtaTot;

    public FragmentDocumentoCarrello() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentDocumentoCarrello.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDocumentoCarrello newInstance(String param1, String param2) {
        FragmentDocumentoCarrello fragment = new FragmentDocumentoCarrello();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_documento_carrello, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
/*
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        } else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

        bcatalog = this.getActivity().findViewById(R.id.doc_art_buttonGiac);
        bpref = this.getActivity().findViewById(R.id.doc_art_buttonPref);
        listaart = this.getActivity().findViewById(R.id.doc_art_lista);
        tvnarts = this.getActivity().findViewById(R.id.doc_art_label_narts);
/*        View header = this.getActivity().getLayoutInflater().inflate(R.layout.lvdoc_dett_header, null);
        listaart.addHeaderView(header);*/

        registerForContextMenu(listaart);


        bpref.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (fdoc.clicod.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDocumentoCarrello.this.getActivity());
                    builder.setMessage("Specificare il cliente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (fdoc.cliblocco && fdoc.righecrp.size() == 0 && fdoc.fcli.sptipodoc.getSelectedItemPosition() != 6) {
                    int azioneb = Env.azioneblocco;
                    if (fdoc.clinscadblocco > 0 || fdoc.cliimpblocco > 0)
                        azioneb = fdoc.cliazioneblocco;
                    if (azioneb == 0 || fdoc.pagtvpos == -1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FragmentDocumentoCarrello.this.getActivity());
                        builder.setMessage("Il cliente risulta bloccato. Per emettere un documento incassare prima gli importi scaduti")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return;
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FragmentDocumentoCarrello.this.getActivity());
                        builder.setMessage("Il cliente risulta bloccato. Emettere documento con pagamento CONTANTI?")
                                .setPositiveButton("SI",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                fdoc.friep.sppagamento.setSelection(fdoc.pagtvpos);
                                                azionePreferiti();
                                            }
                                        })
                                .setNegativeButton("NO",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                return;
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                    }
                } else if (fdoc.fcli.sptipodoc.getSelectedItemPosition() != 5) {
                    azionePreferiti();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDocumentoCarrello.this.getActivity());
                    builder.setMessage("Causale vendita bloccata")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                }

            }
        });

        bcatalog.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (fdoc.clicod.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDocumentoCarrello.this.getActivity());
                    builder.setMessage("Specificare il cliente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (fdoc.cliblocco && fdoc.righecrp.size() == 0 && fdoc.fcli.sptipodoc.getSelectedItemPosition() != 6) {
                    int azioneb = Env.azioneblocco;
                    if (fdoc.clinscadblocco > 0 || fdoc.cliimpblocco > 0)
                        azioneb = fdoc.cliazioneblocco;
                    if (azioneb == 0 || fdoc.pagtvpos == -1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FragmentDocumentoCarrello.this.getActivity());
                        builder.setMessage("Il cliente risulta bloccato. Per emettere un documento incassare prima gli importi scaduti")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return;
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FragmentDocumentoCarrello.this.getActivity());
                        builder.setMessage("Il cliente risulta bloccato. Emettere documento con pagamento CONTANTI?")
                                .setPositiveButton("SI",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                fdoc.friep.sppagamento.setSelection(fdoc.pagtvpos);
                                                azioneListaGiac();
                                            }
                                        })
                                .setNegativeButton("NO",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                return;
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                    }
                } else if (fdoc.fcli.sptipodoc.getSelectedItemPosition() != 5) {
                    azioneListaGiac();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDocumentoCarrello.this.getActivity());
                    builder.setMessage("Causale vendita bloccata")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                }

            }
        });

        listaart.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
                posselez = (int) arg3;
                return false;
            }
        });
        Env.righecrptmp = new ArrayList();//array utile nel caso vengano gestiti i lotti, tiene conto degli articoli inseriti nel carrello
    }

    private void azionePreferiti() {
        String cli = fdoc.clicod;
        String dest = "";
        if (!fdoc.clicodpadre.equals("")) {
            cli = "";
            dest = fdoc.clicod;
        }
        if (!FunzioniJBeerApp.causaleBloccata(0, Env.depcod, Env.agecod, cli, "", dest)) {
            if (fdoc.fcli.sptipodoc.getSelectedItemPosition() == 6) {
                if (Env.gestlotti) {
                    if (!FunzioniJBeerApp.causaleBloccata(0, Env.depcod, Env.agecod, cli, "", dest)) {

                        String cliordlotto = "A";
                        Env.trasfdoc_righecrp = fdoc.righecrp;
                        Intent i = new Intent(FragmentDocumentoCarrello.this.getActivity().getApplicationContext(), FormDocPreferitiOrdineClientePerLotto.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString("clicod", fdoc.clicod);
                        mBundle.putString("clicodpadre", fdoc.clicodpadre);
                        mBundle.putInt("tipodoc", fdoc.fcli.sptipodoc.getSelectedItemPosition() - 1);
                        mBundle.putString("clicod", fdoc.clicod);
                        mBundle.putDouble("clisc1", Formattazione.estraiDouble(fdoc.fcli.edsc1.getText().toString().replace(".", ",")));
                        mBundle.putDouble("clisc2", Formattazione.estraiDouble(fdoc.fcli.edsc2.getText().toString().replace(".", ",")));
                        mBundle.putDouble("clisc3", Formattazione.estraiDouble(fdoc.fcli.edsc3.getText().toString().replace(".", ",")));
                        mBundle.putInt("caucorr", fdoc.caucorr);
                        i.putExtras(mBundle);
                        FragmentDocumentoCarrello.this.startActivityForResult(i, 3);


                    }
                } else {

                    // preferiti per ordine cliente
                    String cliordlotto = "A";
                    Env.trasfdoc_righecrp = fdoc.righecrp;
                    Intent i = new Intent(FragmentDocumentoCarrello.this.getActivity().getApplicationContext(), FormDocPreferitiOrdineCliente.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("clicod", fdoc.clicod);
                    mBundle.putString("clicodpadre", fdoc.clicodpadre);
                    i.putExtras(mBundle);
                    FragmentDocumentoCarrello.this.startActivityForResult(i, 5);
                }
            } else {
                if (Env.vendqtatot) {
                    // vendite per qta totale
                    String cliordlotto = "A";
                    Env.trasfdoc_righecrp = fdoc.righecrp;
                    Intent i = new Intent(FragmentDocumentoCarrello.this.getActivity().getApplicationContext(), FormDocPreferitiQtaTot.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("clicod", fdoc.clicod);
                    mBundle.putString("clicodpadre", fdoc.clicodpadre);
                    i.putExtras(mBundle);
                    FragmentDocumentoCarrello.this.startActivityForResult(i, 4);
                } else {
                    // vendite per singolo lotto
                    String cliordlotto = "A";
                    Env.trasfdoc_righecrp = fdoc.righecrp;
                    Intent i = new Intent(FragmentDocumentoCarrello.this.getActivity().getApplicationContext(), FormDocPreferiti.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("clicod", fdoc.clicod);
                    mBundle.putString("clicodpadre", fdoc.clicodpadre);
                    i.putExtras(mBundle);
                    FragmentDocumentoCarrello.this.startActivityForResult(i, 3);
                }
            }
        }
    }

    private void azioneListaGiac() {
        String cli = fdoc.clicod;
        String dest = "";
        if (!fdoc.clicodpadre.equals("")) {
            cli = "";
            dest = fdoc.clicod;
        }
        if (!FunzioniJBeerApp.causaleBloccata(0, Env.depcod, Env.agecod, cli, "", dest)) {

            String cliordlotto = "A";
            Env.trasfdoc_righecrp = fdoc.righecrp;
            Intent i = new Intent(FragmentDocumentoCarrello.this.getActivity().getApplicationContext(), FormDocListaVend.class);
            Bundle mBundle = new Bundle();
            mBundle.putString("clicod", fdoc.clicod);
            mBundle.putString("clicodpadre", fdoc.clicodpadre);
            mBundle.putInt("tipodoc", fdoc.fcli.sptipodoc.getSelectedItemPosition() - 1);
            mBundle.putString("clicod", fdoc.clicod);
            mBundle.putDouble("clisc1", Formattazione.estraiDouble(fdoc.fcli.edsc1.getText().toString().replace(".", ",")));
            mBundle.putDouble("clisc2", Formattazione.estraiDouble(fdoc.fcli.edsc2.getText().toString().replace(".", ",")));
            mBundle.putDouble("clisc3", Formattazione.estraiDouble(fdoc.fcli.edsc3.getText().toString().replace(".", ",")));
            mBundle.putInt("caucorr", fdoc.caucorr);
            i.putExtras(mBundle);
            FragmentDocumentoCarrello.this.startActivityForResult(i, 3);


        }
    }

    private void azioneInsArt() {
        Env.trasfdoc_rriga = null;
        Env.trasfdoc_rrigavar = null;
        Env.trasfdoc_righecrp = fdoc.righecrp;
        Intent i = new Intent(FragmentDocumentoCarrello.this.getActivity().getApplicationContext(), FormRigaArt.class);
        Bundle mBundle = new Bundle();
        mBundle.putInt("tipodoc", fdoc.fcli.sptipodoc.getSelectedItemPosition() - 1);
        mBundle.putString("clicod", fdoc.clicod);
        mBundle.putDouble("clisc1", Formattazione.estraiDouble(fdoc.fcli.edsc1.getText().toString().replace(".", ",")));
        mBundle.putDouble("clisc2", Formattazione.estraiDouble(fdoc.fcli.edsc2.getText().toString().replace(".", ",")));
        mBundle.putDouble("clisc3", Formattazione.estraiDouble(fdoc.fcli.edsc3.getText().toString().replace(".", ",")));
        mBundle.putInt("caucorr", fdoc.caucorr);
        i.putExtras(mBundle);
        FragmentDocumentoCarrello.this.startActivityForResult(i, 1);
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        fdoc.nfrag++;
        if (fdoc.nfrag == 3)
            fdoc.inizializza();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                // ritorno inserimento riga art
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    if (Env.trasfdoc_rriga != null) {
                        fdoc.righecrp.add(Env.trasfdoc_rriga);
                        fdoc.aggiornaRigheRaggruppate();
                        lvdocdettadapter.notifyDataSetChanged();
                        fdoc.ricalcolaTotale(fdoc.righecrp);
                        fdoc.caucorr = Env.trasfdoc_caucorr;
                    }
                    String esito = ris.getLastPathSegment();
                    if (esito.equals("OK2")) {
                        // riapre ins.art.
                        Env.trasfdoc_rriga = null;
                        Env.trasfdoc_rrigavar = null;
                        Env.trasfdoc_righecrp = fdoc.righecrp;
                        Intent i = new Intent(FragmentDocumentoCarrello.this.getActivity().getApplicationContext(), FormRigaArt.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putInt("tipodoc", fdoc.fcli.sptipodoc.getSelectedItemPosition() - 1);
                        mBundle.putString("clicod", fdoc.clicod);
                        mBundle.putDouble("clisc1", Formattazione.estraiDouble(fdoc.fcli.edsc1.getText().toString().replace(".", ",")));
                        mBundle.putDouble("clisc2", Formattazione.estraiDouble(fdoc.fcli.edsc2.getText().toString().replace(".", ",")));
                        mBundle.putDouble("clisc3", Formattazione.estraiDouble(fdoc.fcli.edsc3.getText().toString().replace(".", ",")));
                        mBundle.putInt("caucorr", fdoc.caucorr);
                        i.putExtras(mBundle);
                        FragmentDocumentoCarrello.this.startActivityForResult(i, 1);
                    }
                }
                break;
            }
            case (2): {
                // ritorno da variazione riga art
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    if (Env.trasfdoc_rriga != null) {
                        fdoc.righecrp.set(posselez, Env.trasfdoc_rriga);
                        fdoc.aggiornaRigheRaggruppate();
                        lvdocdettadapter.notifyDataSetChanged();
                        fdoc.ricalcolaTotale(fdoc.righecrp);
                        fdoc.caucorr = 0;
                    }
                }
                break;
            }
            case (3): {
                // ritorno dal carrello
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    //qtaTot = Formattazione.estraiIntero(ris.getLastPathSegment());
                    ArrayList<HashMap> vris = (ArrayList<HashMap>) data.getExtras().get("vris");
                    for (int i = 0; i < vris.size(); i++) {
                        HashMap riga = vris.get(i);
                        String artcod = (String) riga.get("artcod");
                        String artdescr = (String) riga.get("artdescr");
                        String codivaart = (String) riga.get("codiva");
                        String lotto = (String) riga.get("lotto");
                        double qtavend = (double) riga.get("qtavend");
                        double qtasm = (double) riga.get("qtasm");
                        double qtaomimp = (double) riga.get("qtaomimp");
                        double qtaomtot = (double) riga.get("qtaomtot");
                        int colliriga = (int) riga.get("colli");
                        double przu = 0;
                        double sc1u = 0;
                        double sc2u = 0;
                        double sc3u = 0;
                        if (riga.get("prezzo") != null)
                            przu = (double) riga.get("prezzo");
                        if (riga.get("sc1") != null)
                            sc1u = (double) riga.get("sc1");
                        if (riga.get("sc2") != null)
                            sc2u = (double) riga.get("sc2");
                        if (riga.get("sc3") != null)
                            sc3u = (double) riga.get("sc3");
                        if (qtavend > 0) {
                            double acc = 0;
                            double grado = 0;
                            double plato = 0;

                            grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(artcod);
                            plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(artcod, lotto);
                            if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                                acc = FunzioniJBeerApp.calcoloAccisaAlcolici(artcod, qtavend, grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));

                            }
                            creaRiga(artcod, artdescr, lotto, 0, qtavend, przu, sc1u, sc2u, sc3u, codivaart, colliriga, acc, grado, plato);
                        }
                        if (qtasm > 0) {
                            double acc = 0;
                            double contras = 0;
                            double grado = 0;
                            double plato = 0;

                            grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(artcod);
                            plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(artcod, lotto);
                            if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                                acc = FunzioniJBeerApp.calcoloAccisaAlcolici(artcod, qtasm, grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));

                            }
                            creaRiga(artcod, artdescr, lotto, 4, qtasm, przu, sc1u, sc2u, sc3u, codivaart, colliriga, acc, grado, plato);
                        }
                        if (qtaomimp > 0) {
                            double acc = 0;
                            double contras = 0;
                            double grado = 0;
                            double plato = 0;

                            grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(artcod);
                            plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(artcod, lotto);
                            if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                                acc = FunzioniJBeerApp.calcoloAccisaAlcolici(artcod, qtaomimp, grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));

                            }
                            creaRiga(artcod, artdescr, lotto, 5, qtaomimp, przu, sc1u, sc2u, sc3u, codivaart, colliriga, acc, grado, plato);
                        }
                        if (qtaomtot > 0) {
                            double acc = 0;
                            double contras = 0;
                            double grado = 0;
                            double plato = 0;

                            grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(artcod);
                            plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(artcod, lotto);
                            if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                                acc = FunzioniJBeerApp.calcoloAccisaAlcolici(artcod, qtaomtot, grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));

                            }
                            creaRiga(artcod, artdescr, lotto, 6, qtaomtot, przu, sc1u, sc2u, sc3u, codivaart, colliriga, acc, grado, plato);
                        }
                    }
                    fdoc.aggiornaRigheRaggruppate();
                    fdoc.fart.lvdocdettadapter.notifyDataSetChanged();
                    fdoc.ricalcolaTotale(fdoc.righecrp);
                    if (fdoc.righecrp.size() > 0)
                        fdoc.fcli.sptipodoc.setEnabled(false);

                }
                break;
            }
            case (4): {
                // ritorno da lista preferiti,giacenze qta tot
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    ArrayList<HashMap> vris = (ArrayList<HashMap>) data.getExtras().get("vris");
                    for (int i = 0; i < vris.size(); i++) {
                        HashMap riga = vris.get(i);
                        String artcod = (String) riga.get("artcod");
                        String artdescr = (String) riga.get("artdescr");
                        String codivaart = (String) riga.get("codiva");
                        double qtavend = (double) riga.get("qtavend");
                        double qtasm = (double) riga.get("qtasm");
                        double qtaomimp = (double) riga.get("qtaomimp");
                        double qtaomtot = (double) riga.get("qtaomtot");
                        int colliriga = (int) riga.get("colli");
                        double qtatot = qtavend + qtasm + qtaomimp + qtaomtot;
                        double przu = 0;
                        if (riga.get("prezzo") != null)
                            przu = (double) riga.get("prezzo");
                        double sc1u = 0;
                        double sc2u = 0;
                        double sc3u = 0;
                        if (riga.get("sc1") != null)
                            sc1u = (double) riga.get("sc1");
                        if (riga.get("sc2") != null)
                            sc2u = (double) riga.get("sc2");
                        if (riga.get("sc3") != null)
                            sc3u = (double) riga.get("sc3");
                        // scarica lotti
                        String[] parsl = new String[1];
                        parsl[0] = artcod;
                        ArrayList<Record> vlotti = new ArrayList();
                        Cursor cl = Env.db.rawQuery(
                                "SELECT lotcod,lotgiacenza FROM lotti LEFT JOIN datelotti ON (lotartcod = dlartcod AND lotcod = dllotcod) WHERE lotartcod=? AND lotgiacenza <> 0 ORDER BY dldata ASC,lotgiacenza DESC,lotcod ASC", parsl);
                        while (cl.moveToNext()) {
                            Record r = new Record();
                            r.insStringa("lotto", cl.getString(0));
                            r.insDouble("qvend", 0);
                            r.insDouble("qsm", 0);
                            r.insDouble("qomimp", 0);
                            r.insDouble("qomtot", 0);
                            r.insDouble("residuo", cl.getDouble(1));
                            vlotti.add(r);
                        }
                        cl.close();
                        while (qtavend > 0) {
                            for (int l = 0; l < vlotti.size(); l++) {
                                Record rl = vlotti.get(l);
                                if (rl.leggiDouble("residuo") >= 0) {
                                    if (rl.leggiDouble("residuo") >= qtavend) {
                                        double qold = rl.leggiDouble("qvend");
                                        rl.eliminaCampo("qvend");
                                        rl.insDouble("qvend", OpValute.arrotondaMat(qold + qtavend, 3));
                                        double res = OpValute.arrotondaMat(rl.leggiDouble("residuo") - qtavend, 3);
                                        qtavend = 0;
                                        rl.eliminaCampo("residuo");
                                        rl.insDouble("residuo", res);
                                    } else {
                                        double qtares = rl.leggiDouble("residuo");
                                        rl.eliminaCampo("residuo");
                                        rl.insDouble("residuo", 0);
                                        double qold = rl.leggiDouble("qvend");
                                        rl.eliminaCampo("qvend");
                                        rl.insDouble("qvend", OpValute.arrotondaMat(qold + qtares, 3));
                                        qtavend = OpValute.arrotondaMat(qtavend - qtares, 3);
                                    }
                                }
                            }
                            if (qtavend > 0) {
                                Record rl = vlotti.get(vlotti.size() - 1);
                                double qold = rl.leggiDouble("qvend");
                                rl.eliminaCampo("qvend");
                                rl.insDouble("qvend", OpValute.arrotondaMat(qold + qtavend, 3));
                                qtavend = 0;
                            }
                        }
                        while (qtasm > 0) {
                            for (int l = 0; l < vlotti.size(); l++) {
                                Record rl = vlotti.get(l);
                                if (rl.leggiDouble("residuo") >= 0) {
                                    if (rl.leggiDouble("residuo") >= qtasm) {
                                        double qold = rl.leggiDouble("qsm");
                                        rl.eliminaCampo("qsm");
                                        rl.insDouble("qsm", OpValute.arrotondaMat(qold + qtasm, 3));
                                        double res = OpValute.arrotondaMat(rl.leggiDouble("residuo") - qtasm, 3);
                                        qtasm = 0;
                                        rl.eliminaCampo("residuo");
                                        rl.insDouble("residuo", res);
                                    } else {
                                        double qtares = rl.leggiDouble("residuo");
                                        rl.eliminaCampo("residuo");
                                        rl.insDouble("residuo", 0);
                                        double qold = rl.leggiDouble("qsm");
                                        rl.eliminaCampo("qsm");
                                        rl.insDouble("qsm", OpValute.arrotondaMat(qold + qtares, 3));
                                        qtasm = OpValute.arrotondaMat(qtasm - qtares, 3);
                                    }
                                }
                            }
                            if (qtasm > 0) {
                                Record rl = vlotti.get(vlotti.size() - 1);
                                double qold = rl.leggiDouble("qsm");
                                rl.eliminaCampo("qsm");
                                rl.insDouble("qsm", OpValute.arrotondaMat(qold + qtasm, 3));
                                qtasm = 0;
                            }
                        }
                        while (qtaomimp > 0) {
                            for (int l = 0; l < vlotti.size(); l++) {
                                Record rl = vlotti.get(l);
                                if (rl.leggiDouble("residuo") >= 0) {
                                    if (rl.leggiDouble("residuo") >= qtaomimp) {
                                        double qold = rl.leggiDouble("qomimp");
                                        rl.eliminaCampo("qomimp");
                                        rl.insDouble("qomimp", OpValute.arrotondaMat(qold + qtaomimp, 3));
                                        double res = OpValute.arrotondaMat(rl.leggiDouble("residuo") - qtaomimp, 3);
                                        qtaomimp = 0;
                                        rl.eliminaCampo("residuo");
                                        rl.insDouble("residuo", res);
                                    } else {
                                        double qtares = rl.leggiDouble("residuo");
                                        rl.eliminaCampo("residuo");
                                        rl.insDouble("residuo", 0);
                                        double qold = rl.leggiDouble("qomimp");
                                        rl.eliminaCampo("qomimp");
                                        rl.insDouble("qomimp", OpValute.arrotondaMat(qold + qtares, 3));
                                        qtaomimp = OpValute.arrotondaMat(qtaomimp - qtares, 3);
                                    }
                                }
                            }
                            if (qtaomimp > 0) {
                                Record rl = vlotti.get(vlotti.size() - 1);
                                double qold = rl.leggiDouble("qomimp");
                                rl.eliminaCampo("qomimp");
                                rl.insDouble("qomimp", OpValute.arrotondaMat(qold + qtaomimp, 3));
                                qtaomimp = 0;
                            }
                        }
                        while (qtaomtot > 0) {
                            for (int l = 0; l < vlotti.size(); l++) {
                                Record rl = vlotti.get(l);
                                if (rl.leggiDouble("residuo") >= 0) {
                                    if (rl.leggiDouble("residuo") >= qtaomtot) {
                                        double qold = rl.leggiDouble("qomtot");
                                        rl.eliminaCampo("qomtot");
                                        rl.insDouble("qomtot", OpValute.arrotondaMat(qold + qtaomtot, 3));
                                        double res = OpValute.arrotondaMat(rl.leggiDouble("residuo") - qtaomtot, 3);
                                        qtaomtot = 0;
                                        rl.eliminaCampo("residuo");
                                        rl.insDouble("residuo", res);
                                    } else {
                                        double qtares = rl.leggiDouble("residuo");
                                        rl.eliminaCampo("residuo");
                                        rl.insDouble("residuo", 0);
                                        double qold = rl.leggiDouble("qomtot");
                                        rl.eliminaCampo("qomtot");
                                        rl.insDouble("qomtot", OpValute.arrotondaMat(qold + qtares, 3));
                                        qtaomtot = OpValute.arrotondaMat(qtaomtot - qtares, 3);
                                    }
                                }
                            }
                            if (qtaomtot > 0) {
                                Record rl = vlotti.get(vlotti.size() - 1);
                                double qold = rl.leggiDouble("qomtot");
                                rl.eliminaCampo("qomtot");
                                rl.insDouble("qomtot", OpValute.arrotondaMat(qold + qtaomtot, 3));
                                qtaomtot = 0;
                            }
                        }
                        for (int l = 0; l < vlotti.size(); l++) {
                            Record rl = vlotti.get(l);
                            if (rl.leggiDouble("qvend") > 0) {
                                double acc = 0;
                                double contras = 0;
                                double grado = 0;
                                double plato = 0;

                                grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(artcod);
                                plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(artcod, rl.leggiStringa("lotto"));
                                if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                                    acc = FunzioniJBeerApp.calcoloAccisaAlcolici(artcod, rl.leggiDouble("qvend"), grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));

                                }

                                creaRiga(artcod, artdescr, rl.leggiStringa("lotto"), 0, rl.leggiDouble("qvend"), przu, sc1u, sc2u, sc3u, codivaart, colliriga, acc, grado, plato);
                            }
                            if (rl.leggiDouble("qsm") > 0) {
                                double acc = 0;
                                double contras = 0;
                                double grado = 0;
                                double plato = 0;
                                grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(artcod);
                                plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(artcod, rl.leggiStringa("lotto"));
                                if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                                    acc = FunzioniJBeerApp.calcoloAccisaAlcolici(artcod, rl.leggiDouble("qsm"), grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                }
                                creaRiga(artcod, artdescr, rl.leggiStringa("lotto"), 4, rl.leggiDouble("qsm"), przu, sc1u, sc2u, sc3u, codivaart, colliriga, acc, grado, plato);
                            }
                            if (rl.leggiDouble("qomimp") > 0) {
                                double acc = 0;
                                double contras = 0;
                                double grado = 0;
                                double plato = 0;

                                grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(artcod);
                                plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(artcod, rl.leggiStringa("lotto"));
                                if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                                    acc = FunzioniJBeerApp.calcoloAccisaAlcolici(artcod, rl.leggiDouble("qomimp"), grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                }
                                creaRiga(artcod, artdescr, rl.leggiStringa("lotto"), 5, rl.leggiDouble("qomimp"), przu, sc1u, sc2u, sc3u, codivaart, colliriga, acc, grado, plato);
                            }
                            if (rl.leggiDouble("qomtot") > 0) {
                                double acc = 0;
                                double contras = 0;
                                double grado = 0;
                                double plato = 0;

                                grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(artcod);
                                plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(artcod, rl.leggiStringa("lotto"));
                                if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                                    acc = FunzioniJBeerApp.calcoloAccisaAlcolici(artcod, rl.leggiDouble("qomtot"), grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                }
                                creaRiga(artcod, artdescr, rl.leggiStringa("lotto"), 6, rl.leggiDouble("qomtot"), przu, sc1u, sc2u, sc3u, codivaart, colliriga, acc, grado, plato);
                            }
                        }
                    }
                    fdoc.aggiornaRigheRaggruppate();
                    fdoc.fart.lvdocdettadapter.notifyDataSetChanged();
                    fdoc.ricalcolaTotale(fdoc.righecrp);
                    if (fdoc.righecrp.size() > 0)
                        fdoc.fcli.sptipodoc.setEnabled(false);
                }
                break;
            }
            case (5): {
                // ritorno da lista preferiti ordine cliente
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    ArrayList<HashMap> vris = (ArrayList<HashMap>) data.getExtras().get("vris");
                    for (int i = 0; i < vris.size(); i++) {
                        HashMap riga = vris.get(i);
                        String artcod = (String) riga.get("artcod");
                        String artdescr = (String) riga.get("artdescr");
                        String codivaart = (String) riga.get("codiva");

                        int collivend = (int) riga.get("colli");
                        int collism = (int) riga.get("colli");
                        int colliomimp = (int) riga.get("colli");
                        int colliomtot = (int) riga.get("colli");

                        double qtavend = (double) riga.get("qtavend");
                        double qtasm = (double) riga.get("qtasm");
                        double qtaomimp = (double) riga.get("qtaomimp");
                        double qtaomtot = (double) riga.get("qtaomtot");
                        double przu = 0;
                        double sc1u = 0;
                        double sc2u = 0;
                        double sc3u = 0;
                        if (riga.get("prezzo") != null)
                            przu = (double) riga.get("prezzo");
                        if (riga.get("sc1") != null)
                            sc1u = (double) riga.get("sc1");
                        if (riga.get("sc2") != null)
                            sc2u = (double) riga.get("sc2");
                        if (riga.get("sc3") != null)
                            sc3u = (double) riga.get("sc3");
                        if (qtavend > 0) {
                            double acc = 0;
                            double contras = 0;
                            double grado = 0;
                            double plato = 0;

                            grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(artcod);
                            plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(artcod, "");
                            if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                                acc = FunzioniJBeerApp.calcoloAccisaAlcolici(artcod, qtavend, grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                            }
                            creaRiga(artcod, artdescr, "", 0, qtavend, przu, sc1u, sc2u, sc3u, codivaart, collivend, acc, grado, plato);
                        }
                        if (qtasm > 0) {
                            double acc = 0;
                            double contras = 0;
                            double grado = 0;
                            double plato = 0;

                            grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(artcod);
                            plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(artcod, "");
                            if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                                acc = FunzioniJBeerApp.calcoloAccisaAlcolici(artcod, qtasm, grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                            }
                            creaRiga(artcod, artdescr, "", 4, qtasm, przu, sc1u, sc2u, sc3u, codivaart, collism, acc, grado, plato);
                        }
                        if (qtaomimp > 0) {
                            double acc = 0;
                            double contras = 0;
                            double grado = 0;
                            double plato = 0;

                            grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(artcod);
                            plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(artcod, "");
                            if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                                acc = FunzioniJBeerApp.calcoloAccisaAlcolici(artcod, qtaomimp, grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                            }
                            creaRiga(artcod, artdescr, "", 5, qtaomimp, przu, sc1u, sc2u, sc3u, codivaart, colliomimp, acc, grado, plato);
                        }
                        if (qtaomtot > 0) {
                            double acc = 0;
                            double grado = 0;
                            double plato = 0;

                            grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(artcod);
                            plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(artcod, "");
                            if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                                acc = FunzioniJBeerApp.calcoloAccisaAlcolici(artcod, qtaomtot, grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                            }
                            creaRiga(artcod, artdescr, "", 6, qtaomtot, przu, sc1u, sc2u, sc3u, codivaart, colliomimp, acc, grado, plato);
                        }
                    }
                    fdoc.aggiornaRigheRaggruppate();
                    fdoc.fart.lvdocdettadapter.notifyDataSetChanged();
                    fdoc.ricalcolaTotale(fdoc.righecrp);
                    if (fdoc.righecrp.size() > 0)
                        fdoc.fcli.sptipodoc.setEnabled(false);
                }
                break;
            }
        }
    }

    private void creaRiga(String artcod, String artdescr, String lottoriga, int cau, double qtariga,
                          double przu, double sc1u, double sc2u, double sc3u, String codivaart, int colliriga, double accisa, double grado, double plato) {
        // creazione dati riga
        Record rriga = new Record();
        rriga.insStringa("rmtiporiga", "A");
        rriga.insStringa("rmartcod", artcod);
        rriga.insStringa("rmartdescr", artdescr);
        rriga.insStringa("rmlotto", lottoriga);
        boolean omimp = false;
        boolean omtot = false;
        if (fdoc.fcli.sptipodoc.getSelectedItemPosition() == 6) {
            rriga.insStringa("rmcaumag", "OC");
            if (cau == 4)
                omtot = true;
            else if (cau == 5)
                omimp = true;
            else if (cau == 6)
                omtot = true;
        } else if (cau == 0)
            rriga.insStringa("rmcaumag", "VE");
        else if (cau == 4) {
            rriga.insStringa("rmcaumag", "SM");
            omtot = true;
        } else if (cau == 5) {
            rriga.insStringa("rmcaumag", "OM");
            omimp = true;
        } else if (cau == 6) {
            rriga.insStringa("rmcaumag", "OT");
            omtot = true;
        }
        rriga.insIntero("rmcolli", colliriga);
        rriga.insDouble("rmpezzi", 0);
        double contenutoriga = FunzioniJBeerApp.calcoloContenutoArticolo(artcod, qtariga);
        rriga.insDouble("rmcontenuto", contenutoriga);
        rriga.insDouble("rmqta", qtariga);
        if (przu != 0) {
            rriga.insDouble("rmprz", przu);
            double sc1 = sc1u;
            double sc2 = sc2u;
            double sc3 = sc3u;
            double clisc1 = Formattazione.estraiDouble(fdoc.fcli.edsc1.getText().toString().replace(".", ","));
            double clisc2 = Formattazione.estraiDouble(fdoc.fcli.edsc2.getText().toString().replace(".", ","));
            double clisc3 = Formattazione.estraiDouble(fdoc.fcli.edsc3.getText().toString().replace(".", ","));
            rriga.insDouble("rmartsc1", sc1);
            rriga.insDouble("rmartsc2", sc2);
            rriga.insDouble("rmscvend", sc3);
            double aliq = 0;
            String codiva = "";
            String codivaecc = "";
            double aliqecc = 0;
            String cc = fdoc.clicod;
            if (!fdoc.clicodpadre.equals(""))
                cc = fdoc.clicodpadre;
            String[] parsei = new String[2];
            parsei[0] = cc;
            parsei[1] = artcod;
            Cursor cei = Env.db.rawQuery(
                    "SELECT codiva.ivacod,codiva.ivaaliq FROM cliartiva INNER JOIN codiva ON cliartiva.ivacod = codiva.ivacod WHERE clicod = ? AND artcod = ?", parsei);
            if (cei.moveToFirst()) {
                codivaecc = cei.getString(0);
                aliqecc = cei.getDouble(1);
            }
            cei.close();
            if (!fdoc.clicodiva.equals("")) {
                Record riva = Env.hcodiva.get(fdoc.clicodiva);
                if (riva != null) {
                    codiva = riva.leggiStringa("ivacod");
                    aliq = riva.leggiDouble("ivaaliq");
                }
            } else {
                Record riva = Env.hcodiva.get(codivaart);
                if (riva != null) {
                    codiva = riva.leggiStringa("ivacod");
                    aliq = riva.leggiDouble("ivaaliq");
                }
            }
            if (!codivaecc.equals("")) {
                codiva = codivaecc;
                aliq = aliqecc;
            }
            if (omimp) {
                if (aliq == 22)
                    codiva = Env.codivaomaggiimp20;
                else if (aliq == 10)
                    codiva = Env.codivaomaggiimp10;
                else if (aliq == 4)
                    codiva = Env.codivaomaggiimp04;
            } else if (omtot) {
                if (aliq == 22)
                    codiva = Env.codivaomaggitot20;
                else if (aliq == 10)
                    codiva = Env.codivaomaggitot10;
                else if (aliq == 4)
                    codiva = Env.codivaomaggitot04;
            }
            rriga.insStringa("rmcodiva", codiva);
            rriga.insDouble("rmaliq", aliq);
            rriga.insDouble("rmclisc1", clisc1);
            rriga.insDouble("rmclisc2", clisc2);
            rriga.insDouble("rmclisc3", clisc3);
            double imp = 0;
            if (FunzioniJBeerApp.tipoCalcoloPrezzoArticolo(artcod) == 1) {
                if (colliriga != 0)
                    imp = OpValute.arrotondaMat((double) colliriga * przu, 2);
                else
                    imp = OpValute.arrotondaMat(qtariga * przu, 2);
            } else if (FunzioniJBeerApp.tipoCalcoloPrezzoArticolo(artcod) == 3 || FunzioniJBeerApp.tipoCalcoloPrezzoArticolo(artcod) == 4) {
                if (contenutoriga != 0)
                    imp = OpValute.arrotondaMat(contenutoriga * przu, 2);
                else
                    imp = OpValute.arrotondaMat(qtariga * przu, 2);
            } else
                imp = OpValute.arrotondaMat(qtariga * przu, 2);
            rriga.insDouble("rmlordo", imp);
            float scoart = 0;
            if (sc1 != 0) {
                double s = (imp * sc1 / 100);
                imp -= s;
                scoart += s;
            }
            if (sc2 != 0) {
                double s = (imp * sc2 / 100);
                imp -= s;
                scoart += s;
            }
            if (sc3 != 0) {
                double s = (imp * sc3 / 100);
                imp -= s;
                scoart += s;
            }
            rriga.insDouble("totscontiart", scoart);
            float scocli = 0;
            if (clisc1 != 0) {
                double s = (imp * clisc1 / 100);
                imp -= s;
                scocli += s;
            }
            if (clisc2 != 0) {
                double s = (imp * clisc2 / 100);
                imp -= s;
                scocli += s;
            }
            if (clisc3 != 0) {
                double s = (imp * clisc3 / 100);
                imp -= s;
                scocli += s;
            }
            rriga.insDouble("totsconticli", scocli);
            imp = OpValute.arrotondaMat(imp, 2);
            rriga.insDouble("rmnetto", imp);
            rriga.insDouble("rmnettoivato", imp);
            // cessionario
            String cesscod = "";
            if (!fdoc.clicodpadre.equals(""))
                cesscod = FunzioniJBeerApp.cercaCodiceCessionario(artcod, fdoc.clicod);
            rriga.insStringa("cesscod", cesscod);
            rriga.insDouble("rmaccisa", accisa);
            rriga.insDouble("rmgrado", grado);
            rriga.insDouble("rmplato", plato);
            fdoc.righecrp.add(rriga);
        } else {
            double[] prz = fdoc.calcoloPrezzoArticolo(artcod, fdoc.clicod);
            //if (prz[3] > 0)
            {
                rriga.insDouble("rmprz", prz[3]);
                double sc1 = prz[0];
                double sc2 = prz[1];
                double sc3 = prz[2];
                double clisc1 = Formattazione.estraiDouble(fdoc.fcli.edsc1.getText().toString().replace(".", ","));
                double clisc2 = Formattazione.estraiDouble(fdoc.fcli.edsc2.getText().toString().replace(".", ","));
                double clisc3 = Formattazione.estraiDouble(fdoc.fcli.edsc3.getText().toString().replace(".", ","));
                rriga.insDouble("rmartsc1", sc1);
                rriga.insDouble("rmartsc2", sc2);
                rriga.insDouble("rmscvend", sc3);
                double aliq = 0;
                String codiva = "";
                String codivaecc = "";
                double aliqecc = 0;
                String cc = fdoc.clicod;
                if (!fdoc.clicodpadre.equals(""))
                    cc = fdoc.clicodpadre;
                String[] parsei = new String[2];
                parsei[0] = cc;
                parsei[1] = artcod;
                Cursor cei = Env.db.rawQuery(
                        "SELECT codiva.ivacod,codiva.ivaaliq FROM cliartiva INNER JOIN codiva ON cliartiva.ivacod = codiva.ivacod WHERE clicod = ? AND artcod = ?", parsei);
                if (cei.moveToFirst()) {
                    codivaecc = cei.getString(0);
                    aliqecc = cei.getDouble(1);
                }
                cei.close();
                if (!fdoc.clicodiva.equals("")) {
                    Record riva = Env.hcodiva.get(fdoc.clicodiva);
                    if (riva != null) {
                        codiva = riva.leggiStringa("ivacod");
                        aliq = riva.leggiDouble("ivaaliq");
                    }
                } else {
                    Record riva = Env.hcodiva.get(codivaart);
                    if (riva != null) {
                        codiva = riva.leggiStringa("ivacod");
                        aliq = riva.leggiDouble("ivaaliq");
                    }
                }
                if (!codivaecc.equals("")) {
                    codiva = codivaecc;
                    aliq = aliqecc;
                }
                if (omimp) {
                    if (aliq == 22)
                        codiva = Env.codivaomaggiimp20;
                    else if (aliq == 10)
                        codiva = Env.codivaomaggiimp10;
                    else if (aliq == 4)
                        codiva = Env.codivaomaggiimp04;
                } else if (omtot) {
                    if (aliq == 22)
                        codiva = Env.codivaomaggitot20;
                    else if (aliq == 10)
                        codiva = Env.codivaomaggitot10;
                    else if (aliq == 4)
                        codiva = Env.codivaomaggitot04;
                }
                rriga.insStringa("rmcodiva", codiva);
                rriga.insDouble("rmaliq", aliq);
                rriga.insDouble("rmclisc1", clisc1);
                rriga.insDouble("rmclisc2", clisc2);
                rriga.insDouble("rmclisc3", clisc3);
                double imp = OpValute.arrotondaMat(qtariga * prz[3], 2);
                rriga.insDouble("rmlordo", imp);
                float scoart = 0;
                if (sc1 != 0) {
                    double s = (imp * sc1 / 100);
                    imp -= s;
                    scoart += s;
                }
                if (sc2 != 0) {
                    double s = (imp * sc2 / 100);
                    imp -= s;
                    scoart += s;
                }
                if (sc3 != 0) {
                    double s = (imp * sc3 / 100);
                    imp -= s;
                    scoart += s;
                }
                rriga.insDouble("totscontiart", scoart);
                float scocli = 0;
                if (clisc1 != 0) {
                    double s = (imp * clisc1 / 100);
                    imp -= s;
                    scocli += s;
                }
                if (clisc2 != 0) {
                    double s = (imp * clisc2 / 100);
                    imp -= s;
                    scocli += s;
                }
                if (clisc3 != 0) {
                    double s = (imp * clisc3 / 100);
                    imp -= s;
                    scocli += s;
                }
                rriga.insDouble("totsconticli", scocli);
                imp = OpValute.arrotondaMat(imp, 2);
                rriga.insDouble("rmnetto", imp);
                rriga.insDouble("rmnettoivato", imp);
                // cessionario
                String cesscod = "";
                if (!fdoc.clicodpadre.equals(""))
                    cesscod = FunzioniJBeerApp.cercaCodiceCessionario(artcod, fdoc.clicod);
                rriga.insStringa("cesscod", cesscod);
                rriga.insDouble("rmaccisa", accisa);
                rriga.insDouble("rmgrado", grado);
                rriga.insDouble("rmplato", plato);
                fdoc.righecrp.add(rriga);
            }
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(null);
        menu.add(0, v.getId(), 0, "Elimina");
        //if (!Env.vendqtatot)
        menu.add(0, v.getId(), 0, "Modifica");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals("Elimina")) {
            if (posselez != -1) {
                if (fdoc.fcli.sptipodoc.getSelectedItemPosition() == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDocumentoCarrello.this.getActivity());
                    builder.setMessage("Specificare il documento da emettere")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return false;
                } else {
                    Log.v("jbeergo", "POSSELEZ:" + posselez);
                    if (Env.vendqtatot) {
                        // eliminazione di tutte le righe con stesso raggruppamento
                        Record rec = fdoc.righecrpraggr.get(posselez);
                        String cod = rec.leggiStringa("rmartcod");
                        String causale = rec.leggiStringa("rmcaumag");
                        boolean smpres = false;
                        if (causale.equals("VE")) {
                            for (int i = 0; i < fdoc.righecrp.size(); i++) {
                                Record rec2 = fdoc.righecrp.get(i);
                                String cod2 = rec2.leggiStringa("rmartcod");
                                String causale2 = rec2.leggiStringa("rmcaumag");
                                if (cod2.equals(cod) && causale2.equals("SM")) {
                                    smpres = true;
                                    break;
                                }
                            }
                        }
                        if (smpres) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                    FragmentDocumentoCarrello.this.getActivity());
                            builder.setMessage("Cancellazione bloccata. Cancellare prima sconti in merce per l'articolo " + cod)
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int id) {
                                                }
                                            });
                            AlertDialog ad = builder.create();
                            ad.setCancelable(false);
                            ad.show();
                            return false;
                        } else {
                            for (int i = 0; i < fdoc.righecrp.size(); i++) {
                                Record rx = fdoc.righecrp.get(i);
/*                                if(!Env.gestlotti) {
                                    if (rx.leggiStringa("rmartcod").equals(rec.leggiStringa("rmartcod")) &&
                                            rx.leggiStringa("rmcaumag").equals(rec.leggiStringa("rmcaumag")) &&
                                            rx.leggiDouble("rmprz") == rec.leggiDouble("rmprz") &&
                                            rx.leggiDouble("rmartsc1") == rec.leggiDouble("rmartsc1") &&
                                            rx.leggiDouble("rmartsc2") == rec.leggiDouble("rmartsc2") &&
                                            rx.leggiDouble("rmscvend") == rec.leggiDouble("rmscvend") &&
                                            rx.leggiStringa("rmcodiva").equals(rec.leggiStringa("rmcodiva")) &&
                                            rx.leggiStringa("cesscod").equals(rec.leggiStringa("cesscod"))) {
                                        rx.insStringa("ELIM", "S");
                                    }
                                }else{*/
                                if (rx.leggiStringa("rmartcod").equals(rec.leggiStringa("rmartcod")) &&
                                        rx.leggiStringa("rmcaumag").equals(rec.leggiStringa("rmcaumag")) &&
                                        rx.leggiDouble("rmprz") == rec.leggiDouble("rmprz") &&
                                        rx.leggiDouble("rmartsc1") == rec.leggiDouble("rmartsc1") &&
                                        rx.leggiDouble("rmartsc2") == rec.leggiDouble("rmartsc2") &&
                                        rx.leggiDouble("rmscvend") == rec.leggiDouble("rmscvend") &&
                                        rx.leggiStringa("rmcodiva").equals(rec.leggiStringa("rmcodiva")) &&
                                        rx.leggiStringa("cesscod").equals(rec.leggiStringa("cesscod")) &&
                                        rx.leggiStringa("rmlotto").equals(rec.leggiStringa("rmlotto"))) {
                                    rx.insStringa("ELIM", "S");
                                }

                            }
                            boolean finitocanc = false;
                            while (!finitocanc) {
                                boolean canc = false;
                                for (int i = 0; i < fdoc.righecrp.size(); i++) {
                                    Record rx = fdoc.righecrp.get(i);
                                    if (rx.esisteCampo("ELIM")) {
                                        Env.quantitaCarrello -= rx.leggiIntero("rmcolli");
                                        fdoc.righecrp.remove(i);
                                        canc = true;
                                    }
                                }
                                if (!canc)
                                    finitocanc = true;
                            }

                            fdoc.aggiornaRigheRaggruppate();
                            lvdocdettadapter.notifyDataSetChanged();
                            fdoc.ricalcolaTotale(fdoc.righecrp);
                        }

                    } else {
                        Record rec = fdoc.righecrp.get(posselez);
                        String cod = rec.leggiStringa("rmartcod");
                        String causale = rec.leggiStringa("rmcaumag");
                        Env.quantitaCarrello -= rec.leggiIntero("rmcolli");
                        boolean smpres = false;
                        if (causale.equals("VE")) {
                            for (int i = 0; i < fdoc.righecrp.size(); i++) {
                                Record rec2 = fdoc.righecrp.get(i);
                                String cod2 = rec2.leggiStringa("rmartcod");
                                String causale2 = rec2.leggiStringa("rmcaumag");
                                if (cod2.equals(cod) && causale2.equals("SM")) {
                                    smpres = true;
                                    break;
                                }
                            }
                        }
                        if (smpres) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                    FragmentDocumentoCarrello.this.getActivity());
                            builder.setMessage("Cancellazione bloccata. Cancellare prima sconti in merce per l'articolo " + cod)
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int id) {
                                                }
                                            });
                            AlertDialog ad = builder.create();
                            ad.setCancelable(false);
                            ad.show();
                            return false;
                        } else {
                            fdoc.righecrp.remove(posselez);
                            fdoc.aggiornaRigheRaggruppate();
                            lvdocdettadapter.notifyDataSetChanged();
                            fdoc.ricalcolaTotale(fdoc.righecrp);
                        }
                    }
                }
            }
        } else if (item.getTitle().equals("Modifica")) {
            if (posselez != -1) {
                if (fdoc.fcli.sptipodoc.getSelectedItemPosition() == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentDocumentoCarrello.this.getActivity());
                    builder.setMessage("Specificare il documento da emettere")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return false;
                } else {
                    Record rec = fdoc.righecrp.get(posselez);
                    Env.trasfdoc_rriga = null;
                    Env.trasfdoc_rrigavar = rec;
                    Env.trasfdoc_righecrp = fdoc.righecrp;
                    Intent i = new Intent(FragmentDocumentoCarrello.this.getActivity().getApplicationContext(), FormRigaArt.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putInt("tipodoc", fdoc.fcli.sptipodoc.getSelectedItemPosition() - 1);
                    mBundle.putString("clicod", fdoc.clicod);
                    mBundle.putDouble("clisc1", Formattazione.estraiDouble(fdoc.fcli.edsc1.getText().toString().replace(".", ",")));
                    mBundle.putDouble("clisc2", Formattazione.estraiDouble(fdoc.fcli.edsc2.getText().toString().replace(".", ",")));
                    mBundle.putDouble("clisc3", Formattazione.estraiDouble(fdoc.fcli.edsc3.getText().toString().replace(".", ",")));
                    mBundle.putInt("caucorr", fdoc.caucorr);
                    mBundle.putString("artcod", rec.leggiStringa("rmartcod"));
                    mBundle.putString("lotto", rec.leggiStringa("rmlotto"));
                    i.putExtras(mBundle);
                    FragmentDocumentoCarrello.this.startActivityForResult(i, 2);
                }
            }
        } else {
            return false;
        }
        return true;
    }
}
