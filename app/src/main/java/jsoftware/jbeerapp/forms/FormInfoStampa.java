package jsoftware.jbeerapp.forms;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sewoo.request.android.RequestHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;

public class FormInfoStampa extends AppCompatActivity {
    private TextView tvinfo;
    private Button bann;
    private ProgressBar wait = null;
    private boolean stampanteOk = false;
    ProcessoConnessioneStampanteWIFI procWifi = null;
    ProcessoConnessioneStampanteBT procBT = null;
    private ArrayList<String> vdevdisc = new ArrayList();

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_info_stampa);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("titolo")) {
            String tit = getIntent().getExtras().getString("titolo");
            setTitle(tit);
        }

        wait = (ProgressBar) findViewById(R.id.infostampa_wait);
        tvinfo = (TextView) findViewById(R.id.infostampa_label_info);
        bann = (Button) findViewById(R.id.infostampa_buttonAnnulla);

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("annullabile")) {
                boolean annull = getIntent().getExtras().getBoolean("annullabile");
                if (!annull)
                    bann.setVisibility(View.INVISIBLE);
            } else {
                if (Env.noannstampe)
                    bann.setVisibility(View.INVISIBLE);
            }
        } else {
            if (Env.noannstampe)
                bann.setVisibility(View.INVISIBLE);
        }


        bann.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Env.tipostampante == 0)
                    procWifi.cancel(true);
                else
                    procBT.cancel(true);
            }
        });


        wait.setVisibility(View.VISIBLE);
        if (Env.tipostampante == 0) {
            procWifi = new ProcessoConnessioneStampanteWIFI();
            procWifi.execute();
        } else {
            //IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            //registerReceiver(mBtReceiver, filter);

            procBT = new ProcessoConnessioneStampanteBT();
            procBT.execute();
        }
    }

    private final BroadcastReceiver mBtReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                vdevdisc.add(device.getName());
            }
        }
    };

    private class ProcessoConnessioneStampanteWIFI extends AsyncTask {
        @Override
        protected Object doInBackground(Object... arg0) {
            try {
                if (!FunzioniJBeerApp.tipoConnessione(getApplicationContext()).equals("WIFI")) {
                    HashMap h = new HashMap();
                    h.put("label", "Attivazione WI-FI in corso...");
                    this.publishProgress(h);
                    // attivazione wifi e connessione a rete SSID stampante
                    FunzioniJBeerApp.attivaWiFi(getApplicationContext());
                    while (!FunzioniJBeerApp.tipoConnessione(getApplicationContext()).equals("WIFI")) {
                        Thread.currentThread().sleep(1000);
                        Log.v("jazztv", "attesa attivazione wifi");
                        if (this.isCancelled()) {
                            return null;
                        }
                    }
                }
                if (this.isCancelled()) {
                    return null;
                }
                if (!FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext()).equals(Env.ssidstampante)) {
                    HashMap h = new HashMap();
                    h.put("label", "Collegamento a rete WI-FI stampante in corso...");
                    this.publishProgress(h);
                    FunzioniJBeerApp.connettiReteWiFi(getApplicationContext(), Env.ssidstampante);
                    String rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                    Log.v("jazztv", "connesso a rete:" + rc);
                    while (!rc.equals(Env.ssidstampante)) {
                        //Thread.currentThread().sleep(20000);
                        rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                        Log.v("jazztv", "connesso a rete:" + rc);
                        Log.v("jazztv", "attesa connessione wifi a rete stampante");
                        if (this.isCancelled()) {
                            return null;
                        }
                    }
                    //Thread.currentThread().sleep(5000);
                }
                if (this.isCancelled()) {
                    return null;
                }

                HashMap h = new HashMap();
                h.put("label", "Connessione a stampante in corso...");
                this.publishProgress(h);
                int ntent = 1;
                boolean isconn = Env.wifiPort.isConnected();
                Log.v("jazztv", "wifiport connessa:" + isconn);
                while (!isconn) {
                    Log.v("jazztv", "wifiport connessione tentativo " + ntent);
                    //Env.wifiPort.connect(Env.ipstampante, 9100, 3000);
                    try {
                        Env.wifiPort.connect(Env.ipstampante, 9100, 10000);
                    } catch (Exception econn) {
                        econn.printStackTrace();
                        if (!FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext()).equals(Env.ssidstampante)) {
                            h = new HashMap();
                            h.put("label", "Collegamento a rete WI-FI stampante in corso...");
                            this.publishProgress(h);
                            FunzioniJBeerApp.connettiReteWiFi(getApplicationContext(), Env.ssidstampante);
                            String rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                            Log.v("jazztv", "connesso a rete:" + rc);
                            while (!rc.equals(Env.ssidstampante)) {
                                //Thread.currentThread().sleep(20000);
                                rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                                Log.v("jazztv", "connesso a rete:" + rc);
                                Log.v("jazztv", "attesa connessione wifi a rete stampante");
                            }
                            //Thread.currentThread().sleep(5000);
                        }
                    }
                    ntent++;
                    isconn = Env.wifiPort.isConnected();
                    Log.v("jazztv", "wifiport connessa:" + isconn);
                    if (this.isCancelled()) {
                        return null;
                    }
                }
                if (this.isCancelled()) {
                    return null;
                }
                h = new HashMap();
                h.put("label", "Inizializzazione stampa...");
                this.publishProgress(h);
                while ((Env.hThread != null) && (Env.hThread.isAlive())) {
                    Log.v("jazztv", "interruzione thread requestHandler");
                    Env.hThread.interrupt();
                    //Env.hThread = null;
                }
                RequestHandler rh = new RequestHandler();
                Env.hThread = new Thread(rh);
                Env.hThread.start();
                Thread.currentThread().sleep(1000);
                boolean ok = false;
                while (!ok) {
                    if (this.isCancelled()) {
                        return null;
                    }
                    String stato = FunzioniJBeerApp.controlloStampante(Env.cpclPrinter);
                    Log.v("jazztv", "stato stampante: " + stato);
                    if (stato.contains("Normal")) {
                        h = new HashMap();
                        h.put("label", "Stampante in linea");
                        this.publishProgress(h);
                        ok = true;
                        stampanteOk = true;
                    } else {
                        if (stato.contains("Cover")) {
                            h = new HashMap();
                            h.put("label", "Coperchio aperto, chiudere coperchio");
                            this.publishProgress(h);
                        } else if (stato.contains("Paper")) {
                            h = new HashMap();
                            h.put("label", "Carta terminata, sostituire rotolino");
                            this.publishProgress(h);
                        } else if (stato.contains("Battery")) {
                            h = new HashMap();
                            h.put("label", "Batteria esaurita, ricaricare batteria stampante");
                            this.publishProgress(h);
                        }
                    }
                }
/*
                Log.v("jazztv", "Stampa in corso...");
                try
                {
                    Env.cpclPrinter.setForm(0, 200, 200, 200, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    for (int i = 0; i < 10; i++)
                        Env.cpclPrinter.printCPCLText(0, 0, 1, 1, i * 20, "IO STAMPO RIGA " + (i + 1), 0);
                    //cpclPrinter.printCPCLText(0, 0, 1, 1, 40, "FONT 1", 0);
                    Env.cpclPrinter.printForm();
                }
                catch (Exception eprint)
                {
                    eprint.printStackTrace();
                }

                //hThread.interrupt();
                try
                {
                    Env.wifiPort.disconnect();
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                Log.v("jazztv", "Stampa terminata");
*/
                if (this.isCancelled()) {
                    return null;
                }
            } catch (Exception econn) {
                econn.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Uri sris = Uri.parse("content://avviostampa/ERR");
            Intent res = new Intent(Intent.ACTION_PICK, sris);
            setResult(RESULT_CANCELED, res);
            finish();
        }

        @Override
        protected void onPostExecute(Object result) {
            wait.setVisibility(View.INVISIBLE);
            if (stampanteOk) {
                Uri sris = Uri.parse("content://avviostampa/OK");
                Intent res = new Intent(Intent.ACTION_PICK, sris);
                setResult(RESULT_OK, res);
                finish();
            } else {
                Uri sris = Uri.parse("content://avviostampa/ERR");
                Intent res = new Intent(Intent.ACTION_PICK, sris);
                setResult(RESULT_CANCELED, res);
                finish();
            }
        }

        @Override
        protected void onProgressUpdate(Object[] values) {
            tvinfo.setText((String) ((HashMap) values[0]).get("label"));
        }
    }

    private class ProcessoConnessioneStampanteBT extends AsyncTask {
        @Override
        protected Object doInBackground(Object... arg0) {
            try {
                // controllo bt attivato
                BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
                while (!btAdapter.isEnabled()) {
                    HashMap h = new HashMap();
                    h.put("label", "Attivazione Bluetooth in corso...");
                    this.publishProgress(h);
                    // attivazione bluetooth
                    btAdapter.enable();
                    if (this.isCancelled()) {
                        return null;
                    }
                }

                if (this.isCancelled()) {
                    return null;
                }

                //btAdapter.startDiscovery();
                //while (btAdapter.isDiscovering())
                //{
                //    System.out.println("discovering");
                //}
                //unregisterReceiver(mBtReceiver);
                boolean dispok = false;
                BluetoothDevice devStampante = null;
                while (!dispok) {
                    System.out.println("elenco dispositivi BT:");
                    Set<BluetoothDevice> vbt = btAdapter.getBondedDevices();
                    Iterator<BluetoothDevice> it = vbt.iterator();
                    while (it.hasNext()) {
                        BluetoothDevice btd = it.next();
                        System.out.println("nome:" + btd.getName() + " ind:" + btd.getAddress());
                        //if ((btd.getName().equals(Env.nomebtstampante)) && vdevdisc.contains(Env.nomebtstampante))
                        if ((btd.getName().equals(Env.nomebtstampante)))
                            devStampante = btd;
                    }
                    if (devStampante != null) {
                        dispok = true;
                    }
                }
                HashMap h = new HashMap();
                h.put("label", "Connessione Bluetooth a stampante in corso...");
                this.publishProgress(h);
                int ntent = 1;
                boolean isconn = Env.btPort.isConnected();
                Log.v("jazztv", "btport connessa:" + isconn);
                while (!isconn) {
                    Log.v("jazztv", "btport connessione tentativo " + ntent);
                    try {
                        Env.btPort.connect(devStampante);
                    } catch (Exception econn) {
                        econn.printStackTrace();
                    }
                    ntent++;
                    isconn = Env.btPort.isConnected();
                    Log.v("jazztv", "btport connessa:" + isconn);
                    if (this.isCancelled()) {
                        return null;
                    }
                }
                if (this.isCancelled()) {
                    return null;
                }
                h = new HashMap();
                h.put("label", "Inizializzazione stampa...");
                this.publishProgress(h);
                while ((Env.hThread != null) && (Env.hThread.isAlive())) {
                    Log.v("jazztv", "interruzione thread requestHandler");
                    Env.hThread.interrupt();
                    //Env.hThread = null;
                }
                RequestHandler rh = new RequestHandler();
                Env.hThread = new Thread(rh);
                Env.hThread.start();
                Thread.currentThread().sleep(1000);
                boolean ok = false;
                while (!ok) {
                    if (this.isCancelled()) {
                        return null;
                    }
                    String stato = FunzioniJBeerApp.controlloStampante(Env.cpclPrinter);
                    Log.v("jazztv", "stato stampante: " + stato);
                    if (stato.contains("Normal") || stato.contains("Battery")) {
                        h = new HashMap();
                        h.put("label", "Stampante in linea");
                        this.publishProgress(h);
                        ok = true;
                        stampanteOk = true;
                    } else {
                        if (stato.contains("Cover")) {
                            h = new HashMap();
                            h.put("label", "Coperchio aperto, chiudere coperchio");
                            this.publishProgress(h);
                        } else if (stato.contains("Paper")) {
                            h = new HashMap();
                            h.put("label", "Carta terminata, sostituire rotolino");
                            this.publishProgress(h);
                        }
//                        else if (stato.contains("Battery"))
//                        {
//                            h = new HashMap();
//                            h.put("label", "Batteria esaurita, ricaricare batteria stampante");
//                            this.publishProgress(h);
//                        }
                    }
                }
/*
                Log.v("jazztv", "Stampa in corso...");
                try
                {
                    Env.cpclPrinter.setForm(0, 200, 200, 200, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    for (int i = 0; i < 10; i++)
                        Env.cpclPrinter.printCPCLText(0, 0, 1, 1, i * 20, "IO STAMPO RIGA " + (i + 1), 0);
                    //cpclPrinter.printCPCLText(0, 0, 1, 1, 40, "FONT 1", 0);
                    Env.cpclPrinter.printForm();
                }
                catch (Exception eprint)
                {
                    eprint.printStackTrace();
                }

                //hThread.interrupt();
                try
                {
                    Env.wifiPort.disconnect();
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                Log.v("jazztv", "Stampa terminata");
*/
                if (this.isCancelled()) {
                    return null;
                }
            } catch (Exception econn) {
                econn.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Uri sris = Uri.parse("content://avviostampa/ERR");
            Intent res = new Intent(Intent.ACTION_PICK, sris);
            setResult(RESULT_CANCELED, res);
            finish();
        }

        @Override
        protected void onPostExecute(Object result) {
            wait.setVisibility(View.INVISIBLE);
            if (stampanteOk) {
                Uri sris = Uri.parse("content://avviostampa/OK");
                Intent res = new Intent(Intent.ACTION_PICK, sris);
                setResult(RESULT_OK, res);
                finish();
            } else {
                Uri sris = Uri.parse("content://avviostampa/ERR");
                Intent res = new Intent(Intent.ACTION_PICK, sris);
                setResult(RESULT_CANCELED, res);
                finish();
            }
        }

        @Override
        protected void onProgressUpdate(Object[] values) {
            tvinfo.setText((String) ((HashMap) values[0]).get("label"));
        }
    }

}
