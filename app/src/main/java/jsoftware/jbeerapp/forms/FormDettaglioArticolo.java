package jsoftware.jbeerapp.forms;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Record;

public class FormDettaglioArticolo extends AppCompatActivity {


    private TextView nome_articolo;
    private TextView prezzo_articolo;
    private TextView giacenza_articolo;
    private TextView stile_articolo;
    private TextView colore_articolo;
    private TextView grado_articolo;
    private TextView formato_articolo;
    private ImageView img_art;
    private EditText note_articolo;
    private String codice_articolo;
    private String n_articolo;
    private String p_articolo;
    private String g_articolo;
    private String s_articolo;
    private String c_articolo;
    private String gr_articolo;
    private String f_articolo;
    private String no_articolo;
    private ArrayList<Record> vart;
    private File fimg;
    private ImageButton b;
    private String img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_dettaglio_articolo);

        nome_articolo = findViewById(R.id.nome_art);
        prezzo_articolo = findViewById(R.id.prezzo_art);
        giacenza_articolo = findViewById(R.id.giac_art);
        stile_articolo = findViewById(R.id.stile_art);
        colore_articolo = findViewById(R.id.colore_art);
        grado_articolo = findViewById(R.id.grado_art);
        formato_articolo = findViewById(R.id.formato_art);
        img_art = findViewById(R.id.img);
        note_articolo = findViewById(R.id.note_art);
        b = findViewById(R.id.imageButton);


        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("artcod"))
            codice_articolo = getIntent().getExtras().getString("artcod");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("giacArt"))
            g_articolo = getIntent().getExtras().getString("giacArt");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("catnomeart"))
            n_articolo = getIntent().getExtras().getString("catnomeart");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("catstile"))
            s_articolo = getIntent().getExtras().getString("catstile");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("catcolore"))
            c_articolo = getIntent().getExtras().getString("catcolore");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("catgrado"))
            gr_articolo = getIntent().getExtras().getString("catgrado");


        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("catnote"))
            no_articolo = getIntent().getExtras().getString("catnote");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cauzdescr"))
            f_articolo = getIntent().getExtras().getString("cauzdescr");


        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("prz"))
            p_articolo = getIntent().getExtras().getString("prz");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("nomeimg"))
            img = getIntent().getExtras().getString("nomeimg");
        fimg = this.getBaseContext().getFileStreamPath(img);


        //leggiArticolo();
        visualizzaTextView();


        b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneShare();
            }
        });

    }

    private void leggiArticolo() {
        codice_articolo = codice_articolo.replaceAll("'", "\'");
        vart = new ArrayList<Record>();
        String qry = "SELECT catnomeart,catstile,catcolore,catgrado,giacqta,catnote,cauzdescr " +
                "FROM catalogo INNER JOIN articoli ON catartcod=artcod LEFT JOIN giacart ON artcod=giacartcod LEFT JOIN cauzioni ON artcauzcod=cauzcod " +
                "WHERE artcod = '" + codice_articolo + "'";
        Cursor cursor = Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            Record r = new Record();
            n_articolo = cursor.getString(0);
            s_articolo = cursor.getString(1);
            c_articolo = cursor.getString(2);
            gr_articolo = cursor.getString(3);
            //g_articolo = cursor.getString(4);
            no_articolo = cursor.getString(5);
            f_articolo = cursor.getString(6);
            String qry1 = "SELECT lprezzo1 FROM listini WHERE lartcod='" + codice_articolo + "' ORDER BY ldataval DESC LIMIT 1";
            Cursor cursor1 = Env.db.rawQuery(qry1, null);
            while (cursor1.moveToNext()) {
                p_articolo = Double.toString(cursor1.getDouble(0));
            }
            cursor1.close();
            String qry2 = "SELECT ifileimg FROM imgart WHERE iartcod='" + codice_articolo + "'";
            Cursor cursor2 = Env.db.rawQuery(qry2, null);
            img = "";
            while (cursor2.moveToNext()) {
                img = cursor2.getString(0);
            }
            cursor2.close();
            fimg = this.getBaseContext().getFileStreamPath(img);
        }
        cursor.close();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void visualizzaTextView() {

        if (n_articolo != null) {
            nome_articolo.setText(n_articolo);
        }
        if (p_articolo != null) {
            prezzo_articolo.setText(p_articolo);
        }
        if (g_articolo != null) {
            giacenza_articolo.setText("Disponibilità: " + g_articolo);
        } else {
            giacenza_articolo.setText("Non disponibile");
        }
        if (s_articolo != null) {
            stile_articolo.setText("Stile: " + s_articolo);
        }
        if (c_articolo != null) {
            colore_articolo.setText("Colore: " + c_articolo);
        }
        if (f_articolo != null) {
            formato_articolo.setText("Formato: " + f_articolo);
        }
        if (gr_articolo != null) {
            grado_articolo.setText("Grado: " + gr_articolo);
        }
        if (no_articolo != null) {
            note_articolo.setText(no_articolo);
        }
        if (fimg.exists()) {
            try {
                Bitmap myBitmap = BitmapFactory.decodeFile(fimg.getAbsolutePath());
                img_art.setImageBitmap(myBitmap);
            } catch (Exception eimg) {
                eimg.printStackTrace();
            }
        }
    }

    private void azioneShare() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);

        sharingIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        if (fimg.isFile()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(fimg.getAbsolutePath());
            String pathofBmp =
                    MediaStore.Images.Media.insertImage(getContentResolver(),
                            myBitmap, "title", null);
            Uri imgUri = Uri.parse(pathofBmp);
            sharingIntent.putExtra(Intent.EXTRA_TEXT, n_articolo);
            //sharingIntent.putExtra(Intent.EXTRA_TEXT, n_articolo);
            sharingIntent.putExtra(Intent.EXTRA_STREAM, imgUri);
            sharingIntent.setType("image/*");
            sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(sharingIntent, "Share image using"));
        } else {
            Toast toast = Toast.makeText(FormDettaglioArticolo.this.getApplicationContext(), "Immagine non presente!",
                    Toast.LENGTH_SHORT);
            toast.show();
            return;
        }

        //startActivity(emailIntent);
        //startActivity(Intent.createChooser(emailIntent,"Condividi con"));
    }

}
