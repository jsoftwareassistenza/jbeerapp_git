package jsoftware.jbeerapp.forms;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import jsoftware.jbeerapp.R;


public class FormStatisticheGenerali extends AppCompatActivity implements FragmentStatFiltri.OnFragmentInteractionListener,
        FragmentStatRiepilogo.OnFragmentInteractionListener, FragmentStatArticoli.OnFragmentInteractionListener, FragmentStatClienti.OnFragmentInteractionListener,
        FragmentStatClassi.OnFragmentInteractionListener, FragmentStatInfo.OnFragmentInteractionListener {
    private Fragment fragmentAttuale = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_statistiche_generali);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragmentStatInfo fr = new FragmentStatInfo();
        fragmentTransaction.add(R.id.stat_frag, fr);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
        fragmentAttuale = fr;
    }

    @Override
    public void onFragmentInteraction(String azione) {
        if (azione.startsWith("STATFILTRI")) {
            String datainizio = "", datafine = "";
            String[] tmp = azione.split("\\|", -1);
            if (tmp.length == 4) {
                datainizio = tmp[2];
                datafine = tmp[3];
                if (azione.contains("|RIEP")) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    FragmentStatRiepilogo fr = new FragmentStatRiepilogo();
                    fr.impostaFiltri(datainizio, datafine);
                    if (fragmentAttuale != null)
                        fragmentTransaction.remove(fragmentAttuale);
                    fragmentTransaction.add(R.id.stat_frag, fr);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    fragmentTransaction.commit();
                    fragmentAttuale = fr;
                } else if (azione.contains("|ART")) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    FragmentStatArticoli fr = new FragmentStatArticoli();
                    fr.impostaFiltri(datainizio, datafine, "");
                    if (fragmentAttuale != null)
                        fragmentTransaction.remove(fragmentAttuale);
                    fragmentTransaction.add(R.id.stat_frag, fr);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    fragmentTransaction.commit();
                    fragmentAttuale = fr;
                } else if (azione.contains("|CLI")) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    FragmentStatClienti fr = new FragmentStatClienti();
                    fr.impostaFiltri(datainizio, datafine);
                    if (fragmentAttuale != null)
                        fragmentTransaction.remove(fragmentAttuale);
                    fragmentTransaction.add(R.id.stat_frag, fr);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    fragmentTransaction.commit();
                    fragmentAttuale = fr;
                } else if (azione.contains("|CLASSI")) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    FragmentStatClassi fr = new FragmentStatClassi();
                    fr.impostaFiltri(datainizio, datafine);
                    if (fragmentAttuale != null)
                        fragmentTransaction.remove(fragmentAttuale);
                    fragmentTransaction.add(R.id.stat_frag, fr);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    fragmentTransaction.commit();
                    fragmentAttuale = fr;
                }
            }
        }
    }


}
