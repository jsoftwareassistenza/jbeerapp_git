package jsoftware.jbeerapp.forms;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.sewoo.jpos.printer.CPCLPrinter;
import com.sewoo.port.android.BluetoothPort;
import com.sewoo.port.android.WiFiPort;

import org.jsoup.Jsoup;

import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.CheckForUpdate;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.JBeerAppDatabaseHelper;
import jsoftware.jbeerapp.env.PreferenceUtils;

import static jsoftware.jbeerapp.env.Env.db;

public class MainActivity extends AppCompatActivity {


    Button buttonlogin = null;
    EditText pin;
    Connection c = null;
    private boolean inizio;
    private int count = 0;
    private AppUpdateManager appUpdateManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        buttonlogin = findViewById(R.id.login_ok);
        pin = findViewById(R.id.etpin);


        Env.wifiPort = WiFiPort.getInstance();
        Env.btPort = BluetoothPort.getInstance();
        inizio = true;
        Env.cpclPrinter = new CPCLPrinter();

        checkForAppUpdate();
        // inizializzazione database
        JBeerAppDatabaseHelper dbh = new JBeerAppDatabaseHelper(getApplicationContext(), "", null, Env.versionDatabase);
        Env.db = dbh.getWritableDatabase();

        controllaReinizializzazione();
        controllaJCloud();
        controllaAttivazione();

        FunzioniJBeerApp.initParametri();
        FunzioniJBeerApp.caricaParametri();

        FunzioniJBeerApp.caricaCodPag();
        FunzioniJBeerApp.caricaCodIva();
        FunzioniJBeerApp.caricaBlocchiCau();

        int permission = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            int REQUEST_EXTERNAL_STORAGE = 1;
            String[] PERMISSIONS_STORAGE = {
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };
            ActivityCompat.requestPermissions(
                    MainActivity.this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }


        buttonlogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                inizio = false;
                if (!pin.getText().toString().equals("")) {
                    if (!FunzioniJBeerApp.controllaParametriPrimoAvvio()) {
                        Intent i = new Intent(getApplicationContext(), FormPrimaConf.class);
                        MainActivity.this.startActivity(i);
                    } else {
                        if (!FunzioniJBeerApp.terminaleConfigurato()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                    MainActivity.this);
                            builder.setMessage("Dati non aggiornati, effettuare l'aggiornamento")
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int id) {

                                                    Intent i = new Intent(getApplicationContext(), FormRicezioneDati.class);
                                                    MainActivity.this.startActivity(i);
                                                }
                                            });
                            AlertDialog ad = builder.create();
                            ad.setCancelable(false);
                            ad.show();
                        } else {
                            if (FunzioniJBeerApp.controllaPin(pin.getText().toString())) {
                                c = new MainActivity.Connection();
                                c.key = FunzioniJBeerApp.leggiKey();
                                c.execute();
                                Toast toast = Toast.makeText(getApplicationContext(), "Pin Corretto",
                                        Toast.LENGTH_SHORT);
                                toast.show();
                                pin.setText("");
                            } else {
                                count++;
                                Toast toast = Toast.makeText(getApplicationContext(), "Pin Errato",
                                        Toast.LENGTH_SHORT);
                                toast.show();
                                pin.setText("");
                                if (count > 2) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(
                                            MainActivity.this);
                                    builder.setMessage("Attenzione!\nPin dimenticato?\nVuoi ripristinarlo?")
                                            .setPositiveButton("SI",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog,
                                                                            int id) {

                                                            Intent i = new Intent(getApplicationContext(), FormRipristinoPin.class);
                                                            Bundle mBundle = new Bundle();
                                                            mBundle.putBoolean("ripristino", true);
                                                            i.putExtras(mBundle);
                                                            MainActivity.this.startActivityForResult(i, 1);
                                                        }
                                                    })
                                            .setNegativeButton("NO",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog,
                                                                            int id) {
                                                        }
                                                    });
                                    AlertDialog ad = builder.create();
                                    ad.setCancelable(false);
                                    ad.show();

                                }
                            }
                        }
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            MainActivity.this);
                    builder.setMessage("Inserire il Pin")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {


                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }

            }
        });
    }

    public void controllaAttivazione() {
        if (!FunzioniJBeerApp.controllaParametriPrimoAvvio()) {
            Intent i = new Intent(getApplicationContext(), FormPrimaConf.class);
            MainActivity.this.startActivity(i);
        } else {
            if (!FunzioniJBeerApp.terminaleConfigurato()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        MainActivity.this);
                builder.setMessage("Terminale non configurato, effettuare aggiornamento dati")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                        Intent i = new Intent(getApplicationContext(), FormRicezioneDati.class);
                                        MainActivity.this.startActivity(i);
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
            } else {
                if (!inizio) {

                    Intent i = new Intent(getApplicationContext(), FormMenu.class);
                    MainActivity.this.startActivity(i);
                }
            }
        }

        // @Override
/*    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings)
        {
            Intent i = new Intent(MainActivity.this.getApplicationContext(), FormParametri.class);

            MainActivity.this.startActivityForResult(i, 1);


            return true;
        }
        //else if (item.getItemId() == R.id.action_utilities)
        //{
        //Intent i = new Intent(getApplicationContext(), FormUtilita.class);
        //MainActivity.this.startActivity(i);
//            return true;
        //}
        else
            return super.onOptionsItemSelected(item);
    }*/
    }

    private void controllaReinizializzazione(){
        String pedcod = "";
        String user = "";
        String pwd = "";
        String serverjcloud = "";
        String activationcode = "";
        String pin = "";
        String versionedemo = "";
        Cursor cursor = Env.db.rawQuery(
                "SELECT pedcod,user,pwd,serverjcloud,activationcode,pin,versionedemo FROM datitermreiniz",
                null);
        if (cursor.moveToFirst()){
            pedcod = cursor.getString(0);
            user = cursor.getString(1);
            pwd = cursor.getString(2);
            serverjcloud = cursor.getString(3);
            activationcode = cursor.getString(4);
            pin = cursor.getString(5);
            versionedemo = cursor.getString(6);

            String sql = "INSERT INTO datiterm (pedcod,user,pwd,serverjcloud,activationcode,pin,versionedemo) VALUES (?,?,?,?,?,?,?)";
            SQLiteStatement stmt = db
                    .compileStatement(sql);
            stmt.clearBindings();
            stmt.bindString(1, pedcod);
            stmt.bindString(2, user);
            stmt.bindString(3, pwd);
            stmt.bindString(4, Env.ipServerJCloud);
            stmt.bindString(5, activationcode);
            stmt.bindString(6, pin);
            stmt.bindString(7, versionedemo);

            stmt.execute();
            stmt.close();

            try {
                db.execSQL("DELETE FROM datitermreiniz WHERE pedcod = '" + pedcod + "'");
            } catch (Exception edb) {
                //edb.printStackTrace();
            }

        }
        cursor.close();
    }

    private class Connection extends AsyncTask {
        public String key = "";
        public HashMap risp = new HashMap();
        public int stKey = 1;

        @Override
        protected Object doInBackground(Object... arg0) {
            // test connessione
            // PAX
            //risp = FunzioniJBeerApp.testKey(key);
            //stKey = (int)risp.get("keystato");

            return null;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Object result) {

            if (stKey != 1) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        MainActivity.this);
                builder.setMessage((risp.get("message").toString()))
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.show();
            } else {
                Intent i = new Intent(getApplicationContext(), FormMenu.class);
                MainActivity.this.startActivity(i);
            }

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    String newPin = (String) data.getExtras().get("newpin");
                    if (!newPin.equals("")) {
                        String sql = "UPDATE datiterm SET pin=?";
                        SQLiteStatement stmt = Env.db
                                .compileStatement(sql);
                        stmt.clearBindings();
                        stmt.bindString(1, newPin);
                        stmt.execute();
                        stmt.close();
                    }

                    break;
                }
            }
        }
    }


    private void controllaJCloud() {

        String pedcod = "";
        String user = "";
        String pwd = "";
        String serverjcloud = "";

        Cursor cursor = Env.db.rawQuery(
                "SELECT serverjcloud FROM datiterm",
                null);
        if (cursor.moveToFirst()) {
            SQLiteStatement stpar = Env.db.compileStatement(" UPDATE datiterm SET serverjcloud = ?");
            stpar.clearBindings();
            stpar.bindString(1, Env.ipServerJCloud);
            stpar.execute();
            stpar.close();

        }
    }

    private void checkForAppUpdate() {

        appUpdateManager = AppUpdateManagerFactory.create(this);
// Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

// Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // This example applies an immediate update. To apply a flexible update
                    // instead, pass in AppUpdateType.FLEXIBLE
                    //IMMEDIATE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                // Request the update.

/*                    appUpdateManager.startUpdateFlowForResult(
                            // Pass the intent that is returned by 'getAppUpdateInfo()'.
                            appUpdateInfo,
                            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                            AppUpdateType.IMMEDIATE,
                            // The current activity making the update request.
                            this,
                            // Include a request code to later monitor this update request.
                            MY_REQUEST_CODE);*/

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setTitle("Nuovo aggiornamento");
                alertDialog.setMessage("E' presente una nuova versione.\nAggiorna la tua app dal Play Store.");
                alertDialog.setPositiveButton("Aggiorna Adesso", (dialogInterface, i) -> {
                    CheckForUpdate.launchPlayStoreApp(this);
                    //Log.i("app update service","app is needed to update");
                    PreferenceUtils preferenceUtils = new PreferenceUtils(this);
                    int remoteVersionCode = CheckForUpdate.getRemoteVersionNumber(this);
                    preferenceUtils.createAppVersionCode(remoteVersionCode);
                });
                alertDialog.setNegativeButton("Dopo", (dialogInterface, i) -> {
                });
                alertDialog.show();

/*                    appUpdateManager.startUpdateFlowForResult(appUpdateInfo,this,
                            AppUpdateOptions.defaultOptions(AppUpdateType.IMMEDIATE), MY_REQUEST_CODE);*/

            }
        });
    }
}
