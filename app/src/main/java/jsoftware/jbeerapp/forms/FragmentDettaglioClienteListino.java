package jsoftware.jbeerapp.forms;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaArticoliListinoAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Record;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentDettaglioClienteListino.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDettaglioClienteListino#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDettaglioClienteListino extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FormDettaglioCliente fdcli = null;
    private ListView listaArticoli;
    private ArrayList<Record> varticoli = new ArrayList<Record>();
    private ListaArticoliListinoAdapter lartadapter;


    public FragmentDettaglioClienteListino() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentDocumentoRiepilogo.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDettaglioClienteListino newInstance(String param1, String param2) {
        FragmentDettaglioClienteListino fragment = new FragmentDettaglioClienteListino();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.v("JAZZTV", "ONCREATEVIEW RIEP");
        return inflater.inflate(R.layout.fragment_fragment_dettaglio_cliente_listino, container, false);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
/*
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        } else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        listaArticoli = this.getActivity().findViewById(R.id.detcli_listaart);
        // vrubrica = new ArrayList<>();
        registerForContextMenu(listaArticoli);


/*        View header = this.getActivity().getLayoutInflater().inflate(R.layout.lvlisart_header, null);
        listaArticoli.addHeaderView(header);*/
        varticoli = new ArrayList<Record>();
        lartadapter = new ListaArticoliListinoAdapter(this.getActivity().getApplicationContext(), varticoli);
        listaArticoli.setAdapter(lartadapter);

        listaArtListini();

    }

    /*    @Override
        public void onResume() {
            // TODO Auto-generated method stub
    *//*        super.onResume();
        fdoc.nfrag++;
        if (fdoc.nfrag == 3)
            fdoc.inizializza();*//*
    }*/
    public void listaArtListini() {
        varticoli.clear();
        boolean record = false;
        String dtoggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
        String[] par = new String[2];
        par[0] = dtoggi;
        par[1] = fdcli.clicodice;
        Cursor cursor = Env.db.rawQuery("SELECT lprezzo1,ldataval,artdescr FROM listini INNER JOIN articoli ON listini.lartcod = articoli.artcod " +
                " INNER JOIN clienti ON clienti.clicodlist=listini.lcod WHERE ldataval <= ? AND clicodice = ? ORDER BY ldataval DESC", par);
        while (cursor.moveToNext()) {
            record = true;
            Record r = new Record();
            r.insDouble("przlistino", cursor.getDouble(0));
            r.insStringa("dataval", cursor.getString(1));
            r.insStringa("artdescr", cursor.getString(2));
            varticoli.add(r);
        }
        if (!record) {
            String[] par2 = new String[1];
            par2[0] = dtoggi;
            cursor = Env.db.rawQuery("SELECT lprezzo1,ldataval,artdescr FROM listini INNER JOIN articoli ON listini.lartcod = articoli.artcod " +
                    " INNER JOIN catalogo ON catalogo.catlistcod=listini.lcod WHERE ldataval <= ? ORDER BY ldataval DESC", par2);
            while (cursor.moveToNext()) {

                Record r = new Record();
                r.insDouble("przlistino", cursor.getDouble(0));
                r.insStringa("dataval", cursor.getString(1));
                r.insStringa("artdescr", cursor.getString(2));
                varticoli.add(r);
            }
        }
        cursor.close();
        lartadapter.notifyDataSetChanged();
    }
}
