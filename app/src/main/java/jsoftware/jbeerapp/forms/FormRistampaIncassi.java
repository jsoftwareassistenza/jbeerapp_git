package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sewoo.jpos.command.CPCLConst;

import java.io.File;
import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaRistampaIncAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.env.funzStringa;

public class FormRistampaIncassi extends AppCompatActivity {
    private ListView lista;
    private ListaRistampaIncAdapter ladapter;
    private ArrayList<Record> vinc;
    private int pos = -1;
    private String clicod = "";
    private String ccod = "";
    private String cnome = "";
    private String cind = "";
    private String civa = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_ristampa_incassi);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicod"))
            clicod = getIntent().getExtras().getString("clicod");

        lista = findViewById(R.id.ristampainc_lista);
        View header = this.getLayoutInflater().inflate(R.layout.ristampainc_lista_header, null);
        lista.addHeaderView(header);

/*
        String[] pars = new String[1];
        pars[0] = clicod;
        Cursor cursor = Env.db.rawQuery(
                "SELECT clicodice,clinome,cliindir," +
                        "cliloc,cliprov,clipiva FROM clienti WHERE clicodice = ?", pars);
        cursor.moveToFirst();
        ccod = cursor.getString(0);
        cnome = cursor.getString(1);
        cind = cursor.getString(2) + " " + cursor.getString(3) + " " + cursor.getString(4);
        civa = cursor.getString(5);
        cursor.close();

        tvcli.setText(ccod + "-" + cnome);
*/

        vinc = new ArrayList<Record>();
        Cursor cursor = Env.db.rawQuery("SELECT sisctipo,siscsezdoc,siscdatadoc,siscnumdoc,sisctipoop," +
                "sisaldo,siscdatainc,siscorainc,siscresiduoscad,clicodice,clinome FROM storico_incassi INNER JOIN clienti ON siscclicod = clicodice " +
                (!clicod.equals("") ? " WHERE siscclicod = '" + clicod + "'" : "") +
                "ORDER BY siscdatainc DESC,siscorainc DESC", null);
        while (cursor.moveToNext()) {
            Record rx = new Record();
            rx.insStringa("clicod", cursor.getString(9));
            rx.insStringa("cliente", cursor.getString(9) + "-" + cursor.getString(10));
            String doc = "";
            if (cursor.getString(0).equals("F"))
                doc += "Fattura ";
            else if (cursor.getString(0).equals("B"))
                doc += "DDT ";
            else if (cursor.getString(0).equals("C"))
                doc += "Credito generico ";
            else if (cursor.getString(0).equals("N"))
                doc += "Nota credito ";
            if (cursor.getInt(3) > 0)
                doc += cursor.getString(1) + "/" + cursor.getInt(3);
            if (!cursor.getString(2).equals("") && !cursor.getString(2).equals("0000-00-00"))
                doc += " del " + (new Data()).formatta(Data.GG_MM_AAAA, "/");
            rx.insStringa("tipo", cursor.getString(0));
            rx.insStringa("sez", cursor.getString(1));
            rx.insStringa("data", cursor.getString(2));
            rx.insIntero("num", cursor.getInt(3));
            rx.insStringa("doc", doc);
            rx.insDouble("residuo", cursor.getDouble(8));
            rx.insDouble("saldo", cursor.getDouble(5));
            rx.insStringa("datainc", cursor.getString(6));
            rx.insStringa("orainc", cursor.getString(7));
            vinc.add(rx);
        }
        cursor.close();
        ladapter = new ListaRistampaIncAdapter(this.getApplicationContext(), vinc, this);
        lista.setAdapter(ladapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (position > 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRistampaIncassi.this);
                    builder.setMessage("Confermi ristampa?")
                            .setPositiveButton("SI",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            Record rec = vinc.get(position - 1);
                                            if (Env.tipostampante == 0 && (Env.ssidstampante.equals("") || Env.ipstampante.equals(""))) {
                                                AlertDialog.Builder builder = new AlertDialog.Builder(
                                                        FormRistampaIncassi.this);
                                                builder.setMessage("Stampante WI-FI non configurata")
                                                        .setPositiveButton("Ok",
                                                                new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog,
                                                                                        int id) {
                                                                        finish();
                                                                    }
                                                                });
                                                AlertDialog ad = builder.create();
                                                ad.setCancelable(false);
                                                ad.show();
                                                return;
                                            } else if ((Env.tipostampante == 1 || Env.tipostampante == 2) && Env.nomebtstampante.equals("")) {
                                                AlertDialog.Builder builder = new AlertDialog.Builder(
                                                        FormRistampaIncassi.this);
                                                builder.setMessage("Stampante Bluetooth non configurata")
                                                        .setPositiveButton("Ok",
                                                                new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog,
                                                                                        int id) {
                                                                        finish();
                                                                    }
                                                                });
                                                AlertDialog ad = builder.create();
                                                ad.setCancelable(false);
                                                ad.show();
                                                return;
                                            } else {
                                                pos = position - 1;
                                                Intent i = new Intent(FormRistampaIncassi.this.getApplicationContext(), FormInfoStampa.class);
                                                Bundle mBundle = new Bundle();
                                                mBundle.putString("titolo", "Ristampa Incasso");
                                                i.putExtras(mBundle);
                                                startActivityForResult(i, 1);
                                            }
                                        }
                                    })
                            .setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                // stampa incasso
                if (resultCode == Activity.RESULT_OK) {
                    Record rec = vinc.get(pos);
                    stampaIncasso(getApplicationContext(), rec);
                    try {
                        Env.wifiPort.disconnect();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    try {
                        Env.btPort.disconnect();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            }
        }
    }

    private void stampaIncasso(Context context, Record rec) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;
        try {
            double totsaldo = 0;
            ArrayList<Record> vrighe = new ArrayList();
            String[] parsinc = new String[7];
            parsinc[0] = rec.leggiStringa("tipo");
            parsinc[1] = rec.leggiStringa("sez");
            parsinc[2] = rec.leggiStringa("data");
            parsinc[3] = "" + rec.leggiIntero("num");
            parsinc[4] = rec.leggiStringa("clicod");
            parsinc[5] = rec.leggiStringa("datainc");
            parsinc[6] = rec.leggiStringa("orainc");
            Cursor c = Env.db.rawQuery("SELECT sisctipo,siscsezdoc,siscdatadoc,siscnumdoc,sisctipoop,siscclicod,siscscadid,siscdatascad,sisctiposcad,siscresiduoscad,sisaldo,siscdescr,siscdatainc,siscorainc FROM storico_incassi WHERE sisctipo = ? AND siscsezdoc = ? AND siscdatadoc = ? " +
                    " AND siscnumdoc = ? AND siscclicod = ? AND siscdatainc = ? AND siscorainc = ?", parsinc);
            c.moveToNext();
            Record rx = new Record();
            if (c.getString(0).equals("F"))
                rx.insStringa("tipo", "Fat");
            else if (c.getString(0).equals("B"))
                rx.insStringa("tipo", "DDT");
            else if (c.getString(0).equals("C"))
                rx.insStringa("tipo", "CGe");
            else if (c.getString(0).equals("N"))
                rx.insStringa("tipo", "Ncr");
            if (!c.getString(0).equals("C"))
                rx.insStringa("rifdoc", c.getString(1) + "/" + c.getInt(3));
            else
                rx.insStringa("rifdoc", "");
            if (!c.getString(0).equals("C")) {
                if (!c.getString(2).equals("0000-00-00") && !c.getString(2).equals(""))
                    rx.insStringa("datadoc", (new Data(c.getString(2), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/"));
                if (!c.getString(7).equals("0000-00-00") && !c.getString(7).equals(""))
                    rx.insStringa("datadoc", (new Data(c.getString(7), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/"));
                else
                    rx.insStringa("datadoc", "");
            } else
                rx.insStringa("datadoc", "");

            double imp = c.getDouble(9);
            if (!c.getString(4).equals("V"))
                imp = -imp;
            rx.insStringa("importo", Formattazione.formatta(imp, "###0.00", 2));
            double residuo = 0;
            if (c.getString(0).equals("N"))
                residuo = OpValute.arrotondaMat(c.getDouble(9) - c.getDouble(10), 2);
            else
                residuo = OpValute.arrotondaMat(c.getDouble(9) - Math.abs(c.getDouble(10)), 2);
            if (!c.getString(4).equals("V"))
                residuo = -residuo;
            rx.insStringa("residuo", Formattazione.formatta(residuo, "###0.00", 2));
            rx.insStringa("saldo", Formattazione.formatta(c.getDouble(10), "###0.00", 2));
            if (!c.getString(4).equals("V"))
                totsaldo -= c.getDouble(10);
            else
                totsaldo += c.getDouble(10);
            vrighe.add(rx);
            c.close();

            ArrayList<Record> vddtc = new ArrayList();

            int nrigheform = 25 + vrighe.size() + 4;
            if (Env.stampasedesec)
                nrigheform += 4;
            File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
            if (flogo.exists())
                nrigheform += Env.nrighelogo;
            if (vddtc.size() > 0)
                nrigheform += vddtc.size() + 3;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheform * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);

            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante("RICEVUTA DI PAGAMENTO", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "RICEVUTA DI PAGAMENTO", lf, 24);
            posy += 30;
            if (flogo.exists()) {
                try {
                    Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                    Env.cpclPrinter.printBitmap(bm, 0, posy);
                } catch (Exception elogo) {
                    elogo.printStackTrace();
                }
                posy += 30 * Env.nrighelogo;
            }
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga6 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga7 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 30;
            stmp = "Data:" + (new Data(rec.leggiStringa("datainc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " ora:" + rec.leggiStringa("orainc");
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, "CLIENTE:", lf, 24);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Nome: " + cnome + " ", gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Indirizzo: " + cind + " ", gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Partita IVA: " + civa + " ", gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, "INCASSI:", lf, 24);
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " TIPO DOCUMENTO  DATA DOC IMPORTO  RIMANENZA   SALDO ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " TIPO DOCUMENTO     DATA DOC     IMPORTO      RIMANENZA       SALDO ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            for (int i = 0; i < vrighe.size(); i++) {
                rx = vrighe.get(i);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("tipo") + " ", 3);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("rifdoc") + " ", 11);
                    Env.cpclPrinter.printAndroidFont(54, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("datadoc") + " ", 8);
                    Env.cpclPrinter.printAndroidFont(185, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("importo") + " ", 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(282, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("residuo") + " ", 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(380, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("saldo") + " ", 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(478, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                } else {
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("tipo") + " ", 3);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("rifdoc") + " ", 14);
                    Env.cpclPrinter.printAndroidFont(54, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa(rx.leggiStringa("datadoc") + " ", 8);
                    Env.cpclPrinter.printAndroidFont(217, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("importo") + " ", 11, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(315, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("residuo") + " ", 11, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(478, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(rx.leggiStringa("saldo") + " ", 11, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(608, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                }
                posy += 30;
            }
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, "TOTALE SALDO: " + Formattazione.formValuta(totsaldo, 12, 2, 2), lf, 24);
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printAndroidFont(4, posy, Typeface.MONOSPACE, true, false, false, "Firma conducente", lf, 20);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 119, 1);
            posy += 30 * 4;

            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }
}
