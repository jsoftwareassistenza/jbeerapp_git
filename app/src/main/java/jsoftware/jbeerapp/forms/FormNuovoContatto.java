package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.funzStringa;

public class FormNuovoContatto extends AppCompatActivity {


    private EditText ednome;
    private EditText edcognome;
    private EditText edazienda;
    private EditText edindirizzo;
    private EditText edindnum;
    private EditText edcomune;
    private EditText edmail;
    private EditText edtel;
    private EditText edcell;
    private EditText ednote;
    private RadioButton ckprivato;
    private RadioButton ckazienda;
    private Spinner sptipologia;
    private ArrayList lutenti;
    private SpinnerAdapter lutentiadapter;
    private boolean opmodifica = false;
    private String ccognome;
    private String cnome;
    private String ctel;
    private String cmobile;
    private String cemail;
    private String caziendaindir;
    private String caziendanumciv;
    private String caziendaragsoc;
    private String caziendaloc;
    private String caziendacap;
    private String cnote;
    private String ccodice;
    private EditText edcap;
    private EditText edprov;
    private String caziendaprov;
    private int ctipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_nuovo_contatto);

        ednome = findViewById(R.id.editnewCont_Nome);
        edcognome = findViewById(R.id.editnewCont_Cognome);
        edazienda = findViewById(R.id.editnewCont_Az);
        edindirizzo = findViewById(R.id.editnewCont_ind);
        edindnum = findViewById(R.id.editnewCont_num);
        edcomune = findViewById(R.id.editnewCont_Comune);
        edmail = findViewById(R.id.editnewCont_Mail);
        edtel = findViewById(R.id.editnewCont_Tel);
        edcell = findViewById(R.id.editnewCont_Cell);
        ednote = findViewById(R.id.editnewCont_Note);
        edcap = findViewById(R.id.editnewCont_cap);
        edprov = findViewById(R.id.editnewCont_pr);
        sptipologia = findViewById(R.id.editnewCon_sp);

        lutenti = new ArrayList();
        lutenti.add("Prospect Cliente");
        lutenti.add("Prospect Fornitore");
        lutenti.add("Altro");
        lutentiadapter = new SpinnerAdapter(this, R.layout.spinner_item, lutenti);
        sptipologia.setAdapter(lutentiadapter);
/*        ckcontatto = (RadioButton) findViewById(R.id.rB_contatto);
        ckfornitore = (RadioButton) findViewById(R.id.rB_fornitore);*/
        ckprivato = findViewById(R.id.rB_privato);
        ckazienda = findViewById(R.id.rB_azienda);
        ckazienda.setChecked(true);


        if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean("opmodifica") != false) {
            opmodifica = true;
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("ccognome"))
                ccognome = getIntent().getExtras().getString("ccognome");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cnome"))
                cnome = getIntent().getExtras().getString("cnome");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("ccodice"))
                ccodice = getIntent().getExtras().getString("ccodice");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("ctel"))
                ctel = getIntent().getExtras().getString("ctel");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cmobile"))
                cmobile = getIntent().getExtras().getString("cmobile");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cemail"))
                cemail = getIntent().getExtras().getString("cemail");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("caziendaindir"))
                caziendaindir = getIntent().getExtras().getString("caziendaindir");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("caziendanumciv"))
                caziendanumciv = getIntent().getExtras().getString("caziendanumciv");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("caziendaloc"))
                caziendaloc = getIntent().getExtras().getString("caziendaloc");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("caziendacap"))
                caziendacap = getIntent().getExtras().getString("caziendacap");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("caziendaprov"))
                caziendaprov = getIntent().getExtras().getString("caziendaprov");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("caziendaragsoc"))
                caziendaragsoc = getIntent().getExtras().getString("caziendaragsoc");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cnote"))
                cnote = getIntent().getExtras().getString("cnote");
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("ctipo"))
                ctipo = getIntent().getExtras().getInt("ctipo");
            FormNuovoContatto.this.setTitle("Modifica Contatto");
            // bsalva.setText("MODIFICA");
            edcognome.setText(ccognome);
            ednome.setText(cnome);
            edazienda.setText(caziendaragsoc);
            edindirizzo.setText(caziendaindir);
            edindnum.setText(caziendanumciv);
            edcomune.setText(caziendaloc);
            edmail.setText(cemail);
            edtel.setText(ctel);
            edcell.setText(cmobile);
            ednote.setText(cnote);
            edcap.setText(caziendacap);
            edprov.setText(caziendaprov);
            if (ctipo == 0) {
                ckprivato.setChecked(true);
                ckazienda.setChecked(false);
                ednome.setHint("Nome*");
                edcognome.setHint("Cognome*");
                edazienda.setHint("Azienda");
            } else {
                ckprivato.setChecked(false);
                ckazienda.setChecked(true);
                ednome.setHint("Nome");
                edcognome.setHint("Cognome");
                edazienda.setHint("Azienda*");
            }

        }

        ckprivato.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (ckprivato.isChecked()) {
                    ednome.setHint("Nome*");
                    edcognome.setHint("Cognome*");
                    edazienda.setHint("Azienda");
                    ckazienda.setSelected(false);
                }
            }
        });

        ckazienda.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (ckazienda.isChecked()) {
                    ednome.setHint("Nome");
                    edcognome.setHint("Cognome");
                    edazienda.setHint("Azienda*");
                    ckprivato.setSelected(false);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_form_salva_contatto, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //item.setEnabled(false);

        if (ckprivato.isChecked() && ednome.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoContatto.this);
            builder.setMessage("Inserire il Nome")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
//            return;
        }
        if (ckprivato.isChecked() && edcognome.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoContatto.this);
            builder.setMessage("Inserire il Cognome")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
            //        return;
        }

        if (ckazienda.isChecked() && edazienda.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoContatto.this);
            builder.setMessage("Inserire la Ragione sociale")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
            //        return;
        }

        if (edmail.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoContatto.this);
            builder.setMessage("Inserire l'email")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
            //   return;
        }
/*        if (edcell.getText().toString().equals("") || edtel.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormNuovoContatto.this);
            builder.setMessage("Inserire almeno un numero di telefono")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
            //return;
        }*/

        String oggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
        if (opmodifica) {
            String qry = "UPDATE contatti SET ccognome=?, cnome=?, caziendaragsoc=?, cemail=?,ctel=?,cmobile=?,caziendaindir=?,caziendanumciv=?, caziendaloc=? " +
                    ",ctipologia=?, ctipo=?, cnote=?, caziendacap=?, caziendaprov=?, ctrasferito=?,cdatacont=?,ctermcod=? WHERE ccognome= '" + ccognome + "' AND ccodice= '" + ccodice + "'";
            SQLiteStatement stup = Env.db.compileStatement(qry);
            stup.clearBindings();
            stup.bindString(1, edcognome.getText().toString().trim());
            stup.bindString(2, ednome.getText().toString().trim());
            stup.bindString(3, edazienda.getText().toString().trim());
            stup.bindString(4, edmail.getText().toString().trim());
            stup.bindString(5, edtel.getText().toString().trim());
            stup.bindString(6, edcell.getText().toString().trim());
            stup.bindString(7, edindirizzo.getText().toString().trim());
            stup.bindString(8, edindnum.getText().toString().trim());
            stup.bindString(9, edcomune.getText().toString().trim());
            stup.bindDouble(10, sptipologia.getSelectedItemPosition());
            stup.bindDouble(11, ckazienda.isChecked() ? 1 : 0);
            stup.bindString(12, ednote.getText().toString().trim());
            stup.bindString(13, edcap.equals("") ? "" : edcap.getText().toString().trim());
            stup.bindString(14, edprov.equals("") ? "" : edprov.getText().toString().trim());
            stup.bindString(15, "N");
            stup.bindString(16, oggi);
            stup.bindString(17, Env.pedcod);
            stup.execute();
            stup.close();

            HashMap h = new HashMap();
            h.put("ccognome", edcognome.getText().toString().trim());
            h.put("cnome", ednome.getText().toString().trim());
            h.put("caziendaragsoc", edazienda.getText().toString().trim());
            h.put("cemail", edmail.getText().toString().trim());
            h.put("ctel", edtel.getText().toString().trim());
            h.put("cmobile", (edcell.getText().toString().trim()));
            h.put("caziendaindir", edindirizzo.getText().toString().trim());
            h.put("caziendanumciv", edindnum.getText().toString().trim());
            h.put("caziendaloc", edcomune.getText().toString().trim());
            h.put("ctipologia", sptipologia.getSelectedItemPosition());
            h.put("ctipo", ckazienda.isChecked() ? 1 : 0);
            h.put("cnote", ednote.getText().toString().trim());
            h.put("caziendacap", edcap.equals("") ? "" : edcap.getText().toString().trim());
            h.put("caziendaprov", edprov.equals("") ? "" : edprov.getText().toString().trim());
            ArrayList<HashMap> vris = new ArrayList();
            vris.add(h);
            Uri esito = Uri.parse("content://modificacontatto/OK1");
            Intent result = new Intent(Intent.ACTION_PICK, esito);
            result.putExtra("vris", vris);
            setResult(RESULT_OK, result);
            finish();

        } else {


            SQLiteStatement sti = Env.db
                    .compileStatement("INSERT INTO contatti (ccodice,ccognome,cnome,cdatacont,ceventocont,ctitolo," + //6
                            "cmansione,cemail,ctel,cmobile,cfax,ctipologia, " +
                            "ctipo, caziendaragsoc,caziendaindir, caziendanumciv,caziendaloc,caziendacap," +
                            "caziendaprov, caziendanaz,caziendaemail, caziendasitoweb, cnote, ctermcod) "
                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
/*                SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
                String ora = dh.format(new Date());*/

            String codnew = "";
            boolean trovato = false;
            int progcontatto = 1;
            while (!trovato) {
                codnew = funzStringa.riempiStringa(Env.pedcod + "C" + funzStringa.riempiStringa("" + progcontatto, 5, funzStringa.DX, '0'), 10, funzStringa.SX, ' ').trim();
                String[] pars = new String[1];
                pars[0] = codnew;
                Cursor cursorp = Env.db.rawQuery("SELECT ccodice FROM contatti WHERE ccodice=?", pars);
                if (!cursorp.moveToFirst()) {
                    trovato = true;
                } else {
                    progcontatto++;
                }
                cursorp.close();
            }
            sti.clearBindings();
            sti.bindString(1, codnew);
            String tel = edtel.getText().toString().trim();
            int tipo = ckazienda.isChecked() ? 1 : 0;
            int tipologia = sptipologia.getSelectedItemPosition();
            sti.bindString(2, edcognome.getText().toString().trim());
            sti.bindString(3, ednome.getText().toString().trim());
            sti.bindString(4, oggi);
            if (!Env.depeventocod.equals(""))
                sti.bindString(5, Env.depeventocod);
            else
                sti.bindString(5, Env.depcod);
            sti.bindString(6, "");
            sti.bindString(7, "");
            sti.bindString(8, edmail.getText().toString().trim());
            sti.bindString(9, tel);
            sti.bindString(10, edcell.getText().toString().trim());
            sti.bindString(11, "");
            sti.bindDouble(12, tipologia);
            sti.bindDouble(13, tipo);
            sti.bindString(14, edazienda.getText().toString().trim());
            sti.bindString(15, edindirizzo.getText().toString().trim());
            sti.bindString(16, edindnum.getText().toString().trim());
            sti.bindString(17, edcomune.getText().toString().trim());
            sti.bindString(18, edcap.equals("") ? "" : edcap.getText().toString().trim());
            sti.bindString(19, edprov.equals("") ? "" : edprov.getText().toString().trim());
            sti.bindString(20, "");
            sti.bindString(21, "");
            sti.bindString(22, "");
            sti.bindString(23, ednote.getText().toString().trim());
            sti.bindString(24, Env.pedcod);
            sti.execute();
            sti.close();
            Uri esito = Uri.parse("content://nuovocontatto/OK1");
            Intent result = new Intent(Intent.ACTION_PICK, esito);
            setResult(RESULT_OK, result);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
