package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;

public class FormScambioDati extends AppCompatActivity {
    String dtfg = "";
    String orafg = "";
    private boolean aggiornamento = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_scambio_dati);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        final Button bric = findViewById(R.id.scambiodati_buttonRic);
        final Button btrasm = findViewById(R.id.scambiodati_buttonTrasm);

        bric.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Env.noriccarichi) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormScambioDati.this);
                    builder.setMessage("Ricezione dati bloccata")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            finish();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else {
                    Intent i = new Intent(getApplicationContext(), FormRicezioneDati.class);
                    FormScambioDati.this.startActivityForResult(i, 1);
                }
            }
        });

        btrasm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), FormInvioDati.class);
                Bundle mBundle = new Bundle();
                mBundle.putBoolean("soloordini", false);
                i.putExtras(mBundle);
                FormScambioDati.this.startActivity(i);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                ris = data.getData();
                if (ris != null) {
                    aggiornamento = (boolean) data.getExtras().get("aggiornamento");
                }
                break;

            }
        }
    }

    @Override
    public void onBackPressed() {
        Uri ris = Uri.parse("content://aggiornamento/OK");
        Intent result = new Intent(Intent.ACTION_PICK, ris);
        result.putExtra("aggiornamento", aggiornamento);
        setResult(RESULT_CANCELED, result);
        finish();

    }
}
