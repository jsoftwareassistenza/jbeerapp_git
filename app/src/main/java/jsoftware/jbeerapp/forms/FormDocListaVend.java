package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaVendAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;

public class FormDocListaVend extends AppCompatActivity {
    private Button bok;
    private Button bannulla;
    private EditText edcod;
    private EditText eddescr;
    private ListView listaart;
    private ListaVendAdapter lvendadapter;
    private ArrayList<Record> vart;
    private ArrayList<Record> vartlotto;
    private ArrayList<Record> vins;
    public boolean controlloGiac = true;
    public boolean controlloLotto = false;
    public String cliordlotto = "A";
    public String clicod = "";
    public String clicodpadre = "";
    public String clicodiva = "";
    private double giacenza;
    private MenuItem mCartIconMenuItem;
    private TextView mCountTv;
    private int mCount;
    private ImageButton mImageBtn;
    private ImageButton mRefreshGiac;
    private Context mContext;
    private int tipodoc;
    private double clisc1;
    private double clisc2;
    private double clisc3;
    private int caucorr;
    private int posselez;
    private Double quantita;
    private String codiva;
    private String artdescr;
    private boolean artmaggzero;

    ProgressBar progress = null;
    ProgressBar progress2 = null;
    TextView labelinfo = null;
    TextView labelinfo2 = null;
    ImageView icona = null;
    boolean aggincorso = false;
    private CheckBox ckmaggzero;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_doc_lista_vend);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("controlloGiac"))
            controlloGiac = getIntent().getExtras().getBoolean("controlloGiac");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("controlloLotto"))
            controlloLotto = getIntent().getExtras().getBoolean("controlloLotto");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("cliordlotto"))
            cliordlotto = getIntent().getExtras().getString("cliordlotto");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicod"))
            clicod = getIntent().getExtras().getString("clicod");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodpadre"))
            clicodpadre = getIntent().getExtras().getString("clicodpadre");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("tipodoc"))
            tipodoc = getIntent().getExtras().getInt("tipodoc");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clisc1"))
            clisc1 = getIntent().getExtras().getDouble("clisc1");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clisc2"))
            clisc2 = getIntent().getExtras().getDouble("clisc2");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clisc3"))
            clisc3 = getIntent().getExtras().getDouble("clisc3");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("caucorr"))
            caucorr = getIntent().getExtras().getInt("caucorr");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodiva"))
            clicodiva = getIntent().getExtras().getString("clicodiva");
        bok = this.findViewById(R.id.listavend_buttonOk);
        bannulla = this.findViewById(R.id.listavend_buttonAnnulla);
        listaart = this.findViewById(R.id.listavend_listaart);
        edcod = this.findViewById(R.id.listavend_riccod);
        eddescr = this.findViewById(R.id.listavend_ricdescr);
        ckmaggzero = this.findViewById(R.id.listavend_filtro);
        if (Env.dispart) {
            ckmaggzero.setText("Disponibilita' > 0");
        }
        if (Env.codartnum)
            edcod.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
        vartlotto = new ArrayList<Record>();
        vart = new ArrayList<Record>();
        vins = new ArrayList<Record>();
        lvendadapter = new ListaVendAdapter(this.getApplicationContext(), vart);
        listaart.setAdapter(lvendadapter);

/*        if (!clicod.equals("")) {
            String[] parsc = new String[1];
            parsc[0] = clicod;
            final Cursor cc = Env.db.rawQuery(
                    "SELECT climodprz FROM clienti WHERE clicodice = ?", parsc);
            if (cc.moveToFirst()) {
                modprz = (cc.getString(0).equals("S"));
            }
            cc.close();
        }*/
        if (Env.depeventocod.equals("")) {
            aggiornaLista();
        } else {
            aggiornaListaEvento();
        }

        edcod.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 1) {
                    if (Env.depeventocod.equals("")) {
                        aggiornaLista();
                    } else {
                        aggiornaListaEvento();
                    }
                }
            }
        });

        ckmaggzero.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                artmaggzero = buttonView.isChecked();
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
            }
        });

        eddescr.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 1) {
                    if (Env.depeventocod.equals("")) {
                        aggiornaLista();
                    } else {
                        aggiornaListaEvento();
                    }
                }
            }
        });

        bok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ArrayList<HashMap> vris = new ArrayList();
                for (int i = 0; i < vins.size(); i++) {
                    Record r = vins.get(i);
                    double a = r.leggiDouble("qtavend");
                    if (r.leggiDouble("qtavend") != 0 || r.leggiDouble("qtasm") != 0 || r.leggiDouble("qtaomimp") != 0 ||
                            r.leggiDouble("qtaomtot") != 0) {
                        HashMap h = new HashMap();
                        h.put("artcod", r.leggiStringa("rmartcod"));
                        h.put("artdescr", r.leggiStringa("artdescr"));
                        h.put("codiva", r.leggiStringa("codiva"));
                        h.put("lotto", r.leggiStringa("lotto"));
                        h.put("qtavend", r.leggiDouble("qtavend"));
                        h.put("qtasm", r.leggiDouble("qtasm"));
                        h.put("qtaomimp", r.leggiDouble("qtaomimp"));
                        h.put("qtaomtot", r.leggiDouble("qtaomtot"));
                        h.put("prezzo", r.leggiDouble("prezzo"));
                        h.put("sc1", r.leggiDouble("sc1"));
                        h.put("sc2", r.leggiDouble("sc2"));
                        h.put("sc3", r.leggiDouble("sc3"));
                        h.put("colli", r.leggiIntero("colli"));
/*                        if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                            h.put("rmaccisa", r.leggiDouble("rmaccisa"));
                            h.put("rmgrado", r.leggiDouble("rmgrado"));
                            h.put("rmplato", r.leggiDouble("rmplato"));
                        } else {
                            h.put("rmaccisa", (double) 0);
                            h.put("rmgrado", (double) 0);
                            h.put("rmplato", (double) 0);
                        }*/
                        vris.add(h);
                    }
                }
                Uri codselez = Uri.parse("content://listavend/OK");
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                result.putExtra("vris", vris);
                setResult(RESULT_OK, result);
                finish();
            }
        });

        bannulla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uri codselez = Uri.parse("content://listavend/ANNULLA");
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                setResult(RESULT_CANCELED, result);
                finish();
            }
        });

        listaart.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                posselez = (int) arg3;
                if (posselez != -1) {
                    Record rec = vart.get(posselez);
                    Env.trasfdoc_rriga = null;
                    Env.trasfdoc_rrigavar = null;
                    //Env.trasfdoc_righecrp = fdoc.righecrp;
                    Intent i = new Intent(FormDocListaVend.this.getApplicationContext(), FormRigaArt.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("artcod", rec.leggiStringa("artcod"));
                    //mBundle.putString("lotto", rec.leggiStringa("lotto"));

                    giacenza = rec.leggiDouble("giac");
                    quantita = rec.leggiDouble("qtavend");
                    codiva = rec.leggiStringa("codiva");
                    artdescr = rec.leggiStringa("artdescr");
                    mBundle.putDouble("giac", rec.leggiDouble("giac"));
                    mBundle.putInt("tipodoc", tipodoc);
                    mBundle.putString("clicod", clicod);
                    mBundle.putDouble("clisc1", clisc1);
                    mBundle.putDouble("clisc2", clisc2);
                    mBundle.putDouble("clisc3", clisc3);
                    mBundle.putInt("caucorr", caucorr);
                    mBundle.putDouble("prezzo", rec.leggiDouble("prezzo"));
                    i.putExtras(mBundle);
                    FormDocListaVend.this.startActivityForResult(i, 1);

                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        mCartIconMenuItem = menu.findItem(R.id.cart_count_menu_item);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        View actionView = mCartIconMenuItem.getActionView();
        if (actionView != null) {
            mCountTv = actionView.findViewById(R.id.count_tv_layout);
            mImageBtn = actionView.findViewById(R.id.image_btn_layout);
            //mRefreshGiac = (ImageButton) actionView.findViewById(R.id.cart_refresh_giac);
            //aggiornaQtaCarrello();
        }
        mCountTv.setText("" + (int) Env.quantitaCarrello);
        //saveButton = (ImageButton) menu.findItem(R.id.menu).getActionView();
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                // ritorno da inserimento riga art
                if (resultCode == Activity.RESULT_OK) {
                    Record rart = vart.get(posselez);
                    //ris = data.getData();
                    ArrayList<HashMap> vris = (ArrayList<HashMap>) data.getExtras().get("vris");
                    for (int i = 0; i < vris.size(); i++) {
                        HashMap riga = vris.get(i);
                        boolean trovato = false;
                        //controlla se è già stato inserito lo stesso articolo con eventualmente lo stesso lotto
                        for (int j = 0; j < vins.size(); j++) {
                            Record r2 = vins.get(j);
                            if (((String) riga.get("artcod")).equals(r2.leggiStringa("rmartcod")) &&
                                    ((String) riga.get("rmcaumag")).equals(r2.leggiStringa("rmcaumag")) &&
                                    (Double) riga.get("prezzo") == r2.leggiDouble("prezzo") &&
                                    (Double) riga.get("sc1") == r2.leggiDouble("sc1") &&
                                    (Double) riga.get("sc2") == r2.leggiDouble("sc2") &&
                                    (Double) riga.get("sc3") == r2.leggiDouble("sc3") &&
                                    ((String) riga.get("lotto")).equals(r2.leggiStringa("lotto"))) {
                                Env.righecrptmp.remove(j);
                                double qtaomimp = (Double) riga.get("qtaomimp") + r2.leggiDouble("qtaomimp");
                                double qtasm = (Double) riga.get("qtasm") + r2.leggiDouble("qtasm");
                                double qtavend = (Double) riga.get("qtavend") + r2.leggiDouble("qtavend");
                                double qtaomtot = (Double) riga.get("qtaomtot") + r2.leggiDouble("qtaomtot");
                                giacenza -= (Double) riga.get("quantita");
                                rart.eliminaCampo("giac");
                                rart.insDouble("giac", giacenza);
                                rart.eliminaCampo("qtaomimp");
                                rart.insDouble("qtaomimp", qtaomimp);
                                rart.eliminaCampo("qtasm");
                                rart.insDouble("qtasm", qtasm);
                                rart.eliminaCampo("qtavend");
                                rart.insDouble("qtavend", qtavend);
                                rart.eliminaCampo("qtaomtot");
                                rart.insDouble("qtaomtot", qtaomtot);
                                int colli = (Integer) riga.get("colli") + r2.leggiIntero("colli");
                                double quantita = (Double) riga.get("quantita") + r2.leggiDouble("rmqta");
                                r2.eliminaCampo("qtaomimp");
                                r2.insDouble("qtaomimp", qtaomimp);
                                r2.eliminaCampo("qtasm");
                                r2.insDouble("qtasm", qtasm);
                                r2.eliminaCampo("qtavend");
                                r2.insDouble("qtavend", qtavend);
                                r2.eliminaCampo("qtaomtot");
                                r2.insDouble("qtaomtot", qtaomtot);
                                r2.eliminaCampo("quantita");
                                r2.insDouble("rmqta", quantita);
                                r2.eliminaCampo("colli");
                                r2.insIntero("colli", colli);
                                trovato = true;
                                Env.righecrptmp.add(r2);

                                break;
                            }
                        }
                        if (!trovato) {
                            Record rec = new Record();
                            rec.insDouble("prezzo", (Double) riga.get("prezzo"));
                            rec.insDouble("sc1", (Double) riga.get("sc1"));
                            rec.insDouble("sc2", (Double) riga.get("sc2"));
                            rec.insDouble("sc3", (Double) riga.get("sc3"));
                            rec.insDouble("qtaomimp", (Double) riga.get("qtaomimp"));
                            rec.insDouble("qtasm", (Double) riga.get("qtasm"));
                            rec.insDouble("qtavend", (Double) riga.get("qtavend"));
                            rec.insDouble("qtaomtot", (Double) riga.get("qtaomtot"));
                            rec.insIntero("colli", (Integer) riga.get("colli"));
                            rec.insStringa("lotto", (String) riga.get("lotto"));
                            giacenza -= (Double) riga.get("quantita");
                            rart.eliminaCampo("giac");
                            rart.insDouble("giac", giacenza);
                            rart.eliminaCampo("qtaomimp");
                            rart.insDouble("qtaomimp", (Double) riga.get("qtaomimp"));
                            rart.eliminaCampo("qtasm");
                            rart.insDouble("qtasm", (Double) riga.get("qtasm"));
                            rart.eliminaCampo("qtavend");
                            rart.insDouble("qtavend", (Double) riga.get("qtavend"));
                            rart.eliminaCampo("qtaomtot");
                            rart.insDouble("qtaomtot", (Double) riga.get("qtaomtot"));
                            rec.insDouble("giac", giacenza);
                            rec.insStringa("rmcaumag", (String) riga.get("rmcaumag"));
                            rec.insStringa("rmartcod", (String) riga.get("artcod"));
                            rec.insStringa("rmlotto", (String) riga.get("lotto"));
                            rec.insStringa("codiva", codiva);
                            rec.insStringa("artdescr", artdescr);
                            rec.insDouble("rmqta", (Double) riga.get("quantita"));
                            vins.add(rec);
                            Env.righecrptmp.add(rec);
                        }

                        Env.quantitaCarrello += (Integer) riga.get("colli");
                    }
                    lvendadapter.notifyDataSetChanged();
                    mCountTv.setText("" + (int) Env.quantitaCarrello);

                    Toast toast = Toast.makeText(getApplicationContext(), "Aggiunto al carrello!",
                            Toast.LENGTH_SHORT);
                    toast.show();

                }
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!aggincorso) {
            Uri codselez = Uri.parse("content://listavend/ANNULLA");
            Intent result = new Intent(Intent.ACTION_PICK, codselez);
            setResult(RESULT_CANCELED, result);

            finish();
        }
    }

    private void aggiornaListaEvento() {
        // mem.qta già inserite in lista

        HashMap<String, Double> hqtavendold = new HashMap();
        HashMap<String, Double> hqtasmold = new HashMap();
        HashMap<String, Double> hqtaomimpold = new HashMap();
        HashMap<String, Double> hqtaomtotold = new HashMap();
        for (int i = 0; i < vart.size(); i++) {
            Record r = vart.get(i);
            if (r.leggiDouble("qtavend") > 0) {
                hqtavendold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtavend")));
            }
            if (r.leggiDouble("qtasm") > 0) {
                hqtasmold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtasm")));
            }
            if (r.leggiDouble("qtaomimp") > 0) {
                hqtaomimpold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtaomimp")));
            }
            if (r.leggiDouble("qtaomtot") > 0) {
                hqtaomtotold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtaomtot")));
            }
        }
        vart.clear();
        // caricamento prodotti
        String c = edcod.getText().toString().trim();
        String d = eddescr.getText().toString().trim();
        c.replace("'", "''");
        d.replace("'", "''");
        String qry = "SELECT artcod,artdescr,artum,artgiacenza,artcodiva,artflagum,artweb_linkscheda" +
                " FROM articoli " +
                "INNER JOIN catalogo on articoli.artcod = catalogo.catartcod " +
                "WHERE artcod <> '' " +
                (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                (artmaggzero ? " AND artgiacenza > 0" : "") +
                " ORDER BY artcod " + (cliordlotto.equals("D") ? " DESC" : "");
        Cursor cursor = Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            Record rx = new Record();
            rx.insStringa("artcod", cursor.getString(0));
            rx.insStringa("artdescr", cursor.getString(1));
            rx.insStringa("artum", cursor.getString(2));
            rx.insIntero("artflagum", cursor.getInt(5));
            rx.insStringa("artschedaweb", cursor.getString(6));
            rx.insStringa("codiva", cursor.getString(4));
            //rx.insStringa("lotto", cursor.getString(7));
            double vargiac = 0;
            for (int j = 0; j < Env.trasfdoc_righecrp.size(); j++) {
                Record rr = Env.trasfdoc_righecrp.get(j);
                String acod = rr.leggiStringa("rmartcod");
                String lcod = rr.leggiStringa("rmlotto");
                String cau = rr.leggiStringa("rmcaumag");
                double qta = rr.leggiDouble("rmqta");
                if (acod.equals(cursor.getString(0)) && !cau.equals("RN")) {
                    if (cau.equals("RV"))
                        vargiac += qta;
                    else
                        vargiac -= qta;
                }
            }

            //giacenza = OpValute.arrotondaMat(cursor.getDouble(3) + vargiac, 3);
            rx.insDouble("giac", OpValute.arrotondaMat(cursor.getDouble(3) + vargiac, 2));

            //rx.insDouble("giac", OpValute.arrotondaMat(vargiac, 3));
            if (!clicod.equals("")) {
                String cc = clicod;
                if (!clicodpadre.equals(""))
                    cc = clicodpadre;
                double przvend = 0;
                double prznetto = 0;
                double sc1 = 0, sc2 = 0, sc3 = 0;
                if (Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
                    double[] przsc = FunzioniJBeerApp.leggiPrezzoScontiArticolo(cursor.getString(0), clicod, clicodpadre, (new Data()).formatta(Data.AAAA_MM_GG, "-"), false);
                    przvend = przsc[0];
                    prznetto = przvend;
                    sc1 = przsc[1];
                    sc2 = przsc[2];
                    sc3 = przsc[3];
                    if (przsc[1] > 0)
                        prznetto = prznetto - (prznetto * przsc[1] / 100);
                    if (przsc[2] > 0)
                        prznetto = prznetto - (prznetto * przsc[2] / 100);
                    if (przsc[3] > 0)
                        prznetto = prznetto - (prznetto * przsc[3] / 100);
                    prznetto = OpValute.arrotondaMat(prznetto, 2);
                }
                rx.insDouble("sc1", sc1);
                rx.insDouble("sc2", sc2);
                rx.insDouble("sc3", sc3);
                rx.insDouble("prezzo", przvend);
                rx.insDouble("prezzonetto", prznetto);
            } else {
                rx.insDouble("sc1", 0);
                rx.insDouble("sc2", 0);
                rx.insDouble("sc3", 0);
                rx.insDouble("prezzo", 0);
                rx.insDouble("prezzonetto", 0);
            }
            double qvend = 0;
            if (hqtavendold.get(cursor.getString(0) + "|" + cursor.getString(4)) != null)
                qvend = hqtavendold.get(cursor.getString(0) + "|" + cursor.getString(4)).doubleValue();
            rx.insDouble("qtavend", qvend);
            double qsm = 0;
            if (hqtasmold.get(cursor.getString(0) + "|" + cursor.getString(4)) != null)
                qsm = hqtasmold.get(cursor.getString(0) + "|" + cursor.getString(4)).doubleValue();
            rx.insDouble("qtasm", qsm);
            double qomimp = 0;
            if (hqtaomimpold.get(cursor.getString(0) + "|" + cursor.getString(4)) != null)
                qomimp = hqtaomimpold.get(cursor.getString(0) + "|" + cursor.getString(4)).doubleValue();
            rx.insDouble("qtaomimp", qomimp);
            double qomtot = 0;
            if (hqtaomtotold.get(cursor.getString(0) + "|" + cursor.getString(6)) != null)
                qomtot = hqtaomtotold.get(cursor.getString(0) + "|" + cursor.getString(4)).doubleValue();
            rx.insDouble("qtaomtot", qomtot);
            vart.add(rx);
        }
        cursor.close();
        lvendadapter.notifyDataSetChanged();
    }

    private void aggiornaLista() {
        // mem.qta già inserite in lista

        HashMap<String, Double> hqtavendold = new HashMap();
        HashMap<String, Double> hqtasmold = new HashMap();
        HashMap<String, Double> hqtaomimpold = new HashMap();
        HashMap<String, Double> hqtaomtotold = new HashMap();
        for (int i = 0; i < vart.size(); i++) {
            Record r = vart.get(i);
            if (r.leggiDouble("qtavend") > 0) {
                hqtavendold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtavend")));
            }
            if (r.leggiDouble("qtasm") > 0) {
                hqtasmold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtasm")));
            }
            if (r.leggiDouble("qtaomimp") > 0) {
                hqtaomimpold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtaomimp")));
            }
            if (r.leggiDouble("qtaomtot") > 0) {
                hqtaomtotold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtaomtot")));
            }
        }
        vart.clear();
        // caricamento prodotti
        String c = edcod.getText().toString().trim();
        String d = eddescr.getText().toString().trim();
        c.replace("'", "''");
        d.replace("'", "''");
        String qry = "";

        if (Env.dispart) {
            qry = "SELECT artcod,artdescr,artum,dispqta,artcodiva,artflagum,artweb_linkscheda " +
                    " FROM catalogo INNER JOIN articoli ON catartcod=artcod LEFT JOIN dispart ON artcod=dispartcod WHERE artcod <> '' " +
                    (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                    (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                    (artmaggzero ? " AND dispqta > 0" : "") +
                    " ORDER BY artcod" + (cliordlotto.equals("D") ? " DESC" : "");
            if (android.os.Build.VERSION.SDK_INT <= 23) {
                qry += " LIMIT 200";
            }
        } else {
            qry = "SELECT artcod,artdescr,artum,giacqta,artcodiva,artflagum,artweb_linkscheda " +
                    " FROM catalogo INNER JOIN articoli ON catartcod=artcod LEFT JOIN giacart ON artcod=giacartcod WHERE artcod <> '' " +
                    (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                    (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                    (artmaggzero ? " AND giacqta > 0" : "") +
                    " ORDER BY artcod " + (cliordlotto.equals("D") ? " DESC" : "");
            if (android.os.Build.VERSION.SDK_INT <= 23) {
                qry += " LIMIT 200";
            }

        }
        Cursor cursor = Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            Record rx = new Record();
            rx.insStringa("artcod", cursor.getString(0));
            rx.insStringa("artdescr", cursor.getString(1));
            rx.insStringa("artum", cursor.getString(2));
            rx.insIntero("artflagum", cursor.getInt(5));
            rx.insStringa("artschedaweb", cursor.getString(6));
            rx.insStringa("codiva", cursor.getString(4));
            //rx.insStringa("lotto", cursor.getString(7));
            double vargiac = 0;
            for (int j = 0; j < Env.trasfdoc_righecrp.size(); j++) {
                Record rr = Env.trasfdoc_righecrp.get(j);
                String acod = rr.leggiStringa("rmartcod");
                String lcod = rr.leggiStringa("rmlotto");
                String cau = rr.leggiStringa("rmcaumag");
                double qta = rr.leggiDouble("rmqta");
                if (acod.equals(cursor.getString(0)) && !cau.equals("RN")) {
                    if (cau.equals("RV"))
                        vargiac += qta;
                    else
                        vargiac -= qta;
                }
            }

            //controlla i movimenti - documenti effettuati ma non ancora inviati
            String qrygiac = "SELECT movtipo,movdoc,movsez,movdata,movnum" +
                    " FROM movimenti WHERE movtrasf <> 'S' ORDER BY movtipo,movdata,movnum";
            Cursor cursorgiac = Env.db.rawQuery(qrygiac, null);
            while (cursorgiac.moveToNext()) {
                String[] parsd = new String[6];
                parsd[0] = "" + cursorgiac.getInt(0);
                parsd[1] = cursorgiac.getString(1);
                parsd[2] = cursorgiac.getString(2);
                parsd[3] = cursorgiac.getString(3);
                parsd[4] = "" + cursorgiac.getInt(4);
                parsd[5] = cursor.getString(0);
                int nriga = 1;
                Cursor cd = Env.db.rawQuery(
                        "SELECT rmqta " +
                                "FROM righemov WHERE rmmovtipo=? AND rmmovdoc=? AND rmmovsez=? AND rmmovdata=? AND rmmovnum=? AND rmartcod=? ORDER BY rmriga",
                        parsd);
                while (cd.moveToNext()) {
                    vargiac = vargiac - cd.getDouble(0);
                }
                cd.close();
            }
            cursorgiac.close();
            rx.insDouble("giac", OpValute.arrotondaMat(cursor.getDouble(3) + vargiac, 2));
            if (!clicod.equals("")) {
                String cc = clicod;
                if (!clicodpadre.equals(""))
                    cc = clicodpadre;
                double przvend = 0;
                double prznetto = 0;
                double sc1 = 0, sc2 = 0, sc3 = 0;
                if (Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
                    String artcod = cursor.getString(0);
                    double[] przsc = FunzioniJBeerApp.leggiPrezzoScontiArticolo(artcod, clicod, clicodpadre, (new Data()).formatta(Data.AAAA_MM_GG, "-"), false);
                    przvend = przsc[0];
                    prznetto = przvend;
                    sc1 = przsc[1];
                    sc2 = przsc[2];
                    sc3 = przsc[3];
                    if (przsc[1] > 0)
                        prznetto = prznetto - (prznetto * przsc[1] / 100);
                    if (przsc[2] > 0)
                        prznetto = prznetto - (prznetto * przsc[2] / 100);
                    if (przsc[3] > 0)
                        prznetto = prznetto - (prznetto * przsc[3] / 100);
                    prznetto = OpValute.arrotondaMat(prznetto, 2);
                }
                rx.insDouble("sc1", sc1);
                rx.insDouble("sc2", sc2);
                rx.insDouble("sc3", sc3);
                rx.insDouble("prezzo", przvend);
                rx.insDouble("prezzonetto", prznetto);
            } else {
                rx.insDouble("sc1", 0);
                rx.insDouble("sc2", 0);
                rx.insDouble("sc3", 0);
                rx.insDouble("prezzo", 0);
                rx.insDouble("prezzonetto", 0);
            }
            double qvend = 0;
            if (hqtavendold.get(cursor.getString(0) + "|" + cursor.getString(4)) != null)
                qvend = hqtavendold.get(cursor.getString(0) + "|" + cursor.getString(4)).doubleValue();
            rx.insDouble("qtavend", qvend);
            double qsm = 0;
            if (hqtasmold.get(cursor.getString(0) + "|" + cursor.getString(4)) != null)
                qsm = hqtasmold.get(cursor.getString(0) + "|" + cursor.getString(4)).doubleValue();
            rx.insDouble("qtasm", qsm);
            double qomimp = 0;
            if (hqtaomimpold.get(cursor.getString(0) + "|" + cursor.getString(4)) != null)
                qomimp = hqtaomimpold.get(cursor.getString(0) + "|" + cursor.getString(4)).doubleValue();
            rx.insDouble("qtaomimp", qomimp);
            double qomtot = 0;
            if (hqtaomtotold.get(cursor.getString(0) + "|" + cursor.getString(6)) != null)
                qomtot = hqtaomtotold.get(cursor.getString(0) + "|" + cursor.getString(4)).doubleValue();
            rx.insDouble("qtaomtot", qomtot);
            vart.add(rx);
        }
        cursor.close();

        lvendadapter.notifyDataSetChanged();
    }




/*    private void creaRiga(String artcod, String artdescr, String lottoriga, int cau, double qtariga,
                          double przu, double sc1, double sc2, double sc3, String codivaart, int colliriga, double accisa, double grado, double plato) {
        // creazione dati riga
        Record rriga = new Record();
        rriga.insStringa("rmtiporiga", "A");
        rriga.insStringa("rmartcod", artcod);
        rriga.insStringa("rmartdescr", artdescr);
        rriga.insStringa("rmlotto", lottoriga);
        boolean omimp = false;
        boolean omtot = false;
        if (Env.sptipodoc == 6) {
            rriga.insStringa("rmcaumag", "OC");
            if (cau == 4)
                omtot = true;
            else if (cau == 5)
                omimp = true;
            else if (cau == 6)
                omtot = true;
        } else if (cau == 0)
            rriga.insStringa("rmcaumag", "VE");
        else if (cau == 4) {
            rriga.insStringa("rmcaumag", "SM");
            omtot = true;
        } else if (cau == 5) {
            rriga.insStringa("rmcaumag", "OM");
            omimp = true;
        } else if (cau == 6) {
            rriga.insStringa("rmcaumag", "OT");
            omtot = true;
        }
        rriga.insIntero("rmcolli", colliriga);
        rriga.insDouble("rmpezzi", 0);
        double contenutoriga = FunzioniJBeerApp.calcoloContenutoArticolo(artcod, qtariga);
        rriga.insDouble("rmcontenuto", contenutoriga);
        rriga.insDouble("rmqta", qtariga);
        if (przu != 0) {
            rriga.insDouble("rmprz", przu);
            rriga.insDouble("rmartsc1", sc1);
            rriga.insDouble("rmartsc2", sc2);
            rriga.insDouble("rmscvend", sc3);
            double aliq = 0;
            String codiva = "";
            String codivaecc = "";
            double aliqecc = 0;
            String cc = clicod;
            if (!clicodpadre.equals(""))
                cc = clicodpadre;
            String[] parsei = new String[2];
            parsei[0] = cc;
            parsei[1] = artcod;
            Cursor cei = Env.db.rawQuery(
                    "SELECT codiva.ivacod,codiva.ivaaliq FROM cliartiva INNER JOIN codiva ON cliartiva.ivacod = codiva.ivacod WHERE clicod = ? AND artcod = ?", parsei);
            if (cei.moveToFirst()) {
                codivaecc = cei.getString(0);
                aliqecc = cei.getDouble(1);
            }
            cei.close();
            if (!clicodiva.equals("")) {
                Record riva = Env.hcodiva.get(clicodiva);
                if (riva != null) {
                    codiva = riva.leggiStringa("ivacod");
                    aliq = riva.leggiDouble("ivaaliq");
                }
            } else {
                Record riva = Env.hcodiva.get(codivaart);
                if (riva != null) {
                    codiva = riva.leggiStringa("ivacod");
                    aliq = riva.leggiDouble("ivaaliq");
                }
            }
            if (!codivaecc.equals("")) {
                codiva = codivaecc;
                aliq = aliqecc;
            }
            if (omimp) {
                if (aliq == 22)
                    codiva = Env.codivaomaggiimp20;
                else if (aliq == 10)
                    codiva = Env.codivaomaggiimp10;
                else if (aliq == 4)
                    codiva = Env.codivaomaggiimp04;
            } else if (omtot) {
                if (aliq == 22)
                    codiva = Env.codivaomaggitot20;
                else if (aliq == 10)
                    codiva = Env.codivaomaggitot10;
                else if (aliq == 4)
                    codiva = Env.codivaomaggitot04;
            }
            rriga.insStringa("rmcodiva", codiva);
            rriga.insDouble("rmaliq", aliq);
            rriga.insDouble("rmclisc1", clisc1);
            rriga.insDouble("rmclisc2", clisc2);
            rriga.insDouble("rmclisc3", clisc3);
            double imp = 0;
            if (FunzioniJBeerApp.tipoCalcoloPrezzoArticolo(artcod) == 1) {
                if (colliriga != 0)
                    imp = OpValute.arrotondaMat((double) colliriga * przu, 2);
                else
                    imp = OpValute.arrotondaMat(qtariga * przu, 2);
            } else if (FunzioniJBeerApp.tipoCalcoloPrezzoArticolo(artcod) == 3 || FunzioniJBeerApp.tipoCalcoloPrezzoArticolo(artcod) == 4) {
                if (contenutoriga != 0)
                    imp = OpValute.arrotondaMat(contenutoriga * przu, 2);
                else
                    imp = OpValute.arrotondaMat(qtariga * przu, 2);
            } else
                imp = OpValute.arrotondaMat(qtariga * przu, 2);
            rriga.insDouble("rmlordo", imp);
            float scoart = 0;
            if (sc1 != 0) {
                double s = (imp * sc1 / 100);
                imp -= s;
                scoart += s;
            }
            if (sc2 != 0) {
                double s = (imp * sc2 / 100);
                imp -= s;
                scoart += s;
            }
            if (sc3 != 0) {
                double s = (imp * sc3 / 100);
                imp -= s;
                scoart += s;
            }
            rriga.insDouble("totscontiart", scoart);
            float scocli = 0;
            if (clisc1 != 0) {
                double s = (imp * clisc1 / 100);
                imp -= s;
                scocli += s;
            }
            if (clisc2 != 0) {
                double s = (imp * clisc2 / 100);
                imp -= s;
                scocli += s;
            }
            if (clisc3 != 0) {
                double s = (imp * clisc3 / 100);
                imp -= s;
                scocli += s;
            }
            rriga.insDouble("totsconticli", scocli);
            imp = OpValute.arrotondaMat(imp, 2);
            rriga.insDouble("rmnetto", imp);
            rriga.insDouble("rmnettoivato", imp);
            // cessionario
            String cesscod = "";
            if (!clicodpadre.equals(""))
                cesscod = FunzioniJBeerApp.cercaCodiceCessionario(artcod, clicod);
            rriga.insStringa("cesscod", cesscod);
            rriga.insDouble("rmaccisa", accisa);
            rriga.insDouble("rmgrado", grado);
            rriga.insDouble("rmplato", plato);
            Env.righecrptmp.add(rriga);
        }
    }*/
}



