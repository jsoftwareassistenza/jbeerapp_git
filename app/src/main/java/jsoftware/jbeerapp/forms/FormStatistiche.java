package jsoftware.jbeerapp.forms;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import jsoftware.jbeerapp.R;

public class FormStatistiche extends AppCompatActivity {

    private Button buttonStatGen;
    private Button buttonStatEven;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_statistiche);

        buttonStatGen = findViewById(R.id.stat_generali);
        buttonStatEven = findViewById(R.id.stat_eventi);

        buttonStatGen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneGenerali();
            }
        });

        buttonStatEven.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneStatiche();
            }
        });

    }

    private void azioneStatiche() {

        Intent i = new Intent(getApplicationContext(), FormStatisticheEventi.class);
        jsoftware.jbeerapp.forms.FormStatistiche.this.startActivity(i);

    }

    private void azioneGenerali() {

        Intent i = new Intent(getApplicationContext(), FormStatisticheGenerali.class);
        jsoftware.jbeerapp.forms.FormStatistiche.this.startActivity(i);


    }

}
