package jsoftware.jbeerapp.forms;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.webkit.WebView;

import jsoftware.jbeerapp.R;

public class FormWeb extends AppCompatActivity {
    private WebView wv;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_web);

        wv = findViewById(R.id.web_webview);

        if (getIntent().getExtras() != null && getIntent().getExtras().getString("TITOLO") != null) {
            setTitle(getIntent().getExtras().getString("TITOLO"));
        }

        if (getIntent().getExtras() != null && getIntent().getExtras().getString("URL") != null) {
            url = getIntent().getExtras().getString("URL");
            wv.getSettings().setJavaScriptEnabled(true);
            wv.getSettings().setDisplayZoomControls(true);
            wv.getSettings().setBuiltInZoomControls(true);
            wv.getSettings().setSupportZoom(true);
            wv.loadUrl(url);
        }

    }
}
