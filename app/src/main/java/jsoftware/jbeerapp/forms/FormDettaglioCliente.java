package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaShareCatalogoAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.Record;


public class FormDettaglioCliente extends AppCompatActivity implements OnFragmentInteractionListener {

    public ImageButton saveButton;

    public FragmentDettaglioClienteProfilo fdcp;
    public FragmentDettaglioClienteListino fdcl;
    public FragmentDettaglioClienteStatistiche fdcs;


    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    public ViewPager mViewPager;
    public TextView tvnominativo;
    private String clinome;
    private String clinomepri = "";
    private String clicognomepri = "";
    private TextView saldo;
    public ImageButton bincassi;
    public ImageButton butshop;
    public String clicodice;
    public String clicodpadre;
    public ImageButton butmodif;
    public ImageButton butshare;
    public ImageButton butdelete;
    public ArrayList<Record> vrubrica;
    private boolean trasformato = false;
    private ListView lvcatalogo;
    private ArrayList<Record> vcatalogo;
    private ListaShareCatalogoAdapter lshcatagadapter;
    private boolean cliTrasf = false;
    private boolean flagmodifica = false;
    private int clitipo;
    public double cligpsn = 0, cligpse = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_dettaglio_clienti);

        fdcp = new FragmentDettaglioClienteProfilo();
        fdcp.fdcli = this;
        fdcl = new FragmentDettaglioClienteListino();
        fdcl.fdcli = this;
        fdcs = new FragmentDettaglioClienteStatistiche();
        fdcs.fdcli = this;


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        tvnominativo = findViewById(R.id.detcli_nome);
        saldo = findViewById(R.id.detcli_saldo);
        butmodif = findViewById(R.id.detcli_bmod);
        butshare = findViewById(R.id.detcli_bshare);
        butdelete = findViewById(R.id.detcli_bdel);


        // bincassi = (Button) findViewById(R.id.detcli_bincassi);
        butshop = findViewById(R.id.detcli_bshop);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clinome"))
            clinome = getIntent().getExtras().getString("clinome");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clinomepri"))
            clinomepri = getIntent().getExtras().getString("clinomepri");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicognomepri"))
            clicognomepri = getIntent().getExtras().getString("clicognomepri");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodice"))
            clicodice = getIntent().getExtras().getString("clicodice");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clitipo"))
            clitipo = getIntent().getExtras().getInt("clitipo");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("trasformato"))
            trasformato = getIntent().getExtras().getBoolean("trasformato");
        if (clicognomepri.equals("") || !clinome.equals("")) {
            tvnominativo.setText(clinome);
        } else {
            tvnominativo.setText(clicognomepri + " " + clinomepri);
        }

        double scop = FunzioniJBeerApp.totaleScopertiCliente(clicodice);
        leggiCliente();
        saldo.setText(Formattazione.formValuta(scop, 12, 2, 1));
        if (scop != 0) {
            saldo.setTextColor(Color.RED);
        } else {
            saldo.setTextColor(Color.rgb(0, 100, 0));
        }
        //fdcp.tvgps.setText(fdcp.gps);


        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.detcli_container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(4);

        TabLayout tabLayout = findViewById(R.id.detcli_tabs);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(0);


        butmodif.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneModifica();
            }
        });

        butdelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneElimina();
            }
        });

        butshop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneShop();
            }
        });

        butshare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                azioneShare();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_form_dettaglio_cliente, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }

        //saveButton = (ImageButton) menu.findItem(R.id.menu).getActionView();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.det_action_incassi) {
            Intent i = new Intent(FormDettaglioCliente.this.getApplicationContext(), FormDocumento.class);
            Bundle mBundle = new Bundle();
            mBundle.putString("clinome", clinome);
            mBundle.putString("clinomepri", clinomepri);
            mBundle.putString("clicognomepri", clicognomepri);
            mBundle.putString("clicodice", clicodice);
            i.putExtras(mBundle);
            FormDettaglioCliente.this.startActivityForResult(i, 1);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void uscita() {
        Intent refresh = new Intent(this, FormRubrica.class);
        startActivity(refresh);//Start the same Activity
        finish();
    }

    @Override
    public void onBackPressed() {
        /*if (trasformato) {*/
        if (flagmodifica) {
            Uri ris = Uri.parse("content://modificacontatto/OK");
            Intent result = new Intent(Intent.ACTION_PICK, ris);
            setResult(RESULT_CANCELED, result);
            finish();

        } else {
            Uri ris = Uri.parse("content://modificacontatto/ANNULLA");
            Intent result = new Intent(Intent.ACTION_PICK, ris);
            setResult(RESULT_CANCELED, result);
            finish();

        /*} else {
            uscita();
        }*/
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return fdcp;
                }
                case 1: {
                    return fdcl;
                }
                case 2: {
                    return fdcs;
                }

            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Profilo";
                case 1:
                    return "Listino";
                case 2:
                    return "Statistiche";

            }
            return null;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {

                // Modifica contatto
                if (resultCode == Activity.RESULT_OK) {
                    Toast toast = Toast.makeText(this.getApplicationContext(), "Contatto modificato",
                            Toast.LENGTH_LONG);
                    toast.show();
                    ArrayList<HashMap> vris = (ArrayList<HashMap>) data.getExtras().get("vris");
                    for (int i = 0; i < vris.size(); i++) {
                        HashMap riga = vris.get(i);
                        clinome = (String) riga.get("clinome");
                    }
                    leggiCliente();

                    if (clicognomepri.equals("") || !clinome.equals("")) {
                        tvnominativo.setText(fdcp.clinome);
                    } else {
                        tvnominativo.setText(fdcp.clicognomepri + " " + fdcp.clinomepri);
                    }
                    fdcp.tvnomepri.setText(fdcp.clicognomepri + " " + fdcp.clinomepri);
                    fdcp.tvemail.setText(fdcp.cliemail1);
                    fdcp.tvindiaz.setText(fdcp.cliindir + ", " + fdcp.clinumciv);
                    fdcp.tvcomune.setText(fdcp.clicap + ", " + fdcp.cliloc + " (" + fdcp.cliprov + ")");
                    fdcp.tvpiva.setText(fdcp.clipiva);
                    fdcp.tvsitoaz.setText("");
                    fdcp.tvtel.setText(fdcp.clinumtel);
                    fdcp.tvcell.setText(fdcp.clinumcell);
                    if (!fdcp.clinote.equals("")) {
                        fdcp.llnote.setVisibility(View.VISIBLE);
                        fdcp.tvnotecli.setText(fdcp.clinote);
                    } else {
                        fdcp.llnote.setVisibility(View.GONE);
                    }
                    fdcp.tvgps.setText(fdcp.gps);
                    if (!fdcp.clisdi.equals("")) {
                        fdcp.tvsdi.setVisibility(View.VISIBLE);
                        fdcp.tvsdi.setText("Codice SDI " + fdcp.clisdi);
                    } else {
                        fdcp.tvsdi.setVisibility(View.GONE);
                    }
                    if (!fdcp.cliinsegna.equals("")) {
                        fdcp.tvinsegna.setVisibility(View.VISIBLE);
                        fdcp.tvinsegna.setText("Insegna " + fdcp.cliinsegna);
                    } else {
                        fdcp.tvinsegna.setVisibility(View.GONE);
                    }
                    flagmodifica = true;
                }
                break;

            }
/*            case (2): {
                if (resultCode == Activity.RESULT_CANCELED) {
                    ris = Uri.parse("content://modificacontatto/OK");
                    Intent result = new Intent(Intent.ACTION_PICK, ris);
                    setResult(RESULT_CANCELED, result);
                    finish();


                }
                break;
            }*/
        }
    }

    private void azioneModifica() {
        if (cliTrasf) {
            Toast toast = Toast.makeText(FormDettaglioCliente.this.getApplicationContext(), "Cliente già trasferito, modifica non consentita!",
                    Toast.LENGTH_LONG);
            toast.show();
        } else {
            Intent i = new Intent(FormDettaglioCliente.this.getApplicationContext(), FormNuovoCliente.class);
            Bundle mBundle = new Bundle();
            mBundle.putString("clinome", fdcp.clinome);
            mBundle.putString("clicodice", fdcp.clicodice);
            mBundle.putString("cliindir", fdcp.cliindir);
            mBundle.putString("cliloc", fdcp.cliloc);
            mBundle.putString("cliprov", fdcp.cliprov);
            mBundle.putString("clipiva", fdcp.clipiva);
            mBundle.putString("clinumtel", fdcp.clinumtel);
            mBundle.putString("clinumcell", fdcp.clinumcell);
            mBundle.putString("cliemail1", fdcp.cliemail1);
            mBundle.putString("clicognomepri", fdcp.clicognomepri);
            mBundle.putString("clinomepri", fdcp.clinomepri);
            mBundle.putString("clicodfisc", fdcp.clicodfisc);
            mBundle.putString("clinote", fdcp.clinote);
            mBundle.putString("clinumciv", fdcp.clinumciv);
            mBundle.putString("clicap", fdcp.clicap);
            mBundle.putString("clicodpag", fdcp.clicodpag);
            mBundle.putBoolean("opmodifica", true);
            mBundle.putInt("clitipo", clitipo);
            mBundle.putString("clicodsdinuovo", fdcp.clisdi);
            mBundle.putString("cliinsegna", fdcp.cliinsegna);
            i.putExtras(mBundle);
            FormDettaglioCliente.this.startActivityForResult(i, 1);
        }
    }

    private void azioneElimina() {


        AlertDialog.Builder builder = new AlertDialog.Builder(
                FormDettaglioCliente.this);
        builder.setMessage("Confermi Eliminazione Contatto Cliente?")
                .setPositiveButton("SI",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                SQLiteStatement stmt = Env.db.compileStatement("DELETE FROM clienti WHERE (clinome=? OR (clinomepri=? AND clicognomepri=?)) AND clicodice=? ");
                                stmt.clearBindings();
                                stmt.bindString(1, clinome);
                                stmt.bindString(2, clinomepri);
                                stmt.bindString(3, clicognomepri);
                                stmt.bindString(4, clicodice);
                                stmt.execute();
                                stmt.close();
                                Toast toast = Toast.makeText(FormDettaglioCliente.this.getApplicationContext(), "Contatto Cliente eliminato",
                                        Toast.LENGTH_LONG);
                                toast.show();
                                FormDettaglioCliente.this.finish();
                                Intent inent = new Intent(FormDettaglioCliente.this, FormRubrica.class);

                                // calling an activity using <intent-filter> action name
                                //  Intent inent = new Intent("com.hmkcode.android.ANOTHER_ACTIVITY");

                                startActivity(inent);


                            }
                        })
                .setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                            }
                        });
        AlertDialog ad = builder.create();
        ad.show();


    }

    private void leggiCliente() {
        clinome = clinome.replaceAll("'", "\'");
        //clinome = clinome.replace("'","\`");
        vrubrica = new ArrayList<Record>();
        String qry = "SELECT clinome,clicodice,cliindir,cliloc,cliprov," + //5
                "clipiva,clinumtel,cliemail1,clinumcell,clicognomepri," + //10
                "clinomepri,clicodfisc,clinote,clinumciv, clicap," + //15
                "clicodpadre,clicodpag,clitrasferito,cligpsnordsud,cligpsestovest, " + //20
                "cligpsnordgradi,cligpsnordmin,cligpsnordsec,cligpsestgradi,cligpsestmin, " + //25
                "clicodsdinuovo,cliinsegna " +
                "FROM clienti WHERE clicodice = '" + clicodice + "'";
        Cursor cursor = Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            Record r = new Record();
            fdcp.clinome = cursor.getString(0);
            fdcp.clicodice = cursor.getString(1);
            fdcp.cliindir = cursor.getString(2);
            fdcp.cliloc = cursor.getString(3);
            fdcp.cliprov = cursor.getString(4);
            fdcp.clipiva = cursor.getString(5);
            fdcp.clinumtel = cursor.getString(6);
            fdcp.cliemail1 = cursor.getString(7);
            fdcp.clinumcell = cursor.getString(8);
            fdcp.clicognomepri = cursor.getString(9);
            fdcp.clinomepri = cursor.getString(10);
            fdcp.clicodfisc = cursor.getString(11);
            fdcp.clinote = cursor.getString(12);
            fdcp.clinumciv = cursor.getString(13);
            fdcp.clicap = cursor.getString(14);
            fdcp.clicodpadre = cursor.getString(15);
            fdcp.clicodpag = cursor.getString(16);
            cliTrasf = cursor.getString(17) == "S";

            // gps
            if (cursor.getDouble(18) > 0 || cursor.getDouble(19) > 0 || cursor.getDouble(20) > 0 || cursor.getDouble(21) > 0 || cursor.getDouble(22) > 0 || cursor.getDouble(23) > 0) {
                cligpsn = cursor.getDouble(18) + (cursor.getDouble(19) / 60) + (cursor.getDouble(20) / 3600);
                cligpse = cursor.getDouble(21) + (cursor.getDouble(22) / 60) + (cursor.getDouble(23) / 3600);
                fdcp.gps = "N:" + Formattazione.formatta(cligpsn, "########0.000000", 0) + "\nE:" + Formattazione.formatta(cligpse, "########0.000000", 0);
                //fdcp.tvgps.setText("N:" + Formattazione.formatta(cligpsn, "########0.000000", 0) + "\nE:" + Formattazione.formatta(cligpse, "########0.000000", 0));
            } else {
                cligpsn = 0;
                cligpse = 0;
                fdcp.gps = "";
                //fdcp.tvgps.setText("");
            }
            fdcp.clisdi = cursor.getString(25);
            fdcp.cliinsegna = cursor.getString(26);
            r.insStringa("tipo", "cliente");
            vrubrica.add(r);
        }
        cursor.close();
    }

    private void azioneShop() {
        FormDettaglioCliente.this.finish();
        Intent i = new Intent(FormDettaglioCliente.this.getApplicationContext(), FormDocumento.class);
        Bundle mBundle = new Bundle();
        mBundle.putString("clicodice", clicodice);
        mBundle.putString("clicodpadre", clicodpadre);
        i.putExtras(mBundle);
        /* FormDettaglioCliente.this.startActivityForResult(i, 2);*/
        FormDettaglioCliente.this.startActivity(i);

    }

    private void azioneShare() {
        String email = fdcp.cliemail1;
        if (email.equals("")) {
            Toast toast = Toast.makeText(FormDettaglioCliente.this.getApplicationContext(), "Email non presente!.",
                    Toast.LENGTH_LONG);
            toast.show();
            return;
        }

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(FormDettaglioCliente.this);
        helpBuilder.setTitle("Cataloghi");
        //helpBuilder.setMessage("Aggiungere un nuovo contatto");

        LayoutInflater inflater = getLayoutInflater();
        View checkboxLayout = inflater.inflate(R.layout.activity_share_catalogo, null);
        helpBuilder.setView(checkboxLayout);

        lvcatalogo = checkboxLayout.findViewById(R.id.sharecatag_lista);
        // vrubrica = new ArrayList<>();
        registerForContextMenu(lvcatalogo);
        vcatalogo = new ArrayList<Record>();
        lshcatagadapter = new ListaShareCatalogoAdapter(getApplicationContext(), vcatalogo, this);
        lvcatalogo.setAdapter(lshcatagadapter);

        listaCatalogoPdf();
        CheckBox cbsel = checkboxLayout.findViewById(R.id.checkBox_sharecatag);
        ImageButton bshare = checkboxLayout.findViewById(R.id.sharecatag_mail);
        bshare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String email = fdcp.cliemail1;

                if (!email.equals("")) {
                    invioMail(email);
                } else {
                    Toast toast = Toast.makeText(FormDettaglioCliente.this.getApplicationContext(), "Email non presente!.",
                            Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
            }
        });

        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();

    }

    private void listaCatalogoPdf() {

        vcatalogo.clear();
        Cursor cursor = Env.db.rawQuery("SELECT catpdfevento,catpdfnome,catpdfdata FROM catalogopdf"
                //+ " WHERE ccognome LIKE '%" + n + "%'"
                , null);

        while (cursor.moveToNext()) {
            Record r = new Record();
            r.insStringa("catpdfevento", cursor.getString(0));
            r.insStringa("catpdfnome", cursor.getString(1));
            r.insStringa("catpdfdata", cursor.getString(2));
            vcatalogo.add(r);
        }
        cursor.close();
        //DA ELIMINARE... UTILE SOLO PER TEST!!!!!!
        Record r = new Record();
        r.insStringa("catpdfevento", "");
        r.insStringa("catpdfnome", "catalogoBirre");
        r.insStringa("catpdfdata", "0000-00-00");
        vcatalogo.add(r);


        lshcatagadapter.notifyDataSetChanged();
    }


    private void invioMail(String destinatari) {


        int c = 0;
        String[] catalogoSelezionato = new String[c];
        //final ArrayList<Record> vsel = new ArrayList();
        for (int i = 0; i < vcatalogo.size(); i++) {
            Record r = vcatalogo.get(i);
            if (r.esisteCampo("sel")) {
                c++;
            }
        }
        if (c != 0) {
            catalogoSelezionato = new String[c];
            c = 0;
            for (int i = 0; i < vcatalogo.size(); i++) {
                Record r = vcatalogo.get(i);
                if (r.esisteCampo("sel")) {
                    catalogoSelezionato[c] = r.leggiStringa("catpdfnome");
                    c++;
                }
            }
        } else {
            Toast toast = Toast.makeText(FormDettaglioCliente.this.getApplicationContext(), "Selezionare almeno un catalogo!",
                    Toast.LENGTH_LONG);
            toast.show();
            return;
        }

        ///
/*        File fx = new File("/storage/emulated/0/Android/data/jsoftware.jbeer/files");
        if (!fx.exists())
            fx.mkdir();*/

        String soggettomail = "";
        String testomail = "";
        soggettomail = "Invio Catalogo";
        testomail = "In allegato il catalogo delle nostre birre";

        //String[] maildest = new String[1];
        //maildest[0] = "serena.cecchetto@jsoftware.it";
        //maildest[0] = "salvatore.dipede@jsoftware.it";
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, destinatari);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, soggettomail);
        emailIntent.putExtra(Intent.EXTRA_TEXT, testomail);
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        emailIntent.setType("application/pdf");
        String nomepdf = "";
        for (int i = 0; i < catalogoSelezionato.length; i++) {
            nomepdf = catalogoSelezionato[i];
            File newFile = new File(getApplication().getFilesDir() + File.separator + "docs" + File.separator + nomepdf + ".pdf");
            Uri contentUri = FileProvider.getUriForFile(getApplicationContext(), "jsoftware.jbeerapp.fileprovider", newFile);
            emailIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
        }

        emailIntent.setType("message/rfc822");
        startActivity(emailIntent);

    }
}
