package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaTuttiAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Record;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentRubricaTutti.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentRubricaTutti#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentRubricaTutti extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    public ListaTuttiAdapter ltuttiadapter;
    public ListView listaTutti;
    public ArrayList<Record> vrubrica = new ArrayList<>();
    public FormRubrica frub = null;
    private FragmentRubricaContatti frcon;
    private FragmentRubricaClienti frcli;


    public FragmentRubricaTutti() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentDocumentoCliente.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentRubricaTutti newInstance(String param1, String param2) {
        FragmentRubricaTutti fragment = new FragmentRubricaTutti();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        frcon = new FragmentRubricaContatti();

        frcli = new FragmentRubricaClienti();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_rubrica_tutti, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
/*
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        } else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        listaTutti = this.getActivity().findViewById(R.id.riccli_listatut);
        // vrubrica = new ArrayList<>();
        registerForContextMenu(listaTutti);

        vrubrica = new ArrayList<Record>();
        ltuttiadapter = new ListaTuttiAdapter(this.getActivity().getApplicationContext(), vrubrica, this);
        listaTutti.setAdapter(ltuttiadapter);
        listaCompleta();


        listaTutti.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                int posselez = (int) arg3;
                if (posselez != -1) {

                    Record rec = vrubrica.get(posselez);

                    if (rec.leggiStringa("tipo") == "contatto") {
                        Intent i = new Intent(FragmentRubricaTutti.this.getActivity().getApplicationContext(), FormDettaglioContatto.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString("cognome", rec.leggiStringa("ccognome"));
                        mBundle.putString("nome", rec.leggiStringa("cnome"));
                        mBundle.putString("ragsoc", rec.leggiStringa("caziendaragsoc"));
                        mBundle.putString("codice", rec.leggiStringa("ccodice"));
                        i.putExtras(mBundle);
                        FragmentRubricaTutti.this.startActivityForResult(i, 1);
                    } else {
                        Intent i = new Intent(FragmentRubricaTutti.this.getActivity().getApplicationContext(), FormDettaglioCliente.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString("clinome", rec.leggiStringa("clinome"));
                        mBundle.putString("clinomepri", rec.leggiStringa("clinomepri"));
                        mBundle.putString("clicognomepri", rec.leggiStringa("clicognomepri"));
                        mBundle.putString("clicodice", rec.leggiStringa("clicodice"));
                        mBundle.putInt("clitipo", rec.leggiIntero("clitipo"));
                        i.putExtras(mBundle);
                        FragmentRubricaTutti.this.startActivityForResult(i, 1);
                    }
                }
            }
        });
    }

    public void listaCompleta() {
        vrubrica.clear();
        String n = "";
        n = frub.edricnome.getText().toString().trim();
        //String a = n.replaceAll("'", "\'");
        if (n.contains("'")) {
            n = n.replaceAll("'", "''");
        }

        Cursor cursor = Env.db.rawQuery("SELECT ccognome,cnome,ccodice,cemail,caziendaragsoc FROM contatti" +
                " WHERE (ccognome LIKE \"%" + n + "%\" OR cnome LIKE \"%" + n + "%\" OR caziendaragsoc LIKE \"%" + n + "%\") AND ctrasformatocliente = 'N'", null);

        while (cursor.moveToNext()) {
            Record r = new Record();
            r.insStringa("ccognome", cursor.getString(0));
            r.insStringa("caziendaragsoc", cursor.getString(4));
            r.insStringa("cnome", cursor.getString(1));
            r.insStringa("ccodice", cursor.getString(2));
            r.insStringa("cemail", cursor.getString(3));
            r.insStringa("tipo", "contatto");
            vrubrica.add(r);
        }
        cursor.close();

        cursor = Env.db.rawQuery("SELECT clinome,clicodice,cliemail1,clicognomepri,clinomepri,clitipo FROM clienti" +
                " WHERE (clinome LIKE \"%" + n + "%\" OR clinomepri LIKE \"%" + n + "%\" OR clicognomepri LIKE \"%" + n + "%\") AND clicodpadre = ''", null);
        while (cursor.moveToNext()) {
            Record r = new Record();
            r.insStringa("clinome", cursor.getString(0));
            r.insStringa("clicodice", cursor.getString(1));
            r.insStringa("cliemail", cursor.getString(2));
            r.insStringa("clicognomepri", cursor.getString(3));
            r.insStringa("clinomepri", cursor.getString(4));
            r.insIntero("clitipo", cursor.getInt(5));
            r.insStringa("tipo", "cliente");
            vrubrica.add(r);
        }
        cursor.close();
        ltuttiadapter.notifyDataSetChanged();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (0): {
/*                if (resultCode == Activity.RESULT_OK) {
                    ArrayList<String> matches = data.getStringArrayListExtra(
                            RecognizerIntent.EXTRA_RESULTS);
                    //String voice = ris.getLastPathSegment();
                    // if (matches != null && matches.size() > 0)
                    //    edricnome.setText(matches.get(0).toLowerCase());
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    ris = data.getData();
                    String esito = ris.getLastPathSegment();
                    if (esito.equals("OK")) {
                        ltuttiadapter.clear();
                        listaCompleta();
                    }
                }*/
                if (resultCode == Activity.RESULT_CANCELED) {
                    if (ris != null) {
                        ris = data.getData();
                        String esito = ris.getLastPathSegment();
                        if (esito.equals("OK")) {
                            ltuttiadapter.clear();
                            listaCompleta();
                        }
                    }

                }
                break;
            }
        }
    }
}
