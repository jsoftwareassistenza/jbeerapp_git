package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaClientiAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Record;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentRubricaClienti.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentRubricaClienti#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentRubricaClienti extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    public FormRubrica frub = null;
    public ListView listaClienti;
    public ArrayList<Record> vrubrica = new ArrayList<>();
    public ListaClientiAdapter lcliadapter;

    public FragmentRubricaClienti() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentDocumentoCliente.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentRubricaClienti newInstance(String param1, String param2) {
        FragmentRubricaClienti fragment = new FragmentRubricaClienti();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_rubrica_clienti, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
/*
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        } else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        listaClienti = this.getActivity().findViewById(R.id.riccli_listacli);
        // vrubrica = new ArrayList<>();
        registerForContextMenu(listaClienti);

        vrubrica = new ArrayList<Record>();
        lcliadapter = new ListaClientiAdapter(this.getActivity().getApplicationContext(), vrubrica, this);
        listaClienti.setAdapter(lcliadapter);
        listaclienti();
        //sptipodoc.setVisibility(View.INVISIBLE);
        //tvdoc.setVisibility(View.INVISIBLE);


        listaClienti.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                int posselez = (int) arg3;
                if (posselez != -1) {

                    Record rec = vrubrica.get(posselez);
                    Intent i = new Intent(FragmentRubricaClienti.this.getActivity().getApplicationContext(), FormDettaglioCliente.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("clinome", rec.leggiStringa("clinome"));
                    mBundle.putString("clinomepri", rec.leggiStringa("clinomepri"));
                    mBundle.putString("clicognomepri", rec.leggiStringa("clicognomepri"));
                    mBundle.putString("clicodice", rec.leggiStringa("clicodice"));
                    mBundle.putInt("clitipo", rec.leggiIntero("clitipo"));
                    i.putExtras(mBundle);
                    FragmentRubricaClienti.this.startActivityForResult(i, 1);

                }
            }
        });
    }

    public void listaclienti() {
        vrubrica.clear();
        String n = "";
        n = frub.edricnome.getText().toString().trim();
        n = n.replaceAll("'", "\'");
        Cursor cursor = Env.db.rawQuery("SELECT clinome,clicodice,clinomepri,clicognomepri,clitipo FROM clienti " +
                "WHERE (clinome LIKE \"%" + n + "%\" OR clinomepri LIKE \"%" + n + "%\" OR clicognomepri LIKE \"%" + n + "%\") AND clicodpadre = ''", null);
        while (cursor.moveToNext()) {
            Record r = new Record();
            r.insStringa("clinome", cursor.getString(0));
            r.insStringa("clicodice", cursor.getString(1));
            r.insStringa("clinomepri", cursor.getString(2));
            r.insStringa("clicognomepri", cursor.getString(3));
            r.insIntero("clitipo", cursor.getInt(4));
            vrubrica.add(r);

        }
        cursor.close();
        lcliadapter.notifyDataSetChanged();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                if (resultCode == Activity.RESULT_OK) {
                    ArrayList<String> matches = data.getStringArrayListExtra(
                            RecognizerIntent.EXTRA_RESULTS);
                    //String voice = ris.getLastPathSegment();
                    // if (matches != null && matches.size() > 0)
                    //    edricnome.setText(matches.get(0).toLowerCase());
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    //   ris = data.getData();
                    //   String esito = ris.getLastPathSegment();
                    // if (esito.equals("OK")) {
                    lcliadapter.clear();
                    listaclienti();
                    // }
                }
                break;

            }
        }
    }
}
