package jsoftware.jbeerapp.forms;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniHTTP;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;

public class FormBackupOnline extends AppCompatActivity {

    ProgressBar progress = null;
    TextView labelinfo = null;
    TextView labelinfo2 = null;
    TextView labelris1 = null;
    TextView labelris2 = null;
    TextView labelris3 = null;
    ImageView icona = null;
    ProcInvioBackup proc = null;
    boolean invioincorso = false;
    boolean chiudi = false;


    @Override
    public void onBackPressed() {
        if (!invioincorso)
            super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_backup_online);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("chiudi"))
            chiudi = getIntent().getExtras().getBoolean("chiudi");

        progress = findViewById(R.id.backuponline_progress);
        labelinfo = findViewById(R.id.backuponline_labelinfo);
        labelinfo2 = findViewById(R.id.backuponline_labelinfo2);
        labelris1 = findViewById(R.id.backuponline_labelris1);
        labelris2 = findViewById(R.id.backuponline_labelris2);
        labelris3 = findViewById(R.id.backuponline_labelris3);
        icona = findViewById(R.id.backuponline_icona);

        proc = new ProcInvioBackup();
        proc.execute();
    }

    private class ProcInvioBackup extends AsyncTask<String, HashMap, String> {
        @Override
        protected String doInBackground(String... arg) {
            invioincorso = true;
            boolean connok = false;
            String err = "";
            if (Env.tiposcambiodati == 0) {
                // sim dati
                if (FunzioniJBeerApp.dispositivoOnline(FormBackupOnline.this.getApplicationContext())) {
                    connok = true;
                } else {
                    // se attivata WIFI la disabilita
                    if (FunzioniJBeerApp.tipoConnessione(FormBackupOnline.this.getApplicationContext()).equals("WIFI")) {
                        HashMap h = new HashMap();
                        h.put("label", "Disattivazione Wi-FI...");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);
                        FunzioniJBeerApp.disattivaWiFi(FormBackupOnline.this.getApplicationContext());
                        int ntent = 0;
                        System.out.println("tipo conn:" + FunzioniJBeerApp.tipoConnessione(FormBackupOnline.this.getApplicationContext()));
                        while (FunzioniJBeerApp.tipoConnessione(FormBackupOnline.this.getApplicationContext()).equals("WIFI") && ntent < 20) {
                            System.out.println("tipo conn:" + FunzioniJBeerApp.tipoConnessione(FormBackupOnline.this.getApplicationContext()));
                            try {
                                Thread.currentThread().sleep(1000);
                            } catch (Exception esleep) {
                            }
                            ntent++;
                        }
                    }
                    int ntentonline = 0;
                    while (!FunzioniJBeerApp.dispositivoOnline(getApplicationContext()) && ntentonline < 10) {
                        try {
                            Thread.currentThread().sleep(1000);
                        } catch (Exception esleep) {
                        }
                        ntentonline++;
                    }
                    if (FunzioniJBeerApp.dispositivoOnline(getApplicationContext())) {
                        connok = true;
                    } else {
                        err = "Dispositivo non collegato ad Internet\nImpossibile inviare i dati ora";
                        HashMap h = new HashMap();
                        h.put("label", "");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "N");
                        h.put("indeterminate", "N");
                        h.put("error", err);
                        this.publishProgress(h);
                        invioincorso = false;
                    }
                }
            } else {
                // WIFI
                if (FunzioniJBeerApp.dispositivoOnline(FormBackupOnline.this.getApplicationContext())) {
                    connok = true;
                } else {
                    boolean wifion = false;
                    if (FunzioniJBeerApp.tipoConnessione(FormBackupOnline.this.getApplicationContext()).equals("WIFI")) {
                        wifion = true;
                    } else {
                        HashMap h = new HashMap();
                        h.put("label", "Attivazione Wi-FI...");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "0");
                        h.put("max", "0");
                        h.put("visible", "S");
                        h.put("indeterminate", "S");
                        h.put("error", "");
                        this.publishProgress(h);
                        FunzioniJBeerApp.attivaWiFi(FormBackupOnline.this.getApplicationContext());
                        int ntent = 0;

                        while (!FunzioniJBeerApp.tipoConnessione(FormBackupOnline.this.getApplicationContext()).equals("WIFI") && ntent < 20) {
                            Log.v("JBEERAPP", "controllo " + ntent + " attivazione wifi");
                            try {
                                Thread.currentThread().sleep(1000);
                            } catch (Exception esleep) {
                            }
                            ntent++;
                        }
                        if (!FunzioniJBeerApp.tipoConnessione(FormBackupOnline.this.getApplicationContext()).equals("WIFI")) {
                            h = new HashMap();
                            h.put("label", "");
                            h.put("label2", "");
                            h.put("labelris1", "");
                            h.put("labelris2", "");
                            h.put("labelris3", "");
                            h.put("val", "0");
                            h.put("max", "0");
                            h.put("visible", "N");
                            h.put("indeterminate", "N");
                            h.put("error", "Attivazione Wi-FI fallita");
                            this.publishProgress(h);
                            invioincorso = false;
                        } else {
                            wifion = true;
                        }
                    }
                    if (wifion) {
                        // controlla rete
                        String ssid = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(FormBackupOnline.this.getApplicationContext());
                        if (ssid.equals(Env.ssidscambiodati)) {
                            connok = true;
                        } else {
                            HashMap h = new HashMap();
                            h.put("label", "Connessione a Wi-FI scambio dati...");
                            h.put("label2", "");
                            h.put("labelris1", "");
                            h.put("labelris2", "");
                            h.put("labelris3", "");
                            h.put("val", "0");
                            h.put("max", "0");
                            h.put("visible", "S");
                            h.put("indeterminate", "S");
                            h.put("error", "");
                            this.publishProgress(h);
                            FunzioniJBeerApp.connettiReteWiFi(getApplicationContext(), Env.ssidscambiodati);
                            String rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                            int ntent = 0;
                            while (!rc.equals(Env.ssidscambiodati) && ntent < 30) {
                                Log.v("JBEERAPP", "tentativo " + ntent + " connessione " + Env.ssidscambiodati);
                                FunzioniJBeerApp.connettiReteWiFi(getApplicationContext(), Env.ssidscambiodati);
                                try {
                                    Thread.currentThread().sleep(1000);
                                } catch (Exception esleep) {
                                }
                                rc = FunzioniJBeerApp.leggiSSIDConnessioneWiFi(getApplicationContext());
                                if (rc.equals(Env.ssidscambiodati))
                                    connok = true;
                                ntent++;
                            }
                            if (!connok) {
                                h = new HashMap();
                                h.put("label", "");
                                h.put("label2", "");
                                h.put("labelris1", "");
                                h.put("labelris2", "");
                                h.put("labelris3", "");
                                h.put("val", "0");
                                h.put("max", "0");
                                h.put("visible", "N");
                                h.put("indeterminate", "N");
                                h.put("error", "Connessione Wi-FI a rete " + Env.ssidscambiodati + " fallita");
                                this.publishProgress(h);
                                invioincorso = false;
                            }
                        }
                    }
                }
            }

            if (connok) {
                try {
                    Thread.currentThread().sleep(2000);
                } catch (Exception esleep) {
                }

                FunzioniJBeerApp.salvaLog("Eseguito Backup Online");

                Cursor cursorpar = Env.db
                        .rawQuery(
                                "SELECT pedcod,user,pwd,serverjcloud FROM datiterm",
                                null);
                cursorpar.moveToFirst();
                String tcod = cursorpar.getString(0);
                String user = cursorpar.getString(1);
                String pwd = cursorpar.getString(2);
                String server = cursorpar.getString(3);
                cursorpar.close();

                // pulizia cache
                File fcache = getApplicationContext().getCacheDir();
                File[] lfs = fcache.listFiles();
                for (int i = 0; i < lfs.length; i++) {
                    lfs[i].delete();
                }

                String dtoggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
                SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
                SimpleDateFormat dh2 = new SimpleDateFormat("HH:mm:ss");
                String ora = dh.format(new Date());

                // prepara file backup
//                HashMap h = new HashMap();
//                h.put("label", "Copia database...");
//                h.put("label2", "");
//                h.put("labelris1", "");
//                h.put("labelris2", "");
//                h.put("labelris3", "");
//                h.put("val", "100");
//                h.put("max", "100");
//                h.put("visible", "S");
//                h.put("indeterminate", "S");
//                h.put("error", "");
//                this.publishProgress(h);
//                FunzioniJazzTv.copiaFile("/data/"+ getApplicationContext().getPackageName() +"/databases/jbeerappbd.db",
//                        getApplicationContext().getCacheDir() + File.separator + "backup" + File.separator + "jbeerappbd.db");


                HashMap h = new HashMap();
                h.put("label", "Copia database...");
                h.put("label2", "");
                h.put("labelris1", "");
                h.put("labelris2", "");
                h.put("labelris3", "");
                h.put("val", "100");
                h.put("max", "100");
                h.put("visible", "S");
                h.put("indeterminate", "S");
                h.put("error", "");
                this.publishProgress(h);

                File fc = getApplicationContext().getCacheDir();
                File fdata = Environment.getDataDirectory();
                String dbFilePath = "/data/" + getApplicationContext().getPackageName() + "/databases/jbeerappbd.db";
                String copiaDBPath = "/jbeerappbd.db";
                File fcurrentDB = new File(fdata, dbFilePath);
                File fcopiaDB = new File(fc, copiaDBPath);
                FileChannel src = null;
                FileChannel dest = null;
                try {
                    src = new FileInputStream(fcurrentDB).getChannel();
                    dest = new FileOutputStream(fcopiaDB).getChannel();
                    dest.transferFrom(src, 0, src.size());
                    src.close();
                    dest.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                h = new HashMap();
                h.put("label", "Compressione file...");
                h.put("label2", "");
                h.put("labelris1", "");
                h.put("labelris2", "");
                h.put("labelris3", "");
                h.put("val", "100");
                h.put("max", "100");
                h.put("visible", "S");
                h.put("indeterminate", "S");
                h.put("error", "");
                this.publishProgress(h);

                boolean zipok = true;
                try {
                    FileOutputStream fosz = new FileOutputStream(getApplicationContext().getCacheDir() + File.separator + "db.zip");
                    ZipOutputStream zip = new ZipOutputStream(fosz);
                    zip.putNextEntry(new ZipEntry("jbeerappbd.db"));
                    FileInputStream fis = new FileInputStream(getApplicationContext().getCacheDir() + File.separator + "jbeerappbd.db");
                    while (fis.available() > 0) {
                        if (fis.available() >= 512) {
                            byte[] b = new byte[512];
                            fis.read(b);
                            zip.write(b);
                        } else {
                            byte[] b = new byte[fis.available()];
                            fis.read(b);
                            zip.write(b);
                        }
                    }
                    zip.flush();
                    fis.close();
                    zip.close();
                    fosz.close();
                } catch (Exception ezip) {
                    System.out.println("errore su creazione zip: " + ezip.getMessage());
                    zipok = false;
                }
                if (zipok) {
                        // invio file
                    String nfx = "db.zip";

                    h = new HashMap();
                    h.put("label", "Invio file " + nfx + "...");
                    h.put("label2", "");
                    h.put("labelris1", "");
                    h.put("labelris2", "");
                    h.put("labelris3", "");
                    h.put("val", "100");
                    h.put("max", "100");
                    h.put("visible", "S");
                    h.put("indeterminate", "S");
                    h.put("error", "");
                    this.publishProgress(h);

                    boolean inviook = false;

                    try {
                        ArrayList<HashMap> campiForm = new ArrayList();
                        ArrayList<HashMap> filesUpload = new ArrayList();
                        HashMap r = new HashMap();
                        r.put("nome", "user");
                        r.put("valore", user);
                        campiForm.add(r);
                        r = new HashMap();
                        r.put("nome", "pwd");
                        r.put("valore", pwd);
                        campiForm.add(r);
                        r = new HashMap();
                        r.put("nome", "proc");
                        r.put("valore", "jbeerappDB_" + Env.pedcod);
                        campiForm.add(r);
                        r = new HashMap();
                        r.put("nome", "fileUpload");
                        r.put("nomefile", getApplicationContext().getCacheDir() + File.separator + File.separator + nfx);
                        filesUpload.add(r);

                        BufferedReader bufline = FunzioniHTTP.inviaPostMultipartHttp(server + "/service/uploadbackup.do", campiForm, filesUpload, null, new Hashtable(), false, false,
                                false, "", "", 10000);
                        if (bufline != null) {
                            String s = bufline.readLine();
                            if (s != null && s.startsWith("OK"))
                                inviook = true;
                            bufline.close();
                        }
                    } catch (Exception einvio) {
                        einvio.printStackTrace();
                        inviook = false;
                    }

                    if (inviook) {
                        h = new HashMap();
                        h.put("label", "Invio backup terminato");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "100");
                        h.put("max", "100");
                        h.put("visible", "N");
                        h.put("indeterminate", "N");
                        h.put("error", "");
                        this.publishProgress(h);
                        invioincorso = false;
                    } else {
                        h = new HashMap();
                        h.put("label", "Errore su invio backup");
                        h.put("label2", "");
                        h.put("labelris1", "");
                        h.put("labelris2", "");
                        h.put("labelris3", "");
                        h.put("val", "100");
                        h.put("max", "100");
                        h.put("visible", "N");
                        h.put("indeterminate", "N");
                        h.put("error", "");
                        this.publishProgress(h);
                        invioincorso = false;
                    }
                } else {
                    h = new HashMap();
                    h.put("label", "Errore su compressione file backup");
                    h.put("label2", "");
                    h.put("labelris1", "");
                    h.put("labelris2", "");
                    h.put("labelris3", "");
                    h.put("val", "100");
                    h.put("max", "100");
                    h.put("visible", "N");
                    h.put("indeterminate", "N");
                    h.put("error", "");
                    this.publishProgress(h);
                    invioincorso = false;
                }
            }
            return "";
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(String value) {
            finish();
        }

        @Override
        protected void onProgressUpdate(HashMap... values) {

            labelinfo.setText((String) values[0].get("label"));
            labelinfo2.setText((String) values[0].get("label2"));
            labelris1.setText((String) values[0].get("labelris1"));
            labelris2.setText((String) values[0].get("labelris2"));
            labelris3.setText((String) values[0].get("labelris3"));
            progress.setMax(Formattazione.estraiIntero((String) values[0].get("max")));
            progress.setProgress(Formattazione.estraiIntero((String) values[0].get("val")));
            progress.setVisibility(values[0].get("visible").equals("S") ? View.VISIBLE : View.INVISIBLE);
            progress.setIndeterminate(values[0].get("indeterminate").equals("S"));
            String err = (String) values[0].get("error");
            if (!err.equals("")) {
                labelinfo.setText(err);
                icona.setImageResource(R.drawable.ic_error_black_48dp);
            }
        }
    }

}
