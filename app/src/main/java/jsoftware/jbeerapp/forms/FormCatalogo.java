package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaCatalogoAdapter;
import jsoftware.jbeerapp.adapters.ListaContAdapter;
import jsoftware.jbeerapp.adapters.SpinnerAdapterYellow;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.Record;

public class FormCatalogo extends AppCompatActivity {

    private TextView artdescr;
    private TextView cont;
    private boolean prezzo = true;

    private ImageButton bvoice;
    private ImageButton pdf;
    private ImageButton invio;
    private CheckBox prz;
    private ListView listaart;
    private ArrayList articoli_catalogo;
    private ArrayList contatti_scelti;
    private ArrayList<Record> vart = new ArrayList();
    private ArrayList<Record> vcont = new ArrayList();
    private ArrayList<Record> contatti = new ArrayList();
    private int posselez = -1;
    private ListaCatalogoAdapter lartadapter;
    private ListaContAdapter lcontadapter;
    private Spinner spcolore;
    private Spinner spstile;
    private Spinner spgrado;
    private ArrayList<String> lcolore;
    private ArrayList<Record> vcolore;
    private ArrayList<String> lstile;
    private ArrayList<Record> vstile;
    private ArrayList<String> lgrado;
    private ArrayList<Record> vgrado;
    private SpinnerAdapterYellow coloreadapter;
    private SpinnerAdapterYellow stileadapter;
    private SpinnerAdapterYellow gradoadapter;
    private boolean prezzi = true;
    private boolean checkati = false;
    private boolean checkatic = false;
    private String col = "Colore";
    private String sty = "Stile";
    private String grad = "Grado";
    private CheckBox radio_button;
    private CheckBox radio_button_cont;
    private CheckBox tutti;
    private CheckBox tutticont;
    public AlertDialog.Builder helpBuilder;
    public View checkboxLayout;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_catalogo);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        artdescr = findViewById(R.id.d_artcatalogo);
        bvoice = findViewById(R.id.rub_bvoice2);
        pdf = findViewById(R.id.pdf);

        prz = findViewById(R.id.prz);
        listaart = findViewById(R.id.riccatalogo_listaart);
        spcolore = findViewById(R.id.spcolore);
        spstile = findViewById(R.id.spstile);
        spgrado = findViewById(R.id.spgrado);
        radio_button = findViewById(R.id.radiook);
        tutti = findViewById(R.id.tutti);


        // colore
        lcolore = new ArrayList();
        vcolore = new ArrayList();
        lcolore.add("Colore");
        Record rg = new Record();
        rg.insStringa("cod", "...");
        vcolore.add(rg);
        Cursor c = Env.db.rawQuery("SELECT DISTINCT catcolore FROM catalogo ORDER BY catcolore", null);
        while (c.moveToNext()) {
            lcolore.add(c.getString(0));
            Record rx = new Record();
            rx.insStringa("cod", c.getString(0));
            vcolore.add(rx);
        }
        c.close();
        coloreadapter = new SpinnerAdapterYellow(FormCatalogo.this.getApplicationContext(), R.layout.spinner_item_white, lcolore);
        spcolore.setAdapter(coloreadapter);
        spcolore.setSelection(0);

        // stile
        lstile = new ArrayList();
        vstile = new ArrayList();
        lstile.add("Stile");
        rg = new Record();
        rg.insStringa("cod", "...");
        vstile.add(rg);
        c = Env.db.rawQuery("SELECT DISTINCT catstile FROM catalogo ORDER BY catstile", null);
        while (c.moveToNext()) {
            lstile.add(c.getString(0));
            Record rx = new Record();
            rx.insStringa("cod", c.getString(0));
            vstile.add(rx);
        }
        c.close();
        stileadapter = new SpinnerAdapterYellow(FormCatalogo.this.getApplicationContext(), R.layout.spinner_item_white, lstile);
        spstile.setAdapter(stileadapter);
        spstile.setSelection(0);

        registerForContextMenu(listaart);

        // grado
        lgrado = new ArrayList();
        vgrado = new ArrayList();
        lgrado.add("Grado");
        lgrado.add("2% - 4%");
        lgrado.add("4% - 6%");
        lgrado.add("6% - 8%");
        lgrado.add("8% - 10%");
        lgrado.add("10% +");
        gradoadapter = new SpinnerAdapterYellow(FormCatalogo.this.getApplicationContext(), R.layout.spinner_item_white, lgrado);
        spgrado.setAdapter(gradoadapter);
        spgrado.setSelection(0);

        registerForContextMenu(listaart);

        vart = new ArrayList<Record>();
        lartadapter = new ListaCatalogoAdapter(this.getApplicationContext(), vart, this);
        listaart.setAdapter(lartadapter);
        if (Env.depeventocod.equals("")) {
            aggiornaLista(true, false);
        } else {
            aggiornaListaEvento(true, false);
        }


        artdescr.requestFocus();



       /* listaart.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                int posselez = (int) arg3;
                if (posselez != -1) {

                    Record rec = vart.get(posselez);
                    Intent i = new Intent(FormCatalogo.this.getApplicationContext(), FormDettaglioArticolo.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("artcod", rec.leggiStringa("artcod"));
                    i.putExtras(mBundle);
                    FormCatalogo.this.startActivity(i);

                }
            }
        });*/


        spcolore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                col = (String) spcolore.getSelectedItem();
                if (Env.depeventocod.equals("")) {
                    aggiornaLista(prezzi, false);
                } else {
                    aggiornaListaEvento(prezzi, false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spstile.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                sty = (String) spstile.getSelectedItem();
                if (Env.depeventocod.equals("")) {
                    aggiornaLista(prezzi, false);
                } else {
                    aggiornaListaEvento(prezzi, false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spgrado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                grad = (String) spgrado.getSelectedItem();
                if (Env.depeventocod.equals("")) {
                    aggiornaLista(prezzi, false);
                } else {
                    aggiornaListaEvento(prezzi, false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        bvoice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Articolo...");
                startActivityForResult(intent, 1);
            }
        });

        pdf.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                articoli_catalogo = preparaDati();

                if (articoli_catalogo.size() > 0) {
                    showPopUp();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(FormCatalogo.this);
                    builder.setMessage("Selezionare almeno un articolo")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {

                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
            }
        });

        tutti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkati = buttonView.isChecked();
                if (Env.depeventocod.equals("")) {
                    aggiornaLista(prezzi, checkati);
                } else {
                    aggiornaListaEvento(prezzi, checkati);
                }
            }
        });

        prz.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                prezzi = buttonView.isChecked();
                if (Env.depeventocod.equals("")) {
                    aggiornaLista(prezzi, false);
                } else {
                    aggiornaListaEvento(prezzi, false);
                }
            }
        });

/*        artdescr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                aggiornaLista(true, false);
                return false;
            }
        });*/

        artdescr.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                //if (!artdescr.getText().toString().trim().contains("'")) {
                    if (Env.depeventocod.equals("")) {
                        aggiornaLista(true, false);
                    } else {
                        aggiornaListaEvento(true, false);
                    }
/*                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Carattere non consentito!",
                            Toast.LENGTH_SHORT);
                    toast.show();
                }*/
            }
        });


        listaart.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                /* String tmp = (String) vart.get(arg2).leggiStringa("descrizione");
                Uri codselez = Uri.parse("content://ricart/" + tmp.replace('/', '='));
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                result.putExtra("bloccato", (String) vart.get(arg2).leggiStringa("bloccato"));
                setResult(RESULT_OK, result);
                finish();*/
                int posselez = (int) arg3;
                if (posselez != -1) {

                    Record rec = vart.get(posselez);
                    Intent i = new Intent(FormCatalogo.this.getApplicationContext(), FormDettaglioArticolo.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("artcod", rec.leggiStringa("artcod"));
                    mBundle.putString("giacArt", rec.leggiStringa("giacArt"));
                    mBundle.putString("catnomeart", rec.leggiStringa("catnomeart"));
                    mBundle.putString("catstile", rec.leggiStringa("catstile"));
                    mBundle.putString("catcolore", rec.leggiStringa("catcolore"));
                    mBundle.putString("catnote", rec.leggiStringa("catnote"));
                    mBundle.putString("nomeimg", rec.leggiStringa("nomeimg"));
                    //mBundle.putString("ch", rec.leggiStringa("ch"));
                    mBundle.putString("artcauzcod", rec.leggiStringa("artcauzcod"));
                    mBundle.putString("catgrado", rec.leggiStringa("catgrado"));
                    mBundle.putString("cauzdescr", rec.leggiStringa("cauzdescr"));
                    mBundle.putString("prz", rec.leggiStringa("prz"));
                    i.putExtras(mBundle);
                    startActivity(i);

                }
            }
        });


/*        listaart.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
                posselez = (int) arg2;
                return false;
            }
        });*/

    }


    private void showPopUp() {

        LayoutInflater inflater = getLayoutInflater();
        checkboxLayout = inflater.inflate(R.layout.activity_sel_contatto, null);

        ListView listacont = checkboxLayout.findViewById(R.id.riccont_lista);
        registerForContextMenu(listacont);

        contatti = new ArrayList<Record>();
        lcontadapter = new ListaContAdapter(this.getApplicationContext(), contatti, this);
        listacont.setAdapter(lcontadapter);

        helpBuilder = new AlertDialog.Builder(FormCatalogo.this);
        helpBuilder.setTitle("Selezionare contatto");

        helpBuilder.setView(checkboxLayout);
        cont = checkboxLayout.findViewById(R.id.contatto);
        tutticont = checkboxLayout.findViewById(R.id.tuttic);
        invio = checkboxLayout.findViewById(R.id.invio);
        radio_button_cont = checkboxLayout.findViewById(R.id.radiookc);

        aggiornaListaContatti(checkatic);

        invio.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void onClick(View v) {
                contatti_scelti = preparaContatti();
                if (contatti_scelti.size() > 0) {

                    File fx = new File(getApplicationContext().getFilesDir() + File.separator + "docs");
                    if (!fx.exists()) {
                        fx.mkdir();
                    }
/*                    else{
                        String deleteCmd = "rm -r " + getApplicationContext().getFilesDir() + File.separator + "cat";
                        Runtime runtime = Runtime.getRuntime();
                        try {
                            runtime.exec(deleteCmd);
                        } catch (IOException e) { }
                        fx.mkdir();
                    }*/


/*                    File fx = new File(getApplicationContext().getFilesDir() + File.separator + "docs" + File.separator);
                    if (!fx.exists())
                        fx.mkdir();*/


                    //boolean pdfok = FunzioniJBeerApp.generaPDFDocumentoCatalogo(articoli_catalogo, getApplicationContext(), "catalogoBirre.pdf");
                    boolean pdfok = FunzioniJBeerApp.generaPDFDocumentoCatalogo(articoli_catalogo, getApplicationContext());

                    if (pdfok) {
                        String soggettomail = "";
                        String testomail = "";
                        soggettomail = "Invio Catalogo";
                        testomail = "In allegato il catalogo delle nostre birre";

                        String[] maildest = new String[contatti_scelti.size()];
                        for (int u = 0; u < contatti_scelti.size(); u++) {
                            Record ru = (Record) contatti_scelti.get(u);
                            maildest[u] = ru.leggiStringa("mail");
                        }
                        try {
                            Intent emailIntent = new Intent(Intent.ACTION_SEND);
                            emailIntent.putExtra(Intent.EXTRA_BCC, maildest);
                            emailIntent.putExtra(Intent.EXTRA_SUBJECT, soggettomail);
                            emailIntent.putExtra(Intent.EXTRA_TEXT, testomail);
                            emailIntent.setType("application/pdf");
                            File newFile = new File(fx + File.separator + "catalogoBirre.pdf");
                            //File newFile = new File(getApplication().getFilesDir() + File.separator + "docs" + File.separator + "catalogoBirre.pdf");
                            Date lastModDate = new Date(newFile.lastModified());
                            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                            String dataOggi = date.format(lastModDate);
                            Cursor cursor = Env.db.rawQuery("SELECT catpdfnome FROM catalogopdf"
                                    + " WHERE catpdfnome = 'catalogoBirre'", null);
                            if (cursor.moveToNext()) {
                                // aggiorna la data del catalogo
                                SQLiteStatement sta = Env.db.compileStatement(
                                        "UPDATE catalogopdf SET catpdfdata = ? WHERE catpdfnome = ?");
                                sta.clearBindings();
                                sta.bindString(1, dataOggi);
                                sta.bindString(2, "catalogoBirre");
                                sta.execute();
                                sta.close();
                            } else {
                                SQLiteStatement sti = Env.db.compileStatement(
                                        "insert into catalogopdf (catpdfnome,catpdfdata) " +
                                                "VALUES (?,?)");
                                sti.clearBindings();
                                sti.bindString(1, "catalogoBirre");
                                sti.bindString(2, dataOggi);
                                sti.execute();
                                sti.close();
                            }
                            cursor.close();

                            Uri contentUri = FileProvider.getUriForFile(getApplicationContext(), "jsoftware.jbeerapp.fileprovider", newFile);
                            emailIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                            //FileProvider.getUriForFile(getApplicationContext(), getPackageName()+".fileprovider", mediaFile)
                            emailIntent.setType("message/rfc822");
                            startActivity(emailIntent);
                        } catch (Exception exmail) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(FormCatalogo.this);
                            builder.setMessage("Nessuna APP di gestione Email installata")
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int id) {
                                                }
                                            });
                            AlertDialog ad = builder.create();
                            ad.setCancelable(false);
                            ad.show();
                        }
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(FormCatalogo.this);
                        builder.setMessage("Errore durante la generazione del PDF")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                finish();
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                    }


                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(FormCatalogo.this);
                    builder.setMessage("Selezionare almeno un contatto")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {

                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                }
                //se esito è positivo generato il pdf
                //si deve fare l'pdate o l'ibnsert sul db catalogopdf
                //con i campi presenti sul db
            }
        });

        tutticont.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkatic = buttonView.isChecked();
                aggiornaListaContatti(checkatic);
            }
        });

/*        cont.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                aggiornaListaContatti(false);
                return false;
            }
        });*/

        cont.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                aggiornaListaContatti(false);
            }
        });

        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(null);
        menu.add(0, v.getId(), 0, "Seleziona articolo");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.form_ricerca_clienti, menu);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                if (resultCode == Activity.RESULT_OK) {
                    ArrayList<String> matches = data.getStringArrayListExtra(
                            RecognizerIntent.EXTRA_RESULTS);
                    if (matches != null && matches.size() > 0)
                        artdescr.setText(matches.get(0).toLowerCase());
                }
                break;
            }
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals("Seleziona articolo")) {
            String tmp = vart.get(posselez).leggiStringa("artdescr");
            Uri codselez = Uri.parse("content://ricart/" + tmp);
            Intent result = new Intent(Intent.ACTION_PICK, codselez);
            result.putExtra("bloccato", vart.get(posselez).leggiStringa("bloccato"));
            setResult(RESULT_OK, result);
            finish();
        } else {
            return false;
        }
        return true;
    }


    private void aggiornaListaContatti(boolean checkatic) {
        vcont.clear();
        String scont = cont.getText().toString().trim();
        scont.replace("'", "''");
        String qry = "SELECT clinome,cliemail1 FROM clienti WHERE cliemail1 <> ''";
        if (!scont.equals("")) {
            qry += " AND clinome LIKE \"%" + scont + "%\"";
        }
        Cursor cursor =
                Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            Record h = new Record();

            h.insStringa("nome", cursor.getString(0));
            h.insStringa("mail", cursor.getString(1));
            if (checkati) {
                h.insIntero("ch", 1);
            } else {
                h.insIntero("ch", 0);
            }

            vcont.add(h);
        }
        cursor.close();
        qry = "SELECT ccognome,cnome,cemail FROM contatti WHERE ccodice <> '' AND cemail<>''";
        if (!scont.equals("")) {
            qry += " AND ccognome LIKE \"%" + scont + "%\" OR cnome LIKE \"%" + scont + "%\"";
        }
        qry += " ORDER BY ccognome,cnome";
        cursor =
                Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            Record h = new Record();

            h.insStringa("nome", cursor.getString(0) + " " + cursor.getString(1));
            h.insStringa("mail", cursor.getString(2));
            vcont.add(h);
        }


        cursor.close();

        ArrayList<DataNome> dtl = new ArrayList<DataNome>();
        for (int i = 0; i < vcont.size(); i++) {
            dtl.add(new DataNome(vcont.get(i), vcont.get(i).leggiStringa("nome")));
        }

        Collections.sort(dtl);

        contatti.clear();
        for (int y = 0; y < dtl.size(); y++) {
            DataNome d = dtl.get(y);
            Record r = new Record();
            r.insStringa("nome", d.getNome());
            r.insStringa("mail", d.getContatto().leggiStringa("mail"));
            if (checkatic) {
                r.insIntero("ch", 1);
            } else {
                r.insIntero("ch", 0);
            }
            contatti.add(r);
        }


        lcontadapter.notifyDataSetChanged();


    }

    public class DataNome implements Comparable<DataNome> {

        private Record contatto;
        private String nome;

        /*
         * Comparator implementation to Sort Order object based on Amount
         */

        public class OrderByNome implements Comparator<DataNome> {

            @Override
            public int compare(DataNome o1, DataNome o2) {
                return o1.nome.compareTo(o2.nome);
            }
        }

        public DataNome(Record contatto, String nome) {
            this.contatto = contatto;
            this.nome = nome;
        }


        public Record getContatto() {
            return contatto;
        }

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }


        /*
         * Sorting on orderId is natural sorting for Order.
         */
        @Override
        public int compareTo(DataNome o) {
            return this.nome.compareTo(o.nome);
        }

        /*
         * implementing toString method to print orderId of Order
         */
        @Override
        public String toString() {
            return String.valueOf(nome);
        }
    }

    private void aggiornaLista(boolean prezzi, boolean checkati) {
        vart.clear();
        String c = artdescr.getText().toString().trim();
        c.replace("'", "''");
        String qry = "";
        if (Env.dispart) {
            qry = "SELECT catnomeart,catstile,catcolore,catgrado,catlistcod,dispqta,artcod,catnote,artcauzcod,cauzdescr " +
                    "FROM catalogo INNER JOIN articoli ON catartcod=artcod LEFT JOIN dispart ON artcod=dispartcod LEFT JOIN cauzioni ON artcauzcod=cauzcod WHERE artcod <> '' ";
        } else {
            qry = "SELECT catnomeart,catstile,catcolore,catgrado,catlistcod,giacqta,artcod,catnote,artcauzcod,cauzdescr " +
                    "FROM catalogo INNER JOIN articoli ON catartcod=artcod LEFT JOIN giacart ON artcod=giacartcod LEFT JOIN cauzioni ON artcauzcod=cauzcod WHERE artcod <> '' ";
        }
        if (!c.equals("")) {
            qry += " AND catnomeart LIKE \"%" + c + "%\"";
        }
        if (!col.equals("Colore")) {
            qry += " AND catcolore = '" + col + "'";
        }
        if (!sty.equals("Stile")) {
            qry += " AND catstile = '" + sty + "'";
        }
        if (!grad.equals("Grado")) {
            if (grad.equals("2% - 4%")) {
                qry += " AND catgrado >2 AND catgrado<= 4";
            }
            if (grad.equals("4% - 6%")) {
                qry += " AND catgrado >4 AND catgrado<= 6";
            }
            if (grad.equals("6% - 8%")) {
                qry += " AND catgrado >6 AND catgrado<= 8";
            }
            if (grad.equals("8% - 10%")) {
                qry += " AND catgrado >8 AND catgrado<= 10";
            }
            if (grad.equals("10% +")) {
                qry += " AND catgrado >10";
            }
        }
        qry += " ORDER BY catnomeart";
        Cursor cursor =
                Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            boolean artok = true;
            Record h = new Record();
            String cc = "No Prezzo";
            String prz = cc;
            if (!cursor.getString(6).equals("")) {
                String qry1 = "SELECT lprezzo1 FROM datiterm LEFT JOIN listini ON lcod = listbase WHERE lartcod='" + cursor.getString(6) + "' ORDER BY ldataval DESC LIMIT 1";
                Cursor cursor1 = Env.db.rawQuery(qry1, null);
                while (cursor1.moveToNext()) {
                    cc = Formattazione.formValuta(cursor1.getDouble(0), 12, 2, 0);
                    prz = "€ " + cc;
                }
                cursor1.close();
            }
            h.insStringa("prz", prz);
            h.insStringa("artcauzcod", cursor.getString(8));
            h.insStringa("artcod", cursor.getString(6));
            h.insStringa("catnote", cursor.getString(7));
            h.insStringa("catnomeart", cursor.getString(0));
            h.insStringa("catstile", "Stile: " + cursor.getString(1));
            h.insStringa("catcolore", "Colore: " + cursor.getString(2));
            h.insStringa("catgrado", "Grado alcolico: " + cursor.getString(3) + " %");
            h.insStringa("cauzdescr", cursor.getString(9));
            if (prezzi) {
                h.insStringa("prz", prz);
            } else {
                h.insStringa("prz", "");
            }

            double vargiac = cursor.getDouble(5);

            //controlla i movimenti - documenti effettuati ma non ancora inviati
            String qrygiac = "SELECT movtipo,movdoc,movsez,movdata,movnum" +
                    " FROM movimenti WHERE movtrasf <> 'S' ORDER BY movtipo,movdata,movnum";
            Cursor cursorgiac = Env.db.rawQuery(qrygiac, null);
            while (cursorgiac.moveToNext()) {
                String[] parsd = new String[6];
                parsd[0] = "" + cursorgiac.getInt(0);
                parsd[1] = cursorgiac.getString(1);
                parsd[2] = cursorgiac.getString(2);
                parsd[3] = cursorgiac.getString(3);
                parsd[4] = "" + cursorgiac.getInt(4);
                parsd[5] = cursor.getString(6);
                int nriga = 1;
                Cursor cd = Env.db.rawQuery(
                        "SELECT rmqta " +
                                "FROM righemov WHERE rmmovtipo=? AND rmmovdoc=? AND rmmovsez=? AND rmmovdata=? AND rmmovnum=? AND rmartcod=? ORDER BY rmriga",
                        parsd);
                while (cd.moveToNext()) {
                    vargiac = vargiac - cd.getDouble(0);
                }
                cd.close();
            }

            cursorgiac.close();


            h.insStringa("giac", "Disponibilità: " + Formattazione.formValuta(vargiac, 12, 0, Formattazione.SEGNO_SX_NEG));
            h.insStringa("giacArt", Formattazione.formValuta(cursor.getDouble(5), 12, 0, Formattazione.SEGNO_SX_NEG));
            if (checkati) {
                h.insIntero("ch", 1);
            } else {
                h.insIntero("ch", 0);
            }
            String qry2 = "SELECT ifileimg FROM imgart WHERE iartcod='" + cursor.getString(6) + "'";
            Cursor cursor2 = Env.db.rawQuery(qry2, null);
            String img = "";
            while (cursor2.moveToNext()) {
                img = cursor2.getString(0);
            }
            cursor2.close();
            h.insStringa("nomeimg", img);
            if (artok)
                vart.add(h);
        }
        cursor.close();
        lartadapter.notifyDataSetChanged();


    }

    private void aggiornaListaEvento(boolean prezzi, boolean checkati) {
        vart.clear();
        String c = artdescr.getText().toString().trim();
        c.replace("'", "''");
        String qry = "SELECT catnomeart,catstile,catcolore,catgrado,catlistcod,artgiacenza,artcod,catnote,artcauzcod,cauzdescr " +
                "FROM catalogo INNER JOIN articoli ON catartcod=artcod LEFT JOIN giacart ON artcod=giacartcod LEFT JOIN cauzioni ON artcauzcod=cauzcod WHERE artcod <> '' ";
        if (!c.equals("")) {
            qry += " AND catnomeart LIKE \"%" + c + "%\"";
        }
        if (!col.equals("Colore")) {
            qry += " AND catcolore = '" + col + "'";
        }
        if (!sty.equals("Stile")) {
            qry += " AND catstile = '" + sty + "'";
        }
        if (!grad.equals("Grado")) {
            if (grad.equals("2% - 4%")) {
                qry += " AND catgrado >2 AND catgrado<= 4";
            }
            if (grad.equals("4% - 6%")) {
                qry += " AND catgrado >4 AND catgrado<= 6";
            }
            if (grad.equals("6% - 8%")) {
                qry += " AND catgrado >6 AND catgrado<= 8";
            }
            if (grad.equals("8% - 10%")) {
                qry += " AND catgrado >8 AND catgrado<= 10";
            }
            if (grad.equals("10% +")) {
                qry += " AND catgrado >10";
            }
        }
        qry += " ORDER BY catnomeart";
        Cursor cursor =
                Env.db.rawQuery(qry, null);
        while (cursor.moveToNext()) {
            boolean artok = true;
            Record h = new Record();
            String cc = "No Prezzo";
            String prz = cc;
            if (!cursor.getString(6).equals("")) {
                String qry1 = "SELECT lprezzo1 FROM datiterm LEFT JOIN listini ON lcod = listbase WHERE lartcod='" + cursor.getString(6) + "' ORDER BY ldataval DESC LIMIT 1";
                Cursor cursor1 = Env.db.rawQuery(qry1, null);
                while (cursor1.moveToNext()) {
                    cc = Formattazione.formValuta(cursor1.getDouble(0), 12, 2, 0);
                    prz = "€ " + cc;
                }
                cursor1.close();
            }

            h.insStringa("artcauzcod", cursor.getString(8));
            h.insStringa("artcod", cursor.getString(6));
            h.insStringa("catnote", cursor.getString(7));
            h.insStringa("catnomeart", cursor.getString(0));
            h.insStringa("catstile", "Stile: " + cursor.getString(1));
            h.insStringa("catcolore", "Colore: " + cursor.getString(2));
            h.insStringa("catgrado", "Grado alcolico: " + cursor.getString(3) + " %");
            if (prezzi) {
                h.insStringa("prz", prz);
            } else {
                h.insStringa("prz", "");
            }
            h.insStringa("giac", "Disponibilità: " + Formattazione.formValuta(cursor.getDouble(5), 12, 0, Formattazione.SEGNO_SX_NEG));
            if (checkati) {
                h.insIntero("ch", 1);
            } else {
                h.insIntero("ch", 0);
            }
            h.insStringa("cauzdescr", cursor.getString(9));
            String qry2 = "SELECT ifileimg FROM imgart WHERE iartcod='" + cursor.getString(6) + "'";
            Cursor cursor2 = Env.db.rawQuery(qry2, null);
            String img = "";
            while (cursor2.moveToNext()) {
                img = cursor2.getString(0);
            }
            cursor2.close();
            h.insStringa("nomeimg", img);
            if (artok)
                vart.add(h);
        }
        cursor.close();
        lartadapter.notifyDataSetChanged();


    }

    private ArrayList<Record> preparaDati() {
        ArrayList<Record> array = new ArrayList();
        if (vart.size() > 0) {
            for (int i = 0; i < vart.size(); i++) {
                Record r = vart.get(i);
                if (r.leggiIntero("ch") == 1) {

                    Record r1 = new Record();
                    r1.insStringa("artcod", r.leggiStringa("artcod"));
                    r1.insStringa("catnomeart", r.leggiStringa("catnomeart"));
                    r1.insStringa("catnote", r.leggiStringa("catnote"));
                    r1.insStringa("catstile", r.leggiStringa("catstile"));
                    r1.insStringa("catcolore", r.leggiStringa("catcolore"));
                    r1.insStringa("catgrado", r.leggiStringa("catgrado"));
                    r1.insStringa("prz", r.leggiStringa("prz"));
                    r1.insStringa("nomeimg", r.leggiStringa("nomeimg"));
                    String cauz = "";
                    if (!r.leggiStringa("artcauzcod").equals("")) {
                        String qry = "SELECT cauzdescr FROM cauzioni WHERE cauzcod='" + r.leggiStringa("artcauzcod") + "'";
                        Cursor cursor = Env.db.rawQuery(qry, null);
                        while (cursor.moveToNext()) {
                            cauz = cursor.getString(0);
                        }
                        cursor.close();
                    }
                    r1.insStringa("cauz", cauz);
                    array.add(r1);
                }
            }
        }
        return array;

    }

    private ArrayList<Record> preparaContatti() {
        ArrayList<Record> array = new ArrayList();
        if (contatti.size() > 0) {
            for (int i = 0; i < contatti.size(); i++) {
                Record r = contatti.get(i);
                if (r.leggiIntero("ch") == 1) {

                    Record r1 = new Record();
                    r1.insStringa("mail", r.leggiStringa("mail"));
                    array.add(r1);
                }
            }
        }
        return array;

    }


}
