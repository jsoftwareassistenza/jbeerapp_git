package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

/**
 * Created by pgx71 on 19/08/2014.
 */
public class DatePickerForm extends DialogFragment implements
        DatePickerDialog.OnDateSetListener {

    public Activity activity;

    public DPCallback callback;

    public DatePickerForm() {
    }
//
//    protected DatePickerForm(Activity activity) {
//        this.activity = activity;
//    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(activity, this, year, month, day);
    }

    @Override
    public void onDateSet(android.widget.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        callback.onDialogResult(year, monthOfYear, dayOfMonth);
    }

    public interface DPCallback {
        void onDialogResult(int year, int monthOfYear, int dayOfMonth);
    }

}
