package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.RequiresApi;

import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaDocDettAdapter;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.env.funzStringa;

public class FormDocumento extends AppCompatActivity implements OnFragmentInteractionListener {
    public String clicod = ""; // cliente attuale
    public ArrayList<Record> righecrp = null; // vettore righe articoli (ogni oggetto è una hashtable contenente i campi della riga)
    public ArrayList<Record> righecrpraggr = null; // vettore righe articoli per visualizzazione carrello raggruppata
    public ArrayList<Record> righecauz = null; // vettore righe cauzioni (ogni oggetto è una hashtable contenente i campi della riga)
    public ArrayList<Record> vscad = null; // vettore righe scadenze (ogni oggetto è una hashtable contenente i campi della riga)
    public ArrayList<String> vpag = null; // vettore contenente i codici pagamento
    public ArrayList<Integer> vpagtipo = null; // vettore contenente i tipi di codici pagamento
    public int pagtvpos = -1;
    //public ArrayList<Integer> vscadute = null; // vettore contenente le scadenze rateizzate scadute
    //public ArrayList<Record> righe_nd = null; //AGOCF //prodotti non deteriorabili
    //public ArrayList<Record> righe_f = null; //AGOCF // prodotti freschi
    public ArrayList<Record> righe_nocess = null;
    public HashMap<String, ArrayList<Record>> hrighecess = null;
    public String clicodpadre = "";
    public String clicodpag = "";
    public String clicodiva = "";
    public String clicodlist = "";
    public int clinprz = 1;
    public int cliraggrddt = 1;
    public String climodprz = "S";
    public String clistampaprz = "S";
    public double clifido = 0;
    public double imppag = 0;
    public double imppagscad = 0;
    public boolean clispeseinc = false;
    public boolean clispesetrasp = false;
    public boolean clispesebol = false;
    public boolean clispesecauz = false;
    public boolean clibollaval = true;
    public boolean clibollaqta = true;
    public boolean clifattura = true;
    public boolean clicorrisp = true;
    public boolean cliddtreso = true;
    public boolean cliblocco = false;
    public boolean clivalidato = true; // V121X
    public boolean clinuovo = false;
    public boolean nuovoins = false;
    public ArrayList<String> cliscop = new ArrayList();
    public boolean avvisopagtv = false;
    public String clibloccofido = "A";
    public String clicausm = "S";
    public String clicauom = "S";
    public String clicaureso = "S";
    public String clicausost = "S";
    public double climaxscvend = 0;
    public double totaccisa = 0;
    public int clinscadblocco = 0;
    public double cliimpblocco = 0;
    public int cliggtollblocco = 0;
    public String clidtinizioblocco = "0000-00-00";
    public int cliazioneblocco = 0;
    public int clidocpref = 0;
    public double cligpsn = 0, cligpse = 0;

    public HashMap<String, Record> lcodiva = new HashMap();
    public HashMap<String, String> homaggi = new HashMap();
    public HashMap<String, String> civaomaggitot = new HashMap();
    public HashMap<String, Double> impon = new HashMap();
    public HashMap<String, Double> aliq = new HashMap();
    public HashMap<String, Double> imposta = new HashMap();
    public HashMap<String, Double> impnondetr = new HashMap();
    // variabili totalizzatori
    public double totlordomerce = 0;
    public double totsconticli = 0;
    public double totscontiart = 0;
    public double totomaggi = 0;
    public double totomaggitotali = 0;
    public double totivaomaggitotali = 0;
    public double totmerce = 0;
    public double totcorpo = 0;
    public double totimp = 0;
    public double speseinc = 0;
    public double spesebol = 0;
    public double spesetrasp = 0;
    public double totiva = 0;
    public double totdoc = 0;
    public double totaddeb = 0;
    public double totaccred = 0;
    public double totabbuoni = 0;
    public double nettoapag = 0;
    public boolean flagcalc = false;
    public String nomenuovo = "", pivanuovo = "";
    public int caucorr = 0;

    public int numdocnd = 0, numdocfr = 0;
    public String docnd = "", docfr = "";
    public int docinstampa = 0;
    public boolean ristampa = false;
    public boolean clienteok = false;
    ArrayList<Record> vdocgen = null;
    ArrayList<String> vtd = new ArrayList();
    public String datacons = "";

    public FragmentDocumentoCliente fcli;
    public FragmentDocumentoCarrello fart;
    public FragmentDocumentoRiepilogo friep;

    public int nfrag = 0;

    SpinnerAdapter ltdadapter = null;
    private String clicodice;
    private boolean clickDaclienti = false;
    private int tipodoc = 0;
    boolean aggiornamento = false;
    public Drawable yourdrawable;
    private controlloAggiornamento concar;
    public boolean modificadoc = false;
    public int movtipocorr = 0;
    public String movdoccorr = "";
    public String movdatacorr = "";
    public String movsezcorr = "";
    public String movoracorr = "";
    public int movnumcorr = 0;

    public MenuItem itemP;

    public void preparaNuovoDoc() {
        Log.v("JBeerApp", "PREPARANUOVO");
        if (FunzioniJBeerApp.leggiMessaggi().size() > 0) {
            Intent i = new Intent(getApplicationContext(), FormMessaggi.class);
            FormDocumento.this.startActivity(i);
        } else {
            //Env.contatore_backupperiodico++;
            int cnt_backupperiodico = Formattazione.estraiIntero(FunzioniJBeerApp.leggiProprieta("contatore_backupperiodico"));
            if (Env.backupperiodico > 0 && cnt_backupperiodico > 0) {
                boolean backupok = false;
                if ((Env.backupperiodico == 1 && cnt_backupperiodico % 5 == 0) ||
                        (Env.backupperiodico == 2 && cnt_backupperiodico % 10 == 0)) {
                    backupok = true;
                }
                if (backupok) {
                    Intent i = new Intent(FormDocumento.this.getApplicationContext(), FormBackupOnline.class);
                    FormDocumento.this.startActivity(i);
                }
            }
        }
        numdocnd = 0;
        numdocfr = 0;
        docnd = "";
        docfr = "";
        docinstampa = 0;
        ristampa = false;
        fcli.briccli.setEnabled(true);
        //fcli.bnuovocli.setEnabled(true);
        clicod = "";
        nomenuovo = "";
        pivanuovo = "";
        fcli.tvcliente.setText("cliente");
        fcli.edsc1.setText("");
        fcli.edsc2.setText("");
        fcli.edsc3.setText("");
        fcli.edsc1.setEnabled(true);
        fcli.edsc2.setEnabled(true);
        fcli.edsc3.setEnabled(true);
        fcli.ednrif.setText("");
        fcli.tvscopcli.setText("");
        fcli.tvemail.setText("");
        fcli.tvindiaz.setText("");
        fcli.tvcomune.setText("");
        fcli.tvpiva.setText("");
        fcli.tvtel.setText("");
        fcli.tvcell.setText("");
        fcli.tvnotecli.setText("");
        fcli.tvinsegna.setText("");
        fcli.tvsdi.setText("");

        fcli.viewclientefatt.setVisibility(View.GONE);
        fcli.viewdettaglio.setVisibility(View.INVISIBLE);
        fcli.tvclientefatt.setText("");
        clicodpadre = "";
        clicodpag = "";
        friep.sppagamento.setEnabled(true);
        clicodiva = "";
        clicodlist = "";
        clinprz = 1;
        cliraggrddt = 1;
        climodprz = "S";
        clistampaprz = "S";
        clifido = 0;
        clibloccofido = "A";
        cliblocco = false;

        clinscadblocco = 0;
        cliimpblocco = 0;
        cliggtollblocco = 0;
        clidtinizioblocco = "0000-00-00";
        cliazioneblocco = 0;
        clidocpref = 0;

        clivalidato = true; // V121X
        clinuovo = false;
        nuovoins = false;
        cliscop = new ArrayList();
        avvisopagtv = false;
        clicausm = "S";
        clicauom = "S";
        clicaureso = "S";
        clicausost = "S";
        climaxscvend = 0;
        clispeseinc = false;
        clispesebol = false;
        clispesecauz = false;
        clispesetrasp = false;
        clibollaval = true;
        clibollaqta = true;
        clifattura = true;
        clicorrisp = true;
        cliddtreso = true;
        cligpse = 0;
        cligpsn = 0;
        datacons = "";
        fcli.tvdatacons.setText("");
        //fcli.viewdettaglio.setVisibility(View.VISIBLE);

        //vscadute = new ArrayList();
        imppag = 0;
        imppagscad = 0;
        fcli.sptipodoc.setEnabled(true);
        vtd.set(1, "DDT VENDITA");
        vtd.set(2, "DDT A QUANTITA");
        vtd.set(3, "FATTURA");
        vtd.set(4, "CORRISPETTIVO");
        vtd.set(5, "DDT DI RESO");
        vtd.set(6, "ORDINE CLIENTE");

        if (Env.termordini || Env.tipoterm == Env.TIPOTERM_RACCOLTAORDINI) {
            fcli.sptipodoc.setSelection(6);
            fcli.sptipodoc.setEnabled(false);
        }

/*        else
            fcli.sptipodoc.setSelection(1);*/
        this.visualizzaDoc();

        // articoli
        righecrp = new ArrayList();
        righecrpraggr = new ArrayList();
        if (Env.vendqtatot)
            fart.lvdocdettadapter = new ListaDocDettAdapter(this.getApplicationContext(), righecrpraggr, fart);
        else
            fart.lvdocdettadapter = new ListaDocDettAdapter(this.getApplicationContext(), righecrp, fart);
        fart.listaart.setAdapter(fart.lvdocdettadapter);
        fart.lvdocdettadapter.notifyDataSetChanged();

        aggiornaNumeroArticoli();

        righe_nocess = new ArrayList();
        hrighecess = new HashMap();

        // riepilogo
        if (vpag.size() > 0)
            friep.sppagamento.setSelection(0);
        friep.tvnart.setText("");
        friep.tvtotqta.setText("");
        friep.tvspeseinc.setText("");
        friep.tvtotimp.setText("");
        friep.tvtotiva.setText("");
        friep.tvtotdoc.setText("");
        friep.tvaddeb.setText("");
        friep.tvaccred.setText("");
        friep.tvnettoapag.setText("");
        friep.edincasso.setText("");
        friep.edannot.setText("");
        friep.tvtotaccisa.setText("");
        // iniz.variabili per calcoli
        totlordomerce = 0;
        totsconticli = 0;
        totscontiart = 0;
        totomaggi = 0;
        totmerce = 0;
        totcorpo = 0;
        totimp = 0;
        speseinc = 0;
        spesebol = 0;
        spesetrasp = 0;
        totiva = 0;
        totdoc = 0;
        totaddeb = 0;
        totaccred = 0;
        totabbuoni = 0;
        nettoapag = 0;
        totaccisa = 0;
        friep.edincasso.setText("");
        friep.edincasso.setVisibility(View.INVISIBLE);
        Env.righecrptmp = new ArrayList();
        // cauzioni
        righecauz = new ArrayList();

        vscad = new ArrayList();

        caucorr = 0;

        mViewPager.setCurrentItem(0);

        ////
        if (clickDaclienti) {
            boolean bloccato = false;
            String bloca = "";
            if (Env.nscadblocco > 0) {
                double[] bloc = FunzioniJBeerApp.scadenzeScadute(clicodpadre, (new Data()), Env.ggtollblocco);
                bloccato = (bloc[1] > 0 && bloc[0] > 0 && bloc[0] > Env.impblocco);
                bloca = bloccato ? "S" : "N";

            } else {
                bloca = "N";

            }

            Uri codselez = Uri.parse("content://riccli/" + clicodice);
            Intent result = new Intent(Intent.ACTION_PICK, codselez);
            result.putExtra("bloccato", bloca);
            fcli.onActivityResult(1, -1, result);
        }


    }

    public void aggiornaRigheRaggruppate() {
        righecrpraggr.clear();
        for (int i = 0; i < righecrp.size(); i++) {
            Record r = righecrp.get(i);
            boolean trovato = false;
            for (int j = 0; j < righecrpraggr.size(); j++) {
                Record r2 = righecrpraggr.get(j);
                if (r.leggiStringa("rmartcod").equals(r2.leggiStringa("rmartcod")) &&
                        r.leggiStringa("rmcaumag").equals(r2.leggiStringa("rmcaumag")) &&
                        r.leggiDouble("rmprz") == r2.leggiDouble("rmprz") &&
                        r.leggiDouble("rmartsc1") == r2.leggiDouble("rmartsc1") &&
                        r.leggiDouble("rmartsc2") == r2.leggiDouble("rmartsc2") &&
                        r.leggiDouble("rmscvend") == r2.leggiDouble("rmscvend") &&
                        r.leggiStringa("rmcodiva").equals(r2.leggiStringa("rmcodiva")) &&
                        r.leggiStringa("cesscod").equals(r2.leggiStringa("cesscod")) &&
                        r.leggiStringa("rmlotto").equals(r2.leggiStringa("rmlotto"))) {
                    String lottoold = r2.leggiStringa("rmlotto");
/*                    if (!lottoold.contains("[" + r.leggiStringa("rmlotto") + "]")) {
                        r2.eliminaCampo("rmlotto");
                        r2.insStringa("rmlotto", lottoold + " [" + r.leggiStringa("rmlotto") + "]");
                    }*/
                    r2.insStringa("rmlotto", lottoold);
                    int colliold = r2.leggiIntero("rmcolli");
                    r2.eliminaCampo("rmcolli");
                    r2.insIntero("rmcolli", colliold + r.leggiIntero("rmcolli"));
                    double pezziold = r2.leggiDouble("rmpezzi");
                    r2.eliminaCampo("rmpezzi");
                    r2.insDouble("rmpezzi", OpValute.arrotondaMat(pezziold + r.leggiDouble("rmpezzi"), 3));
                    double contold = r2.leggiDouble("rmcontenuto");
                    r2.eliminaCampo("rmcontenuto");
                    r2.insDouble("rmcontenuto", OpValute.arrotondaMat(contold + r.leggiDouble("rmcontenuto"), 3));
                    double qold = r2.leggiDouble("rmqta");
                    r2.eliminaCampo("rmqta");
                    r2.insDouble("rmqta", OpValute.arrotondaMat(qold + r.leggiDouble("rmqta"), 3));
                    double lold = r2.leggiDouble("rmlordo");
                    r2.eliminaCampo("rmlordo");
                    r2.insDouble("rmlordo", OpValute.arrotondaMat(lold + r.leggiDouble("rmlordo"), 2));
                    double nold = r2.leggiDouble("rmnetto");
                    r2.eliminaCampo("rmnetto");
                    r2.insDouble("rmnetto", OpValute.arrotondaMat(nold + r.leggiDouble("rmnetto"), 2));
                    trovato = true;
                    break;
                }
            }
            if (!trovato) {
                Record rx = new Record();
                rx.insStringa("rmtiporiga", r.leggiStringa("rmtiporiga"));
                rx.insStringa("rmartcod", r.leggiStringa("rmartcod"));
                rx.insStringa("rmartdescr", r.leggiStringa("rmartdescr"));
                rx.insStringa("rmlotto", r.leggiStringa("rmlotto"));
                rx.insStringa("rmcaumag", r.leggiStringa("rmcaumag"));
                rx.insIntero("rmcolli", r.leggiIntero("rmcolli"));
                rx.insDouble("rmpezzi", r.leggiDouble("rmpezzi"));
                rx.insDouble("rmcontenuto", r.leggiDouble("rmcontenuto"));
                rx.insDouble("rmqta", r.leggiDouble("rmqta"));
                rx.insDouble("rmprz", r.leggiDouble("rmprz"));
                rx.insDouble("rmartsc1", r.leggiDouble("rmartsc1"));
                rx.insDouble("rmartsc2", r.leggiDouble("rmartsc2"));
                rx.insDouble("rmscvend", r.leggiDouble("rmscvend"));
                rx.insStringa("rmcodiva", r.leggiStringa("rmcodiva"));
                rx.insDouble("rmaliq", r.leggiDouble("rmaliq"));
                rx.insDouble("rmclisc1", r.leggiDouble("rmclisc1"));
                rx.insDouble("rmclisc2", r.leggiDouble("rmclisc2"));
                rx.insDouble("rmclisc3", r.leggiDouble("rmclisc3"));
                rx.insDouble("rmlordo", r.leggiDouble("rmlordo"));
                rx.insDouble("totscontiart", r.leggiDouble("totscontiart"));
                rx.insDouble("totsconticli", r.leggiDouble("totsconticli"));
                rx.insDouble("rmnetto", r.leggiDouble("rmnetto"));
                rx.insDouble("rmnettoivato", r.leggiDouble("rmnettoivato"));
                rx.insStringa("cesscod", r.leggiStringa("cesscod"));
                righecrpraggr.add(rx);
            }
            if (trovato) {
                righecrp.clear();
                righecrp.addAll(righecrpraggr);
            }
        }

    }

    public void ricalcolaTotale(ArrayList<Record> righeN) {
        if (!flagcalc) {
            aggiornaNumeroArticoli();

            totlordomerce = 0;
            totsconticli = 0;
            totscontiart = 0;
            totomaggi = 0;
            totomaggitotali = 0;
            totivaomaggitotali = 0;
            totmerce = 0;
            totcorpo = 0;
            totimp = 0;
            speseinc = 0;
            spesebol = 0;
            spesetrasp = 0;
            totiva = 0;
            totdoc = 0;
            totaddeb = 0;
            totaccred = 0;
            totaccisa = 0;
            //totabbuoni = 0;
            nettoapag = 0;
            double totqta = 0;
            int nart = 0;
            HashMap hart = new HashMap();
            for (int i = 0; i < righeN.size(); i++) {
                Record riga = righeN.get(i);
                hart.put(riga.leggiStringa("rmartcod"), "");
                String cau = riga.leggiStringa("rmcaumag");
                if (cau.equals("RV") || cau.equals("RN")) {
                    double z = riga.leggiDouble("rmprz");
                    double x = riga.leggiDouble("rmlordo");
                    double y = riga.leggiDouble("rmnetto");
                    if (y > 0 || z > 0) {
                        if (z > 0)
                            z = -z;
                        if (y > 0) {
                            y = -y;
                            x = -x;
                        }
                        riga.eliminaCampo("rmprz");
                        riga.eliminaCampo("rmlordo");
                        riga.eliminaCampo("rmnetto");
                        riga.eliminaCampo("rmnettoivato");
                        riga.insDouble("rmprz", z);
                        riga.insDouble("rmlordo", x);
                        riga.insDouble("rmnetto", y);
                        riga.insDouble("rmnettoivato", y);
                    }
                    totqta -= riga.leggiDouble("rmqta");
                    if (riga.esisteCampo("rmaccisa"))
                        totaccisa -= riga.leggiDouble("rmaccisa");
                } else {
                    totqta += riga.leggiDouble("rmqta");
                    if (riga.esisteCampo("rmaccisa"))
                        totaccisa += riga.leggiDouble("rmaccisa");
                }
                double lrd = riga.leggiDouble("rmlordo");
                double n = riga.leggiDouble("rmnetto");
                totlordomerce += OpValute.arrotondaMat(lrd, 2);
                double scli = riga.leggiDouble("totsconticli");
                totsconticli += scli;
                double sart = riga.leggiDouble("totscontiart");
                totscontiart += sart;
                if (cau.equals("OM")) {
                    totomaggi += OpValute.arrotondaMat(n, 2);
                }
            }
            // netto merce
            totmerce = totlordomerce - totsconticli - totscontiart;

            // calcolo spese incasso riba (n.rate * spese) e aggiunta a imponibili (se cliente ha spese inc.)
            String cp = vpag.get(friep.sppagamento.getSelectedItemPosition());
            String codivasi = "";
            if (this.clispeseinc && fcli.sptipodoc.getSelectedItemPosition() == 3) {
                Record rpag = Env.hcodpag.get(cp);
                if (rpag != null) {
                    this.speseinc = rpag.leggiDouble("pagspeseinc") * (double) rpag.leggiIntero("pagnrate");
                    codivasi = rpag.leggiStringa("pagcodivaspeseinc");
                }
            }

            // calcolo IVA
            lcodiva = new HashMap();
            homaggi = new HashMap();
            civaomaggitot = new HashMap();
            impon = new HashMap();
            aliq = new HashMap();
            imposta = new HashMap();
            impnondetr = new HashMap();
            // somma tutti gli imponibili
            for (int i = 0; i < righeN.size(); i++) {
                Record riga = righeN.get(i);
                String ci = riga.leggiStringa("rmcodiva");
                if (lcodiva.get(ci) == null) {
                    String[] pars = new String[1];
                    pars[0] = ci;
                    Cursor cursor = Env.db.rawQuery("SELECT ivacod,ivadescr,ivagruppo,ivaaliq,ivapercind,ivagest,ivacodivagest,ivaomaggiotot FROM codiva WHERE ivacod=?", pars);
                    if (cursor.moveToFirst()) {
                        Record ri = new Record();
                        ri.insStringa("ivacod", cursor.getString(0));
                        ri.insStringa("ivadescr", cursor.getString(1));
                        ri.insIntero("ivagruppo", cursor.getInt(2));
                        ri.insDouble("ivaaliq", cursor.getDouble(3));
                        ri.insDouble("ivapercind", cursor.getDouble(4));
                        ri.insIntero("ivagest", cursor.getInt(5));
                        ri.insStringa("ivacodivagest", cursor.getString(6));
                        ri.insStringa("ivaomaggiotot", cursor.getString(7));
                        lcodiva.put(ci, ri);
                        if (cursor.getInt(5) == 1 && !cursor.getString(6).equals("") && cursor.getString(7).equals("S") &&
                                !civaomaggitot.containsKey(ci))
                            civaomaggitot.put(ci, "");
                    }
                    cursor.close();
                }
                if (impon.containsKey(ci)) {
                    double imp = impon.get(ci);
                    impon.remove(ci);
                    imp += OpValute.arrotondaMat(riga.leggiDouble("rmnetto"), 2);
                    impon.put(ci, imp);
                } else {
                    double imp = OpValute.arrotondaMat(riga.leggiDouble("rmnetto"), 2);
                    impon.put(ci, imp);
                }
                if (!aliq.containsKey(ci))
                    aliq.put(ci, riga.leggiDouble("rmaliq"));
                // controllo omaggi
                Record riva = lcodiva.get(ci);
                if (riva.leggiIntero("ivagest") == 1 && !riva.leggiStringa("ivacodivagest").equals("")) {
                    if (!homaggi.containsKey(riva.leggiStringa("ivacodivagest")))
                        homaggi.put(riva.leggiStringa("ivacodivagest"), ci);
                    // inserisce riga iva per omaggio
                    String cio = riva.leggiStringa("ivacodivagest");
                    if (!lcodiva.containsKey(cio)) {
                        String[] pars = new String[1];
                        pars[0] = cio;
                        Cursor cursor = Env.db.rawQuery("SELECT ivacod,ivadescr,ivagruppo,ivaaliq,ivapercind,ivagest,ivacodivagest,ivaomaggiotot FROM codiva WHERE ivacod=?", pars);
                        if (cursor.moveToFirst()) {
                            Record rio = new Record();
                            rio.insStringa("ivacod", cursor.getString(0));
                            rio.insStringa("ivadescr", cursor.getString(1));
                            rio.insIntero("ivagruppo", cursor.getInt(2));
                            rio.insDouble("ivaaliq", cursor.getDouble(3));
                            rio.insDouble("ivapercind", cursor.getDouble(4));
                            rio.insIntero("ivagest", cursor.getInt(5));
                            rio.insStringa("ivacodivagest", cursor.getString(6));
                            rio.insStringa("ivaomaggiotot", cursor.getString(7));
                            lcodiva.put(cio, rio);
                        }
                        cursor.close();
                    }
                    if (impon.containsKey(cio)) {
                        double imp = impon.get(cio);
                        impon.remove(cio);
                        imp -= OpValute.arrotondaMat(riga.leggiDouble("rmnetto"), 2);
                        impon.put(cio, imp);
                    } else {
                        double imp = -OpValute.arrotondaMat(riga.leggiDouble("rmnetto"), 2);
                        impon.put(cio, imp);
                    }
                    if (!aliq.containsKey(cio))
                        aliq.put(cio, riga.leggiDouble("rmaliq"));
                }
            }
            if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
                // accisa
                for (int i = 0; i < righeN.size(); i++) {
                    Record riga = righeN.get(i);
                    if (riga.esisteCampo("rmaccisa") && riga.leggiDouble("rmaccisa") > 0) {
                        String ci = riga.leggiStringa("rmcodiva");
                        if (lcodiva.get(ci) == null) {
                            String[] pars = new String[1];
                            pars[0] = ci;
                            Cursor cursor = Env.db.rawQuery("SELECT ivacod,ivadescr,ivagruppo,ivaaliq,ivapercind,ivagest,ivacodivagest,ivaomaggiotot FROM codiva WHERE ivacod=?", pars);
                            if (cursor.moveToFirst()) {
                                Record ri = new Record();
                                ri.insStringa("ivacod", cursor.getString(0));
                                ri.insStringa("ivadescr", cursor.getString(1));
                                ri.insIntero("ivagruppo", cursor.getInt(2));
                                ri.insDouble("ivaaliq", cursor.getDouble(3));
                                ri.insDouble("ivapercind", cursor.getDouble(4));
                                ri.insIntero("ivagest", cursor.getInt(5));
                                ri.insStringa("ivacodivagest", cursor.getString(6));
                                ri.insStringa("ivaomaggiotot", cursor.getString(7));
                                lcodiva.put(ci, ri);
                            }
                            cursor.close();
                        }
                        if (impon.containsKey(ci)) {
                            double imp = impon.get(ci);
                            impon.remove(ci);
                            imp += OpValute.arrotondaMat(riga.leggiDouble("rmaccisa"), 2);
                            impon.put(ci, imp);
                        } else {
                            double imp = OpValute.arrotondaMat(riga.leggiDouble("rmaccisa"), 2);
                            impon.put(ci, imp);
                        }
                        if (!aliq.containsKey(ci))
                            aliq.put(ci, riga.leggiDouble("rmaliq"));
                    }
                }
            }
            // spese incasso
            if (this.speseinc > 0 && !codivasi.equals("")) {
                double aliqsi = 0;
                if (lcodiva.get(codivasi) == null) {
                    String[] pars = new String[1];
                    pars[0] = codivasi;
                    Cursor cursor = Env.db.rawQuery("SELECT ivacod,ivadescr,ivagruppo,ivaaliq,ivapercind,ivagest,ivacodivagest,ivaomaggiotot FROM codiva WHERE ivacod=?", pars);
                    if (cursor.moveToFirst()) {
                        Record ri = new Record();
                        ri.insStringa("ivacod", cursor.getString(0));
                        ri.insStringa("ivadescr", cursor.getString(1));
                        ri.insIntero("ivagruppo", cursor.getInt(2));
                        ri.insDouble("ivaaliq", cursor.getDouble(3));
                        aliqsi = cursor.getDouble(3);
                        ri.insDouble("ivapercind", cursor.getDouble(4));
                        ri.insIntero("ivagest", cursor.getInt(5));
                        ri.insStringa("ivacodivagest", cursor.getString(6));
                        ri.insStringa("ivaomaggiotot", cursor.getString(7));
                        lcodiva.put(codivasi, ri);
                    }
                    cursor.close();
                } else {
                    Record rii = lcodiva.get(codivasi);
                    aliqsi = rii.leggiDouble("ivaaliq");
                }
                if (impon.containsKey(codivasi)) {
                    double imp = impon.get(codivasi);
                    impon.remove(codivasi);
                    imp += OpValute.arrotondaMat(speseinc, 2);
                    impon.put(codivasi, imp);
                } else {
                    impon.put(codivasi, speseinc);
                }
                if (!aliq.containsKey(codivasi))
                    aliq.put(codivasi, aliqsi);
            }
            // arrotondamento matematico imponibili
            HashMap<String, Double> htmp = new HashMap();
            Iterator it = impon.keySet().iterator();
            while (it.hasNext()) {
                String ci = (String) it.next();
                double imp = impon.get(ci);
                imp = OpValute.arrotondaMat(imp, 2);
                htmp.put(ci, imp);
            }
            impon = htmp;

            // rilegge tutti gli id iva e ripiena altri campi
            it = impon.keySet().iterator();
            while (it.hasNext()) {
                String ci = (String) it.next();
                boolean civaOmaggiTot = false;
                if (civaomaggitot.containsKey(ci))
                    civaOmaggiTot = true;
                Record ri = lcodiva.get(ci);
                // calcola imposta
                int tipoImp = ri.leggiIntero("ivagruppo");
                double val = 0;
                double valInd = 0;
                if (tipoImp <= 1) {
                    val = impon.get(ci);
                    double al = aliq.get(ci);
                    val = OpValute.arrotondaMat(val * al / 100, 2);
                    if (ri.leggiDouble("ivapercind") > 0) {
                        valInd = OpValute.arrotondaMat(val * ri.leggiDouble("ivapercind") / 100, 2);
                        val = OpValute.arrotondaMat(val - valInd, 2);
                        double tmp = 0;
                        if (ri.leggiDouble("ivapercind") == 50 && valInd < val) {
                            tmp = val;
                            val = valInd;
                            valInd = tmp;
                        }
                    } else
                        valInd = 0;
                } else {
                    val = 0;
                    valInd = 0;
                }
                if (civaOmaggiTot)
                    totivaomaggitotali += (val + valInd);
                imposta.put(ci, val);
                impnondetr.put(ci, valInd);
            }
            // somma imponibili
            it = impon.keySet().iterator();
            while (it.hasNext()) {
                String ci = (String) it.next();
                double imp = impon.get(ci);
                totimp += imp;
            }
            totimp = OpValute.arrotondaMat(totimp, 2);
            // aggiorna totali iva
            it = impon.keySet().iterator();
            while (it.hasNext()) {
                String ci = (String) it.next();
                Record ri = lcodiva.get(ci);
                totiva += imposta.get(ci) + impnondetr.get(ci);
            }
            totiva = OpValute.arrotondaMat(totiva, 2);
            totcorpo = totimp;

            // addebiti e accrediti cauzionabili
            totaddeb = 0;
            totaccred = 0;

            totdoc = OpValute.arrotondaMat(totimp + totiva, 2);
            nettoapag = OpValute.arrotondaMat(totdoc + totaddeb - totaccred - totivaomaggitotali, 2);

            // calcolo scadenze
            vscad = new ArrayList();
            Record rpag2 = Env.hcodpag.get(cp);
            // calcolo singola rata
            double imprata = OpValute.arrotondaMat(nettoapag / (double) rpag2.leggiIntero("pagnrate"), 2);
            double diff = OpValute.arrotondaMat(nettoapag - (imprata * (double) rpag2.leggiIntero("pagnrate")), 2);
            // differenza su prima rata
            double primarata = imprata;
            if (diff != 0)
                primarata = OpValute.arrotondaMat(imprata + diff, 2);


            Data oggi = new Data();
            int ggprec = 0;
            for (int nr = 1; nr <= rpag2.leggiIntero("pagnrate"); nr++) {
                int gg = rpag2.leggiIntero("paggg" + nr) - ggprec;
                if (gg != 0 && gg % 30 == 0) {
                    // multiplo di 30 -> giorni commerciali
                    int m = oggi.mese();
                    int a = oggi.anno();
                    int am = gg / 30;
                    m += am;
                    if (m > 12) {
                        m -= 12;
                        a++;
                    }
                    oggi = new Data(oggi.giorno(), m, a);
                } else {
                    oggi.incrementaGiorni(rpag2.leggiIntero("paggg" + nr));
                }
                // controllo fine mese
                if (rpag2.leggiStringa("pagtipocalc").equals("FM")) {
                    String datas = oggi.formatta(Data.AAAA_MM_GG, "-");
                    int mese = Formattazione.estraiIntero(datas.substring(5, 7));
                    if (mese == 1 || mese == 3 || mese == 5 || mese == 7 || mese == 8 || mese == 10 || mese == 12) {
                        datas = "31/" + Formattazione.formatta(mese, "00", Formattazione.NO_SEGNO) + "/" + datas.substring(0, 4);
                    } else if (mese == 4 || mese == 6 || mese == 9 || mese == 11) {
                        datas = "30/" + Formattazione.formatta(mese, "00", Formattazione.NO_SEGNO) + "/" + datas.substring(0, 4);
                    } else {
                        datas = "28/" + Formattazione.formatta(mese, "00", Formattazione.NO_SEGNO) + "/" + datas.substring(0, 4);
                    }
                    oggi = new Data(datas, Data.GG_MM_AAAA);
                }
                Record rscad = new Record();
                if (nr == 1)
                    rscad.insDouble("importo", primarata);
                else
                    rscad.insDouble("importo", imprata);
                rscad.insStringa("data", oggi.formatta(Data.AAAA_MM_GG, "-"));
                vscad.add(rscad);
                ggprec = rpag2.leggiIntero("paggg" + nr);
            }
            // dati a video
            friep.tvtotqta.setText(Formattazione.formatta(totqta, "########0.000", Formattazione.SEGNO_DX));
            friep.tvnart.setText("" + hart.size());
            friep.tvspeseinc.setText(Formattazione.formatta(speseinc, "########0.00", Formattazione.SEGNO_DX));
            friep.tvtotimp.setText(Formattazione.formatta(totimp, "########0.00", Formattazione.SEGNO_DX));
            friep.tvtotiva.setText(Formattazione.formatta(totiva, "########0.00", Formattazione.SEGNO_DX));
            friep.tvtotdoc.setText(Formattazione.formatta(totdoc, "########0.00", Formattazione.SEGNO_DX));
            friep.tvaddeb.setText(Formattazione.formatta(totaddeb, "########0.00", Formattazione.SEGNO_DX));
            friep.tvaccred.setText(Formattazione.formatta(totaccred, "########0.00", Formattazione.SEGNO_DX));
            friep.tvnettoapag.setText(Formattazione.formatta(nettoapag, "########0.00", Formattazione.SEGNO_DX));
            friep.tvtotaccisa.setText(Formattazione.formatta(totaccisa, "########0.00", Formattazione.SEGNO_DX));

        }
    }

    public void svuotaDatiCliente() {
        fcli.tvcliente.setText("cliente");
        fcli.edsc1.setText("");
        fcli.edsc2.setText("");
        fcli.edsc3.setText("");
        fcli.ednrif.setText("");
        fcli.tvscopcli.setText("");
        fcli.tvemail.setText("");
        fcli.tvindiaz.setText("");
        fcli.tvcomune.setText("");
        fcli.tvpiva.setText("");
        fcli.tvtel.setText("");
        fcli.tvcell.setText("");
        fcli.tvnotecli.setText("");
        fcli.tvinsegna.setText("");
        fcli.tvsdi.setText("");


        fcli.viewclientefatt.setVisibility(View.GONE);

        fcli.tvcliente.setText("");
        fcli.tvclientefatt.setText("");


        clicod = "";
        clicodpadre = "";
        clicodpag = "";
        clicodiva = "";
        clicodlist = "";
        clinprz = 1;
        cliraggrddt = 1;
        climodprz = "S";
        clistampaprz = "S";
        clifido = 0;
        clibloccofido = "A";
        cliblocco = false;
        clinuovo = false;
        clivalidato = true; // V121X
        clinscadblocco = 0;
        cliazioneblocco = 0;
        clidocpref = 0;
        cliimpblocco = 0;
        cliggtollblocco = 0;
        clidtinizioblocco = "0000-00-00";
        cliazioneblocco = 0;
        clidocpref = 0;
        nuovoins = false;
        cliscop = new ArrayList();
        avvisopagtv = false;
        clicausm = "S";
        clicauom = "S";
        clicaureso = "S";
        clicausost = "S";
        climaxscvend = 0;
        clispeseinc = false;
        clispesebol = false;
        clispesetrasp = false;
        clispesecauz = false;
        clibollaval = true;
        clibollaqta = true;
        clifattura = true;
        clicorrisp = true;
        cliddtreso = true;

        friep.tvspeseinc.setVisibility(View.VISIBLE);
        friep.tvtotimp.setVisibility(View.VISIBLE);
        friep.tvtotiva.setVisibility(View.VISIBLE);
        friep.tvtotdoc.setVisibility(View.VISIBLE);
        friep.tvnettoapag.setVisibility(View.VISIBLE);
        friep.tvaddeb.setVisibility(View.VISIBLE);
        friep.tvaccred.setVisibility(View.VISIBLE);
        if (Env.calcaccisa) {
            friep.lltotaccisa.setVisibility(View.VISIBLE);
        } else {
            friep.lltotaccisa.setVisibility(View.GONE);
        }
    }

    public void visualizzaDatiCliente() {
        clienteok = true;
        ArrayList<String> vmsgavviso = new ArrayList();
        if (!this.clicod.equals("")) {
            boolean luogodest = false;
            String[] pars = new String[1];
            pars[0] = clicod;
            final Cursor cursor = Env.db.rawQuery(
                    "SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva,clicodpadre,clicodpag," + //8
                            "clicodiva,clinumtel,clinumcell,clinumfax,clicodlist,clinprz,cliraggrddt,climodprz," + //8
                            "clistampaprz,clisc1,clisc2,clisc3,climag,clifido,clibloccofido,clicausm," + //8
                            "clicauom,clicaureso,clicausost,climaxscvend,clispeseinc,clispesebol,clispesetrasp,clispesecauz," + //8
                            "climodpag,clibollaval,clibollaqta,clifattura,clicorrisp,cliddtreso,cliblocco,clinuovo," + //8
                            "clivalidato,clinscadblocco,cliimpblocco,cliggtollblocco,clidtblocco,cliazioneblocco,cligpsnordsud,cligpsestovest," + //8
                            "cligpsnordgradi,cligpsnordmin,cligpsnordsec,cligpsestgradi,cligpsestmin,cligpsestsec,clidocpref,cliemail1, " + //8
                            "clinumciv,clicap,clinote,clicodsdinuovo,cliinsegna " +
                            "FROM clienti WHERE clicodice = ?", pars);
            if (cursor.moveToFirst()) {
                clicodpadre = cursor.getString(6);
                if (!clicodpadre.equals(""))
                    luogodest = true;
                clicodpag = cursor.getString(7);
                clicodiva = cursor.getString(8);
                clicodlist = cursor.getString(12);
                clinprz = cursor.getInt(13);
                cliraggrddt = (cursor.getString(14).equals("S") ? 1 : 0);
                climodprz = cursor.getString(15);
                clistampaprz = cursor.getString(16);
                clifido = cursor.getDouble(21);
                clibloccofido = cursor.getString(22);
                clicausm = cursor.getString(23);
                clicauom = cursor.getString(24);
                clicaureso = cursor.getString(25);
                clicausost = cursor.getString(26);
                climaxscvend = cursor.getFloat(27);
                clispeseinc = (cursor.getString(28).equals("S"));
                clispesebol = (cursor.getString(29).equals("S"));
                clispesetrasp = (cursor.getString(30).equals("S"));
                clispesecauz = (cursor.getString(31).equals("S"));
                clibollaval = (cursor.getString(33).equals("S"));
                clibollaqta = (cursor.getString(34).equals("S"));
                clifattura = (cursor.getString(35).equals("S"));
                clicorrisp = (cursor.getString(36).equals("S"));
                cliddtreso = (cursor.getString(37).equals("S"));
                clinuovo = (cursor.getInt(39) == 1);
                clivalidato = (cursor.getString(40).equals("S"));

                clinscadblocco = cursor.getInt(41);
                cliimpblocco = cursor.getDouble(42);
                cliggtollblocco = cursor.getInt(43);
                clidtinizioblocco = cursor.getString(44);
                if (clidtinizioblocco.equals(""))
                    clidtinizioblocco = "0000-00-00";
                cliazioneblocco = cursor.getInt(45);
                clidocpref = cursor.getInt(54);
                fcli.tvemail.setText(cursor.getString(55));
                fcli.tvindiaz.setText(cursor.getString(2) + ", " + cursor.getString(56));
                fcli.tvcomune.setText(cursor.getString(57) + ", " + cursor.getString(3) + " (" + cursor.getString(4) + ")");
                fcli.tvpiva.setText("p.IVA:" + cursor.getString(5));
                fcli.tvtel.setText(cursor.getString(9));
                fcli.tvcell.setText(cursor.getString(10));
                if (!cursor.getString(58).equals("")) {
                    fcli.tvnotecli.setText(cursor.getString(58));
                } else {
                    fcli.llnote.setVisibility(View.GONE);
                }
                if (!cursor.getString(59).equals("")) {
                    fcli.tvsdi.setText(cursor.getString(59));
                } else {
                    fcli.tvsdi.setVisibility(View.GONE);
                }
                if (!cursor.getString(60).equals("")) {
                    fcli.tvnotecli.setText(cursor.getString(60));
                } else {
                    fcli.tvnotecli.setVisibility(View.GONE);
                }

                if (clibollaval)
                    vtd.set(1, "DDT VENDITA");
                else
                    vtd.set(1, "...");
                if (clibollaqta)
                    vtd.set(2, "DDT A QUANTITA");
                else
                    vtd.set(2, "...");
                if (clifattura)
                    vtd.set(3, "FATTURA");
                else
                    vtd.set(3, "...");
                if (clicorrisp)
                    vtd.set(4, "CORRISPETTIVO");
                else
                    vtd.set(4, "...");
                if (cliddtreso)
                    vtd.set(5, "DDT DI RESO");
                else
                    vtd.set(5, "...");
                ltdadapter.notifyDataSetChanged();


                String infocli = cursor.getString(1);
                String infoclifatt = "";
/*                        + "\r\n"
                        + cursor.getString(2) + "\r\n"
                        + cursor.getString(3) + " " + cursor.getString(4) + "\r\n"
                        + "p.IVA:" + cursor.getString(5) + "\r\n"*/
                if (luogodest) {
                    String[] parsp = new String[1];
                    parsp[0] = clicodpadre;
                    Cursor cpadre = Env.db.rawQuery(
                            "SELECT clicodice,clinome FROM clienti WHERE clicodice = ?", parsp);

                    if (cpadre.moveToFirst()) {
                        infoclifatt = cpadre.getString(1) + "\r\n";
                        fcli.viewclientefatt.setVisibility(View.VISIBLE);
                        infocli = cursor.getString(0);
                        //infocli += "Cliente fatt.:" + cpadre.getString(0) + " " + cpadre.getString(1) + "\r\n";
                    }
                    cpadre.close();
                } else {
                    fcli.viewclientefatt.setVisibility(View.GONE);
                }
                /*if (!cursor.getString(9).equals(""))
                    infocli += "tel." + cursor.getString(9) + " ";
                if (!cursor.getString(10).equals(""))
                    infocli += "cel." + cursor.getString(10) + " ";
                if (!cursor.getString(8).equals(""))
                    infocli += "Cod.IVA:" + cursor.getString(8) + " ";
                if (clifido > 0)
                    infocli += "Fido:" + Formattazione.formatta(clifido, "#########0.00", Formattazione.NO_SEGNO) + " ";*/
                fcli.tvcliente.setText(infocli);
                fcli.tvclientefatt.setText(infoclifatt);

                if (cursor.getDouble(17) != 0) {
                    fcli.edsc1.setText(Formattazione.formatta(cursor.getDouble(17), "##0.00", Formattazione.NO_SEGNO).replaceAll(",", "."));
                } else
                    fcli.edsc1.setText("");
                if (cursor.getDouble(18) != 0) {
                    fcli.edsc2.setText(Formattazione.formatta(cursor.getDouble(18), "##0.00", Formattazione.NO_SEGNO).replaceAll(",", "."));
                } else
                    fcli.edsc2.setText("");
                if (cursor.getDouble(19) != 0) {
                    fcli.edsc3.setText(Formattazione.formatta(cursor.getDouble(19), "##0.00", Formattazione.NO_SEGNO).replaceAll(",", "."));
                } else
                    fcli.edsc3.setText("");
                if (!cursor.getString(15).equals("S")) {
                    // blocco sconti cliente
                    fcli.edsc1.setEnabled(false);
                    fcli.edsc2.setEnabled(false);
                    fcli.edsc3.setEnabled(false);
                }
                // codice pagamento
                if (!clicodpag.equals("")) {
                    for (int i = 0; i < vpag.size(); i++) {
                        if (clicodpag.equals(vpag.get(i))) {
                            friep.sppagamento.setSelection(i);
                            if (cursor.getString(32).equals("S"))
                                friep.sppagamento.setEnabled(true);
                            else
                                friep.sppagamento.setEnabled(false);
                            break;
                        }
                    }
                }

                // scoperti
                String cc = clicod;
                if (!clicodpadre.equals(""))
                    cc = clicodpadre;
                double tscop = FunzioniJBeerApp.totaleScopertiCliente(cc);
                double impscaduto = 0;
                if (clinscadblocco > 0 || cliimpblocco > 0) {
                    double[] bloc = FunzioniJBeerApp.scadenzeScadute(cc, (new Data()), cliggtollblocco);
                    cliblocco = (bloc[1] > 0 && bloc[0] > 0 && bloc[0] > cliimpblocco);
                    impscaduto = bloc[0];
                } else {
                    if (Env.nscadblocco > 0 || Env.impblocco > 0) {
                        double[] bloc = FunzioniJBeerApp.scadenzeScadute(cc, (new Data()), Env.ggtollblocco);
                        cliblocco = (bloc[1] > 0 && bloc[0] > 0 && bloc[0] > Env.impblocco);
                        impscaduto = bloc[0];
                    }
                }
                if (tscop != 0) {
                    fcli.tvscopcli.setText("Scoperto: " + Formattazione.formValuta(tscop, 12, 2, 1) + (cliblocco ? " BLOCCATO\n(scaduto=" + Formattazione.formValuta(impscaduto, 12, 2, 0) + ")" : ""));
                } else {
                    fcli.tvscopcli.setText("");
                }
                // visualizzazione prezzi
                friep.tvspeseinc.setVisibility(View.VISIBLE);
                friep.tvtotimp.setVisibility(View.VISIBLE);
                friep.tvtotiva.setVisibility(View.VISIBLE);
                friep.tvtotdoc.setVisibility(View.VISIBLE);
                friep.tvnettoapag.setVisibility(View.VISIBLE);
                friep.tvaddeb.setVisibility(View.VISIBLE);
                friep.tvaccred.setVisibility(View.VISIBLE);
                if (Env.calcaccisa) {
                    friep.lltotaccisa.setVisibility(View.VISIBLE);
                } else {
                    friep.lltotaccisa.setVisibility(View.GONE);
                }
                if (!modificadoc) {
                    // documento preferito
                    if (clidocpref > 0 && !Env.termordini) {
                        if (clidocpref == 1) {
                            if (!Env.docbollaval.equals("") || !Env.sezbollaval.equals("")) {
                                fcli.sptipodoc.setSelection(1);
                                if (fcli.sptipodoc.getSelectedItem().equals("...")) {
                                    for (int nd = 0; nd < vtd.size(); nd++) {
                                        if (!vtd.get(nd).equals("...")) {
                                            fcli.sptipodoc.setSelection(nd);
                                            break;
                                        }
                                    }
                                }
                                visualizzaDoc();
                            }
                        } else {
                            if (!Env.docfattura.equals("") || !Env.sezfattura.equals("")) {
                                fcli.sptipodoc.setSelection(3);
                                if (fcli.sptipodoc.getSelectedItem().equals("...")) {
                                    for (int nd = 0; nd < vtd.size(); nd++) {
                                        if (!vtd.get(nd).equals("...")) {
                                            fcli.sptipodoc.setSelection(nd);
                                            break;
                                        }
                                    }
                                }
                                visualizzaDoc();
                            }
                        }
                    }
                    if (fcli.sptipodoc.getSelectedItem().equals("...")) {
                        for (int nd = 0; nd < vtd.size(); nd++) {
                            if (!vtd.get(nd).equals("...")) {
                                fcli.sptipodoc.setSelection(nd);
                                break;
                            }
                        }
                    }
                }
            } else {
                clienteok = false;
                svuotaDatiCliente();
                return;
            }
            cursor.close();
        } else {
            clicod = "";
            svuotaDatiCliente();
        }
        if (clienteok) {
            if (!modificadoc) {
                if (vmsgavviso.size() > 0) {
                    Intent i = new Intent(FormDocumento.this.getApplicationContext(), FormVisAvvisi.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putStringArrayList("vavvisi", vmsgavviso);
                    i.putExtras(mBundle);
                    FormDocumento.this.startActivity(i);
                } else if (!cliblocco) {
                    Cursor cpr = Env.db.rawQuery(
                            "SELECT * FROM preferiti WHERE prefclicod = '" + clicod + "'", null);
                    boolean prefs = cpr.moveToFirst();
                    cpr.close();
                    if (prefs) {
                        mViewPager.setCurrentItem(1);
                        fart.bpref.performClick();
/*
                    if (fcli.sptipodoc.getSelectedItemPosition() != 5)
                    {
                        String cli = clicod;
                        String dest = "";
                        if (!clicodpadre.equals(""))
                        {
                            cli = "";
                            dest = clicod;
                        }
                        if (!FunzioniJBeerApp.causaleBloccata(0, Env.depcod, Env.agecod, cli, "", dest))
                        {
                            mViewPager.setCurrentItem(1);
                            String cliordlotto = "A";
                            Env.trasfdoc_righecrp = righecrp;
                            Intent i = new Intent(FormDocumento.this.getApplicationContext(), FormDocPreferiti.class);
                            Bundle mBundle = new Bundle();
                            if (!clicodpadre.equals(""))
                                mBundle.putString("clicod", clicodpadre);
                            else
                                mBundle.putString("clicod", clicod);
                            i.putExtras(mBundle);
                            this.startActivityForResult(i, 3);
                        }
                    }
                    else
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormDocumento.this);
                        builder.setMessage("Causale vendita bloccata")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(DialogInterface dialog,
                                                                int id)
                                            {
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return;
                    }
*/
                    }
                }
            }
        } else {
            clicod = "";
            svuotaDatiCliente();
        }
    }

    public void visualizzaDoc() {
        fcli.llnrif.setVisibility(View.GONE);
        fcli.viewdataconsegna.setVisibility(View.GONE);
        switch (fcli.sptipodoc.getSelectedItemPosition()) {
            case 1:
                if (Env.docbollaval.equals("") || Env.sezbollaval.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDocumento.this);
                    builder.setMessage("Documento non configurato")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            fcli.sptipodoc.setSelection(1);
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (!clibollaval) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDocumento.this);
                    builder.setMessage("DDT valorizzato non abilitato per questo cliente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            fcli.sptipodoc.setSelection(1);
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                }
                this.setTitle("DDT val.n." + (Env.numbollaval));
                break;
            case 2:
                if (Env.docbollaqta.equals("") || Env.sezbollaqta.equals("")) // *RPROM
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDocumento.this);
                    builder.setMessage("Documento non configurato")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            fcli.sptipodoc.setSelection(1);
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (!clibollaqta) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDocumento.this);
                    builder.setMessage("DDT a quantità non abilitato per questo cliente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            fcli.sptipodoc.setSelection(1);
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                }
                this.setTitle("DDT QTA.n." + (Env.numbollaqta));
                break;
            case 3:
                if (Env.docfattura.equals("") || Env.sezfattura.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDocumento.this);
                    builder.setMessage("Documento non configurato")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            fcli.sptipodoc.setSelection(1);
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (!clifattura) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDocumento.this);
                    builder.setMessage("Fattura non abilitata per questo cliente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            fcli.sptipodoc.setSelection(1);
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                }
                this.setTitle("Fattura n." + (Env.numfattura));
                break;
            case 4:
                if (Env.doccorrisp.equals("") || Env.sezcorrisp.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDocumento.this);
                    builder.setMessage("Documento non configurato")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            fcli.sptipodoc.setSelection(1);
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (!clicorrisp) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDocumento.this);
                    builder.setMessage("Corrispettivo non abilitato per questo cliente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            fcli.sptipodoc.setSelection(1);
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                }
                this.setTitle("Corrisp.n." + (Env.numcorrisp));
                break;
            case 5:
                if (Env.docddtreso.equals("") || Env.sezddtreso.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDocumento.this);
                    builder.setMessage("Documento non configurato")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            fcli.sptipodoc.setSelection(1);
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                } else if (!cliddtreso) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDocumento.this);
                    builder.setMessage("DDT di reso non abilitato per questo cliente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            fcli.sptipodoc.setSelection(1);
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                }
                this.setTitle("DDT reso n." + (Env.numddtreso));
                fcli.llnrif.setVisibility(View.VISIBLE);
                //fcli.ednrif.setEnabled(true);
                break;
            case 6:
                if (Env.docordinecli.equals("") || Env.sezordinecli.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormDocumento.this);
                    builder.setMessage("Documento non configurato")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            fcli.sptipodoc.setSelection(1);
                                            return;
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();
                }
                this.setTitle("Ordine n." + (Env.numordinecli));
                fcli.llnrif.setVisibility(View.VISIBLE);
                //fcli.ednrif.setEnabled(true);
                fcli.viewdataconsegna.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    public ViewPager mViewPager;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_documento);

        Toolbar toolbar = findViewById(R.id.doc_toolbar);
        setSupportActionBar(toolbar);


//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setIcon(R.mipmap.ic_launcher);


        fcli = new FragmentDocumentoCliente();
        fcli.fdoc = this;
        fart = new FragmentDocumentoCarrello();
        fart.fdoc = this;
        friep = new FragmentDocumentoRiepilogo();
        friep.fdoc = this;

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.doc_container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(4);

        TabLayout tabLayout = findViewById(R.id.doc_tabs);
        tabLayout.setupWithViewPager(mViewPager);
        //mViewPager.setCurrentItem(1);
        //android.support.v4.app.FragmentTransaction xx = getSupportFragmentManager().beginTransaction();
        //xx.replace(R.id.doc_container, friep);
        //xx.commit();
        mViewPager.setCurrentItem(1);
        //mViewPager.setCurrentItem(0);

        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("clicodice")) {
            clicodice = this.getIntent().getExtras().getString("clicodice");
            clickDaclienti = true;
        }
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("clicodpadre")) {
            clicodpadre = this.getIntent().getExtras().getString("clicodpadre");
        }
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("tipodoc")) {
            tipodoc = this.getIntent().getExtras().getInt("tipodoc");
            if (tipodoc >= 5 && tipodoc != 11) {
                tipodoc = 0;
            }
            if (tipodoc == 11) {
                tipodoc = 5;
            }
        }


    }

    public void inizializza() {


        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("modificadoc"))
            modificadoc = true;
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("movtipo"))
            movtipocorr = getIntent().getExtras().getInt("movtipo");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("movdoc"))
            movdoccorr = getIntent().getExtras().getString("movdoc");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("movdata"))
            movdatacorr = getIntent().getExtras().getString("movdata");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("movnum"))
            movnumcorr = getIntent().getExtras().getInt("movnum");


        // caricamento tipi documento
        vtd = new ArrayList();
        vtd.add("...");
        vtd.add("DDT VENDITA");
        vtd.add("DDT A QUANTITA");
        vtd.add("FATTURA");
        vtd.add("CORRISPETTIVO");
        vtd.add("DDT DI RESO");
        vtd.add("ORDINE CLIENTE");
        ltdadapter = new SpinnerAdapter(FormDocumento.this.getApplicationContext(), R.layout.spinner_item, vtd);
        fcli.sptipodoc.setAdapter(ltdadapter);
        fcli.sptipodoc.setSelection(tipodoc + 1);
        Env.quantitaCarrello = 0;
        Env.righecrptmp.clear();

        // caricamento lista pagamenti
        vpag = new ArrayList();
        vpagtipo = new ArrayList();
        ArrayList<String> vtmp = new ArrayList();
        int posp = 0;
        Cursor cursor = Env.db.rawQuery("SELECT pagcod,pagdescr,pagtipo FROM pagamenti ORDER BY pagcod", null);
        while (cursor.moveToNext()) {
            //vtmp.add(cursor.getString(0) + "-" + cursor.getString(1));
            vtmp.add(cursor.getString(1));
            vpag.add(cursor.getString(0));
            vpagtipo.add(cursor.getInt(2));
            if (cursor.getInt(2) == 8)
                pagtvpos = posp;
            posp++;
        }
        cursor.close();
        SpinnerAdapter lpagadapter = new SpinnerAdapter(FormDocumento.this.getApplicationContext(), R.layout.spinner_item, vtmp);
        friep.sppagamento.setAdapter(lpagadapter);
        friep.sppagamento.setSelection(0);

        righecrp = new ArrayList();
        righecrpraggr = new ArrayList();
        if (Env.vendqtatot)
            fart.lvdocdettadapter = new ListaDocDettAdapter(this.getApplicationContext(), righecrpraggr, fart);
        else
            fart.lvdocdettadapter = new ListaDocDettAdapter(this.getApplicationContext(), righecrp, fart);
        fart.listaart.setAdapter(fart.lvdocdettadapter);
        fart.lvdocdettadapter.notifyDataSetChanged();

        if (Env.termordini || Env.tipoterm == Env.TIPOTERM_RACCOLTAORDINI || Env.tipoterm == Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
            friep.tvincasso.setVisibility(View.INVISIBLE);
            friep.edincasso.setVisibility(View.INVISIBLE);
        }

        if (modificadoc && !movdoccorr.equals("") && !movdatacorr.equals("") && movnumcorr > 0) {
            this.mostraDocumento();
        } else {
            this.preparaNuovoDoc();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_form_documento, menu);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }


        yourdrawable = menu.getItem(2).getIcon(); // change 0 with 1,2 ...

        if (Env.alertagg) {
            yourdrawable.mutate();
            yourdrawable.setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_IN);
        }

        concar = new controlloAggiornamento();
        concar.start();
        //ImageButton brefresh = () findViewById(R.id.doc_action_refresh_giac);
/*        if(Env.alertagg){
            brefresh.setBackgroundColor(Color.RED);
        }*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        itemP = item;
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.doc_action_salva) {
            // controlli
            if (clicod.equals("") && this.nomenuovo.equals("")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormDocumento.this);
                builder.setMessage("Specificare il cliente")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.show();
                return false;
            } else if (fcli.sptipodoc.getSelectedItem().equals("...")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormDocumento.this);
                builder.setMessage("Specificare il documento da emettere")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.show();
                return false;
            } else if (fcli.sptipodoc.getSelectedItemPosition() == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormDocumento.this);
                builder.setMessage("Specificare il documento da emettere")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.show();
                return false;
            }
            if (righecrp.size() == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormDocumento.this);
                builder.setMessage("Inserire almeno un prodotto")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.show();
                return false;
            }

            if (fcli.sptipodoc.getSelectedItemPosition() == 6 && datacons.equals("")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormDocumento.this);
                builder.setMessage("Indicare la data di consegna merce")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.show();
                mViewPager.setCurrentItem(0);
                return false;
            }

            // controllo pagamento contanti
            boolean pagok = true;
            if (vpagtipo.get(friep.sppagamento.getSelectedItemPosition()) == 8 && fcli.sptipodoc.getSelectedItemPosition() != 6) {
                String cc = "";
                if (this.clicodpadre.equals(""))
                    cc = this.clicod;
                else
                    cc = this.clicodpadre;
                final String cc2 = cc;
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormDocumento.this);
                builder.setMessage("Confermi salvataggio documento?")
                        .setPositiveButton("SI",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        double totscaduto = 0;
                                        Intent i = new Intent(FormDocumento.this.getApplicationContext(), FormIncassoDoc.class);
                                        Bundle mBundle = new Bundle();
                                        mBundle.putString("clicod", cc2);
                                        mBundle.putDouble("totdoc", nettoapag);
                                        mBundle.putDouble("totscaduto", totscaduto);
                                        double impinc = Formattazione.estraiDouble(friep.edincasso.getText().toString().replace(".", ","));
                                        if (impinc != 0)
                                            mBundle.putDouble("impinc", impinc);
                                        i.putExtras(mBundle);
                                        FormDocumento.this.startActivityForResult(i, 1);
                                    }
                                })
                        .setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
            } else {
                String cc = "";
                if (this.clicodpadre.equals(""))
                    cc = this.clicod;
                else
                    cc = this.clicodpadre;
                double totscaduto = 0;//Utilita.totaleRateScadute(cc);

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormDocumento.this);
                builder.setMessage("Confermi salvataggio documento?")
                        .setPositiveButton("SI",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        onActivityResult(1, Activity.RESULT_OK, null);
                                    }
                                })
                        .setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
            }
        } else if (id == R.id.doc_action_nuovo) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormDocumento.this);
            builder.setMessage("Nuovo documento?")
                    .setPositiveButton("SI",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    // cancella cliente nuovo
                                    if (clinuovo && nuovoins) {
                                        SQLiteStatement stmt = Env.db.compileStatement("DELETE FROM clienti WHERE clicodice = ?");
                                        stmt.clearBindings();
                                        stmt.bindString(1, clicod);
                                        stmt.execute();
                                        stmt.close();
                                    }
                                    preparaNuovoDoc();
                                }
                            })
                    .setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
        } else if (id == R.id.doc_action_refresh_giac) {
            //aggiornamento carico
            azioneTrasmissione();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void uscita() {

        if (clickDaclienti) {
            concar.interrupt();
            Uri ris = Uri.parse("content://modificacontatto/OK1");
            Intent result = new Intent(Intent.ACTION_PICK, ris);
            setResult(RESULT_FIRST_USER, result);
            finish();

        } else {
            concar.interrupt();
            super.onBackPressed();
        }

    }


    class controlloAggiornamento extends Thread {
        public String user = "", pwd = "", termcod = "", server = "";
        public boolean uscita = false;
        Handler handler = new Handler();

        public void run() {
            while (!uscita) {
                if (Env.alertagg) {
                    handler.post(new Runnable() {
                        public void run() {
                            yourdrawable.mutate();
                            yourdrawable.setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_IN);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        public void run() {
                            yourdrawable.mutate();
                            yourdrawable.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_IN);
                        }
                    });
                }
                try {
                    //    alert.setVisibility(View.INVISIBLE);
                    Thread.sleep(120000); //2 minuti
                } catch (Exception e) {
                }
            }

        }

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                FormDocumento.this);
        builder.setMessage("Uscire da caricamento documenti?")
                .setPositiveButton("SI",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                if (!clicod.equals("") && clinuovo && nuovoins) {
                                    SQLiteStatement stmt = Env.db.compileStatement("DELETE FROM clienti WHERE clicodice = ?");
                                    stmt.clearBindings();
                                    stmt.bindString(1, clicod);
                                    stmt.execute();
                                    stmt.close();
                                }
                                uscita();
                            }
                        })
                .setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                            }
                        });
        AlertDialog ad = builder.create();
        ad.setCancelable(false);
        ad.show();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return fcli;
                }
                case 1: {
                    return fart;
                }
                case 2: {
                    return friep;
                }
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Cliente";
                case 1:
                    return "carrello";
                case 2:
                    return "Riepilogo";
            }
            return null;
        }
    }

    public Record salvaDoc(int tipodoc, ArrayList<Record> righeN, boolean art62, String codcess) {
        Record ris = new Record();
        int num = 0;
        String sez = "";
        String doc = "";
        String stipodoc = "";
        String oradoc = "";
        Data dtoggi = new Data();
        //fcli.tvdatadoc.setText(dtoggi.formatta(Data.GG_MM_AAAA, "/") + "\n" + dtoggi.giornoSettimana());
        String datadoc = dtoggi.formatta(Data.AAAA_MM_GG, "-");
        if (!clicod.equals("")) {
            this.nomenuovo = "";
            this.pivanuovo = "";
        }
        if (!modificadoc) {
            // assegnazione numero
            stipodoc = "";
            if (tipodoc == 1) {
                // bolla valorizzata
                num = Env.numbollaval;
                sez = Env.sezbollaval;
                doc = Env.docbollaval;
                Env.numbollaval++;
                if (Env.sezbollaqta.toLowerCase().equals(Env.sezbollaval.toLowerCase())) {
                    Env.numbollaqta++;
                }
                stipodoc = "DDT val.";
            } else if (tipodoc == 2) {
                // bolla qta
                num = Env.numbollaqta;
                sez = Env.sezbollaqta;
                doc = Env.docbollaqta;
                Env.numbollaqta++;
                if (Env.sezbollaval.toLowerCase().equals(Env.sezbollaqta.toLowerCase())) {
                    Env.numbollaval++;
                }
                stipodoc = "DDT qta";
            } else if (tipodoc == 3) {
                // fattura
                num = Env.numfattura;
                sez = Env.sezfattura;
                doc = Env.docfattura;
                Env.numfattura++;
                stipodoc = "fattura";
            } else if (tipodoc == 4) {
                // corrispettivo
                num = Env.numcorrisp;
                sez = Env.sezcorrisp;
                doc = Env.doccorrisp;
                Env.numcorrisp++;
                stipodoc = "corrispettivo";
            } else if (tipodoc == 5) {
                // ddt reso
                num = Env.numddtreso;
                sez = Env.sezddtreso;
                doc = Env.docddtreso;
                Env.numddtreso++;
                stipodoc = "DDT di reso";
            } else if (tipodoc == 6) {
                // ordine cliente
                num = Env.numordinecli;
                sez = Env.sezordinecli;
                doc = Env.docordinecli;
                Env.numordinecli++;
                stipodoc = "Ordine cliente";
            }
            //String datadoc = (new Data()).formatta(Data.AAAA_MM_GG, "-");
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            oradoc = dh.format(new Date());
        } else {
            stipodoc = "";
            datadoc = movdatacorr;
            num = movnumcorr;
            sez = movsezcorr;
            doc = movdoccorr;
            if (tipodoc == 1) {
                // bolla valorizzata
                stipodoc = "DDT val.";
            } else if (tipodoc == 2) {
                // bolla qta
                stipodoc = "DDT qta";
            } else if (tipodoc == 3) {
                // fattura
                stipodoc = "fattura";
            } else if (tipodoc == 4) {
                // corrispettivo
                stipodoc = "corrispettivo";
            } else if (tipodoc == 5) {
                // ddt reso
                stipodoc = "DDT di reso";
            } else if (tipodoc == 6) {
                // ordine cliente
                stipodoc = "Ordine cliente";
            } else if (tipodoc == 7) {
                // ordine cliente
                stipodoc = "PRE-DDT";
            }
            oradoc = movoracorr;
        }

        if (modificadoc) {
            // modifica -> cancellazione movimento attuale per reinserimento
            // aggiornamento giacenze articoli
            String[] parsm = new String[3];
            parsm[0] = "" + movtipocorr;
            parsm[1] = "" + movnumcorr;
            parsm[2] = movdatacorr;
            Cursor cm = Env.db.rawQuery("SELECT rmartcod,rmqta,rmcaumag,rmlotto FROM righemov WHERE rmmovtipo = ? AND rmmovnum = ? AND rmmovdata = ?", parsm);
            while (cm.moveToNext()) {
                String cod = cm.getString(0);
                double qta = cm.getDouble(1);
                String cau = cm.getString(2);
                String lotto = cm.getString(3);
                // legge attuale giacenza articolo
                double giac = FunzioniJBeerApp.leggiGiacenzaArticolo(cod);
                // aggiorna giacenza articolo
                if (cau.equals("VE") || cau.equals("VQ") || cau.equals("OM") || cau.equals("OT") || cau.equals("SM"))
                    FunzioniJBeerApp.impostaGiacenzaArticolo(cod, OpValute.arrotondaMat(giac + qta, 3));
                else if (cau.equals("RV"))
                    FunzioniJBeerApp.impostaGiacenzaArticolo(cod, OpValute.arrotondaMat(giac - qta, 3));
                else
                    FunzioniJBeerApp.impostaGiacenzaArticolo(cod, OpValute.arrotondaMat(giac, 3));
                // aggiorna giacenza lotto
                if (!lotto.equals("")) {
                    float segno = 0;
                    if (cau.equals("VE") || cau.equals("VQ") || cau.equals("OM") || cau.equals("OT") || cau.equals("SM"))
                        segno = 1;
                    else if (cau.equals("RV"))
                        segno = -1;
                    else
                        segno = 0;
                    FunzioniJBeerApp.aggiornaGiacenzaLotto(lotto, cod, segno * qta);
                }
            }
            cm.close();
            // cancellazione movimento attuale
            SQLiteStatement st = Env.db.compileStatement("DELETE FROM movimenti WHERE movtipo = ? AND movnum = ? AND movdata = ?");
            st.clearBindings();
            st.bindLong(1, movtipocorr);
            st.bindLong(2, movnumcorr);
            st.bindString(3, movdatacorr);
            st.execute();
            st.close();
            st = Env.db.compileStatement("DELETE FROM righemov WHERE rmmovtipo = ? AND rmmovnum = ? AND rmmovdata = ?");
            st.clearBindings();
            st.bindLong(1, movtipocorr);
            st.bindLong(2, movnumcorr);
            st.bindString(3, movdatacorr);
            st.execute();
            st.close();
            st = Env.db.compileStatement("DELETE FROM ivamov WHERE immovsez = ? AND immovnum = ? AND immovdata = ?");
            st.clearBindings();
            st.bindString(1, movsezcorr);
            st.bindLong(2, movnumcorr);
            st.bindString(3, movdatacorr);
            st.execute();
            st.close();
            st = Env.db.compileStatement("DELETE FROM cauzmov WHERE cmmovsez = ? AND cmmovnum = ? AND cmmovdata = ?");
            st.clearBindings();
            st.bindString(1, movsezcorr);
            st.bindLong(2, movnumcorr);
            st.bindString(3, movdatacorr);
            st.execute();
            st.close();
            st = Env.db.compileStatement("DELETE FROM lottimov WHERE lmmovsez = ? AND lmmovnum = ? AND lmmovdata = ?");
            st.clearBindings();
            st.bindString(1, movsezcorr);
            st.bindLong(2, movnumcorr);
            st.bindString(3, movdatacorr);
            st.execute();
            st.close();
            st = Env.db.compileStatement("DELETE FROM scadmov WHERE smmovsez = ? AND smmovnum = ? AND smmovdata = ?");
            st.clearBindings();
            st.bindString(1, movsezcorr);
            st.bindLong(2, movnumcorr);
            st.bindString(3, movdatacorr);
            st.execute();
            st.close();
            // eliminazione scoperti e incassi documento
            st = Env.db.compileStatement("DELETE FROM scoperti WHERE scsezdoc = ? AND scnumdoc = ?");
            st.bindString(1, movsezcorr);
            st.bindLong(2, movnumcorr);
            st.execute();
            st.close();
            st = Env.db.compileStatement("DELETE FROM incassi WHERE iscsezdoc = ? AND iscnumdoc = ?");
            st.bindString(1, movsezcorr);
            st.bindLong(2, movnumcorr);
            st.execute();
            st.close();
        }


        String[] pars = new String[1];
        pars[0] = this.clicod;
        Cursor ccli = Env.db.rawQuery("SELECT clicodpadre,clicodlist,clinprz,clibancacod,cliagenzia,clicab FROM clienti WHERE clicodice = ?", pars);
        boolean clipres = (ccli.moveToFirst());


        SQLiteStatement stmt = Env.db.compileStatement(
                "insert into movimenti (movtipo,movdoc,movsez,movdata,movnum,movdocora," + //6
                        "movtrasf,movsospeso,movclicod,movdestcod,movspeseinc,movspesebolli," + //12
                        "movspesetrasp,movspesecauz,movcodlist,movnprz,movbancacod,movagenzia," +
                        "movcab,movpagcod,movaspbeni,movnumcolli,movpeso,movnote1," + // 24
                        "movnote2,movnote3,movnote4,movnote5,movnote6,movtotlordomerce, " +
                        "movtotsconticli,movtotscontiart,movomaggi,movtotmerce,movtotnetti,movtotspeseinc, " +
                        "movtotspesetrasp,movtotspesebol,movtotimp,movtotiva,movtotale,movaddeb, " + // 42
                        "movaccred,movnettoapag,movacconto,movnrif,movoperatore,movevaso, " + //48
                        "movart62,movkey,movcesscod,movdatacons,movabbuoni,movdep,movaccisa) " +
                        "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        stmt.clearBindings();
        if (tipodoc == 6)
            stmt.bindLong(1, 20);
        else
            stmt.bindLong(1, tipodoc - 1);
        stmt.bindString(2, doc);
        stmt.bindString(3, sez);
        stmt.bindString(4, datadoc);
        stmt.bindLong(5, num);
        stmt.bindString(6, oradoc);
        stmt.bindString(7, "N");
        stmt.bindString(8, "N");
        String codpadre = "";
        if (clipres) {
            codpadre = ccli.getString(0);
        }
        if (codpadre.equals("")) {
            stmt.bindString(9, this.clicod);
            stmt.bindString(10, "");
        } else {
            stmt.bindString(9, codpadre);
            stmt.bindString(10, clicod);
        }
        stmt.bindString(11, (this.clispeseinc ? "S" : "N"));
        stmt.bindString(12, (this.clispesebol ? "S" : "N"));
        stmt.bindString(13, (this.clispesetrasp ? "S" : "N"));
        stmt.bindString(14, (this.clispesecauz ? "S" : "N"));
        if (clipres) {
            stmt.bindString(15, ccli.getString(1));
            stmt.bindLong(16, ccli.getInt(2));
            stmt.bindString(17, ccli.getString(3));
            stmt.bindString(18, ccli.getString(4));
            stmt.bindString(19, ccli.getString(5));
        } else {
            stmt.bindString(15, "");
            stmt.bindLong(16, 1);
            stmt.bindString(17, "");
            stmt.bindString(18, "");
            stmt.bindString(19, "");
        }

        stmt.bindString(20, vpag.get(friep.sppagamento.getSelectedItemPosition()));
        stmt.bindString(21, "A Vista");
        stmt.bindLong(22, 0);
        stmt.bindLong(23, 0);
        String note = friep.edannot.getText().toString();
        note = note.replace("\n", "");
        note = note.replace("\r", "");
        String note1 = "", note2 = "", note3 = "", note4 = "", note5 = "", note6 = "";
        if (note.length() >= 50) {
            note1 = note.substring(0, 50);
            note = note.substring(50);
        } else {
            note1 = note;
            note = "";
        }
        if (note.length() >= 50) {
            note2 = note.substring(0, 50);
            note = note.substring(50);
        } else {
            note2 = note;
            note = "";
        }
        if (note.length() >= 50) {
            note3 = note.substring(0, 50);
            note = note.substring(50);
        } else {
            note3 = note;
            note = "";
        }
        if (note.length() >= 50) {
            note4 = note.substring(0, 50);
            note = note.substring(50);
        } else {
            note4 = note;
            note = "";
        }
        if (note.length() >= 50) {
            note5 = note.substring(0, 50);
            note = note.substring(50);
        } else {
            note5 = note;
            note = "";
        }
        if (note.length() >= 50) {
            note6 = note.substring(0, 50);
            note = note.substring(50);
        } else {
            note6 = note;
            note = "";
        }

        stmt.bindString(24, note1);
        stmt.bindString(25, note2);
        stmt.bindString(26, note3);
        stmt.bindString(27, note4);
        stmt.bindString(28, note5);
        stmt.bindString(29, note6);
        stmt.bindDouble(30, this.totlordomerce);
        stmt.bindDouble(31, this.totsconticli);
        stmt.bindDouble(32, this.totscontiart);
        stmt.bindDouble(33, this.totomaggi);
        stmt.bindDouble(34, this.totmerce);
        stmt.bindDouble(35, this.totcorpo);
        stmt.bindDouble(36, this.speseinc);
        stmt.bindDouble(37, this.spesetrasp);
        stmt.bindDouble(38, this.spesebol);
        stmt.bindDouble(39, this.totimp);
        stmt.bindDouble(40, this.totiva);
        stmt.bindDouble(41, this.totdoc);
        stmt.bindDouble(42, this.totaddeb);
        stmt.bindDouble(43, this.totaccred);
        stmt.bindDouble(44, this.nettoapag);
        String[] parsp = new String[1];
        parsp[0] = vpag.get(friep.sppagamento.getSelectedItemPosition());
        Cursor cpag = Env.db.rawQuery("SELECT pagtipo FROM pagamenti WHERE pagcod = ?", parsp);
        cpag.moveToFirst();
        int pagtipo = cpag.getInt(0);
        cpag.close();
        stmt.bindDouble(45, Formattazione.estraiDouble(friep.edincasso.getText().toString().replace(".", ",")));
        stmt.bindString(46, fcli.ednrif.getText().toString().trim());
        stmt.bindString(47, "");
        stmt.bindLong(48, 0);
        if (art62)
            stmt.bindLong(49, 2);
        else
            stmt.bindLong(49, 1);
        stmt.bindString(50, "");
        stmt.bindString(51, codcess);
        stmt.bindString(52, datacons);
        stmt.bindDouble(53, this.totabbuoni);
        if (!Env.depeventocod.equals(""))
            stmt.bindString(54, Env.depeventocod);
        else
            stmt.bindString(54, Env.depcod);
        stmt.bindDouble(55, this.totaccisa);
        stmt.execute();
        stmt.close();

        // righe articoli
        SQLiteStatement str = Env.db.compileStatement(
                "insert into righemov (rmmovtipo,rmmovdoc,rmmovsez,rmmovdata,rmmovnum,rmriga," +
                        "rmtiporiga,rmartcod,rmlotto,rmcaumag,rmcolli,rmpezzi,rmcontenuto,rmqta,rmprz," +
                        "rmartsc1,rmartsc2,rmscvend,rmcodiva,rmaliq,rmclisc1,rmclisc2,rmclisc3," +
                        "rmlordo,rmnetto,rmnettoivato,rmaccisa,rmgrado,rmplato) " +
                        "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        for (int i = 0; i < righeN.size(); i++) {
            Record riga = righeN.get(i);
            str.clearBindings();
            if (tipodoc == 6)
                str.bindLong(1, 20);
            else
                str.bindLong(1, tipodoc - 1);
            str.bindString(2, doc);
            str.bindString(3, sez);
            str.bindString(4, datadoc);
            str.bindLong(5, num);
            str.bindLong(6, i);
            str.bindString(7, "A");
            str.bindString(8, riga.leggiStringa("rmartcod"));
            str.bindString(9, riga.leggiStringa("rmlotto"));
            str.bindString(10, riga.leggiStringa("rmcaumag"));
            str.bindLong(11, riga.leggiIntero("rmcolli"));
            str.bindDouble(12, riga.leggiDouble("rmpezzi"));
            str.bindDouble(13, riga.leggiDouble("rmcontenuto"));
            str.bindDouble(14, riga.leggiDouble("rmqta"));
            str.bindDouble(15, riga.leggiDouble("rmprz"));
            str.bindDouble(16, riga.leggiDouble("rmartsc1"));
            str.bindDouble(17, riga.leggiDouble("rmartsc2"));
            str.bindDouble(18, riga.leggiDouble("rmscvend"));
            str.bindString(19, riga.leggiStringa("rmcodiva"));
            str.bindDouble(20, riga.leggiDouble("rmaliq"));
            str.bindDouble(21, riga.leggiDouble("rmclisc1"));
            str.bindDouble(22, riga.leggiDouble("rmclisc2"));
            str.bindDouble(23, riga.leggiDouble("rmclisc3"));
            str.bindDouble(24, riga.leggiDouble("rmlordo"));
            str.bindDouble(25, riga.leggiDouble("rmnetto"));
            str.bindDouble(26, riga.leggiDouble("rmnettoivato"));
            if (riga.esisteCampo("rmaccisa"))
                str.bindDouble(27, riga.leggiDouble("rmaccisa"));
            else
                str.bindDouble(27, 0);
            if (riga.esisteCampo("rmgrado"))
                str.bindDouble(28, riga.leggiDouble("rmgrado"));
            else
                str.bindDouble(28, 0);
            if (riga.esisteCampo("rmplato"))
                str.bindDouble(29, riga.leggiDouble("rmplato"));
            else
                str.bindDouble(29, 0);
            str.execute();
        }
        str.close();

        // righe iva
        if (tipodoc == 3) // fattura
        {
            SQLiteStatement sti = Env.db.compileStatement(
                    "insert into ivamov (immovtipo,immovdoc,immovsez,immovdata,immovnum,imriga," +
                            "imcodiva,imtipo,imaliq,imimp,imiva,imivaindetr) " +
                            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
            Iterator it = impon.keySet().iterator();
            int i = 0;
            while (it.hasNext()) {
                String codiva = (String) it.next();
                Record riva = lcodiva.get(codiva);
                double imp = impon.get(codiva);
                double al = riva.leggiDouble("ivaaliq");
                int tipo = riva.leggiIntero("ivagruppo");
                double iva = imposta.get(codiva);
                double ivanodetr = impnondetr.get(codiva);
                sti.clearBindings();
                sti.bindLong(1, tipodoc - 1);
                sti.bindString(2, doc);
                sti.bindString(3, sez);
                sti.bindString(4, datadoc);
                sti.bindLong(5, num);
                sti.bindLong(6, i++);
                sti.bindString(7, codiva);
                sti.bindLong(8, tipo);
                sti.bindDouble(9, al);
                sti.bindDouble(10, imp);
                sti.bindDouble(11, iva);
                sti.bindDouble(12, ivanodetr);
                sti.execute();
            }
            sti.close();
        }

        // scadenze
        if (tipodoc == 3 && vscad.size() > 0) // fattura
        {
            SQLiteStatement sti = Env.db.compileStatement(
                    "insert into scadmov (smmovtipo,smmovdoc,smmovsez,smmovdata,smmovnum,smriga," +
                            "smdata,smimp) " +
                            "VALUES (?,?,?,?,?,?,?,?)");
            for (int i = 0; i < vscad.size(); i++) {
                Record riga = vscad.get(i);
                sti.clearBindings();
                sti.bindLong(1, tipodoc - 1);
                sti.bindString(2, doc);
                sti.bindString(3, sez);
                sti.bindString(4, datadoc);
                sti.bindLong(5, num);
                sti.bindLong(6, i);
                sti.bindString(7, riga.leggiStringa("data"));
                sti.bindDouble(8, riga.leggiDouble("importo"));
                sti.execute();
            }
            sti.close();
        }

        // movimenti lotti
        if (tipodoc != 6) {
            SQLiteStatement stml = Env.db.compileStatement(
                    "insert into lottimov (lmmovtipo,lmmovdoc,lmmovsez,lmmovdata,lmmovnum,lmriga," +
                            "lmlotcod,lmlotartcod,lmtipomov,lmpezzi,lmqta) " +
                            "VALUES (?,?,?,?,?,?,?,?,?,?,?)");
            int nr = 0;
            for (int i = 0; i < righeN.size(); i++) {
                Record riga = righeN.get(i);
                String lc = riga.leggiStringa("rmlotto");
                String art = riga.leggiStringa("rmartcod");
                String cau = riga.leggiStringa("rmcaumag");
                if (!lc.equals("") && !art.equals("") && !cau.equals("RC") && !cau.equals("RN")) {
                    stml.clearBindings();
                    stml.bindLong(1, tipodoc - 1);
                    stml.bindString(2, doc);
                    stml.bindString(3, sez);
                    stml.bindString(4, datadoc);
                    stml.bindLong(5, num);
                    stml.bindLong(6, nr);
                    stml.bindString(7, lc);
                    stml.bindString(8, art);
                    if (cau.equals("VE") || cau.equals("OM") || cau.equals("OT") || cau.equals("SM"))
                        stml.bindString(9, "S");
                    else
                        stml.bindString(9, "C");
                    stml.bindLong(10, 0);
                    stml.bindDouble(11, riga.leggiDouble("rmqta"));
                    stml.execute();
                    nr++;
                }
            }
            stml.close();
        }
        // nuovo cliente
        if (!this.nomenuovo.equals("")) {
            SQLiteStatement stcn = Env.db.compileStatement(
                    "insert into nuovicli (ncmovtipo,ncmovdoc,ncmovsez,ncmovdata,ncmovnum," +
                            "ncnome,ncind,ncpiva) " +
                            "VALUES (?,?,?,?,?,?,?,?)");
            stcn.clearBindings();
            if (tipodoc == 6)
                stcn.bindLong(1, 20);
            else
                stcn.bindLong(1, fcli.sptipodoc.getSelectedItemPosition() - 1);
            stcn.bindString(2, doc);
            stcn.bindString(3, sez);
            stcn.bindString(4, datadoc);
            stcn.bindLong(5, num);
            stcn.bindString(6, this.nomenuovo);
            stcn.bindString(7, "");
            stcn.bindString(8, this.pivanuovo);
            stcn.execute();
            stcn.close();
        }

        // aggiorna giacenze articoli
        HashMap<String, String> hartcod = new HashMap();
        for (int i = 0; i < righeN.size(); i++) {
            Record riga = righeN.get(i);
            String cod = riga.leggiStringa("rmartcod");
            hartcod.put(cod, "");
            double qta = riga.leggiDouble("rmqta");
            String cau = riga.leggiStringa("rmcaumag");
            if (!cod.equals("")) {
                // legge giac.articolo
                double giac = FunzioniJBeerApp.leggiGiacenzaArticolo(cod);
                if (cau.equals("VE") || cau.equals("OM") || cau.equals("OT") || cau.equals("SM"))
                    FunzioniJBeerApp.impostaGiacenzaArticolo(cod, OpValute.arrotondaMat(giac - qta, 3));
                else if (cau.equals("RV") || cau.equals("RP")) // aumenta giacenza (solo nel caso di reso vendibile o rientro da prom.)
                    FunzioniJBeerApp.impostaGiacenzaArticolo(cod, OpValute.arrotondaMat(giac + qta, 3));
            }
        }
        if (modificadoc) {
            FunzioniJBeerApp.salvaLog("Modificato " + stipodoc + " n." + num);
        } else {
            FunzioniJBeerApp.salvaLog("Registrato " + stipodoc + " n." + num);
        }
        Iterator it = hartcod.keySet().iterator();
        while (it.hasNext()) {
            String ca = (String) it.next();
            double g = FunzioniJBeerApp.leggiGiacenzaArticolo(ca);
            if (g < 0) {
                FunzioniJBeerApp.salvaLog(stipodoc + " n." + num + ": giacenza art." + ca + " in negativo (" + Formattazione.formValuta(g, 12, 3, 1) + ")");
            }
        }

        // aggiorna giacenze lotti
        for (int i = 0; i < righeN.size(); i++) {
            Record riga = righeN.get(i);
            String cod = riga.leggiStringa("rmartcod");
            String lotto = riga.leggiStringa("rmlotto");
            double qta = riga.leggiDouble("rmqta");
            String cau = riga.leggiStringa("rmcaumag");
            if (!cod.equals("") && !lotto.equals("")) {
                float segno = 0;
                if (cau.equals("VE") || cau.equals("OM") || cau.equals("OT") || cau.equals("SM"))
                    segno = -1;
                else
                    segno = 1;
                if (!cau.equals("RN"))
                    FunzioniJBeerApp.aggiornaGiacenzaLotto(lotto, cod, segno * qta);
            }
        }

        // sistemazione giacenze negative di lotti e articoli
        Env.db.execSQL("UPDATE articoli SET artgiacenza = 0 WHERE artgiacenza < 0");
        Cursor cd = Env.db.rawQuery("SELECT DISTINCT lotartcod FROM lotti WHERE lotgiacenza < 0", null);
        while (cd.moveToNext()) {
            FunzioniJBeerApp.allineaGiacenzaLottiArticolo(cd.getString(0));
        }
        cd.close();

        // aggiorna sezionali su db
        SQLiteStatement stsez = Env.db.compileStatement(
                "UPDATE datiterm SET numbollaval=?,numbollaqta=?,numfattura=?,numcorrisp=?,numddtreso=?,numordinecli=?");
        stsez.clearBindings();
        stsez.bindLong(1, Env.numbollaval);
        stsez.bindLong(2, Env.numbollaqta);
        stsez.bindLong(3, Env.numfattura);
        stsez.bindLong(4, Env.numcorrisp);
        stsez.bindLong(5, Env.numddtreso);
        stsez.bindLong(6, Env.numordinecli);
        stsez.execute();
        stsez.close();

        // aggiorna cliente del giro visita
        if (!Env.gvcod.equals("") && !modificadoc) {
            String[] parsgv = new String[3];
            parsgv[0] = Env.gvcod;
            parsgv[1] = this.clicod;
            parsgv[2] = this.clicod;
            Cursor cgv = Env.db.rawQuery("SELECT gvprog FROM girivisita WHERE gvcod=? AND (gvcodcli=? OR gvcoddest=?)", parsgv);
            if (cgv.moveToFirst()) {
                int prog = cgv.getInt(0);
                String[] parsgv2 = new String[2];
                parsgv2[0] = Env.gvcod;
                parsgv2[1] = "" + prog;
                Cursor cgv2 = Env.db.rawQuery("SELECT gvcodcli,gvcoddest FROM girivisita WHERE gvcod=? AND gvprog > ? ORDER BY gvprog", parsgv2);
                if (cgv2.moveToFirst()) {
                    if (!cgv2.getString(0).equals(""))
                        FunzioniJBeerApp.impostaProprieta("cligiro", cgv2.getString(0));
                    else
                        FunzioniJBeerApp.impostaProprieta("cligiro", cgv2.getString(1));
                } else {
                    FunzioniJBeerApp.impostaProprieta("cligiro", "");
                }
                cgv2.close();
            }
            cgv.close();
        }

        // se ddt sospeso aggiorna tab.scoperti
        if (tipodoc != 6) {
            if ((tipodoc == 1 || tipodoc == 2 || tipodoc == 5) && vpagtipo.get(friep.sppagamento.getSelectedItemPosition()) == 8) {
                double incdoc = Formattazione.estraiDouble(friep.edincasso.getText().toString().replace(".", ","));
                if (incdoc == 0 || (this.nettoapag > 0 && OpValute.arrotondaMat(incdoc + totabbuoni, 2) < this.nettoapag) || (this.nettoapag < 0 && OpValute.arrotondaMat(incdoc + totabbuoni, 2) > this.nettoapag)) {
                    SQLiteStatement stsc = Env.db.compileStatement(
                            "INSERT INTO scoperti (" +
                                    "sctipo," +
                                    "scsezdoc," +
                                    "scdatadoc," +
                                    "scnumdoc," +
                                    "sctipoop," +
                                    "scnote," +
                                    "scclicod," +
                                    "scdestcod," +
                                    "scscadid," +
                                    "scdatascad," +
                                    "sctiposcad," +
                                    "scimportoscad," +
                                    "scresiduoscad," +
                                    "scrateizzata," +
                                    "scdataconsfat) " +
                                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    stsc.clearBindings();
                    stsc.bindString(1, "B");
                    stsc.bindString(2, sez);
                    stsc.bindString(3, datadoc);
                    stsc.bindLong(4, num);
                    stsc.bindString(5, (this.nettoapag >= 0 ? "V" : "R"));
                    stsc.bindString(6, "");
                    if (codpadre.equals("")) {
                        stsc.bindString(7, this.clicod);
                        stsc.bindString(8, "");
                    } else {
                        stsc.bindString(7, codpadre);
                        stsc.bindString(8, this.clicod);
                    }
                    stsc.bindLong(9, -1);
                    stsc.bindString(10, datadoc);
                    stsc.bindLong(11, 6);
                    stsc.bindDouble(12, this.nettoapag);
                    stsc.bindDouble(13, OpValute.arrotondaMat(Math.abs(this.nettoapag - incdoc), 2));
                    stsc.bindLong(14, 0);
                    stsc.bindString(15, "0000-00-00");
                    stsc.execute();
                    stsc.close();
                }
            }
            if (tipodoc == 3 && vpagtipo.get(friep.sppagamento.getSelectedItemPosition()) != 0) {
                // se fattura non RIBA inserisce scoperto
                double incdoc = Formattazione.estraiDouble(friep.edincasso.getText().toString().replace(".", ","));
                if (incdoc == 0 || (this.nettoapag > 0 && OpValute.arrotondaMat(incdoc + totabbuoni, 2) < this.nettoapag) ||
                        (this.nettoapag < 0 && OpValute.arrotondaMat(incdoc + totabbuoni, 2) > this.nettoapag)) {
                    SQLiteStatement stsc = Env.db.compileStatement(
                            "INSERT INTO scoperti (" +
                                    "sctipo," +
                                    "scsezdoc," +
                                    "scdatadoc," +
                                    "scnumdoc," +
                                    "sctipoop," +
                                    "scnote," +
                                    "scclicod," +
                                    "scdestcod," +
                                    "scscadid," +
                                    "scdatascad," +
                                    "sctiposcad," +
                                    "scimportoscad," +
                                    "scresiduoscad," +
                                    "scrateizzata," +
                                    "scdataconsfat) " +
                                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    stsc.clearBindings();
                    stsc.bindString(1, "F");
                    stsc.bindString(2, sez);
                    stsc.bindString(3, datadoc);
                    stsc.bindLong(4, num);
                    stsc.bindString(5, (this.nettoapag >= 0 ? "V" : "R"));
                    stsc.bindString(6, "");
                    if (codpadre.equals("")) {
                        stsc.bindString(7, this.clicod);
                        stsc.bindString(8, "");
                    } else {
                        stsc.bindString(7, codpadre);
                        stsc.bindString(8, this.clicod);
                    }
                    stsc.bindLong(9, -1);
                    stsc.bindString(10, datadoc);
                    stsc.bindLong(11, 6);
                    stsc.bindDouble(12, this.nettoapag);
                    stsc.bindDouble(13, OpValute.arrotondaMat(Math.abs(this.nettoapag - incdoc), 2));
                    stsc.bindLong(14, 0);
                    stsc.bindString(15, "0000-00-00");
                    stsc.execute();
                    stsc.close();
                }
            }
        }
        ccli.close();
        if (!modificadoc) {
            FunzioniJBeerApp.impostaProprieta("ultimodoc_num" + (tipodoc - 1), "" + num);
            FunzioniJBeerApp.impostaProprieta("ultimodoc_data" + (tipodoc - 1), datadoc);
        }
        FunzioniJBeerApp.impostaProprieta("stato", "SESSIONE");

        ris.insIntero("num", num);
        ris.insStringa("doc", doc);
        return ris;
    }

    public void aggiornaNumeroArticoli() {
        HashMap hart = new HashMap();
        for (int i = 0; i < righecrp.size(); i++) {
            Record riga = righecrp.get(i);
            hart.put(riga.leggiStringa("rmartcod"), "");
        }
        if (hart.size() == 0)
            fart.tvnarts.setText("Nessun articolo in lista");
        else if (hart.size() == 1)
            fart.tvnarts.setText("1 articolo in lista");
        else
            fart.tvnarts.setText(hart.size() + " articoli in lista");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                if (resultCode == Activity.RESULT_OK) {
                    // ritorno da form incasso doc. per salvataggio documento
                    if (data != null) {
                        ris = data.getData();
                        String esito = ris.getLastPathSegment();
                        StringTokenizer stok = new StringTokenizer(esito, "|");
                        stok.nextToken();
                        imppag = Formattazione.estraiDouble(stok.nextToken());
                        imppagscad = Formattazione.estraiDouble(stok.nextToken());
                        totabbuoni = Formattazione.estraiDouble(stok.nextToken());
                        //nettoapag = OpValute.arrotondaMat(totdoc + totaddeb - totaccred - totivaomaggitotali, 2);
                        friep.edincasso.setText(Formattazione.formatta(imppag, "#########0.00", Formattazione.SEGNO_SX_NEG).replaceAll(",", "."));
                    }

                    String ccx = "";
                    if (this.clicodpadre.equals(""))
                        ccx = this.clicod;
                    else
                        ccx = this.clicodpadre;
                    float totscad = 0;

                    // divisione documento per cessionari
                    righe_nocess = new ArrayList();
                    hrighecess = new HashMap();
                    if (!this.clicodpadre.equals("") && fcli.sptipodoc.getSelectedItemPosition() != 6) {
                        // luogo di consegna
                        // cerca tutti i differenti cessionari
                        for (int i = 0; i < righecrp.size(); i++) {
                            Record riga = righecrp.get(i);
                            if (!riga.leggiStringa("cesscod").equals("")) {
                                if (hrighecess.get(riga.leggiStringa("cesscod")) == null) {
                                    ArrayList<Record> vx = new ArrayList();
                                    vx.add(riga);
                                    hrighecess.put(riga.leggiStringa("cesscod"), vx);
                                } else {
                                    ArrayList<Record> vx = hrighecess.get(riga.leggiStringa("cesscod"));
                                    vx.add(riga);
                                }
                            } else {
                                righe_nocess.add(riga);
                            }
                        }
                    } else {
                        for (int i = 0; i < righecrp.size(); i++) {
                            Record riga = righecrp.get(i);
                            righe_nocess.add(riga);
                        }
                    }

                    double pagato = Formattazione.estraiDouble(friep.edincasso.getText().toString().replace(".", ","));

                    vdocgen = new ArrayList();
                    if (righe_nocess.size() > 0) {
                        ricalcolaTotale(righe_nocess);
                        if (vpagtipo.get(friep.sppagamento.getSelectedItemPosition()) == 8) {
                            if (pagato >= nettoapag) {
                                friep.edincasso.setText(Formattazione.formatta(nettoapag, "#########0.00", Formattazione.SEGNO_SX_NEG).replaceAll(",", "."));
                                imppag = nettoapag;
                                pagato = pagato - nettoapag;
                            } else {
                                friep.edincasso.setText(Formattazione.formatta(pagato, "#########0.00", Formattazione.SEGNO_SX_NEG).replaceAll(",", "."));
                                imppag = pagato;
                                pagato = 0;
                            }
                        }
                        Record risdoc = this.salvaDoc(fcli.sptipodoc.getSelectedItemPosition(), righe_nocess, false, "");
                        Record rx = new Record();
                        rx.insIntero("num", risdoc.leggiIntero("num"));
                        rx.insStringa("doc", risdoc.leggiStringa("doc"));
                        rx.insIntero("tipo", fcli.sptipodoc.getSelectedItemPosition());
                        vdocgen.add(rx);
                    }
                    Iterator it = hrighecess.keySet().iterator();
                    while (it.hasNext()) {
                        String codcess = (String) it.next();
                        ArrayList<Record> vrc = hrighecess.get(codcess);
                        ricalcolaTotale(vrc);
                        Record risdocc = this.salvaDoc(2, vrc, false, codcess);
                        Record rx = new Record();
                        rx.insIntero("num", risdocc.leggiIntero("num"));
                        rx.insStringa("doc", risdocc.leggiStringa("doc"));
                        rx.insIntero("tipo", 2);
                        vdocgen.add(rx);
                    }

                    int cnt_backupperiodico = Formattazione.estraiIntero(FunzioniJBeerApp.leggiProprieta("contatore_backupperiodico"));
                    cnt_backupperiodico++;

                    FunzioniJBeerApp.impostaProprieta("contatore_backupperiodico", "" + cnt_backupperiodico);
                    if (vdocgen.size() > 0) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Documento salvato",
                                Toast.LENGTH_SHORT);
                        toast.show();

                        //this.preparaNuovoDoc();
                        if (Env.nostampadocvend) {
                            if (Env.sceltaemail) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(
                                        FormDocumento.this);
                                builder.setMessage("Invio documento per E-Mail?")
                                        .setPositiveButton("SI",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int id) {
                                                        ArrayList<Record> vdocs = new ArrayList();
                                                        for (int nd = 0; nd < vdocgen.size(); nd++) {
                                                            Record rdocpdf = vdocgen.get(nd);
                                                            Record rx = new Record();
                                                            rx.insStringa("doc", rdocpdf.leggiStringa("doc"));
                                                            rx.insIntero("num", rdocpdf.leggiIntero("num"));
                                                            rx.insStringa("data", (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                                            vdocs.add(rx);
                                                        }
                                                        String emailcliente = "";
                                                        String[] paremail = new String[1];
                                                        paremail[0] = clicod;
                                                        if (!clicodpadre.equals(""))
                                                            paremail[0] = clicodpadre;
                                                        Cursor cemail = Env.db.rawQuery(
                                                                "SELECT cliemail1,cliemail2,cliemailpec FROM clienti WHERE clicodice = ?", paremail);
                                                        if (cemail.moveToFirst()) {
                                                            if (!cemail.getString(0).equals(""))
                                                                emailcliente = cemail.getString(0);
                                                            else if (!cemail.getString(1).equals(""))
                                                                emailcliente = cemail.getString(1);
                                                            else if (!cemail.getString(2).equals(""))
                                                                emailcliente = cemail.getString(2);
                                                        }
                                                        cemail.close();
                                                        FunzioniJBeerApp.invioEMailDocumento(FormDocumento.this, getApplicationContext(), vdocs, emailcliente);
                                                        terminaDDT();
                                                    }
                                                })
                                        .setNegativeButton("NO",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int id) {

                                                        terminaDDT();
                                                    }
                                                });
                                AlertDialog ad = builder.create();
                                ad.setCancelable(false);
                                ad.show();
                            } else {
                                this.preparaNuovoDoc();
                            }
                        } else {
                            if (Env.sceltaemail) {
                                if (FunzioniJBeerApp.stampanteConfigurata()) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(
                                            FormDocumento.this);
                                    builder.setMessage("Stampa documento o invio E-Mail?")
                                            .setPositiveButton("Stampa",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog,
                                                                            int id) {
                                                            docinstampa = 1;
                                                            ristampa = false;
                                                            Intent i = new Intent(FormDocumento.this.getApplicationContext(), FormInfoStampa.class);
                                                            Bundle mBundle = new Bundle();
                                                            mBundle.putString("titolo", "Stampa Documento");
                                                            i.putExtras(mBundle);
                                                            startActivityForResult(i, 2);
                                                        }
                                                    })
                                            .setNegativeButton("E-Mail",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog,
                                                                            int id) {

                                                            ArrayList<Record> vdocs = new ArrayList();
                                                            for (int nd = 0; nd < vdocgen.size(); nd++) {
                                                                Record rdocpdf = vdocgen.get(nd);
                                                                Record rx = new Record();
                                                                rx.insStringa("doc", rdocpdf.leggiStringa("doc"));
                                                                rx.insIntero("num", rdocpdf.leggiIntero("num"));
                                                                rx.insStringa("data", (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                                                vdocs.add(rx);
                                                            }
                                                            String emailcliente = "";
                                                            String[] paremail = new String[1];
                                                            paremail[0] = clicod;
                                                            if (!clicodpadre.equals(""))
                                                                paremail[0] = clicodpadre;
                                                            Cursor cemail = Env.db.rawQuery(
                                                                    "SELECT cliemail1,cliemail2,cliemailpec FROM clienti WHERE clicodice = ?", paremail);
                                                            if (cemail.moveToFirst()) {
                                                                if (!cemail.getString(0).equals(""))
                                                                    emailcliente = cemail.getString(0);
                                                                else if (!cemail.getString(1).equals(""))
                                                                    emailcliente = cemail.getString(1);
                                                                else if (!cemail.getString(2).equals(""))
                                                                    emailcliente = cemail.getString(2);
                                                            }
                                                            cemail.close();
                                                            FunzioniJBeerApp.invioEMailDocumento(FormDocumento.this, getApplicationContext(), vdocs, emailcliente);
                                                            terminaDDT();
                                                        }
                                                    });
                                    AlertDialog ad = builder.create();
                                    ad.setCancelable(false);
                                    ad.show();
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(
                                            FormDocumento.this);
                                    builder.setMessage("Invio documento per E-Mail?")
                                            .setPositiveButton("SI",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog,
                                                                            int id) {
                                                            ArrayList<Record> vdocs = new ArrayList();
                                                            for (int nd = 0; nd < vdocgen.size(); nd++) {
                                                                Record rdocpdf = vdocgen.get(nd);
                                                                Record rx = new Record();
                                                                rx.insStringa("doc", rdocpdf.leggiStringa("doc"));
                                                                rx.insIntero("num", rdocpdf.leggiIntero("num"));
                                                                rx.insStringa("data", (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                                                                vdocs.add(rx);
                                                            }
                                                            String emailcliente = "";
                                                            String[] paremail = new String[1];
                                                            paremail[0] = clicod;
                                                            if (!clicodpadre.equals(""))
                                                                paremail[0] = clicodpadre;
                                                            Cursor cemail = Env.db.rawQuery(
                                                                    "SELECT cliemail1,cliemail2,cliemailpec FROM clienti WHERE clicodice = ?", paremail);
                                                            if (cemail.moveToFirst()) {
                                                                if (!cemail.getString(0).equals(""))
                                                                    emailcliente = cemail.getString(0);
                                                                else if (!cemail.getString(1).equals(""))
                                                                    emailcliente = cemail.getString(1);
                                                                else if (!cemail.getString(2).equals(""))
                                                                    emailcliente = cemail.getString(2);
                                                            }
                                                            cemail.close();
                                                            FunzioniJBeerApp.invioEMailDocumento(FormDocumento.this, getApplicationContext(), vdocs, emailcliente);
                                                            terminaDDT();
                                                        }
                                                    })
                                            .setNegativeButton("NO",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog,
                                                                            int id) {

                                                            terminaDDT();
                                                        }
                                                    });
                                    AlertDialog ad = builder.create();
                                    ad.setCancelable(false);
                                    ad.show();
                                }
                            } else {
                                if (FunzioniJBeerApp.stampanteConfigurata()) {
                                    docinstampa = 1;
                                    ristampa = false;
                                    Intent i = new Intent(FormDocumento.this.getApplicationContext(), FormInfoStampa.class);
                                    Bundle mBundle = new Bundle();
                                    mBundle.putString("titolo", "Stampa Documento");
                                    i.putExtras(mBundle);
                                    startActivityForResult(i, 2);
                                } else {
                                    terminaDDT();
                                }
                            }
                        }
                    }
                } else {
                    // cancella cliente nuovo
                    if (clinuovo && nuovoins) {
                        SQLiteStatement stmt = Env.db.compileStatement("DELETE FROM clienti WHERE clicodice = ?");
                        stmt.clearBindings();
                        stmt.bindString(1, clicod);
                        stmt.execute();
                        stmt.close();
                    }
                    if (modificadoc) {
                        itemP.setEnabled(true);
                        finish();
                    } else {
                        itemP.setEnabled(true);
                        this.preparaNuovoDoc();
                    }
                }
                break;
            }
            case (2): {
                // stampa documento
                Record rdocst = vdocgen.get(docinstampa - 1);
                while (!FunzioniJBeerApp.controlloStampante(Env.cpclPrinter).contains("Normal")) {
                    Log.v("JAZZTV", "BUSY");
                    try {
                        Thread.currentThread().sleep(500);
                    } catch (Exception ex) {
                    }
                }
                if (!ristampa) {
                    Log.v("JAZZTV", "STAMPA DOC COPIA 1");
                    if (rdocst.leggiStringa("doc").equals(Env.docfattura)) {
                        FunzioniJBeerApp.stampaFatturaNew(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"),
                                imppag, imppagscad, cliblocco, cliscop, avvisopagtv, getApplicationContext());
                    } else if (rdocst.leggiStringa("doc").equals(Env.docordinecli)) {
                        FunzioniJBeerApp.stampaOrdineCliente(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"), getApplicationContext());
                    } else if (rdocst.leggiStringa("doc").equals(Env.docbollaval)) {
                        FunzioniJBeerApp.stampaDDT(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"),
                                imppag, imppagscad, cliblocco, cliscop, avvisopagtv, getApplicationContext());
                        if (Env.copieddt >= 2) {
                            try {
                                Thread.currentThread().sleep(2000);
                            } catch (Exception ex) {
                            }
                            FunzioniJBeerApp.stampaDDT(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"),
                                    imppag, imppagscad, cliblocco, cliscop, avvisopagtv, getApplicationContext());
                            Log.v("JAZZTV", "STAMPA DOC COPIA 2");
                        }
                        if (Env.copieddt >= 3) {
                            try {
                                Thread.currentThread().sleep(2000);
                            } catch (Exception ex) {
                            }
                            FunzioniJBeerApp.stampaDDT(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"),
                                    imppag, imppagscad, cliblocco, cliscop, avvisopagtv, getApplicationContext());
                            Log.v("JAZZTV", "STAMPA DOC COPIA 3");

                        }
                    } else if (rdocst.leggiStringa("doc").equals(Env.docbollaqta)) {
                        FunzioniJBeerApp.stampaDDT(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"),
                                imppag, imppagscad, cliblocco, cliscop, avvisopagtv, getApplicationContext());
                        if (Env.copieddtqta >= 2) {
                            try {
                                Thread.currentThread().sleep(2000);
                            } catch (Exception ex) {
                            }
                            FunzioniJBeerApp.stampaDDT(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"),
                                    imppag, imppagscad, cliblocco, cliscop, avvisopagtv, getApplicationContext());
                            Log.v("JAZZTV", "STAMPA DOC COPIA 2");

                        }
                        if (Env.copieddtqta >= 3) {
                            try {
                                Thread.currentThread().sleep(2000);
                            } catch (Exception ex) {
                            }
                            FunzioniJBeerApp.stampaDDT(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"),
                                    imppag, imppagscad, cliblocco, cliscop, avvisopagtv, getApplicationContext());
                            Log.v("JAZZTV", "STAMPA DOC COPIA 3");

                        }
                    }

//                    try
//                    {
//                        Thread.currentThread().sleep(5000);
//                    }
//                    catch (Exception ex)
//                    {
//                    }
//                    Log.v("JAZZTV", "STAMPA DOC COPIA 2");
//                    if (rdocst.leggiStringa("doc").equals(Env.docfattura))
//                    {
//                        FunzioniJazzTv.stampaFattura(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"),
//                                imppag, imppagscad, cliblocco, cliscop, avvisopagtv, getApplicationContext());
//                    }
//                    else
//                    {
//                        FunzioniJazzTv.stampaDDT(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"),
//                                imppag, imppagscad, cliblocco, cliscop, avvisopagtv, getApplicationContext());
//                    }
                } else {
                    Log.v("JAZZTV", "RISTAMPA DOC");
                    if (rdocst.leggiStringa("doc").equals(Env.docfattura)) {
                        FunzioniJBeerApp.stampaFatturaNew(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"),
                                imppag, imppagscad, cliblocco, cliscop, avvisopagtv, getApplicationContext());
                    } else if (rdocst.leggiStringa("doc").equals(Env.docordinecli)) {
                        FunzioniJBeerApp.stampaOrdineCliente(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"), getApplicationContext());
                    } else {
                        FunzioniJBeerApp.stampaDDT(rdocst.leggiStringa("doc"), rdocst.leggiIntero("num"), (new Data()).formatta(Data.AAAA_MM_GG, "-"),
                                imppag, imppagscad, cliblocco, cliscop, avvisopagtv, getApplicationContext());
                    }
                }
                try {
                    Env.wifiPort.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                try {
                    Env.btPort.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormDocumento.this);
                builder.setMessage("Effettuare un'altra stampa di questo documento?")
                        .setPositiveButton("SI",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        Intent i = new Intent(FormDocumento.this.getApplicationContext(), FormInfoStampa.class);
                                        Bundle mBundle = new Bundle();
                                        mBundle.putString("titolo", "Stampa Documento");
                                        i.putExtras(mBundle);
                                        startActivityForResult(i, 2);
                                        ristampa = true;
                                    }
                                })
                        .setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        docinstampa++;
                                        if (docinstampa <= vdocgen.size()) {
                                            ristampa = false;
                                            Intent i = new Intent(FormDocumento.this.getApplicationContext(), FormInfoStampa.class);
                                            Bundle mBundle = new Bundle();
                                            mBundle.putString("titolo", "Stampa Documento");
                                            i.putExtras(mBundle);
                                            startActivityForResult(i, 2);
                                        } else {
                                            terminaDDT();
                                        }
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
                break;
            }
            case (3): {
                break;
            }
            case (4): {
                ris = data.getData();
                if (ris != null) {
                    aggiornamento = (boolean) data.getExtras().get("aggiornamento");
                    if (aggiornamento) {
                        yourdrawable.mutate();
                        yourdrawable.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_IN);
                    }
                }
                break;

            }
        }
    }

    public void terminaDDT() {
        if (modificadoc) {
            HashMap h = new HashMap();
            h.put("modificadoc", true);
            h.put("movtipo", movtipocorr);
            h.put("movdoc", movdoccorr);
            h.put("movdata", movdatacorr);
            h.put("movnum", movnumcorr);

            ArrayList<HashMap> vris = new ArrayList();
            vris.add(h);
            Uri esito = Uri.parse("content://modificadoc/OK");
            Intent result = new Intent(Intent.ACTION_PICK, esito);
            result.putExtra("vris", vris);
            setResult(RESULT_OK, result);
            finish();
        } else {
            preparaNuovoDoc();
        }
    }

    public double[] calcoloPrezzoArticolo(String codart, String clicod) {
        double[] ris = new double[4];
        ris[0] = 0;
        ris[1] = 0;
        ris[2] = 0;
        ris[3] = 0;

        String dtoggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
        // legge dati articolo
        String[] pars = new String[1];
        pars[0] = codart;
        Cursor c = Env.db.rawQuery("SELECT artscprom,artscdatainizio,artscdatafine,artcpesclaltri,artcodsc FROM articoli WHERE artcod = ?", pars);
        if (c.moveToFirst()) {
            // *** prezzo
            int nprz = 1;
            String codlist = "";
            String codsc = c.getString(4);
            // sconto promozionale
            boolean esclaltri = false;
            if (c.getDouble(0) != 0) {
                // controllo data
                boolean promOk = true;
                if (!c.getString(1).equals("0000-00-00") && c.getString(1).compareTo(dtoggi) > 0)
                    promOk = false;
                if (!c.getString(2).equals("0000-00-00") && c.getString(2).compareTo(dtoggi) < 0)
                    promOk = false;
                if (promOk) {
                    if (ris[0] == 0)
                        ris[0] = c.getDouble(0);
                    else if (ris[1] == 0)
                        ris[1] = c.getDouble(0);
                    else if (ris[2] == 0)
                        ris[2] = c.getDouble(0);
                    if (c.getString(3).equals("S"))
                        esclaltri = true;
                }
            }

            if (!clicod.equals("")) {
                codlist = this.clicodlist;
                nprz = this.clinprz;
            } else {
                codlist = Env.clinuovolist;
                nprz = Env.clinuovoposprz;
                if (nprz == 0)
                    nprz = 1;
            }
            // vede se esistono condizioni cliente particolari
            if (!esclaltri && !clicod.equals("")) {
                boolean condes = false;
                String[] pars2 = new String[2];
                pars2[0] = clicod;
                pars2[1] = codart;
                Cursor c2 = Env.db.rawQuery("SELECT cpdatainizio,cpdatafine,cpsc1,cpsc2,cpsc3,cplistcod,cpprezzo FROM condpers WHERE cpclicod = ? AND cpartcod = ?", pars2);
                while (c2.moveToNext()) {
                    // controllo validita
                    boolean condOk = true;
                    if (!c2.getString(0).equals("0000-00-00") && c2.getString(0).compareTo(dtoggi) > 0)
                        condOk = false;
                    if (!c2.getString(1).equals("0000-00-00") && c2.getString(1).compareTo(dtoggi) < 0)
                        condOk = false;
                    if (condOk) {
                        if (c2.getDouble(6) != 0)
                            ris[3] = c2.getDouble(6);
                        else if (c2.getDouble(2) != 0 || c2.getDouble(3) != 0 || c2.getDouble(4) != 0) {
                            if (!c2.getString(5).equals("")) {
                                if (c2.getString(5).equals(codlist)) {
                                    if (c2.getDouble(2) != 0) {
                                        if (ris[0] == 0)
                                            ris[0] = c2.getDouble(2);
                                        else if (ris[1] == 0)
                                            ris[1] = c2.getDouble(2);
                                        else if (ris[2] == 0)
                                            ris[2] = c2.getDouble(2);
                                    }
                                    if (c2.getDouble(3) != 0) {
                                        if (ris[0] == 0)
                                            ris[0] = c2.getDouble(3);
                                        else if (ris[1] == 0)
                                            ris[1] = c2.getDouble(3);
                                        else if (ris[2] == 0)
                                            ris[2] = c2.getDouble(3);
                                    }
                                    if (c2.getDouble(4) != 0) {
                                        if (ris[0] == 0)
                                            ris[0] = c2.getDouble(4);
                                        else if (ris[1] == 0)
                                            ris[1] = c2.getDouble(4);
                                        else if (ris[2] == 0)
                                            ris[2] = c2.getDouble(4);
                                    }
                                }
                            } else {
                                if (c2.getDouble(2) != 0) {
                                    if (ris[0] == 0)
                                        ris[0] = c2.getDouble(2);
                                    else if (ris[1] == 0)
                                        ris[1] = c2.getDouble(2);
                                    else if (ris[2] == 0)
                                        ris[2] = c2.getDouble(2);
                                }
                                if (c2.getDouble(3) != 0) {
                                    if (ris[0] == 0)
                                        ris[0] = c2.getDouble(3);
                                    else if (ris[1] == 0)
                                        ris[1] = c2.getDouble(3);
                                    else if (ris[2] == 0)
                                        ris[2] = c2.getDouble(3);
                                }
                                if (c2.getDouble(4) != 0) {
                                    if (ris[0] == 0)
                                        ris[0] = c2.getDouble(4);
                                    else if (ris[1] == 0)
                                        ris[1] = c2.getDouble(4);
                                    else if (ris[2] == 0)
                                        ris[2] = c2.getDouble(4);
                                }
                            }
                        }
                    }
                }
                c2.close();

                if (!condes && !clicodpadre.equals("") && !clicodpadre.equals(clicod)) {
                    pars2 = new String[2];
                    pars2[0] = clicodpadre;
                    pars2[1] = codart;
                    c2 = Env.db.rawQuery("SELECT cpdatainizio,cpdatafine,cpsc1,cpsc2,cpsc3,cplistcod,cpprezzo FROM condpers WHERE cpclicod = ? AND cpartcod = ?", pars2);
                    while (c2.moveToNext()) {
                        // controllo validita
                        boolean condOk = true;
                        if (!c2.getString(0).equals("0000-00-00") && c2.getString(0).compareTo(dtoggi) > 0)
                            condOk = false;
                        if (!c2.getString(1).equals("0000-00-00") && c2.getString(1).compareTo(dtoggi) < 0)
                            condOk = false;
                        if (condOk) {
                            if (c2.getDouble(6) != 0)
                                ris[3] = c2.getDouble(6);
                            else if (c2.getDouble(2) != 0 || c2.getDouble(3) != 0 || c2.getDouble(4) != 0) {
                                if (!c2.getString(5).equals("")) {
                                    if (c2.getString(5).equals(codlist)) {
                                        if (c2.getDouble(2) != 0) {
                                            if (ris[0] == 0)
                                                ris[0] = c2.getDouble(2);
                                            else if (ris[1] == 0)
                                                ris[1] = c2.getDouble(2);
                                            else if (ris[2] == 0)
                                                ris[2] = c2.getDouble(2);
                                        }
                                        if (c2.getDouble(3) != 0) {
                                            if (ris[0] == 0)
                                                ris[0] = c2.getDouble(3);
                                            else if (ris[1] == 0)
                                                ris[1] = c2.getDouble(3);
                                            else if (ris[2] == 0)
                                                ris[2] = c2.getDouble(3);
                                        }
                                        if (c2.getDouble(4) != 0) {
                                            if (ris[0] == 0)
                                                ris[0] = c2.getDouble(4);
                                            else if (ris[1] == 0)
                                                ris[1] = c2.getDouble(4);
                                            else if (ris[2] == 0)
                                                ris[2] = c2.getDouble(4);
                                        }
                                    }
                                } else {
                                    if (c2.getDouble(2) != 0) {
                                        if (ris[0] == 0)
                                            ris[0] = c2.getDouble(2);
                                        else if (ris[1] == 0)
                                            ris[1] = c2.getDouble(2);
                                        else if (ris[2] == 0)
                                            ris[2] = c2.getDouble(2);
                                    }
                                    if (c2.getDouble(3) != 0) {
                                        if (ris[0] == 0)
                                            ris[0] = c2.getDouble(3);
                                        else if (ris[1] == 0)
                                            ris[1] = c2.getDouble(3);
                                        else if (ris[2] == 0)
                                            ris[2] = c2.getDouble(3);
                                    }
                                    if (c2.getDouble(4) != 0) {
                                        if (ris[0] == 0)
                                            ris[0] = c2.getDouble(4);
                                        else if (ris[1] == 0)
                                            ris[1] = c2.getDouble(4);
                                        else if (ris[2] == 0)
                                            ris[2] = c2.getDouble(4);
                                    }
                                }
                            }
                        }
                    }
                    c2.close();
                }
            }

            if (ris[3] == 0) {
                // legge prezzo di listino
                if (!codlist.equals("")) {
                    String[] parsp = new String[3];
                    parsp[0] = codlist;
                    parsp[1] = codart;
                    parsp[2] = dtoggi;
                    Cursor cp = Env.db.rawQuery("SELECT lprezzo" + nprz + " FROM listini WHERE lcod = ? AND lartcod = ? AND ldataval <= ?", parsp);
                    if (cp.moveToFirst()) {
                        ris[3] = cp.getDouble(0);
                    }
                    cp.close();

                    // legge sconti listino
                    if (!esclaltri) {
                        String[] parsc = new String[2];
                        parsc[0] = codlist;
                        parsc[1] = c.getString(4);
                        Cursor csc = Env.db.rawQuery("SELECT slsc" + nprz + "_1,slsc" + nprz + "_2 FROM scontilist WHERE sllistcod = ? AND slcodsc = ?", parsc);
                        if (csc.moveToFirst()) {
                            if (csc.getDouble(0) != 0) {
                                if (ris[0] == 0)
                                    ris[0] = csc.getDouble(0);
                                else if (ris[1] == 0)
                                    ris[1] = csc.getDouble(0);
                                else if (ris[2] == 0)
                                    ris[2] = csc.getDouble(0);
                            }
                            if (csc.getDouble(1) != 0) {
                                if (ris[0] == 0)
                                    ris[0] = csc.getDouble(1);
                                else if (ris[1] == 0)
                                    ris[1] = csc.getDouble(1);
                                else if (ris[2] == 0)
                                    ris[2] = csc.getDouble(1);
                            }
                        }
                        csc.close();
                    }
                }
            }
        }
        c.close();
        return ris;
    }


    private void azioneTrasmissione() {
        if (FunzioniJBeerApp.leggiMessaggi().size() > 0) {
            Intent i = new Intent(getApplicationContext(), FormMessaggi.class);
            FormDocumento.this.startActivity(i);
        } else {
            Intent i = new Intent(getApplicationContext(), FormScambioDati.class);
            FormDocumento.this.startActivityForResult(i, 4);
        }
    }

    public void mostraDocumento() {

        String[] parsmov = new String[4];
        parsmov[0] = "" + movtipocorr;
        parsmov[1] = "" + movdoccorr;
        parsmov[2] = "" + movdatacorr;
        parsmov[3] = "" + movnumcorr;
        Cursor cm = Env.db.rawQuery("SELECT * FROM movimenti WHERE movtipo=? AND movdoc=? AND movdata=? AND movnum=?", parsmov);
        cm.moveToFirst();
        movsezcorr = cm.getString(cm.getColumnIndex("movsez"));
        movoracorr = cm.getString(cm.getColumnIndex("movdocora"));

        // data
        numdocnd = 0;
        numdocfr = 0;
        docnd = "";
        docfr = "";
        docinstampa = 0;
        ristampa = false;

        // cliente
        fcli.briccli.setEnabled(false);
        fcli.bnuovocli.setEnabled(false);
        nomenuovo = "";
        pivanuovo = "";
        clicodpadre = "";
        clicodiva = "";
        clicodlist = "";
        clinprz = 1;
        cliraggrddt = 1;
        climodprz = "S";
        clistampaprz = "S";
        clifido = 0;
        clibloccofido = "A";
        cliblocco = false;
        clinscadblocco = 0;
        cliimpblocco = 0;
        cliggtollblocco = 0;
        clidtinizioblocco = "0000-00-00";
        cliazioneblocco = 0;
        clidocpref = 0;
        clivalidato = true; // V121X
        clinuovo = false;
        nuovoins = false;
        cliscop = new ArrayList();
        avvisopagtv = false;
        clicausm = "S";
        clicauom = "S";
        clicaureso = "S";
        clicausost = "S";
        climaxscvend = 0;
        clispeseinc = false;
        clispesebol = false;
        clispesecauz = false;
        clispesetrasp = false;
        clibollaval = true;
        clibollaqta = true;
        clifattura = true;
        clicorrisp = true;
        cliddtreso = true;
        cligpse = 0;
        cligpsn = 0;
        if (!cm.getString(cm.getColumnIndex("movdestcod")).equals("")) {
            clicod = cm.getString(cm.getColumnIndex("movdestcod"));
        } else {
            clicod = cm.getString(cm.getColumnIndex("movclicod"));
        }
        visualizzaDatiCliente();

        fcli.edsc1.setEnabled(false);
        fcli.edsc2.setEnabled(false);
        fcli.edsc3.setEnabled(false);

        // tipo doc.
        fcli.sptipodoc.setEnabled(false);
/*        if (!Env.eticbollaval.equals(""))
            vtd.set(1, Env.eticbollaval);
        else
            vtd.set(1, "DDT VENDITA");
        if (!Env.eticbollaqta.equals(""))
            vtd.set(2, Env.eticbollaqta);
        else
            vtd.set(2, "DDT A QUANTITA");
        if (!Env.eticfattura.equals(""))
            vtd.set(3, Env.eticfattura);
        else
            vtd.set(3, "FATTURA");
        vtd.set(4, "CORRISPETTIVO");
        if (!Env.eticddtreso.equals(""))
            vtd.set(5, Env.eticddtreso);
        else
            vtd.set(5, "DDT DI RESO");
        if (!Env.eticordinecli.equals(""))
            vtd.set(6, Env.eticordinecli);
        else
            vtd.set(6, "ORDINE CLIENTE");
        vtd.set(7, "PRE-DDT");*/
        if (cm.getInt(cm.getColumnIndex("movtipo")) == 0) {
            fcli.sptipodoc.setSelection(1);
        } else if (cm.getInt(cm.getColumnIndex("movtipo")) == 1) {
            fcli.sptipodoc.setSelection(2);
        } else if (cm.getInt(cm.getColumnIndex("movtipo")) == 2) {
            fcli.sptipodoc.setSelection(3);
        } else if (cm.getInt(cm.getColumnIndex("movtipo")) == 3) {
            fcli.sptipodoc.setSelection(4);
        } else if (cm.getInt(cm.getColumnIndex("movtipo")) == 4) {
            fcli.sptipodoc.setSelection(5);
        } else if (cm.getInt(cm.getColumnIndex("movtipo")) == 20) {
            fcli.sptipodoc.setSelection(6);
        }
        if (fcli.sptipodoc.getSelectedItemPosition() == 1) {
        //    fcli.tvinfodoc.setText("Modifica DDT valorizzato (n." + cm.getInt(cm.getColumnIndex("movnum")) + ")");
            this.setTitle("Modifica DDT val.n." + cm.getInt(cm.getColumnIndex("movnum")));
        } else if (fcli.sptipodoc.getSelectedItemPosition() == 2) {
         //   fcli.tvinfodoc.setText("Modifica DDT quantità (n." + cm.getInt(cm.getColumnIndex("movnum")) + ")");
            this.setTitle("Modifica DDT qtà.n." + cm.getInt(cm.getColumnIndex("movnum")));
        } else if (fcli.sptipodoc.getSelectedItemPosition() == 3) {
          //  fcli.tvinfodoc.setText("Modifica Fattura (n." + cm.getInt(cm.getColumnIndex("movnum")) + ")");
            this.setTitle("Modifica Fattura.n." + cm.getInt(cm.getColumnIndex("movnum")));
        } else if (fcli.sptipodoc.getSelectedItemPosition() == 4) {
           // fcli.tvinfodoc.setText("Modifica Corrispettivo (n." + cm.getInt(cm.getColumnIndex("movnum")) + ")");
            this.setTitle("Modifica Corrisp.n." + cm.getInt(cm.getColumnIndex("movnum")));
        } else if (fcli.sptipodoc.getSelectedItemPosition() == 5) {
            //fcli.tvinfodoc.setText("Modifica DDT reso (n." + cm.getInt(cm.getColumnIndex("movnum")) + ")");
            this.setTitle("Modifica DDT reso.n." + cm.getInt(cm.getColumnIndex("movnum")));
        } else if (fcli.sptipodoc.getSelectedItemPosition() == 6) {
            //fcli.tvinfodoc.setText("Modifica Ordine cliente (n." + cm.getInt(cm.getColumnIndex("movnum")) + ")");
            this.setTitle("Modifica Ordine cliente.n." + cm.getInt(cm.getColumnIndex("movnum")));
        }

        imppag = 0;
        imppagscad = 0;

        // pagamento
        clicodpag = cm.getString(cm.getColumnIndex("movpagcod"));
        friep.sppagamento.setEnabled(true);
        for (int i = 0; i < vpag.size(); i++) {
            if (clicodpag.equals(vpag.get(i))) {
                friep.sppagamento.setSelection(i);
                break;
            }
        }

        fcli.ednrif.setText(cm.getString(cm.getColumnIndex("movnrif")));

        if (!cm.getString(cm.getColumnIndex("movdatacons")).equals("") && !cm.getString(cm.getColumnIndex("movdatacons")).equals("0000-00-00")) {
            Data dt = new Data(cm.getString(cm.getColumnIndex("movdatacons")), Data.AAAA_MM_GG);
            datacons = dt.formatta(Data.AAAA_MM_GG, "-");
            fcli.tvdatacons.setText(dt.formatta(Data.GG_MM_AAAA, "/") + "\n" + dt.giornoSettimana());
        } else {
            datacons = "";
            fcli.tvdatacons.setText("");
        }

        // articoli
        double csc1 = 0, csc2 = 0, csc3 = 0;
        righecrp = new ArrayList();
        righecrpraggr = new ArrayList();
        String[] parsd = new String[4];
        parsd[0] = "" + movtipocorr;
        parsd[1] = "" + movdoccorr;
        parsd[2] = "" + movdatacorr;
        parsd[3] = "" + movnumcorr;
        Cursor cd = Env.db.rawQuery("SELECT * FROM righemov WHERE rmmovtipo=? AND rmmovdoc=? AND rmmovdata=? AND rmmovnum=?", parsd);
        while (cd.moveToNext()) {
            Record rriga = new Record();
            rriga.insStringa("rmtiporiga", "A");
            rriga.insStringa("rmartcod", cd.getString(cd.getColumnIndex("rmartcod")));
            String descr = "";
            String[] parsa = new String[1];
            parsa[0] = cd.getString(cd.getColumnIndex("rmartcod"));
            Cursor cart = Env.db.rawQuery("SELECT artdescr FROM articoli WHERE artcod = ?", parsa);
            if (cart.moveToFirst())
                descr = cart.getString(0);
            cart.close();
            rriga.insStringa("rmartdescr", descr);
            rriga.insStringa("rmlotto", cd.getString(cd.getColumnIndex("rmlotto")));
            rriga.insStringa("rmcaumag", cd.getString(cd.getColumnIndex("rmcaumag")));
            rriga.insIntero("rmcolli", cd.getInt(cd.getColumnIndex("rmcolli")));
            rriga.insDouble("rmpezzi", cd.getDouble(cd.getColumnIndex("rmpezzi")));
            rriga.insDouble("rmcontenuto", cd.getDouble(cd.getColumnIndex("rmcontenuto")));
            rriga.insDouble("rmqta", cd.getDouble(cd.getColumnIndex("rmqta")));
            rriga.insDouble("rmprz", cd.getDouble(cd.getColumnIndex("rmprz")));
            rriga.insDouble("rmartsc1", cd.getDouble(cd.getColumnIndex("rmartsc1")));
            rriga.insDouble("rmartsc2", cd.getDouble(cd.getColumnIndex("rmartsc2")));
            rriga.insDouble("rmscvend", cd.getDouble(cd.getColumnIndex("rmscvend")));
            rriga.insStringa("rmcodiva", cd.getString(cd.getColumnIndex("rmcodiva")));
            rriga.insDouble("rmaliq", cd.getDouble(cd.getColumnIndex("rmaliq")));
            rriga.insDouble("rmclisc1", cd.getDouble(cd.getColumnIndex("rmclisc1")));
            rriga.insDouble("rmclisc2", cd.getDouble(cd.getColumnIndex("rmclisc2")));
            rriga.insDouble("rmclisc3", cd.getDouble(cd.getColumnIndex("rmclisc3")));
            rriga.insDouble("rmlordo", cd.getDouble(cd.getColumnIndex("rmlordo")));
            float scoart = 0;
            double imp = cd.getDouble(cd.getColumnIndex("rmlordo"));
            if (cd.getDouble(cd.getColumnIndex("rmartsc1")) != 0) {
                double s = (imp * cd.getDouble(cd.getColumnIndex("rmartsc1")) / 100);
                imp -= s;
                scoart += s;
            }
            if (cd.getDouble(cd.getColumnIndex("rmartsc2")) != 0) {
                double s = (imp * cd.getDouble(cd.getColumnIndex("rmartsc2")) / 100);
                imp -= s;
                scoart += s;
            }
            if (cd.getDouble(cd.getColumnIndex("rmscvend")) != 0) {
                double s = (imp * cd.getDouble(cd.getColumnIndex("rmscvend")) / 100);
                imp -= s;
                scoart += s;
            }
            rriga.insDouble("totscontiart", scoart);
            float scocli = 0;
            if (cd.getDouble(cd.getColumnIndex("rmclisc1")) != 0) {
                double s = (imp * cd.getDouble(cd.getColumnIndex("rmclisc1")) / 100);
                imp -= s;
                scocli += s;
            }
            if (cd.getDouble(cd.getColumnIndex("rmclisc2")) != 0) {
                double s = (imp * cd.getDouble(cd.getColumnIndex("rmclisc2")) / 100);
                imp -= s;
                scocli += s;
            }
            if (cd.getDouble(cd.getColumnIndex("rmclisc3")) != 0) {
                double s = (imp * cd.getDouble(cd.getColumnIndex("rmclisc3")) / 100);
                imp -= s;
                scocli += s;
            }
            rriga.insDouble("totsconticli", scocli);
            rriga.insDouble("rmnetto", cd.getDouble(cd.getColumnIndex("rmnetto")));
            rriga.insDouble("rmnettoivato", cd.getDouble(cd.getColumnIndex("rmnettoivato")));
            rriga.insStringa("cesscod", cm.getString(cm.getColumnIndex("movcesscod")));
            rriga.insDouble("rmaccisa", cd.getDouble(cd.getColumnIndex("rmaccisa")));
            //rriga.insDouble("rmcontrassegno", cd.getDouble(cd.getColumnIndex("rmcontrassegno")));
            rriga.insDouble("rmgrado", cd.getDouble(cd.getColumnIndex("rmgrado")));
            rriga.insDouble("rmplato", cd.getDouble(cd.getColumnIndex("rmplato")));
            //rriga.insDouble("rmqtaimpostatabacchi", cd.getDouble(cd.getColumnIndex("rmqtaimpostatabacchi")));
            //rriga.insDouble("rmvalunitimpostatabacchi", cd.getDouble(cd.getColumnIndex("rmvalunitimpostatabacchi")));
            //rriga.insDouble("rmimpostatabacchi", cd.getDouble(cd.getColumnIndex("rmimpostatabacchi")));
            righecrp.add(rriga);
            if (cd.getDouble(cd.getColumnIndex("rmclisc1")) > 0)
                csc1 = cd.getDouble(cd.getColumnIndex("rmclisc1"));
            if (cd.getDouble(cd.getColumnIndex("rmclisc2")) > 0)
                csc2 = cd.getDouble(cd.getColumnIndex("rmclisc2"));
            if (cd.getDouble(cd.getColumnIndex("rmclisc3")) > 0)
                csc3 = cd.getDouble(cd.getColumnIndex("rmclisc3"));
        }
        cd.close();

        aggiornaRigheRaggruppate();

        if (csc1 > 0)
            fcli.edsc1.setText(Formattazione.formatta(csc1, "##0.00", Formattazione.NO_SEGNO).replaceAll(",", "."));
        if (csc2 > 0)
            fcli.edsc2.setText(Formattazione.formatta(csc2, "##0.00", Formattazione.NO_SEGNO).replaceAll(",", "."));
        if (csc3 > 0)
            fcli.edsc3.setText(Formattazione.formatta(csc3, "##0.00", Formattazione.NO_SEGNO).replaceAll(",", "."));

        if (Env.vendqtatot)
            fart.lvdocdettadapter = new ListaDocDettAdapter(this.getApplicationContext(), righecrpraggr, fart);
        else
            fart.lvdocdettadapter = new ListaDocDettAdapter(this.getApplicationContext(), righecrp, fart);
        fart.listaart.setAdapter(fart.lvdocdettadapter);
        fart.lvdocdettadapter.notifyDataSetChanged();

        aggiornaNumeroArticoli();

        righe_nocess = new ArrayList();
        hrighecess = new HashMap();

        // riepilogo
        vscad = new ArrayList();
        ricalcolaTotale(righecrp);

        // incasso
        //incasso_tipocassa = 0;
        friep.edincasso.setText(funzStringa.sostituisci(Formattazione.formatta(cm.getDouble(cm.getColumnIndex("movacconto")), "########0.00", Formattazione.SEGNO_SX_NEG), ',', '.'));
        //friep.edincasso.setVisibility(View.INVISIBLE);

        friep.edannot.setText(cm.getString(cm.getColumnIndex("movnote1")) + cm.getString(cm.getColumnIndex("movnote2")) + cm.getString(cm.getColumnIndex("movnote3")) +
                cm.getString(cm.getColumnIndex("movnote4")) + cm.getString(cm.getColumnIndex("movnote5")) + cm.getString(cm.getColumnIndex("movnote6")));

        // cauzioni
        righecauz = new ArrayList();
        caucorr = 0;

        cm.close();

        mViewPager.setCurrentItem(1);
    }

}
