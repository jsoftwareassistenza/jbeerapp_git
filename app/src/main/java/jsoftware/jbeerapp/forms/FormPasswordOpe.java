package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;

public class FormPasswordOpe extends AppCompatActivity {
    private boolean pwdOk = false;
    TextView tvinfo;
    EditText edpwd;
    Button bok;
    Button bannulla;
    String pwd = "";

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_password_ope);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        tvinfo = findViewById(R.id.pwdope_labelpwd);
        bok = findViewById(R.id.pwdope_buttonOk);
        bannulla = findViewById(R.id.pwdope_buttonAnnulla);
        edpwd = findViewById(R.id.pwdope_pwd);

        if (getIntent().getExtras() != null && getIntent().getExtras().getString("DESCR") != null) {
            tvinfo.setText("Digitare password per autorizzazione " + getIntent().getExtras().getString("DESCR") + ":");
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().getString("PWD") != null) {
            pwd = getIntent().getExtras().getString("PWD");
        }

        bok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (edpwd.getText().toString().equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormPasswordOpe.this);
                    builder.setMessage("Inserire password")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            edpwd.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                } else {
                    if (!pwd.equals("")) {
                        if (edpwd.getText().toString().trim().equals(pwd)) {
                            pwdOk = true;
                        }
                        if (pwdOk) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                    FormPasswordOpe.this);
                            builder.setMessage("Password accettata")
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int id) {
                                                    Uri ris = Uri.parse("content://pwdope/OK:PWDGEN");
                                                    Intent result = new Intent(Intent.ACTION_PICK, ris);
                                                    setResult(RESULT_OK, result);
                                                    finish();
                                                }
                                            });
                            AlertDialog ad = builder.create();
                            ad.setCancelable(false);
                            ad.show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                    FormPasswordOpe.this);
                            builder.setMessage("Password errata")
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int id) {
                                                }
                                            });
                            AlertDialog ad = builder.create();
                            ad.setCancelable(false);
                            ad.show();
                        }
                    } else {
                        String[] pars = new String[1];
                        pars[0] = edpwd.getText().toString().trim();
                        String ope = "";
                        Cursor cl = Env.db.rawQuery("SELECT pwdoperatore FROM password WHERE pwdpassword=?", pars);
                        if (cl.moveToFirst()) {
                            ope = cl.getString(0);
                            pwdOk = true;
                        }
                        cl.close();
                        if (pwdOk) {
                            final String ope2 = ope;
                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                    FormPasswordOpe.this);
                            builder.setMessage("Password accettata\r\nOperatore:" + ope)
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int id) {
                                                    Uri ris = Uri.parse("content://pwdope/OK:" + ope2);
                                                    Intent result = new Intent(Intent.ACTION_PICK, ris);
                                                    setResult(RESULT_OK, result);
                                                    finish();
                                                }
                                            });
                            AlertDialog ad = builder.create();
                            ad.setCancelable(false);
                            ad.show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                    FormPasswordOpe.this);
                            builder.setMessage("Password errata")
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int id) {
                                                }
                                            });
                            AlertDialog ad = builder.create();
                            ad.setCancelable(false);
                            ad.show();
                        }
                    }
                }
            }
        });

        bannulla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uri ris = Uri.parse("content://pwdope/ANNULLA");
                Intent result = new Intent(Intent.ACTION_PICK, ris);
                setResult(RESULT_CANCELED, result);
                finish();
            }
        });

        edpwd.requestFocus();
    }
}
