package jsoftware.jbeerapp.forms;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaAvvisiAdapter;

public class FormVisAvvisi extends AppCompatActivity {
    Button bok = null;
    ListView lista = null;
    ArrayList<String> vavvisi = null;
    private ListaAvvisiAdapter lavadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_vis_avvisi);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("vavvisi"))
            vavvisi = getIntent().getExtras().getStringArrayList("vavvisi");
        else
            vavvisi = new ArrayList();

        bok = findViewById(R.id.visavvisi_buttonOk);
        lista = findViewById(R.id.visavvisi_lista);

        lavadapter = new ListaAvvisiAdapter(this.getApplicationContext(), vavvisi, this);
        lista.setAdapter(lavadapter);

        bok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

    }
}
