package jsoftware.jbeerapp.forms;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaPrefAdapter;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;

public class FormDocPreferiti extends AppCompatActivity {
    private Button bok;
    private Button bannulla;
    private EditText edcod;
    private EditText eddescr;
    private Spinner ricstato;
    private ListView listaart;
    private ListaPrefAdapter lprefadapter;
    private ArrayList<Record> vart;
    public ArrayList<Record> righecrp = null;
    private String clicod = "";
    public String clicodpadre = "";
    private ArrayList<String> lst;
    private SpinnerAdapter lstadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_doc_preferiti);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicod"))
            clicod = getIntent().getExtras().getString("clicod");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicodpadre"))
            clicodpadre = getIntent().getExtras().getString("clicodpadre");

        bok = this.findViewById(R.id.pref_buttonOk);
        bannulla = this.findViewById(R.id.pref_buttonAnnulla);
        ricstato = this.findViewById(R.id.pref_ricstato);
        listaart = this.findViewById(R.id.pref_listaart);
        edcod = this.findViewById(R.id.pref_riccod);
        eddescr = this.findViewById(R.id.pref_ricdescr);
        if (Env.codartnum)
            edcod.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);

        // carica stato
        lst = new ArrayList();
        lst.add("PREFERITI");
        lst.add("NUOVI");
        lst.add("IN OFFERTA");
        lst.add("PREZZO AUMENTATO");
        lst.add("PREZZO DIMINUITO");
        lstadapter = new SpinnerAdapter(FormDocPreferiti.this.getApplicationContext(), R.layout.spinner_item, lst);
        ricstato.setAdapter(lstadapter);

/*        View header = this.getLayoutInflater().inflate(R.layout.pref_listaart_header, null);
        listaart.addHeaderView(header);*/

        vart = new ArrayList<Record>();
        lprefadapter = new ListaPrefAdapter(this.getApplicationContext(), vart);
        listaart.setAdapter(lprefadapter);

        if (Env.depeventocod.equals("")) {
            aggiornaLista();
        } else {
            aggiornaListaEvento();
        }

        bok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ArrayList<HashMap> vris = new ArrayList();
                //if (controlloGiac)
/*
                if (!Env.lottineg)
                {
                    // controllo quantita
                    for (int i = 0; i < vart.size(); i++)
                    {
                        Record r = vart.get(i);
                        if (r.leggiDouble("qtavend") > 0 || r.leggiDouble("qtasm") > 0 || r.leggiDouble("qtaomimp") > 0 ||
                                r.leggiDouble("qtaomtot") > 0)
                        {
                            String lotv = r.leggiStringa("lotto");
                            double diff = OpValute.arrotondaMat(r.leggiDouble("giac") - r.leggiDouble("qtavend") - r.leggiDouble("qtasm") - r.leggiDouble("qtaomimp") - r.leggiDouble("qtaomtot"), 2);
                            if ((r.leggiDouble("qtavend") != 0 || r.leggiDouble("qtasm") != 0 || r.leggiDouble("qtaomimp") != 0 ||
                                r.leggiDouble("qtaomtot") != 0) && diff < 0)
                            {
                                AlertDialog.Builder builder = new AlertDialog.Builder(
                                        FormDocPreferiti.this);
                                builder.setMessage("Giacenza non sufficiente per articolo " + r.leggiStringa("artcod") + " lotto " +
                                        r.leggiStringa("lotto"))
                                        .setPositiveButton("Ok",
                                                new DialogInterface.OnClickListener()
                                                {
                                                    public void onClick(DialogInterface dialog,
                                                                        int id)
                                                    {
                                                    }
                                                });
                                AlertDialog ad = builder.create();
                                ad.setCancelable(false);
                                ad.show();
                                return;
                            }
                        }
                    }
                }
*/
                double quantita = 0.0;
                for (int i = 0; i < vart.size(); i++) {
                    Record r = vart.get(i);
                    if (r.leggiDouble("qtavend") != 0 || r.leggiDouble("qtasm") != 0 || r.leggiDouble("qtaomimp") != 0 ||
                            r.leggiDouble("qtaomtot") != 0) {
                        HashMap h = new HashMap();
                        h.put("artcod", r.leggiStringa("artcod"));
                        h.put("artdescr", r.leggiStringa("artdescr"));
                        h.put("codiva", r.leggiStringa("codiva"));
                        h.put("lotto", r.leggiStringa("lotto"));
                        h.put("qtavend", r.leggiDouble("qtavend"));
                        h.put("qtasm", r.leggiDouble("qtasm"));
                        h.put("qtaomimp", r.leggiDouble("qtaomimp"));
                        h.put("qtaomtot", r.leggiDouble("qtaomtot"));
                        h.put("prezzo", r.leggiDouble("prezzo"));
                        h.put("sc1", r.leggiDouble("sc1"));
                        h.put("sc2", r.leggiDouble("sc2"));
                        h.put("sc3", r.leggiDouble("sc3"));
                        h.put("colli", r.leggiIntero("collivend"));
                        //double qtacarrello = Math.round(r.leggiDouble("qtavend"));
                        int quantitaCarrellotmp = Integer.parseInt(Formattazione.formatta(Math.round(r.leggiDouble("qtavend")), "###0", 0));
                        h.put("quantitaCarrello", quantitaCarrellotmp);
                        vris.add(h);
                    }
                }
                Uri codselez = Uri.parse("content://listapref/OK");
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                result.putExtra("vris", vris);
                setResult(RESULT_OK, result);
                finish();
            }
        });

        bannulla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uri codselez = Uri.parse("content://listapref/ANNULLA");
                Intent result = new Intent(Intent.ACTION_PICK, codselez);
                setResult(RESULT_CANCELED, result);
                finish();
            }
        });

        ricstato.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

/*        edcod.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
                return false;
            }
        });

        eddescr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
                return false;
            }
        });*/

        edcod.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
            }
        });

        eddescr.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Env.depeventocod.equals("")) {
                    aggiornaLista();
                } else {
                    aggiornaListaEvento();
                }
            }
        });

        listaart.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                //Log.v("jorders", "CLICK PREF " + arg0 + " " + arg1 + " " + arg2 + " " + arg3);
                if (arg3 >= 0) {
                    final Record rec = vart.get((int) arg3);
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            FormDocPreferiti.this);
                    final AlertDialog optionDialog = alert.create();
                    optionDialog.setMessage("Dati articolo");
                    LinearLayout ll = new LinearLayout(FormDocPreferiti.this.getApplicationContext());
                    ll.setOrientation(LinearLayout.VERTICAL);

                    final TextView tvlabelart = new TextView(FormDocPreferiti.this);
                    tvlabelart.setText(rec.leggiStringa("artcod") + "-" + rec.leggiStringa("artdescr") + " (" + rec.leggiStringa("artum") + ")");
                    tvlabelart.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                    tvlabelart.setTextColor(Color.BLACK);
                    tvlabelart.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    tvlabelart.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    tvlabelart.setBackgroundDrawable(getResources().getDrawable(R.drawable.textview1));
                    ll.addView(tvlabelart);

                    if (!rec.leggiStringa("artschedaweb").equals("")) {
                        final Button bweb = new Button(FormDocPreferiti.this);
                        bweb.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_public_white_48dp, 0, 0, 0);
                        bweb.setText("Scheda articolo WEB");
                        bweb.setTextColor(Color.WHITE);
                        bweb.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                        ll.addView(bweb);
                        bweb.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                Intent i = new Intent(FormDocPreferiti.this.getApplicationContext(), FormWeb.class);
                                Bundle mBundle = new Bundle();
                                mBundle.putString("URL", rec.leggiStringa("artschedaweb"));
                                mBundle.putString("TITOLO", "Scheda art." + rec.leggiStringa("artcod"));
                                i.putExtras(mBundle);
                                FormDocPreferiti.this.startActivity(i);
                            }
                        });
                    }

                    LinearLayout lqtavend = new LinearLayout(FormDocPreferiti.this.getApplicationContext());
                    lqtavend.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelqtavend = new TextView(FormDocPreferiti.this);
                    tvlabelqtavend.setText("Qtà vendita: ");
                    tvlabelqtavend.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelqtavend.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelqtavend.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    lqtavend.addView(tvlabelqtavend);
                    final EditText inputqvend = new EditText(FormDocPreferiti.this);
                    inputqvend.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputqvend.setHint(" quantità vendita");
                    if (!FunzioniJBeerApp.umDecimale(rec.leggiStringa("artcod")))
                        inputqvend.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    else
                        inputqvend.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputqvend.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputqvend.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtavend.addView(inputqvend);
                    boolean cbloccvend = FunzioniJBeerApp.causaleBloccata(0, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                    if (!cbloccvend) {
                        ll.addView(lqtavend);
                        LinearLayout.LayoutParams lParams_lqtavend = (LinearLayout.LayoutParams) tvlabelqtavend.getLayoutParams();
                        lParams_lqtavend.weight = 0.4f;
                        lParams_lqtavend.width = 0;
                        LinearLayout.LayoutParams lParams_qtavend = (LinearLayout.LayoutParams) inputqvend.getLayoutParams();
                        lParams_qtavend.weight = 0.6f;
                        lParams_qtavend.width = 0;
                    }

                    LinearLayout lqtasm = new LinearLayout(FormDocPreferiti.this.getApplicationContext());
                    lqtasm.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelqtasm = new TextView(FormDocPreferiti.this);
                    tvlabelqtasm.setText("Qtà sconto merce: ");
                    tvlabelqtasm.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelqtasm.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelqtasm.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    lqtasm.addView(tvlabelqtasm);
                    final EditText inputqsm = new EditText(FormDocPreferiti.this);
                    inputqsm.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputqsm.setHint(" quantità sconto merce");
                    if (!FunzioniJBeerApp.umDecimale(rec.leggiStringa("artcod")))
                        inputqsm.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    else
                        inputqsm.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputqsm.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputqsm.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtasm.addView(inputqsm);
                    boolean cbloccsm = FunzioniJBeerApp.causaleBloccata(4, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                    if (!cbloccsm) {
                        ll.addView(lqtasm);
                        LinearLayout.LayoutParams lParams_lqtasm = (LinearLayout.LayoutParams) tvlabelqtasm.getLayoutParams();
                        lParams_lqtasm.weight = 0.4f;
                        lParams_lqtasm.width = 0;
                        LinearLayout.LayoutParams lParams_qtasm = (LinearLayout.LayoutParams) inputqsm.getLayoutParams();
                        lParams_qtasm.weight = 0.6f;
                        lParams_qtasm.width = 0;
                    }

                    LinearLayout lqtaomimp = new LinearLayout(FormDocPreferiti.this.getApplicationContext());
                    lqtaomimp.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelqtaomimp = new TextView(FormDocPreferiti.this);
                    tvlabelqtaomimp.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelqtaomimp.setText("Qtà omaggio imponibile: ");
                    tvlabelqtaomimp.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelqtaomimp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    lqtaomimp.addView(tvlabelqtaomimp);
                    final EditText inputqomimp = new EditText(FormDocPreferiti.this);
                    inputqomimp.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputqomimp.setHint(" quantità omaggio imponibile");
                    if (!FunzioniJBeerApp.umDecimale(rec.leggiStringa("artcod")))
                        inputqomimp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    else
                        inputqomimp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputqomimp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputqomimp.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtaomimp.addView(inputqomimp);
                    boolean cbloccomimp = FunzioniJBeerApp.causaleBloccata(5, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                    if (!cbloccomimp) {
                        ll.addView(lqtaomimp);
                        LinearLayout.LayoutParams lParams_lqtaomimp = (LinearLayout.LayoutParams) tvlabelqtaomimp.getLayoutParams();
                        lParams_lqtaomimp.weight = 0.4f;
                        lParams_lqtaomimp.width = 0;
                        LinearLayout.LayoutParams lParams_qtaomimp = (LinearLayout.LayoutParams) inputqomimp.getLayoutParams();
                        lParams_qtaomimp.weight = 0.6f;
                        lParams_qtaomimp.width = 0;
                    }

                    LinearLayout lqtaomtot = new LinearLayout(FormDocPreferiti.this.getApplicationContext());
                    lqtaomtot.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelqtaomtot = new TextView(FormDocPreferiti.this);
                    tvlabelqtaomtot.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelqtaomtot.setText("Qtà omaggio totale: ");
                    tvlabelqtaomtot.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelqtaomtot.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    lqtaomtot.addView(tvlabelqtaomtot);
                    final EditText inputqomtot = new EditText(FormDocPreferiti.this);
                    inputqomtot.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputqomtot.setHint(" quantità omaggio totale");
                    if (!FunzioniJBeerApp.umDecimale(rec.leggiStringa("artcod")))
                        inputqomtot.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    else
                        inputqomtot.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputqomtot.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputqomtot.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    lqtaomtot.addView(inputqomtot);
                    boolean cbloccomtot = FunzioniJBeerApp.causaleBloccata(6, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                    if (!cbloccomtot) {
                        ll.addView(lqtaomtot);
                        LinearLayout.LayoutParams lParams_lqtaomtot = (LinearLayout.LayoutParams) tvlabelqtaomtot.getLayoutParams();
                        lParams_lqtaomtot.weight = 0.4f;
                        lParams_lqtaomtot.width = 0;
                        LinearLayout.LayoutParams lParams_qtaomtot = (LinearLayout.LayoutParams) inputqomtot.getLayoutParams();
                        lParams_qtaomtot.weight = 0.6f;
                        lParams_qtaomtot.width = 0;
                    }

                    LinearLayout lprz = new LinearLayout(FormDocPreferiti.this.getApplicationContext());
                    lprz.setOrientation(LinearLayout.HORIZONTAL);
                    final TextView tvlabelprz = new TextView(FormDocPreferiti.this);
                    tvlabelprz.setText("Prezzo: ");
                    tvlabelprz.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    tvlabelprz.setTextColor(Color.rgb(5, 50, 73));
                    tvlabelprz.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    lprz.addView(tvlabelprz);

                    final EditText inputp = new EditText(FormDocPreferiti.this);
                    inputp.setHint(" prezzo");
                    inputp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    inputp.setBackgroundDrawable(getResources().getDrawable(R.drawable.grayrect));
                    inputp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    inputp.setTypeface(tvlabelart.getTypeface(), Typeface.BOLD);
                    inputp.setText(Formattazione.formatta(Math.abs(rec.leggiDouble("prezzo")), "######0.000", Formattazione.SEGNO_DX));
                    lprz.addView(inputp);
                    LinearLayout.LayoutParams lParams_lprz = (LinearLayout.LayoutParams) tvlabelprz.getLayoutParams();
                    lParams_lprz.weight = 0.4f;
                    lParams_lprz.width = 0;
                    LinearLayout.LayoutParams lParams_prz = (LinearLayout.LayoutParams) inputp.getLayoutParams();
                    lParams_prz.weight = 0.6f;
                    lParams_prz.width = 0;
                    ll.addView(lprz);
                    if (Env.visprzrif) {
                        LinearLayout lpr = new LinearLayout(FormDocPreferiti.this.getApplicationContext());
                        lpr.setOrientation(LinearLayout.HORIZONTAL);
                        final TextView tvlabelprzrif = new TextView(FormDocPreferiti.this);
                        tvlabelprzrif.setText("Prezzo base: ");
                        tvlabelprzrif.setTextColor(Color.rgb(5, 50, 73));
                        tvlabelprzrif.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                        tvlabelprzrif.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                        lpr.addView(tvlabelprzrif);
                        final TextView tvprzrif = new TextView(FormDocPreferiti.this);
                        tvprzrif.setTypeface(tvprzrif.getTypeface(), Typeface.BOLD);
                        tvprzrif.setTextColor(Color.BLUE);
                        tvprzrif.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                        tvprzrif.setBackgroundDrawable(getResources().getDrawable(R.drawable.textview1));
                        tvprzrif.setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL);
                        tvprzrif.setVisibility(View.INVISIBLE);
                        if (rec.leggiDouble("przrif") > 0)
                            tvprzrif.setText(Formattazione.formatta(rec.leggiDouble("przrif"), "#####0.000", Formattazione.NO_SEGNO));
                        else
                            tvprzrif.setText("");
                        final ImageButton bvisprz = new ImageButton(FormDocPreferiti.this);
                        bvisprz.setImageResource(R.drawable.ic_visibility_white_48dp);
                        bvisprz.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                        lpr.addView(bvisprz);
                        bvisprz.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                if (bvisprz.getTag() == null || (bvisprz.getTag() != null && bvisprz.getTag().equals("NOVIS"))) {
                                    tvprzrif.setVisibility(View.VISIBLE);
                                    bvisprz.setTag("VIS");
                                } else {
                                    tvprzrif.setVisibility(View.INVISIBLE);
                                    bvisprz.setTag("NOVIS");
                                }
                            }
                        });
                        lpr.addView(tvprzrif);
                        LinearLayout.LayoutParams lParams_lprzrif = (LinearLayout.LayoutParams) tvlabelprzrif.getLayoutParams();
                        lParams_lprzrif.weight = 0.4f;
                        lParams_lprzrif.width = 0;
                        lParams_lprzrif.height = LinearLayout.LayoutParams.MATCH_PARENT;
                        LinearLayout.LayoutParams lParams_przrif = (LinearLayout.LayoutParams) tvprzrif.getLayoutParams();
                        lParams_przrif.weight = 0.4f;
                        lParams_przrif.width = 0;
                        lParams_przrif.height = LinearLayout.LayoutParams.MATCH_PARENT;
                        LinearLayout.LayoutParams lParams_bvisprz = (LinearLayout.LayoutParams) bvisprz.getLayoutParams();
                        lParams_bvisprz.weight = 0.2f;
                        lParams_bvisprz.width = 0;
                        ll.addView(lpr);
                    }

                    final TextView tvinfo = new TextView(FormDocPreferiti.this);
                    tvinfo.setTextColor(Color.RED);
                    tvinfo.setTypeface(tvinfo.getTypeface(), Typeface.BOLD);
                    tvinfo.setText("");
                    ll.addView(tvinfo);
                    LinearLayout lb = new LinearLayout(FormDocPreferiti.this.getApplicationContext());
                    lb.setOrientation(LinearLayout.HORIZONTAL);
                    Button bdok = new Button(FormDocPreferiti.this);
                    bdok.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                    bdok.setTextColor(Color.WHITE);
                    bdok.setText("OK");
                    lb.addView(bdok);
                    Button bdann = new Button(FormDocPreferiti.this);
                    bdann.setText("Annulla");
                    bdann.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawbutton1));
                    bdann.setTextColor(Color.WHITE);
                    lb.addView(bdann);
                    LinearLayout.LayoutParams lParams_bdok = (LinearLayout.LayoutParams) bdok.getLayoutParams();
                    lParams_bdok.weight = 0.5f;
                    lParams_bdok.width = 0;
                    LinearLayout.LayoutParams lParams_bdann = (LinearLayout.LayoutParams) bdann.getLayoutParams();
                    lParams_bdann.weight = 0.5f;
                    lParams_bdann.width = 0;
                    ll.addView(lb);

                    bdok.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            String valueqvend = inputqvend.getText().toString();
                            double qvend = OpValute.arrotondaMat(Formattazione.estraiDouble(valueqvend.replace(".", ",")), 3);
                            String valueqsm = inputqsm.getText().toString();
                            double qsm = OpValute.arrotondaMat(Formattazione.estraiDouble(valueqsm.replace(".", ",")), 3);
                            String valueqomimp = inputqomimp.getText().toString();
                            double qomimp = OpValute.arrotondaMat(Formattazione.estraiDouble(valueqomimp.replace(".", ",")), 3);
                            String valueqomtot = inputqomtot.getText().toString();
                            double qomtot = OpValute.arrotondaMat(Formattazione.estraiDouble(valueqomtot.replace(".", ",")), 3);

                            String valuep = inputp.getText().toString();
                            double p = OpValute.arrotondaMat(Formattazione.estraiDouble(valuep.replace(".", ",")), 3);

                            if (qvend > 0) {
                                boolean cbloccvend = FunzioniJBeerApp.causaleBloccata(0, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                                if (cbloccvend) {
                                    tvinfo.setText("Vendita bloccata dalla sede");
                                    return;
                                }
                            }
                            if (qsm > 0) {
                                boolean cbloccsm = FunzioniJBeerApp.causaleBloccata(4, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                                if (cbloccsm) {
                                    tvinfo.setText("Sconto merce bloccata dalla sede");
                                    return;
                                }
                            }
                            if (qomimp > 0) {
                                boolean cbloccomimp = FunzioniJBeerApp.causaleBloccata(5, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                                if (cbloccomimp) {
                                    tvinfo.setText("Omaggio imponibile bloccato dalla sede");
                                    return;
                                }
                            }
                            if (qomtot > 0) {
                                boolean cbloccomtot = FunzioniJBeerApp.causaleBloccata(6, Env.depcod, Env.agecod, clicod, rec.leggiStringa("artcod"), "");
                                if (cbloccomtot) {
                                    tvinfo.setText("Omaggio totale bloccato dalla sede");
                                    return;
                                }
                            }

                            if (Env.ctrprzrif && rec.leggiDouble("przrif") > 0 && p < rec.leggiDouble("przrif")) {
                                tvinfo.setText("ERRORE: Prezzo di vendita minore del prezzo di riferimento");
                                return;
                            } else {
                                rec.eliminaCampo("prezzo");
                                rec.insDouble("prezzo", p);
                                rec.eliminaCampo("qtavend");
                                rec.insDouble("qtavend", qvend);
                                rec.eliminaCampo("qtasm");
                                rec.insDouble("qtasm", qsm);
                                rec.eliminaCampo("qtaomimp");
                                rec.insDouble("qtaomimp", qomimp);
                                rec.eliminaCampo("qtaomtot");
                                rec.insDouble("qtaomtot", qomtot);
                                rec.insIntero("collivend", 0);
                                lprefadapter.notifyDataSetChanged();
                                optionDialog.dismiss();

                            }
                        }
                    });

                    bdann.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            optionDialog.dismiss();
                        }
                    });
                    optionDialog.setView(ll);
                    optionDialog.setCancelable(false);
                    optionDialog.show();

    /*
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            FormDocPreferiti.this);
                    alert.setMessage("Indicare la quantità");
                    LinearLayout ll = new LinearLayout(FormDocPreferiti.this.getApplicationContext());
                    ll.setOrientation(LinearLayout.VERTICAL);
                    final EditText inputq = new EditText(FormDocPreferiti.this);
                    inputq.setHint("quantità");
                    inputq.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    ll.addView(inputq);
                    alert.setView(ll);
                    alert.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    String value = inputq.getText().toString();
                                    double q = Formattazione.estraiDouble(value.replace(".", ","));
                                    rec.eliminaCampo("qta");
                                    rec.insDouble("qta", q);
                                    lprefadapter.notifyDataSetChanged();
                                }
                            });

                    alert.setNegativeButton("Annulla",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    // Canceled.
                                }
                            });
                    alert.show();
    */
                }
            }
        });
    }

    private void aggiornaLista() {
        // mem.qta già inserite in lista
        HashMap<String, Double> hqtavendold = new HashMap();
        HashMap<String, Double> hqtasmold = new HashMap();
        HashMap<String, Double> hqtaomimpold = new HashMap();
        HashMap<String, Double> hqtaomtotold = new HashMap();
        for (int i = 0; i < vart.size(); i++) {
            Record r = vart.get(i);
            if (r.leggiDouble("qtavend") > 0) {
                hqtavendold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtavend")));
            }
            if (r.leggiDouble("qtasm") > 0) {
                hqtasmold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtasm")));
            }
            if (r.leggiDouble("qtaomimp") > 0) {
                hqtaomimpold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtaomimp")));
            }
            if (r.leggiDouble("qtaomtot") > 0) {
                hqtaomtotold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtaomtot")));
            }
        }
        vart.clear();

        String c = edcod.getText().toString().trim();
        String d = eddescr.getText().toString().trim();
        c.replace("'", "''");
        d.replace("'", "''");
        if (ricstato.getSelectedItemPosition() == 0) {
            // caricamento preferiti
            String cc = clicod;
            if (!clicodpadre.equals(""))
                cc = clicodpadre;
            HashMap<String, String> hart = new HashMap();
            String qry = "";
            if (Env.dispart) {
                qry = "SELECT artcod,artdescr,artum,dispqta,artcodiva,prefprezzo,lotcod,lotgiacenza,artnotificaterm,artflagum,artweb_linkscheda " +
                        "FROM articoli LEFT JOIN lotti ON articoli.artcod = lotti.lotartcod " +
                        "INNER JOIN preferiti ON articoli.artcod = preferiti.prefartcod " +
                        "LEFT JOIN catalogo ON catartcod=artcod " +
                        "LEFT JOIN dispart ON artcod=dispartcod WHERE artcod <> '' " +
                        "WHERE prefclicod = '" + cc + "' AND " +
                        "artcod <> '' AND artstato = 'A' " +
                        (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                        (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                        " ORDER BY artcod";
                if (android.os.Build.VERSION.SDK_INT <= 23) {
                    qry += " LIMIT 200";
                }
            } else {
                qry = "SELECT artcod,artdescr,artum,giacqta,artcodiva,prefprezzo,lotcod,lotgiacenza,artnotificaterm,artflagum,artweb_linkscheda " +
                        "FROM articoli LEFT JOIN lotti ON articoli.artcod = lotti.lotartcod " +
                        "INNER JOIN preferiti ON articoli.artcod = preferiti.prefartcod " +
                        "LEFT JOIN catalogo ON catartcod=artcod " +
                        "LEFT JOIN giacart ON artcod=giacartcod " +
                        "WHERE prefclicod = '" + cc + "' AND " +
                        "artcod <> '' AND artstato = 'A' " +
                        (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                        (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                        " ORDER BY artcod";
                if (android.os.Build.VERSION.SDK_INT <= 23) {
                    qry += " LIMIT 200";
                }
            }


            Cursor cursor = Env.db.rawQuery(qry, null);
            while (cursor.moveToNext()) {
                Record rx = new Record();
                hart.put(cursor.getString(0), "");
                rx.insStringa("artcod", cursor.getString(0));
                rx.insStringa("artdescr", cursor.getString(1));
                rx.insStringa("artum", cursor.getString(2));
                rx.insIntero("artflagum", cursor.getInt(9));
                rx.insStringa("artschedaweb", cursor.getString(10));
                rx.insStringa("codiva", cursor.getString(4));
                if (!cursor.isNull(6))
                    rx.insStringa("lotto", cursor.getString(6));
                else
                    rx.insStringa("lotto", ".");
                double vargiac = 0;
                for (int j = 0; j < Env.trasfdoc_righecrp.size(); j++) {
                    Record rr = Env.trasfdoc_righecrp.get(j);
                    String acod = rr.leggiStringa("rmartcod");
                    String lcod = rr.leggiStringa("rmlotto");
                    String cau = rr.leggiStringa("rmcaumag");
                    double qta = rr.leggiDouble("rmqta");
                    if (acod.equals(cursor.getString(0)) && lcod.equals(cursor.getString(6)) && !cau.equals("RN")) {
                        if (cau.equals("RV"))
                            vargiac += qta;
                        else
                            vargiac -= qta;
                    }
                }

                String qrygiac = "SELECT movtipo,movdoc,movsez,movdata,movnum" +
                        " FROM movimenti WHERE movtrasf <> 'S' ORDER BY movtipo,movdata,movnum";
                Cursor cursorgiac = Env.db.rawQuery(qrygiac, null);
                while (cursorgiac.moveToNext()) {
                    String[] parsd = new String[6];
                    parsd[0] = "" + cursorgiac.getInt(0);
                    parsd[1] = cursorgiac.getString(1);
                    parsd[2] = cursorgiac.getString(2);
                    parsd[3] = cursorgiac.getString(3);
                    parsd[4] = "" + cursorgiac.getInt(4);
                    parsd[5] = cursor.getString(0);
                    int nriga = 1;
                    Cursor cd = Env.db.rawQuery(
                            "SELECT rmqta " +
                                    "FROM righemov WHERE rmmovtipo=? AND rmmovdoc=? AND rmmovsez=? AND rmmovdata=? AND rmmovnum=? AND rmartcod=? ORDER BY rmriga",
                            parsd);
                    while (cd.moveToNext()) {
                        vargiac = vargiac - cd.getDouble(0);
                    }
                }


                rx.insDouble("giac", OpValute.arrotondaMat(cursor.getDouble(7) + vargiac, 3));
                if (!clicod.equals("")) {
                    double przvend = 0;
                    double prznetto = 0;
                    double sc1 = 0, sc2 = 0, sc3 = 0;
//                    if (Env.prezzopref)
//                    {
//                        przvend = cursor.getDouble(5);
//                        prznetto = przvend;
//                    }
                    //if (przvend == 0)
                    if (Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
                        double[] prz = FunzioniJBeerApp.calcoloPrezzoArticolo(cursor.getString(0), cc, true);
                        przvend = prz[3];
                        prznetto = przvend;
                        sc1 = prz[0];
                        sc2 = prz[1];
                        sc3 = prz[2];
                        if (prz[0] > 0)
                            prznetto = prznetto - (prznetto * prz[0] / 100);
                        if (prz[1] > 0)
                            prznetto = prznetto - (prznetto * prz[1] / 100);
                        if (prz[2] > 0)
                            prznetto = prznetto - (prznetto * prz[2] / 100);
                        prznetto = OpValute.arrotondaMat(prznetto, 3);
                    }
                    rx.insDouble("sc1", sc1);
                    rx.insDouble("sc2", sc2);
                    rx.insDouble("sc3", sc3);
                    rx.insDouble("prezzo", przvend);
                    rx.insDouble("prezzonetto", prznetto);
                } else {
                    rx.insDouble("sc1", 0);
                    rx.insDouble("sc2", 0);
                    rx.insDouble("sc3", 0);
                    rx.insDouble("prezzo", 0);
                    rx.insDouble("prezzonetto", 0);
                }
                double qvend = 0;
                if (hqtavendold.get(cursor.getString(0) + "|" + cursor.getString(6)) != null)
                    qvend = hqtavendold.get(cursor.getString(0) + "|" + cursor.getString(6)).doubleValue();
                rx.insDouble("qtavend", qvend);
                double qsm = 0;
                if (hqtasmold.get(cursor.getString(0) + "|" + cursor.getString(6)) != null)
                    qsm = hqtasmold.get(cursor.getString(0) + "|" + cursor.getString(6)).doubleValue();
                rx.insDouble("qtasm", qsm);
                double qomimp = 0;
                if (hqtaomimpold.get(cursor.getString(0) + "|" + cursor.getString(6)) != null)
                    qomimp = hqtaomimpold.get(cursor.getString(0) + "|" + cursor.getString(6)).doubleValue();
                rx.insDouble("qtaomimp", qomimp);
                double qomtot = 0;
                if (hqtaomtotold.get(cursor.getString(0) + "|" + cursor.getString(6)) != null)
                    qomtot = hqtaomtotold.get(cursor.getString(0) + "|" + cursor.getString(6)).doubleValue();
                rx.insDouble("qtaomtot", qomtot);
                rx.insIntero("notifica", cursor.getInt(8));
                vart.add(rx);
            }
            cursor.close();
        } else {
            // nuovi, in offerta, prezzo variato ecc.
            String qry = "SELECT artcod,artdescr,artum,giacqta,artcodiva,artnotificaterm,lotcod,lotgiacenza,artweb_linkscheda " +
                    "FROM articoli LEFT JOIN lotti ON articoli.artcod = lotti.lotartcod " +
                    "LEFT JOIN catalogo ON catartcod=artcod " +
                    //" WHERE lotcod <> '' AND artgiacenza > 0 AND lotgiacenza > 0 AND artstato = 'A' ";
                    " WHERE artstato = 'A' ";
            qry += " AND artnotificaterm = " + ricstato.getSelectedItemPosition();
            if (!c.equals("")) {
                qry += " AND artcod LIKE \"%" + c + "%\"";
            }
            if (!d.equals("")) {
                qry += " AND artdescr LIKE \"%" + d + "%\"";
            }
            qry += " ORDER BY artcod";
            Cursor ca = Env.db.rawQuery(qry, null);
            while (ca.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", ca.getString(0));
                rx.insStringa("artdescr", ca.getString(1));
                rx.insStringa("artum", ca.getString(2));
                rx.insStringa("artschedaweb", ca.getString(8));
                rx.insStringa("codiva", ca.getString(4));
                if (!ca.isNull(6))
                    rx.insStringa("lotto", ca.getString(6));
                else
                    rx.insStringa("lotto", ".");
                double vargiac = 0;
                for (int j = 0; j < Env.trasfdoc_righecrp.size(); j++) {
                    Record rr = Env.trasfdoc_righecrp.get(j);
                    String acod = rr.leggiStringa("rmartcod");
                    String lcod = rr.leggiStringa("rmlotto");
                    String cau = rr.leggiStringa("rmcaumag");
                    double qta = rr.leggiDouble("rmqta");
                    if (acod.equals(ca.getString(0)) && lcod.equals(ca.getString(6)) && !cau.equals("RN")) {
                        if (cau.equals("RV"))
                            vargiac += qta;
                        else
                            vargiac -= qta;
                    }
                }
                rx.insDouble("giac", OpValute.arrotondaMat(ca.getDouble(7) + vargiac, 3));
                if (!clicod.equals("")) {
                    String cc = clicod;
                    if (!clicodpadre.equals(""))
                        cc = clicodpadre;
                    double przvend = 0;
                    double prznetto = 0;
                    double sc1 = 0, sc2 = 0, sc3 = 0;
//                    if (Env.prezzopref)
//                    {
//                        przvend = FunzioniJBeerApp.leggiPrezzoDaPreferiti(ca.getString(0), cc);
//                        prznetto = przvend;
//                    }
//                    if (przvend == 0)
                    if (Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
                        double[] prz = FunzioniJBeerApp.calcoloPrezzoArticolo(ca.getString(0), cc, true);
                        przvend = prz[3];
                        prznetto = przvend;
                        sc1 = prz[0];
                        sc2 = prz[1];
                        sc3 = prz[2];
                        if (prz[0] > 0)
                            prznetto = prznetto - (prznetto * prz[0] / 100);
                        if (prz[1] > 0)
                            prznetto = prznetto - (prznetto * prz[1] / 100);
                        if (prz[2] > 0)
                            prznetto = prznetto - (prznetto * prz[2] / 100);
                        prznetto = OpValute.arrotondaMat(prznetto, 3);
                    }
                    rx.insDouble("sc1", sc1);
                    rx.insDouble("sc2", sc2);
                    rx.insDouble("sc3", sc3);
                    rx.insDouble("prezzo", przvend);
                    rx.insDouble("prezzonetto", prznetto);
                } else {
                    rx.insDouble("sc1", 0);
                    rx.insDouble("sc2", 0);
                    rx.insDouble("sc3", 0);
                    rx.insDouble("prezzo", 0);
                    rx.insDouble("prezzonetto", 0);
                }
                double qvend = 0;
                if (hqtavendold.get(ca.getString(0) + "|" + ca.getString(6)) != null)
                    qvend = hqtavendold.get(ca.getString(0) + "|" + ca.getString(6)).doubleValue();
                rx.insDouble("qtavend", qvend);
                double qsm = 0;
                if (hqtasmold.get(ca.getString(0) + "|" + ca.getString(6)) != null)
                    qsm = hqtasmold.get(ca.getString(0) + "|" + ca.getString(6)).doubleValue();
                rx.insDouble("qtasm", qsm);
                double qomimp = 0;
                if (hqtaomimpold.get(ca.getString(0) + "|" + ca.getString(6)) != null)
                    qomimp = hqtaomimpold.get(ca.getString(0) + "|" + ca.getString(6)).doubleValue();
                rx.insDouble("qtaomimp", qomimp);
                double qomtot = 0;
                if (hqtaomtotold.get(ca.getString(0) + "|" + ca.getString(6)) != null)
                    qomtot = hqtaomtotold.get(ca.getString(0) + "|" + ca.getString(6)).doubleValue();
                rx.insDouble("qtaomtot", qomtot);
                rx.insIntero("notifica", ca.getInt(5));
                vart.add(rx);
            }
            ca.close();
        }
        lprefadapter.notifyDataSetChanged();

    }


    private void aggiornaListaEvento() {
        // mem.qta già inserite in lista
        HashMap<String, Double> hqtavendold = new HashMap();
        HashMap<String, Double> hqtasmold = new HashMap();
        HashMap<String, Double> hqtaomimpold = new HashMap();
        HashMap<String, Double> hqtaomtotold = new HashMap();
        for (int i = 0; i < vart.size(); i++) {
            Record r = vart.get(i);
            if (r.leggiDouble("qtavend") > 0) {
                hqtavendold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtavend")));
            }
            if (r.leggiDouble("qtasm") > 0) {
                hqtasmold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtasm")));
            }
            if (r.leggiDouble("qtaomimp") > 0) {
                hqtaomimpold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtaomimp")));
            }
            if (r.leggiDouble("qtaomtot") > 0) {
                hqtaomtotold.put(r.leggiStringa("artcod") + "|" + r.leggiStringa("lotto"), new Double(r.leggiDouble("qtaomtot")));
            }
        }
        vart.clear();

        String c = edcod.getText().toString().trim();
        String d = eddescr.getText().toString().trim();
        c.replace("'", "''");
        d.replace("'", "''");
        if (ricstato.getSelectedItemPosition() == 0) {
            // caricamento preferiti
            String cc = clicod;
            if (!clicodpadre.equals(""))
                cc = clicodpadre;
            HashMap<String, String> hart = new HashMap();
            Cursor cursor = Env.db.rawQuery(
                    "SELECT artcod,artdescr,artum,artgiacenza,artcodiva,prefprezzo,lotcod,lotgiacenza,artnotificaterm,artflagum,artweb_linkscheda " +
                            "FROM articoli LEFT JOIN lotti ON articoli.artcod = lotti.lotartcod " +
                            "INNER JOIN preferiti " +
                            "LEFT JOIN catalogo ON catartcod=artcod " +
                            "ON articoli.artcod = preferiti.prefartcod WHERE prefclicod = '" + cc + "' AND " +
                            //"artcod <> '' AND lotcod <> '' AND artgiacenza > 0 AND lotgiacenza > 0 AND artstato = 'A' " +
                            "artcod <> '' AND artstato = 'A' " +
                            (!c.equals("") ? " AND artcod LIKE \"%" + c + "%\"" : "") +
                            (!d.equals("") ? " AND artdescr LIKE \"%" + d + "%\"" : "") +
                            " ORDER BY artcod", null);
            while (cursor.moveToNext()) {
                Record rx = new Record();
                hart.put(cursor.getString(0), "");
                rx.insStringa("artcod", cursor.getString(0));
                rx.insStringa("artdescr", cursor.getString(1));
                rx.insStringa("artum", cursor.getString(2));
                rx.insIntero("artflagum", cursor.getInt(9));
                rx.insStringa("artschedaweb", cursor.getString(10));
                rx.insStringa("codiva", cursor.getString(4));
                if (!cursor.isNull(6))
                    rx.insStringa("lotto", cursor.getString(6));
                else
                    rx.insStringa("lotto", ".");
                double vargiac = 0;
                for (int j = 0; j < Env.trasfdoc_righecrp.size(); j++) {
                    Record rr = Env.trasfdoc_righecrp.get(j);
                    String acod = rr.leggiStringa("rmartcod");
                    String lcod = rr.leggiStringa("rmlotto");
                    String cau = rr.leggiStringa("rmcaumag");
                    double qta = rr.leggiDouble("rmqta");
                    if (acod.equals(cursor.getString(0)) && lcod.equals(cursor.getString(6)) && !cau.equals("RN")) {
                        if (cau.equals("RV"))
                            vargiac += qta;
                        else
                            vargiac -= qta;
                    }
                }
                rx.insDouble("giac", OpValute.arrotondaMat(cursor.getDouble(7) + vargiac, 3));
                if (!clicod.equals("")) {
                    double przvend = 0;
                    double prznetto = 0;
                    double sc1 = 0, sc2 = 0, sc3 = 0;
//                    if (Env.prezzopref)
//                    {
//                        przvend = cursor.getDouble(5);
//                        prznetto = przvend;
//                    }
                    //if (przvend == 0)
                    if (Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
                        double[] prz = FunzioniJBeerApp.calcoloPrezzoArticolo(cursor.getString(0), cc, true);
                        przvend = prz[3];
                        prznetto = przvend;
                        sc1 = prz[0];
                        sc2 = prz[1];
                        sc3 = prz[2];
                        if (prz[0] > 0)
                            prznetto = prznetto - (prznetto * prz[0] / 100);
                        if (prz[1] > 0)
                            prznetto = prznetto - (prznetto * prz[1] / 100);
                        if (prz[2] > 0)
                            prznetto = prznetto - (prznetto * prz[2] / 100);
                        prznetto = OpValute.arrotondaMat(prznetto, 3);
                    }
                    rx.insDouble("sc1", sc1);
                    rx.insDouble("sc2", sc2);
                    rx.insDouble("sc3", sc3);
                    rx.insDouble("prezzo", przvend);
                    rx.insDouble("prezzonetto", prznetto);
                } else {
                    rx.insDouble("sc1", 0);
                    rx.insDouble("sc2", 0);
                    rx.insDouble("sc3", 0);
                    rx.insDouble("prezzo", 0);
                    rx.insDouble("prezzonetto", 0);
                }
                double qvend = 0;
                if (hqtavendold.get(cursor.getString(0) + "|" + cursor.getString(6)) != null)
                    qvend = hqtavendold.get(cursor.getString(0) + "|" + cursor.getString(6)).doubleValue();
                rx.insDouble("qtavend", qvend);
                double qsm = 0;
                if (hqtasmold.get(cursor.getString(0) + "|" + cursor.getString(6)) != null)
                    qsm = hqtasmold.get(cursor.getString(0) + "|" + cursor.getString(6)).doubleValue();
                rx.insDouble("qtasm", qsm);
                double qomimp = 0;
                if (hqtaomimpold.get(cursor.getString(0) + "|" + cursor.getString(6)) != null)
                    qomimp = hqtaomimpold.get(cursor.getString(0) + "|" + cursor.getString(6)).doubleValue();
                rx.insDouble("qtaomimp", qomimp);
                double qomtot = 0;
                if (hqtaomtotold.get(cursor.getString(0) + "|" + cursor.getString(6)) != null)
                    qomtot = hqtaomtotold.get(cursor.getString(0) + "|" + cursor.getString(6)).doubleValue();
                rx.insDouble("qtaomtot", qomtot);
                rx.insIntero("notifica", cursor.getInt(8));
                vart.add(rx);
            }
            cursor.close();
        } else {
            // nuovi, in offerta, prezzo variato ecc.
            String qry = "SELECT artcod,artdescr,artum,artgiacenza,artcodiva,artnotificaterm,lotcod,lotgiacenza,artweb_linkscheda " +
                    "FROM articoli LEFT JOIN lotti ON articoli.artcod = lotti.lotartcod " +
                    "LEFT JOIN catalogo ON catartcod=artcod " +
                    //" WHERE lotcod <> '' AND artgiacenza > 0 AND lotgiacenza > 0 AND artstato = 'A' ";
                    " WHERE artstato = 'A' ";
            qry += " AND artnotificaterm = " + ricstato.getSelectedItemPosition();
            if (!c.equals("")) {
                qry += " AND artcod LIKE \"%" + c + "%\"";
            }
            if (!d.equals("")) {
                qry += " AND artdescr LIKE \"%" + d + "%\"";
            }
            qry += " ORDER BY artcod";
            Cursor ca = Env.db.rawQuery(qry, null);
            while (ca.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", ca.getString(0));
                rx.insStringa("artdescr", ca.getString(1));
                rx.insStringa("artum", ca.getString(2));
                rx.insStringa("artschedaweb", ca.getString(8));
                rx.insStringa("codiva", ca.getString(4));
                if (!ca.isNull(6))
                    rx.insStringa("lotto", ca.getString(6));
                else
                    rx.insStringa("lotto", ".");
                double vargiac = 0;
                for (int j = 0; j < Env.trasfdoc_righecrp.size(); j++) {
                    Record rr = Env.trasfdoc_righecrp.get(j);
                    String acod = rr.leggiStringa("rmartcod");
                    String lcod = rr.leggiStringa("rmlotto");
                    String cau = rr.leggiStringa("rmcaumag");
                    double qta = rr.leggiDouble("rmqta");
                    if (acod.equals(ca.getString(0)) && lcod.equals(ca.getString(6)) && !cau.equals("RN")) {
                        if (cau.equals("RV"))
                            vargiac += qta;
                        else
                            vargiac -= qta;
                    }
                }
                rx.insDouble("giac", OpValute.arrotondaMat(ca.getDouble(7) + vargiac, 3));
                if (!clicod.equals("")) {
                    String cc = clicod;
                    if (!clicodpadre.equals(""))
                        cc = clicodpadre;
                    double przvend = 0;
                    double prznetto = 0;
                    double sc1 = 0, sc2 = 0, sc3 = 0;
//                    if (Env.prezzopref)
//                    {
//                        przvend = FunzioniJBeerApp.leggiPrezzoDaPreferiti(ca.getString(0), cc);
//                        prznetto = przvend;
//                    }
//                    if (przvend == 0)
                    if (Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
                        double[] prz = FunzioniJBeerApp.calcoloPrezzoArticolo(ca.getString(0), cc, true);
                        przvend = prz[3];
                        prznetto = przvend;
                        sc1 = prz[0];
                        sc2 = prz[1];
                        sc3 = prz[2];
                        if (prz[0] > 0)
                            prznetto = prznetto - (prznetto * prz[0] / 100);
                        if (prz[1] > 0)
                            prznetto = prznetto - (prznetto * prz[1] / 100);
                        if (prz[2] > 0)
                            prznetto = prznetto - (prznetto * prz[2] / 100);
                        prznetto = OpValute.arrotondaMat(prznetto, 3);
                    }
                    rx.insDouble("sc1", sc1);
                    rx.insDouble("sc2", sc2);
                    rx.insDouble("sc3", sc3);
                    rx.insDouble("prezzo", przvend);
                    rx.insDouble("prezzonetto", prznetto);
                } else {
                    rx.insDouble("sc1", 0);
                    rx.insDouble("sc2", 0);
                    rx.insDouble("sc3", 0);
                    rx.insDouble("prezzo", 0);
                    rx.insDouble("prezzonetto", 0);
                }
                double qvend = 0;
                if (hqtavendold.get(ca.getString(0) + "|" + ca.getString(6)) != null)
                    qvend = hqtavendold.get(ca.getString(0) + "|" + ca.getString(6)).doubleValue();
                rx.insDouble("qtavend", qvend);
                double qsm = 0;
                if (hqtasmold.get(ca.getString(0) + "|" + ca.getString(6)) != null)
                    qsm = hqtasmold.get(ca.getString(0) + "|" + ca.getString(6)).doubleValue();
                rx.insDouble("qtasm", qsm);
                double qomimp = 0;
                if (hqtaomimpold.get(ca.getString(0) + "|" + ca.getString(6)) != null)
                    qomimp = hqtaomimpold.get(ca.getString(0) + "|" + ca.getString(6)).doubleValue();
                rx.insDouble("qtaomimp", qomimp);
                double qomtot = 0;
                if (hqtaomtotold.get(ca.getString(0) + "|" + ca.getString(6)) != null)
                    qomtot = hqtaomtotold.get(ca.getString(0) + "|" + ca.getString(6)).doubleValue();
                rx.insDouble("qtaomtot", qomtot);
                rx.insIntero("notifica", ca.getInt(5));
                vart.add(rx);
            }
            ca.close();
        }
        lprefadapter.notifyDataSetChanged();

    }


    @Override
    public void onBackPressed() {
        Uri codselez = Uri.parse("content://listapref/ANNULLA");
        Intent result = new Intent(Intent.ACTION_PICK, codselez);
        setResult(RESULT_CANCELED, result);
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v("JAZZTV", "*************RESUME");
    }
}
