package jsoftware.jbeerapp.forms;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.ListaStatArtAdapter;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentStatArticoli.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentStatArticoli#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentStatArticoli extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ListView lista;
    private TextView tvtotvend;

    private String datainizio = "";
    private String datafine = "";
    private String clicod = "";

    private ArrayList<Record> vart;

    private ListaStatArtAdapter ladapter;
    private TextView tv;

    public FragmentStatArticoli() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentStatArticoli.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentStatArticoli newInstance(String param1, String param2) {
        FragmentStatArticoli fragment = new FragmentStatArticoli();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_stat_articoli, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String azione);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lista = view.findViewById(R.id.statart_lista);
        tvtotvend = view.findViewById(R.id.statart_totvend);
        tv = view.findViewById(R.id.statart_label_totvend);
    }

    private void calcolaDati() {
        vart = FunzioniJBeerApp.vendutoArticoli(datainizio, datafine, clicod, 2);
        ladapter = new ListaStatArtAdapter(this.getActivity().getApplicationContext(), vart);
        lista.setAdapter(ladapter);
        double tv = 0;
        for (Record r : vart) {
            tv += r.leggiDouble("val");
        }
        tvtotvend.setText(Formattazione.formValuta(OpValute.arrotondaMat(tv, 2), 12, 2, Formattazione.SEGNO_SX_NEG));
    }

    private void calcolaDatiOrdinato() {
        tv.setText("Totale ordinato ");
        vart = FunzioniJBeerApp.ordinatoArticoli(datainizio, datafine, clicod, 2);
        ladapter = new ListaStatArtAdapter(this.getActivity().getApplicationContext(), vart);
        lista.setAdapter(ladapter);
        double tv = 0;
        for (Record r : vart) {
            tv += r.leggiDouble("val");
        }
        tvtotvend.setText(Formattazione.formValuta(OpValute.arrotondaMat(tv, 2), 12, 2, Formattazione.SEGNO_SX_NEG));
    }

    public void impostaFiltri(String dti, String dtf, String cli) {
        datainizio = dti;
        datafine = dtf;
        clicod = cli;
    }

    @Override
    public void onStart() {
        super.onStart();
        System.out.println("STATART START " + datainizio + " " + datafine + " " + clicod);
        if (Env.termordini) {
            calcolaDatiOrdinato();
        } else {
            calcolaDati();
        }
    }
}
