package jsoftware.jbeerapp.forms;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import jsoftware.jbeerapp.R;

public class FormStatisticheEventi extends AppCompatActivity implements FragmentStatEventiFiltri.OnFragmentInteractionListener,
        FragmentStatEventiVendite.OnFragmentInteractionListener, FragmentStatEventiMerceInviata.OnFragmentInteractionListener,
        FragmentStatEventiMerceTornata.OnFragmentInteractionListener, FragmentStatEventiContatti.OnFragmentInteractionListener {
    private Fragment fragmentAttuale = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_statistiche_eventi);
//
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        FragmentStatInfo fr = new FragmentStatInfo();
//        fragmentTransaction.add(R.id.stat_frag, fr);
//        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        fragmentTransaction.commit();
//        fragmentAttuale = fr;
    }

    @Override
    public void onFragmentInteraction(String azione) {
        if (azione.startsWith("STATFILTRI")) {
            String[] tmp = azione.split("\\|", -1);
            if (tmp.length == 3) {
                if (azione.contains("|VEND|")) {
                    String eventocod = tmp[2];
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    FragmentStatEventiVendite fr = new FragmentStatEventiVendite();
                    fr.impostaFiltri(eventocod);
                    if (fragmentAttuale != null)
                        fragmentTransaction.remove(fragmentAttuale);
                    fragmentTransaction.add(R.id.stat_frag, fr);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    fragmentTransaction.commit();
                    fragmentAttuale = fr;
                } else if (azione.contains("|MERCEINVIATA|")) {
                    String eventocod = tmp[2];
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    FragmentStatEventiMerceInviata fr = new FragmentStatEventiMerceInviata();
                    fr.impostaFiltri(eventocod);
                    if (fragmentAttuale != null)
                        fragmentTransaction.remove(fragmentAttuale);
                    fragmentTransaction.add(R.id.stat_frag, fr);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    fragmentTransaction.commit();
                    fragmentAttuale = fr;
                } else if (azione.contains("|MERCETORNATA|")) {
                    String eventocod = tmp[2];
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    FragmentStatEventiMerceTornata fr = new FragmentStatEventiMerceTornata();
                    fr.impostaFiltri(eventocod);
                    if (fragmentAttuale != null)
                        fragmentTransaction.remove(fragmentAttuale);
                    fragmentTransaction.add(R.id.stat_frag, fr);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    fragmentTransaction.commit();
                    fragmentAttuale = fr;
                } else if (azione.contains("|CONTATTI|")) {
                    String eventocod = tmp[2];
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    FragmentStatEventiContatti fr = new FragmentStatEventiContatti();
                    fr.impostaFiltri(eventocod);
                    if (fragmentAttuale != null)
                        fragmentTransaction.remove(fragmentAttuale);
                    fragmentTransaction.add(R.id.stat_frag, fr);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    fragmentTransaction.commit();
                    fragmentAttuale = fr;
                }
            }
        }
    }


}
