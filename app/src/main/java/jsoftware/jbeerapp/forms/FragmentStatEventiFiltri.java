package jsoftware.jbeerapp.forms;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Env;

public class FragmentStatEventiFiltri extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Spinner spevento = null;
    private ImageButton bvend;
    private ImageButton bcontatti;
    private ImageButton bmerceinviata;
    private ImageButton bmercetornata;
    private ArrayList<String> veventi = new ArrayList();
    private ArrayList<String> veventiCod = new ArrayList();


    private OnFragmentInteractionListener mListener;
    private int ColorShareBlue;
    private int ColorArancione;
    private TextView tvsel;

    public FragmentStatEventiFiltri() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        ColorShareBlue = Color.parseColor("#0091ea"); //shareblue
        ColorArancione = Color.parseColor("#ffc107"); //Amber

        //tvgiac.setTextColor(newColor);


    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentStatFiltri.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentStatEventiFiltri newInstance(String param1, String param2) {
        FragmentStatEventiFiltri fragment = new FragmentStatEventiFiltri();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_stat_eventi_filtri, container, false);

    }

    public void sceltaVendite() {
        if (mListener != null) {
            tvsel.setText("Vendite");
            bvend.setBackgroundColor(ColorArancione);
            //bvend.setTextColor(Color.BLACK);
            bcontatti.setBackgroundColor(ColorShareBlue);
            //bcontatti.setTextColor(Color.WHITE);
            bmerceinviata.setBackgroundColor(ColorShareBlue);
            // bmerceinviata.setTextColor(Color.WHITE);
            bmercetornata.setBackgroundColor(ColorShareBlue);
            //bmercetornata.setTextColor(Color.WHITE);
            mListener.onFragmentInteraction("STATFILTRI|VEND|" + veventiCod.get(spevento.getSelectedItemPosition()));
        }
    }

    public void sceltaContatti() {
        if (mListener != null) {
            tvsel.setText("Contatti Acquisiti");
            bvend.setBackgroundColor(ColorShareBlue);
            //bvend.setTextColor(Color.WHITE);
            bcontatti.setBackgroundColor(ColorArancione);
            // bcontatti.setTextColor(Color.BLACK);
            bmerceinviata.setBackgroundColor(ColorShareBlue);
            //bmerceinviata.setTextColor(Color.WHITE);
            bmercetornata.setBackgroundColor(ColorShareBlue);
            // bmercetornata.setTextColor(Color.WHITE);
            mListener.onFragmentInteraction("STATFILTRI|CONTATTI|" + veventiCod.get(spevento.getSelectedItemPosition()));
        }
    }

    public void sceltaMerceInviata() {
        if (mListener != null) {
            tvsel.setText("Merce inviata all'evento");
            bvend.setBackgroundColor(ColorShareBlue);
            // bvend.setTextColor(Color.WHITE);
            bcontatti.setBackgroundColor(ColorShareBlue);
            // bcontatti.setTextColor(Color.WHITE);
            bmerceinviata.setBackgroundColor(ColorArancione);
            //bmerceinviata.setTextColor(Color.BLACK);
            bmercetornata.setBackgroundColor(ColorShareBlue);
            // bmercetornata.setTextColor(Color.WHITE);
            mListener.onFragmentInteraction("STATFILTRI|MERCEINVIATA|" + veventiCod.get(spevento.getSelectedItemPosition()));
        }
    }

    public void sceltaMerceTornata() {
        if (mListener != null) {
            tvsel.setText("Merce rientrata in sede");
            bvend.setBackgroundColor(ColorShareBlue);
            // bvend.setTextColor(Color.WHITE);
            bcontatti.setBackgroundColor(ColorShareBlue);
            // bcontatti.setTextColor(Color.WHITE);
            bmerceinviata.setBackgroundColor(ColorShareBlue);
            // bmerceinviata.setTextColor(Color.WHITE);
            bmercetornata.setBackgroundColor(ColorArancione);
            // bmercetornata.setTextColor(Color.BLACK);
            mListener.onFragmentInteraction("STATFILTRI|MERCETORNATA|" + veventiCod.get(spevento.getSelectedItemPosition()));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String azione);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spevento = view.findViewById(R.id.statfiltri_evento);
        bvend = view.findViewById(R.id.statfiltri_buttonVend);
        bcontatti = view.findViewById(R.id.statfiltri_buttonContatti);
        bmerceinviata = view.findViewById(R.id.statfiltri_buttonMerceInviata);
        bmercetornata = view.findViewById(R.id.statfiltri_buttonMerceTornata);
        tvsel = view.findViewById(R.id.statfiltri_tv_button);

        veventi = new ArrayList();
        veventi.add("...");
        veventiCod = new ArrayList();
        veventiCod.add("");
        Cursor cursor = Env.db.rawQuery("SELECT depcod,depdescr,depubic1,depeventi,depdatai,depdataf,depubic2 FROM depositi WHERE depeventi = 1 ORDER BY depcod", null);
        while (cursor.moveToNext()) {
            veventiCod.add(cursor.getString(0));
            veventi.add(cursor.getString(1));
        }
        cursor.close();
        SpinnerAdapter ltdadapter = new SpinnerAdapter(this.getActivity().getApplicationContext(), R.layout.spinner_item, veventi);
        spevento.setAdapter(ltdadapter);
        spevento.setSelection(0);

        bvend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (spevento.getSelectedItemPosition() == 0) {
                    Toast toast = Toast.makeText(getContext(), "Seleziona un evento",
                            Toast.LENGTH_SHORT);
                    toast.show();
                    return;
/*                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentStatEventiFiltri.this.getActivity());
                    builder.setMessage("Indicare un evento")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog,
                                                            int id)
                                        {
                                        }
                                    }).setTitle("Errore");
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();*/
                } else {
                    sceltaVendite();
                }
            }
        });

        bcontatti.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (spevento.getSelectedItemPosition() == 0) {
                    Toast toast = Toast.makeText(getContext(), "Seleziona un evento",
                            Toast.LENGTH_SHORT);
                    toast.show();
                    return;
/*                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentStatEventiFiltri.this.getActivity());
                    builder.setMessage("Indicare un evento")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog,
                                                            int id)
                                        {
                                        }
                                    }).setTitle("Errore");
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();*/
                } else {
                    sceltaContatti();
                }
            }
        });

        bmerceinviata.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (spevento.getSelectedItemPosition() == 0) {
                    Toast toast = Toast.makeText(getContext(), "Seleziona un evento",
                            Toast.LENGTH_SHORT);
                    toast.show();
                    return;
/*                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentStatEventiFiltri.this.getActivity());
                    builder.setMessage("Indicare un evento")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog,
                                                            int id)
                                        {
                                        }
                                    }).setTitle("Errore");
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();*/
                } else {
                    sceltaMerceInviata();
                }
            }
        });

        bmercetornata.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (spevento.getSelectedItemPosition() == 0) {
                    Toast toast = Toast.makeText(getContext(), "Seleziona un evento",
                            Toast.LENGTH_SHORT);
                    toast.show();
                    return;
/*                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FragmentStatEventiFiltri.this.getActivity());
                    builder.setMessage("Indicare un evento")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog,
                                                            int id)
                                        {
                                        }
                                    }).setTitle("Errore");
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();*/
                } else {
                    sceltaMerceTornata();
                }
            }
        });

        spevento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                bvend.performClick();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

    }
}
