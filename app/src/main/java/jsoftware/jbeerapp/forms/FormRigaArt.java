package jsoftware.jbeerapp.forms;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.adapters.SpinnerAdapter;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.FunzioniJBeerApp;
import jsoftware.jbeerapp.env.OpValute;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.env.funzStringa;

public class FormRigaArt extends AppCompatActivity {
    Spinner spcausale;
    EditText edcodart;
    EditText edlottoman;
    TextView tvdescrart;
    TextView tvinfo;
    TextView tvgiaclotto;
    TextView tvlotto;
    EditText edqta;
    EditText edcolli;
    Spinner splotto;
    EditText edprezzo;
    TextView tvprezzo;
    TextView tvimp;
    EditText edsc1;
    EditText edsc2;
    EditText edsc3;
    Button bok;
    Button bfine;
    TextView tvlabelprzrif;
    TextView tvprzrif;
    LinearLayout llsconti;
    ImageButton bvisprzrif;
    LinearLayout lprzacq;
    TextView tvprzacq;
    LinearLayout lgiacsede;
    TextView tvgiacsede;

    public double npezzi = 1;

    private int tipodoc = 0;
    private String codart = "";
    private String urlart = "";
    private String clicod = "";
    private String clicodpadre = "";
    private String clicodiva = "";
    public boolean altro = false;
    private double clisc1 = 0, clisc2 = 0, clisc3 = 0;
    private boolean climodprz = true;
    private boolean clistampaprz = true;
    private boolean clicausm = true;
    private boolean clicauom = true;
    private boolean clicaureso = true;
    private boolean clicausost = true;
    private double climaxscvend = 0;
    private boolean cliddtreso = true;
    private String cliordlotto = "A";
    private String clicodpag = "";
    private boolean clinuovo = false;
    private boolean clivalidato = true;
    private ArrayList<String> vlotti = new ArrayList();
    private ArrayList<String> vlottigma = new ArrayList();
    HashMap<String, Double> vlottigiac = new HashMap();
    private SpinnerAdapter llottoadapter;
    private double przrif = 0;
    private double przcat = 0; //prezzo da catalogo
    private double przlist = 0;
    private double sc1list = 0;
    private double sc2list = 0;
    private double sc3list = 0;
    private TextView llgiacsede;
    public int caucorr = -1;
    private String artcod;
    private boolean nuovoArticolo = false;
    private double vargiac;
    private double giactmp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_riga_art);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        tvinfo = findViewById(R.id.rigaart_label_info);
        spcausale = findViewById(R.id.rigaart_causale);
        edcodart = findViewById(R.id.rigaart_artcod);
        if (Env.codartnum)
            edcodart.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
        tvdescrart = findViewById(R.id.rigaart_label_descrart);
        tvgiaclotto = findViewById(R.id.rigaart_label_giaclotto);
        edqta = findViewById(R.id.rigaart_qta);
        edcolli = findViewById(R.id.rigaart_colli);
        splotto = findViewById(R.id.rigaart_lotto);
        edlottoman = findViewById(R.id.rigaart_lottoman);
        edprezzo = findViewById(R.id.rigaart_prezzo);
        tvprezzo = findViewById(R.id.rigaart_label_prezzo);
        tvimp = findViewById(R.id.rigaart_nettoriga);
        edsc1 = findViewById(R.id.rigaart_sc1);
        edsc2 = findViewById(R.id.rigaart_sc2);
        edsc3 = findViewById(R.id.rigaart_sc3);
        bok = findViewById(R.id.rigaart_buttonOk);
        bfine = findViewById(R.id.rigaart_buttonFine);
        tvlotto = findViewById(R.id.rigaart_label_lotto);
        tvlabelprzrif = findViewById(R.id.rigaart_label_przrif);
        tvprzrif = findViewById(R.id.rigaart_przr);
        llsconti = findViewById(R.id.rigaart_psconti);
        bvisprzrif = findViewById(R.id.rigaart_buttonVisPrzRif);
        lprzacq = findViewById(R.id.rigaart_lprzacq);
        tvprzacq = findViewById(R.id.rigaart_przacq);
        lgiacsede = findViewById(R.id.rigaart_lgiacsede);
        tvgiacsede = findViewById(R.id.rigaart_giacsede);
        llgiacsede =  findViewById(R.id.rigaart_label_giacsede);

        if (Env.bloccosconti) {
            llsconti.setVisibility(View.GONE);
        }

        if(Env.dispart){
            llgiacsede.setText("disponibilita' in sede:");
        }

        // caricamento causali
        ArrayList<String> vc = new ArrayList();
        vc.add("*");//0
        vc.add("*");//1
        vc.add("*");//2
        vc.add("*");//3
        vc.add("SCONTO IN MERCE");//4 -> 0
        vc.add("OMAGGIO IMPONIBILE");//5 -> 1
        vc.add("OMAGGIO TOTALE");//6 -> 2
        vc.add("ORDINE CLIENTE");//7 -> 3
        SpinnerAdapter lcadapter = new SpinnerAdapter(FormRigaArt.this.getApplicationContext(), R.layout.spinner_item, vc);
        spcausale.setAdapter(lcadapter);


        edprezzo.setEnabled(true);
        edsc1.setEnabled(true);
        edsc2.setEnabled(true);
        edsc3.setEnabled(true);

        tvlabelprzrif.setVisibility(Env.visprzrif ? View.VISIBLE : View.GONE);
        bvisprzrif.setVisibility(Env.visprzrif ? View.VISIBLE : View.GONE);
        tvprzrif.setVisibility(View.GONE);

        // combo lotti
        llottoadapter = new SpinnerAdapter(FormRigaArt.this.getApplicationContext(), R.layout.spinner_item, vlotti);
        splotto.setAdapter(llottoadapter);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("artcod"))
            artcod = getIntent().getExtras().getString("artcod");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("tipodoc"))
            tipodoc = getIntent().getExtras().getInt("tipodoc");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clicod"))
            clicod = getIntent().getExtras().getString("clicod");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clisc1"))
            clisc1 = getIntent().getExtras().getDouble("clisc1");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clisc2"))
            clisc2 = getIntent().getExtras().getDouble("clisc2");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("clisc3"))
            clisc3 = getIntent().getExtras().getDouble("clisc3");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("giac"))
            giactmp = getIntent().getExtras().getDouble("giac");
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("prezzo"))
            przcat = getIntent().getExtras().getDouble("prezzo");
        // impostazioni cliente
        String[] pars = new String[1];
        pars[0] = clicod;
        Cursor c = Env.db.rawQuery("SELECT climodprz,clicausm,clicauom,clicaureso,clicausost,climaxscvend,clicodiva,cliddtreso,clicodpadre,clistampaprz,clicodpag,clinuovo,clivalidato FROM clienti WHERE clicodice = ?", pars);
        if (c.moveToFirst()) {
            climodprz = (c.getString(0).equals("S"));
            clicausm = (c.getString(1).equals("S"));
            clicauom = (c.getString(2).equals("S"));
            clicaureso = (c.getString(3).equals("S"));
            clicausost = (c.getString(4).equals("S"));
            climaxscvend = c.getDouble(5);
            clicodiva = c.getString(6);
            cliddtreso = (c.getString(7).equals("S"));

            cliordlotto = "A";
            clicodpadre = c.getString(8);
            clistampaprz = (c.getString(9).equals("S"));
            clicodpag = c.getString(10);
            clinuovo = (c.getInt(11) == 1);
            clivalidato = (c.getString(12).equals("S"));
        }
        c.close();
        if (climodprz) {
            edprezzo.setEnabled(true);
            edsc1.setEnabled(true);
            edsc2.setEnabled(true);
            edsc3.setEnabled(true);
        } else {
            edprezzo.setEnabled(false);
            edsc1.setEnabled(false);
            edsc2.setEnabled(false);
            edsc3.setEnabled(false);
        }

        if (caucorr != -1) {
            spcausale.setSelection(caucorr);
        }

        if (Env.trasfdoc_rrigavar == null) {
            tvinfo.setText("INSERIMENTO ARTICOLO");

            if (caucorr != -1)
                spcausale.setSelection(caucorr);
            else if (tipodoc == 0 || tipodoc == 2 || tipodoc == 3)
                spcausale.setSelection(0);
            else if (tipodoc == 1)
                spcausale.setSelection(0);
            else if (tipodoc == 4)
                spcausale.setSelection(1);
            else if (tipodoc == 5)
                spcausale.setSelection(7);
            edcodart.setText(artcod);
            nuovoArticolo = true;
            npezzi = 1;
            tvdescrart.setText("");
            tvgiaclotto.setText("");
            codart = "";
            edcolli.setText("");
            edqta.setText("");
            vlotti.clear();
            vlottigma.clear();
            vlottigiac.clear();
            llottoadapter.notifyDataSetChanged();
            edlottoman.setText("");
            edprezzo.setText("");
            edsc1.setText("");
            edsc2.setText("");
            edsc3.setText("");
            tvprzrif.setText("");
            lprzacq.setVisibility(View.GONE);
            LinearLayout.LayoutParams lParams = (LinearLayout.LayoutParams) lprzacq.getLayoutParams();
            lParams.width = 0;
            lParams.height = 0;
            lgiacsede.setVisibility(View.GONE);
            lParams = (LinearLayout.LayoutParams) lgiacsede.getLayoutParams();
            lParams.width = 0;
            lParams.height = 0;
            //edcodart.requestFocus();
            ricercaArticolo();
            edcolli.requestFocus();
        } else {

            nuovoArticolo = false;
            tvinfo.setText("VARIAZIONE ARTICOLO");
            bok.setText("MODIFICA");
            //bokealtro.setEnabled(false);
            vlotti.clear();
            vlottigma.clear();
            vlottigiac.clear();
            llottoadapter.notifyDataSetChanged();
            String cau = Env.trasfdoc_rrigavar.leggiStringa("rmcaumag");
            if (cau.equals("VE"))
                spcausale.setSelection(0);
            else if (cau.equals("RV"))
                spcausale.setSelection(1);
            else if (cau.equals("RN"))
                spcausale.setSelection(2);
            else if (cau.equals("RC"))
                spcausale.setSelection(3);
            else if (cau.equals("SM"))
                spcausale.setSelection(4);
            else if (cau.equals("OM"))
                spcausale.setSelection(5);
            else if (cau.equals("OT"))
                spcausale.setSelection(6);
            else if (cau.equals("OC"))
                spcausale.setSelection(7);
            edcodart.setText(artcod);
            this.codart = Env.trasfdoc_rrigavar.leggiStringa("rmartcod");
            this.visualizzaDatiArticolo();
            String lot = Env.trasfdoc_rrigavar.leggiStringa("rmlotto");
            boolean trovato = false;
            for (int i = 0; i < vlotti.size(); i++) {
                if (vlotti.get(i).equals(lot)) {
                    splotto.setSelection(i);
                    edlottoman.setVisibility(View.GONE);
                    trovato = true;
                    break;
                } else {
                    splotto.setSelection(vlotti.size() - 1);
                }
            }
            if (!trovato) {
                edlottoman.setText(lot);
            }
/*            if (spcausale.getSelectedItemPosition() == 1 || spcausale.getSelectedItemPosition() == 2 || spcausale.getSelectedItemPosition() == 7) {
                splotto.setSelection(vlotti.size() - 1);
                edlottoman.setText(lot);
            }*/
            edcolli.setText("" + Env.trasfdoc_rrigavar.leggiIntero("rmcolli"));
            edqta.setText(Formattazione.formatta(Env.trasfdoc_rrigavar.leggiDouble("rmqta"), "######0.00", Formattazione.NO_SEGNO));
            if (Env.trasfdoc_rrigavar.leggiDouble("rmprz") != 0)
                edprezzo.setText(Formattazione.formatta(Math.abs(Env.trasfdoc_rrigavar.leggiDouble("rmprz")), "######0.00", Formattazione.SEGNO_DX));
            else
                edprezzo.setText("");
            double sc1 = Env.trasfdoc_rrigavar.leggiDouble("rmartsc1");
            double sc2 = Env.trasfdoc_rrigavar.leggiDouble("rmartsc2");
            double sc3 = Env.trasfdoc_rrigavar.leggiDouble("rmscvend");
            if (sc1 != 0)
                edsc1.setText(Formattazione.formatta(sc1, "##0.00", Formattazione.NO_SEGNO));
            if (sc2 != 0)
                edsc2.setText(Formattazione.formatta(sc2, "##0.00", Formattazione.NO_SEGNO));
            if (sc3 != 0)
                edsc3.setText(Formattazione.formatta(sc3, "##0.00", Formattazione.NO_SEGNO));
            if (Env.visprzrif) {
                double pb = FunzioniJBeerApp.leggiPrezzoBaseArticolo(codart, lot, true);
                if (pb > 0) {
                    tvprzrif.setText(Formattazione.formatta(pb, "#####0.00", Formattazione.NO_SEGNO));
                } else {
                    tvprzrif.setText("");
                }
            } else {
                tvprzrif.setText("");
            }
            edcolli.requestFocus();
        }
        ricalcolaTotRiga();

        /*bwebart.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i = new Intent(FormRigaArt.this.getApplicationContext(), FormWeb.class);
                if (!urlart.equals(""))
                {
                    Bundle mBundle = new Bundle();
                    mBundle.putString("URL", urlart);
                    mBundle.putString("TITOLO", "Scheda art." + codart);
                    i.putExtras(mBundle);
                }
                FormRigaArt.this.startActivity(i);
            }
        });*/

        bfine.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uri ris = Uri.parse("content://rigaart/ANNULLA");
                Intent result = new Intent(Intent.ACTION_PICK, ris);
                setResult(RESULT_CANCELED, result);
                finish();
            }
        });

        bok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (nuovoArticolo) {
                    if (controlliRiga()) {
                        ArrayList<HashMap> vris = new ArrayList();
                        HashMap h = new HashMap();
                        h.put("artcod", artcod);
                        //h.put("artdescr", r.leggiStringa("artdescr"));
                        //h.put("codiva", r.leggiStringa("codiva"));
                        if (!edlottoman.getText().toString().trim().equals("")) {
                            h.put("lotto", edlottoman.getText().toString().trim());
                        } else if (Env.gestlotti) {
                            h.put("lotto", vlotti.get(splotto.getSelectedItemPosition()));
                        } else {
                            h.put("lotto", "");
                        }
                        double q = Formattazione.estraiDouble(edqta.getText().toString().replace(".", ","));
                        h.put("quantita", q);
                        if (spcausale.getSelectedItemPosition() == 0 || spcausale.getSelectedItemPosition() == 7) {
                            h.put("qtavend", q);
                            h.put("qtasm", 0.00);
                            h.put("qtaomimp", 0.00);
                            h.put("qtaomtot", 0.00);
                        }
                        if (spcausale.getSelectedItemPosition() == 4) {
                            h.put("qtasm", q);
                            h.put("qtavend", 0.00);
                            h.put("qtaomimp", 0.00);
                            h.put("qtaomtot", 0.00);
                        }
                        if (spcausale.getSelectedItemPosition() == 5) {
                            h.put("qtaomimp", q);
                            h.put("qtasm", 0.00);
                            h.put("qtavend", 0.00);
                            h.put("qtaomtot", 0.00);
                        }
                        if (spcausale.getSelectedItemPosition() == 6) {
                            h.put("qtaomtot", q);
                            h.put("qtasm", 0.00);
                            h.put("qtavend", 0.00);
                            h.put("qtaomimp", 0.00);
                        }
                        double prz = Formattazione.estraiDouble(edprezzo.getText().toString().replace(".", ","));
                        h.put("prezzo", prz);
                        final int colli = Formattazione.estraiIntero(edcolli.getText().toString());
                        h.put("colli", colli);
                        final double sc1 = Formattazione.estraiDouble(edsc1.getText().toString().replace(".", ","));
                        final double sc2 = Formattazione.estraiDouble(edsc2.getText().toString().replace(".", ","));
                        final double sc3 = Formattazione.estraiDouble(edsc3.getText().toString().replace(".", ","));
                        h.put("sc1", sc1);
                        h.put("sc2", sc2);
                        h.put("sc3", sc3);
                        Env.trasfdoc_caucorr = spcausale.getSelectedItemPosition();
                        if (Env.trasfdoc_caucorr == 0)
                            h.put("rmcaumag", "VE");
                        else if (Env.trasfdoc_caucorr == 1)
                            h.put("rmcaumag", "RV");
                        else if (Env.trasfdoc_caucorr == 2)
                            h.put("rmcaumag", "RN");
                        else if (Env.trasfdoc_caucorr == 3)
                            h.put("rmcaumag", "RC");
                        else if (Env.trasfdoc_caucorr == 4) {
                            h.put("rmcaumag", "SM");
                        } else if (Env.trasfdoc_caucorr == 5) {
                            h.put("rmcaumag", "OM");
                        } else if (Env.trasfdoc_caucorr == 6) {
                            h.put("rmcaumag", "OT");
                        } else if (Env.trasfdoc_caucorr == 7)
                            h.put("rmcaumag", "OC");
                        else
                            h.put("rmcaumag", "VE");
                        vris.add(h);
                        Uri codselez = Uri.parse("content://listavend/OK");
                        Intent result = new Intent(Intent.ACTION_PICK, codselez);
                        result.putExtra("vris", vris);
                        setResult(RESULT_OK, result);
                        finish();

                    }
                } else {
                    if (controlliRiga()) {
                        double q = Formattazione.estraiDouble(edqta.getText().toString().replace(".", ","));
                        Record rriga = creaRecordRiga();
                        Env.trasfdoc_rriga = rriga;
                        Env.trasfdoc_caucorr = spcausale.getSelectedItemPosition();
                        Uri ris = Uri.parse("content://rigaart/OK1");
                        Intent result = new Intent(Intent.ACTION_PICK, ris);
                        setResult(RESULT_OK, result);
                        finish();
                    }
                }
            }
        });

        /*bokealtro.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                if (controlliRiga())
                {
                    double q = Formattazione.estraiDouble(edqta.getText().toString().replace(".", ","));
                    Record rriga = creaRecordRiga();
                    Env.trasfdoc_rriga = rriga;
                    Env.trasfdoc_caucorr = spcausale.getSelectedItemPosition();
                    Uri ris = Uri.parse("content://rigaart/OK2");
                    Intent result = new Intent(Intent.ACTION_PICK, ris);
                    setResult(RESULT_OK, result);
                    finish();
                }
            }
        });*/

        spcausale.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (((String) spcausale.getSelectedItem()).startsWith("*")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRigaArt.this);
                    builder.setMessage("Causale non gestita")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            if (caucorr != -1)
                                                spcausale.setSelection(caucorr);
                                            else
                                                spcausale.setSelection(0);
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (spcausale.getSelectedItemPosition() == 1 && Env.cmresov.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRigaArt.this);
                    builder.setMessage("Causale non configurata")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            if (caucorr != -1)
                                                spcausale.setSelection(caucorr);
                                            else
                                                spcausale.setSelection(0);
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (spcausale.getSelectedItemPosition() == 2 && Env.cmresonv.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRigaArt.this);
                    builder.setMessage("Causale non configurata")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            if (caucorr != -1)
                                                spcausale.setSelection(caucorr);
                                            else
                                                spcausale.setSelection(0);
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (spcausale.getSelectedItemPosition() == 4 && Env.cmsm.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRigaArt.this);
                    builder.setMessage("Causale non configurata")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            if (caucorr != -1)
                                                spcausale.setSelection(caucorr);
                                            else
                                                spcausale.setSelection(0);
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (spcausale.getSelectedItemPosition() == 5 && Env.cmomaggi.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRigaArt.this);
                    builder.setMessage("Causale non configurata")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            if (caucorr != -1)
                                                spcausale.setSelection(caucorr);
                                            else
                                                spcausale.setSelection(0);
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (spcausale.getSelectedItemPosition() == 6 && Env.cmomaggi.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRigaArt.this);
                    builder.setMessage("Causale non configurata")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            if (caucorr != -1)
                                                spcausale.setSelection(caucorr);
                                            else
                                                spcausale.setSelection(0);
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (spcausale.getSelectedItemPosition() == 7 && Env.cmordinesede.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRigaArt.this);
                    builder.setMessage("Causale non configurata")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            if (caucorr != -1)
                                                spcausale.setSelection(caucorr);
                                            else
                                                spcausale.setSelection(0);
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (tipodoc == 4 && (spcausale.getSelectedItemPosition() != 1 && spcausale.getSelectedItemPosition() != 2)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRigaArt.this);
                    builder.setMessage("Causale non ammessa per il DDT di reso")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            spcausale.setSelection(1);
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (tipodoc == 5 && (spcausale.getSelectedItemPosition() != 7)) {
                    spcausale.setSelection(7);
                } else if (tipodoc == 0 && spcausale.getSelectedItemPosition() == 7) {
                    spcausale.setSelection(0);
                } else if ((spcausale.getSelectedItemPosition() == 1 || spcausale.getSelectedItemPosition() == 2) && !clicaureso) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRigaArt.this);
                    builder.setMessage("Reso bloccato per questo cliente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            spcausale.setSelection(0);
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                } else if (spcausale.getSelectedItemPosition() == 3 && !clicausost) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRigaArt.this);
                    builder.setMessage("Sostituzione bloccata per questo cliente")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            spcausale.setSelection(0);
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return;
                }
                if (!clinuovo && clivalidato) {
                    if (spcausale.getSelectedItemPosition() == 4) {
                        if (!clicausm) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                    FormRigaArt.this);
                            builder.setMessage("Sconto in merce bloccato per questo cliente")
                                    .setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int id) {
                                                    spcausale.setSelection(0);
                                                }
                                            });
                            AlertDialog ad = builder.create();
                            ad.setCancelable(false);
                            ad.show();
                            return;
                        }
                    } else if (spcausale.getSelectedItemPosition() == 6 && !clicauom) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormRigaArt.this);
                        builder.setMessage("Omaggio bloccato per questo cliente")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                spcausale.setSelection(0);
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return;
                    } else if ((tipodoc == 0 || tipodoc == 1 || tipodoc == 2 || tipodoc == 3) && (spcausale.getSelectedItemPosition() == 1 || spcausale.getSelectedItemPosition() == 2) && cliddtreso) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormRigaArt.this);
                        builder.setMessage("Emettere DDT di reso a parte per questo cliente")
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                spcausale.setSelection(0);
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return;
                    }
                }
                if (spcausale.getSelectedItemPosition() == 2) {
                    splotto.setSelection(vlotti.size() - 1);
                    splotto.setVisibility(View.GONE);
                    tvlotto.setVisibility(View.GONE);
                    edlottoman.setVisibility(View.GONE);
                    if (tipodoc == 5) {
                        splotto.setVisibility(View.GONE);
                        tvlotto.setVisibility(View.GONE);
                    } else {
                        splotto.setVisibility(View.VISIBLE);
                        tvlotto.setVisibility(View.VISIBLE);
                    }
/*                    if (vlotti.size() == 1) {
                        edlottoman.setVisibility(View.VISIBLE);
                        tvlottoman.setVisibility(View.VISIBLE);
                    }*/
                } else if (spcausale.getSelectedItemPosition() == 0 || spcausale.getSelectedItemPosition() == 4 || spcausale.getSelectedItemPosition() == 5 || spcausale.getSelectedItemPosition() == 6) {
                    edlottoman.setVisibility(View.GONE);
                    splotto.setVisibility(View.VISIBLE);
                    tvlotto.setVisibility(View.VISIBLE);
                    if (vlotti.size() == 1) {
                        edlottoman.setVisibility(View.VISIBLE);
                        splotto.setVisibility(View.GONE);
                        tvlotto.setVisibility(View.GONE);
                    }
                } else if (spcausale.getSelectedItemPosition() == 1) {
                    splotto.setVisibility(View.GONE);
                    tvlotto.setVisibility(View.GONE);
                    edlottoman.setVisibility(View.VISIBLE);
                } else if (spcausale.getSelectedItemPosition() == 7) //  ordine cliente
                {
                    if (Env.gestlotti) {
                        splotto.setVisibility(View.VISIBLE);
                        tvlotto.setVisibility(View.VISIBLE);
                        edlottoman.setVisibility(View.VISIBLE);
                    } else {
                        splotto.setVisibility(View.GONE);
                        tvlotto.setVisibility(View.GONE);
                        edlottoman.setVisibility(View.GONE);
                    }
                } else {
                    splotto.setVisibility(View.VISIBLE);
                    tvlotto.setVisibility(View.VISIBLE);
                    edlottoman.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        splotto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (tipodoc == 5) {
                    // per ordine cliente visualizza sempre prezzo rif.azienda
                    if (Env.visprzrif) {
                        double pb = FunzioniJBeerApp.leggiPrezzoListinoBaseArticolo(codart);
                        if (pb > 0) {
                            tvprzrif.setText(Formattazione.formatta(pb, "#####0.00", Formattazione.NO_SEGNO));
                            przrif = pb;
                        } else {
                            tvprzrif.setText("");
                            przrif = 0;
                        }
                    } else {
                        tvprzrif.setText("");
                        przrif = 0;
                    }
                    if (Env.gestlotti) {
                        if (vlottigiac != null && vlottigiac.size() > 0 && !splotto.getSelectedItem().equals("...")) {
                            tvgiaclotto.setText( (Env.dispart ? "Disp.Lotto: " : "Giac.Lotto: ") + Formattazione.formatta(vlottigiac.get((String) splotto.getSelectedItem()), "#######0", Formattazione.SEGNO_SX));
                        } else {
                            tvgiaclotto.setText("");
                        }
                    }

                } else if (vlottigiac != null && vlottigiac.size() > 0 && !splotto.getSelectedItem().equals("...") && splotto.getVisibility() == View.VISIBLE) {

                    tvgiaclotto.setText((Env.dispart && Env.gestlotti ? "Disp.Lotto: " : "Giac.Lotto: ") + Formattazione.formatta(vlottigiac.get((String) splotto.getSelectedItem()), "#######0", Formattazione.SEGNO_SX));

                    przrif = 0;
                    if (Env.visprzrif) {
                        double pb = FunzioniJBeerApp.leggiPrezzoBaseArticolo(codart, (String) splotto.getSelectedItem(), true);
                        if (pb > 0) {
                            tvprzrif.setText(Formattazione.formatta(pb, "#####0.00", Formattazione.NO_SEGNO));
                            przrif = pb;
                        } else {
                            tvprzrif.setText("");
                            przrif = 0;
                        }
                    } else {
                        tvprzrif.setText("");
                        przrif = 0;
                    }
                } else {
                    tvgiaclotto.setText("");
                    tvprzrif.setText("");
                    przrif = 0;
                }
                if (!splotto.getSelectedItem().equals("...")) {
                    edlottoman.setVisibility(View.GONE);
                    edlottoman.setText("");
                } else {
                    edlottoman.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        bvisprzrif.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (bvisprzrif.getTag() == null || (bvisprzrif.getTag() != null && bvisprzrif.getTag().equals("NOVIS"))) {
                    tvprzrif.setVisibility(View.VISIBLE);
                    bvisprzrif.setTag("VIS");
                } else {
                    tvprzrif.setVisibility(View.GONE);
                    bvisprzrif.setTag("NOVIS");
                }
            }
        });

        edcodart.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //here is your code
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                ricercaArticolo();
            }
        });

        edcolli.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    double col = Formattazione.estraiDouble(edcolli.getText()
                            .toString());
                    if (npezzi > 0 && col > 0) {
                        double q = OpValute.arrotondaMat(col * npezzi, 2);
                        edqta.setText(Formattazione.formatta(q, "#####0.00", 0)
                                .replace(",", "."));
                    }
                    ricalcolaTotRiga();
                }
            }
        });

        edqta.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    ricalcolaTotRiga();
                }
            }
        });

        edsc1.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    ricalcolaTotRiga();
                }
            }
        });

        edsc2.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    ricalcolaTotRiga();
                }
            }
        });

        edsc3.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    ricalcolaTotRiga();
                }
            }
        });

        edprezzo.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    ricalcolaTotRiga();
                }
            }
        });
    }

    private void ricercaArticolo() {
        String tmp = artcod;
        if (!tmp.equals("")) {
            String[] pars = new String[2];
            pars[0] = tmp;
            pars[1] = tmp;
            Cursor c = Env.db.rawQuery("SELECT artdescr,artcodbarre,artcod FROM articoli WHERE (artcod = ? OR artcodbarre = ?) AND artstato = 'A'", pars);
            if (c.moveToFirst()) {
                if (c.getString(1).equals(tmp)) {
                    edcodart.setText(c.getString(2));
                    this.codart = c.getString(2);
                } else
                    this.codart = tmp;
                visualizzaDatiArticolo();
            } else {
                this.codart = "";
                svuotaDatiArticolo();
            }
            c.close();
        } else {
            this.codart = "";
            svuotaDatiArticolo();
        }
    }

    public boolean caricaLottiArticolo() {
        boolean ris = true;
        try {
            if (!codart.equals("")) {
                // caricamento lotti articolo selezionato
                vlotti.clear();
                vlottigma.clear();
                vlottigiac.clear();
                String[] pars = new String[1];
                pars[0] = codart;
/*                Cursor c = Env.db.rawQuery("SELECT lotcod, lotgiacenza FROM lotti WHERE lotartcod = ? AND " +
                        "lotgiacenza > 0 ORDER BY lotcod " + (cliordlotto.equals("D") ? "DESC" : ""), pars);*/
                Cursor c = Env.db.rawQuery("SELECT giaclartcod, giacllotto,giaclqta FROM giacartlotti WHERE giaclartcod = ? AND " +
                        "giaclqta > 0 ORDER BY giaclartcod " + (cliordlotto.equals("D") ? "DESC" : ""), pars);
                while (c.moveToNext()) {
                    vlottigma.add(c.getString(0));
                    vlotti.add(c.getString(1));
                    vargiac = c.getDouble(2);
/*                    if (!Env.gestlotti) {
                        if (nuovoArticolo) {
                            vargiac = giactmp;
                        }
                    }*/
                    String lottotmp = c.getString(1);
                    //controlla gli articoli presenti nel carrello, inseriti precedentemente
                    for (int j = 0; j < Env.righecrptmp.size(); j++) {
                        Record rr = Env.righecrptmp.get(j);
                        String acod = rr.leggiStringa("rmartcod");
                        String lcod = rr.leggiStringa("rmlotto");
                        String cau = rr.leggiStringa("rmcaumag");
                        double qta = rr.leggiDouble("rmqta");
                        if (acod.equals(artcod) && lcod.equals(lottotmp) && !cau.equals("RN")) {
                            if (cau.equals("RV"))
                                vargiac += qta;
                            else
                                vargiac -= qta;
                        }
                    }
                    //controlla i movimenti - documenti effettuati ma non ancora inviati
                    String qrygiac = "SELECT movtipo,movdoc,movsez,movdata,movnum" +
                            " FROM movimenti WHERE movcalcgiac = 'S' ORDER BY movtipo,movdata,movnum";
                    Cursor cursorgiac = Env.db.rawQuery(qrygiac, null);
                    while (cursorgiac.moveToNext()) {
                        String[] parsd = new String[6];
                        parsd[0] = "" + cursorgiac.getInt(0);
                        parsd[1] = cursorgiac.getString(1);
                        parsd[2] = cursorgiac.getString(2);
                        parsd[3] = cursorgiac.getString(3);
                        parsd[4] = "" + cursorgiac.getInt(4);
                        parsd[5] = artcod;
                        int nriga = 1;
                        Cursor cd = Env.db.rawQuery(
                                "SELECT rmqta,rmlotto,rmcaumag " +
                                        "FROM righemov WHERE rmmovtipo=? AND rmmovdoc=? AND rmmovsez=? AND rmmovdata=? AND rmmovnum=? AND rmartcod=? ORDER BY rmriga",
                                parsd);
                        while (cd.moveToNext()) {
                            if (cd.getString(1).equals(lottotmp) && !cd.getString(2).equals("RN")) {
                                if (cd.getString(2).equals("RV"))
                                    vargiac += cd.getDouble(0);
                                else
                                    vargiac -= cd.getDouble(0);
                            }
                        }
                        cd.close();
                    }
                    cursorgiac.close();

                    //se attivo disponibilità e gestione lotti si toglie dalla giacenza del lotto la disponibilita'
                    if (Env.gestlotti && Env.dispart) {
                        String[] parsd = new String[2];
                        parsd[0] = codart;;
                        parsd[1] = c.getString(1);
                        Cursor clotto = Env.db.rawQuery(
                                "SELECT dlqta " +
                                        "FROM dispartlotti WHERE dlartcod=? AND dllotto=? ",
                                parsd);
                        if (clotto.moveToNext()) {
                            double a = clotto.getDouble(0);
                            vargiac -= clotto.getDouble(0);
                        }
                        clotto.close();
                    }
                    vlottigiac.put(c.getString(1), vargiac);


                }
                c.close();
                vlotti.add("...");
                vlottigma.add("...");
                //vlottigiac.put(0.0);
                llottoadapter.notifyDataSetChanged();
                splotto.setSelection(0);
                if (vlotti.size() == 1) {
                    edlottoman.setVisibility(View.VISIBLE);
                    splotto.setVisibility(View.GONE);
                }
            } else {
                vlotti.clear();
                vlottigma.clear();
                vlottigiac.clear();
                vlotti.add("...");
                vlottigma.add("...");
                //vlottigiac.add(0.0);
                llottoadapter.notifyDataSetChanged();
                splotto.setSelection(0);
            }
        } catch (Exception e) {
            ris = false;
        }
        return ris;
    }

    public void svuotaDatiArticolo() {
        tvdescrart.setText("");
        tvgiaclotto.setText("");
        edqta.setText("");
        edlottoman.setText("");
        vlotti.clear();
        vlottigma.clear();
        vlottigiac.clear();
        llottoadapter.notifyDataSetChanged();
        edprezzo.setText("");
        edsc1.setText("");
        edsc2.setText("");
        edsc3.setText("");
        urlart = "";
        //bwebart.setVisibility(View.INVISIBLE);
        lprzacq.setVisibility(View.GONE);
        LinearLayout.LayoutParams lParams = (LinearLayout.LayoutParams) lprzacq.getLayoutParams();
        lParams.width = 0;
        lParams.height = 0;
        lgiacsede.setVisibility(View.GONE);
        lParams = (LinearLayout.LayoutParams) lgiacsede.getLayoutParams();
        lParams.width = 0;
        lParams.height = 0;
    }

    public void visualizzaDatiArticolo() {
        svuotaDatiArticolo();
        if (!this.codart.equals("")) {
            // legge dati articoli
            String[] pars = new String[1];
            pars[0] = codart;
            Cursor c = Env.db.rawQuery("SELECT artcod,artdescr,artum,artcodiva,artpezzi,artscprom,artclassecod," +
                    "artsottoclassecod,artmarcacod,artscdatainizio,artscdatafine,artcpesclaltri," +
                    "artcauvend,artcausm,artcauom,artcaureso,artcausost,artgiacenza,artcodsc,artpeso,artcauzcod,artflagum,artweb_linkscheda,artprzacq,giacqta FROM articoli LEFT JOIN giacart ON articoli.artcod = giacart.giacartcod WHERE artcod = ?", pars);
            if (c.moveToFirst()) {
                // codice e descr.
                tvdescrart.setText(
                        //c.getString(0) + "-" +
                                c.getString(1) +
                        " (U.M.: " + c.getString(2) + ")  "
                        //+ (Env.dispart ? "Disp.: " : "Giac.: ") + Formattazione.formatta(giactmp, "######0", Formattazione.SEGNO_SX)
                );
                if (!FunzioniJBeerApp.umDecimale(c.getString(0)))
                    edqta.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                else
                    edqta.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                tvgiaclotto.setText("");
                npezzi = c.getDouble(4);
                urlart = c.getString(22);
                if (Env.gestlotti) {
                    // lotti
                    caricaLottiArticolo();
                }
                // *** prezzo
                double prz = 0;
                int nprz = 1;
                String codlist = "";
                String codsc = c.getString(18);
                double[] sc = new double[3];
                sc[0] = 0;
                sc[1] = 0;
                sc[2] = 0;

                if (Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
                    boolean esclaltri = false;
                    //if (prz == 0)
                    boolean condpers = false;
                    {
                        // sconto promozionale
                        if (c.getDouble(5) != 0) {
                            // controllo data
                            boolean promOk = true;
                            String dtoggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
                            if (!c.getString(9).equals("0000-00-00") && c.getString(9).compareTo(dtoggi) > 0)
                                promOk = false;
                            if (!c.getString(10).equals("0000-00-00") && c.getString(10).compareTo(dtoggi) < 0)
                                promOk = false;
                            if (promOk) {
                                if (sc[0] == 0)
                                    sc[0] = c.getDouble(5);
                                else if (sc[1] == 0)
                                    sc[1] = c.getDouble(5);
                                else if (sc[2] == 0)
                                    sc[2] = c.getDouble(5);
                                if (c.getString(11).equals("S"))
                                    esclaltri = true;
                            }
                        }

                        if (!clicod.equals("")) {
                            // dati listino cliente
                            String[] pars2 = new String[1];
                            pars2[0] = clicod;
                            Cursor c2 = Env.db.rawQuery("SELECT clicodlist,clinprz FROM clienti WHERE clicodice = ?", pars2);
                            if (c2.moveToFirst()) {
                                codlist = c2.getString(0);
                                nprz = c2.getInt(1);
                                if (nprz == 0)
                                    nprz = 1;
                            }
                            c2.close();
                        } else {
                            codlist = Env.clinuovolist;
                            nprz = Env.clinuovoposprz;
                            if (nprz == 0)
                                nprz = 1;
                        }
                        // vede se esistono condizioni cliente particolari
                        if (!esclaltri && !clicod.equals("")) {
                            String dtoggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
                            boolean condes = false;
                            String[] parscp = new String[2];
                            parscp[0] = clicod;
                            parscp[1] = codart;
                            Cursor ccp = Env.db.rawQuery("SELECT cpdatainizio,cpdatafine,cpsc1,cpsc2,cpsc3,cplistcod,cpprezzo FROM condpers WHERE cpclicod = ? AND cpartcod = ?", parscp);
                            while (ccp.moveToNext()) {
                                // controllo validita
                                boolean condOk = true;
                                if (!ccp.getString(0).equals("0000-00-00") && ccp.getString(0).compareTo(dtoggi) > 0)
                                    condOk = false;
                                if (!ccp.getString(1).equals("0000-00-00") && ccp.getString(1).compareTo(dtoggi) < 0)
                                    condOk = false;
                                if (condOk) {
                                    condes = true;
                                    condpers = true;
                                    if (ccp.getDouble(6) != 0)
                                        prz = ccp.getDouble(6);
                                    if (ccp.getDouble(2) != 0 || ccp.getDouble(3) != 0 || ccp.getDouble(4) != 0) {
                                        if (!ccp.getString(5).equals("")) {
                                            if (ccp.getString(5).equals(codlist)) {
                                                if (ccp.getDouble(2) != 0) {
                                                    if (sc[0] == 0)
                                                        sc[0] = ccp.getDouble(2);
                                                    else if (sc[1] == 0)
                                                        sc[1] = ccp.getDouble(2);
                                                    else if (sc[2] == 0)
                                                        sc[2] = ccp.getDouble(2);
                                                }
                                                if (ccp.getDouble(3) != 0) {
                                                    if (sc[0] == 0)
                                                        sc[0] = ccp.getDouble(3);
                                                    else if (sc[1] == 0)
                                                        sc[1] = ccp.getDouble(3);
                                                    else if (sc[2] == 0)
                                                        sc[2] = ccp.getDouble(3);
                                                }
                                                if (ccp.getDouble(4) != 0) {
                                                    if (sc[0] == 0)
                                                        sc[0] = ccp.getDouble(4);
                                                    else if (sc[1] == 0)
                                                        sc[1] = ccp.getDouble(4);
                                                    else if (sc[2] == 0)
                                                        sc[2] = ccp.getDouble(4);
                                                }
                                            }
                                        } else {
                                            if (ccp.getDouble(2) != 0) {
                                                if (sc[0] == 0)
                                                    sc[0] = ccp.getDouble(2);
                                                else if (sc[1] == 0)
                                                    sc[1] = ccp.getDouble(2);
                                                else if (sc[2] == 0)
                                                    sc[2] = ccp.getDouble(2);
                                            }
                                            if (ccp.getDouble(3) != 0) {
                                                if (sc[0] == 0)
                                                    sc[0] = ccp.getDouble(3);
                                                else if (sc[1] == 0)
                                                    sc[1] = ccp.getDouble(3);
                                                else if (sc[2] == 0)
                                                    sc[2] = ccp.getDouble(3);
                                            }
                                            if (ccp.getDouble(4) != 0) {
                                                if (sc[0] == 0)
                                                    sc[0] = ccp.getDouble(4);
                                                else if (sc[1] == 0)
                                                    sc[1] = ccp.getDouble(4);
                                                else if (sc[2] == 0)
                                                    sc[2] = ccp.getDouble(4);
                                            }
                                        }
                                    }
                                }
                            }
                            ccp.close();

                            if (!condes && !this.clicodpadre.equals("") && !this.clicodpadre.equals(this.clicod)) {
                                parscp = new String[2];
                                parscp[0] = clicodpadre;
                                parscp[1] = codart;
                                ccp = Env.db.rawQuery("SELECT cpdatainizio,cpdatafine,cpsc1,cpsc2,cpsc3,cplistcod,cpprezzo FROM condpers WHERE cpclicod = ? AND cpartcod = ?", parscp);
                                while (ccp.moveToNext()) {
                                    // controllo validita
                                    boolean condOk = true;
                                    if (!ccp.getString(0).equals("0000-00-00") && ccp.getString(0).compareTo(dtoggi) > 0)
                                        condOk = false;
                                    if (!ccp.getString(1).equals("0000-00-00") && ccp.getString(1).compareTo(dtoggi) < 0)
                                        condOk = false;
                                    if (condOk) {
                                        condes = true;
                                        condpers = true;
                                        if (ccp.getDouble(6) != 0)
                                            prz = ccp.getDouble(6);
                                        if (ccp.getDouble(2) != 0 || ccp.getDouble(3) != 0 || ccp.getDouble(4) != 0) {
                                            if (!ccp.getString(5).equals("")) {
                                                if (ccp.getString(5).equals(codlist)) {
                                                    if (ccp.getDouble(2) != 0) {
                                                        if (sc[0] == 0)
                                                            sc[0] = ccp.getDouble(2);
                                                        else if (sc[1] == 0)
                                                            sc[1] = ccp.getDouble(2);
                                                        else if (sc[2] == 0)
                                                            sc[2] = ccp.getDouble(2);
                                                    }
                                                    if (ccp.getDouble(3) != 0) {
                                                        if (sc[0] == 0)
                                                            sc[0] = ccp.getDouble(3);
                                                        else if (sc[1] == 0)
                                                            sc[1] = ccp.getDouble(3);
                                                        else if (sc[2] == 0)
                                                            sc[2] = ccp.getDouble(3);
                                                    }
                                                    if (ccp.getDouble(4) != 0) {
                                                        if (sc[0] == 0)
                                                            sc[0] = ccp.getDouble(4);
                                                        else if (sc[1] == 0)
                                                            sc[1] = ccp.getDouble(4);
                                                        else if (sc[2] == 0)
                                                            sc[2] = ccp.getDouble(4);
                                                    }
                                                }
                                            } else {
                                                if (ccp.getDouble(2) != 0) {
                                                    if (sc[0] == 0)
                                                        sc[0] = ccp.getDouble(2);
                                                    else if (sc[1] == 0)
                                                        sc[1] = ccp.getDouble(2);
                                                    else if (sc[2] == 0)
                                                        sc[2] = ccp.getDouble(2);
                                                }
                                                if (ccp.getDouble(3) != 0) {
                                                    if (sc[0] == 0)
                                                        sc[0] = ccp.getDouble(3);
                                                    else if (sc[1] == 0)
                                                        sc[1] = ccp.getDouble(3);
                                                    else if (sc[2] == 0)
                                                        sc[2] = ccp.getDouble(3);
                                                }
                                                if (ccp.getDouble(4) != 0) {
                                                    if (sc[0] == 0)
                                                        sc[0] = ccp.getDouble(4);
                                                    else if (sc[1] == 0)
                                                        sc[1] = ccp.getDouble(4);
                                                    else if (sc[2] == 0)
                                                        sc[2] = ccp.getDouble(4);
                                                }
                                            }
                                        }
                                    }
                                }
                                ccp.close();
                            }
                        }
                    }
/*                    if (Env.prezzopref && !condpers) {
                        String cc = clicod;
                        if (!clicodpadre.equals(""))
                            cc = clicodpadre;
                        double przpref = FunzioniJBeerApp.leggiPrezzoDaPreferiti(this.codart, cc);
                        if (przpref > 0) {
                            prz = przpref;
                            sc[0] = 0;
                            sc[1] = 0;
                            sc[2] = 0;
                            esclaltri = true;
                        }
                    }*/

                    if (prz == 0) {
                        // legge prezzo di listino
                        if (!codlist.equals("")) {
                            String dtoggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
                            String[] pars2 = new String[3];
                            pars2[0] = codlist;
                            pars2[1] = codart;
                            pars2[2] = dtoggi;
                            Cursor c2 = Env.db.rawQuery("SELECT lprezzo" + nprz + " FROM listini WHERE lcod = ? AND lartcod = ? AND ldataval <= ? ORDER BY ldataval DESC", pars2);
                            if (c2.moveToFirst()) {
                                prz = c2.getDouble(0);
                            }
                            c2.close();
                            if (prz == 0 && Env.usalistinonuovo && !Env.clinuovolist.equals("")) {
                                pars2[0] = Env.clinuovolist;
                                pars2[1] = codart;
                                pars2[2] = dtoggi;
                                c2 = Env.db.rawQuery("SELECT lprezzo" + nprz + " FROM listini WHERE lcod = ? AND lartcod = ? COLLATE NOCASE AND ldataval <= ? ORDER BY ldataval DESC", pars2);
                                if (c2.moveToFirst()) {
                                    prz = c2.getDouble(0);
                                }
                                c2.close();

                            }
                            // legge sconti listino
                            if (!esclaltri) {
                                String[] pars3 = new String[2];
                                pars3[0] = codlist;
                                pars3[1] = c.getString(18);
                                Cursor c3 = Env.db.rawQuery("SELECT slsc" + nprz + "_1,slsc" + nprz + "_2 FROM scontilist WHERE sllistcod = ? AND slcodsc = ?", pars3);
                                if (c3.moveToFirst()) {
                                    if (c3.getDouble(0) != 0) {
                                        if (sc[0] == 0)
                                            sc[0] = c3.getDouble(0);
                                        else if (sc[1] == 0)
                                            sc[1] = c3.getDouble(0);
                                        else if (sc[2] == 0)
                                            sc[2] = c3.getDouble(0);
                                    }
                                    if (c3.getDouble(1) != 0) {
                                        if (sc[0] == 0)
                                            sc[0] = c3.getDouble(1);
                                        else if (sc[1] == 0)
                                            sc[1] = c3.getDouble(1);
                                        else if (sc[2] == 0)
                                            sc[2] = c3.getDouble(1);
                                    }
                                }
                                c3.close();
                            }
                        }
                    }
                }

                // visualizza prezzo
                if (prz != 0) {
                    edprezzo.setText(funzStringa.sostituisci(Formattazione.formatta(prz, "########0.00", Formattazione.NO_SEGNO), ',', '.'));
                } else if (przcat != 0) {
                    edprezzo.setText(funzStringa.sostituisci(Formattazione.formatta(przcat, "########0.00", Formattazione.NO_SEGNO), ',', '.'));
                } else {
                    edprezzo.setText("");
                }
                // visualizza sconti
                if (sc[0] > 0)
                    edsc1.setText(funzStringa.sostituisci(Formattazione.formatta(sc[0], "##0.00", Formattazione.NO_SEGNO), ',', '.'));
                else
                    edsc1.setText("");
                if (sc[1] > 0)
                    edsc2.setText(funzStringa.sostituisci(Formattazione.formatta(sc[1], "##0.00", Formattazione.NO_SEGNO), ',', '.'));
                else
                    edsc2.setText("");
                if (sc[2] > 0)
                    edsc3.setText(funzStringa.sostituisci(Formattazione.formatta(sc[2], "##0.00", Formattazione.NO_SEGNO), ',', '.'));
                else
                    edsc3.setText("");
                przlist = prz;
                sc1list = sc[0];
                sc2list = sc[1];
                sc3list = sc[2];

                przrif = 0;
                if (Env.visprzrif) {
                    double pb = FunzioniJBeerApp.leggiPrezzoListinoBaseArticolo(codart);
                    if (pb > 0) {
                        tvprzrif.setText(Formattazione.formatta(pb, "#####0.00", Formattazione.NO_SEGNO));
                        przrif = pb;
                    } else {
                        tvprzrif.setText("");
                        pb = 0;
                    }
                } else {
                    tvprzrif.setText("");
                }
                if (Env.visprzacq) {
                    lprzacq.setVisibility(View.VISIBLE);
                    tvprzacq.setText(Formattazione.formatta(c.getDouble(23), "#####0.00", Formattazione.NO_SEGNO));
                    LinearLayout.LayoutParams lParams = (LinearLayout.LayoutParams) lprzacq.getLayoutParams();
                    lParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
                    lParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                } else {
                    LinearLayout.LayoutParams lParams = (LinearLayout.LayoutParams) lprzacq.getLayoutParams();
                    lParams.width = 0;
                    lParams.height = 0;
                }
                if (Env.termordini || Env.tipoterm == Env.TIPOTERM_RACCOLTAORDINI) {
                    lgiacsede.setVisibility(View.VISIBLE);
                    LinearLayout.LayoutParams lParams = (LinearLayout.LayoutParams) lgiacsede.getLayoutParams();
                    lParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
                    lParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                    //tvgiacsede.setText(Formattazione.formatta(c.getDouble(24), "#####0.00", Formattazione.SEGNO_SX_NEG));
                    tvgiacsede.setText(Formattazione.formatta(giactmp, "#####0", Formattazione.SEGNO_SX_NEG));
                    if (c.getDouble(24) < 0) {
                        tvgiacsede.setTextColor(Color.RED);
                    } else {
                        tvgiacsede.setTextColor(Color.rgb(0, 100, 0));
                    }
                } else {
                    LinearLayout.LayoutParams lParams = (LinearLayout.LayoutParams) lgiacsede.getLayoutParams();
                    lParams.width = 0;
                    lParams.height = 0;
                }
            }
            c.close();
        } else {
            urlart = "";
            //bwebart.setVisibility(View.INVISIBLE);
            lprzacq.setVisibility(View.GONE);
            LinearLayout.LayoutParams lParams = (LinearLayout.LayoutParams) lprzacq.getLayoutParams();
            lParams.width = 0;
            lParams.height = 0;
            lgiacsede.setVisibility(View.GONE);
            lParams = (LinearLayout.LayoutParams) lgiacsede.getLayoutParams();
            lParams.width = 0;
            lParams.height = 0;
        }
    }

    private Record creaRecordRiga() {
        // creazione dati riga
        final double q = Formattazione.estraiDouble(edqta.getText().toString().replace(".", ","));
        final int c = Formattazione.estraiIntero(edcolli.getText().toString());
        final double sc1 = Formattazione.estraiDouble(edsc1.getText().toString().replace(".", ","));
        final double sc2 = Formattazione.estraiDouble(edsc2.getText().toString().replace(".", ","));
        final double sc3 = Formattazione.estraiDouble(edsc3.getText().toString().replace(".", ","));
        Record rriga = new Record();
        rriga.insStringa("rmtiporiga", "A");
        rriga.insStringa("rmartcod", codart);
        String[] pars = new String[1];
        pars[0] = codart;
        Cursor cursor = Env.db.rawQuery("SELECT artdescr FROM articoli WHERE artcod = ?", pars);
        cursor.moveToFirst();
        rriga.insStringa("rmartdescr", cursor.getString(0));
        cursor.close();
        String lotto = "";
        if (Env.tipogestlotti == 0) {
            if (!edlottoman.getText().toString().trim().equals("")) {
                rriga.insStringa("rmlotto", edlottoman.getText().toString().trim());
                lotto = edlottoman.getText().toString().trim();
            } else if (edlottoman.getVisibility() == View.GONE && splotto.getVisibility() == View.GONE) {
                rriga.insStringa("rmlotto", "");
            } else {
                if (!edlottoman.getText().toString().trim().equals("")) {
                    rriga.insStringa("rmlotto", edlottoman.getText().toString().trim());
                    lotto = edlottoman.getText().toString().trim();
                } else if (splotto.getSelectedItem().equals("...")) {
                    rriga.insStringa("rmlotto", "");
                } else {
                    rriga.insStringa("rmlotto", vlotti.get(splotto.getSelectedItemPosition()));
                    lotto = vlotti.get(splotto.getSelectedItemPosition());
                }
            }
        } else {
            rriga.insStringa("rmlotto", ".");
        }
        boolean omimp = false;
        boolean omtot = false;
        if (spcausale.getSelectedItemPosition() == 0)
            rriga.insStringa("rmcaumag", "VE");
        else if (spcausale.getSelectedItemPosition() == 1)
            rriga.insStringa("rmcaumag", "RV");
        else if (spcausale.getSelectedItemPosition() == 2)
            rriga.insStringa("rmcaumag", "RN");
        else if (spcausale.getSelectedItemPosition() == 3)
            rriga.insStringa("rmcaumag", "RC");
        else if (spcausale.getSelectedItemPosition() == 4) {
            rriga.insStringa("rmcaumag", "SM");
            omtot = true;
        } else if (spcausale.getSelectedItemPosition() == 5) {
            rriga.insStringa("rmcaumag", "OM");
            omimp = true;
        } else if (spcausale.getSelectedItemPosition() == 6) {
            rriga.insStringa("rmcaumag", "OT");
            omtot = true;
        } else if (spcausale.getSelectedItemPosition() == 7)
            rriga.insStringa("rmcaumag", "OC");
        else
            rriga.insStringa("rmcaumag", "VE");
        rriga.insIntero("rmcolli", c);
        rriga.insDouble("rmpezzi", (double) 0);
        double contenutoriga = FunzioniJBeerApp.calcoloContenutoArticolo(codart, q);
        rriga.insDouble("rmcontenuto", contenutoriga);
        //rriga.insDouble("rmcontenuto", (double) 0);
        rriga.insDouble("rmqta", q);
        double prz = Formattazione.estraiDouble(edprezzo.getText().toString().replace(".", ","));
        float segno = 1;
        // se doc.di vendita e causale di reso -> prezzo negativo
        if ((spcausale.getSelectedItemPosition() == 1 || spcausale.getSelectedItemPosition() == 2) &&
                (tipodoc == 0 || tipodoc == 1 || tipodoc == 2 || tipodoc == 3 || tipodoc == 4)) {
            prz = -prz;
            segno = -1;
        }
        rriga.insDouble("rmprz", prz);
        rriga.insDouble("rmartsc1", sc1);
        rriga.insDouble("rmartsc2", sc2);
        rriga.insDouble("rmscvend", sc3);
        double aliq = 0;
        String codiva = "";
        String codivaecc = "";
        double aliqecc = 0;
        String cc = clicod;
        if (!clicodpadre.equals(""))
            cc = clicodpadre;
        String[] parsei = new String[2];
        parsei[0] = cc;
        parsei[1] = codart;
        Cursor cei = Env.db.rawQuery(
                "SELECT codiva.ivacod,codiva.ivaaliq FROM cliartiva INNER JOIN codiva ON cliartiva.ivacod = codiva.ivacod WHERE clicod = ? AND artcod = ?", parsei);
        if (cei.moveToFirst()) {
            codivaecc = cei.getString(0);
            aliqecc = cei.getDouble(1);
        }
        cei.close();

        if (!clicodiva.equals("")) {
            String[] parsi = new String[1];
            parsi[0] = clicodiva;
            Cursor ci = Env.db.rawQuery("SELECT ivacod,ivaaliq FROM codiva WHERE ivacod=?", parsi);
            if (ci.moveToFirst()) {
                codiva = ci.getString(0);
                aliq = ci.getDouble(1);
            }
            ci.close();
        } else {
            String[] parsi = new String[1];
            parsi[0] = codart;
            Cursor ci = Env.db.rawQuery("SELECT ivacod,ivaaliq FROM articoli INNER JOIN codiva ON articoli.artcodiva = codiva.ivacod WHERE artcod = ?", parsi);
            if (ci.moveToFirst()) {
                codiva = ci.getString(0);
                aliq = ci.getDouble(1);
            }
            ci.close();
        }
        if (!codivaecc.equals("")) {
            codiva = codivaecc;
            aliq = aliqecc;
        }
        if (omimp) {
            if (aliq == 22)
                codiva = Env.codivaomaggiimp20;
            else if (aliq == 10)
                codiva = Env.codivaomaggiimp10;
            else if (aliq == 4)
                codiva = Env.codivaomaggiimp04;
        } else if (omtot) {
            if (aliq == 22)
                codiva = Env.codivaomaggitot20;
            else if (aliq == 10)
                codiva = Env.codivaomaggitot10;
            else if (aliq == 4)
                codiva = Env.codivaomaggitot04;
        }
        rriga.insStringa("rmcodiva", codiva);
        rriga.insDouble("rmaliq", aliq);
        rriga.insDouble("rmclisc1", clisc1);
        rriga.insDouble("rmclisc2", clisc2);
        rriga.insDouble("rmclisc3", clisc3);
        double imp = OpValute.arrotondaMat(Math.abs(q * prz), 2);
        rriga.insDouble("rmlordo", segno * imp);
        float scoart = 0;
        if (sc1 != 0) {
            double s = (imp * sc1 / 100);
            imp -= s;
            scoart += s;
        }
        if (sc2 != 0) {
            double s = (imp * sc2 / 100);
            imp -= s;
            scoart += s;
        }
        if (sc3 != 0) {
            double s = (imp * sc3 / 100);
            imp -= s;
            scoart += s;
        }
        rriga.insDouble("totscontiart", segno * scoart);
        double scocli = 0;
        if (clisc1 != 0) {
            double s = (imp * clisc1 / 100);
            imp -= s;
            scocli += s;
        }
        if (clisc2 != 0) {
            double s = (imp * clisc2 / 100);
            imp -= s;
            scocli += s;
        }
        if (clisc3 != 0) {
            double s = (imp * clisc3 / 100);
            imp -= s;
            scocli += s;
        }
        rriga.insDouble("totsconticli", segno * scocli);
        imp = segno * OpValute.arrotondaMat(imp, 2);
        rriga.insDouble("rmnetto", imp);
        rriga.insDouble("rmnettoivato", imp);

        // accisa
        double acc = 0;
        double contras = 0;
        double grado = 0;
        double plato = 0;

        //if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
        grado = FunzioniJBeerApp.leggiGradoAlcolicoArticolo(codart);
        plato = FunzioniJBeerApp.leggiGradoPlatoArticolo(codart, lotto);
        if (Env.calcaccisa && Env.tipocalcaccisa != 2) {
            acc = FunzioniJBeerApp.calcoloAccisaAlcolici(codart, q, grado, plato, "IT", FunzioniJBeerApp.LITRI_IDRATI, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
        }

        //}
        rriga.insDouble("rmaccisa", acc);

        rriga.insDouble("rmcontrassegno", contras);
        rriga.insDouble("rmgrado", grado);
        rriga.insDouble("rmplato", plato);

        // cessionario
        String cesscod = "";
        if (!clicodpadre.equals(""))
            cesscod = FunzioniJBeerApp.cercaCodiceCessionario(codart, clicod);
        rriga.insStringa("cesscod", cesscod);
        return rriga;
    }

    public void ricalcolaTotRiga() {
        double q = Formattazione.estraiDouble(edqta.getText().toString().replace(".", ","));
        double p = Formattazione.estraiDouble(edprezzo.getText().toString().replace(".", ","));
        double sc1 = Formattazione.estraiDouble(edsc1.getText().toString().replace(".", ","));
        double sc2 = Formattazione.estraiDouble(edsc2.getText().toString().replace(".", ","));
        double sc3 = Formattazione.estraiDouble(edsc3.getText().toString().replace(".", ","));
        double imp = OpValute.arrotondaMat(Math.abs(q * p), 2);
        if (sc1 != 0) {
            double s = (imp * sc1 / 100);
            imp -= s;
        }
        if (sc2 != 0) {
            double s = (imp * sc2 / 100);
            imp -= s;
        }
        if (sc3 != 0) {
            double s = (imp * sc3 / 100);
            imp -= s;
        }
        if (clisc1 != 0) {
            double s = (imp * clisc1 / 100);
            imp -= s;
        }
        if (clisc2 != 0) {
            double s = (imp * clisc2 / 100);
            imp -= s;
        }
        if (clisc3 != 0) {
            double s = (imp * clisc3 / 100);
            imp -= s;
        }
        imp = OpValute.arrotondaMat(imp, 2);
        tvimp.setText(Formattazione.formValuta(imp, 12, 2, 0));
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Uri ris = null;
        switch (requestCode) {
            case (1): {
                // ricerca articolo
                if (resultCode == Activity.RESULT_OK) {
                    ris = data.getData();
                    this.codart = ris.getLastPathSegment();
                    if (!codart.equals("")) {
                        edcodart.setText(codart);
                    }
                }
                break;
            }
        }

    }

    private boolean controlliRiga() {
        if (this.codart.equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormRigaArt.this);
            builder.setMessage("Scegliere un articolo")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {

                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }
        final double q = Formattazione.estraiDouble(edqta.getText().toString().replace(".", ","));
        if (q == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormRigaArt.this);
            builder.setMessage("Inserire quantità")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    edqta.requestFocus();
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }
        if (q < 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormRigaArt.this);
            builder.setMessage("Inserire quantità positiva")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    edqta.requestFocus();
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }
        if(Env.gestlotti) {
            if(vlotti.get(splotto.getSelectedItemPosition()).equals("...")){

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormRigaArt.this);
                builder.setMessage("Attenzione\nInserire un Lotto valido!!!")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        edqta.requestFocus();
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
                return false;
            }
            Double qgiac = vlottigiac.get((String) splotto.getSelectedItem());
            if (qgiac - q < 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormRigaArt.this);
                builder.setMessage("Attenzione\nQuantita' Lotto non disponibile!!!")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        edqta.requestFocus();
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
                return false;
            }
        }
        if (((String) spcausale.getSelectedItem()).startsWith("*")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormRigaArt.this);
            builder.setMessage("Causale non gestita")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    if (caucorr != -1)
                                        spcausale.setSelection(caucorr);
                                    else
                                        spcausale.setSelection(0);
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }
        double p = Formattazione.estraiDouble(edprezzo.getText().toString().replace(".", ","));
        if (p == 0 && Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormRigaArt.this);
            builder.setMessage("Indicare il prezzo dell'articolo")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }
        final double sc1 = Formattazione.estraiDouble(edsc1.getText().toString().replace(".", ","));
        final double sc2 = Formattazione.estraiDouble(edsc2.getText().toString().replace(".", ","));
        final double sc3 = Formattazione.estraiDouble(edsc3.getText().toString().replace(".", ","));
        if (sc3 != 0 && climaxscvend > 0 && climaxscvend < sc3 && Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormRigaArt.this);
            builder.setMessage("Massimo sconto venditore ammesso: " + Formattazione.formatta(climaxscvend, "##0.00", 0))
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    if (edsc3.getVisibility() == View.VISIBLE)
                                        edsc3.requestFocus();
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }
        if (Env.maxscvend > 0) {
            double przlistnet = przlist;
            if (sc1list > 0)
                przlistnet -= przlistnet * sc1list / 100;
            if (sc2list > 0)
                przlistnet -= przlistnet * sc2list / 100;
            if (sc3list > 0)
                przlistnet -= przlistnet * sc3list / 100;
            double prznet = p;
            if (sc1 > 0)
                prznet -= prznet * sc1 / 100;
            if (sc2 > 0)
                prznet -= prznet * sc2 / 100;
            if (sc3 > 0)
                prznet -= prznet * sc3 / 100;
            double diffprz = przlistnet - prznet;
            if (diffprz > 0) {
                double percdiff = diffprz / przlistnet * 100;
                if (percdiff > Env.maxscvend && Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            FormRigaArt.this);
                    builder.setMessage("Il prezzo assegnato eccede lo sconto massimo del " + Formattazione.formValuta(Env.maxscvend, 3, 2, 0) + "% rispetto al prezzo di listino impostato dalla sede")
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            if (edsc3.getVisibility() == View.VISIBLE)
                                                edsc3.requestFocus();
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.setCancelable(false);
                    ad.show();
                    return false;
                }
            }
        }
        if (spcausale.getSelectedItemPosition() == 3) {
            // sconto merce o sostituzione: azzera prezzo e sconti
            edprezzo.setText("");
            edsc1.setText("");
            edsc2.setText("");
            edsc3.setText("");
        }
        // controllo prezzo riferimento
        if (Env.ctrprzrif) {
            double przv = p;
            if (sc1 != 0)
                przv = przv - (przv * sc1 / 100);
            if (sc2 != 0)
                przv = przv - (przv * sc2 / 100);
            if (sc3 != 0)
                przv = przv - (przv * sc3 / 100);
            if (clisc1 != 0)
                przv = przv - (przv * clisc1 / 100);
            if (clisc2 != 0)
                przv = przv - (przv * clisc2 / 100);
            if (clisc3 != 0)
                przv = przv - (przv * clisc3 / 100);
            przv = OpValute.arrotondaMat(przv, 3);
            if (przrif > 0 && przv < przrif && Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormRigaArt.this);
                builder.setMessage("Prezzo di vendita minore del prezzo di riferimento")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
                return false;
            }
        }
        if (Env.tipogestlotti == 0 && spcausale.getSelectedItemPosition() == 1 && edlottoman.getText().toString().trim().equals("")) {
            if (!this.codart.equals("")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormRigaArt.this);
                builder.setMessage("Inserire lotto del reso")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        if (splotto.getVisibility() == View.VISIBLE) {
                                            edlottoman.requestFocus();
                                        }
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
                return false;
            }
        }
        if (Env.tipogestlotti == 0 && (spcausale.getSelectedItemPosition() != 2 && spcausale.getSelectedItemPosition() != 0 && spcausale.getSelectedItemPosition() != 7 &&
                splotto.getSelectedItem().equals("...") && edlottoman.getText().toString().trim().equals(""))
                || (spcausale.getSelectedItemPosition() == 1 && edlottoman.getText().toString().trim().equals(""))) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormRigaArt.this);
            builder.setMessage("Inserire lotto")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    if (splotto.getVisibility() == View.VISIBLE) {
                                        splotto.requestFocus();
                                    } else if (edlottoman.getVisibility() == View.VISIBLE) {
                                        edlottoman.requestFocus();
                                    }
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }

        if (Env.tipogestlotti == 0 && (spcausale.getSelectedItemPosition() == 0 || spcausale.getSelectedItemPosition() == 6) && splotto.getSelectedItem().equals("...") && edlottoman.getText().toString().trim().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    FormRigaArt.this);
            builder.setMessage("Indicare un lotto per l'articolo " + this.codart)
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    if (splotto.getVisibility() == View.VISIBLE)
                                        splotto.requestFocus();
                                    else if (edlottoman.getVisibility() == View.VISIBLE)
                                        edlottoman.requestFocus();
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
            return false;
        }

        String cli = clicod;
        String dest = "";
        if (!clicodpadre.equals("")) {
            cli = "";
            dest = clicod;
        }
        //Al momento viene commentata la gestione dei blocchi/sblocchi per gli sconti
/*        if (!clinuovo && clivalidato)
        {
            if (FunzioniJBeerApp.causaleBloccata(spcausale.getSelectedItemPosition(), Env.depcod, Env.agecod, cli, codart, dest))
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormRigaArt.this);
                builder.setMessage("Causale " + (String) spcausale.getSelectedItem() + " bloccata dalla sede")
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener()
                                {
                                    public void onClick(DialogInterface dialog,
                                                        int id)
                                    {
                                        if (splotto.getVisibility() == View.VISIBLE)
                                            splotto.requestFocus();
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
                return false;
            }
        }*/
        if (spcausale.getSelectedItemPosition() == 4 && Env.tipoterm != Env.TIPOTERM_DISTRIBUTORIAUTOMATICI) {
            boolean artok = false;
            for (int j = 0; j < Env.trasfdoc_righecrp.size(); j++) {
                Record rr = Env.trasfdoc_righecrp.get(j);
                String acod = rr.leggiStringa("rmartcod");
                String rcau = rr.leggiStringa("rmcaumag");
                if (acod.equals(codart) && rcau.equals("VE"))
                    artok = true;
            }
            if (!artok) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        FormRigaArt.this);
                builder.setMessage("Sconto in merce: deve essere inserita prima una vendita di art." + codart)
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                });
                AlertDialog ad = builder.create();
                ad.setCancelable(false);
                ad.show();
                return false;
            }
        }
        // controllo giacenza
/*
        if (tipodoc != 5)
        {
            if (Env.tipogestlotti == 0)
            {
                if (spcausale.getSelectedItemPosition() == 0 || spcausale.getSelectedItemPosition() == 4 || spcausale.getSelectedItemPosition() == 5 || spcausale.getSelectedItemPosition() == 6)
                {
                    double giacl = FunzioniJBeerApp.leggiGiacenzaLotto(vlotti.get(splotto.getSelectedItemPosition()), this.codart);
                    // considera anche eventuali righe dello stesso lotto già inserite nel documento
                    for (int j = 0; j < Env.trasfdoc_righecrp.size(); j++)
                    {
                        Record rr = Env.trasfdoc_righecrp.get(j);
                        if (Env.trasfdoc_rrigavar == null || !Env.trasfdoc_rrigavar.equals(rr))
                        {
                            String acod = rr.leggiStringa("rmartcod");
                            String lcod = rr.leggiStringa("rmlotto");
                            String rcau = rr.leggiStringa("rmcaumag");
                            double rqta = rr.leggiDouble("rmqta");
                            if (acod.equals(this.codart) && lcod.equals(vlotti.get(splotto.getSelectedItemPosition())) && !rcau.equals("RN"))
                            {
                                if (rcau.equals("RV"))
                                    giacl += rqta;
                                else
                                    giacl -= rqta;
                            }
                        }
                    }
                    double diff = OpValute.arrotondaMat(giacl - q, 2);
                    if (diff < 0 && !Env.lottineg)
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormRigaArt.this);
                        builder.setMessage("Giacenza non sufficiente per lotto " + (String) splotto.getSelectedItem() + " di articolo " + this.codart)
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(DialogInterface dialog,
                                                                int id)
                                            {
                                                edqta.requestFocus();
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return false;
                    }
                }
            } else
            {
                if (spcausale.getSelectedItemPosition() == 0 || spcausale.getSelectedItemPosition() == 4 || spcausale.getSelectedItemPosition() == 5 || spcausale.getSelectedItemPosition() == 6)
                {
                    double giac = FunzioniJBeerApp.leggiGiacenzaArticolo(this.codart);
                    // considera anche eventuali righe dello stesso articolo già inserite nel documento
                    for (int j = 0; j < Env.trasfdoc_righecrp.size(); j++)
                    {
                        Record rr = Env.trasfdoc_righecrp.get(j);
                        if (Env.trasfdoc_rrigavar == null || !Env.trasfdoc_rrigavar.equals(rr))
                        {
                            String acod = rr.leggiStringa("rmartcod");
                            String rcau = rr.leggiStringa("rmcaumag");
                            double rqta = rr.leggiDouble("rmqta");
                            if (acod.equals(this.codart) && !rcau.equals("RN"))
                            {
                                if (rcau.equals("RV"))
                                    giac += rqta;
                                else
                                    giac -= rqta;
                            }
                        }
                    }
                    double diff = OpValute.arrotondaMat(giac - q, 2);
                    if (diff < 0 && !Env.lottineg)
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                FormRigaArt.this);
                        builder.setMessage("Giacenza non sufficiente per articolo " + this.codart)
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(DialogInterface dialog,
                                                                int id)
                                            {
                                                edqta.requestFocus();
                                            }
                                        });
                        AlertDialog ad = builder.create();
                        ad.setCancelable(false);
                        ad.show();
                        return false;
                    }
                }
            }
        }
*/
        return true;
    }
}
