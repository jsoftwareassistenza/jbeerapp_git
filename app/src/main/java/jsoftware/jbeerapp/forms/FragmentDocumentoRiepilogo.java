package jsoftware.jbeerapp.forms;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import jsoftware.jbeerapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentDocumentoRiepilogo.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDocumentoRiepilogo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDocumentoRiepilogo extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FormDocumento fdoc = null;

    public TextView tvnart;
    public TextView tvtotqta;
    public TextView tvspeseinc;
    public TextView tvtotimp;
    public TextView tvtotiva;
    public TextView tvtotdoc;
    public TextView tvaddeb;
    public TextView tvaccred;
    public TextView tvnettoapag;
    public Spinner sppagamento;
    public EditText edincasso;
    public TextView tvincasso;
    public EditText edannot;
    public TextView tvtotaccisa;
    public LinearLayout lltotaccisa;

    public FragmentDocumentoRiepilogo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentDocumentoRiepilogo.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDocumentoRiepilogo newInstance(String param1, String param2) {
        FragmentDocumentoRiepilogo fragment = new FragmentDocumentoRiepilogo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        //Log.v("JAZZTV", "ONCREATE RIEP");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.v("JAZZTV", "ONCREATEVIEW RIEP");
        return inflater.inflate(R.layout.fragment_fragment_documento_riepilogo, container, false);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
/*
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        } else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        tvnart = this.getActivity().findViewById(R.id.doc_riep_nart);
        tvtotqta = this.getActivity().findViewById(R.id.doc_riep_totqta);
        tvspeseinc = this.getActivity().findViewById(R.id.doc_riep_speseinc);
        tvtotimp = this.getActivity().findViewById(R.id.doc_riep_totimp);
        tvtotiva = this.getActivity().findViewById(R.id.doc_riep_totiva);
        tvtotdoc = this.getActivity().findViewById(R.id.doc_riep_totdoc);
        tvaddeb = this.getActivity().findViewById(R.id.doc_riep_addeb);
        tvaccred = this.getActivity().findViewById(R.id.doc_riep_accred);
        tvnettoapag = this.getActivity().findViewById(R.id.doc_riep_nettoapag);
        sppagamento = this.getActivity().findViewById(R.id.doc_riep_pagamento);
        edincasso = this.getActivity().findViewById(R.id.doc_riep_incasso);
        tvincasso = this.getActivity().findViewById(R.id.doc_riep_label_incasso);
        edannot = this.getActivity().findViewById(R.id.doc_riep_annot);
        tvtotaccisa = this.getActivity().findViewById(R.id.doc_riep_totaccisa);
        lltotaccisa = this.getActivity().findViewById(R.id.doc_riep_lltotaccisa);
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        fdoc.nfrag++;
        if (fdoc.nfrag == 3)
            fdoc.inizializza();
    }
}
