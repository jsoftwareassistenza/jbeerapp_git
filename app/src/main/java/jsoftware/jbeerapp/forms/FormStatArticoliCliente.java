package jsoftware.jbeerapp.forms;

import android.database.Cursor;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;

public class FormStatArticoliCliente extends AppCompatActivity implements FragmentStatArticoli.OnFragmentInteractionListener {
    TextView tvcliente = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setTheme(android.R.style.Theme_Material_Light_Dialog);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_stat_articoli_cliente);

        tvcliente = findViewById(R.id.statartcli_cliente);

        String dti = getIntent().getExtras().getString("DATAINIZIO");
        String dtf = getIntent().getExtras().getString("DATAFINE");
        String clicod = getIntent().getExtras().getString("CLICOD");

        Cursor ccli = Env.db.rawQuery("SELECT clicodice,clinome FROM clienti WHERE clicodice = '" + clicod + "'", null);
        ccli.moveToFirst();
        tvcliente.setText(ccli.getString(0) + "-" + ccli.getString(1));
        ccli.close();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragmentStatArticoli fr = new FragmentStatArticoli();
        fr.impostaFiltri(dti, dtf, clicod);
        fragmentTransaction.add(R.id.statartcli_frag, fr);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void onFragmentInteraction(String azione) {
    }

}
