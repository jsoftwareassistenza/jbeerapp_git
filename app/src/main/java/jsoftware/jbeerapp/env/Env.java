package jsoftware.jbeerapp.env;

import android.database.sqlite.SQLiteDatabase;

import com.sewoo.jpos.printer.CPCLPrinter;
import com.sewoo.port.android.BluetoothPort;
import com.sewoo.port.android.WiFiPort;

import java.util.ArrayList;
import java.util.HashMap;

	public class Env
	{

		public static SQLiteDatabase db = null;
		public static int versionDatabase = 25;
		public static boolean agg_dati_incorso = false;

		public static String versione = "6.3";
		public static int numterm = 0;
		public static String depcod = "";
        public static String depeventocod = "";
		public static String pedcod = "";
		public static String agecod = "";
		public static String agenome = "";
		public static String targamezzo = "";
		public static String depsede = "";
		public static String docbollaval = "";
		public static String sezbollaval = "";
		public static String docbollaqta = "";
		public static String sezbollaqta = "";
		public static String docfattura = "";
		public static String sezfattura = "";
		public static String doccorrisp = "";
		public static String sezcorrisp = "";
		public static String docddtreso = "";
		public static String sezddtreso = "";
		public static String docddtcarico = "";
		public static String sezddtcarico = "";
		public static String docscaricosede = "";
		public static String sezscaricosede = "";
		public static String doctrasfamezzo = "";
		public static String seztrasfamezzo = "";
		public static String doctrasfdamezzo = "";
		public static String seztrasfdamezzo = "";
		public static String docordinesede = "";
		public static String sezordinesede = "";
		public static String docordinecli = "";
		public static String sezordinecli = "";
		public static int numbollaval = 1;
		public static int numbollaqta = 1;
		public static int numfattura = 1;
		public static int numcorrisp = 1;
		public static int numddtreso = 1;
		public static int numddtcarico = 1;
		public static int numscaricosede = 1;
		public static int numtrasfamezzo = 1;
		public static int numtrasfdamezzo = 1;
		public static int numordinesede = 1;
		public static int numordinecli = 1;
		public static String docrimfs = "";
		public static String sezrimfs = "";
		public static String cmvend = "";
		public static String cmrprom = "";
		public static String cmresov = "";
		public static String cmresonv = "";
		public static String cmann = "";
		public static String cmomaggi = "";
		public static String cmsm = "";
		public static String cmtrasfamezzo = "";
		public static String cmtrasfdamezzo = "";
		public static String cmscaricosede = "";
		public static String cmddtcarico = "";
		public static String cmdistrmerce = "";
		public static String cmordinesede = "";
		public static String cmscaduto = "";
		public static String codivaomaggiimp20 = "";
		public static String codivaomaggiimp10 = "";
		public static String codivaomaggiimp04 = "";
		public static String codivaomaggitot20 = "";
		public static String codivaomaggitot10 = "";
		public static String codivaomaggitot04 = "";
		public static String clinuovocod = "";
		public static String clinuovolist = "";
		public static int clinuovoposprz = 1;
		public static String clinuovopag = "";
		public static String cliordine = "";
		public static int tipoddtcar = 0;
		public static String autcod = "";
		public static String autdescr = "";
		public static String auttarga = "";
		public static int tipostampante = 0;
		public static String nomebtstampante = "";
		public static String ssidstampante = "";
		public static String ipstampante = "";
		public static int tiposcambiodati = 0; // 0 = sim dati, 1 = wifi
		public static String ssidscambiodati = "";
		public static String ipscambiodati = "";
		public static int tiponuovicli = 0;
		public static boolean gestlotti = false; // index 0 false, index 1 true
		public static int tipogestlotti = 0;
		public static String intazriga1 = "";
		public static String intazriga2 = "";
		public static String intazriga3 = "";
		public static String intazriga4 = "";
		public static String intazriga5 = "";
		public static String intazriga6 = "";
		public static String intazriga7 = "";
		public static boolean visprzrif = false;
		public static boolean ctrprzrif = false;
		public static boolean bloccosconti = false;
		public static boolean obbligoincpagtv = false;
		public static boolean pwdcambiovend = false;
		public static int tipoannullamentodoc = 0;
		public static int nscadblocco = 0;
		public static double impblocco = 0;
		public static int ggtollblocco = 0;
		public static int nrighelogo = 7;
		public static boolean stampasedesec = false;
		public static boolean stampaddtcar_classi = false;
		public static boolean stampaord = false;
		public static boolean stampascasede = false;
		public static boolean stampatrasfvs = false;
		public static boolean stampatrasfda = false;
		public static boolean lottineg = false;
		public static String listbase = "";
		public static String datainizioblocco = "";
		//public static boolean ordforeditqta = false;
		public static boolean inviodopofineg = false;
		public static boolean inviotrasfvsmezzo = false;
		public static boolean noxeterm = false;
		public static boolean ddtstampatimbro = false;
		public static int ddtdatiage = 0;
		public static int numinv = 1;
		public static boolean noscarsede = false;
		public static boolean notrasfvs = false;
		public static boolean notrasfda = false;
		public static boolean noanndoc = false;
		public static int azioneblocco = 0;
		public static boolean vendqtatot = false;
		public static boolean prezzopref = false;
		public static boolean noriccarichi = false;
		public static boolean ddtstamparaggrlotti = false;
		public static boolean ddtstampascop = false;
		public static boolean ddtqtanocess = false;
		public static boolean finegstampadatiart = false;
		public static boolean finegstampaelencodoc = false;
		public static boolean backupauto = false;
		public static int tipobackupauto = 0;
		public static int backupperiodico = 0;
		public static boolean scasedenovalvend = false;
		public static boolean scasedenovalrot = false;
		public static boolean scasedenovalscad = false;
		public static boolean termordini = true;
		public static double maxscvend = 0;
		public static boolean visprzacq = false;
		public static boolean incattivanote = false;
		public static boolean incsceglicassa = false;
		public static int tipoterm = 0;
		public static final int TIPOTERM_TENTATAVENDITA = 0;
		public static final int TIPOTERM_RACCOLTAORDINI = 1;
		public static final int TIPOTERM_DISTRIBUTORIAUTOMATICI = 2;

		public static boolean calcaccisa = false;
		public static int tipocalcaccisa = 0;
		public static String voceaccisa = "";
		public static String cmddtqta = "";

		public static boolean codartnum = false;
		public static boolean ricsolodest = false;
		public static boolean nostampadocvend = false;
		public static boolean nostampaddtcarico = false;
		public static boolean stampaordtotali = false;

		public static boolean noordsede = false;
		public static boolean nosostlotti = false;
		public static boolean nogestlist = false;
		public static boolean nocambiovend = false;
		public static boolean nocambiogirovisita = false;
		public static boolean noprztrasp = false;
		public static boolean noinv = false;
		public static boolean nomenustampe = false;
		public static boolean incnostampa = false;
		public static boolean abbuoniattivi = false;
		public static boolean alertagg = false; //variabile che identifica se è presente o meno un aggiornamento
		public static boolean dispart = false;
		public static String gvcod = "";
		public static boolean moddoc = false;

/*		public static String bt_indserver = "";
		public static String bt_indservercs = "";*/

		public static String gprs_indserver = "";
		public static String gprs_postserver = "";

		public static HashMap<String, Record> hcodpag = null;
		public static HashMap<String, Record> hcodiva = null;
		public static HashMap<String, Record> hblocchicau = null;

		public static boolean sceltaemail = false;
		public static String emailfirmariga1 = "";
		public static String emailfirmariga2 = "";
		public static String emailfirmariga3 = "";
		public static boolean orddepsede = false;
		public static boolean aggiornamenti = false;

		public static boolean ddtcarlottosede = false;

		public static boolean pwdanndoc = false;
		public static boolean pwdpar = false;
		public static boolean pwdutilita = false;
		public static boolean passcodetrasf = false;
		public static int copieddt = 1;
		public static int copieddtqta = 1;
		public static boolean noannstampe = false;
		public static boolean colli1 = false;
		public static boolean usalistinonuovo = false;
		public static boolean ordinicligiacterm = false;
		public static boolean nostampafinegiornata = false;

		public static boolean anndocnoagggiac = false;
		public static int ordinericart = 0;

		public static boolean finegstampasospdainc = true;
		public static boolean finegdistintadenaro = false;
		public static boolean ordforeditqta = false;
		public static boolean invioordineauto = false;
		// scambio dati tra forms

		public static ArrayList<Record> trasfdoc_righecrp = null;
		public static ArrayList<Record> righecrptmp = null; // vettore righe articoli (ogni oggetto è una hashtable contenente i campi della riga)
		public static Record trasfdoc_rrigavar = null;
		public static Record trasfdoc_rriga = null;
		public static int trasfdoc_caucorr = 0;
        public static boolean ricdest2 = false;
		public static WiFiPort wifiPort = null;
		public static BluetoothPort btPort = null;
		public static CPCLPrinter cpclPrinter = null;
		public static Thread hThread = null;
		public static int quantitaCarrello = 0;
		public static String depgiac = "";
		public static int sptipodoc = 0;
		public static int stampe_par1 = 4;

		// PAX
		public static String ipServerJCloud = "http://j-cloud.it:8080/J-Cloud";
		//public static String ipServerJCloud = "http://192.168.10.59:8080/J-Cloud/";
}
