package jsoftware.jbeerapp.env;

import java.util.StringTokenizer;
import java.util.Vector;

public class funzStringa
{
	// The following function is a placeholder for control initialization.
	// You should call this function from a constructor or initialization function.
	public void vcInit() {
		//{{INIT_CONTROLS
		//}}
	}
	
    public static final int SX = 0;
    public static final int DX = 1;
    
    public static String stringa(String s, int lung)
    {
        String ris = "";
        for (int i = 0; i < lung; i++) ris += s;
        return ris;
    }
    
    public static String spazi(int lung)
    {
        if (lung == 0)
            return "";
        else
            return stringa(" ",lung);
    }

    public static String riempiStringa(String s, int lung, int dir, char cr)
    {
        String ris;
        char[] c = {cr};
        
        if (s == null || s.equals(""))
            ris = stringa(new String(c), lung);
        else
        {
            if (dir == SX)
            {
                if (s.length() >= lung)
                    ris = s.substring(0, lung);
                else
                    ris = s + stringa(new String(c), lung - s.length());
            }
            else
            {
                if (s.length() >= lung)
                    ris = s.substring(0, lung);
                else
                    ris = stringa(new String(c), lung - s.length()) + s;
            }
        }            
        return ris;
    }        

    public static String rimpiazza(String orig, String s, int inizio)
    {
        String ris = "";
        
        if (orig == null)
            return spazi(inizio) + s;
        else if (orig.length() == 0)
            return spazi(inizio) + s;
        else if (orig.length() < inizio + 1)
            return orig;
        else if (inizio < 0)
            return orig;
        else if (s == null || s == "")
            return orig;
        else if (inizio + s.length() > orig.length())
        {
            ris = orig.substring(0, inizio) + s;
            ris = ris.substring(0, orig.length());
            return ris;
        }
        else if (inizio + s.length() == orig.length())
            return orig.substring(0, inizio) + s; 
        else if (inizio == 0)
        {
            ris = s + orig.substring(s.length(), orig.length());
            return ris;
        }
        else
        {
            return orig.substring(0, inizio) + s + orig.substring(inizio + s.length());
        }
    }        

    public static String rimuovi(String orig, String s)
    {
        String ris = orig;
        int i;
        
        if (orig == null || orig.length() == 0 || s == null || s.length() == 0)
            return orig;
        else
        {
            while((i = ris.indexOf(s)) != -1)
            {
                if (i == ris.length() - 1)
                    ris = ris.substring(0, i);
                else
                    ris = ris.substring(0, i) + ris.substring(i + 1);  
            }            
            return ris;
        }
    }        

    public static String rimuovi(String orig, char c)
    {
        char[] ca = {c};
        String s = new String(ca);
        return rimuovi(orig, s);
    }

    public static String sostituisci(String orig, char c1, char c2)
    {
        String ris = orig;
        
        if (orig == null)
            return null;
        if (orig.length() == 0)
            return orig;
        int pos = 0;
        while ((pos = ris.indexOf(c1)) >= 0)
        {
            char[] c = {c2};
            ris = rimpiazza(ris,new String(c),pos);
        }
        return ris;
    }
	//{{DECLARE_CONTROLS
	//}}

    public static Vector creaRighe(String orig, int lungriga)
    {
        Vector v = new Vector();
        
        try
        {
            int rigacorr = 0;
            String riga = "";
            StringTokenizer st = new StringTokenizer(orig);
            while (st.hasMoreTokens())
            {
                String token = st.nextToken();
                if (token.length() <= lungriga)
                {
                    if (riga.length() + token.length() + ((riga.length() == 0)?0:1) <= lungriga)
                    {
                        if (riga.length() == 0)
                            riga += token;
                        else
                            riga += " " + token;
                        if (riga.length() == lungriga)
                        {
                            v.addElement(riga);
                            riga = "";
                        }
                    }
                    else
                    {
                        v.addElement(riga);
                        riga = token;
                        if (riga.length() == lungriga)
                        {
                            v.addElement(riga);
                            riga = "";
                        }
                    }
                }
                else
                {
                    boolean primo = true;
                    while (token.length() > lungriga)
                    {
                        if (primo && riga.length() > 0)
                        {
                            int residuo = lungriga - riga.length() - 1;
                            riga += " " + token.substring(0, residuo);
                            token = token.substring(residuo);
                            v.addElement(riga);
                            riga = "";
                        }
                        else
                        {
                            int residuo = lungriga - riga.length();
                            riga += token.substring(0, residuo);
                            token = token.substring(residuo);
                            v.addElement(riga);
                            riga = "";
                        }
                        primo = false;
                    }
                    if (token.length() > 0)
                    {
                        riga += token;
                        if (riga.length() == lungriga)
                        {
                            v.addElement(riga);
                            riga = "";
                        }
                    }
                }
            }
            if (riga.length() > 0)
                v.addElement(riga);
        }
        catch (Exception e)
        {
            System.out.println("Errore su creazione righe:" + e.getMessage());
        }
        return v;
    }

    public static String invertiStringa(String s)
    {
        String inv = "";
        int size = s.length();
        for(int i = size - 1; i >= 0; i--)
        {
            inv = inv.concat(String.valueOf(s.charAt(i)));
        }
        return inv;
    }
    public static String invertiStringaData(String s)
    {
        String inv = "";

        inv = inv.concat(String.valueOf(s.charAt(4)));
        inv = inv.concat(String.valueOf(s.charAt(5)));
        inv = inv.concat(String.valueOf(s.charAt(2)));
        inv = inv.concat(String.valueOf(s.charAt(3)));
        inv = inv.concat(String.valueOf(s.charAt(0)));
        inv = inv.concat(String.valueOf(s.charAt(1)));

        return inv;
    }

    public static String tagliaStringa(String s, int ncar)
    {
        if (s.length() > ncar)
            return s.substring(0, ncar);
        else
            return s;
    }
}