package jsoftware.jbeerapp.env;

import java.util.HashMap;

/**
 * Created by pgx71 on 15/06/2016.
 */
public class Record
{
    private HashMap map;

    public Record()
    {
        map = new HashMap();
    }

    public int numeroCampi()
    {
        return map.size();
    }

    public void insStringa(String nomecampo, String valore)
    {
        map.put(nomecampo, valore);
    }

    public void insIntero(String nomecampo, int valore)
    {
        map.put(nomecampo, valore);
    }

    public void insLong(String nomecampo, long valore)
    {
        map.put(nomecampo, valore);
    }

    public void insDouble(String nomecampo, double valore)
    {
        map.put(nomecampo, valore);
    }

    public void eliminaCampo(String nomecampo)
    {
        map.remove(nomecampo);
    }

    public boolean esisteCampo(String nomecampo)
    {
        return map.containsKey(nomecampo);
    }

    public boolean nullo(String nomecampo)
    {
        return (map.get(nomecampo) == null);
    }

    public String leggiStringa(String nomecampo)
    {
        return (String) map.get(nomecampo);
    }

    public int leggiIntero(String nomecampo)
    {
        return (int) map.get(nomecampo);
    }

    public long leggiLong(String nomecampo)
    {
        return (long) map.get(nomecampo);
    }

    public double leggiDouble(String nomecampo)
    {
        return (double) map.get(nomecampo);
    }

}

