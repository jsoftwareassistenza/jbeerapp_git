package jsoftware.jbeerapp.env;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;

import javax.net.ssl.HttpsURLConnection;


public class FunzioniHTTP
{

    public static BufferedReader inviaPostHttp(String url, String content, String contentType, Proxy proxy, Hashtable hcookies, boolean aggiornaCookies, boolean usaproxy,
                                               boolean autproxy, String userproxy, String pwdproxy, int httptimeout)
    {
        BufferedReader bufline = null;
        try
        {
            System.out.println("INVIO RICHIESTA " + url);
            URL u = new URL(url);
            HttpURLConnection con = null;
            if (usaproxy)
            {
                con = (HttpURLConnection) u.openConnection(proxy);
            }
            else
            {
                con = (HttpURLConnection) u.openConnection();
            }
            con.setReadTimeout(httptimeout);
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false);
            con.setRequestMethod("POST");
            if (usaproxy && autproxy)
            {
                String aut = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                con.setRequestProperty("Proxy-Authorization", "Basic " + aut);
            }
            con.setRequestProperty("Content-Type", contentType);
            con.setRequestProperty("Content-Length", "" + content.getBytes().length);
            String c = preparaCookies(hcookies);
            //System.err.println("cookies:" + c);
            con.setRequestProperty("Cookie", c);
            con.setInstanceFollowRedirects(false);
            con.connect();
            DataOutputStream printout = new DataOutputStream(con.getOutputStream());
            printout.writeBytes(content);
            printout.flush();
            printout.close();
            if (aggiornaCookies)
                leggiCookies(con, hcookies);
            bufline = new BufferedReader(new InputStreamReader(con.getInputStream()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return bufline;
    }

    public static BufferedInputStream inviaPostHttp2(String url, String content, String contentType, Proxy proxy, Hashtable hcookies, boolean aggiornaCookies, boolean usaproxy,
                                                     boolean autproxy, String userproxy, String pwdproxy, int httptimeout)
    {
        BufferedInputStream buf = null;
        try
        {
            System.out.println("INVIO RICHIESTA " + url);
            URL u = new URL(url);
            HttpURLConnection con = null;
            if (usaproxy)
            {
                con = (HttpURLConnection) u.openConnection(proxy);
            }
            else
            {
                con = (HttpURLConnection) u.openConnection();
            }
            con.setReadTimeout(httptimeout);
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false);
            con.setRequestMethod("POST");
            if (usaproxy && autproxy)
            {
                String aut = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                con.setRequestProperty("Proxy-Authorization", "Basic " + aut);
            }
            con.setRequestProperty("Content-Type", contentType);
            con.setRequestProperty("Content-Length", "" + content.getBytes().length);
            String c = preparaCookies(hcookies);
            //System.err.println("cookies:" + c);
            con.setRequestProperty("Cookie", c);
            con.setInstanceFollowRedirects(false);
            con.connect();
            DataOutputStream printout = new DataOutputStream(con.getOutputStream());
            printout.writeBytes(content);
            printout.flush();
            printout.close();
            if (aggiornaCookies)
                leggiCookies(con, hcookies);
            buf = new BufferedInputStream(con.getInputStream());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return buf;
    }

    public static void leggiCookies(HttpsURLConnection urlc, Hashtable hcookies)
    {
        //Hashtable hcookies = new Hashtable();
        for (int j = 1;; j++)
        {
            String header = urlc.getHeaderField(j);
            if (header == null)
            {
                break;
            }
            //System.out.println("H:" + urlc.getHeaderField(j));
            if (urlc.getHeaderFieldKey(j).startsWith("Set-Cookie"))
            {
                String cookie = urlc.getHeaderField(j);
                cookie = cookie.substring(0, cookie.indexOf(";"));
                hcookies.put(cookie.substring(0, cookie.indexOf("=")), cookie);
                //System.out.println("cookie in:" + cookie);
            }
        }
    }

    public static void leggiCookies(HttpURLConnection urlc, Hashtable hcookies)
    {
        //Hashtable hcookies = new Hashtable();
        for (int j = 1;; j++)
        {
            String header = urlc.getHeaderField(j);
            if (header == null)
            {
                break;
            }
            //System.out.println("H:" + urlc.getHeaderField(j));
            if (urlc.getHeaderFieldKey(j).startsWith("Set-Cookie"))
            {
                String cookie = urlc.getHeaderField(j);
                cookie = cookie.substring(0, cookie.indexOf(";"));
                hcookies.put(cookie.substring(0, cookie.indexOf("=")), cookie);
                //System.out.println("cookie in:" + cookie);
            }
        }
    }

    public static String preparaCookies(Hashtable hcookies)
    {
        String cookies = "";
        Enumeration en = hcookies.elements();
        while (en.hasMoreElements())
        {
            cookies += (String) en.nextElement() + "; ";
        }
        if (cookies.length() > 0)
        {
            cookies = cookies.substring(0, cookies.length() - 2);
        }
        return cookies;
    }
    
    public static BufferedReader inviaPostMultipartHttp(String url, ArrayList<HashMap> campiForm, ArrayList<HashMap> filesUpload, Proxy proxy, Hashtable hcookies, boolean aggiornaCookies, boolean usaproxy,
                                                        boolean autproxy, String userproxy, String pwdproxy, int httptimeout)
    {
        BufferedReader bufline = null;
        try
        {
            String boundary = "===" + System.currentTimeMillis() + "===";
            
            System.out.println("INVIO RICHIESTA " + url);
            URL u = new URL(url);
            HttpURLConnection con = null;
            if (usaproxy)
            {
                con = (HttpURLConnection) u.openConnection(proxy);
            }
            else
            {
                con = (HttpURLConnection) u.openConnection();
            }
            con.setReadTimeout(httptimeout);
            con.setDoInput(true);
            con.setDoOutput(true);
            //con.setUseCaches(false);
            con.setRequestMethod("POST");
            if (usaproxy && autproxy)
            {
                String aut = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                con.setRequestProperty("Proxy-Authorization", "Basic " + aut);
            }
            con.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            if (hcookies.size() > 0)
            {
                String c = preparaCookies(hcookies);
                con.setRequestProperty("Cookie", c);
            }
            //con.setInstanceFollowRedirects(false);
            con.connect();
            OutputStream outputstream = con.getOutputStream();
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputstream, "UTF-8"), true);
            for (int i = 0; i < campiForm.size(); i++)
            {
                HashMap rc = campiForm.get(i);
                writer.append("--" + boundary).append("\r\n");
                writer.append("Content-Disposition: form-data; name=\"" + (String) rc.get("nome") + "\"").append("\r\n");
                writer.append("Content-Type: text/plain; charset=UTF-8").append("\r\n");
                writer.append("\r\n");
                writer.append((String) rc.get("valore")).append("\r\n");
                writer.flush();
            }
            for (int i = 0; i < filesUpload.size(); i++)
            {
                HashMap rc = filesUpload.get(i);
                String fileName = new File((String) rc.get("nomefile")).getName();
                writer.append("--" + boundary).append("\r\n");
                writer.append("Content-Disposition: form-data; name=\"" + (String) rc.get("nome")
                        + "\"; filename=\"" + fileName + "\"").append("\r\n");
                writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName)).append("\r\n");
                writer.append("Content-Transfer-Encoding: binary").append("\r\n");
                writer.append("\r\n");
                writer.flush();
                FileInputStream inputStream = new FileInputStream((String) rc.get("nomefile"));
                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                while ((bytesRead = inputStream.read(buffer)) != -1)
                {
                    outputstream.write(buffer, 0, bytesRead);
                }
                outputstream.flush();
                inputStream.close();
                writer.append("\r\n");
                writer.flush();
            }
            writer.append("\r\n").flush();
            writer.append("--" + boundary + "--").append("\r\n");
            writer.flush();
            writer.close();
            bufline = new BufferedReader(new InputStreamReader(con.getInputStream()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return bufline;
    }
}

