package jsoftware.jbeerapp.env;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Data
{
    private String d;

    public static final int GGMMAAAA = 0;
    public static final int AAAAMMGG = 1;
    public static final int GG_MM_AAAA = 2;
    public static final int AAAA_MM_GG = 3;
    public static final int GGMMAA = 4;
    public static final int AAMMGG = 5;
    public static final int GG_MM_AA = 6;




    // data odierna
    public Data()
    {
        SimpleDateFormat sfd = new SimpleDateFormat("yyyyMMdd");
        Date oggi = new Date();
        d = sfd.format(oggi);
    }

    public Data(int giorno, int mese, int anno)
    {
        d = Formattazione.formIntero(anno, 4, "0") + Formattazione.formIntero(mese, 2, "0") +
            Formattazione.formIntero(giorno, 2, "0");
    }

    public Data(String dt, int tipo)
    {
        switch (tipo)
        {
            case GGMMAAAA:
                d = dt.substring(4, 8) + dt.substring(2, 4) + dt.substring(0, 2);
                break;
            case AAAAMMGG:
                d = dt;
                break;
            case GG_MM_AAAA:
                if (dt.length() == 10)
                    d = dt.substring(6, 10) + dt.substring(3, 5) + dt.substring(0, 2);
                else if (dt.length() == 8)
                    d = dt.substring(4, 8) + dt.substring(2, 4) + dt.substring(0, 2);
                break;
            case AAAA_MM_GG:
                d = dt.substring(0, 4) + dt.substring(5, 7) + dt.substring(8, 10);
                break;
            case GGMMAA:
                d = "20" + dt.substring(4, 6) + dt.substring(2, 4) + dt.substring(0, 2);
                break;
            case AAMMGG:
                d = "20" + dt.substring(0, 2) + dt.substring(2, 4) + dt.substring(4, 6);
                break;
            case GG_MM_AA:
                d = "20" + dt.substring(6, 8) + dt.substring(3, 5) + dt.substring(0, 2);
                break;
        }
    }

    // intero del tipo AAAAMMGG
    public int intero()
    {
        return Integer.parseInt(d);
    }

    public int giorno()
    {
        return Integer.parseInt(d.substring(6, 8));
    }

    public int mese()
    {
        return Integer.parseInt(d.substring(4, 6));
    }

    public String descrizioneMese()
    {
        int m = Integer.parseInt(d.substring(4, 6));
        if (m == 1)
            return "GENNAIO";
        else if (m == 2)
            return "FEBBRAIO";
        else if (m == 3)
            return "MARZO";
        else if (m == 4)
            return "APRILE";
        else if (m == 5)
            return "MAGGIO";
        else if (m == 6)
            return "GIUGNO";
        else if (m == 7)
            return "LUGLIO";
        else if (m == 8)
            return "AGOSTO";
        else if (m == 9)
            return "SETTEMBRE";
        else if (m == 10)
            return "OTTOBRE";
        else if (m == 11)
            return "NOVEMBRE";
        else if (m == 12)
            return "DICEMBRE";
        else
            return "";
    }

    public int anno()
    {
        return Integer.parseInt(d.substring(0, 4));
    }

    public boolean maggiore(Data dt)
    {
        return (this.intero() > dt.intero());
    }

    public boolean minore(Data dt)
    {
        return (this.intero() < dt.intero());
    }

    public boolean uguale(Data dt)
    {
        return (this.intero() == dt.intero());
    }

    public boolean minoreuguale(Data dt)
    {
        return (this.intero() <= dt.intero());
    }

    public boolean maggioreuguale(Data dt)
    {
        return (this.intero() >= dt.intero());
    }

	public static boolean annoBisestile(String anno)
	{
        return annoBisestile(Integer.parseInt(anno));
	}

	public static boolean annoBisestile (int anno)
	{
	    if ((anno % 4 == 0 & anno % 100 != 0) || (anno % 400 == 0))
    		return true;
	    else
		    return false;
	}

    public static boolean dataValida(String dt, int tipo)
    {
        return (new Data(dt, tipo)).dataValida();
    }

	public boolean dataValida()
	{
		boolean ris = true;
		int lungMese[] = new int [13];
		int giorno, mese, anno;

		giorno = giorno();
		mese = mese();
		anno = anno();
		lungMese[0] = 0;
		lungMese[1] = 31;
		lungMese[2] = 28;
		lungMese[3] = 31;
		lungMese[4] = 30;
		lungMese[5] = 31;
		lungMese[6] = 30;
		lungMese[7] = 31;
		lungMese[8] = 31;
		lungMese[9] = 30;
		lungMese[10] = 31;
		lungMese[11] = 30;
		lungMese[12] = 31;

		if (annoBisestile(anno))
			lungMese[2] = 29;

		//controllo della validita del mese
		if (mese < 1 || mese > 12)
			ris = false;
	    else
		//controllo della validita del giorno
		    if (giorno < 1 || giorno > lungMese[mese])
			    ris = false;
		return ris;
	}

    public String formatta(int tipo)
    {
        switch (tipo)
        {
            case GGMMAAAA:
                return d.substring(6, 8) + d.substring(4, 6) + d.substring(0, 4);
            case AAAAMMGG:
                return d;
            case GG_MM_AAAA:
                return d.substring(6, 8) + "/" + d.substring(4, 6) + "/" + d.substring(0, 4);
            case AAAA_MM_GG:
                return d.substring(0, 4) + "/" + d.substring(4, 6) + "/" + d.substring(6, 8);
            case GGMMAA:
                return d.substring(6, 8) + d.substring(4, 6) + d.substring(2, 4);
            case AAMMGG:
                return d.substring(2, 4) + d.substring(4, 6) + d.substring(6, 8);
            case GG_MM_AA:
                return d.substring(6, 8) + "/" + d.substring(4, 6) + "/" + d.substring(2, 4);
        }
        return "";
    }

    public String formatta(int tipo, String sep)
    {
        switch (tipo)
        {
            case GGMMAAAA:
                return d.substring(6, 8) + d.substring(4, 6) + d.substring(0, 4);
            case AAAAMMGG:
                return d;
            case GG_MM_AAAA:
                return d.substring(6, 8) + sep + d.substring(4, 6) + sep + d.substring(0, 4);
            case AAAA_MM_GG:
                return d.substring(0, 4) + sep + d.substring(4, 6) + sep + d.substring(6, 8);
            case GGMMAA:
                return d.substring(6, 8) + d.substring(4, 6) + d.substring(2, 4);
            case AAMMGG:
                return d.substring(2, 4) + d.substring(4, 6) + d.substring(6, 8);
            case GG_MM_AA:
                return d.substring(6, 8) + sep + d.substring(4, 6) + sep + d.substring(2, 4);
        }
        return "";
    }

    public long numeroGiorni()
    {
		// restituisce il numero dei giorni da 0/0/0 a data2   (data2 ggmmaaaa)
		long giornix[] = {0,31,59,90,120,151,181,212,243,273,304,334};
		long giorno = 0;
		int  mese = 0;
		long anno = 0;
		long data = 0;
		long giorni = 0;

        if (dataValida())
	    {
		    // decodifico la stringa in numeri
            giorno = (long) giorno();
            mese = mese();
            anno = (long) anno();
		    data = anno * 10000 + mese * 100 + giorno;

		    //  controllo che non siano digitate date tra 03/09/1752 e 13/09/1752
		    if ( data > 17520902 && data < 17520914)
		        return Long.MAX_VALUE;
		    else
		    {
		        // calcolo numero giorni tenendo conto dei bisestili
		        giorni = 365 * (anno - 1) + ((anno - 1) / 4) + ((anno - 1) / 400) - ((anno - 1) / 100);
		        giorni += giorno + giornix[mese - 1];
		        // allineamento per modifica calendario 1752
		        if (data > 17520902)
		            giorni -= 11;
		        //  sommo uno se l' anno corrente e' bisestile e il mese e' > febbraio
		        if (anno > 1700 && mese > 2 && ((anno % 4 == 0 && anno % 100 != 0) || anno % 400 == 0))
                    giorni += 1;
		        giorni--;     // aggiustamento
		        return giorni;
		    }
        }
        else
            return Long.MAX_VALUE;
    }

    public String giornoSettimana()
    {
	    String gioset[] = {"venerdi'", "sabato", "domenica", "lunedi'", "martedi'", "mercoledi'", "giovedi'"};
	    BigInteger bb[];
	    bb = new BigInteger[2];
        bb[0] = null;
        bb[1] = null ;
        bb[0] = BigInteger.valueOf(numeroGiorni());
        if (bb[0].equals(BigInteger.valueOf(Long.MAX_VALUE)))
            return "";
        else
        {
            bb=bb[0].divideAndRemainder(BigInteger.valueOf(7));
            return gioset[bb[1].intValue()];
        }
    }
    public String giornoSettimanaNum()
    {
	    String gioset[] = {"5", "6", "7", "1", "2", "3", "4"};
	    BigInteger bb[];
	    bb = new BigInteger[2];
        bb[0] = null;
        bb[1] = null ;
        bb[0] = BigInteger.valueOf(numeroGiorni());
        if (bb[0].equals(BigInteger.valueOf(Long.MAX_VALUE)))
            return "";
        else
        {
            bb=bb[0].divideAndRemainder(BigInteger.valueOf(7));
            return gioset[bb[1].intValue()];
        }
    }

    public void incrementaGiorni(long giorni)
    {
        // restituisce la data + giorni
        d = calcolaData(giorni + numeroGiorni());

    }

    public void incrementaGiorniCommerciale(long giorni, int ggtoll)
    {
		System.out.println("Data: " + this.formatta(Data.GG_MM_AAAA, "/"));
		int giornix[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	    long nc = (long) giorni / 30;
        long resto = giorni % 30;
	    if (nc > 0)
	    {
	        for (int i = 1; i <= nc; i++)
	        {
		        long g = 0;
                if (ggtoll == 0 && this.mese() == 1 && this.giorno() > (annoBisestile(this.anno())?28:27))  // flax
                    this.decrementaGiorni(3);
		        //g = giornix[this.mese() - 1] - ((i == nc)?ggtoll:0);
		        if (this.mese() == 2 && annoBisestile(this.anno()))   // flax
		            g = 29 - ((i == nc)?ggtoll:0);                // flax
		        else    if (this.mese() == 12)
		                    g = giornix[0] - ((i == nc)?ggtoll:0);
		                else
                        {
		                    g = giornix[this.mese() - 1] - ((i == nc)?ggtoll:0);  // flax
                                    if(giornix[this.mese() - 1] > giornix[this.mese()] && this.giorno() == 31)  // flax
                                        g--;
                        }
                this.incrementaGiorni(g);
                //System.out.println("i: " + i + " Data: " + this.formatta(Data.GG_MM_AAAA, "/"));
            }
            // resto
            if (resto > 0)
                this.incrementaGiorni(resto);
            //System.out.println("Data dopo incr.comm.: " + this.formatta(Data.GG_MM_AAAA, "/"));
        }
        else
        {
            d = calcolaData(giorni + numeroGiorni());
        }
    }

    public void decrementaGiorni(long giorni)
    {
        // restituisce la data - giorni
        d = calcolaData(numeroGiorni() - giorni);
    }

    public static String calcolaData(long giorni)
    {
        //  restituisce una stringa corrispondente alla data dati quei giorni
        //  la stringa risultante e' in formato aaaammgg

        String dataConfronto;
        long dataLavoro, salvaData, fattoreSalva, fattoreLavoro, giorno, mese, anno;
        int contatore;

        dataConfronto = "";
        dataLavoro = 10101;
        salvaData = 10101;
        contatore = 10000000;
        fattoreLavoro = 0;
        dataLavoro += contatore;
        dataConfronto = String.valueOf(dataLavoro);
        if (giorni == fattoreLavoro)
            return String.valueOf(dataLavoro);
        else
        {
            while (contatore !=0 && fattoreLavoro != giorni)
            {
                salvaData = dataLavoro;
                fattoreSalva = fattoreLavoro;
                dataLavoro += contatore;
                dataConfronto = String.valueOf(dataLavoro);
                mese = Long.valueOf(dataConfronto.substring(4,6)).longValue();
                fattoreLavoro = new Data(dataConfronto, Data.AAAAMMGG).numeroGiorni();
                if (fattoreLavoro > giorni)
                {
                    fattoreLavoro = fattoreSalva;
                    dataLavoro = salvaData;
                    contatore = contatore / 10;
                }
            }
        }
        return String.valueOf(dataLavoro);
    }

    public static boolean controllaData(String d, int tipo)
    {
        switch (tipo)
        {
            case GGMMAAAA:
            {
                try
                {
                    Integer i = new Integer(d);
                    if (d.trim().length() != 8)
                    {
                        return false;
                    }
                    else if (!dataValida(d.substring(0,2) + "/" + d.substring(2,4) + "/" +
                                        d.substring(4,8), GG_MM_AAAA))
                        return false;
                    else
                        return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            case GGMMAA:
            {
                try
                {
                    Integer i = new Integer(d);
                    if (d.trim().length() != 6)
                    {
                        return false;
                    }
                    else if (!dataValida(d.substring(0,2) + "/" + d.substring(2,4) + "/" +
                                        "20" + d.substring(4,6), GG_MM_AAAA))
                        return false;
                    else
                        return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            case AAAAMMGG:
            {
                try
                {
                    Integer i = new Integer(d);
                    if (d.trim().length() != 8)
                    {
                        return false;
                    }
                    else if (!dataValida(d.substring(6, 8) + "/" + d.substring(4, 6) + "/" +
                                        d.substring(0, 4), GG_MM_AAAA))
                        return false;
                    else
                        return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            case AAMMGG:
            {
                try
                {
                    Integer i = new Integer(d);
                    if (d.trim().length() != 6)
                    {
                        return false;
                    }
                    else if (!dataValida(d.substring(4, 6) + "/" + d.substring(2, 4) + "/" +
                                        "20" + d.substring(0, 2), GG_MM_AAAA))
                        return false;
                    else
                        return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            case GG_MM_AAAA:
            {
                try
                {
                    d = d.substring(0,2) + d.substring(3,5) + d.substring(6,10);
                    Integer i = new Integer(d);
                    if (d.trim().length() != 8)
                    {
                        return false;
                    }
                    else if (!dataValida(d.substring(0,2) + "/" + d.substring(2,4) + "/" +
                                        d.substring(4,8), GG_MM_AAAA))
                        return false;
                    else
                        return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            case GG_MM_AA:
            {
                try
                {
                    d = d.substring(0,2) + d.substring(3,5) + d.substring(6,8);
                    Integer i = new Integer(d);
                    if (d.trim().length() != 6)
                    {
                        return false;
                    }
                    else if (!dataValida(d.substring(0,2) + "/" + d.substring(2,4) + "/" +
                                        "20" + d.substring(4,6), GG_MM_AA))
                        return false;
                    else
                        return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            case AAAA_MM_GG:
            {
                try
                {
                    d = d.substring(0, 4) + d.substring(5, 7) + d.substring(8, 10);
                    Integer i = new Integer(d);
                    if (d.trim().length() != 8)
                    {
                        return false;
                    }
                    else if (!dataValida(d.substring(6, 8) + "/" + d.substring(4, 6) + "/" +
                                        d.substring(0, 4), GG_MM_AAAA))
                        return false;
                    else
                        return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }
        return false;
    }


    public void incrementaGiorniFineMese(long giorni)
    {
        //restituisce la data data di fine mese di data2 + giorni (data2 ggmmaaaa)
        // restituisce aaaammgg

        int giornix[] = {31,28,31,30,31,30,31,31,30,31,30,31};

        d = calcolaData(giorni + numeroGiorni());

        if (annoBisestile(this.anno()))
            giornix[1] = 29;
        d = d.substring(0, 4) + d.substring(4, 6) +
            giornix[Integer.valueOf(d.substring(4,6)).intValue() - 1];
    }

      public void cercaMeseBuono(int meseEscluso, int giornoPreferito)
        {
       //  controlla se il mese di data  e' un escluso data ggmmaaaa
       //  restituisce ggmmaaaa
       //  controlla giorno preferito del mese successivo all' escluso

        long mese, anno, giorno = 0;
        int giornix[] = {31,28,31,30,31,30,31,31,30,31,30,31};
        mese = this.mese();
        anno = this.anno();
        giorno = this.giorno();
        if (giornoPreferito != 0)
        {
            if (giornix[meseEscluso - 1] < giornoPreferito)
            {
                if (meseEscluso == 2 && (((anno%4 == 0 && anno % 100 != 0) || anno % 400 == 0)))
                    giornoPreferito = 29;
                else if (meseEscluso == 2)
                    giornoPreferito=28;
                else giornoPreferito = giornix[meseEscluso - 1];
            }
        }
        if (mese == meseEscluso &&  mese != 12)
        {
            mese++;
            if (giornoPreferito != 0)
                giorno = giornoPreferito;
        }
        else if (mese == meseEscluso)
        {
            mese = 1;
            anno++;
            if (giornoPreferito != 0)
                giorno = giornoPreferito;
        }
        String ris = String.valueOf(anno * 10000 + mese * 100 + giorno);
        if (ris.length() != 8)
            d = "0" + ris;
        else
            d = ris;
    }

    public long differenza(Data dt)
    {
        return Math.abs(this.numeroGiorni() - dt.numeroGiorni());
    }
}