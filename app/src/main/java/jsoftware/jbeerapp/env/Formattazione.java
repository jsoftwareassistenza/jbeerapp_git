package jsoftware.jbeerapp.env;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;


public class Formattazione
{
	// The following function is a placeholder for control initialization.
	// You should call this function from a constructor or initialization function.
	public void vcInit() {
		//{{INIT_CONTROLS
		//}}
	}
	public static final int NO_SEGNO = 0;
	public static final int SEGNO_SX = 1;
	public static final int SEGNO_DX = 2;
	public static final int SEGNO_DX_NEG = 3;
	public static final int SEGNO_SX_NEG = 4;
	
    public static char leggiSeparatoreDecimale()
    {
        char dec = '\0';
        int i = 0;
        
        try
        {
            new Double("0.0");
            dec = '.';
        }
        catch (Exception e)
        {
            dec = ',';
        }
        return dec;
    }	

    public static String formatta(int val, String frm, char sepmigl, int segno)
    {
        DecimalFormat df = new DecimalFormat(frm);
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setGroupingSeparator(sepmigl);
        df.setDecimalFormatSymbols(dfs);
        String ris = df.format(Math.abs(val));
        switch (segno)
        {
            case SEGNO_SX:
                ris = ((val >= 0)?"+":"-") + ris;
                break;
            case SEGNO_DX:
                ris = ris + ((val >= 0)?"+":"-");
                break;
        }            
        
        return ris;        
    }        

    public static String formatta(long val, String frm, char sepmigl, int segno)
    {
        DecimalFormat df = new DecimalFormat(frm);
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setGroupingSeparator(sepmigl);
        df.setDecimalFormatSymbols(dfs);
        String ris = df.format(Math.abs(val));
        switch (segno)
        {
            case SEGNO_SX:
                ris = ((val >= 0)?"+":"-") + ris;
                break;
            case SEGNO_DX:
                ris = ris + ((val >= 0)?"+":"-");
                break;
        }            
        
        return ris;        
    }        

    public static String formatta(float val, String frm, char sepdec, char sepmigl, int segno)
    {
        DecimalFormat df = new DecimalFormat(frm);
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator(sepdec);
        dfs.setGroupingSeparator(sepmigl);
        df.setDecimalFormatSymbols(dfs);
        String ris = df.format(Math.abs(val));
        switch (segno)
        {
            case SEGNO_SX:
                ris = ((val >= 0)?"+":"-") + ris;
                break;
            case SEGNO_DX:
                ris = ris + ((val >= 0)?"+":"-");
                break;
        }            
        
        return ris;        
    }        

    public static String formatta(double val, String frm, int segno)
    {
        DecimalFormat df = new DecimalFormat(frm);
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator(',');
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        String ris = df.format(Math.abs(val));
        switch (segno)
        {
            case SEGNO_SX:
                ris = ((val >= 0)?"+":"-") + ris;
                break;
            case SEGNO_DX:
                ris = ris + ((val >= 0)?"+":"-");
                break;
            case SEGNO_DX_NEG:
                ris = ris + ((val >= 0)?"":"-");
                break;
            case SEGNO_SX_NEG:
                ris = ((val >= 0)?"":"-") + ris;
                break;
        }            
        
        return ris;        
    }        

    public static String formatta(double val, String frm, char sepdec, char sepmigl, int segno)
    {
        DecimalFormat df = new DecimalFormat(frm);
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator(sepdec);
        dfs.setGroupingSeparator(sepmigl);
        df.setDecimalFormatSymbols(dfs);
        String ris = df.format(Math.abs(val));
        switch (segno)
        {
            case SEGNO_SX:
                ris = ((val >= 0)?"+":"-") + ris;
                break;
            case SEGNO_DX:
                ris = ris + ((val >= 0)?"+":"-");
                break;
        }            
        
        return ris;        
    }        

    public static String formatta(short val, String frm, char sepmigl, int segno)
    {
        DecimalFormat df = new DecimalFormat(frm);
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setGroupingSeparator(sepmigl);
        df.setDecimalFormatSymbols(dfs);
        String ris = df.format(Math.abs(val));
        switch (segno)
        {
            case SEGNO_SX:
                ris = ((val >= 0)?"+":"-") + ris;
                break;
            case SEGNO_DX:
                ris = ris + ((val >= 0)?"+":"-");
                break;
        }            
        
        return ris;        
    }        


    public static String formValuta(String s, int nInt, int nDec, char sepmigl, int segno)
    {
        int i = 0;
        String ris = "";
        // ripulisce stringa
        for (i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) != '.')
                ris = ris + s.charAt(i);
        }            
        ris = ris.replace(',','.');
        // analizza segno
        if (segno == SEGNO_DX)
            if (ris.charAt(ris.length() - 1) == '+')
                ris = "+" + ris.substring(0, ris.length() - 1);
            else if (ris.charAt(ris.length() - 1) == '-')
                ris = "-" + ris.substring(0, ris.length() - 1);
        // crea stringa di formattazione
        String frm = "";
        // parte intera
        for (i = 0; i < nInt; i++)
        {
            if ((i % 3 == 0) && (i != 0))
                frm = "#," + frm;
            else
                frm = "#" + frm;
        }
        // parte decimale
        if (nDec > 0)
            frm = frm + "." + funzStringa.stringa("0", nDec);
        
        return formatta((new Double(ris)).doubleValue(), frm, segno);
    }        
        
    public static String formValuta(String s, int nInt, int nDec, int segno)
    {
        int i = 0;
        String ris = "";
        // ripulisce stringa
        for (i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) != ',')
                ris = ris + s.charAt(i);
        }            
        // analizza segno
        if (segno == SEGNO_DX)
            if (ris.charAt(ris.length() - 1) == '+')
                ris = "+" + ris.substring(0, ris.length() - 1);
            else if (ris.charAt(ris.length() - 1) == '-')
                ris = "-" + ris.substring(0, ris.length() - 1);
        // crea stringa di formattazione
        String frm = "";
        // parte intera
        for (i = 0; i < nInt; i++)
        {
            if ((i % 3 == 0) && (i != 0))
                frm = "#," + frm;
            else
                frm = "#" + frm;
        }
        // parte decimale
        if (nDec > 0)
            frm = frm + "." + funzStringa.stringa("0", nDec);
        
        return formatta((new Double(ris)).doubleValue(), frm, segno);
    }        

    public static String formValuta(double v, int nInt, int nDec, int segno)
    {
        int i = 0;
        String ris = "";
        String s = (new Double(v)).toString();
        // ripulisce stringa
        for (i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) != ',')
                ris = ris + s.charAt(i);
        }            
        // analizza segno
        if (segno == SEGNO_DX)
            if (ris.charAt(ris.length() - 1) == '+')
                ris = "+" + ris.substring(0, ris.length() - 1);
            else if (ris.charAt(ris.length() - 1) == '-')
                ris = "-" + ris.substring(0, ris.length() - 1);
        // crea stringa di formattazione
        String frm = "";
        // parte intera
        for (i = 0; i < nInt; i++)
        {
            if ((i % 3 == 0) && (i != 0))
                frm = "#," + frm;
            else
            {
                if (i == 0)
                    frm = "0" + frm;
                else
                    frm = "#" + frm;
            }
        }
        // parte decimale
        if (nDec > 0)
            frm = frm + "." + funzStringa.stringa("0", nDec);
        
        return formatta((new Double(ris)).doubleValue(), frm, segno);
    }        



    public static String formIntero(int i, int nCifre, String riemp)
    {
        String s = (new Integer(i)).toString();
        String ris = "";
        
        if (nCifre > s.length())
        {
            for (int j = 1; j <= nCifre - s.length(); j++)
                ris += riemp;        
            ris += s;
        }            
        else
            ris = s;
        return ris;            
    }        

    public static String formLong(long i, int nCifre, String riemp)
    {
        String s = (new Long(i)).toString();
        String ris = "";
        
        if (nCifre > s.length())
        {
            for (int j = 1; j <= nCifre - s.length(); j++)
                ris += riemp;        
            ris += s;
        }            
        else
            ris = s;
        return ris;            
    }        

    public static String formIntero(Integer i, int nCifre, String riemp)
    {
        String s = i.toString();
        String ris = "";
        
        if (nCifre > s.length())
        {
            for (int j = 1; j <= nCifre - s.length(); j++)
                ris += riemp;        
            ris += s;
        }            
        else
            ris = s;
        return ris;            
    }        

    public static String formLong(Long i, int nCifre, String riemp)
    {
        String s = i.toString();
        String ris = "";
        
        if (nCifre > s.length())
        {
            for (int j = 1; j <= nCifre - s.length(); j++)
                ris += riemp;        
            ris += s;
        }            
        else
            ris = s;
        return ris;            
    }        

    public static String formStringa(String s, int lung, String riemp, int dir)
    {
        String ris = "";
        
        if (s == null)
        {
            return null;
        }            
        else if (s.length() == 0)
        {
            return s;
        }            
        else
        {
            switch (dir)
            {
                case DIR_SX:
                    ris = s;
                    for (int i = 1; i <= lung - s.length(); i++)
                        ris += riemp;
                    ris = ris.substring(0, lung);                        
                    break;
                case DIR_DX:
                    ris = s;
                    for (int i = 1; i <= lung - s.length(); i++)
                        ris = riemp + ris;
                    ris = ris.substring(0, lung);                        
                    break;
            }                
            return ris;
        }
    }        
    
    public static String stringa(String s, int lung)
    {
        String ris = "";
        
        if (s == null)
        {
            return null;
        }
        else if (s.length() == 0)
        {
            return s;
        }
        else
        {
            for (int i = 1; i <= lung; i++)
                ris += s;
            ris = ris.substring(0, lung);
            return ris;
        }
    }
    
    public static String stringa(char c, int lung)
    {
        char[] ca = {c};
        String s = new String(ca);
        return stringa(s, lung);
    }
 
    public static String stringa(Character c, int lung)
    {
        char[] ca = {c.charValue()};
        String s = new String(ca);
        return stringa(s, lung);
    }

    public static double estraiDouble(String s)
    {
        String ris = "";
        for (int i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) == ',' ||
                Character.getType(s.charAt(i)) ==
                    Character.DECIMAL_DIGIT_NUMBER)
                ris = ris + s.charAt(i);

            if (s.charAt(i) == '+' ||
                s.charAt(i) == '-')
                ris = s.charAt(i) + ris;
        }
        //Log.v("s:" + s + " estraidouble:", ris);
        try
        {
            return (new Double(ris.replace(',','.'))).doubleValue();
        }            
        catch (Exception e)
        {
            return 0;
        }            
    }
    
    public static float estraiFloat(String s)
    {
        String ris = "";
        for (int i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) == ',' ||
                s.charAt(i) == '+' ||
                s.charAt(i) == '-' ||
                Character.getType(s.charAt(i)) ==
                    Character.DECIMAL_DIGIT_NUMBER)
                ris = ris + s.charAt(i);
        }            
        try
        {
            return (new Float(ris.replace(',','.'))).floatValue();
        }            
        catch (Exception e)
        {
            return 0;
        }            
    }

    public static int estraiIntero(String s)
    {
        String ris = "";
        for (int i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) == '+' ||
                s.charAt(i) == '-' ||
                Character.getType(s.charAt(i)) ==
                    Character.DECIMAL_DIGIT_NUMBER)
                ris = ris + s.charAt(i);
        }            
        if (ris.length() > 1)
        {      
            if (ris.substring(ris.length() - 1).equals("-"))
                ris = ris.substring(ris.length() - 1) + ris.substring(0, ris.length() - 1);
            else if (ris.substring(ris.length() - 1).equals("+"))
                ris = ris.substring(0, ris.length() - 1);
        }
        try
        {
            return (new Integer(ris)).intValue();
        }            
        catch (Exception e)
        {
            return 0;
        }            
    }

    public static long estraiLong(String s)
    {
        String ris = "";
        for (int i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) == '+' ||
                s.charAt(i) == '-' ||
                Character.getType(s.charAt(i)) ==
                    Character.DECIMAL_DIGIT_NUMBER)
                ris = ris + s.charAt(i);
        }            
        try
        {
            return (new Long(ris)).longValue();
        }            
        catch (Exception e)
        {
            return 0;
        }            
    }

    public static final int DIR_SX = 0;
    public static final int DIR_DX = 1;
	//{{DECLARE_CONTROLS
	//}}

    public static final String inLettere(double val, int valuta)
    {
        String[] vu = {"ZERO","UNO","DUE","TRE","QUATTRO","CINQUE","SEI","SETTE","OTTO","NOVE"};
        String[] vd2 = {"UNDICI","DODICI","TREDICI","QUATTORDICI","QUINDICI","SEDICI","DICIASSETTE","DICIOTTO","DICIANNOVE"};
        String[] vd = {"DIECI","VENTI","TRENTA","QUARANTA","CINQUANTA","SESSANTA","SETTANTA","OTTANTA","NOVANTA"};
        
        String ris = "";
        
        // trasf.in stringa
        String v = Formattazione.formatta(val, "000000000000.00", 0);
        // estrae parte intera
        String vint = v.substring(0, v.indexOf(",") + 1);
        // estrae parte decimale
        String vdec = v.substring(v.indexOf(",") + 1);
    
        if (val == 100)
        {
            ris = "CENTO";
        }
        else if (val == 1000)
        {
            ris = "MILLE";
        }
        else
        {
            // analizza stringa interi a gruppi di tre
            for (int i = 0; i < 4; i++)
            {
                String ccc = "";
                if (i == 0)
                    ccc = vint.substring(0, 3);
                else if (i == 1)
                    ccc = vint.substring(3, 6);
                else if (i == 2)
                    ccc = vint.substring(6, 9);
                else if (i == 3)
                    ccc = vint.substring(9, 12);
            
                int n1 = Integer.parseInt(ccc.substring(2));
                int n2 = Integer.parseInt(ccc.substring(1, 2));
                int n3 = Integer.parseInt(ccc.substring(0, 1));
                
                if (n3 > 0)
                {
                    if (n3 > 1)
                        ris += vu[n3] + "CENTO";
                    else
                        ris += "CENTO";
                }
                if (n2 > 0)
                {
                    if (n2 >= 2 || n1 == 0)
                    {
                        if (n2 > 0)    
                        {
                            ris += vd[n2 - 1];
                            // toglie ultima vocale da decine
                            if (n1 == 1 || n1 == 8)
                                ris = ris.substring(0, ris.length() - 1);
                        }
                        if (n1 > 0)
                            ris += vu[n1];
                    }
                    else
                    {
                        ris += vd2[n1 - 1];
                    }
                }
                else
                {
                    if (i != 2 || n1 != 1)
                    {
                        if (n1 > 0)
                            ris += vu[n1];
                    }
                }
                
                if (i == 0)
                {
                    if (n3 > 0 || n2 > 0 || n1 > 0)
                    {
                        if (n3 == 0 && n2 == 0 && n1 == 1)
                            ris += "MILIARDO";
                        else
                            ris += "MILIARDI";
                    }
                }
                else if (i == 1)
                {
                    if (n3 > 0 || n2 > 0 || n1 > 0)
                    {
                        if (n3 == 0 && n2 == 0 && n1 == 1)
                            ris += "MILIONE";
                        else
                            ris += "MILIONI";
                    }
                }
                else if (i == 2)
                {
                    if (n3 > 0 || n2 > 0 || n1 > 0)
                    {
                        if (n3 == 0 && n2 == 0 && n1 == 1)
                            ris += "MILLE";
                        else
                        {
                            if (n1 == 1 && n2 == 0)
                                ris += "UNO";
                            ris += "MILA";
                        }
                    }
                }
            }
        }
        // secondo passo: eliminazione vocali doppie e imperfezioni varie
        // UNOMILIONE
        while (true)
        {
            int ind = ris.indexOf("UNOMILIONE");
            if (ind == -1)
                break;
            else
                ris = ris.substring(0, ind + 2) + ris.substring(ind + 3);
        }
        // UNOMILIARDO
        while (true)
        {
            int ind = ris.indexOf("UNOMILIARDO");
            if (ind == -1)
                break;
            else
                ris = ris.substring(0, ind + 2) + ris.substring(ind + 3);
        }
/*
        // IUN
        while (true)
        {
            int ind = ris.indexOf("IU");
            if (ind == -1)
                break;
            else
                ris = ris.substring(0, ind) + ris.substring(ind + 1);
        }
*/
        if (valuta == 2)
            ris += "/" + vdec;
        
        
        return ris;
    }
} 