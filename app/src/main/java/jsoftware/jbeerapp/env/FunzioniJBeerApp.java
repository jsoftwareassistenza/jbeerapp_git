package jsoftware.jbeerapp.env;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.pdf.PdfDocument;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;

import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.util.Log;

import com.sewoo.jpos.command.CPCLConst;
import com.sewoo.jpos.printer.CPCLPrinter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;


public class FunzioniJBeerApp {
    public static final int KG = 0;
    public static final int LITRI_ANIDRI = 1;
    public static final int LITRI_IDRATI = 2;
    public static final int ETTOGRADI = 3;
    public static final int LITRI = 4;
    public static final int LITRI_AMBIE = 5;

    public static int estraiIntero(String s) {
        String ris = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '+'
                    || s.charAt(i) == '-'
                    || Character.getType(s.charAt(i)) == Character.DECIMAL_DIGIT_NUMBER)
                ris = ris + s.charAt(i);
        }
        if (ris.length() > 1) {
            if (ris.substring(ris.length() - 1).equals("-"))
                ris = ris.substring(ris.length() - 1)
                        + ris.substring(0, ris.length() - 1);
            else if (ris.substring(ris.length() - 1).equals("+"))
                ris = ris.substring(0, ris.length() - 1);
        }
        try {
            return (new Integer(ris)).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public static double estraiDouble(String s) {
        String ris = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ','
                    || Character.getType(s.charAt(i)) == Character.DECIMAL_DIGIT_NUMBER)
                ris = ris + s.charAt(i);

            if (s.charAt(i) == '+' || s.charAt(i) == '-')
                ris = s.charAt(i) + ris;
        }
        try {
            return (new Double(ris.replace(',', '.'))).doubleValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public static boolean controllaParametriPrimoAvvio() {
        boolean ris = true;
        Cursor cursor = Env.db
                .rawQuery(
                        "SELECT pedcod,user,pwd,serverjcloud FROM datiterm",
                        null);
        if (cursor.moveToFirst()) {
            if (cursor.getString(0).equals("")
                    || cursor.getString(1).equals("")
                    || cursor.getString(2).equals("")
                    || cursor.getString(3).equals(""))
                ris = false;
        } else
            ris = false;
        cursor.close();
        return ris;
    }

    public static boolean terminaleConfigurato() {
        boolean ris = true;
        Cursor cursor = Env.db
                .rawQuery(
                        "SELECT pedcod,agecod FROM datiterm",
                        null);
        if (cursor.moveToFirst()) {
            Log.v("jbeerapp", "depcod:" + cursor.getString(0));
            Log.v("jbeerapp", "agecod:" + cursor.getString(1));
            if (cursor.getString(0).equals("")
                    || cursor.getString(1).equals(""))
                ris = false;
        } else {
            Log.v("jbeerapp", "terminale non configurato");
            ris = false;
        }
        cursor.close();
        return ris;
    }

    public static boolean stampanteConfigurata() {
        boolean ris = true;
        if (Env.tipostampante == 0 && (Env.ssidstampante.equals("") || Env.ipstampante.equals(""))) {
            ris = false;
        } else if ((Env.tipostampante == 1 || Env.tipostampante == 2) && Env.nomebtstampante.equals("")) {
            ris = false;
        }
        return ris;
    }

    public static boolean dispositivoOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static String tipoConnessione(Context context) {
        String ris = "";
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                ris = "WIFI";
            else if (activeNetwork.getType() == ConnectivityManager.TYPE_BLUETOOTH)
                ris = "BT";
            else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                ris = "MOBILE";
            else if (activeNetwork.getType() == ConnectivityManager.TYPE_ETHERNET)
                ris = "ETH";
            else if (activeNetwork.getType() == ConnectivityManager.TYPE_VPN)
                ris = "VPN";
        } catch (Exception ex) {
            //ex.printStackTrace();
        }
        return ris;
    }

    public static String leggiSSIDConnessioneWiFi(Context context) {
        String ris = "";
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            ris = wifiInfo.getSSID().replaceAll("\"", "");
        } catch (Exception e) {
        }
        return ris;
    }

    public static String controlloStampante(CPCLPrinter cpclPrinter) {
        String result = "";
        //Log.v("jbeerapp", "avvio controllo stampante...");
        try {
            if (!(cpclPrinter.printerCheck(5000) < 0)) {
                int sts = cpclPrinter.status();
                if (sts == CPCLConst.LK_STS_CPCL_NORMAL)
                    return "Normal";
                if ((sts & CPCLConst.LK_STS_CPCL_BUSY) > 0)
                    result = result + " Busy";
                if ((sts & CPCLConst.LK_STS_CPCL_PAPER_EMPTY) > 0)
                    result = result + " Paper empty";
                if ((sts & CPCLConst.LK_STS_CPCL_COVER_OPEN) > 0)
                    result = result + " Cover open";
                if ((sts & CPCLConst.LK_STS_CPCL_BATTERY_LOW) > 0)
                    result = result + " Battery low";
            } else {
                result = "No response";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.v("jbeerapp", "controllo stampante OK");
        return result;
    }

    public static void attivaWiFi(Context context) {
        final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        final WifiConfiguration config = new WifiConfiguration();
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }
    }

    public static void disattivaWiFi(Context context) {
        final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        final WifiConfiguration config = new WifiConfiguration();
        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }
    }

    public static void connettiReteWiFi(Context context, String ssid) {
        final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        final WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + ssid + "\"";
//		conf.status = WifiConfiguration.Status.ENABLED;
//		conf.priority = 40;
//		conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
//		conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
//		conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
//		conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
//		conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
//		conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
//		conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
//		conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
//		conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
//		conf.preSharedKey = "\"951418B841\"";
        //config.SSID = ssid;
        //config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        //config.wepKeys[0] = "951418B841";
        //config.wepTxKeyIndex = 0;
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }
        //int networkId = wifiManager.addNetwork(config);
        //wifiManager.enableNetwork(networkId, true);

        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for (WifiConfiguration i : list) {
            Log.v("jbeerapp", "ssid:" + i.SSID + " networkid:" + i.networkId);
            if (i.SSID != null && i.SSID.equals("\"" + ssid + "\"")) {
                wifiManager.disconnect();
                wifiManager.enableNetwork(i.networkId, true);
                wifiManager.reconnect();
                while (wifiManager.getWifiState() != WifiManager.WIFI_STATE_ENABLED) {
                    Log.v("jbeerapp", "rete " + ssid + " in abilitazione...");
                    try {
                        Thread.currentThread().sleep(1000);
                    } catch (Exception ex) {

                    }

                }
                //break;
            }
        }
    }


    public static boolean testUtente(String user, String pwd, String termcod, String server) {
        boolean ris = false;
        try {
            String content = "";
            JSONObject req = new JSONObject();
            req.put("user", user);
            req.put("pwd", pwd);
            req.put("codpost", termcod);
            req.put("cmd", "TESTUSER");
            content = req.toString();
            BufferedReader in = FunzioniHTTP.inviaPostHttp(server + "/service/termservices.json",
                    content, "application/json", null, new Hashtable(), false, false, false, "", "", 10000);
            String sjsonobj = "";
            if (in != null) {
                String s = in.readLine();
                if (s != null)
                    sjsonobj += s;
                in.close();
            }
            if (!sjsonobj.equals("")) {
                JSONObject risp = new JSONObject(sjsonobj);
                if (risp.get("esito").equals("OK"))
                    ris = true;
                else
                    ris = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static HashMap testKey(String key) {
        HashMap ris = new HashMap();
        try {
            String content = "";
            JSONObject req = new JSONObject();
            req.put("key", key);
            if (key.contains("DM123")) {
                ris.put("versionedemo", "S");
            } else {
                ris.put("versionedemo", "N");
            }

            req.put("cmd", "TESTKEY");
            content = req.toString();
            BufferedReader in = FunzioniHTTP.inviaPostHttp(Env.ipServerJCloud + "/service/termservices.json",
                    content, "application/json", null, new Hashtable(), false, false, false, "", "", 10000);
            String sjsonobj = "";
            if (in != null) {
                String s = in.readLine();
                if (s != null)
                    sjsonobj += s;
                in.close();
            }
            if (!sjsonobj.equals("")) {
                JSONObject risp = new JSONObject(sjsonobj);
                if (risp.get("esito").equals("OK")) {
                    ris.put("esito", true);
                    ris.put("keystato", ((JSONObject) risp.get("risposta")).get("intkey"));
                    ris.put("terminale", ((JSONObject) risp.get("risposta")).get("term"));
                    ris.put("utente", ((JSONObject) risp.get("risposta")).get("utente"));
                    ris.put("password", ((JSONObject) risp.get("risposta")).get("password"));
                    ris.put("message", ((JSONObject) risp.get("risposta")).get("statokey"));
                } else {
                    ris.put("esito", false);
                    ris.put("keystato", "");
                    ris.put("message", ((JSONObject) risp.get("risposta")).get("statokey"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static HashMap aggiornaKey(String key) {
        HashMap ris = new HashMap();
        try {
            String content = "";
            JSONObject req = new JSONObject();
            req.put("key", key);
            req.put("value", 1);
            content = req.toString();
            BufferedReader in = FunzioniHTTP.inviaPostHttp(Env.ipServerJCloud + "/service/gestkeyapp.json",
//					BufferedReader in = FunzioniHTTP.inviaPostHttp("http://192.168.10.58:8080/J-Cloud/service/termservices.json",
                    content, "application/json", null, new Hashtable(), false, false, false, "", "", 10000);
            String sjsonobj = "";
            if (in != null) {
                String s = in.readLine();
                if (s != null)
                    sjsonobj += s;
                in.close();
            }
            if (!sjsonobj.equals("")) {
                JSONObject risp = new JSONObject(sjsonobj);
                if (risp.get("esito").equals("OK")) {
                    ris.put("esito", true);

                } else {
                    ris.put("esito", false);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static ArrayList<HashMap> filesDaRicevere(String user, String pwd, String termcod, String server) {
        ArrayList<HashMap> ris = new ArrayList();
        try {
            String content = "";
            JSONObject req = new JSONObject();
            req.put("user", user);
            req.put("pwd", pwd);
            req.put("codpost", termcod);
            req.put("cmd", "GETTRASFFILES");
            content = req.toString();
            BufferedReader in = FunzioniHTTP.inviaPostHttp(server + "/service/termservices.json",
                    content, "application/json", null, new Hashtable(), false, false, false, "", "", 10000);
            String sjsonobj = "";
            if (in != null) {
                String s = in.readLine();
                if (s != null)
                    sjsonobj += s;
                in.close();
            }
            if (!sjsonobj.equals("")) {
                JSONObject risp = new JSONObject(sjsonobj);
                if (risp.get("esito").equals("OK")) {
                    JSONArray risp2 = risp.getJSONArray("risposta");
                    if (risp2 != null) {
                        for (int i = 0; i < risp2.length(); i++) {
                            HashMap elem = new HashMap();
                            JSONObject jsonelem = risp2.getJSONObject(i);
                            elem.put("file", jsonelem.get("file"));
                            elem.put("postorig", jsonelem.get("postorig"));
                            elem.put("dimfile", jsonelem.get("dimfile"));
                            ris.add(elem);
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static ArrayList<HashMap> elencoBackupOnline(String user, String pwd, String termcod, String server) {
        ArrayList<HashMap> ris = new ArrayList();
        try {
            String content = "";
            JSONObject req = new JSONObject();
            req.put("user", user);
            req.put("pwd", pwd);
            req.put("proc", "jbeerappDB_" + termcod);
            content = req.toString();
            BufferedReader in = FunzioniHTTP.inviaPostHttp(server + "/service/getbackupfiles.json",
                    content, "application/json", null, new Hashtable(), false, false, false, "", "", 10000);
            String sjsonobj = "";
            if (in != null) {
                String s = in.readLine();
                if (s != null)
                    sjsonobj += s;
                in.close();
            }
            if (!sjsonobj.equals("")) {
                JSONObject risp = new JSONObject(sjsonobj);
                if (risp.get("esito").equals("OK")) {
                    JSONArray risp2 = risp.getJSONArray("backups");
                    if (risp2 != null) {
                        for (int i = 0; i < risp2.length(); i++) {
                            HashMap elem = new HashMap();
                            JSONObject jsonelem = risp2.getJSONObject(i);
                            elem.put("nomefile", jsonelem.get("nomefile"));
                            elem.put("dataarrivo", jsonelem.get("dataarrivo"));
                            elem.put("oraarrivo", jsonelem.get("oraarrivo"));
                            elem.put("dimbytes", jsonelem.get("dimBytes"));
                            ris.add(elem);
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }


    public static void compattaDatabase() {
        Env.db.execSQL("VACUUM");
    }

    public static void impostaProprieta(String prop, String val) {
        SQLiteStatement stmt = Env.db.compileStatement("DELETE FROM proprieta WHERE pcod=?");
        stmt.clearBindings();
        stmt.bindString(1, prop);
        stmt.execute();
        stmt = Env.db.compileStatement("INSERT INTO proprieta (pcod,pval) VALUES (?,?)");
        stmt.clearBindings();
        stmt.bindString(1, prop);
        stmt.bindString(2, val);
        stmt.execute();
        stmt.close();
    }

    public static String leggiProprieta(String prop) {
        String ris = "";
        Cursor cursor = Env.db.rawQuery("SELECT pval FROM proprieta WHERE pcod = '" + prop + "'", null);
        if (cursor.moveToFirst())
            ris = cursor.getString(0);
        cursor.close();
        return ris;
    }

    public static double leggiGiacenzaArticolo(String artcod) {
        double ris = 0;
        String[] pars = new String[1];
        pars[0] = artcod;
        Cursor cursor = Env.db.rawQuery("SELECT artgiacenza FROM articoli WHERE artcod = ?", pars);
        if (cursor.moveToFirst())
            ris = OpValute.arrotondaMat(cursor.getDouble(0), 3);
        cursor.close();
        return ris;
    }

    public static double leggiGiacenzaLotto(String lotcod, String artcod) {
        double ris = 0;
        String[] pars = new String[2];
        pars[0] = lotcod;
        pars[1] = artcod;
        Cursor cursor = Env.db.rawQuery("SELECT lotgiacenza FROM lotti WHERE lotcod=? AND lotartcod=?", pars);
        if (cursor.moveToFirst())
            ris = OpValute.arrotondaMat(cursor.getDouble(0), 3);
        cursor.close();
        return ris;
    }

    public static HashMap ultimaRicezioneDati() {
        HashMap ris = new HashMap();
        String dtric = leggiProprieta("ultimaric_data");
        String oraric = leggiProprieta("ultimaric_ora");
        ris.put("data", dtric);
        ris.put("ora", oraric);
        return ris;
    }

    public static HashMap ultimoInvioDati() {
        HashMap ris = new HashMap();
        String dtinv = leggiProprieta("ultimoinv_data");
        String orainv = leggiProprieta("ultimoinv_ora");
        ris.put("data", dtinv);
        ris.put("ora", orainv);
        return ris;
    }

    public static double aggiornaGiacenzaLotto(String lotcod, String artcod, double vargiac) {
        double ris = 0;
        // legge giacenza precedente
        double precgiac = 0;
        String[] pars = new String[2];
        pars[0] = lotcod;
        pars[1] = artcod;
        Cursor cl = Env.db.rawQuery("SELECT lotgiacenza FROM lotti WHERE lotcod = ? AND lotartcod = ?", pars);
        if (cl.moveToFirst())
            precgiac = cl.getDouble(0);
        cl.close();
        // aggiornamento lotto
        ris = OpValute.arrotondaMat(precgiac + vargiac, 3);
        SQLiteStatement stdl = Env.db.compileStatement("DELETE FROM lotti WHERE lotcod = ? AND lotartcod = ?");
        stdl.clearBindings();
        stdl.bindString(1, lotcod);
        stdl.bindString(2, artcod);
        stdl.execute();
        stdl.close();
        SQLiteStatement stl = Env.db.compileStatement("INSERT INTO lotti (lotcod,lotartcod,lotgiacenza) VALUES (?,?,?)");
        stl.clearBindings();
        stl.bindString(1, lotcod);
        stl.bindString(2, artcod);
        stl.bindDouble(3, ris);
        stl.execute();
        stl.close();
        return ris;
    }

    public static void impostaGiacenzaLotto(String lotcod, String artcod, double giac) {
        SQLiteStatement stdl = Env.db.compileStatement("DELETE FROM lotti WHERE lotcod = ? AND lotartcod = ?");
        stdl.clearBindings();
        stdl.bindString(1, lotcod);
        stdl.bindString(2, artcod);
        stdl.execute();
        stdl.close();
        SQLiteStatement stl = Env.db.compileStatement("INSERT INTO lotti (lotcod,lotartcod,lotgiacenza) VALUES (?,?,?)");
        stl.clearBindings();
        stl.bindString(1, lotcod);
        stl.bindString(2, artcod);
        stl.bindDouble(3, OpValute.arrotondaMat(giac, 3));
        stl.execute();
        stl.close();
    }

    public static void impostaGiacenzaArticolo(String artcod, double giac) {
        SQLiteStatement sta = Env.db.compileStatement("UPDATE articoli SET artgiacenza=? WHERE artcod=?");
        sta.clearBindings();
        sta.bindDouble(1, OpValute.arrotondaMat(giac, 3));
        sta.bindString(2, artcod);
        sta.execute();
        sta.close();
    }

    public static void allineaGiacenzaLottiArticolo(String artcod) {
        double g = leggiGiacenzaArticolo(artcod);
        double gl = 0;
        SQLiteStatement stdl = Env.db.compileStatement("DELETE FROM lotti WHERE lotartcod = ? AND lotgiacenza < 0");
        stdl.clearBindings();
        stdl.bindString(1, artcod);
        stdl.execute();
        stdl.close();
        Cursor ccl = Env.db.rawQuery(
                "SELECT SUM(lotgiacenza) FROM lotti WHERE lotartcod = '" + artcod + "'", null);
        if (ccl.moveToNext() && !ccl.isNull(0)) {
            gl = ccl.getDouble(0);
        }
        ccl.close();
        if (gl != g) {
            double diff = OpValute.arrotondaMat(g - gl, 3);
            if (diff > 0) {
                // giacenza art. > totale lotti -> aggiunge differenza al primo lotto
                String[] parsl = new String[1];
                parsl[0] = artcod;
                Cursor cl = Env.db.rawQuery(
                        "SELECT lotcod,lotgiacenza FROM lotti LEFT JOIN datelotti ON (lotartcod = dlartcod AND lotcod = dllotcod) WHERE lotartcod=? AND lotgiacenza <> 0 ORDER BY dldata ASC,lotgiacenza DESC,lotcod ASC", parsl);
                if (cl.moveToNext()) {
                    FunzioniJBeerApp.aggiornaGiacenzaLotto(cl.getString(0), artcod, diff);
                }
                cl.close();
            } else {
                // giac.art. < totale lotti -> scala da lotti partendo dal più vecchio
                diff = Math.abs(diff);
                String[] parsl = new String[1];
                parsl[0] = artcod;
                Cursor cl = Env.db.rawQuery(
                        "SELECT lotcod,lotgiacenza FROM lotti LEFT JOIN datelotti ON (lotartcod = dlartcod AND lotcod = dllotcod) WHERE lotartcod=? AND lotgiacenza <> 0 ORDER BY dldata ASC,lotgiacenza DESC,lotcod ASC", parsl);
                while (cl.moveToNext()) {
                    System.out.println("lotto:" + cl.getString(0) + " giac:" + cl.getDouble(1));
                    if (diff > 0) {
                        if (cl.getDouble(1) >= diff) {
                            FunzioniJBeerApp.impostaGiacenzaLotto(cl.getString(0), artcod, OpValute.arrotondaMat(cl.getDouble(1) - diff, 3));
                            double tmp = FunzioniJBeerApp.leggiGiacenzaLotto(cl.getString(0), artcod);
                            System.out.println("lotto:" + cl.getString(0) + " nuova giac:" + tmp);
                            diff = 0;
                        } else {
                            stdl = Env.db.compileStatement("DELETE FROM lotti WHERE lotartcod = ? AND lotcod = ?");
                            stdl.clearBindings();
                            stdl.bindString(1, artcod);
                            stdl.bindString(2, cl.getString(0));
                            stdl.execute();
                            stdl.close();
                            diff = OpValute.arrotondaMat(diff - cl.getDouble(1), 3);
                        }
                    }
                }
                cl.close();
                if (diff > 0) {
                    cl = Env.db.rawQuery(
                            "SELECT lotcod,lotgiacenza FROM lotti LEFT JOIN datelotti ON (lotartcod = dlartcod AND lotcod = dllotcod) WHERE lotartcod=? AND lotgiacenza <> 0 ORDER BY dldata ASC,lotgiacenza DESC,lotcod ASC", parsl);
                    while (cl.moveToNext()) {
                        if (diff > 0) {
                            if (cl.getDouble(1) >= diff) {
                                diff = 0;
                                FunzioniJBeerApp.impostaGiacenzaLotto(cl.getString(0), artcod, OpValute.arrotondaMat(cl.getDouble(1) - diff, 3));
                            } else {
                                stdl = Env.db.compileStatement("DELETE FROM lotti WHERE lotartcod = ? AND lotcod = ?");
                                stdl.clearBindings();
                                stdl.bindString(1, artcod);
                                stdl.bindString(2, cl.getString(0));
                                stdl.execute();
                                stdl.close();
                                diff = OpValute.arrotondaMat(diff - cl.getDouble(1), 3);
                            }
                        }
                    }
                    cl.close();
                }
            }
        }
    }

    public static void caricaCodIva() {
        Env.hcodiva = new HashMap();
        Cursor c = Env.db.rawQuery("SELECT ivacod,ivadescr,ivagruppo,ivaaliq,ivapercind,ivagest,ivacodivagest,ivaomaggiotot FROM codiva", null);
        while (c.moveToNext()) {
            Record rec = new Record();
            rec.insStringa("ivacod", c.getString(0));
            rec.insStringa("ivadescr", c.getString(1));
            rec.insIntero("ivagruppo", c.getInt(2));
            rec.insDouble("ivaaliq", c.getDouble(3));
            rec.insDouble("ivapercind", c.getDouble(4));
            rec.insIntero("ivagest", c.getInt(5));
            rec.insStringa("ivacodivagest", c.getString(6));
            rec.insStringa("ivaomaggiotot", c.getString(7));
            Env.hcodiva.put(c.getString(0), rec);
        }
        c.close();
    }

    public static void caricaCodPag() {
        Env.hcodpag = new HashMap();
        Cursor c = Env.db.rawQuery(
                "SELECT pagcod,pagdescr,pagtipo,pagspeseinc,pagcodivaspeseinc,pagnrate,pagbancacod,pagbancadescr,pagabi,pagagenzia," +
                        "pagcab,pagtipocalc,paggg1,paggg2,paggg3,paggg4,paggg5,paggg6 " +
                        "FROM pagamenti", null);
        while (c.moveToNext()) {
            Record rec = new Record();
            rec.insStringa("pagcod", c.getString(0));
            rec.insStringa("pagdescr", c.getString(1));
            rec.insIntero("pagtipo", c.getInt(2));
            rec.insDouble("pagspeseinc", c.getDouble(3));
            rec.insStringa("pagcodivaspeseinc", c.getString(4));
            rec.insIntero("pagnrate", c.getInt(5));
            rec.insStringa("pagbancacod", c.getString(6));
            rec.insStringa("pagbancadescr", c.getString(7));
            rec.insStringa("pagabi", c.getString(8));
            rec.insStringa("pagagenzia", c.getString(9));
            rec.insStringa("pagcab", c.getString(10));
            rec.insStringa("pagtipocalc", c.getString(11));
            rec.insIntero("paggg1", c.getInt(12));
            rec.insIntero("paggg2", c.getInt(13));
            rec.insIntero("paggg3", c.getInt(14));
            rec.insIntero("paggg4", c.getInt(15));
            rec.insIntero("paggg5", c.getInt(16));
            rec.insIntero("paggg6", c.getInt(17));
            Env.hcodpag.put(c.getString(0), rec);
        }
        c.close();
    }

    public static void caricaBlocchiCau() {
        int ind = 0;
        Env.hblocchicau = new HashMap();
        Cursor c = Env.db.rawQuery(
                "SELECT causale,agecod,clicod,artcod,gita,blocco,destcod FROM blocchicau ORDER BY causale,agecod,clicod,artcod,gita,blocco,destcod", null);
        while (c.moveToNext()) {
            Record rec = new Record();
            rec.insIntero("causale", c.getInt(0));
            rec.insStringa("agecod", c.getString(1));
            rec.insStringa("clicod", c.getString(2));
            rec.insStringa("artcod", c.getString(3));
            rec.insStringa("gita", c.getString(4));
            rec.insStringa("blocco", c.getString(5));
            rec.insStringa("destcod", c.getString(6));
            Env.hblocchicau.put(("" + ind), rec);
            ind++;
        }
        c.close();
    }

    public static void cancellaBlocchiClienteFineGiornata() {
        Env.db.execSQL("DELETE FROM blocchicli_fineg");
    }

    public static void salvaLog(String descrop) {
        try {
            if (descrop.length() > 255)
                descrop = descrop.substring(0, 255);
            SQLiteStatement stlog = Env.db.compileStatement(
                    "INSERT INTO logop (logdata,logora,logdescr) VALUES (? , ? , ?)");
            stlog.bindString(1, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            stlog.bindString(2, ora);
            stlog.bindString(3, descrop);
            stlog.execute();
            stlog.close();
        } catch (Exception e) {
        }
    }

    public static void puliziaLog() {
        try {
            Data dtoggi = new Data();
            dtoggi.decrementaGiorni(10);
            Env.db.execSQL("DELETE FROM logop WHERE logdata < '" + dtoggi.formatta(Data.AAAA_MM_GG, "-") + "'");
        } catch (Exception e) {
        }
    }

    public static void initParametri() {
        boolean datipres = false;
        Cursor cursor = Env.db.rawQuery(
                "SELECT pedcod FROM datiterm", null);
        if (cursor.moveToFirst())
            datipres = true;
        cursor.close();
        Log.v("jbeerapp", "datipres:" + datipres);
        if (!datipres) {
            SQLiteStatement st = Env.db.compileStatement(
                    "insert into datiterm (numterm,depcod,pedcod,agecod,agenome,targamezzo,depsede," +
                            "docbollaval,sezbollaval,numbollaval,docbollaqta,sezbollaqta,numbollaqta," +
                            "docfattura,sezfattura,numfattura,doccorrisp,sezcorrisp,numcorrisp," +
                            "docddtcarico,sezddtcarico,numddtcarico," +
                            "docscaricosede,sezscaricosede,numscaricosede," +
                            "doctrasfamezzo,seztrasfamezzo,numtrasfamezzo," +
                            "doctrasfdamezzo,seztrasfdamezzo,numtrasfdamezzo," +
                            "docddtreso,sezddtreso,numddtreso," +
                            "docordinesede,sezordinesede,numordinesede," +
                            "docrimfs,sezrimfs," +
                            "cmvend,cmresov,cmresonv,cmsost,cmomaggi,cmsm,cmscaduto," +
                            "cmddtcarico,cmscaricosede,cmtrasfamezzo,cmtrasfdamezzo,cmdistrmerce,cmordinesede,cmrprom," +
                            "codivaomaggiimp20,codivaomaggiimp10,codivaomaggiimp04," +
                            "codivaomaggitot20,codivaomaggitot10,codivaomaggitot04," +
                            "gvcod,clinuovocod,clinuovolist,clinuovoposprz,clinuovopag,cliordine,tipoddtcar," +
                            "gprs_indserver,gprs_postserver,autcod,autdescr,auttarga,numordinecli) " +
                            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); // 72
            st.clearBindings();
            st.bindLong(1, 0);
            st.bindString(2, "");
            st.bindString(3, "");
            st.bindString(4, "");
            st.bindString(5, "");
            st.bindString(6, "");
            st.bindString(7, "");
            st.bindString(8, "");
            st.bindString(9, "");
            st.bindLong(10, 1); //numboll
            st.bindString(11, "");
            st.bindString(12, "");
            st.bindLong(13, 1); //numbollqta
            st.bindString(14, "");
            st.bindString(15, "");
            st.bindLong(16, 1); //numfattura
            st.bindString(17, "");
            st.bindString(18, "");
            st.bindLong(19, 1); //numcorris
            st.bindString(20, "");
            st.bindString(21, "");
            st.bindLong(22, 1); //numddtcarico
            st.bindString(23, "");
            st.bindString(24, "");
            st.bindLong(25, 1); //numscaricosede
            st.bindString(26, "");
            st.bindString(27, "");
            st.bindLong(28, 1); //numtrasfamezzo
            st.bindString(29, "");
            st.bindString(30, "");
            st.bindLong(31, 1); //numddtreso
            st.bindString(32, "");
            st.bindString(33, "");
            st.bindLong(34, 1); //numordinesede
            st.bindString(35, "");
            st.bindString(36, "");
            st.bindLong(37, 1);
            st.bindString(38, ""); // rim.fine sessione
            st.bindString(39, "");

            st.bindString(40, ""); // causali
            st.bindString(41, "");
            st.bindString(42, "");
            st.bindString(43, "");
            st.bindString(44, "");
            st.bindString(45, "");
            st.bindString(46, "");
            st.bindString(47, "");
            st.bindString(48, "");
            st.bindString(49, "");
            st.bindString(50, "");
            st.bindString(51, "");
            st.bindString(52, "");
            st.bindString(53, "");

            st.bindString(54, ""); // cod.iva omaggi
            st.bindString(55, "");
            st.bindString(56, "");
            st.bindString(57, "");
            st.bindString(58, "");
            st.bindString(59, "");

            st.bindString(60, ""); // gvcod

            st.bindString(61, ""); // cliente nuovo
            st.bindString(62, "");
            st.bindLong(63, 1);
            st.bindString(64, "");

            st.bindString(65, ""); // cliente ordine

            st.bindLong(66, 1); // tipo ddt car

            st.bindString(67, ""); // gprs
            st.bindString(68, "sede"); // gprs

            st.bindString(69, ""); // automezzo
            st.bindString(70, "");
            st.bindString(71, "");
            st.bindLong(72, 1); //numordinecli
            st.execute();
            st.close();
        }
    }

    public static void caricaParametri() {
        Cursor cursor = Env.db.rawQuery(
                "SELECT numterm,depcod,pedcod,agecod,agenome," +
                        "targamezzo,docbollaval,sezbollaval,numbollaval,docbollaqta,sezbollaqta,numbollaqta," +
                        "docfattura,sezfattura,numfattura,doccorrisp,sezcorrisp,numcorrisp," +
                        "docddtreso,sezddtreso,numddtreso,docddtcarico,sezddtcarico,numddtcarico," +
                        "docscaricosede,sezscaricosede,numscaricosede,doctrasfamezzo,seztrasfamezzo,numtrasfamezzo," +
                        "doctrasfdamezzo,seztrasfdamezzo,numtrasfdamezzo," +
                        "docordinesede,sezordinesede,numordinesede," +
                        "docrimfs,sezrimfs," +
                        "cmvend,cmresov,cmresonv,cmsost,cmomaggi,cmsm,cmddtcarico,cmscaricosede,cmtrasfamezzo,cmtrasfdamezzo," +
                        "cmdistrmerce,cmordinesede,cmscaduto," +
                        "depsede," +
                        "codivaomaggiimp20,codivaomaggiimp10,codivaomaggiimp04,codivaomaggitot20,codivaomaggitot10,codivaomaggitot04," +
                        "gvcod,clinuovocod,clinuovolist,clinuovoposprz,clinuovopag,cliordine,tipoddtcar," +
                        "gprs_indserver,gprs_postserver,autcod,autdescr,auttarga," +
                        "ssidstampante,ipstampante,tiposcambiodati,ssidscambiodati,ipscambiodati,tiponuovicli,tipogestlotti," +
                        "intazriga1,intazriga2,intazriga3,intazriga4,intazriga5,visprzrif,ctrprzrif,bloccosconti,obbligoincpagtv,pwdcambiovend,tipoannullamentodoc," +
                        "nscadblocco,ggtollblocco,impblocco,nrighelogo,stampasedesec,stampaddtcar_classi,stampaord,lottineg,listbase,datainizioblocco,tipostampante," +
                        "nomebtstampante,intazriga6,intazriga7,trasmdopofg,inviotrasfvsmezzo,noxeterm,ddtstampatimbro,ddtdatiage,numinv,noscarsede,notrasfvs,notrasfda," +
                        "noanndoc,azioneblocco,vendqtatot,prezzopref,noriccarichi,ddtstamparaggrlotti,ddtstampascop,ddtqtanocess,finegstampadatiart,finegstampaelencodoc," +
                        "backupauto,tipobackupauto,docordinecli,sezordinecli,numordinecli,stampascasede,stampatrasfvs,stampatrasfda,scasedenovalvend,scasedenovalrot," +
                        "scasedenovalscad,stampaordtotali,maxscvend,visprzacq,incattivanote,incsceglicassa,noordsede,nosostlotti,nogestlist,nocambiovend,nocambiogirovisita,noprztrasp,noinv," +
                        "nomenustampe,incnostampa,tipoterm,depgiac,sceltaemail,emailfirmariga1,emailfirmariga2,emailfirmariga3,abbuoniattivi,backupperiodico,calcaccisa,tipocalcaccisa,voceaccisa,dispart," +
                        "pwdanndoc,pwdpar,passcodetrasf,copieddt,copieddtqta,finegstampasospdainc,noannstampe,colli1," +
                        "usalistinonuovo,cmrprom,finegdistintadenaro,pwdutilita,invioordineauto,ordinicligiacterm,nostampafinegiornata,orddepsede,gestlotti,moddoc" +
                        " FROM datiterm", null);
        if (cursor.moveToFirst()) {
            Env.numterm = cursor.getInt(0);
            Env.depcod = cursor.getString(1);
            Env.pedcod = cursor.getString(2);
            Env.agecod = cursor.getString(3);
            Env.agenome = cursor.getString(4);
            Env.targamezzo = cursor.getString(5);
            Env.docbollaval = cursor.getString(6);
            Env.sezbollaval = cursor.getString(7);
            Env.numbollaval = cursor.getInt(8);
            Env.docbollaqta = cursor.getString(9);
            Env.sezbollaqta = cursor.getString(10);
            Env.numbollaqta = cursor.getInt(11);
            Env.docfattura = cursor.getString(12);
            Env.sezfattura = cursor.getString(13);
            Env.numfattura = cursor.getInt(14);
            Env.doccorrisp = cursor.getString(15);
            Env.sezcorrisp = cursor.getString(16);
            Env.numcorrisp = cursor.getInt(17);
            Env.docddtreso = cursor.getString(18);
            Env.sezddtreso = cursor.getString(19);
            Env.numddtreso = cursor.getInt(20);
            Env.docddtcarico = cursor.getString(21);
            Env.sezddtcarico = cursor.getString(22);
            Env.numddtcarico = cursor.getInt(23);
            Env.docscaricosede = cursor.getString(24);
            Env.sezscaricosede = cursor.getString(25);
            Env.numscaricosede = cursor.getInt(26);
            Env.doctrasfamezzo = cursor.getString(27);
            Env.seztrasfamezzo = cursor.getString(28);
            Env.numtrasfamezzo = cursor.getInt(29);
            Env.doctrasfdamezzo = cursor.getString(30);
            Env.seztrasfdamezzo = cursor.getString(31);
            Env.numtrasfdamezzo = cursor.getInt(32);
            Env.docordinesede = cursor.getString(33);
            Env.sezordinesede = cursor.getString(34);
            Env.numordinesede = cursor.getInt(35);
            Env.docrimfs = cursor.getString(36);
            Env.sezrimfs = cursor.getString(37);
            Env.cmvend = cursor.getString(38);
            Env.cmresov = cursor.getString(39);
            Env.cmresonv = cursor.getString(40);
            Env.cmann = cursor.getString(41);
            Env.cmomaggi = cursor.getString(42);
            Env.cmsm = cursor.getString(43);
            Env.cmddtcarico = cursor.getString(44);
            Env.cmscaricosede = cursor.getString(45);
            Env.cmtrasfamezzo = cursor.getString(46);
            Env.cmtrasfdamezzo = cursor.getString(47);
            Env.cmdistrmerce = cursor.getString(48);
            Env.cmordinesede = cursor.getString(49);
            Env.cmscaduto = cursor.getString(50);
            Env.depsede = cursor.getString(51);
            Env.codivaomaggiimp20 = cursor.getString(52);
            Env.codivaomaggiimp10 = cursor.getString(53);
            Env.codivaomaggiimp04 = cursor.getString(54);
            Env.codivaomaggitot20 = cursor.getString(55);
            Env.codivaomaggitot10 = cursor.getString(56);
            Env.codivaomaggitot04 = cursor.getString(57);
            Env.gvcod = cursor.getString(58);
            Env.clinuovocod = cursor.getString(59);
            Env.clinuovolist = cursor.getString(60);
            Env.clinuovoposprz = cursor.getInt(61);
            Env.clinuovopag = cursor.getString(62);
            Env.cliordine = cursor.getString(63);
            Env.tipoddtcar = cursor.getInt(64);
            Env.gprs_indserver = cursor.getString(65);
            Env.gprs_postserver = cursor.getString(66);
            Env.autcod = cursor.getString(67);
            Env.autdescr = cursor.getString(68);
            Env.auttarga = cursor.getString(69);
            Env.ssidstampante = cursor.getString(70);
            Env.ipstampante = cursor.getString(71);
            Env.tiposcambiodati = cursor.getInt(72);
            Env.ssidscambiodati = cursor.getString(73);
            Env.ipscambiodati = cursor.getString(74);
            Env.tiponuovicli = cursor.getInt(75);
            Env.tipogestlotti = cursor.getInt(76);
            Env.intazriga1 = cursor.getString(77);
            Env.intazriga2 = cursor.getString(78);
            Env.intazriga3 = cursor.getString(79);
            Env.intazriga4 = cursor.getString(80);
            Env.intazriga5 = cursor.getString(81);
            Env.visprzrif = (cursor.getString(82).equals("S"));
            Env.ctrprzrif = (cursor.getString(83).equals("S"));
            Env.bloccosconti = (cursor.getString(84).equals("S"));
            Env.obbligoincpagtv = (cursor.getString(85).equals("S"));
            Env.pwdcambiovend = (cursor.getString(86).equals("S"));
            Env.tipoannullamentodoc = cursor.getInt(87);
            Env.nscadblocco = cursor.getInt(88);
            Env.ggtollblocco = cursor.getInt(89);
            Env.impblocco = cursor.getInt(90);
            Env.nrighelogo = cursor.getInt(91);
            Env.stampasedesec = (cursor.getString(92).equals("S"));
            Env.stampaddtcar_classi = (cursor.getString(93).equals("S"));
            Env.stampaord = (cursor.getString(94).equals("S"));
            Env.lottineg = (cursor.getString(95).equals("S"));
            Env.listbase = cursor.getString(96);
            Env.datainizioblocco = cursor.getString(97);
            Env.tipostampante = cursor.getInt(98);
            Env.nomebtstampante = cursor.getString(99);
            Env.intazriga6 = cursor.getString(100);
            Env.intazriga7 = cursor.getString(101);
            Env.inviodopofineg = (cursor.getString(102).equals("S"));
            Env.inviotrasfvsmezzo = (cursor.getString(103).equals("S"));
            Env.noxeterm = (cursor.getString(104).equals("S"));
            Env.ddtstampatimbro = (cursor.getString(105).equals("S"));
            Env.ddtdatiage = cursor.getInt(106);
            Env.numinv = cursor.getInt(107);
            Env.noscarsede = (cursor.getString(108).equals("S"));
            Env.notrasfvs = (cursor.getString(109).equals("S"));
            Env.notrasfda = (cursor.getString(110).equals("S"));
            Env.noanndoc = (cursor.getString(111).equals("S"));
            Env.azioneblocco = cursor.getInt(112);
            Env.vendqtatot = (cursor.getString(113).equals("S"));
            Env.prezzopref = (cursor.getString(114).equals("S"));
            Env.noriccarichi = (!cursor.isNull(115) && cursor.getString(115).equals("S"));
            Env.ddtstamparaggrlotti = (cursor.getString(116).equals("S"));
            Env.ddtstampascop = (cursor.getString(117).equals("S"));
            Env.ddtqtanocess = (cursor.getString(118).equals("S"));
            Env.finegstampadatiart = (cursor.getString(119).equals("S"));
            Env.finegstampaelencodoc = (cursor.getString(120).equals("S"));
            Env.backupauto = (cursor.getString(121).equals("S"));
            Env.tipobackupauto = cursor.getInt(122);
            Env.docordinecli = cursor.getString(123);
            Env.sezordinecli = cursor.getString(124);
            Env.numordinecli = cursor.getInt(125);
            Env.stampascasede = (cursor.getString(126).equals("S"));
            Env.stampatrasfvs = (cursor.getString(127).equals("S"));
            Env.stampatrasfda = (cursor.getString(128).equals("S"));
            Env.scasedenovalvend = (cursor.getString(129).equals("S"));
            Env.scasedenovalrot = (cursor.getString(130).equals("S"));
            Env.scasedenovalscad = (cursor.getString(131).equals("S"));
            Env.stampaordtotali = (cursor.getString(132).equals("S"));
            Env.maxscvend = cursor.getDouble(133);
            Env.visprzacq = (cursor.getString(134).equals("S"));
            Env.incattivanote = (cursor.getString(135).equals("S"));
            Env.incsceglicassa = (cursor.getString(136).equals("S"));
            Env.noordsede = (cursor.getString(137).equals("S"));
            Env.nosostlotti = (cursor.getString(138).equals("S"));
            Env.nogestlist = (cursor.getString(139).equals("S"));
            Env.nocambiovend = (cursor.getString(140).equals("S"));
            Env.nocambiogirovisita = (cursor.getString(141).equals("S"));
            Env.noprztrasp = (cursor.getString(142).equals("S"));
            Env.noinv = (cursor.getString(143).equals("S"));
            Env.nomenustampe = (cursor.getString(144).equals("S"));
            Env.incnostampa = (cursor.getString(145).equals("S"));
            Env.tipoterm = cursor.getInt(146);
            Env.depgiac = cursor.getString(147);
            Env.sceltaemail = (cursor.getString(148).equals("S"));
            Env.emailfirmariga1 = cursor.getString(149);
            Env.emailfirmariga2 = cursor.getString(150);
            Env.emailfirmariga3 = cursor.getString(151);
            Env.abbuoniattivi = (cursor.getString(152).equals("S"));
            Env.backupperiodico = cursor.getInt(153);
            Env.calcaccisa = (cursor.getString(154).equals("S"));
            Env.tipocalcaccisa = cursor.getInt(155);
            Env.voceaccisa = cursor.getString(156);
            Env.dispart = (cursor.getString(157).equals("S"));
            Env.pwdanndoc = (cursor.getString(158).equals("S"));
            Env.pwdpar = (cursor.getString(159).equals("S"));
            Env.passcodetrasf = (cursor.getString(160).equals("S"));
            Env.copieddt = cursor.getInt(161);
            Env.copieddtqta = cursor.getInt(162);
            Env.finegstampasospdainc = (cursor.getString(163).equals("S"));
            Env.noannstampe = (cursor.getString(164).equals("S"));
            Env.colli1 = (cursor.getString(165).equals("S"));
            Env.usalistinonuovo = (cursor.getString(166).equals("S"));
            Env.cmddtqta = cursor.getString(167);
            Env.finegdistintadenaro = (cursor.getString(168).equals("S"));
            Env.pwdutilita = (cursor.getString(169).equals("S"));
            Env.invioordineauto = (cursor.getString(170).equals("S"));
            Env.ordinicligiacterm = (cursor.getString(171).equals("S"));
            Env.nostampafinegiornata = (cursor.getString(172).equals("S"));
            Env.orddepsede = (cursor.getString(173).equals("S"));
            Env.gestlotti = cursor.getInt(174) == 0 ? false : true;
            Env.moddoc = (cursor.getString(175).equals("S"));
            Env.codartnum = (leggiProprieta("codartnum").equals("S"));
            Env.ricsolodest = (leggiProprieta("ricsolodest").equals("S"));
            Env.ricdest2 = (leggiProprieta("ricdest2").equals("S"));
            Env.nostampadocvend = (leggiProprieta("nostampadocvend").equals("S"));
            Env.nostampaddtcarico = (leggiProprieta("nostampaddtcarico").equals("S"));
            Env.termordini = (leggiProprieta("termordini").equals("S"));
            Env.ordinericart = Formattazione.estraiIntero(leggiProprieta("ordinericart"));
            Env.ddtcarlottosede = (leggiProprieta("ddtcarlottosede").equals("S"));
            Env.anndocnoagggiac = (leggiProprieta("anndocnoagggiac").equals("S"));
            Env.ordforeditqta = (leggiProprieta("ordforeditqta").equals("S"));

        }
        cursor.close();
    }

    public static boolean causaleBloccata(int causale, String gita, String agecod, String clicod, String artcod, String destcod) {
        boolean bloccata = false;
        if (causale == 0 || causale == 1 || causale == 2 || causale == 7) {
            bloccata = false;
            // vendita,reso,rientro da prom.: default libera controllo blocco
            Iterator it = Env.hblocchicau.keySet().iterator();
            while (it.hasNext() && !bloccata) {
                String k = (String) it.next();
                Record rec = Env.hblocchicau.get(k);
                if (rec.leggiIntero("causale") == causale) {
                    String agecod2 = rec.leggiStringa("agecod");
                    String clicod2 = rec.leggiStringa("clicod");
                    String artcod2 = rec.leggiStringa("artcod");
                    String gita2 = rec.leggiStringa("gita");
                    String destcod2 = rec.leggiStringa("destcod");
                    if (agecod2.equals("") && clicod2.equals("") && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals("") && gita2.equals("") && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals("") && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals("") && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(""))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals("") && gita2.equals("") && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals("") && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals("") && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals("") && gita2.equals("") && destcod2.equals(destcod))
                        bloccata = true;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals("") && gita2.equals("") && destcod2.equals(""))
                        bloccata = true;
                }
            }
        } else {
            // sconto merce, omaggio, promozione cessionari: default bloccate controllo sblocco
            bloccata = true;
            Iterator it = Env.hblocchicau.keySet().iterator();
            while (it.hasNext() && bloccata) {
                String k = (String) it.next();
                Record rec = Env.hblocchicau.get(k);
                if (rec.leggiIntero("causale") == causale) {
                    String agecod2 = rec.leggiStringa("agecod");
                    String clicod2 = rec.leggiStringa("clicod");
                    String artcod2 = rec.leggiStringa("artcod");
                    String gita2 = rec.leggiStringa("gita");
                    String destcod2 = rec.leggiStringa("destcod");
                    if (agecod2.equals("") && clicod2.equals("") && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals("") && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals("") && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals("") && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals("") && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = false;
                    else if (agecod2.equals(agecod) && clicod2.equals(clicod) && artcod2.equals(artcod) && gita2.equals(gita) && destcod2.equals(destcod))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals("") && gita2.equals("") && destcod2.equals(destcod))
                        bloccata = false;
                    else if (agecod2.equals("") && clicod2.equals("") && artcod2.equals("") && gita2.equals("") && destcod2.equals(""))
                        bloccata = false;
                }
            }
        }
        return bloccata;
    }

    public static boolean clienteContanti(String cc) {
        boolean ris = false;
        try {
            String[] pars = new String[1];
            pars[0] = cc;
            Cursor cursor = Env.db.rawQuery("SELECT clicodpag FROM clienti WHERE clicodice = ?", pars);
            cursor.moveToFirst();
            String codpag = cursor.getString(0);
            cursor.close();
            if (!codpag.equals("")) {
                pars = new String[1];
                pars[0] = codpag;
                cursor = Env.db.rawQuery("SELECT pagtipo FROM pagamenti WHERE pagcod = ?", pars);
                cursor.moveToFirst();
                cursor.moveToFirst();
                if (cursor.getInt(0) == 8)
                    ris = true;
                cursor.close();
            }
        } catch (Exception e) {
        }
        return ris;
    }

    public static boolean DDTScopertiMesePrecedente(String clicod, Data dtdoc) {
        int num = 0;
        try {
            int mese = dtdoc.mese();
            int anno = dtdoc.anno();
            mese--;
            if (mese == 0) {
                mese = 12;
                anno--;
            }
            String dtctrinizio = (new Data(1, mese, anno)).formatta(Data.AAAA_MM_GG, "-");
            String dtctrfine = funzStringa.riempiStringa("" + anno, 4, funzStringa.DX, '0') + "-" +
                    funzStringa.riempiStringa("" + mese, 2, funzStringa.DX, '0') + "-31";
            String[] pars = new String[3];
            pars[0] = clicod;
            pars[1] = dtctrinizio;
            pars[2] = dtctrfine;
            Cursor cursor = Env.db.rawQuery(
                    "SELECT COUNT(*) AS tot FROM scoperti WHERE scclicod = ? AND scdatadoc >= ? AND scdatadoc <= ? AND sctipo = 'B' AND scresiduoscad <> 0", pars);
            if (cursor.moveToFirst() && !cursor.isNull(0))
                num = cursor.getInt(0);
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (num > 0);
    }

    public static boolean copiaFile(String fOrig, String fDest) {
        byte[] b = new byte[8192];
        FileInputStream fis = null;
        FileOutputStream fos = null;
        boolean ok = true;
        int nBlocchi, nScarto;
        boolean scarto;
        int i;

        // controllo esistenza file di origine
        File fo = new File(fOrig);
        if (!fo.exists()) {
            return false;
        }
        nBlocchi = (int) fo.length() / 8192;
        scarto = ((fo.length() % 8192) != 0);
        nScarto = (int) (fo.length() % 8192);
        // copia effettiva
        try {
            fis = new FileInputStream(fOrig);
            fos = new FileOutputStream(fDest);
            for (i = 0; i < nBlocchi; i++) {
                fis.read(b);
                fos.write(b);
            }
            // copia blocco di scarto
            if (scarto) {
                byte[] sc = new byte[nScarto];
                fis.read(sc);
                fos.write(sc);
            }
            fis.close();
            fos.close();
        } catch (Exception e) {
            ok = false;
            e.printStackTrace();
        } finally {
            try {
                fis.close();
                fos.close();
            } catch (Exception e) {
                ok = false;
            }
            return ok;
        }
    }

    public static int numeroDDTScopertiContanti(String clicod) {
        int num = 0;
        try {
            String oggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
            String dti = oggi.substring(0, 7) + "-01";
            String[] pars = new String[2];
            pars[0] = clicod;
            pars[1] = dti;
            Cursor cursor = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM scoperti WHERE scclicod = ? AND scdatadoc >= ? AND sctipo = 'B' AND scresiduoscad <> 0", pars);
            if (cursor.moveToFirst() && !cursor.isNull(0))
                num = cursor.getInt(0);
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return num;
    }

    public static double totaleDDTScopertiMesePrecedente(String clicod, Data dtdoc) {
        double ris = 0;
        try {
            int mese = dtdoc.mese();
            int anno = dtdoc.anno();
            mese--;
            if (mese == 0) {
                mese = 12;
                anno--;
            }
            String dtctrinizio = (new Data(1, mese, anno)).formatta(Data.AAAA_MM_GG, "-");
            String dtctrfine = funzStringa.riempiStringa("" + anno, 4, funzStringa.DX, '0') + "-" + funzStringa.riempiStringa("" + mese, 2, funzStringa.DX, '0') + "-31";
            String[] pars = new String[3];
            pars[0] = clicod;
            pars[1] = dtctrinizio;
            pars[2] = dtctrfine;
            Cursor cursor = Env.db.rawQuery(
                    "SELECT scresiduoscad FROM scoperti WHERE scclicod = ? AND scdatadoc >= ? AND scdatadoc <= ? AND sctipo = 'B' AND scresiduoscad <> 0", pars);
            while (cursor.moveToNext())
                ris += cursor.getDouble(0);
            cursor.close();
            ris = OpValute.arrotondaMat(ris, 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static double totaleDDTScoperti(String clicod, int mese, int anno) {
        double ris = 0;
        try {
            String dtctrinizio = (new Data(1, mese, anno)).formatta(Data.AAAA_MM_GG, "-");
            String dtctrfine = funzStringa.riempiStringa("" + anno, 4, funzStringa.DX, '0') + "-" + funzStringa.riempiStringa("" + mese, 2, funzStringa.DX, '0') + "-31";
            String[] pars = new String[3];
            pars[0] = clicod;
            pars[1] = dtctrinizio;
            pars[2] = dtctrfine;
            Cursor cursor = Env.db.rawQuery(
                    "SELECT scresiduoscad FROM scoperti WHERE scclicod = ? AND scdatadoc >= ? AND scdatadoc <= ? AND sctipo = 'B' AND scresiduoscad <> 0", pars);
            while (cursor.moveToNext())
                ris += cursor.getDouble(0);
            cursor.close();
            ris = OpValute.arrotondaMat(ris, 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }


    public static double[] scadenzeScadute(String clicod, Data dtoggi, int ggtolleranza) {
        double[] ris = new double[2];
        ris[0] = ris[1] = 0;
        try {
            dtoggi.decrementaGiorni(ggtolleranza);
            String oggi = dtoggi.formatta(Data.AAAA_MM_GG, "-");
            String[] parss = new String[2];
            parss[0] = clicod;
            if (!Env.datainizioblocco.equals("") && !Env.datainizioblocco.equals("0000-00-00"))
                parss[1] = Env.datainizioblocco;
            else
                parss[1] = "0000-00-00";
            Cursor cs = Env.db.rawQuery(
                    "SELECT sctipoop,scresiduoscad,sctipo,scdatascad,scdatadoc FROM scoperti WHERE scclicod = ? AND scdatadoc >= ? AND sctipo <> 'B' AND (scresiduoscad >= 1 OR scresiduoscad <= -1)", parss);
            while (cs.moveToNext()) {
                double imp = cs.getDouble(1);
                String datascad = cs.getString(3);
                if (datascad.equals("") || datascad.equals("0000-00-00"))
                    datascad = cs.getString(4);
                if (!datascad.equals("") && !datascad.equals("0000-00-00") && datascad.compareTo(oggi) < 0) {
                    ris[1]++;
                    if (cs.getString(0).equals("V"))
                        ris[0] += imp;
                    else
                        ris[0] -= imp;
                }
            }
            cs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static ArrayList scadenzeInScadenza(String clicod, Data dt, boolean pagtv) {
        ArrayList ris = new ArrayList();
        String avviso_video = "";
        Data dtmax = new Data(31, 12, 2999);
        try {
            String pagcod = "";
            String[] pars = new String[1];
            pars[0] = clicod;
            Cursor cursor = Env.db.rawQuery(
                    "SELECT clicodpag FROM clienti WHERE clicodice = ?", pars);
            if (cursor.moveToFirst() && !cursor.isNull(0))
                pagcod = cursor.getString(0);
            cursor.close();

            int ggtolleranza = 0;
            int ggavviso = 0;
            if (!pagcod.equals("")) {
                String[] parsp = new String[1];
                parsp[0] = pagcod;
                Cursor cp = Env.db.rawQuery(
                        "SELECT pagtipo,paggg1 FROM pagamenti WHERE pagcod = ?", parsp);
                if (cp.moveToFirst()) {
                    if (cp.getInt(0) == 8) {
                        ggtolleranza = 5;
                        ggavviso = 10;
                    } else if (cp.getInt(0) == 6) {
                        if (cp.getInt(1) == 0)
                            ggtolleranza = 30;
                        else
                            ggtolleranza = 60;
                        ggavviso = 10;
                    } else {
                        ggtolleranza = 90;
                        ggavviso = 10;
                    }
                }
                cp.close();
            }
            boolean avvisopagtv = false;
            Data dtctrscad = new Data(dt.giorno(), dt.mese(), dt.anno());
            dtctrscad.decrementaGiorni(90);
            String[] parss = new String[3];
            parss[0] = clicod;
            parss[1] = dtctrscad.formatta(Data.AAAA_MM_GG, "-");
            if (!Env.datainizioblocco.equals("") && !Env.datainizioblocco.equals("0000-00-00"))
                parss[2] = Env.datainizioblocco;
            else
                parss[2] = "0000-00-00";
            Cursor cs = Env.db.rawQuery(
                    "SELECT sctipoop,scresiduoscad,sctipo,scdatascad,scdatadoc,scsezdoc,scnumdoc FROM scoperti WHERE scclicod = ? AND sctipo <> 'B' AND (scresiduoscad >= 1 OR scresiduoscad <= -1) AND scdatascad >= ? AND scdatadoc >= ? ORDER BY scdatadoc", parss);
            while (cs.moveToNext()) {
                if (cs.getString(0).equals("V")) {
                    Data dtoggi = new Data(dt.giorno(), dt.mese(), dt.anno());
                    dtoggi.incrementaGiorni(ggavviso);
                    String dts = cs.getString(4);
                    if (dts.equals("") || dts.equals("0000-00-00"))
                        dts = cs.getString(3);
                    Data dtscadrett = new Data(Formattazione.estraiIntero(dts.substring(8, 10)), Formattazione.estraiIntero(dts.substring(5, 7)), Formattazione.estraiIntero(dts.substring(0, 4)));
                    int ggt = ggtolleranza;
                    if (cs.getString(2).equals("C")) {
                        Data dtscad = new Data(Formattazione.estraiIntero(cs.getString(3).substring(8, 10)), Formattazione.estraiIntero(cs.getString(3).substring(5, 7)), Formattazione.estraiIntero(cs.getString(3).substring(0, 4)));
                        int a = Formattazione.estraiIntero(cs.getString(3).substring(0, 4));
                        int m = Formattazione.estraiIntero(cs.getString(3).substring(5, 7));
                        m++;
                        if (m > 12) {
                            a++;
                            m = 1;
                        }
                        Data dtscadtol = new Data(1, m, a);
                        dtscadtol.decrementaGiorni(1);
                        ggt = dtscadtol.giorno() - dtscad.giorno() + 30;
                        dtoggi = new Data(dt.giorno(), dt.mese(), dt.anno());
                        dtoggi.incrementaGiorni(ggt);
                    }
                    dtscadrett.incrementaGiorni(ggt);
                    double imp = cs.getDouble(1);
                    if (dtoggi.maggiore(dtscadrett)) {
                        if (!pagtv) {
                            Data dtoggiavvpagtv = new Data(dtoggi.giorno(), dtoggi.mese(), dtoggi.anno());
                            dtoggiavvpagtv.decrementaGiorni(5);
                            if (dtoggiavvpagtv.maggiore(dtscadrett))
                                avvisopagtv = true;
                        }
                        if (dtmax.maggiore(dtscadrett)) {
                            dtmax = dtscadrett;
                            if (avvisopagtv)
                                avviso_video = "Il cliente ha un pagamento in scadenza al " + dtscadrett.formatta(Data.GG_MM_AAAA, "/") + " per " + Formattazione.formValuta(imp, 12, 2, 0) + "," +
                                        "Se non saldato in tempo il cliente sarà passato a PAGAMENTO CONTANTI";
                            else
                                avviso_video = "Il cliente ha un pagamento in scadenza al " + dtscadrett.formatta(Data.GG_MM_AAAA, "/") + " per " + Formattazione.formValuta(imp, 12, 2, 0);
                        }
                        String avviso = "";
                        String dtfatt = cs.getString(4);
                        if (dtfatt.equals("") || dtfatt.equals("0000-00-00"))
                            dtfatt = cs.getString(3);
                        avviso += dtscadrett.formatta(Data.GG_MM_AAAA, "/") + " " + funzStringa.riempiStringa(Formattazione.formValuta(imp, 12, 2, 0), 13, funzStringa.DX, ' ') + funzStringa.riempiStringa(" n." + cs.getString(5) + "/" + cs.getInt(6), 12, funzStringa.SX, ' ') + " " + dtfatt.substring(8, 10) + "/" + dtfatt.substring(5, 7) + "/" + dtfatt.substring(0, 4);
                        ris.add(avviso);
                    }
                }
            }
            cs.close();
            if (ris.size() > 0) {
                ris.add(0, avviso_video);
                if (avvisopagtv)
                    ris.add(1, "S");
                else
                    ris.add(1, "N");
            } else {
                ris.add(avviso_video);
                if (avvisopagtv)
                    ris.add(1, "S");
                else
                    ris.add(1, "N");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static String[] controlloDDTSospesoPrec(String clicod, Data dt) {
        String[] ris = new String[2];
        ris[0] = "NO";
        ris[1] = "";
        try {
            Data dtiniziomese = new Data(1, dt.mese(), dt.anno());
            Data dtpart = new Data(dt.giorno(), dt.mese(), dt.anno());
            dtpart.decrementaGiorni(5);
            String[] parss = new String[3];
            parss[0] = clicod;
            parss[1] = dtiniziomese.formatta(Data.AAAA_MM_GG, "-");
            parss[2] = dtpart.formatta(Data.AAAA_MM_GG, "-");
            Cursor cs = Env.db.rawQuery(
                    "SELECT scdatadoc FROM scoperti WHERE scclicod = ? AND sctipo = 'B' AND scresiduoscad <> 0 AND scdatadoc >= ? AND scdatadoc <= ? ORDER BY scdatadoc", parss);
            if (cs.moveToFirst()) {
                ris[0] = "SI";
                String dtv = cs.getString(0);
                ris[1] = dtv.substring(8, 10) + "/" + dtv.substring(5, 7) + "/" + dtv.substring(0, 4);
            }
            cs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static double totaleRateScadute(String clicod) {
        double ris = 0;
        try {
            String oggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
            String[] parss = new String[2];
            parss[0] = clicod;
            parss[1] = oggi;
            Cursor cs = Env.db.rawQuery(
                    "SELECT scscadid,scresiduoscad FROM scoperti WHERE scclicod = ? AND scdatascad <= ? AND scresiduoscad <> 0 AND scrateizzata = 1", parss);
            while (cs.moveToNext()) {
                ris += cs.getDouble(1);
            }
            cs.close();
            ris = OpValute.arrotondaMat(ris, 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static double totaleScopertiCliente(String clicod) {
        double ris = 0;
        try {
            String[] pars = new String[2];
            pars[0] = clicod;
            if (!Env.datainizioblocco.equals("") && !Env.datainizioblocco.equals("0000-00-00"))
                pars[1] = Env.datainizioblocco;
            else
                pars[1] = "0000-00-00";
            Cursor cs = Env.db.rawQuery(
                    "SELECT SUM(scresiduoscad) AS imp,sctipoop FROM scoperti WHERE scclicod = ? AND scdatadoc >= ? AND scresiduoscad > 0 GROUP BY sctipoop", pars);
            while (cs.moveToNext()) {
                if (cs.getString(1).equals("V"))
                    ris += cs.getDouble(0);
                else
                    ris -= cs.getDouble(0);
            }
            cs.close();
            ris = OpValute.arrotondaMat(ris, 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static ArrayList<Record> elencoScopertiCliente(String clicod) {
        ArrayList<Record> vscop = new ArrayList();
        try {
            String[] pars = new String[2];
            pars[0] = clicod;
            if (!Env.datainizioblocco.equals("") && !Env.datainizioblocco.equals("0000-00-00"))
                pars[1] = Env.datainizioblocco;
            else
                pars[1] = "0000-00-00";
            Cursor cursor = Env.db.rawQuery(
                    "SELECT sctipo,scsezdoc,scdatadoc,scnumdoc,sctipoop,scnote,scclicod,scdestcod," +
                            "scdatascad,sctiposcad,scimportoscad,scresiduoscad,scscadid,scrateizzata FROM scoperti WHERE scclicod=? " +
                            " AND scresiduoscad > 0 AND scdatadoc >= ? ORDER BY scdatadoc DESC,scnumdoc ASC", pars);
            while (cursor.moveToNext()) {
                Boolean scopok = true;
                // se bolla sospesa e presente fattura di fine mese, non stampa la bolla
                if (cursor.getString(0).equals("B")) {
                    int anno = Formattazione.estraiIntero(cursor.getString(2).substring(0, 4));
                    int mese = Formattazione.estraiIntero(cursor.getString(2).substring(5, 7));
                    mese++;
                    if (mese > 12) {
                        anno++;
                        mese = 1;
                    }
                    Data dt = new Data(1, mese, anno);
                    dt.decrementaGiorni(1);
                    String dtf = dt.formatta(Data.AAAA_MM_GG, "-");
                    String[] pars2 = new String[2];
                    pars2[0] = clicod;
                    pars2[1] = dtf;
                    Cursor cursor2 = Env.db.rawQuery(
                            "SELECT * FROM scoperti WHERE scclicod=? AND scresiduoscad > 0 AND sctipo = 'F' AND scdatadoc=?", pars2);
                    if (cursor2.moveToFirst())
                        scopok = false;
                    cursor2.close();
                }
                if (scopok) {
                    // prepara riga
                    Record r = new Record();
                    r.insStringa("tipo", cursor.getString(0));
                    r.insStringa("sezdoc", cursor.getString(1));
                    r.insStringa("datadoc", cursor.getString(2));
                    r.insIntero("numdoc", cursor.getInt(3));
                    r.insStringa("tipoop", cursor.getString(4));
                    r.insStringa("note", cursor.getString(5));
                    r.insIntero("scadid", cursor.getInt(12));
                    r.insStringa("datascad", cursor.getString(8));
                    r.insIntero("tiposcad", cursor.getInt(9));
                    r.insDouble("importoscad", cursor.getDouble(10));
                    r.insDouble("residuoscad", cursor.getDouble(11));
                    r.insDouble("saldo", 0);
                    r.insIntero("rateizzata", cursor.getInt(13));
                    r.insIntero("calctot", scopok ? 1 : 0);
                    vscop.add(r);
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vscop;
    }

    public static ArrayList<Integer> elencoRateScadute(String clicod) {
        ArrayList<Integer> vscadute = new ArrayList();
        try {
            String oggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
            String[] parss = new String[3];
            parss[0] = clicod;
            parss[1] = oggi;
            if (!Env.datainizioblocco.equals("") && !Env.datainizioblocco.equals("0000-00-00"))
                parss[2] = Env.datainizioblocco;
            else
                parss[2] = "0000-00-00";
            Cursor cs = Env.db.rawQuery(
                    "SELECT scscadid,scresiduoscad FROM scoperti WHERE scclicod = ? AND scdatascad <= ? AND scdatadoc >= ? AND scresiduoscad <> 0 AND scrateizzata = 1", parss);
            while (cs.moveToFirst()) {
                vscadute.add(cs.getInt(0));
            }
            cs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vscadute;
    }

    public static double totaleSospesi() {
        double ris = 0;
        try {
            Cursor cs = Env.db.rawQuery(
                    "SELECT scresiduoscad,sctipoop AS tsosp FROM scoperti WHERE sctipo = 'B' AND scresiduoscad > 0", null);
            double totalesosp = 0;
            while (cs.moveToNext()) {
                double sospdoc = cs.getDouble(0);
                if (!cs.getString(1).equals("V"))
                    sospdoc = -sospdoc;
                ris += sospdoc;
            }
            cs.close();
            ris = OpValute.arrotondaMat(ris, 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }


    public static boolean clienteBloccabile(String cc) {
        boolean ris = false;
        try {
            String[] pars = new String[1];
            pars[0] = cc;
            Cursor cs = Env.db.rawQuery(
                    "SELECT cliblocco FROM clienti WHERE clicodice = ?", pars);
            cs.moveToFirst();
            String blocco = cs.getString(0);
            cs.close();
            ris = (blocco.equals("S"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static double saldoContabileCliente(String cc) {
        double ris = 0;
        try {
            String[] pars = new String[1];
            pars[0] = cc;
            Cursor cs = Env.db.rawQuery(
                    "SELECT clisaldo FROM clienti WHERE clicodice = ?", pars);
            cs.moveToFirst();
            ris = cs.getDouble(0);
            cs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static void creaBloccoCliente(String clicod, double importo) {
        SQLiteStatement st = Env.db.compileStatement(
                "INSERT INTO blocchicli (clicod,data,importo,trasf) VALUES (?,?,?,?)");
        st.clearBindings();
        st.bindString(1, clicod);
        st.bindString(2, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
        st.bindDouble(3, importo);
        st.bindString(4, "N");
        st.execute();
        st.close();
        registraBloccoClienteFineGiornata(clicod);
    }

    public static void rimuoviBloccoCliente(String clicod) {
        SQLiteStatement st = Env.db.compileStatement(
                "DELETE FROM blocchicli WHERE clicod=?");
        st.clearBindings();
        st.bindString(1, clicod);
        st.execute();
        st.close();

        st = Env.db.compileStatement(
                "DELETE FROM blocchicli_fineg WHERE clicod=?");
        st.clearBindings();
        st.bindString(1, clicod);
        st.execute();
        st.close();
    }

    // blocco cliente
    public static boolean bloccoClientePresente(String clicod) {
        boolean ris = false;
        String[] pars = new String[1];
        pars[0] = clicod;
        Cursor cs = Env.db.rawQuery(
                "SELECT clicod FROM blocchicli WHERE clicod = ?", pars);
        if (cs.moveToFirst())
            ris = true;
        cs.close();
        return ris;
    }

    public static boolean controllaPin(String pin) {
        boolean ris = false;
        String[] pars = new String[1];
        pars[0] = pin;
        Cursor cs = Env.db.rawQuery(
                "SELECT pin FROM datiterm WHERE pin = ?", pars);
        if (cs.moveToFirst())
            ris = true;
        cs.close();
        return ris;
    }

    public static String leggiKey() {
        String ris = null;
        Cursor cs = Env.db.rawQuery(
                "SELECT activationcode FROM datiterm", null);
        if (cs.moveToFirst()) {
            ris = cs.getString(0);
        }
        cs.close();
        return ris;
    }

    public static int giorniBloccoCliente(String clicod) {
        int ris = 0;
        String[] pars = new String[1];
        pars[0] = clicod;
        Cursor cs = Env.db.rawQuery(
                "SELECT data FROM blocchicli WHERE clicod = ?", pars);
        if (cs.moveToFirst()) {
            String tmp = cs.getString(0);
            Data dtblocco = new Data(Formattazione.estraiIntero(tmp.substring(8, 10)), Formattazione.estraiIntero(tmp.substring(5, 7)), Formattazione.estraiIntero(tmp.substring(0, 4)));
            Data d1 = new Data();
            ris = (int) (d1.numeroGiorni() - dtblocco.numeroGiorni());
        }
        cs.close();
        return ris;
    }

    public static void registraBloccoClienteFineGiornata(String clicod) {
        SQLiteStatement st = Env.db.compileStatement(
                "DELETE FROM blocchicli_fineg WHERE clicod=?");
        st.clearBindings();
        st.bindString(1, clicod);
        st.execute();
        st.close();
        st = Env.db.compileStatement(
                "INSERT INTO blocchicli_fineg (clicod) VALUES (?)");
        st.clearBindings();
        st.bindString(1, clicod);
        st.execute();
        st.close();
    }

    public static void stampaIntegrazione(int num, String data, Context context, boolean aggnum) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        int numddtcar = estraiIntero(leggiProprieta("ddtn"));
        numddtcar--;
        if (numddtcar < 1)
            numddtcar = 1;
        int numintegr = estraiIntero(leggiProprieta("numintegr"));
        if (numintegr == 0)
            numintegr = 1;

        try {
            ArrayList<Record> vrighe = new ArrayList();
            String[] pars = new String[3];
            pars[0] = Env.docddtcarico;
            pars[1] = data;
            pars[2] = "" + num;

            Cursor cm = Env.db.rawQuery("SELECT movdocora FROM movimenti WHERE movdoc = ? AND movdata = ? AND movnum = ?", pars);
            cm.moveToFirst();
            String oradoc = cm.getString(0);
            cm.close();

            Cursor c = Env.db.rawQuery("SELECT rmartcod,rmlotto,rmqta,rmaliq FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? ORDER BY rmriga", pars);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", c.getString(0));
                rx.insStringa("lotto", c.getString(1));
                rx.insDouble("qta", c.getDouble(2));
                rx.insDouble("aliq", c.getDouble(3));
                String[] parsa = new String[1];
                parsa[0] = c.getString(0);
                Cursor ca = Env.db.rawQuery("SELECT artdescr,artum FROM articoli WHERE artcod = ?", parsa);
                if (ca.moveToFirst()) {
                    rx.insStringa("artdescr", ca.getString(0));
                    rx.insStringa("um", ca.getString(1));
                } else {
                    rx.insStringa("artdescr", "");
                    rx.insStringa("um", "");
                }
                ca.close();
                vrighe.add(rx);
            }
            c.close();

            int nrigheform = 16;
            if (Env.stampasedesec)
                nrigheform += 4;
            File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
            if (flogo.exists())
                nrigheform += Env.nrighelogo;

            Env.cpclPrinter.setForm(0, 200, 200, nrigheform * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("DDT DI INTEGRAZIONE MERCE", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "DDT DI INTEGRAZIONE MERCE", lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Documento di Trasporto (D.P.R.472 del 14-08-96)", lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Riferimento DDT N. " + numddtcar + "/" + numintegr, lf, 20);
            posy += 30;
            //Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "PROPRIETARIO DELLA MERCE:", lf, 20);
            //posy += 30;
            if (flogo.exists()) {
                try {
                    Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                    Env.cpclPrinter.printBitmap(bm, 0, posy);
                } catch (Exception elogo) {
                    elogo.printStackTrace();
                }
                posy += 30 * Env.nrighelogo;
            }
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga6 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga7 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + " - " + (new Data(data, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " h:" + oradoc;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = "Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "VENDITORE / CARICATORE MERCE / VETTORE :", lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;

            String[] parsage = new String[1];
            parsage[0] = Env.agecod;
            Cursor cage = Env.db.rawQuery("SELECT ageindirizzo,agelocalita,agepartitaiva,agecodicefiscale,agenregalbo FROM agenti WHERE agecod = ?", parsage);
            if (cage.moveToFirst()) {
                stmp = funzStringa.tagliaStringa("p.IVA:" + cage.getString(2) + " C.Fisc.:" + cage.getString(3), gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa("N.Iscr.Albo:" + cage.getString(4), gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
            } else {
                posy += 60;
            }
			/*
			String[] parsage = new String[1];
			parsage[0] = Env.agecod;
			Cursor cage = Env.db.rawQuery("SELECT ageindirizzo,agelocalita,agepartitaiva,agecodicefiscale,agenregalbo FROM agenti WHERE agecod = ?", parsage);
			if (cage.moveToFirst())
			{
				stmp = funzStringa.tagliaStringa(cage.getString(0) + " ", gf20);
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
				posy += 30;
				stmp = funzStringa.tagliaStringa(cage.getString(1) + " ", gf20);
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
				posy += 30;
				stmp = funzStringa.tagliaStringa("p.IVA:" + cage.getString(2) + " C.Fisc.:" + cage.getString(3), gf20);
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
				posy += 30;
				stmp = funzStringa.tagliaStringa("N.Iscr.Albo:" + cage.getString(4), gf20);
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
				posy += 30;
			}
			else
			{
				posy += 120;
			}
*/
            cage.close();

            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Committente: TENTATA VENDITA", lf, 20);
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                         UM LOTTO       QTA ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                        UM LOTTO       QTA ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            double totimp = 0;
            double totkg = 0;
            HashMap<Double, Double> himp = new HashMap();
            for (int i = 0; i < vrighe.size(); i++) {
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(30);
                Env.cpclPrinter.setSpeed(5);
                posy = 0;
                Record rx = vrighe.get(i);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    String riga = "";
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 23, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                } else {
                    String riga = "";
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 38, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                }
                posy += 30;
                Env.cpclPrinter.printForm();
                double pb = leggiPrezzoBaseArticolo(rx.leggiStringa("artcod"), rx.leggiStringa("lotto"), true);
                double imp = OpValute.arrotondaMat(pb * rx.leggiDouble("qta"), 2);
                totimp += imp;
                totkg += FunzioniJBeerApp.calcolaKgArticolo(rx.leggiStringa("artcod"), rx.leggiDouble("qta"));
                if (himp.get(new Double(rx.leggiDouble("aliq"))) == null) {
                    himp.put(new Double(rx.leggiDouble("aliq")), new Double(imp));
                } else {
                    double impold = ((Double) himp.get(new Double(rx.leggiDouble("aliq")))).doubleValue();
                    himp.put(new Double(rx.leggiDouble("aliq")), new Double(impold + imp));
                }
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 10)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 11 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;

            double totiva = 0;
            Iterator it = himp.keySet().iterator();
            while (it.hasNext()) {
                double aliq = ((Double) it.next()).doubleValue();
                double imp = ((Double) himp.get(new Double(aliq))).doubleValue();
                double iva = OpValute.arrotondaMat(imp * aliq / 100.0, 2);
                totiva += iva;
            }

            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.Prodotti       : " + vrighe.size(), lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Totale KG        : " +
                    funzStringa.riempiStringa(Formattazione.formValuta(totkg, 12, 3, 0), 15, funzStringa.DX, ' '), lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Totale imponibile: " +
                    funzStringa.riempiStringa(Formattazione.formValuta(totimp, 12, 2, 0), 15, funzStringa.DX, ' '), lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Totale IVA       : " +
                    funzStringa.riempiStringa(Formattazione.formValuta(totiva, 12, 2, 0), 15, funzStringa.DX, ' '), lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(4, posy, Typeface.MONOSPACE, true, false, false, "Firma conducente", lf, 20);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 119, 1);
            posy += 120;

            Env.cpclPrinter.printForm();

            c.close();

            if (aggnum) {
                numintegr++;
                FunzioniJBeerApp.impostaProprieta("numintegr", "" + numintegr);
            }
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    public static void stampaDDTCarico(Context context, boolean storico, String stordata, String storora, int stornum) {
        try {
            int lf = 576;
            if (Env.tipostampante == 1)
                lf = 740;
            int gf16 = 58;
            if (Env.tipostampante == 1)
                gf16 = 76;
            int gf18 = 53;
            if (Env.tipostampante == 1)
                gf18 = 68;
            int gf20 = 47;
            if (Env.tipostampante == 1)
                gf20 = 61;
            int gf24 = 39;
            if (Env.tipostampante == 1)
                gf24 = 41;

            int ddtc = estraiIntero(leggiProprieta("ddtn"));
            if (ddtc == 0)
                ddtc = 1;

            if (Env.stampaddtcar_classi) {
                ArrayList<Record> vrighe = new ArrayList();
                if (storico) {
                    String[] parsst = new String[3];
                    parsst[0] = stordata;
                    parsst[1] = storora;
                    parsst[2] = "" + stornum;
                    Cursor c = Env.db.rawQuery("SELECT straggrcod,straggrdescr,stum,stqta,stprezzo FROM storico_ddtcarico WHERE stdata = ? AND stora = ? AND stnum = ? ORDER BY straggrcod", parsst);
                    while (c.moveToNext()) {
                        Record rx = new Record();
                        String cod = "-";
                        if (!c.isNull(0))
                            cod = c.getString(0);
                        String d = "-";
                        if (!c.isNull(1))
                            d = c.getString(1);
                        rx.insStringa("raggrcod", cod);
                        rx.insStringa("raggrdescr", d);
                        rx.insStringa("um", c.getString(2));
                        rx.insDouble("qta", c.getDouble(3));
                        rx.insDouble("prezzo", c.getDouble(4));
                        vrighe.add(rx);
                    }
                    c.close();
                } else {
                    Cursor c = Env.db.rawQuery("SELECT raggrcod,raggrdescr,artum,SUM(artgiacenza) AS tgiac " +
                            " FROM articoli LEFT JOIN raggrart ON (articoli.artraggrcod = raggrart.raggrcod) " +
                            " WHERE artgiacenza > 0 GROUP BY raggrcod ORDER BY raggrcod", null);
                    while (c.moveToNext()) {
                        Record rx = new Record();
                        String cod = "-";
                        if (!c.isNull(0))
                            cod = c.getString(0);
                        String d = "-";
                        if (!c.isNull(1))
                            d = c.getString(1);
                        rx.insStringa("raggrcod", cod);
                        rx.insStringa("raggrdescr", d);
                        rx.insStringa("um", c.getString(2));
                        rx.insDouble("qta", c.getDouble(3));
                        rx.insDouble("prezzo", 0);
                        vrighe.add(rx);
                    }
                    c.close();
                }

                int nrigheint = 17;
                if (Env.stampasedesec)
                    nrigheint += 1;
                File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                if (flogo.exists())
                    nrigheint += Env.nrighelogo;
                Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.setSpeed(5);
                int posy = 0;
                Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Documento di Trasporto (D.P.R.472 del 14-08-96)", lf, 20);
                posy += 30;
                if (storico)
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N." + stornum, lf, 20);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N." + ddtc, lf, 20);
                posy += 30;
                //Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "PROPRIETARIO DELLA MERCE:", lf, 20);
                //posy += 30;
                if (flogo.exists()) {
                    try {
                        Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                        Env.cpclPrinter.printBitmap(bm, 0, posy);
                    } catch (Exception elogo) {
                        elogo.printStackTrace();
                    }
                    posy += 30 * Env.nrighelogo;
                }
                String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 20;
                stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 20;
                stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 20;
                stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 20;
                stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 20;
                stmp = funzStringa.tagliaStringa(Env.intazriga6 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 20;
                stmp = funzStringa.tagliaStringa(Env.intazriga7 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 30;
                if (Env.stampasedesec) {
                    String[] parsdep = new String[1];
                    parsdep[0] = Env.depsede;
                    Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                    cdep.moveToFirst();
                    String lp = funzStringa.tagliaStringa("Luogo di partenza:" + cdep.getString(2) + " " + cdep.getString(0) + " " + cdep.getString(1), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, lp, lf, 20);
                    posy += 30;
                    cdep.close();
                }
                SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
                String ora = dh.format(new Date());
                SimpleDateFormat dh2 = new SimpleDateFormat("HH:mm:ss");
                String ora2 = dh2.format(new Date());
                if (storico)
                    stmp = "Terminale: " + Env.pedcod + " - " + (new Data(stordata, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " h:" + storora;
                else
                    stmp = "Terminale: " + Env.pedcod + " - " + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " h:" + ora;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                stmp = "Targa: " + Env.targamezzo;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "VENDITORE / CARICATORE MERCE / VETTORE :", lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(Env.agecod + ", " + Env.agenome, gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;

                String[] parsage = new String[1];
                parsage[0] = Env.agecod;
                Cursor cage = Env.db.rawQuery("SELECT ageindirizzo,agelocalita,agepartitaiva,agecodicefiscale,agenregalbo FROM agenti WHERE agecod = ?", parsage);
                if (cage.moveToFirst()) {
                    stmp = funzStringa.tagliaStringa(cage.getString(0) + " ", gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cage.getString(1) + " ", gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("p.IVA:" + cage.getString(2) + " C.Fisc.:" + cage.getString(3), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("N.Iscr.Albo:" + cage.getString(4), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                    posy += 30;
                } else {
                    posy += 120;
                }
                cage.close();

                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Committente: TENTATA VENDITA", lf, 20);
                posy += 30;
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                UM      QTA ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                               UM      QTA ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printForm();

                for (int i = 0; i < vrighe.size(); i++) {
                    Record rx = vrighe.get(i);
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    Env.cpclPrinter.setSpeed(5);
                    posy = 0;
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        String riga = "";
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("raggrcod"), 10, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("raggrdescr"), 28, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    } else {
                        String riga = "";
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("raggrcod"), 10, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("raggrdescr"), 43, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    }
                    posy += 30;
                    Env.cpclPrinter.printForm();
                    if (i % Env.stampe_par1 == 0 && i > 0) {
                        try {
                            Thread.currentThread().sleep(5000);
                        } catch (Exception ex) {
                        }
//                        int ntent = 0;
//                        while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 10)
//                        {
//                            Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                            try
//                            {
//                                Thread.currentThread().sleep(Env.stampe_par2);
//                            } catch (Exception ex)
//                            {
//                            }
//                            ntent++;
//                        }
                    }
                }
                try {
                    Thread.currentThread().sleep(5000);
                } catch (Exception ex) {
                }
                Env.cpclPrinter.setForm(0, 200, 200, 9 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.setSpeed(5);
                posy = 0;
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(4, posy, Typeface.MONOSPACE, true, false, false, "Dati compilatore (" + Env.agenome + ")", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 119, 1);
                posy += 120;
                Env.cpclPrinter.printForm();

                if (!storico) {
                    // storicizzazione ddt di carico
                    SQLiteStatement stmt = Env.db.compileStatement("INSERT INTO storico_ddtcarico (stdata,stora,stnum,startcod,startdescr,stum,stlotto," +
                            "straggrcod,straggrdescr,stqta,stprezzo) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                    for (int i = 0; i < vrighe.size(); i++) {
                        Record rx = vrighe.get(i);
                        stmt.clearBindings();
                        stmt.bindString(1, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                        stmt.bindString(2, ora2);
                        stmt.bindLong(3, ddtc);
                        stmt.bindString(4, "");
                        stmt.bindString(5, "");
                        stmt.bindString(6, rx.leggiStringa("um"));
                        stmt.bindString(7, "");
                        stmt.bindString(8, rx.leggiStringa("raggrcod"));
                        stmt.bindString(9, rx.leggiStringa("raggrdescr"));
                        stmt.bindDouble(10, rx.leggiDouble("qta"));
                        stmt.bindDouble(11, 0);
                        stmt.execute();
                    }
                    stmt.close();
                }
            } else {
                ArrayList<Record> vrighe = new ArrayList();
                HashMap<String, Double> htot = new HashMap();
                if (storico) {
                    String[] parsst = new String[3];
                    parsst[0] = stordata;
                    parsst[1] = storora;
                    parsst[2] = "" + stornum;
                    Cursor c = Env.db.rawQuery("SELECT startcod,startdescr,stum,stlotto,stqta,stprezzo FROM storico_ddtcarico WHERE stdata = ? AND stora = ? AND stnum = ? ORDER BY startcod", parsst);
                    while (c.moveToNext()) {
                        Record rx = new Record();
                        rx.insStringa("artcod", c.getString(0));
                        rx.insStringa("artdescr", c.getString(1));
                        rx.insStringa("um", c.getString(2));
                        rx.insStringa("lotto", c.getString(3));
                        rx.insDouble("qta", c.getDouble(4));
                        rx.insDouble("prezzo", c.getDouble(5));
                        vrighe.add(rx);
                        if (htot.get(rx.leggiStringa("artcod")) == null) {
                            htot.put(rx.leggiStringa("artcod"), rx.leggiDouble("qta"));
                        } else {
                            double qtaold = htot.get(rx.leggiStringa("artcod"));
                            htot.put(rx.leggiStringa("artcod"), OpValute.arrotondaMat(qtaold + rx.leggiDouble("qta"), 3));
                        }
                    }
                    c.close();
                } else {
                    Cursor c = Env.db.rawQuery("SELECT artcod,artdescr,artgiacenza,artum FROM articoli WHERE artgiacenza > 0 ORDER BY artcod", null);
                    while (c.moveToNext()) {
                        String[] parsl = new String[1];
                        parsl[0] = c.getString(0);
                        Cursor cl = Env.db.rawQuery(
                                "SELECT lotcod,lotgiacenza FROM lotti WHERE lotgiacenza <> 0 AND lotartcod = ? ORDER BY lotcod DESC", parsl);
                        while (cl.moveToNext()) {
                            Record rx = new Record();
                            rx.insStringa("artcod", c.getString(0));
                            rx.insStringa("artdescr", c.getString(1));
                            rx.insStringa("um", c.getString(3));
                            rx.insStringa("lotto", cl.getString(0));
                            rx.insDouble("qta", cl.getDouble(1));
                            vrighe.add(rx);
                            if (htot.get(rx.leggiStringa("artcod")) == null) {
                                htot.put(rx.leggiStringa("artcod"), rx.leggiDouble("qta"));
                            } else {
                                double qtaold = htot.get(rx.leggiStringa("artcod"));
                                htot.put(rx.leggiStringa("artcod"), OpValute.arrotondaMat(qtaold + rx.leggiDouble("qta"), 3));
                            }
                        }
                        cl.close();
                    }
                    c.close();
                }
                int nrigheint = 18;
                if (Env.stampasedesec)
                    nrigheint += 4;
                File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                if (flogo.exists())
                    nrigheint += Env.nrighelogo;

                Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.setSpeed(5);
                int posy = 0;
                Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Documento di Trasporto (D.P.R.472 del 14-08-96)", lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Scheda di trasporto art. 7bis D.lgs.286/2005", lf, 20);
                posy += 30;
                if (storico)
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N." + stornum, lf, 20);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N." + ddtc, lf, 20);
                posy += 30;
                //Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "PROPRIETARIO DELLA MERCE:", lf, 20);
                //posy += 30;
                if (flogo.exists()) {
                    try {
                        Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                        Env.cpclPrinter.printBitmap(bm, 0, posy);
                    } catch (Exception elogo) {
                        elogo.printStackTrace();
                    }
                    posy += 30 * Env.nrighelogo;
                }
                String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 20;
                stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 20;
                stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 20;
                stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 20;
                stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 20;
                stmp = funzStringa.tagliaStringa(Env.intazriga6 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 20;
                stmp = funzStringa.tagliaStringa(Env.intazriga7 + " ", gf16);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
                posy += 30;
                if (Env.stampasedesec) {
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                    posy += 30;
                    String[] parsdep = new String[1];
                    parsdep[0] = Env.depsede;
                    Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                    cdep.moveToFirst();
                    stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    cdep.close();
                }
                SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
                String ora = dh.format(new Date());
                SimpleDateFormat dh2 = new SimpleDateFormat("HH:mm:ss");
                String ora2 = dh2.format(new Date());
                if (storico)
                    stmp = "Terminale: " + Env.pedcod + " - " + (new Data(stordata, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " h:" + storora;
                else
                    stmp = "Terminale: " + Env.pedcod + " - " + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " h:" + ora;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                stmp = "Targa: " + Env.targamezzo;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "VENDITORE / CARICATORE MERCE / VETTORE :", lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(Env.agecod + ", " + Env.agenome, gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;

                String[] parsage = new String[1];
                parsage[0] = Env.agecod;
                Cursor cage = Env.db.rawQuery("SELECT ageindirizzo,agelocalita,agepartitaiva,agecodicefiscale,agenregalbo FROM agenti WHERE agecod = ?", parsage);
                if (cage.moveToFirst()) {
                    stmp = funzStringa.tagliaStringa(cage.getString(0) + " ", gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cage.getString(1) + " ", gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("p.IVA:" + cage.getString(2) + " C.Fisc.:" + cage.getString(3), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("N.Iscr.Albo:" + cage.getString(4), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                    posy += 30;
                } else {
                    posy += 120;
                }
                cage.close();

                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Committente: TENTATA VENDITA", lf, 20);
                posy += 30;
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                         UM LOTTO       QTA ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                        UM LOTTO       QTA ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printForm();

                for (int i = 0; i < vrighe.size(); i++) {
                    Record rx = vrighe.get(i);
                    boolean stampaart = true;
                    if (Env.tipogestlotti == 0) {
                        if (i > 0) {
                            Record rprec = vrighe.get(i - 1);
                            if (rprec.leggiStringa("artcod").equals(rx.leggiStringa("artcod"))) {
                                stampaart = false;
                            }
                        }
                    }
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                        Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                        Env.cpclPrinter.setTone(30);
                        Env.cpclPrinter.setSpeed(5);
                        posy = 0;
                        String riga = "";
                        if (stampaart) {
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 23, funzStringa.SX, ' ');
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                        } else {
                            riga += " " + funzStringa.spazi(35);
                        }
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                        System.out.println("riga:" + riga);
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                        posy += 30;
                        Env.cpclPrinter.printForm();
                        if (Env.tipogestlotti == 0) {
                            if ((i < vrighe.size() - 1 && !vrighe.get(i + 1).leggiStringa("artcod").equals(rx.leggiStringa("artcod"))) || (i == vrighe.size() - 1)) {
                                Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
                                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                                Env.cpclPrinter.setTone(30);
                                Env.cpclPrinter.setSpeed(5);
                                posy = 0;
                                String rigatot = funzStringa.spazi(32) + "Totale " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 4, funzStringa.SX, ' ') + " " +
                                        funzStringa.riempiStringa(Formattazione.formatta(htot.get(rx.leggiStringa("artcod")), "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, rigatot, lf, 18);
                                posy += 30;
                                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, funzStringa.stringa("-", 53), lf, 18);
                                posy += 30;
                                Env.cpclPrinter.printForm();
                            }
                        }
                    } else {
                        Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                        Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                        Env.cpclPrinter.setTone(30);
                        Env.cpclPrinter.setSpeed(5);
                        posy = 0;
                        String riga = "";
                        if (stampaart) {
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 38, funzStringa.SX, ' ');
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                        } else {
                            riga += " " + funzStringa.spazi(50);
                        }
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                        posy += 30;
                        Env.cpclPrinter.printForm();
                        if (Env.tipogestlotti == 0) {
                            if ((i < vrighe.size() - 1 && !vrighe.get(i + 1).leggiStringa("artcod").equals(rx.leggiStringa("artcod"))) || (i == vrighe.size() - 1)) {
                                Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
                                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                                Env.cpclPrinter.setTone(30);
                                Env.cpclPrinter.setSpeed(5);
                                posy = 0;
                                String rigatot = funzStringa.spazi(43) + "Totale " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ') + " " +
                                        funzStringa.riempiStringa(Formattazione.formatta(htot.get(rx.leggiStringa("artcod")), "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, rigatot, lf, 18);
                                posy += 30;
                                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, funzStringa.stringa("-", 53), lf, 18);
                                posy += 30;
                                Env.cpclPrinter.printForm();
                            }
                        }
                    }
                    if (i % Env.stampe_par1 == 0) {
                        try {
                            Thread.currentThread().sleep(5000);
                        } catch (Exception ex) {
                        }
//                        int ntent = 0;
//                        while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 10)
//                        {
//                            Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                            try
//                            {
//                                Thread.currentThread().sleep(Env.stampe_par2);
//                            } catch (Exception ex)
//                            {
//                            }
//                            ntent++;
//                        }
                    }
                }
                try {
                    Thread.currentThread().sleep(5000);
                } catch (Exception ex) {
                }
                Env.cpclPrinter.setForm(0, 200, 200, 7 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.setSpeed(5);
                posy = 0;
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(4, posy, Typeface.MONOSPACE, true, false, false, "Dati compilatore (" + Env.agenome + ")", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 119, 1);
                posy += 120;

                Env.cpclPrinter.printForm();

                if (!storico) {
                    // storicizzazione ddt di carico
                    SQLiteStatement stmt = Env.db.compileStatement("INSERT INTO storico_ddtcarico (stdata,stora,stnum,startcod,startdescr,stum,stlotto," +
                            "straggrcod,straggrdescr,stqta,stprezzo) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                    for (int i = 0; i < vrighe.size(); i++) {
                        Record rx = vrighe.get(i);
                        stmt.clearBindings();
                        stmt.bindString(1, (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                        stmt.bindString(2, ora2);
                        stmt.bindLong(3, ddtc);
                        stmt.bindString(4, rx.leggiStringa("artcod"));
                        stmt.bindString(5, rx.leggiStringa("artdescr"));
                        stmt.bindString(6, rx.leggiStringa("um"));
                        stmt.bindString(7, rx.leggiStringa("lotto"));
                        stmt.bindString(8, "");
                        stmt.bindString(9, "");
                        stmt.bindDouble(10, rx.leggiDouble("qta"));
                        stmt.bindDouble(11, 0);
                        stmt.execute();
                    }
                    stmt.close();
                }
            }
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }


    public static void stampaListinoTerminale(Context context, String cod) {
        try {
            int lf = 576;
            if (Env.tipostampante == 1)
                lf = 740;
            int gf16 = 58;
            if (Env.tipostampante == 1)
                gf16 = 76;
            int gf18 = 53;
            if (Env.tipostampante == 1)
                gf18 = 68;
            int gf20 = 47;
            if (Env.tipostampante == 1)
                gf20 = 61;
            int gf24 = 39;
            if (Env.tipostampante == 1)
                gf24 = 41;

            Cursor cl = Env.db.rawQuery("SELECT ltdescr FROM listini_term WHERE ltcod = '" + cod + "'", null);
            cl.moveToFirst();
            Vector vd = null;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                vd = funzStringa.creaRighe(cl.getString(0), 53);
            else
                vd = funzStringa.creaRighe(cl.getString(0), 67);

            ArrayList<Record> vart = new ArrayList();
            Cursor c = Env.db.rawQuery("SELECT ltdartcod,ltdprezzo,artdescr,artum FROM listini_term_dett INNER JOIN articoli ON ltdartcod = artcod " +
                    "WHERE ltdcodlist = '" + cod + "' ORDER BY artcod", null);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", c.getString(0));
                rx.insStringa("artdescr", c.getString(2));
                rx.insStringa("um", c.getString(3));
                rx.insDouble("prezzo", c.getDouble(1));
                vart.add(rx);
            }
            c.close();

            int nrigheform = 11 + vd.size();
            if (Env.stampasedesec)
                nrigheform += 4;
            File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
            if (flogo.exists())
                nrigheform += Env.nrighelogo;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheform * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);

            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante("LISTINO", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "LISTINO", lf, 24);
            posy += 30;
            if (flogo.exists()) {
                try {
                    Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                    Env.cpclPrinter.printBitmap(bm, 0, posy);
                } catch (Exception elogo) {
                    elogo.printStackTrace();
                }
                posy += 30 * Env.nrighelogo;
            }
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga6 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga7 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(FunzioniJBeerApp.centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "DESCRIZIONE:", lf, 20);
            posy += 30;
            for (int i = 0; i < vd.size(); i++) {
                String d = (String) vd.elementAt(i);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, d, lf, 18);
                posy += 30;
            }
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                UM   PREZZO ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                            UM     PREZZO ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            for (int i = 0; i < vart.size(); i++) {
                Record rx = vart.get(i);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    String riga = "";
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 30, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    Env.cpclPrinter.printForm();
                } else {
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    String riga = "";
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 42, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "#####0.000", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    Env.cpclPrinter.printForm();
                }
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 5 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)7
//		{
//			ex.printStackTrace();
//		}
    }

    public static void stampaDocTrasfDaMezzo(int num, String data, Context context) {
        try {
            String[] pars = new String[3];
            pars[0] = Env.doctrasfdamezzo;
            pars[1] = data;
            pars[2] = "" + num;
            Cursor cm = Env.db.rawQuery("SELECT movnote1,movdocora,movnote6 FROM movimenti WHERE movdoc = ? AND movdata = ? AND movnum = ?", pars);
            cm.moveToFirst();
            String oradoc = cm.getString(1);

            ArrayList<Record> vrighe = new ArrayList();
            HashMap<String, Double> htot = new HashMap();
            Cursor c = Env.db.rawQuery("SELECT rmartcod,rmlotto,rmqta,rmprz FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? ORDER BY rmriga", pars);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", c.getString(0));
                rx.insStringa("lotto", c.getString(1));
                rx.insDouble("qta", c.getDouble(2));
                rx.insDouble("prezzo", c.getDouble(3));
                String[] parsa = new String[1];
                parsa[0] = c.getString(0);
                Cursor ca = Env.db.rawQuery("SELECT artdescr,artum FROM articoli WHERE artcod = ?", parsa);
                if (ca.moveToFirst()) {
                    rx.insStringa("artdescr", ca.getString(0));
                    rx.insStringa("um", ca.getString(1));
                } else {
                    rx.insStringa("artdescr", "");
                    rx.insStringa("um", "");
                }
                ca.close();
                vrighe.add(rx);
                if (htot.get(rx.leggiStringa("artcod")) == null) {
                    htot.put(rx.leggiStringa("artcod"), rx.leggiDouble("qta"));
                } else {
                    double qtaold = htot.get(rx.leggiStringa("artcod"));
                    htot.put(rx.leggiStringa("artcod"), OpValute.arrotondaMat(qtaold + rx.leggiDouble("qta"), 3));
                }
            }
            c.close();

            int nrigheint = 12;
            if (Env.stampasedesec)
                nrigheint += 4;
            if (Env.passcodetrasf)
                nrigheint += 1;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, 576, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("CARICO DA AUTOMEZZO", 24, 576), posy, Typeface.MONOSPACE, true, false, false, "CARICO DA AUTOMEZZO", 576, 24);
            posy += 30;
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            String tdescr = "";
            String[] parst = new String[1];
            parst[0] = cm.getString(0);
            Cursor ct = Env.db.rawQuery("SELECT termdescr FROM terminali WHERE termcod = ?", parst);
            if (ct.moveToFirst())
                tdescr = ct.getString(0);
            ct.close();
            if (tdescr.equals("")) {
                ct = Env.db.rawQuery("SELECT depdescr FROM depositi WHERE depcod = ?", parst);
                if (ct.moveToFirst())
                    tdescr = ct.getString(0);
                ct.close();
            }
            stmp = funzStringa.tagliaStringa(cm.getString(0) + "-" + tdescr, 38);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "DA TERM: " + stmp, 576, 20);
            posy += 30;
            stmp = "N." + num + " Data:" + (new Data(data, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " ora:" + oradoc;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, 576, 20);
            posy += 30;
            if (Env.passcodetrasf) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "PASSCODE: " + cm.getString(2), 576, 24);
                posy += 30;
            }
            Env.cpclPrinter.printLine(0, posy + 15, 576, posy + 15, 1);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", 576, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, 576, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                UM LOTTO       QTA   PREZZO ", 576, 18);
            Env.cpclPrinter.printBox(0, posy, 576, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            for (int i = 0; i < vrighe.size(); i++) {
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Record rx = vrighe.get(i);
                String riga = "";
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 14, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, 576, 18);
                posy += 30;
                Env.cpclPrinter.printForm();
//				if ((i < vrighe.size() - 1 && !vrighe.get(i + 1).leggiStringa("artcod").equals(rx.leggiStringa("artcod"))) || (i == vrighe.size() - 1))
//				{
//					Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
//					Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
//					Env.cpclPrinter.setTone(20);
//					posy = 0;
//                    riga = "";
//                    riga += funzStringa.riempiStringa("Totale " + rx.leggiStringa("artcod"), 43, funzStringa.DX, ' ');
//                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(htot.get(rx.leggiStringa("artcod")), "####0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
//                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, 576, 18);
//					posy += 30;
//					Env.cpclPrinter.printForm();
//				}
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, 576, posy + 15, 1);
            posy += 30;

            Env.cpclPrinter.printForm();

            cm.close();
            c.close();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    public static void stampaDocTrasfVsMezzo(int num, String data, Context context) {
        try {
            String[] pars = new String[3];
            pars[0] = Env.doctrasfamezzo;
            pars[1] = data;
            pars[2] = "" + num;
            Cursor cm = Env.db.rawQuery("SELECT movnote1,movdocora,movnote6 FROM movimenti WHERE movdoc = ? AND movdata = ? AND movnum = ?", pars);
            cm.moveToFirst();
            String oradoc = cm.getString(1);

            ArrayList<Record> vrighe = new ArrayList();
            HashMap<String, Double> htot = new HashMap();
            Cursor c = Env.db.rawQuery("SELECT rmartcod,rmlotto,rmqta,rmprz FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? ORDER BY rmriga", pars);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", c.getString(0));
                rx.insStringa("lotto", c.getString(1));
                rx.insDouble("qta", c.getDouble(2));
                rx.insDouble("prezzo", c.getDouble(3));
                String[] parsa = new String[1];
                parsa[0] = c.getString(0);
                Cursor ca = Env.db.rawQuery("SELECT artdescr,artum FROM articoli WHERE artcod = ?", parsa);
                if (ca.moveToFirst()) {
                    rx.insStringa("artdescr", ca.getString(0));
                    rx.insStringa("um", ca.getString(1));
                } else {
                    rx.insStringa("artdescr", "");
                    rx.insStringa("um", "");
                }
                ca.close();
                vrighe.add(rx);
                if (htot.get(rx.leggiStringa("artcod")) == null) {
                    htot.put(rx.leggiStringa("artcod"), rx.leggiDouble("qta"));
                } else {
                    double qtaold = htot.get(rx.leggiStringa("artcod"));
                    htot.put(rx.leggiStringa("artcod"), OpValute.arrotondaMat(qtaold + rx.leggiDouble("qta"), 3));
                }
            }
            c.close();

            int nrigheint = 12;
            if (Env.stampasedesec)
                nrigheint += 4;
            if (Env.passcodetrasf)
                nrigheint += 1;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, 576, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("SCARICO VERSO AUTOMEZZO", 24, 576), posy, Typeface.MONOSPACE, true, false, false, "SCARICO VERSO AUTOMEZZO", 576, 24);
            posy += 30;
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            String tdescr = "";
            String[] parst = new String[1];
            parst[0] = cm.getString(0);
            Cursor ct = Env.db.rawQuery("SELECT termdescr FROM terminali WHERE termcod = ?", parst);
            if (ct.moveToFirst())
                tdescr = ct.getString(0);
            ct.close();
            if (tdescr.equals("")) {
                ct = Env.db.rawQuery("SELECT depdescr FROM depositi WHERE depcod = ?", parst);
                if (ct.moveToFirst())
                    tdescr = ct.getString(0);
                ct.close();
            }
            stmp = funzStringa.tagliaStringa(cm.getString(0) + "-" + tdescr, 39);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "A TERM: " + stmp, 576, 20);
            posy += 30;
            stmp = "N." + num + " Data:" + (new Data(data, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " ora:" + oradoc;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, 576, 20);
            posy += 30;
            if (Env.passcodetrasf) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "PASSCODE: " + cm.getString(2), 576, 24);
                posy += 30;
            }
            Env.cpclPrinter.printLine(0, posy + 15, 576, posy + 15, 1);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", 576, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, 576, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                UM LOTTO       QTA   PREZZO ", 576, 18);
            Env.cpclPrinter.printBox(0, posy, 576, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            for (int i = 0; i < vrighe.size(); i++) {
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Record rx = vrighe.get(i);
                String riga = "";
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 14, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, 576, 18);
                posy += 30;
                Env.cpclPrinter.printForm();
//				if ((i < vrighe.size() - 1 && !vrighe.get(i + 1).leggiStringa("artcod").equals(rx.leggiStringa("artcod"))) || (i == vrighe.size() - 1))
//				{
//					Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
//					Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
//					Env.cpclPrinter.setTone(20);
//					posy = 0;
//                    riga = "";
//                    riga += funzStringa.riempiStringa("Totale " + rx.leggiStringa("artcod"), 43, funzStringa.DX, ' ');
//                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(htot.get(rx.leggiStringa("artcod")), "####0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
//					Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, 576, 18);
//					posy += 30;
//					Env.cpclPrinter.printForm();
//				}
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, 576, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            cm.close();
            c.close();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    public static void stampaDocScaricoSede(int num, String data, Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            String[] pars = new String[3];
            pars[0] = Env.docscaricosede;
            pars[1] = data;
            pars[2] = "" + num;
            Cursor cm = Env.db.rawQuery("SELECT movnote2,movnote4,movnote6,movdocora FROM movimenti WHERE movdoc = ? AND movdata = ? AND movnum = ?", pars);
            cm.moveToFirst();
            String oradoc = cm.getString(3);

            ArrayList<Record> vrighe = new ArrayList();
            HashMap<String, Double> htot = new HashMap();
            Cursor c = Env.db.rawQuery("SELECT rmartcod,rmlotto,rmqta,rmcaumag,rmprz FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? ORDER BY rmartcod,rmlotto", pars);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", c.getString(0));
                rx.insStringa("lotto", c.getString(1));
                rx.insStringa("causale", c.getString(3));
                rx.insDouble("qta", c.getDouble(2));
                rx.insDouble("prezzo", c.getDouble(4));
                String[] parsa = new String[1];
                parsa[0] = c.getString(0);
                Cursor ca = Env.db.rawQuery("SELECT artdescr,artum FROM articoli WHERE artcod = ?", parsa);
                if (ca.moveToFirst()) {
                    rx.insStringa("artdescr", ca.getString(0));
                    rx.insStringa("um", ca.getString(1));
                } else {
                    rx.insStringa("artdescr", "");
                    rx.insStringa("um", "");
                }
                ca.close();
                vrighe.add(rx);
                if (htot.get(rx.leggiStringa("artcod")) == null) {
                    htot.put(rx.leggiStringa("artcod"), rx.leggiDouble("qta"));
                } else {
                    double qtaold = htot.get(rx.leggiStringa("artcod"));
                    htot.put(rx.leggiStringa("artcod"), OpValute.arrotondaMat(qtaold + rx.leggiDouble("qta"), 3));
                }
            }
            c.close();

            int nrigheint = 6;
            if (Env.stampasedesec)
                nrigheint += 4;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("SCARICO A SEDE", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "SCARICO A SEDE", lf, 24);
            posy += 30;

            String stmp = "N." + num + " Data:" + (new Data(data, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " ora:" + oradoc;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                     UM LOTTO       QTA CAU ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                          UM LOTTO       QTA    PREZZO CAU ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            for (int i = 0; i < vrighe.size(); i++) {
                Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Record rx = vrighe.get(i);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    String riga = "";
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 19, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                    String cau = "";
                    if (rx.leggiStringa("causale").equals("SS"))
                        cau = "VE";
                    else if (rx.leggiStringa("causale").equals("PE"))
                        cau = "RO";
                    else
                        cau = "SC";
                    riga += " " + funzStringa.riempiStringa(cau, 2, funzStringa.SX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    Env.cpclPrinter.printForm();
                    if ((i < vrighe.size() - 1 && !vrighe.get(i + 1).leggiStringa("artcod").equals(rx.leggiStringa("artcod"))) || (i == vrighe.size() - 1)) {
                        Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                        Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                        Env.cpclPrinter.setTone(20);
                        posy = 0;
                        riga = "";
                        riga += funzStringa.riempiStringa("Totale " + rx.leggiStringa("artcod"), 39, funzStringa.DX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(htot.get(rx.leggiStringa("artcod")), "####0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 18);
                        posy += 30;
                        Env.cpclPrinter.printForm();
                    }
                } else {
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    String riga = "";
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 24, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "####0.000", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                    String cau = "";
                    if (rx.leggiStringa("causale").equals("SS"))
                        cau = "VE";
                    else if (rx.leggiStringa("causale").equals("PE"))
                        cau = "RO";
                    else
                        cau = "SC";
                    riga += " " + funzStringa.riempiStringa(cau, 2, funzStringa.SX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    Env.cpclPrinter.printForm();
                    if ((i < vrighe.size() - 1 && !vrighe.get(i + 1).leggiStringa("artcod").equals(rx.leggiStringa("artcod"))) || (i == vrighe.size() - 1)) {
                        Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                        Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                        Env.cpclPrinter.setTone(20);
                        posy = 0;
                        riga = "";
                        riga += funzStringa.riempiStringa("Totale " + rx.leggiStringa("artcod"), 44, funzStringa.DX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(htot.get(rx.leggiStringa("artcod")), "####0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 18);
                        posy += 30;
                        Env.cpclPrinter.printForm();
                    }
                }
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 7 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "LEGENDA CAUSALI:", lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "VE = MERCE VENDIBILE", lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "RO = MERCE DANNEGGIATA", lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "SC = MERCE SCADUTA", lf, 20);
            posy += 30;
            Env.cpclPrinter.printForm();

            cm.close();
            c.close();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    public static void stampaDDT(String doccod, int num, String data, double imppag, double imppagscad,
                                 boolean cliblocco, ArrayList cliscop, boolean avvisopagtv, Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 770;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            String[] pars = new String[3];
            pars[0] = doccod;
            pars[1] = data;
            pars[2] = "" + num;
            Cursor cm = Env.db.rawQuery("SELECT movtipo,movdoc,movsez,movdata,movnum,movdocora,movclicod,movdestcod,movpagcod," +
                    "movnumcolli,movtotimp,movtotiva,movtotale,movacconto,movnrif,movcesscod,movabbuoni,movaccontotipocassa FROM movimenti WHERE movdoc = ? AND movdata = ? AND movnum = ?", pars);
            cm.moveToFirst();

            ArrayList<Record> vrighe = new ArrayList();
            Cursor c = Env.db.rawQuery("SELECT rmartcod,rmlotto,rmcaumag,rmcolli,rmpezzi,rmcontenuto,rmqta,rmprz,rmartsc1,rmartsc2," +
                    "rmscvend,rmcodiva,rmaliq,rmclisc1,rmclisc2,rmclisc3,rmlordo,rmnetto,rmnettoivato FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? ORDER BY rmriga", pars);
            while (c.moveToNext()) {
                Record rx = new Record();
                String artcod = c.getString(0);
                if (cm.getInt(0) == 1) {
                    String[] parscess = new String[2];
                    parscess[0] = cm.getString(15);
                    parscess[1] = artcod;
                    Cursor ccodcess = Env.db.rawQuery("SELECT dcartcodext FROM daticessionari WHERE dcclicod = ? AND dcartcod = ?", parscess);
                    if (ccodcess.moveToFirst() && !ccodcess.getString(0).equals("")) {
                        artcod = ccodcess.getString(0);
                    }
                    ccodcess.close();
                }
                rx.insStringa("artcod", artcod);
                String[] parsa = new String[1];
                parsa[0] = c.getString(0);
                Cursor ca = Env.db.rawQuery("SELECT artdescr,artum,artscprom,artscdatainizio,artscdatafine FROM articoli WHERE artcod = ?", parsa);
                if (ca.moveToFirst()) {
                    rx.insStringa("artdescr", ca.getString(0));
                    rx.insStringa("um", ca.getString(1));
                    if (ca.getDouble(2) != 0 && !ca.getString(3).equals("0000-00-00") && !ca.getString(4).equals("0000-00-00") &&
                            data.compareTo(ca.getString(3)) >= 0 && data.compareTo(ca.getString(4)) <= 0) {

                        rx.insDouble("scprom", ca.getDouble(2));
                        rx.insStringa("datainizioscprom", ca.getString(3));
                        rx.insStringa("datafinescprom", ca.getString(4));
                    } else {
                        rx.insDouble("scprom", 0);
                        rx.insStringa("datainizioscprom", "");
                        rx.insStringa("datafinescprom", "");
                    }
                } else {
                    rx.insStringa("artdescr", "");
                    rx.insStringa("um", "");
                    rx.insDouble("scprom", 0);
                    rx.insStringa("datainizioscprom", "");
                    rx.insStringa("datafinescprom", "");
                }
                ca.close();
                rx.insStringa("lotto", c.getString(1));
                rx.insStringa("causale", c.getString(2));
                rx.insIntero("colli", c.getInt(3));
                rx.insDouble("qta", c.getDouble(6));
                rx.insDouble("prezzo", c.getDouble(7));
                rx.insDouble("sc1", c.getDouble(8));
                rx.insDouble("sc2", c.getDouble(9));
                rx.insDouble("sc3", c.getDouble(10));
                rx.insStringa("codiva", c.getString(11));
                rx.insDouble("aliquota", c.getDouble(12));
                rx.insDouble("clisc1", c.getDouble(13));
                rx.insDouble("clisc2", c.getDouble(14));
                rx.insDouble("clisc3", c.getDouble(15));
                rx.insDouble("lordo", c.getDouble(16));
                rx.insDouble("netto", c.getDouble(17));
                vrighe.add(rx);
            }
            c.close();

            ArrayList<Record> vrigheraggr = new ArrayList();
            HashMap<Integer, ArrayList<Record>> hraggrlotti = new HashMap();
            if (Env.ddtstamparaggrlotti) {
                for (int i = 0; i < vrighe.size(); i++) {
                    Record r = vrighe.get(i);
                    boolean trovato = false;
                    for (int j = 0; j < vrigheraggr.size(); j++) {
                        Record r2 = vrigheraggr.get(j);
                        if (r.leggiStringa("artcod").equals(r2.leggiStringa("artcod")) &&
                                r.leggiStringa("causale").equals(r2.leggiStringa("causale")) &&
                                r.leggiDouble("prezzo") == r2.leggiDouble("prezzo") &&
                                r.leggiDouble("sc1") == r2.leggiDouble("sc1") &&
                                r.leggiDouble("sc2") == r2.leggiDouble("sc2") &&
                                r.leggiDouble("sc3") == r2.leggiDouble("sc3") &&
                                r.leggiStringa("codiva").equals(r2.leggiStringa("codiva")) &&
                                r.leggiDouble("clisc1") == r2.leggiDouble("clisc1") &&
                                r.leggiDouble("clisc2") == r2.leggiDouble("clisc2") &&
                                r.leggiDouble("clisc3") == r2.leggiDouble("clisc3")) {
                            double qtaold = r2.leggiDouble("qta");
                            int colliold = r2.leggiIntero("colli");
                            double lordoold = r2.leggiDouble("lordo");
                            double nettoold = r2.leggiDouble("netto");
                            r2.eliminaCampo("lordo");
                            r2.eliminaCampo("netto");
                            r2.eliminaCampo("qta");
                            r2.eliminaCampo("colli");
                            r2.insDouble("lordo", OpValute.arrotondaMat(r.leggiDouble("lordo") + lordoold, 2));
                            r2.insDouble("netto", OpValute.arrotondaMat(r.leggiDouble("netto") + nettoold, 2));
                            r2.insDouble("qta", OpValute.arrotondaMat(r.leggiDouble("qta") + qtaold, 3));
                            r2.insIntero("colli", r.leggiIntero("colli") + colliold);
                            int nriga = j;
                            ArrayList<Record> vl = hraggrlotti.get(new Integer(nriga));
                            Record rl = new Record();
                            rl.insStringa("lotto", r.leggiStringa("lotto"));
                            rl.insDouble("qtalotto", r.leggiDouble("qta"));
                            vl.add(rl);
                            trovato = true;
                            break;
                        }
                    }
                    if (!trovato) {
                        vrigheraggr.add(r);
                        Record rl = new Record();
                        rl.insStringa("lotto", r.leggiStringa("lotto"));
                        rl.insDouble("qtalotto", r.leggiDouble("qta"));
                        int nriga = vrigheraggr.size() - 1;
                        ArrayList<Record> vl = new ArrayList();
                        vl.add(rl);
                        hraggrlotti.put(new Integer(nriga), vl);
                    }
                }
            }

            String pclicod = "";
            String pclinome = "";
            String pcliinsegna = "";
            String pcliindir = "";
            String pcliloc = "";
            String pcliprov = "";
            String pclipiva = "";
            String pclispesecauz = "";
            String pclinumcontr = "";
            String pclidatacontr = "";
            String pclistampaprz = "";
            String lclicod = "";
            String lclinome = "";
            String lcliinsegna = "";
            String lcliindir = "";
            String lcliloc = "";
            String lcliprov = "";
            String cesscod = "";
            String cessnome = "";
            String cessindir = "";
            String cessloc = "";
            String cessprov = "";

            String[] parscli = new String[1];
            parscli[0] = cm.getString(6);
            Cursor ccli = Env.db.rawQuery(
                    "SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva,clispesecauz,clinumcontr,clidatacontr,clistampaprz,cliinsegna FROM clienti WHERE clicodice = ?", parscli);
            if (ccli.moveToFirst()) {
                pclicod = ccli.getString(0);
                pclinome = ccli.getString(1);
                pcliinsegna = ccli.getString(10);
                pcliindir = ccli.getString(2);
                pcliloc = ccli.getString(3);
                pcliprov = ccli.getString(4);
                pclipiva = ccli.getString(5);
                pclispesecauz = ccli.getString(6);
                pclinumcontr = ccli.getString(7);
                pclidatacontr = ccli.getString(8);
                pclistampaprz = ccli.getString(9);
            }
            ccli.close();

            if (!cm.getString(7).equals("")) {
                String[] parsdest = new String[1];
                parsdest[0] = cm.getString(7);
                Cursor cdest = Env.db.rawQuery(
                        "SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva,cliinsegna FROM clienti WHERE clicodice = ?", parsdest);
                if (cdest.moveToFirst()) {
                    lclicod = cdest.getString(0);
                    lclinome = cdest.getString(1);
                    lcliinsegna = cdest.getString(6);
                    lcliindir = cdest.getString(2);
                    lcliloc = cdest.getString(3);
                    lcliprov = cdest.getString(4);
                }
                cdest.close();
            }

            if (!cm.getString(15).equals("")) {
                String[] parscess = new String[1];
                parscess[0] = cm.getString(15);
                Cursor ccess = Env.db.rawQuery(
                        "SELECT clicodice,clinome,cliindir,cliloc,cliprov FROM clienti WHERE clicodice = ?", parscess);
                if (ccess.moveToFirst()) {
                    cesscod = ccess.getString(0);
                    cessnome = ccess.getString(1);
                    cessindir = ccess.getString(2);
                    cessloc = ccess.getString(3);
                    cessprov = ccess.getString(4);
                }
                ccess.close();
            }

            String pagdescr = "";
            int pagtipo = 0;
            String[] parsp = new String[1];
            parsp[0] = cm.getString(8);
            Cursor cpag = Env.db.rawQuery(
                    "SELECT pagdescr,pagtipo FROM pagamenti WHERE pagcod = ?", parsp);
            if (cpag.moveToFirst()) {
                pagdescr = cpag.getString(0);
                pagtipo = cpag.getInt(1);
            }
            cpag.close();

            boolean stampaprezzi = false;
            if (pagtipo == 8)
                stampaprezzi = true;
            else {
                if (!pclinumcontr.equals("") && !pclinumcontr.equals("0")) {
                    stampaprezzi = false;
                } else if (pclistampaprz.equals("S") || pclistampaprz.equals(""))
                    stampaprezzi = true;
                else
                    stampaprezzi = false;
            }
            if (cm.getInt(0) == 1) {
                // ddt qta no prezzi
                stampaprezzi = false;
            }

            // controllo incassi
            int ninc = 0;
            String[] parsinc = new String[1];
            parsinc[0] = cm.getString(6);
            Cursor cinc = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM incassi WHERE iscclicod = ?", parsinc);
            if (cinc.moveToFirst() && !cinc.isNull(0))
                ninc = cinc.getInt(0);
            cinc.close();

            // controllo se ci sono sospesi del giorno
            int nsosp = 0;
            Cursor csosp = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti LEFT JOIN pagamenti ON movimenti.movpagcod = pagamenti.pagcod WHERE movtrasf = 'N' AND pagtipo = 8 AND movacconto <> 0 and movclicod =?", parsinc);
            if (csosp.moveToFirst() && !csosp.isNull(0))
                nsosp = csosp.getInt(0);
            csosp.close();
            ArrayList<Record> vinc = new ArrayList();
            if (ninc > 0) {
                Cursor crinc = Env.db.rawQuery(
                        "SELECT isctipo,iscsezdoc,iscnumdoc,iscdatadoc,iscdatascad,isaldo FROM incassi WHERE iscclicod = ? ORDER BY iscdatascad DESC", parsinc);
                while (crinc.moveToNext()) {
                    Record rx = new Record();
                    rx.insStringa("tipo", crinc.getString(0));
                    rx.insStringa("sezdoc", crinc.getString(1));
                    rx.insIntero("numdoc", crinc.getInt(2));
                    rx.insStringa("datadoc", crinc.getString(3));
                    rx.insStringa("datascad", crinc.getString(4));
                    rx.insDouble("saldo", crinc.getDouble(5));
                    vinc.add(rx);
                }
                crinc.close();

                crinc = Env.db.rawQuery(
                        "SELECT movtipo,movsez,movnum,movdata,movacconto FROM movimenti LEFT JOIN pagamenti ON movimenti.movpagcod = pagamenti.pagcod where movtrasf = 'N' AND pagtipo=8 AND movacconto <> 0 and movclicod =?", parsinc);
                while (crinc.moveToNext()) {
                    Record rx = new Record();
                    if (crinc.getInt(0) == 0)
                        rx.insStringa("tipo", "B");
                    else if (crinc.getInt(0) == 1)
                        rx.insStringa("tipo", "F");
                    rx.insStringa("sezdoc", crinc.getString(1));
                    rx.insIntero("numdoc", crinc.getInt(2));
                    rx.insStringa("datadoc", crinc.getString(3));
                    rx.insStringa("datascad", "");
                    rx.insDouble("saldo", crinc.getDouble(4));
                    vinc.add(rx);
                }
                crinc.close();
            }

            // consegna fatture art62
            ArrayList<Record> vcf = new ArrayList();
            String[] parscf = new String[2];
            parscf[0] = cm.getString(6);
            parscf[1] = (new Data()).formatta(Data.AAAA_MM_GG, "-");
            Cursor ccf = Env.db.rawQuery(
                    "SELECT scdatadoc,scnumdoc FROM scoperti WHERE scclicod=? " +
                            "AND sctipo = 'F' AND scdataconsfat =? ORDER BY scdatadoc DESC,scnumdoc ASC", parscf);
            while (ccf.moveToNext()) {
                Record rx = new Record();
                rx.insIntero("numdoc", ccf.getInt(1));
                rx.insStringa("datadoc", ccf.getString(0));
                vcf.add(rx);
            }
            ccf.close();

            // elenco scoperti per blocco
            ArrayList<String> vcliscop = new ArrayList();
            if (cliblocco && cliscop.size() > 0) {
                for (int i = 0; i < cliscop.size(); i++) {
                    vcliscop.add((String) cliscop.get(i));
                }
            }

            // elenco scoperti in coda al ddt
            ArrayList<Record> vscop = new ArrayList();
            if (Env.ddtstampascop) {
                vscop = FunzioniJBeerApp.elencoScopertiCliente(cm.getString(6));
            }

            int nrigheformtesta = 27;
            if (Env.ddtstamparaggrlotti)
                nrigheformtesta--;
            if (Env.tipostampante == 1) {
                // 4 pollici dicitura art.62
                nrigheformtesta--;
            }
            if (Env.ddtdatiage == 0)
                nrigheformtesta += 6;
            else
                nrigheformtesta += 2;
            if (Env.stampasedesec)
                nrigheformtesta += 1;
            if (cm.getInt(0) == 1) {
                File flogoq = new File(context.getFilesDir() + File.separator + "logoazddtqta.png");
                if (flogoq.exists())
                    nrigheformtesta += Env.nrighelogo;
                else {
                    File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                    if (flogo.exists())
                        nrigheformtesta += Env.nrighelogo;
                }
            } else {
                File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                if (flogo.exists())
                    nrigheformtesta += Env.nrighelogo;
            }
            if (!cm.getString(15).equals("") && !Env.ddtqtanocess) // cessionario
                nrigheformtesta += 5;
            if (!cm.getString(14).equals("")) // n.rif
                nrigheformtesta += 1;
            if (!lclicod.equals(""))
                nrigheformtesta += 5;
            if (!lcliinsegna.equals(""))
                nrigheformtesta += 1;
            if (!pcliinsegna.equals(""))
                nrigheformtesta += 1;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheformtesta * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(10);
            Env.cpclPrinter.setSpeed(5);

            int posy = 0;
            //Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "PROPRIETARIO DELLA MERCE:", lf, 20);
            //posy += 30;

            if (cm.getInt(0) == 1) {
                File flogoq = new File(context.getFilesDir() + File.separator + "logoazddtqta.png");
                if (flogoq.exists()) {
                    try {
                        Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoazddtqta.png");
                        Env.cpclPrinter.printBitmap(bm, 0, posy);
                    } catch (Exception elogo) {
                        elogo.printStackTrace();
                    }
                    posy += 30 * Env.nrighelogo;
                } else {
                    File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                    if (flogo.exists()) {
                        try {
                            Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                            Env.cpclPrinter.printBitmap(bm, 0, posy);
                        } catch (Exception elogo) {
                            elogo.printStackTrace();
                        }
                        posy += 30 * Env.nrighelogo;
                    }
                }
            } else {
                File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                if (flogo.exists()) {
                    try {
                        Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                        Env.cpclPrinter.printBitmap(bm, 0, posy);
                    } catch (Exception elogo) {
                        elogo.printStackTrace();
                    }
                    posy += 30 * Env.nrighelogo;
                }
            }

//            try
//            {
//                Thread.currentThread().sleep(6000);
//            } catch (Exception ex)
//            {
//            }

            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga6 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga7 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 30;
            if (Env.stampasedesec) {
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                String lp = funzStringa.tagliaStringa("Luogo di partenza:" + cdep.getString(2) + " " + cdep.getString(0) + " " + cdep.getString(1), gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, lp, lf, 20);
                posy += 30;
                cdep.close();
            }
            if (cm.getInt(0) == 0) {
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "NOTA di consegna beni ceduti col sistema", lf, 20);
                    posy += 30;
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "della tentata vendita", lf, 20);
                    posy += 30;
                } else {
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "NOTA di consegna beni ceduti con sistema tentata vendita", lf, 20);
                    posy += 30;
                    posy += 30;
                }
            } else if (cm.getInt(0) == 1) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "DDT A QUANTITA'", lf, 20);
                posy += 30;
                posy += 30;
            } else if (cm.getInt(0) == 4) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "DDT DI RESO MERCE", lf, 20);
                posy += 30;
                posy += 30;
            }
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                    "N." + Env.pedcod + "/" + cm.getInt(4) + " del " +
                            (new Data(cm.getString(3), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " ore " + cm.getString(5), lf, 24);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                        "Assolve gli obblighi di cui all'articolo 62, comma 1,", lf, 18);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                        "del decreto legge 24 gennaio 2012, n. 1, convertito,", lf, 18);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                        "con modificazioni, dalla legge 24 marzo 2012, n. 27.", lf, 18);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                        "Durata Immediata.", lf, 18);
                posy += 30;
            } else {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                        "Assolve gli obblighi di cui all'articolo 62, comma 1, del decreto legge", lf, 18);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                        "24 gennaio 2012, n. 1, convertito, con modificazioni, dalla legge 24", lf, 18);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                        "marzo 2012, n. 27. Durata Immediata.", lf, 18);
                posy += 30;
            }

            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30; // 15
            if (!cm.getString(14).equals("")) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.RIF.CLIENTE: " + cm.getString(14), lf, 20);
                posy += 30;
            }
            stmp = "Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "VENDITORE / CARICATORE MERCE / VETTORE :", lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30; // 18

            if (Env.ddtdatiage == 0) {
                String[] parsage = new String[1];
                parsage[0] = Env.agecod;
                Cursor cage = Env.db.rawQuery("SELECT ageindirizzo,agelocalita,agepartitaiva,agecodicefiscale,agenregalbo,agetel,agefax,agemobile,ageemail FROM agenti WHERE agecod = ?", parsage);
                if (cage.moveToFirst()) {
                    stmp = funzStringa.tagliaStringa(cage.getString(0), gf20);
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cage.getString(1) + " ", gf20);
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("p.IVA:" + cage.getString(2) + " C.Fisc.:" + cage.getString(3), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("N.Iscr.Albo:" + cage.getString(4), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("tel.:" + cage.getString(5) + " mobile:" + cage.getString(7), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("E-Mail.:" + cage.getString(8), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                } else {
                    posy += 180;
                }
                cage.close();
            } else {
                String[] parsage = new String[1];
                parsage[0] = Env.agecod;
                Cursor cage = Env.db.rawQuery("SELECT agetel,agemobile FROM agenti WHERE agecod = ?", parsage);
                if (cage.moveToFirst()) {
                    stmp = funzStringa.tagliaStringa("tel.:" + cage.getString(0) + " mobile:" + cage.getString(1), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                } else {
                    posy += 30;
                }
                cage.close();
            }
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE / COMMITTENTE", lf, 20);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Codice:", lf, 20);
            Env.cpclPrinter.printAndroidFont(98, posy, Typeface.MONOSPACE, true, false, false, pclicod, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Nome:", lf, 20);
            if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                stmp = funzStringa.tagliaStringa(pclinome + " ", 40);
            } else {
                stmp = funzStringa.tagliaStringa(pclinome + " ", 58);
            }
            if (stmp.length() == 0)
                stmp = " ";
            Env.cpclPrinter.printAndroidFont(73, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (!pcliinsegna.equals("")) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Insegna:", lf, 20);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    stmp = funzStringa.tagliaStringa(pcliinsegna + " ", 37);
                } else {
                    stmp = funzStringa.tagliaStringa(pcliinsegna + " ", 55);
                }
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(73, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
            }
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Indirizzo:", lf, 20);
            stmp = funzStringa.tagliaStringa(pcliindir + " ", 50);
            if (stmp.length() == 0)
                stmp = " ";
            Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Localita':", lf, 20);
            stmp = funzStringa.tagliaStringa((pcliloc + " " + pcliprov).trim() + " ", 50);
            if (stmp.length() == 0)
                stmp = " ";
            Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Partita IVA:", lf, 20);
            stmp = pclipiva;
            if (stmp.length() == 0)
                stmp = " ";
            Env.cpclPrinter.printAndroidFont(159, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (!pclinumcontr.equals("")) {
                String dataContratto = "";
                if (pclidatacontr.equals("0000-00-00") || pclidatacontr.equals(""))
                    dataContratto = "";
                else
                    dataContratto = (new Data(pclidatacontr, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Contratto: " + pclinumcontr + " del " + dataContratto, lf, 16);
                posy += 30;
            } else {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Il contratto ha validità limitata alla presente cessione", lf, 16);
                posy += 30;
            }
            // 31

            if (!lclicod.equals("")) {
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " LUOGO DI CONSEGNA", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Codice:", lf, 20);
                Env.cpclPrinter.printAndroidFont(98, posy, Typeface.MONOSPACE, true, false, false, lclicod, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Nome:", lf, 20);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    stmp = funzStringa.tagliaStringa(lclinome + " ", 40);
                } else {
                    stmp = funzStringa.tagliaStringa(lclinome + " ", 55);
                }
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(73, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                if (!lcliinsegna.equals("")) {
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Insegna:", lf, 20);
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        stmp = funzStringa.tagliaStringa(lcliinsegna + " ", 37);
                    } else {
                        stmp = funzStringa.tagliaStringa(lcliinsegna + " ", 52);
                    }
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(73, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                    posy += 30;
                }
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Indirizzo:", lf, 20);
                stmp = funzStringa.tagliaStringa(lcliindir + " ", 50);
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Localita':", lf, 20);
                stmp = funzStringa.tagliaStringa((lcliloc + " " + lcliprov).trim(), 50);
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
            }
            if (!cesscod.equals("") && !Env.ddtqtanocess) {
                // cessionario
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "Merce consegnata in nome e per conto di:", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Codice:", lf, 20);
                Env.cpclPrinter.printAndroidFont(98, posy, Typeface.MONOSPACE, true, false, false, cesscod, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Nome:", lf, 20);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    stmp = funzStringa.tagliaStringa(cessnome + " ", 40);
                } else {
                    stmp = funzStringa.tagliaStringa(cessnome + " ", 55);
                }
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(73, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Indirizzo:", lf, 20);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    stmp = funzStringa.tagliaStringa(cessindir + " ", 35);
                } else {
                    stmp = funzStringa.tagliaStringa(cessindir + " ", 50);
                }
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Localita':", lf, 20);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    stmp = funzStringa.tagliaStringa(cessloc + cessprov + " ", 35);
                } else {
                    stmp = funzStringa.tagliaStringa(cessloc + cessprov + " ", 50);
                }
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
            }

            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (!Env.ddtstamparaggrlotti) {
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "COD.     LOTTO       DESCRIZIONE PRODOTTO            ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "COD.     LOTTO        DESCRIZIONE PRODOTTO                         ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "CAUSALE   UM QUANTITA' PRZ.UNIT. SCONTO   IMPORTO IVA", lf, 18);
                posy += 30;
            } else {
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                CAU. UM     QTA  PREZZO IVA ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                              CAU. UM       QTA    PREZZO IVA ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
            }
            Env.cpclPrinter.printForm();

            double totqta = 0;
            double totresi = 0;
            if (!Env.ddtstamparaggrlotti) {
                // stampa dettagliata
                Log.v("JAZZTV", "NRIGHE:" + vrighe.size());
                for (int i = 0; i < vrighe.size(); i++) {
                    Record rx = vrighe.get(i);
                    if (rx.leggiDouble("scprom") > 0)
                        Env.cpclPrinter.setForm(0, 200, 200, 4 * 30, 1);
                    else
                        Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(10);
                    Env.cpclPrinter.setSpeed(5);
                    posy = 0;
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        String riga = "";
                        riga += funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 11, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 31, funzStringa.SX, ' ');
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    } else {
                        String riga = "";
                        riga += funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 12, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 49, funzStringa.SX, ' ');
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    }
                    posy += 30;
                    String riga = "";
                    String cau = "";
                    boolean omaggio = false;
                    if (rx.leggiStringa("causale").equals("VE"))
                        cau = "VENDITA";
                    else if (rx.leggiStringa("causale").equals("RN")) {
                        cau = "RESO N.V.";
                        totresi += rx.leggiDouble("qta");
                    } else if (rx.leggiStringa("causale").equals("RV")) {
                        cau = "RESO";
                        totresi += rx.leggiDouble("qta");
                    } else if (rx.leggiStringa("causale").equals("OM") || rx.leggiStringa("causale").equals("OT")) {
                        cau = "OMAGGIO";
                        omaggio = true;
                    } else if (rx.leggiStringa("causale").equals("SM")) {
                        cau = "SC.MERCE";
                        omaggio = true;
                    } else if (rx.leggiStringa("causale").equals("VQ"))
                        cau = "VENDITA";
                    if (!rx.leggiStringa("causale").equals("RN") && !rx.leggiStringa("causale").equals("RV"))
                        totqta += rx.leggiDouble("qta");
                    riga += funzStringa.riempiStringa(cau, 9, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.000", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                    if (stampaprezzi && !omaggio) {
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                        stmp = "";
                        if (rx.leggiDouble("sc2") > 0) {
                            stmp += Formattazione.formatta(rx.leggiDouble("sc2"), "#0.0", Formattazione.NO_SEGNO);
                        }
                        if (rx.leggiDouble("sc1") > 0) {
                            stmp += " " + Formattazione.formatta(rx.leggiDouble("sc1"), "#0.0", Formattazione.NO_SEGNO);
                        }
                        riga += " " + funzStringa.riempiStringa(stmp, 9, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("netto"), "###0.00", Formattazione.SEGNO_SX), 7, funzStringa.DX, ' ');
                        if (rx.leggiDouble("aliquota") > 0) {
                            riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("aliquota"), "#0", Formattazione.NO_SEGNO), 3, funzStringa.DX, ' ');
                        } else {
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("codiva"), 3, funzStringa.SX, ' ');
                        }
                    }
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    if (rx.leggiDouble("scprom") > 0) {
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "--- In promozione dal " +
                                (new Data(rx.leggiStringa("datainizioscprom"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/") +
                                " al " + (new Data(rx.leggiStringa("datafinescprom"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/") + " ---", lf, 18);
                        posy += 30;
                    }
                    Env.cpclPrinter.setPattern(CPCLConst.LK_CPCL_VERTICAL_PATTERN);
                    Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                    Env.cpclPrinter.setPattern(CPCLConst.LK_CPCL_DEFAULT_PATTERN);
                    posy += 30;
                    Env.cpclPrinter.printForm();

                    if (i % Env.stampe_par1 == 0) {
                        try {
                            Thread.currentThread().sleep(5000);
                        } catch (Exception ex) {
                        }
                    }

//                    if (Env.stampe_par1 > 4)
//                    {
//                        if (i % Env.stampe_par1 == 0 && i > 0)
//                        {
//                            int ntentd = 0;
//                            while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntentd < 10)
//                            {
//                                Log.v("JAZZTV", "SPEED: BUSY corpo ntent:" + ntentd);
//                                try
//                                {
//                                    Thread.currentThread().sleep(Env.stampe_par2);
//                                } catch (Exception ex)
//                                {
//                                }
//                                ntentd++;
//                            }
//                        }
//                    }
//                    else
//                    {
//                        if (i % Env.stampe_par1 == 0)
//                        {
//                            int ntentd = 0;
//                            while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntentd < 10)
//                            {
//                                Log.v("JAZZTV", "BUSY corpo ntent:" + ntentd);
//                                try
//                                {
//                                    Thread.currentThread().sleep(Env.stampe_par2);
//                                } catch (Exception ex)
//                                {
//                                }
//                                ntentd++;
//                            }
//                        }
//                    }

                }
            } else {
                // stampa raggruppata
                Log.v("JAZZTV", "NRIGHE RAGGR:" + vrigheraggr.size());
                for (int i = 0; i < vrigheraggr.size(); i++) {
                    Record rx = vrigheraggr.get(i);
                    ArrayList<Record> vl = hraggrlotti.get(new Integer(i));
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        // riga articolo
                        Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                        Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                        Env.cpclPrinter.setTone(10);
                        Env.cpclPrinter.setSpeed(5);
                        posy = 0;
                        String riga = "";
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 14, funzStringa.SX, ' ');
                        String cau = "";
                        boolean omaggio = false;
                        if (rx.leggiStringa("causale").equals("VE"))
                            cau = "VEND";
                        else if (rx.leggiStringa("causale").equals("RN")) {
                            cau = "RESO";
                            totresi += rx.leggiDouble("qta");
                        } else if (rx.leggiStringa("causale").equals("RV")) {
                            cau = "RESO";
                            totresi += rx.leggiDouble("qta");
                        } else if (rx.leggiStringa("causale").equals("OM") || rx.leggiStringa("causale").equals("OT")) {
                            cau = "OMAG";
                            omaggio = true;
                        } else if (rx.leggiStringa("causale").equals("SM")) {
                            cau = "SC.M";
                            omaggio = true;
                        } else if (rx.leggiStringa("causale").equals("VQ"))
                            cau = "VEND";
                        if (!rx.leggiStringa("causale").equals("RN") && !rx.leggiStringa("causale").equals("RV"))
                            totqta += rx.leggiDouble("qta");
                        riga += " " + funzStringa.riempiStringa(cau, 4, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "##0.000", Formattazione.NO_SEGNO), 7, funzStringa.DX, ' ');
                        if (stampaprezzi && !omaggio) {
                            riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "###0.00", Formattazione.NO_SEGNO), 7, funzStringa.DX, ' ');
                            if (rx.leggiDouble("aliquota") > 0) {
                                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("aliquota"), "#0", Formattazione.NO_SEGNO), 3, funzStringa.DX, ' ');
                            } else {
                                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("codiva"), 3, funzStringa.SX, ' ');
                            }
                        } else {
                            riga += funzStringa.spazi(12);
                        }
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                        Env.cpclPrinter.printForm();
                        // righe lotti
                        String slotti = "LOTTI:";
                        ArrayList<String> vsl = new ArrayList();
                        for (int nl = 0; nl < vl.size(); nl++) {
                            Record rl = vl.get(nl);
                            String sl = "[" + rl.leggiStringa("lotto") + "=";
                            String sql = Formattazione.formatta(rl.leggiDouble("qtalotto"), "######0.000", Formattazione.NO_SEGNO);
                            while (sql.endsWith("0"))
                                sql = sql.substring(0, sql.length() - 1);
                            if (sql.endsWith(","))
                                sql = sql.substring(0, sql.length() - 1);
                            sl += sql + "]";
                            if (slotti.length() + sl.length() > 51) {
                                vsl.add(slotti);
                                slotti = "LOTTI:" + sl;
                            } else {
                                slotti += sl;
                            }
                        }
                        vsl.add(slotti);
                        for (int nl = 0; nl < vsl.size(); nl++) {
                            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                            Env.cpclPrinter.setTone(10);
                            Env.cpclPrinter.setSpeed(5);
                            posy = 0;
                            stmp = funzStringa.riempiStringa(vsl.get(nl), 51, funzStringa.SX, ' ');
                            if (stmp.length() == 0)
                                stmp = " ";
                            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                            Env.cpclPrinter.printForm();
                        }
                    } else {
                        // riga articolo
                        Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                        Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                        Env.cpclPrinter.setTone(10);
                        Env.cpclPrinter.setSpeed(5);
                        posy = 0;
                        String riga = "";
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 28, funzStringa.SX, ' ');
                        String cau = "";
                        boolean omaggio = false;
                        if (rx.leggiStringa("causale").equals("VE"))
                            cau = "VEND";
                        else if (rx.leggiStringa("causale").equals("RN")) {
                            cau = "RESO";
                            totresi += rx.leggiDouble("qta");
                        } else if (rx.leggiStringa("causale").equals("RV")) {
                            cau = "RESO";
                            totresi += rx.leggiDouble("qta");
                        } else if (rx.leggiStringa("causale").equals("OM") || rx.leggiStringa("causale").equals("OT")) {
                            cau = "OMAG";
                            omaggio = true;
                        } else if (rx.leggiStringa("causale").equals("SM")) {
                            cau = "SC.M";
                            omaggio = true;
                        } else if (rx.leggiStringa("causale").equals("VQ"))
                            cau = "VEND";
                        if (!rx.leggiStringa("causale").equals("RN") && !rx.leggiStringa("causale").equals("RV"))
                            totqta += rx.leggiDouble("qta");
                        riga += " " + funzStringa.riempiStringa(cau, 4, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.000", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                        if (stampaprezzi && !omaggio) {
                            riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "####0.000", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                            if (rx.leggiDouble("aliquota") > 0) {
                                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("aliquota"), "#0", Formattazione.NO_SEGNO), 3, funzStringa.DX, ' ');
                            } else {
                                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("codiva"), 3, funzStringa.SX, ' ');
                            }
                        } else {
                            riga += funzStringa.spazi(14);
                        }
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                        Env.cpclPrinter.printForm();
                        // righe lotti
                        String slotti = "LOTTI:";
                        ArrayList<String> vsl = new ArrayList();
                        for (int nl = 0; nl < vl.size(); nl++) {
                            Record rl = vl.get(nl);
                            String sl = "[" + rl.leggiStringa("lotto") + "=";
                            String sql = Formattazione.formatta(rl.leggiDouble("qtalotto"), "######0.000", Formattazione.NO_SEGNO);
                            while (sql.endsWith("0"))
                                sql = sql.substring(0, sql.length() - 1);
                            if (sql.endsWith(","))
                                sql = sql.substring(0, sql.length() - 1);
                            sl += sql + "]";
                            if (slotti.length() + sl.length() > 68) {
                                vsl.add(slotti);
                                slotti = "LOTTI:" + sl;
                            } else {
                                slotti += sl;
                            }
                        }
                        vsl.add(slotti);
                        for (int nl = 0; nl < vsl.size(); nl++) {
                            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                            Env.cpclPrinter.setTone(10);
                            Env.cpclPrinter.setSpeed(5);
                            posy = 0;
                            stmp = funzStringa.riempiStringa(vsl.get(nl), 65, funzStringa.SX, ' ');
                            if (stmp.length() == 0)
                                stmp = " ";
                            Log.v("JAZZTV", stmp);
                            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                            Env.cpclPrinter.printForm();
                        }
                    }
                    if (i % Env.stampe_par1 == 0) {
                        System.out.println("PRINTER SLEEP");
                        try {
                            Thread.currentThread().sleep(5000);
                        } catch (Exception ex) {
                        }
//                        int ntentd = 0;
//                        while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntentd < 10)
//                        {
//                            Log.v("JAZZTV", "BUSY corpo ntent:" + ntentd);
//                            try
//                            {
//                                Thread.currentThread().sleep(Env.stampe_par2);
//                            } catch (Exception ex)
//                            {
//                            }
//                            ntentd++;
//                        }
                    }
                }
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.setPattern(CPCLConst.LK_CPCL_VERTICAL_PATTERN);
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                Env.cpclPrinter.setPattern(CPCLConst.LK_CPCL_DEFAULT_PATTERN);
                posy += 30;
                Env.cpclPrinter.printForm();
            }

            try {
                Thread.currentThread().sleep(7000);
            } catch (Exception ex) {
            }

            //if (i % Env.stampe_par1 == 0 && i > 0)
            {
//                try
//                {
//                    Thread.currentThread().sleep(4000);
//                } catch (Exception ex)
//                {
//                }
//                int ntentd = 0;
//                while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntentd < 10)
//                {
//                    Log.v("JAZZTV", "SPEED: BUSY fine corpo ntent:" + ntentd);
//                    try
//                    {
//                        Thread.currentThread().sleep(Env.stampe_par2);
//                    } catch (Exception ex)
//                    {
//                    }
//                    ntentd++;
//                }
            }


            int nrighepiede = 17;
            if (stampaprezzi)
                nrighepiede++;
            if (pagtipo == 8 && imppag >= 0)
                nrighepiede++;
            else if (pagtipo == 8 && imppag < 0)
                nrighepiede++;
            if (cm.getDouble(16) != 0)
                nrighepiede++;
            else if (pagtipo == 8 && imppagscad != 0)
                nrighepiede++;
            if (ninc > 0)
                nrighepiede += 3 + vinc.size();
            else {
                if (Env.ddtstampatimbro)
                    nrighepiede += 8;
            }
            if (vcf.size() > 0)
                nrighepiede += 2 + vcf.size();
            if (cliblocco && vcliscop.size() > 0)
                nrighepiede += 3 + vcliscop.size();

            if (vscop.size() > 0 && cm.getInt(0) != 1) {
                nrighepiede += 7 + vscop.size();
            }

            Env.cpclPrinter.setForm(0, 200, 200, nrighepiede * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;

            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, " Tot.imponibile     Tot. IVA             Totale", lf, 20);
            posy += 30;
            if (stampaprezzi) {
                stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(10), "######0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(61, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(11), "######0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(220, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                if (cm.getDouble(12) >= 0) {
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(12), "#####0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
                } else {
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(12), "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
                }
                posy += 30;
            }
            if (pagtipo == 8 && imppag >= 0) {
                String tipocassa = "       Tot.incassato in CONTANTI:";
                if (cm.getInt(17) == 1)
                    tipocassa = "           Tot.incassato con POS:";
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, tipocassa, lf, 20);
                stmp = funzStringa.riempiStringa(Formattazione.formatta(imppag, "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
                posy += 30;
                if (cm.getDouble(16) != 0) {
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "                         Abbuono:", lf, 20);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(16), "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
                    posy += 30;
                }
            } else if (pagtipo == 8 && imppag < 0) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "            Uscita di cassa:", lf, 20);
                stmp = funzStringa.riempiStringa(Formattazione.formatta(imppag, "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
                posy += 30;
                if (cm.getDouble(16) != 0) {
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "                         Abbuono:", lf, 20);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(16), "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
                    posy += 30;
                }
            } else if (pagtipo == 8 && imppagscad != 0) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "         Tot.rate incassate:", lf, 20);
                stmp = funzStringa.riempiStringa(Formattazione.formatta(imppagscad, "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
                posy += 30;
            }

            stmp = "Tot.qta:" + Formattazione.formatta(totqta, "#####0.000", Formattazione.NO_SEGNO);
            if (totresi != 0)
                stmp += "      Tot.resi:" + Formattazione.formatta(totresi, "#####0.000", Formattazione.NO_SEGNO);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Pagamento: " + cm.getString(8) + "-" + pagdescr, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;

            if (ninc > 0) {
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " RICEVUTA DI PAGAMENTO", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " TIPO DOC.  N.DOC.       DATA DOC.DATA SCAD.   SALDO ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                for (int i = 0; i < vinc.size(); i++) {
                    Record rx = vinc.get(i);
                    stmp = "";
                    if (rx.leggiStringa("tipo").equals("F"))
                        stmp = "Fattura";
                    else if (rx.leggiStringa("tipo").equals("B"))
                        stmp = "DDT";
                    else if (rx.leggiStringa("tipo").equals("C"))
                        stmp = "Credito g.";
                    else if (rx.leggiStringa("tipo").equals("N"))
                        stmp = "Nota cred.";
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    if (rx.leggiIntero("numdoc") > 0)
                        stmp = rx.leggiStringa("sezdoc") + "/" + rx.leggiIntero("numdoc");
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(130, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    if (!rx.leggiStringa("datadoc").equals("") && !rx.leggiStringa("datadoc").equals("0000-00-00"))
                        stmp = (new Data(rx.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(272, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    if (!rx.leggiStringa("datascad").equals("") && !rx.leggiStringa("datascad").equals("0000-00-00"))
                        stmp = (new Data(rx.leggiStringa("datascad"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(369, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("saldo"), "#####0.00", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(467, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    posy += 30;
                }
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
            } else {
                if (Env.ddtstampatimbro) {
                    Env.cpclPrinter.printBox(0, posy, lf, posy + 30 * 7, 1);
                    Env.cpclPrinter.printAndroidFont(10, posy + 2, Typeface.MONOSPACE, true, false, false, "TIMBRO", lf, 20);
                    posy += 30 * 8;
                }
            }

            if (vcf.size() > 0) {
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " FATTURE CONSEGNATE", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                for (int i = 0; i < vcf.size(); i++) {
                    Record rx = vcf.get(i);
                    stmp = "Fattura Num." + rx.leggiIntero("numdoc");
                    if (!rx.leggiStringa("datadoc").equals("") && !rx.leggiStringa("datadoc").equals("0000-00-00"))
                        stmp += " del " + (new Data(rx.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    posy += 30;
                }
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
            }

            if (cliblocco && vcliscop.size() > 0) {
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " SCADENZE DA SALDARE", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "Data scad. Importo       Num.doc.    Data doc. ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                for (int i = 0; i < vcliscop.size(); i++) {
                    String txt = vcliscop.get(i);
                    if (txt.length() == 0)
                        txt = " ";
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, txt, lf, 18);
                    posy += 30;
                }
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
            }

            if (vscop.size() > 0 && cm.getInt(0) != 1) {
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " Riconosco il debito residuo come da elenco", lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " fatture che segue:", lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " ELENCO SCOPERTI DEL CLIENTE", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " TIPO DOC.  N.DOC.       DATA DOC.DATA SCAD. RESIDUO ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                double totscop = 0;
                for (int i = 0; i < vscop.size(); i++) {
                    Record rx = vscop.get(i);
                    stmp = "";
                    if (rx.leggiStringa("tipo").equals("F"))
                        stmp = "Fattura";
                    else if (rx.leggiStringa("tipo").equals("B"))
                        stmp = "DDT";
                    else if (rx.leggiStringa("tipo").equals("C"))
                        stmp = "Credito g.";
                    else if (rx.leggiStringa("tipo").equals("N"))
                        stmp = "Nota cred.";
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    if (rx.leggiIntero("numdoc") > 0)
                        stmp = rx.leggiStringa("sezdoc") + "/" + rx.leggiIntero("numdoc");
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(130, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    if (!rx.leggiStringa("datadoc").equals("") && !rx.leggiStringa("datadoc").equals("0000-00-00"))
                        stmp = (new Data(rx.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(272, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    if (!rx.leggiStringa("datascad").equals("") && !rx.leggiStringa("datascad").equals("0000-00-00"))
                        stmp = (new Data(rx.leggiStringa("datascad"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        stmp = " ";
                    double segno = 1;
                    if (rx.leggiStringa("tipoop").equals("V"))
                        segno = 1;
                    else
                        segno = -1;
                    Env.cpclPrinter.printAndroidFont(369, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(segno * rx.leggiDouble("residuoscad"), "#####0.00", Formattazione.SEGNO_SX), 9, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(467, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    posy += 30;
                    totscop += segno * rx.leggiDouble("residuoscad");
                }
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
                totscop = OpValute.arrotondaMat(totscop, 2);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE SCOPERTI DEL CLIENTE: " + Formattazione.formatta(totscop, " ######0.00", Formattazione.SEGNO_DX), lf, 24);
                posy += 30;
                posy += 30;
            }

            posy += 30;
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, " Dati compilatore            Firma destinatario", lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, Env.agenome, lf, 20);
            posy += 30;
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;

            Env.cpclPrinter.printForm();

            cm.close();
            c.close();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }


    public static void stampaOrdineCliente(String doccod, int num, String data, Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            String[] pars = new String[3];
            pars[0] = doccod;
            pars[1] = data;
            pars[2] = "" + num;
            Cursor cm = Env.db.rawQuery("SELECT movtipo,movdoc,movsez,movdata,movnum,movdocora,movclicod,movdestcod,movpagcod," +
                    "movnumcolli,movtotimp,movtotiva,movtotale,movacconto,movnrif,movcesscod,movdatacons,movnote1,movnote2,movnote3 FROM movimenti WHERE movdoc = ? AND movdata = ? AND movnum = ?", pars);
            cm.moveToFirst();

            ArrayList<Record> vrighe = new ArrayList();
            Cursor c = Env.db.rawQuery("SELECT rmartcod,rmlotto,rmcaumag,rmcolli,rmpezzi,rmcontenuto,rmqta,rmprz,rmartsc1,rmartsc2," +
                    "rmscvend,rmcodiva,rmaliq,rmclisc1,rmclisc2,rmclisc3,rmlordo,rmnetto,rmnettoivato FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? ORDER BY rmriga", pars);
            while (c.moveToNext()) {
                Record rx = new Record();
                String artcod = c.getString(0);
                if (cm.getInt(0) == 1) {
                    String[] parscess = new String[2];
                    parscess[0] = cm.getString(15);
                    parscess[1] = artcod;
                    Cursor ccodcess = Env.db.rawQuery("SELECT dcartcodext FROM daticessionari WHERE dcclicod = ? AND dcartcod = ?", parscess);
                    if (ccodcess.moveToFirst() && !ccodcess.getString(0).equals("")) {
                        artcod = ccodcess.getString(0);
                    }
                    ccodcess.close();
                }
                rx.insStringa("artcod", artcod);
                String[] parsa = new String[1];
                parsa[0] = c.getString(0);
                Cursor ca = Env.db.rawQuery("SELECT artdescr,artum FROM articoli WHERE artcod = ?", parsa);
                if (ca.moveToFirst()) {
                    rx.insStringa("artdescr", ca.getString(0));
                    rx.insStringa("um", ca.getString(1));
                } else {
                    rx.insStringa("artdescr", "");
                    rx.insStringa("um", "");
                }
                ca.close();
                rx.insStringa("lotto", c.getString(1));
                rx.insStringa("causale", c.getString(2));
                rx.insIntero("colli", c.getInt(3));
                rx.insDouble("qta", c.getDouble(6));
                rx.insDouble("prezzo", c.getDouble(7));
                rx.insDouble("sc1", c.getDouble(8));
                rx.insDouble("sc2", c.getDouble(9));
                rx.insDouble("sc3", c.getDouble(10));
                rx.insStringa("codiva", c.getString(11));
                rx.insDouble("aliquota", c.getDouble(12));
                rx.insDouble("clisc1", c.getDouble(13));
                rx.insDouble("clisc2", c.getDouble(14));
                rx.insDouble("clisc3", c.getDouble(15));
                rx.insDouble("lordo", c.getDouble(16));
                rx.insDouble("netto", c.getDouble(17));
                vrighe.add(rx);
            }
            c.close();

            ArrayList<Record> vrigheraggr = new ArrayList();
            HashMap<Integer, ArrayList<Record>> hraggrlotti = new HashMap();
            if (Env.ddtstamparaggrlotti) {
                for (int i = 0; i < vrighe.size(); i++) {
                    Record r = vrighe.get(i);
                    boolean trovato = false;
                    for (int j = 0; j < vrigheraggr.size(); j++) {
                        Record r2 = vrigheraggr.get(j);
                        if (r.leggiStringa("artcod").equals(r2.leggiStringa("artcod")) &&
                                r.leggiStringa("causale").equals(r2.leggiStringa("causale")) &&
                                r.leggiDouble("prezzo") == r2.leggiDouble("prezzo") &&
                                r.leggiDouble("sc1") == r2.leggiDouble("sc1") &&
                                r.leggiDouble("sc2") == r2.leggiDouble("sc2") &&
                                r.leggiDouble("sc3") == r2.leggiDouble("sc3") &&
                                r.leggiStringa("codiva").equals(r2.leggiStringa("codiva")) &&
                                r.leggiDouble("clisc1") == r2.leggiDouble("clisc1") &&
                                r.leggiDouble("clisc2") == r2.leggiDouble("clisc2") &&
                                r.leggiDouble("clisc3") == r2.leggiDouble("clisc3")) {
                            double qtaold = r2.leggiDouble("qta");
                            int colliold = r2.leggiIntero("colli");
                            double lordoold = r2.leggiDouble("lordo");
                            double nettoold = r2.leggiDouble("netto");
                            r2.eliminaCampo("lordo");
                            r2.eliminaCampo("netto");
                            r2.eliminaCampo("qta");
                            r2.eliminaCampo("colli");
                            r2.insDouble("lordo", OpValute.arrotondaMat(r.leggiDouble("lordo") + lordoold, 2));
                            r2.insDouble("netto", OpValute.arrotondaMat(r.leggiDouble("netto") + nettoold, 2));
                            r2.insDouble("qta", OpValute.arrotondaMat(r.leggiDouble("qta") + qtaold, 3));
                            r2.insIntero("colli", r.leggiIntero("colli") + colliold);
                            int nriga = j;
                            ArrayList<Record> vl = hraggrlotti.get(new Integer(nriga));
                            Record rl = new Record();
                            rl.insStringa("lotto", r.leggiStringa("lotto"));
                            rl.insDouble("qtalotto", r.leggiDouble("qta"));
                            vl.add(rl);
                            trovato = true;
                            break;
                        }
                    }
                    if (!trovato) {
                        vrigheraggr.add(r);
                        Record rl = new Record();
                        rl.insStringa("lotto", r.leggiStringa("lotto"));
                        rl.insDouble("qtalotto", r.leggiDouble("qta"));
                        int nriga = vrigheraggr.size() - 1;
                        ArrayList<Record> vl = new ArrayList();
                        vl.add(rl);
                        hraggrlotti.put(new Integer(nriga), vl);
                    }
                }
            }

            String pclicod = "";
            String pclinome = "";
            String pcliindir = "";
            String pcliloc = "";
            String pcliprov = "";
            String pclipiva = "";
            String pclispesecauz = "";
            String pclinumcontr = "";
            String pclidatacontr = "";
            String pclistampaprz = "";
            String lclicod = "";
            String lclinome = "";
            String lcliindir = "";
            String lcliloc = "";
            String lcliprov = "";
            String cesscod = "";
            String cessnome = "";

            String[] parscli = new String[1];
            parscli[0] = cm.getString(6);
            Cursor ccli = Env.db.rawQuery(
                    "SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva,clispesecauz,clinumcontr,clidatacontr,clistampaprz FROM clienti WHERE clicodice = ?", parscli);
            if (ccli.moveToFirst()) {
                pclicod = ccli.getString(0);
                pclinome = ccli.getString(1);
                pcliindir = ccli.getString(2);
                pcliloc = ccli.getString(3);
                pcliprov = ccli.getString(4);
                pclipiva = ccli.getString(5);
                pclispesecauz = ccli.getString(6);
                pclinumcontr = ccli.getString(7);
                pclidatacontr = ccli.getString(8);
                pclistampaprz = ccli.getString(9);
            }
            ccli.close();

            if (!cm.getString(7).equals("")) {
                String[] parsdest = new String[1];
                parsdest[0] = cm.getString(7);
                Cursor cdest = Env.db.rawQuery(
                        "SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva FROM clienti WHERE clicodice = ?", parsdest);
                if (cdest.moveToFirst()) {
                    lclicod = cdest.getString(0);
                    lclinome = cdest.getString(1);
                    lcliindir = cdest.getString(2);
                    lcliloc = cdest.getString(3);
                    lcliprov = cdest.getString(4);
                }
                cdest.close();
            }

            if (!cm.getString(15).equals("")) {
                String[] parscess = new String[1];
                parscess[0] = cm.getString(15);
                Cursor ccess = Env.db.rawQuery(
                        "SELECT clicodice,clinome FROM clienti WHERE clicodice = ?", parscess);
                if (ccess.moveToFirst()) {
                    cesscod = ccess.getString(0);
                    cessnome = ccess.getString(1);
                }
                ccess.close();
            }

            String pagdescr = "";
            int pagtipo = 0;
            String[] parsp = new String[1];
            parsp[0] = cm.getString(8);
            Cursor cpag = Env.db.rawQuery(
                    "SELECT pagdescr,pagtipo FROM pagamenti WHERE pagcod = ?", parsp);
            if (cpag.moveToFirst()) {
                pagdescr = cpag.getString(0);
                pagtipo = cpag.getInt(1);
            }
            cpag.close();

            int nrigheformtesta = 22;
            if (Env.ddtdatiage == 0)
                nrigheformtesta += 6;
            else
                nrigheformtesta += 2;
            if (Env.stampasedesec)
                nrigheformtesta += 1;
            if (cm.getInt(0) == 1) {
                File flogoq = new File(context.getFilesDir() + File.separator + "logoazddtqta.png");
                if (flogoq.exists())
                    nrigheformtesta += Env.nrighelogo;
                else {
                    File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                    if (flogo.exists())
                        nrigheformtesta += Env.nrighelogo;
                }
            } else {
                File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                if (flogo.exists())
                    nrigheformtesta += Env.nrighelogo;
            }
            if (!lclicod.equals(""))
                nrigheformtesta += 5;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheformtesta * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(10);
            Env.cpclPrinter.setSpeed(5);

            int posy = 0;
            if (cm.getInt(0) == 1) {
                File flogoq = new File(context.getFilesDir() + File.separator + "logoazddtqta.png");
                if (flogoq.exists()) {
                    try {
                        Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoazddtqta.png");
                        Env.cpclPrinter.printBitmap(bm, 0, posy);
                    } catch (Exception elogo) {
                        elogo.printStackTrace();
                    }
                    posy += 30 * Env.nrighelogo;
                } else {
                    File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                    if (flogo.exists()) {
                        try {
                            Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                            Env.cpclPrinter.printBitmap(bm, 0, posy);
                        } catch (Exception elogo) {
                            elogo.printStackTrace();
                        }
                        posy += 30 * Env.nrighelogo;
                    }
                }
            } else {
                File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                if (flogo.exists()) {
                    try {
                        Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                        Env.cpclPrinter.printBitmap(bm, 0, posy);
                    } catch (Exception elogo) {
                        elogo.printStackTrace();
                    }
                    posy += 30 * Env.nrighelogo;
                }
            }
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga6 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga7 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 30;
            if (Env.stampasedesec) {
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                String lp = funzStringa.tagliaStringa("Luogo di partenza:" + cdep.getString(2) + " " + cdep.getString(0) + " " + cdep.getString(1), gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, lp, lf, 20);
                posy += 30;
                cdep.close();
            }
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "ORDINE CLIENTE", lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                    "N." + Env.pedcod + "/" + cm.getInt(4) + " del " +
                            (new Data(cm.getString(3), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " ore " + cm.getString(5), lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                    "DATA CONSEGNA MERCE: " + (new Data(cm.getString(16), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"), lf, 24);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            stmp = "Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "VENDITORE / CARICATORE MERCE / VETTORE :", lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;

            if (Env.ddtdatiage == 0) {
                String[] parsage = new String[1];
                parsage[0] = Env.agecod;
                Cursor cage = Env.db.rawQuery("SELECT ageindirizzo,agelocalita,agepartitaiva,agecodicefiscale,agenregalbo,agetel,agefax,agemobile,ageemail FROM agenti WHERE agecod = ?", parsage);
                if (cage.moveToFirst()) {
                    stmp = funzStringa.tagliaStringa(cage.getString(0), gf20);
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cage.getString(1) + " ", gf20);
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("p.IVA:" + cage.getString(2) + " C.Fisc.:" + cage.getString(3), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("N.Iscr.Albo:" + cage.getString(4), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("tel.:" + cage.getString(5) + " mobile:" + cage.getString(7), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("E-Mail.:" + cage.getString(8), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                } else {
                    posy += 180;
                }
                cage.close();
            } else {
                String[] parsage = new String[1];
                parsage[0] = Env.agecod;
                Cursor cage = Env.db.rawQuery("SELECT agetel,agemobile FROM agenti WHERE agecod = ?", parsage);
                if (cage.moveToFirst()) {
                    stmp = funzStringa.tagliaStringa("tel.:" + cage.getString(0) + " mobile:" + cage.getString(1), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                } else {
                    posy += 30;
                }
                cage.close();
            }
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE / COMMITTENTE", lf, 20);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Codice:", lf, 20);
            Env.cpclPrinter.printAndroidFont(98, posy, Typeface.MONOSPACE, true, false, false, pclicod, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Nome:", lf, 20);
            stmp = funzStringa.tagliaStringa(pclinome + " ", 55);
            if (stmp.length() == 0)
                stmp = " ";
            Env.cpclPrinter.printAndroidFont(73, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Indirizzo:", lf, 20);
            stmp = funzStringa.tagliaStringa(pcliindir + " ", 50);
            if (stmp.length() == 0)
                stmp = " ";
            Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Localita':", lf, 20);
            stmp = funzStringa.tagliaStringa((pcliloc + " " + pcliprov).trim() + " ", 50);
            if (stmp.length() == 0)
                stmp = " ";
            Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Partita IVA:", lf, 20);
            stmp = pclipiva;
            if (stmp.length() == 0)
                stmp = " ";
            Env.cpclPrinter.printAndroidFont(159, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (!pclinumcontr.equals("")) {
                String dataContratto = "";
                if (pclidatacontr.equals("0000-00-00") || pclidatacontr.equals(""))
                    dataContratto = "";
                else
                    dataContratto = (new Data(pclidatacontr, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Contratto: " + pclinumcontr + " del " + dataContratto, lf, 16);
                posy += 30;
            } else {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Il contratto ha validità limitata alla presente cessione", lf, 16);
                posy += 30;
            }
            // 31

            if (!lclicod.equals("")) {
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " LUOGO DI CONSEGNA", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Codice:", lf, 20);
                Env.cpclPrinter.printAndroidFont(98, posy, Typeface.MONOSPACE, true, false, false, lclicod, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Nome:", lf, 20);
                stmp = funzStringa.tagliaStringa(lclinome + " ", 55);
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(73, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Indirizzo:", lf, 20);
                stmp = funzStringa.tagliaStringa(lcliindir + " ", 50);
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Localita':", lf, 20);
                stmp = funzStringa.tagliaStringa((lcliloc + " " + lcliprov).trim(), 50);
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
            }

            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "COD.     DESCRIZIONE PRODOTTO                        ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "COD.      DESCRIZIONE PRODOTTO                                     ", lf, 18);
            if (Env.gestlotti) {
                Env.cpclPrinter.printBox(0, posy, lf, posy + 89, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "LOTTO", lf, 18);
            } else {
                Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
            }
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "CAU.UM COLLI PEZZI/QTA'PRZ.UNIT. SCONTO   IMPORTO IVA", lf, 18);
            posy += 30;
            Env.cpclPrinter.printForm();

            for (int i = 0; i < vrighe.size(); i++) {
                Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(10);
                Env.cpclPrinter.setSpeed(5);
                posy = 0;
                Record rx = vrighe.get(i);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    String riga = "";
                    riga += funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 42, funzStringa.SX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                } else {
                    String riga = "";
                    riga += funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 55, funzStringa.SX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                }
                posy += 30;
                String riga = "";
                String cau = "ORD";
                //boolean omaggio = false;
                if (rx.leggiStringa("codiva").equals(Env.codivaomaggiimp04) || rx.leggiStringa("codiva").equals(Env.codivaomaggiimp10) || rx.leggiStringa("codiva").equals(Env.codivaomaggiimp20) ||
                        rx.leggiStringa("codiva").equals(Env.codivaomaggitot04) || rx.leggiStringa("codiva").equals(Env.codivaomaggitot10) || rx.leggiStringa("codiva").equals(Env.codivaomaggitot20))
                    cau = "OMA";
                riga += funzStringa.riempiStringa(cau, 3, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa("" + rx.leggiIntero("colli"), 5, funzStringa.DX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.000", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                stmp = "";
                if (rx.leggiDouble("sc2") > 0) {
                    stmp += Formattazione.formatta(rx.leggiDouble("sc2"), "#0.0", Formattazione.NO_SEGNO);
                }
                if (rx.leggiDouble("sc1") > 0) {
                    stmp += " " + Formattazione.formatta(rx.leggiDouble("sc1"), "#0.0", Formattazione.NO_SEGNO);
                }
                riga += " " + funzStringa.riempiStringa(stmp, 9, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("netto"), "###0.00", Formattazione.SEGNO_SX), 7, funzStringa.DX, ' ');
                if (rx.leggiDouble("aliquota") > 0) {
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("aliquota"), "#0", Formattazione.NO_SEGNO), 3, funzStringa.DX, ' ');
                } else {
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("codiva"), 3, funzStringa.SX, ' ');
                }
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                posy += 30;
                if (Env.gestlotti) {
                    riga = "";
                    riga += funzStringa.riempiStringa("Lotto: " + rx.leggiStringa("lotto"), 20, funzStringa.SX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                }

                Env.cpclPrinter.setPattern(CPCLConst.LK_CPCL_VERTICAL_PATTERN);
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                Env.cpclPrinter.setPattern(CPCLConst.LK_CPCL_DEFAULT_PATTERN);
                posy += 30;
                Env.cpclPrinter.printForm();

                try {
                    Thread.currentThread().sleep(5000);
                } catch (Exception ex) {
                }
//                int ntentd = 0;
//                while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntentd < 10)
//                {
//                    Log.v("JAZZTV", "BUSY corpo ntent:" + ntentd);
//                    try
//                    {
//                        Thread.currentThread().sleep(Env.stampe_par2);
//                    } catch (Exception ex)
//                    {
//                    }
//                    ntentd++;
//                }
            }

            int nrighepiede = 8;
            if (Env.stampaordtotali)
                nrighepiede = 8;
            else
                nrighepiede = 6;
            String note1 = cm.getString(17);
            String note2 = cm.getString(18);
            String note3 = cm.getString(19);
            boolean notepres = false;
            if (!note1.equals("")) {
                nrighepiede++;
                notepres = true;
            }
            if (!note2.equals("")) {
                nrighepiede++;
                notepres = true;
            }
            if (!note3.equals("")) {
                nrighepiede++;
                notepres = true;
            }
            if (notepres)
                nrighepiede++;
            Env.cpclPrinter.setForm(0, 200, 200, nrighepiede * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;

            if (Env.stampaordtotali) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, " Tot.imponibile     Tot. IVA      Totale ordine", lf, 20);
                posy += 30;
                stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(10), "######0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(61, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(11), "######0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(220, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                if (cm.getDouble(12) >= 0) {
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(12), "#####0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
                } else {
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(12), "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
                }
                posy += 30;
            }
            stmp = funzStringa.tagliaStringa("Pagamento: " + cm.getString(8) + "-" + pagdescr, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (!note1.equals("")) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, note1, lf, 18);
                posy += 30;
            }
            if (!note2.equals("")) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, note2, lf, 18);
                posy += 30;
            }
            if (!note3.equals("")) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, note3, lf, 18);
                posy += 30;
            }
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;

            Env.cpclPrinter.printForm();

            cm.close();
            c.close();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    /*
	public static void stampaFattura(String doccod, int num, String data, double imppag, double imppagscad,
								 boolean cliblocco, ArrayList cliscop, boolean avvisopagtv, Context context)
	{
		int lf = 576;
		if (Env.tipostampante == 1)
			lf = 740;
		int gf16 = 58;
		if (Env.tipostampante == 1)
			gf16 = 76;
		int gf18 = 53;
		if (Env.tipostampante == 1)
			gf18 = 68;
		int gf20 = 47;
		if (Env.tipostampante == 1)
			gf20 = 61;
		int gf24 = 39;
		if (Env.tipostampante == 1)
			gf24 = 41;

		try
		{
			String[] pars = new String[3];
			pars[0] = doccod;
			pars[1] = data;
			pars[2] = "" + num;
			Cursor cm = Env.db.rawQuery("SELECT movtipo,movdoc,movsez,movdata,movnum,movdocora,movclicod,movdestcod,movpagcod," +
					"movnumcolli,movtotimp,movtotiva,movtotale,movacconto,movnrif FROM movimenti WHERE movdoc = ? AND movdata = ? AND movnum = ?", pars);
			cm.moveToFirst();

			ArrayList<Record> vrighe = new ArrayList();
			Cursor c = Env.db.rawQuery("SELECT rmartcod,rmlotto,rmcaumag,rmcolli,rmpezzi,rmcontenuto,rmqta,rmprz,rmartsc1,rmartsc2," +
					"rmscvend,rmcodiva,rmaliq,rmclisc1,rmclisc2,rmclisc3,rmlordo,rmnetto,rmnettoivato FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? ORDER BY rmriga", pars);
			while (c.moveToNext())
			{
				Record rx = new Record();
				rx.insStringa("artcod", c.getString(0));
				String[] parsa = new String[1];
				parsa[0] = c.getString(0);
				Cursor ca = Env.db.rawQuery("SELECT artdescr,artum FROM articoli WHERE artcod = ?", parsa);
				if (ca.moveToFirst())
				{
					rx.insStringa("artdescr", ca.getString(0));
					rx.insStringa("um", ca.getString(1));
				} else
				{
					rx.insStringa("artdescr", "");
					rx.insStringa("um", "");
				}
				ca.close();
				rx.insStringa("lotto", c.getString(1));
				rx.insStringa("causale", c.getString(2));
				rx.insIntero("colli", c.getInt(3));
				rx.insDouble("qta", c.getDouble(6));
				rx.insDouble("prezzo", c.getDouble(7));
				rx.insDouble("sc1", c.getDouble(8));
				rx.insDouble("sc2", c.getDouble(9));
				rx.insDouble("sc3", c.getDouble(10));
				rx.insStringa("codiva", c.getString(11));
				rx.insDouble("aliquota", c.getDouble(12));
				rx.insDouble("clisc1", c.getDouble(13));
				rx.insDouble("clisc2", c.getDouble(14));
				rx.insDouble("clisc3", c.getDouble(15));
				rx.insDouble("lordo", c.getDouble(16));
				rx.insDouble("netto", c.getDouble(17));
				vrighe.add(rx);
			}
			c.close();

			ArrayList<Record> viva = new ArrayList();
			Cursor civa = Env.db.rawQuery("SELECT imcodiva,imaliq,imimp,imiva FROM ivamov WHERE immovdoc = ? AND immovdata = ? AND immovnum = ? ORDER BY imcodiva", pars);
			while (civa.moveToNext())
			{
				Record rx = new Record();
				rx.insStringa("ivacod", civa.getString(0));
				String[] parci = new String[1];
				parci[0] = civa.getString(0);
				Cursor cci = Env.db.rawQuery("SELECT ivadescr FROM codiva WHERE ivacod = ?", parci);
				if (cci.moveToFirst())
				{
					rx.insStringa("ivadescr", cci.getString(0));
				} else
				{
					rx.insStringa("ivadescr", "");
				}
				cci.close();
				rx.insDouble("ivaaliq", civa.getDouble(1));
				rx.insDouble("ivaimp", civa.getDouble(2));
				rx.insDouble("ivaiva", civa.getDouble(3));
				viva.add(rx);
			}
			civa.close();

			String pclicod = "";
			String pclinome = "";
			String pcliindir = "";
			String pcliloc = "";
			String pcliprov = "";
			String pclipiva = "";
			String pclispesecauz ="";
			String pclinumcontr = "";
			String pclidatacontr = "";
			String lclicod = "";
			String lclinome = "";
			String lcliindir = "";
			String lcliloc = "";
			String lcliprov = "";

			String[] parscli = new String[1];
			parscli[0] = cm.getString(6);
			Cursor ccli = Env.db.rawQuery(
					"SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva,clispesecauz,clinumcontr,clidatacontr FROM clienti WHERE clicodice = ?", parscli);
			if (ccli.moveToFirst())
			{
				pclicod = ccli.getString(0);
				pclinome = ccli.getString(1);
				pcliindir = ccli.getString(2);
				pcliloc = ccli.getString(3);
				pcliprov = ccli.getString(4);
				pclipiva = ccli.getString(5);
				pclispesecauz = ccli.getString(6);
				pclinumcontr = ccli.getString(7);
				pclidatacontr = ccli.getString(8);
			}
			ccli.close();

			if (!cm.getString(7).equals(""))
			{
				String[] parsdest = new String[1];
				parsdest[0] = cm.getString(7);
				Cursor cdest = Env.db.rawQuery(
						"SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva FROM clienti WHERE clicodice = ?", parsdest);
				if (cdest.moveToFirst())
				{
					lclicod = cdest.getString(0);
					lclinome = cdest.getString(1);
					lcliindir = cdest.getString(2);
					lcliloc = cdest.getString(3);
					lcliprov = cdest.getString(4);
				}
				cdest.close();
			}

			String pagdescr = "";
			int pagtipo = 0;
			String[] parsp = new String[1];
			parsp[0] = cm.getString(8);
			Cursor cpag = Env.db.rawQuery(
					"SELECT pagdescr,pagtipo FROM pagamenti WHERE pagcod = ?", parsp);
			if (cpag.moveToFirst())
			{
				pagdescr = cpag.getString(0);
				pagtipo = cpag.getInt(1);
			}
			cpag.close();

			boolean stampaprezzi = true;

			// controllo incassi
			int ninc = 0;
			String[] parsinc = new String[1];
			parsinc[0] = cm.getString(6);
			Cursor cinc = Env.db.rawQuery(
					"SELECT COUNT(*) FROM incassi WHERE iscclicod = ?", parsinc);
			if (cinc.moveToFirst() && !cinc.isNull(0))
				ninc = cinc.getInt(0);
			cinc.close();

			// controllo se ci sono sospesi del giorno
			int nsosp = 0;
			Cursor csosp = Env.db.rawQuery(
					"SELECT COUNT(*) FROM movimenti LEFT JOIN pagamenti ON movimenti.movpagcod = pagamenti.pagcod WHERE movtrasf = 'N' AND pagtipo = 8 AND movacconto <> 0 and movclicod =?", parsinc);
			if (csosp.moveToFirst() && !csosp.isNull(0))
				nsosp = csosp.getInt(0);
			csosp.close();
			ArrayList<Record> vinc = new ArrayList();
			if (ninc > 0)
			{
				Cursor crinc = Env.db.rawQuery(
						"SELECT isctipo,iscsezdoc,iscnumdoc,iscdatadoc,iscdatascad,isaldo FROM incassi WHERE iscclicod = ? ORDER BY iscdatascad DESC", parsinc);
				while (crinc.moveToNext())
				{
					Record rx = new Record();
					rx.insStringa("tipo", crinc.getString(0));
					rx.insStringa("sezdoc", crinc.getString(1));
					rx.insIntero("numdoc", crinc.getInt(2));
					rx.insStringa("datadoc", crinc.getString(3));
					rx.insStringa("datascad", crinc.getString(4));
					rx.insDouble("saldo", crinc.getDouble(5));
					vinc.add(rx);
				}
				crinc.close();

				crinc = Env.db.rawQuery(
						"SELECT movtipo,movsez,movnum,movdata,movacconto FROM movimenti LEFT JOIN pagamenti ON movimenti.movpagcod = pagamenti.pagcod where movtrasf = 'N' AND pagtipo=8 AND movacconto <> 0 and movclicod =?", parsinc);
				while (crinc.moveToNext())
				{
					Record rx = new Record();
					if (crinc.getInt(0) == 0)
						rx.insStringa("tipo", "B");
					else if (crinc.getInt(0) == 1)
						rx.insStringa("tipo", "F");
					rx.insStringa("sezdoc", crinc.getString(1));
					rx.insIntero("numdoc", crinc.getInt(2));
					rx.insStringa("datadoc", crinc.getString(3));
					rx.insStringa("datascad", "");
					rx.insDouble("saldo", crinc.getDouble(4));
					vinc.add(rx);
				}
				crinc.close();
			}

			// consegna fatture art62
			ArrayList<Record> vcf = new ArrayList();
			String[] parscf = new String[2];
			parscf[0] = cm.getString(6);
			parscf[1] = (new Data()).formatta(Data.AAAA_MM_GG, "-");
			Cursor ccf = Env.db.rawQuery(
					"SELECT scdatadoc,scnumdoc FROM scoperti WHERE scclicod=? " +
							"AND sctipo = 'F' AND scdataconsfat =? ORDER BY scdatadoc DESC,scnumdoc ASC", parscf);
			while (ccf.moveToNext())
			{
				Record rx = new Record();
				rx.insIntero("numdoc", ccf.getInt(1));
				rx.insStringa("datadoc", ccf.getString(0));
				vcf.add(rx);
			}
			ccf.close();

			// elenco scoperti per blocco
			ArrayList<String> vcliscop = new ArrayList();
			if (cliblocco && cliscop.size() > 0)
			{
				for (int i = 0; i < cliscop.size(); i++)
				{
					vcliscop.add((String) cliscop.get(i));
				}
			}

			int nrigheform = 54 + vrighe.size() * 3;
			if (Env.stampasedesec)
				nrigheform += 4;
			if (!cm.getString(14).equals(""))
				nrigheform++;
			if (!lclicod.equals(""))
				nrigheform += 5;
			if (stampaprezzi)
				nrigheform++;
			if (pagtipo == 8 && imppag >= 0)
				nrigheform++;
			else if (pagtipo == 8 && imppag < 0)
				nrigheform++;
			else if (pagtipo == 8 && imppagscad != 0)
				nrigheform++;
			if (ninc > 0)
				nrigheform += 3 + vinc.size();
			else
				nrigheform += 6;
			if (vcf.size() > 0)
				nrigheform += 2 + vcf.size();
			if (cliblocco && vcliscop.size() > 0)
				nrigheform += 3 + vcliscop.size();
			File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
			if (flogo.exists())
				nrigheform += Env.nrighelogo;
			nrigheform += viva.size() + 4;

			Env.cpclPrinter.setForm(0, 200, 200, nrigheform * 30, 1);
			Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
			Env.cpclPrinter.setTone(20);

			int posy = 0;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "PROPRIETARIO DELLA MERCE:", lf, 20);
			posy += 30;

			if (flogo.exists())
			{
				try
				{
					Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
					Env.cpclPrinter.printBitmap(bm, 0, posy);
				}
				catch (Exception elogo)
				{
					elogo.printStackTrace();
				}
				posy += 30 * Env.nrighelogo;
			}
			String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", gf16);
			Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
			posy += 20;
			stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", gf16);
			Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
			posy += 20;
			stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", gf16);
			Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
			posy += 20;
			stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", gf16);
			Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
			posy += 20;
			stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", gf16);
			Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
			posy += 20;
			stmp = funzStringa.tagliaStringa(Env.intazriga6 + " ", gf16);
			Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
			posy += 20;
			stmp = funzStringa.tagliaStringa(Env.intazriga7 + " ", gf16);
			Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
			posy += 30;
			if (Env.stampasedesec)
			{
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
				posy += 30;
				String[] parsdep = new String[1];
				parsdep[0] = Env.depsede;
				Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
				cdep.moveToFirst();
				stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", gf20);
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
				posy += 30;
				stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", gf20);
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
				posy += 30;
				stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", gf20);
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
				posy += 30;
				cdep.close();
			}
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "FATTURA", lf, 24);
			posy += 30;
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
					"N." + Env.sezfattura + "/" + cm.getInt(4) + " del " +
							(new Data(cm.getString(3), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " ore " + cm.getString(5), lf, 24);
			posy += 30;
			Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
					"Assolve gli obblighi di cui all'articolo 62, comma 1,", lf, 18);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
					"del decreto legge 24 gennaio 2012, n. 1, convertito,", lf, 18);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
					"con modificazioni, dalla legge 24 marzo 2012, n. 27.", lf, 18);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
					"Durata Immediata.", lf, 18);
			posy += 30;
			Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
			posy += 30;
			if (!cm.getString(14).equals(""))
			{
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.RIF.CLIENTE: " + cm.getString(14), lf, 20);
				posy += 30;
			}
			stmp = "Targa: " + Env.targamezzo;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "VENDITORE / CARICATORE MERCE / VETTORE :", lf, 20);
			posy += 30;
			stmp = funzStringa.tagliaStringa(Env.agecod + ", " + Env.agenome, gf20);
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
			posy += 30;

			String[] parsage = new String[1];
			parsage[0] = Env.agecod;
			Cursor cage = Env.db.rawQuery("SELECT ageindirizzo,agelocalita,agepartitaiva,agecodicefiscale,agenregalbo,agetel,agefax,agemobile,ageemail FROM agenti WHERE agecod = ?", parsage);
			if (cage.moveToFirst())
			{
				stmp = funzStringa.tagliaStringa(cage.getString(0), gf20);
				if (stmp.length() == 0)
					stmp = " ";
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
				posy += 30;
				stmp = funzStringa.tagliaStringa(cage.getString(1) + " ", gf20);
				if (stmp.length() == 0)
					stmp = " ";
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
				posy += 30;
				stmp = funzStringa.tagliaStringa("p.IVA:" + cage.getString(2) + " C.Fisc.:" + cage.getString(3), gf20);
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
				posy += 30;
				stmp = funzStringa.tagliaStringa("N.Iscr.Albo:" + cage.getString(4), gf20);
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
				posy += 30;
				stmp = funzStringa.tagliaStringa("tel.:" + cage.getString(5) + " mobile:" + cage.getString(7), gf20);
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
				posy += 30;
				stmp = funzStringa.tagliaStringa("E-Mail.:" + cage.getString(8), gf20);
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
				posy += 30;
			}
			else
			{
				posy += 120;
			}
			cage.close();
			Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE / COMMITTENTE", lf, 20);
			Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Codice:", lf, 20);
			Env.cpclPrinter.printAndroidFont(98, posy, Typeface.MONOSPACE, true, false, false, pclicod, lf, 20);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Nome:", lf, 20);
			stmp = funzStringa.tagliaStringa(pclinome + " ", 55);
			if (stmp.length() == 0)
				stmp = " ";
			Env.cpclPrinter.printAndroidFont(73, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Indirizzo:", lf, 20);
			stmp = funzStringa.tagliaStringa(pcliindir + " ", 50);
			if (stmp.length() == 0)
				stmp = " ";
			Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Localita':", lf, 20);
			stmp = funzStringa.tagliaStringa((pcliloc + " " + pcliprov).trim() + " ", 50);
			if (stmp.length() == 0)
				stmp = " ";
			Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Partita IVA:", lf, 20);
			stmp = pclipiva;
			if (stmp.length() == 0)
				stmp = " ";
			Env.cpclPrinter.printAndroidFont(159, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
			posy += 30;
			if (!pclinumcontr.equals(""))
			{
				String dataContratto = "";
				if (pclidatacontr.equals("0000-00-00") || pclidatacontr.equals(""))
					dataContratto = "";
				else
					dataContratto = (new Data(pclidatacontr, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Contratto: " + pclinumcontr + " del " + dataContratto, lf, 16);
				posy += 30;
			}
			else
			{
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Il contratto ha validità limitata alla presente cessione", lf, 16);
				posy += 30;
			}
			if (!lclicod.equals(""))
			{
				Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " LUOGO DI CONSEGNA", lf, 20);
				Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
				posy += 30;
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Codice:", lf, 20);
				Env.cpclPrinter.printAndroidFont(98, posy, Typeface.MONOSPACE, true, false, false, lclicod, lf, 20);
				posy += 30;
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Nome:", lf, 20);
				stmp = funzStringa.tagliaStringa(lclinome + " ", 55);
				if (stmp.length() == 0)
					stmp = " ";
				Env.cpclPrinter.printAndroidFont(73, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
				posy += 30;
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Indirizzo:", lf, 20);
				stmp = funzStringa.tagliaStringa(lcliindir + " ", 50);
				if (stmp.length() == 0)
					stmp = " ";
				Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
				posy += 30;
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Localita':", lf, 20);
				stmp = funzStringa.tagliaStringa((lcliloc + " " + lcliprov).trim(), 50);
				if (stmp.length() == 0)
					stmp = " ";
				Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
				posy += 30;
			}
			Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
			posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
				Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " COD. LOTTO      DESCRIZIONE PRODOTTO                ", lf, 18);
			else
				Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " COD.     LOTTO  DESCRIZIONE PRODOTTO                               ", lf, 18);
			Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "CAUSALE   UM QUANTITA' PRZ.UNIT. SCONTO   IMPORTO IVA", lf, 18);
			posy += 30;
			double totqta = 0;
			double totresi = 0;
			for (int i = 0; i < vrighe.size(); i++)
			{
				Record rx = vrighe.get(i);
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
				{
					String descr = rx.leggiStringa("artdescr");
					String acod = funzStringa.tagliaStringa(rx.leggiStringa("artcod"), 4);
					Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, acod, lf, 18);
					String scad = rx.leggiStringa("lotto");
					if (scad.length() == 0)
						scad = " ";
					Env.cpclPrinter.printAndroidFont(54, posy, Typeface.MONOSPACE, true, false, false, scad, lf, 18);
					stmp = funzStringa.tagliaStringa(rx.leggiStringa("artdescr"), 39);
					if (stmp.length() == 0)
						stmp = " ";
					Env.cpclPrinter.printAndroidFont(152, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
				}
				else
				{
					String descr = rx.leggiStringa("artdescr");
					String acod = funzStringa.tagliaStringa(rx.leggiStringa("artcod"), 4);
					Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, acod, lf, 18);
					String scad = funzStringa.tagliaStringa(rx.leggiStringa("lotto"), 6);
					if (scad.length() == 0)
						scad = " ";
					Env.cpclPrinter.printAndroidFont(108, posy, Typeface.MONOSPACE, true, false, false, scad, lf, 18);
					stmp = funzStringa.tagliaStringa(rx.leggiStringa("artdescr"), 50);
					if (stmp.length() == 0)
						stmp = " ";
					Env.cpclPrinter.printAndroidFont(184, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
				}
				posy += 30;
				String cau = "";
				if (rx.leggiStringa("causale").equals("VE"))
					cau = "VENDITA";
				else if (rx.leggiStringa("causale").equals("RN"))
				{
					cau = "RESO N.V.";
					totresi += rx.leggiDouble("qta");
				}
				else if (rx.leggiStringa("causale").equals("RV"))
				{
					cau = "RESO";
					totresi += rx.leggiDouble("qta");
				}
				else if (rx.leggiStringa("causale").equals("OM") || rx.leggiStringa("causale").equals("OT"))
					cau = "OMAGGIO";
				else if (rx.leggiStringa("causale").equals("SM"))
					cau = "SC.MERCE";
				else if (rx.leggiStringa("causale").equals("RP"))
					cau = "R.PROM.";
				if (!rx.leggiStringa("causale").equals("RN") && !rx.leggiStringa("causale").equals("RV"))
					totqta += rx.leggiDouble("qta");
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, cau, lf, 18);
				Env.cpclPrinter.printAndroidFont(108, posy, Typeface.MONOSPACE, true, false, false, rx.leggiStringa("um"), lf, 18);
				stmp = funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
				Env.cpclPrinter.printAndroidFont(141, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
				stmp = funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
				Env.cpclPrinter.printAndroidFont(239, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
				stmp = "";
				if (rx.leggiDouble("sc2") > 0)
				{
					stmp += Formattazione.formatta(rx.leggiDouble("sc2"), "#0.0", Formattazione.NO_SEGNO);
				}
				if (rx.leggiDouble("sc1") > 0)
				{
					stmp += " " + Formattazione.formatta(rx.leggiDouble("sc1"), "#0.0", Formattazione.NO_SEGNO);
				}
				stmp = funzStringa.riempiStringa(stmp, 9, funzStringa.SX, ' ');
				Env.cpclPrinter.printAndroidFont(336, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
				stmp = funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("netto"), "###0.00", Formattazione.SEGNO_SX), 8, funzStringa.DX, ' ');
				Env.cpclPrinter.printAndroidFont(456, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
				if (rx.leggiDouble("aliquota") > 0)
				{
					stmp = funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("aliquota"), "#0", Formattazione.NO_SEGNO), 3, funzStringa.DX, ' ');
					Env.cpclPrinter.printAndroidFont(543, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
				}
				else
				{
					stmp = funzStringa.riempiStringa(rx.leggiStringa("codiva"), 3, funzStringa.SX, ' ');
					if (stmp.length() == 0)
						stmp = " ";
					Env.cpclPrinter.printAndroidFont(543, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
				}
				posy += 30;
				Env.cpclPrinter.setPattern(CPCLConst.LK_CPCL_VERTICAL_PATTERN);
				Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
				Env.cpclPrinter.setPattern(CPCLConst.LK_CPCL_DEFAULT_PATTERN);
				posy += 30;
			}

			Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Cod.IVA              Aliq. Imponibile      IVA ", lf, 20);
			posy += 30;
			for (int iv = 0; iv < viva.size(); iv++)
			{
				Record riva = viva.get(iv);
				stmp = funzStringa.tagliaStringa(riva.leggiStringa("ivacod") + " " + riva.leggiStringa("ivadescr"), 21) +
						(riva.leggiDouble("ivaaliq") > 0?funzStringa.riempiStringa(Formattazione.formatta(riva.leggiDouble("ivaaliq"), "##0", Formattazione.NO_SEGNO), 5, funzStringa.DX, ' '):funzStringa.spazi(5)) +
						funzStringa.riempiStringa(Formattazione.formatta(riva.leggiDouble("ivaimp"), "#######0.00", Formattazione.SEGNO_SX), 11, funzStringa.DX, ' ') +
						funzStringa.riempiStringa(Formattazione.formatta(riva.leggiDouble("ivaiva"), "#####0.00", Formattazione.SEGNO_SX), 9, funzStringa.DX, ' ');
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
				posy += 30;
			}
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, " Tot.imponibile     Tot. IVA             Totale", lf, 20);
			posy += 30;
			stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(10), "######0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
			Env.cpclPrinter.printAndroidFont(61, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
			stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(11), "######0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
			Env.cpclPrinter.printAndroidFont(220, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
			if (cm.getDouble(12) >= 0)
			{
				stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(12), "#####0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
				Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
			}
			else
			{
				stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(12), "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
				Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
			}
			posy += 30;
			Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
			posy += 30;

			if (pagtipo == 8 && imppag >= 0)
			{
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "                   Tot.incassato:", lf, 20);
				stmp = funzStringa.riempiStringa(Formattazione.formatta(imppag, "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
				Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
				posy += 30;
			}
			else if (pagtipo == 8 && imppag < 0)
			{
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "            Uscita di cassa:", lf, 20);
				stmp = funzStringa.riempiStringa(Formattazione.formatta(imppag, "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
				Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
				posy += 30;
			}
			else if (pagtipo == 8 && imppagscad != 0)
			{
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "         Tot.rate incassate:", lf, 20);
				stmp = funzStringa.riempiStringa(Formattazione.formatta(imppagscad, "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
				Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
				posy += 30;
			}

			stmp = "Tot.qta:" + Formattazione.formatta(totqta, "#####0.00", Formattazione.NO_SEGNO);
			if (totresi != 0)
				stmp += "      Tot.resi:" + Formattazione.formatta(totresi, "#####0.00", Formattazione.NO_SEGNO);
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
			posy += 30;
			stmp = funzStringa.tagliaStringa("Pagamento: " + cm.getString(8) + "-" + pagdescr, 47);
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
			posy += 30;
			Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
			posy += 30;

			if (ninc > 0)
			{
				Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " RICEVUTA DI PAGAMENTO", lf, 20);
				Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
				posy += 30;
				Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " TIPO DOC.  N.DOC.       DATA DOC.DATA SCAD.   SALDO ", lf, 18);
				Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
				posy += 30;
				for (int i = 0; i < vinc.size(); i++)
				{
					Record rx = vinc.get(i);
					stmp = "";
					if (rx.leggiStringa("tipo").equals("F"))
						stmp = "Fattura";
					else if (rx.leggiStringa("tipo").equals("B"))
						stmp = "DDT";
					else if (rx.leggiStringa("tipo").equals("C"))
						stmp = "Credito g.";
					else if (rx.leggiStringa("tipo").equals("N"))
						stmp = "Nota cred.";
					if (stmp.length() == 0)
						stmp = " ";
					Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
					if (rx.leggiIntero("numdoc") > 0)
						stmp = rx.leggiStringa("sezdoc") + "/" + rx.leggiIntero("numdoc");
					if (stmp.length() == 0)
						stmp = " ";
					Env.cpclPrinter.printAndroidFont(130, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
					if (!rx.leggiStringa("datadoc").equals("") && !rx.leggiStringa("datadoc").equals("0000-00-00"))
						stmp = (new Data(rx.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
					else
						stmp = " ";
					Env.cpclPrinter.printAndroidFont(272, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
					if (!rx.leggiStringa("datascad").equals("") && !rx.leggiStringa("datascad").equals("0000-00-00"))
						stmp = (new Data(rx.leggiStringa("datascad"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
					else
						stmp = " ";
					Env.cpclPrinter.printAndroidFont(369, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
					stmp = funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("saldo"), "#####0.00", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
					Env.cpclPrinter.printAndroidFont(467, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
					posy += 30;
				}
				Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
				posy += 30;
			}
			else
			{
				Env.cpclPrinter.printBox(0, posy, lf, posy + 30 * 7, 1);
				Env.cpclPrinter.printAndroidFont(10, posy + 2, Typeface.MONOSPACE, true, false, false, "TIMBRO", lf, 20);
				posy += 30 * 8;
			}

			if (vcf.size() > 0)
			{
				Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " FATTURE CONSEGNATE", lf, 20);
				Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
				posy += 30;
				for (int i = 0; i < vcf.size(); i++)
				{
					Record rx = vcf.get(i);
					stmp = "Fattura Num." + rx.leggiIntero("numdoc");
					if (!rx.leggiStringa("datadoc").equals("") && !rx.leggiStringa("datadoc").equals("0000-00-00"))
						stmp += " del " + (new Data(rx.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
					if (stmp.length() == 0)
						stmp = " ";
					Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
					posy += 30;
				}
				Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
				posy += 30;
			}

			if (cliblocco && vcliscop.size() > 0)
			{
				Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " SCADENZE DA SALDARE", lf, 20);
				Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
				posy += 30;
				Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "Data scad. Importo       Num.doc.    Data doc. ", lf, 18);
				Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
				posy += 30;
				for (int i = 0; i < vcliscop.size(); i++)
				{
					String txt = vcliscop.get(i);
					if (txt.length() == 0)
						txt = " ";
					Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, txt, lf, 18);
					posy += 30;
				}
				Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
				posy += 30;
			}

			//Bitmap bmcee = BitmapFactory.decodeResource(context.getResources(), R.drawable.cee);
			//Env.cpclPrinter.printBitmap(bmcee, 230, posy);
			posy += 30;
			posy += 30;
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, " Dati compilatore            Firma destinatario", lf, 20);
			posy += 30;
			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, Env.agenome, lf, 20);
			posy += 30;
			posy += 30;
			posy += 30;
			Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
			posy += 30;

			Env.cpclPrinter.printForm();

			cm.close();
			c.close();
		}
		catch (Exception eprint)
		{
			eprint.printStackTrace();
		}

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
	}
*/
    public static void stampaFatturaNew(String doccod, int num, String data, double imppag, double imppagscad,
                                        boolean cliblocco, ArrayList cliscop, boolean avvisopagtv, Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            String[] pars = new String[3];
            pars[0] = doccod;
            pars[1] = data;
            pars[2] = "" + num;
            Cursor cm = Env.db.rawQuery("SELECT movtipo,movdoc,movsez,movdata,movnum,movdocora,movclicod,movdestcod,movpagcod," +
                    "movnumcolli,movtotimp,movtotiva,movtotale,movacconto,movnrif,movcesscod FROM movimenti WHERE movdoc = ? AND movdata = ? AND movnum = ?", pars);
            cm.moveToFirst();

            ArrayList<Record> vrighe = new ArrayList();
            Cursor c = Env.db.rawQuery("SELECT rmartcod,rmlotto,rmcaumag,rmcolli,rmpezzi,rmcontenuto,rmqta,rmprz,rmartsc1,rmartsc2," +
                    "rmscvend,rmcodiva,rmaliq,rmclisc1,rmclisc2,rmclisc3,rmlordo,rmnetto,rmnettoivato FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? ORDER BY rmriga", pars);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", c.getString(0));
                String[] parsa = new String[1];
                parsa[0] = c.getString(0);
                Cursor ca = Env.db.rawQuery("SELECT artdescr,artum,artscprom,artscdatainizio,artscdatafine FROM articoli WHERE artcod = ?", parsa);
                if (ca.moveToFirst()) {
                    rx.insStringa("artdescr", ca.getString(0));
                    rx.insStringa("um", ca.getString(1));
                    if (ca.getDouble(2) != 0 && !ca.getString(3).equals("0000-00-00") && !ca.getString(4).equals("0000-00-00") &&
                            data.compareTo(ca.getString(3)) >= 0 && data.compareTo(ca.getString(4)) <= 0) {

                        rx.insDouble("scprom", ca.getDouble(2));
                        rx.insStringa("datainizioscprom", ca.getString(3));
                        rx.insStringa("datafinescprom", ca.getString(4));
                    } else {
                        rx.insDouble("scprom", 0);
                        rx.insStringa("datainizioscprom", "");
                        rx.insStringa("datafinescprom", "");
                    }
                } else {
                    rx.insStringa("artdescr", "");
                    rx.insStringa("um", "");
                    rx.insDouble("scprom", 0);
                    rx.insStringa("datainizioscprom", "");
                    rx.insStringa("datafinescprom", "");
                }
                ca.close();
                rx.insStringa("lotto", c.getString(1));
                rx.insStringa("causale", c.getString(2));
                rx.insIntero("colli", c.getInt(3));
                rx.insDouble("qta", c.getDouble(6));
                rx.insDouble("prezzo", c.getDouble(7));
                rx.insDouble("sc1", c.getDouble(8));
                rx.insDouble("sc2", c.getDouble(9));
                rx.insDouble("sc3", c.getDouble(10));
                rx.insStringa("codiva", c.getString(11));
                rx.insDouble("aliquota", c.getDouble(12));
                rx.insDouble("clisc1", c.getDouble(13));
                rx.insDouble("clisc2", c.getDouble(14));
                rx.insDouble("clisc3", c.getDouble(15));
                rx.insDouble("lordo", c.getDouble(16));
                rx.insDouble("netto", c.getDouble(17));
                vrighe.add(rx);
            }
            c.close();

            ArrayList<Record> vrigheraggr = new ArrayList();
            HashMap<Integer, ArrayList<Record>> hraggrlotti = new HashMap();
            if (Env.ddtstamparaggrlotti) {
                for (int i = 0; i < vrighe.size(); i++) {
                    Record r = vrighe.get(i);
                    boolean trovato = false;
                    for (int j = 0; j < vrigheraggr.size(); j++) {
                        Record r2 = vrigheraggr.get(j);
                        if (r.leggiStringa("artcod").equals(r2.leggiStringa("artcod")) &&
                                r.leggiStringa("causale").equals(r2.leggiStringa("causale")) &&
                                r.leggiDouble("prezzo") == r2.leggiDouble("prezzo") &&
                                r.leggiDouble("sc1") == r2.leggiDouble("sc1") &&
                                r.leggiDouble("sc2") == r2.leggiDouble("sc2") &&
                                r.leggiDouble("sc3") == r2.leggiDouble("sc3") &&
                                r.leggiStringa("codiva").equals(r2.leggiStringa("codiva")) &&
                                r.leggiDouble("clisc1") == r2.leggiDouble("clisc1") &&
                                r.leggiDouble("clisc2") == r2.leggiDouble("clisc2") &&
                                r.leggiDouble("clisc3") == r2.leggiDouble("clisc3")) {
                            double qtaold = r2.leggiDouble("qta");
                            int colliold = r2.leggiIntero("colli");
                            double lordoold = r2.leggiDouble("lordo");
                            double nettoold = r2.leggiDouble("netto");
                            r2.eliminaCampo("lordo");
                            r2.eliminaCampo("netto");
                            r2.eliminaCampo("qta");
                            r2.eliminaCampo("colli");
                            r2.insDouble("lordo", OpValute.arrotondaMat(r.leggiDouble("lordo") + lordoold, 2));
                            r2.insDouble("netto", OpValute.arrotondaMat(r.leggiDouble("netto") + nettoold, 2));
                            r2.insDouble("qta", OpValute.arrotondaMat(r.leggiDouble("qta") + qtaold, 3));
                            r2.insIntero("colli", r.leggiIntero("colli") + colliold);
                            int nriga = j;
                            ArrayList<Record> vl = hraggrlotti.get(new Integer(nriga));
                            Record rl = new Record();
                            rl.insStringa("lotto", r.leggiStringa("lotto"));
                            rl.insDouble("qtalotto", r.leggiDouble("qta"));
                            vl.add(rl);
                            trovato = true;
                            break;
                        }
                    }
                    if (!trovato) {
                        vrigheraggr.add(r);
                        Record rl = new Record();
                        rl.insStringa("lotto", r.leggiStringa("lotto"));
                        rl.insDouble("qtalotto", r.leggiDouble("qta"));
                        int nriga = vrigheraggr.size() - 1;
                        ArrayList<Record> vl = new ArrayList();
                        vl.add(rl);
                        hraggrlotti.put(new Integer(nriga), vl);
                    }
                }
            }

            ArrayList<Record> viva = new ArrayList();
            Cursor civa = Env.db.rawQuery("SELECT imcodiva,imaliq,imimp,imiva FROM ivamov WHERE immovdoc = ? AND immovdata = ? AND immovnum = ? ORDER BY imcodiva", pars);
            while (civa.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("ivacod", civa.getString(0));
                String[] parci = new String[1];
                parci[0] = civa.getString(0);
                Cursor cci = Env.db.rawQuery("SELECT ivadescr FROM codiva WHERE ivacod = ?", parci);
                if (cci.moveToFirst()) {
                    rx.insStringa("ivadescr", cci.getString(0));
                } else {
                    rx.insStringa("ivadescr", "");
                }
                cci.close();
                rx.insDouble("ivaaliq", civa.getDouble(1));
                rx.insDouble("ivaimp", civa.getDouble(2));
                rx.insDouble("ivaiva", civa.getDouble(3));
                viva.add(rx);
            }
            civa.close();

            String pclicod = "";
            String pclinome = "";
            String pcliindir = "";
            String pcliloc = "";
            String pcliprov = "";
            String pclipiva = "";
            String pclispesecauz = "";
            String pclinumcontr = "";
            String pclidatacontr = "";
            String pclistampaprz = "";
            String lclicod = "";
            String lclinome = "";
            String lcliindir = "";
            String lcliloc = "";
            String lcliprov = "";

            String[] parscli = new String[1];
            parscli[0] = cm.getString(6);
            Cursor ccli = Env.db.rawQuery(
                    "SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva,clispesecauz,clinumcontr,clidatacontr,clistampaprz FROM clienti WHERE clicodice = ?", parscli);
            if (ccli.moveToFirst()) {
                pclicod = ccli.getString(0);
                pclinome = ccli.getString(1);
                pcliindir = ccli.getString(2);
                pcliloc = ccli.getString(3);
                pcliprov = ccli.getString(4);
                pclipiva = ccli.getString(5);
                pclispesecauz = ccli.getString(6);
                pclinumcontr = ccli.getString(7);
                pclidatacontr = ccli.getString(8);
                pclistampaprz = ccli.getString(9);
            }
            ccli.close();

            if (!cm.getString(7).equals("")) {
                String[] parsdest = new String[1];
                parsdest[0] = cm.getString(7);
                Cursor cdest = Env.db.rawQuery(
                        "SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva FROM clienti WHERE clicodice = ?", parsdest);
                if (cdest.moveToFirst()) {
                    lclicod = cdest.getString(0);
                    lclinome = cdest.getString(1);
                    lcliindir = cdest.getString(2);
                    lcliloc = cdest.getString(3);
                    lcliprov = cdest.getString(4);
                }
                cdest.close();
            }

            String pagdescr = "";
            int pagtipo = 0;
            String[] parsp = new String[1];
            parsp[0] = cm.getString(8);
            Cursor cpag = Env.db.rawQuery(
                    "SELECT pagdescr,pagtipo FROM pagamenti WHERE pagcod = ?", parsp);
            if (cpag.moveToFirst()) {
                pagdescr = cpag.getString(0);
                pagtipo = cpag.getInt(1);
            }
            cpag.close();

            // controllo incassi
            int ninc = 0;
            String[] parsinc = new String[1];
            parsinc[0] = cm.getString(6);
            Cursor cinc = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM incassi WHERE iscclicod = ?", parsinc);
            if (cinc.moveToFirst() && !cinc.isNull(0))
                ninc = cinc.getInt(0);
            cinc.close();

            // controllo se ci sono sospesi del giorno
            int nsosp = 0;
            Cursor csosp = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti LEFT JOIN pagamenti ON movimenti.movpagcod = pagamenti.pagcod WHERE movtrasf = 'N' AND pagtipo = 8 AND movacconto <> 0 and movclicod =?", parsinc);
            if (csosp.moveToFirst() && !csosp.isNull(0))
                nsosp = csosp.getInt(0);
            csosp.close();
            ArrayList<Record> vinc = new ArrayList();
            if (ninc > 0) {
                Cursor crinc = Env.db.rawQuery(
                        "SELECT isctipo,iscsezdoc,iscnumdoc,iscdatadoc,iscdatascad,isaldo FROM incassi WHERE iscclicod = ? ORDER BY iscdatascad DESC", parsinc);
                while (crinc.moveToNext()) {
                    Record rx = new Record();
                    rx.insStringa("tipo", crinc.getString(0));
                    rx.insStringa("sezdoc", crinc.getString(1));
                    rx.insIntero("numdoc", crinc.getInt(2));
                    rx.insStringa("datadoc", crinc.getString(3));
                    rx.insStringa("datascad", crinc.getString(4));
                    rx.insDouble("saldo", crinc.getDouble(5));
                    vinc.add(rx);
                }
                crinc.close();

                crinc = Env.db.rawQuery(
                        "SELECT movtipo,movsez,movnum,movdata,movacconto FROM movimenti LEFT JOIN pagamenti ON movimenti.movpagcod = pagamenti.pagcod where movtrasf = 'N' AND pagtipo=8 AND movacconto <> 0 and movclicod =?", parsinc);
                while (crinc.moveToNext()) {
                    Record rx = new Record();
                    if (crinc.getInt(0) == 0)
                        rx.insStringa("tipo", "B");
                    else if (crinc.getInt(0) == 1)
                        rx.insStringa("tipo", "F");
                    rx.insStringa("sezdoc", crinc.getString(1));
                    rx.insIntero("numdoc", crinc.getInt(2));
                    rx.insStringa("datadoc", crinc.getString(3));
                    rx.insStringa("datascad", "");
                    rx.insDouble("saldo", crinc.getDouble(4));
                    vinc.add(rx);
                }
                crinc.close();
            }

            // consegna fatture art62
            ArrayList<Record> vcf = new ArrayList();
            String[] parscf = new String[2];
            parscf[0] = cm.getString(6);
            parscf[1] = (new Data()).formatta(Data.AAAA_MM_GG, "-");
            Cursor ccf = Env.db.rawQuery(
                    "SELECT scdatadoc,scnumdoc FROM scoperti WHERE scclicod=? " +
                            "AND sctipo = 'F' AND scdataconsfat =? ORDER BY scdatadoc DESC,scnumdoc ASC", parscf);
            while (ccf.moveToNext()) {
                Record rx = new Record();
                rx.insIntero("numdoc", ccf.getInt(1));
                rx.insStringa("datadoc", ccf.getString(0));
                vcf.add(rx);
            }
            ccf.close();

            // elenco scoperti per blocco
            ArrayList<String> vcliscop = new ArrayList();
            if (cliblocco && cliscop.size() > 0) {
                for (int i = 0; i < cliscop.size(); i++) {
                    vcliscop.add((String) cliscop.get(i));
                }
            }

            // elenco scoperti in coda al ddt
            ArrayList<Record> vscop = new ArrayList();
            if (Env.ddtstampascop) {
                vscop = FunzioniJBeerApp.elencoScopertiCliente(cm.getString(6));
            }

            int nrigheformtesta = 27;
            if (Env.ddtstamparaggrlotti)
                nrigheformtesta--;
            if (Env.ddtdatiage == 0)
                nrigheformtesta += 6;
            else
                nrigheformtesta += 2;
            if (Env.stampasedesec)
                nrigheformtesta += 1;

            File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
            if (flogo.exists())
                nrigheformtesta += Env.nrighelogo;

            if (!cm.getString(14).equals("")) // n.rif
                nrigheformtesta += 1;
            if (!lclicod.equals(""))
                nrigheformtesta += 5;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheformtesta * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(10);
            Env.cpclPrinter.setSpeed(5);

            int posy = 0;
            //Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "PROPRIETARIO DELLA MERCE:", lf, 20);
            //posy += 30;

            flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
            if (flogo.exists()) {
                try {
                    Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                    Env.cpclPrinter.printBitmap(bm, 0, posy);
                } catch (Exception elogo) {
                    elogo.printStackTrace();
                }
                posy += 30 * Env.nrighelogo;
            }
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga6 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 20;
            stmp = funzStringa.tagliaStringa(Env.intazriga7 + " ", gf16);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante(stmp, 16, lf), posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 16);
            posy += 30;
            if (Env.stampasedesec) {
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                String lp = funzStringa.tagliaStringa("Luogo di partenza:" + cdep.getString(2) + " " + cdep.getString(0) + " " + cdep.getString(1), gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, lp, lf, 20);
                posy += 30;
                cdep.close();
            }
            if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "FATTURA", lf, 20);
                posy += 30;
                posy += 30;
            } else {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "FATTURA", lf, 20);
                posy += 30;
                posy += 30;
            }
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                    "N." + Env.pedcod + "/" + cm.getInt(4) + " del " +
                            (new Data(cm.getString(3), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " ore " + cm.getString(5), lf, 24);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                    "Assolve gli obblighi di cui all'articolo 62, comma 1,", lf, 18);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                    "del decreto legge 24 gennaio 2012, n. 1, convertito,", lf, 18);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                    "con modificazioni, dalla legge 24 marzo 2012, n. 27.", lf, 18);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false,
                    "Durata Immediata.", lf, 18);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30; // 15
            if (!cm.getString(14).equals("")) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.RIF.CLIENTE: " + cm.getString(14), lf, 20);
                posy += 30;
            }
            stmp = "Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "VENDITORE / CARICATORE MERCE / VETTORE :", lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30; // 18

            if (Env.ddtdatiage == 0) {
                String[] parsage = new String[1];
                parsage[0] = Env.agecod;
                Cursor cage = Env.db.rawQuery("SELECT ageindirizzo,agelocalita,agepartitaiva,agecodicefiscale,agenregalbo,agetel,agefax,agemobile,ageemail FROM agenti WHERE agecod = ?", parsage);
                if (cage.moveToFirst()) {
                    stmp = funzStringa.tagliaStringa(cage.getString(0), gf20);
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cage.getString(1) + " ", gf20);
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("p.IVA:" + cage.getString(2) + " C.Fisc.:" + cage.getString(3), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("N.Iscr.Albo:" + cage.getString(4), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("tel.:" + cage.getString(5) + " mobile:" + cage.getString(7), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa("E-Mail.:" + cage.getString(8), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                } else {
                    posy += 180;
                }
                cage.close();
            } else {
                String[] parsage = new String[1];
                parsage[0] = Env.agecod;
                Cursor cage = Env.db.rawQuery("SELECT agetel,agemobile FROM agenti WHERE agecod = ?", parsage);
                if (cage.moveToFirst()) {
                    stmp = funzStringa.tagliaStringa("tel.:" + cage.getString(0) + " mobile:" + cage.getString(1), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                } else {
                    posy += 30;
                }
                cage.close();
            }
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE / COMMITTENTE", lf, 20);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Codice:", lf, 20);
            Env.cpclPrinter.printAndroidFont(98, posy, Typeface.MONOSPACE, true, false, false, pclicod, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Nome:", lf, 20);
            if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                stmp = funzStringa.tagliaStringa(pclinome + " ", 40);
            } else {
                stmp = funzStringa.tagliaStringa(pclinome + " ", 55);
            }
            if (stmp.length() == 0)
                stmp = " ";
            Env.cpclPrinter.printAndroidFont(73, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Indirizzo:", lf, 20);
            stmp = funzStringa.tagliaStringa(pcliindir + " ", 50);
            if (stmp.length() == 0)
                stmp = " ";
            Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Localita':", lf, 20);
            stmp = funzStringa.tagliaStringa((pcliloc + " " + pcliprov).trim() + " ", 50);
            if (stmp.length() == 0)
                stmp = " ";
            Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Partita IVA:", lf, 20);
            stmp = pclipiva;
            if (stmp.length() == 0)
                stmp = " ";
            Env.cpclPrinter.printAndroidFont(159, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (!pclinumcontr.equals("")) {
                String dataContratto = "";
                if (pclidatacontr.equals("0000-00-00") || pclidatacontr.equals(""))
                    dataContratto = "";
                else
                    dataContratto = (new Data(pclidatacontr, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Contratto: " + pclinumcontr + " del " + dataContratto, lf, 16);
                posy += 30;
            } else {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Il contratto ha validità limitata alla presente cessione", lf, 16);
                posy += 30;
            }
            // 31

            if (!lclicod.equals("")) {
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " LUOGO DI CONSEGNA", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Codice:", lf, 20);
                Env.cpclPrinter.printAndroidFont(98, posy, Typeface.MONOSPACE, true, false, false, lclicod, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Nome:", lf, 20);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    stmp = funzStringa.tagliaStringa(lclinome + " ", 40);
                } else {
                    stmp = funzStringa.tagliaStringa(lclinome + " ", 55);
                }
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(73, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Indirizzo:", lf, 20);
                stmp = funzStringa.tagliaStringa(lcliindir + " ", 50);
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Localita':", lf, 20);
                stmp = funzStringa.tagliaStringa((lcliloc + " " + lcliprov).trim(), 50);
                if (stmp.length() == 0)
                    stmp = " ";
                Env.cpclPrinter.printAndroidFont(134, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
            }

            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (!Env.ddtstamparaggrlotti) {
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " COD. LOTTO      DESCRIZIONE PRODOTTO                ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "COD.     LOTTO        DESCRIZIONE PRODOTTO                         ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "CAUSALE   UM QUANTITA' PRZ.UNIT. SCONTO   IMPORTO IVA", lf, 18);
                posy += 30;
            } else {
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                CAU. UM     QTA  PREZZO IVA ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                          CAU. UM       QTA    PREZZO IVA ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
            }
            Env.cpclPrinter.printForm();

            double totqta = 0;
            double totresi = 0;
            if (!Env.ddtstamparaggrlotti) {
                // stampa dettagliata
                Log.v("JAZZTV", "NRIGHE:" + vrighe.size());
                for (int i = 0; i < vrighe.size(); i++) {
                    Record rx = vrighe.get(i);
                    if (rx.leggiDouble("scprom") > 0)
                        Env.cpclPrinter.setForm(0, 200, 200, 4 * 30, 1);
                    else
                        Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(10);
                    Env.cpclPrinter.setSpeed(5);
                    posy = 0;
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        String riga = "";
                        riga += funzStringa.riempiStringa(rx.leggiStringa("artcod"), 4, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 11, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 35, funzStringa.SX, ' ');
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    } else {
                        String riga = "";
                        riga += funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 12, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 44, funzStringa.SX, ' ');
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    }
                    posy += 30;
                    String riga = "";
                    String cau = "";
                    boolean omaggio = false;
                    if (rx.leggiStringa("causale").equals("VE"))
                        cau = "VENDITA";
                    else if (rx.leggiStringa("causale").equals("RN")) {
                        cau = "RESO N.V.";
                        totresi += rx.leggiDouble("qta");
                    } else if (rx.leggiStringa("causale").equals("RV")) {
                        cau = "RESO";
                        totresi += rx.leggiDouble("qta");
                    } else if (rx.leggiStringa("causale").equals("OM") || rx.leggiStringa("causale").equals("OT")) {
                        cau = "OMAGGIO";
                        omaggio = true;
                    } else if (rx.leggiStringa("causale").equals("SM")) {
                        cau = "SC.MERCE";
                        omaggio = true;
                    } else if (rx.leggiStringa("causale").equals("VQ"))
                        cau = "VENDITA";
                    if (!rx.leggiStringa("causale").equals("RN") && !rx.leggiStringa("causale").equals("RV"))
                        totqta += rx.leggiDouble("qta");
                    riga += funzStringa.riempiStringa(cau, 9, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.000", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                    stmp = "";
                    if (rx.leggiDouble("sc2") > 0) {
                        stmp += Formattazione.formatta(rx.leggiDouble("sc2"), "#0.0", Formattazione.NO_SEGNO);
                    }
                    if (rx.leggiDouble("sc1") > 0) {
                        stmp += " " + Formattazione.formatta(rx.leggiDouble("sc1"), "#0.0", Formattazione.NO_SEGNO);
                    }
                    riga += " " + funzStringa.riempiStringa(stmp, 9, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("netto"), "###0.00", Formattazione.SEGNO_SX), 7, funzStringa.DX, ' ');
                    if (rx.leggiDouble("aliquota") > 0) {
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("aliquota"), "#0", Formattazione.NO_SEGNO), 3, funzStringa.DX, ' ');
                    } else {
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("codiva"), 3, funzStringa.SX, ' ');
                    }
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    if (rx.leggiDouble("scprom") > 0) {
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "--- In promozione dal " +
                                (new Data(rx.leggiStringa("datainizioscprom"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/") +
                                " al " + (new Data(rx.leggiStringa("datafinescprom"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/") + " ---", lf, 18);
                        posy += 30;
                    }
                    Env.cpclPrinter.setPattern(CPCLConst.LK_CPCL_VERTICAL_PATTERN);
                    Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                    Env.cpclPrinter.setPattern(CPCLConst.LK_CPCL_DEFAULT_PATTERN);
                    posy += 30;
                    Env.cpclPrinter.printForm();

                    if (i % Env.stampe_par1 == 0 && i > 0) {
                        try {
                            Thread.currentThread().sleep(5000);
                        } catch (Exception ex) {
                        }
                    }

//                    if (Env.stampe_par1 > 4)
//                    {
//                        if (i % Env.stampe_par1 == 0 && i > 0)
//                        {
//                            int ntentd = 0;
//                            while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntentd < 10)
//                            {
//                                Log.v("JAZZTV", "SPEED: BUSY corpo ntent:" + ntentd);
//                                try
//                                {
//                                    Thread.currentThread().sleep(Env.stampe_par2);
//                                } catch (Exception ex)
//                                {
//                                }
//                                ntentd++;
//                            }
//                        }
//                    }
//                    else
//                    {
//                        if (i % Env.stampe_par1 == 0)
//                        {
//                            int ntentd = 0;
//                            while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntentd < 10)
//                            {
//                                Log.v("JAZZTV", "BUSY corpo ntent:" + ntentd);
//                                try
//                                {
//                                    Thread.currentThread().sleep(Env.stampe_par2);
//                                } catch (Exception ex)
//                                {
//                                }
//                                ntentd++;
//                            }
//                        }
//                    }
                }
            } else {
                // stampa raggruppata
                Log.v("JAZZTV", "NRIGHE RAGGR:" + vrigheraggr.size());
                for (int i = 0; i < vrigheraggr.size(); i++) {
                    Record rx = vrigheraggr.get(i);
                    ArrayList<Record> vl = hraggrlotti.get(new Integer(i));
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        // riga articolo
                        Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                        Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                        Env.cpclPrinter.setTone(10);
                        Env.cpclPrinter.setSpeed(5);
                        posy = 0;
                        String riga = "";
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 4, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 18, funzStringa.SX, ' ');
                        String cau = "";
                        boolean omaggio = false;
                        if (rx.leggiStringa("causale").equals("VE"))
                            cau = "VEND";
                        else if (rx.leggiStringa("causale").equals("RN")) {
                            cau = "RESO";
                            totresi += rx.leggiDouble("qta");
                        } else if (rx.leggiStringa("causale").equals("RV")) {
                            cau = "RESO";
                            totresi += rx.leggiDouble("qta");
                        } else if (rx.leggiStringa("causale").equals("OM") || rx.leggiStringa("causale").equals("OT")) {
                            cau = "OMAG";
                            omaggio = true;
                        } else if (rx.leggiStringa("causale").equals("SM")) {
                            cau = "SC.M";
                            omaggio = true;
                        } else if (rx.leggiStringa("causale").equals("VQ"))
                            cau = "VEND";
                        if (!rx.leggiStringa("causale").equals("RN") && !rx.leggiStringa("causale").equals("RV"))
                            totqta += rx.leggiDouble("qta");
                        riga += " " + funzStringa.riempiStringa(cau, 4, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.00", Formattazione.NO_SEGNO), 7, funzStringa.DX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "##0.000", Formattazione.NO_SEGNO), 7, funzStringa.DX, ' ');
                        if (rx.leggiDouble("aliquota") > 0) {
                            riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("aliquota"), "#0", Formattazione.NO_SEGNO), 3, funzStringa.DX, ' ');
                        } else {
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("codiva"), 3, funzStringa.SX, ' ');
                        }
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                        Env.cpclPrinter.printForm();
                        // righe lotti
                        String slotti = "LOTTI:";
                        ArrayList<String> vsl = new ArrayList();
                        for (int nl = 0; nl < vl.size(); nl++) {
                            Record rl = vl.get(nl);
                            String sl = "[" + rl.leggiStringa("lotto") + "=";
                            String sql = Formattazione.formatta(rl.leggiDouble("qtalotto"), "######0.000", Formattazione.NO_SEGNO);
                            while (sql.endsWith("0"))
                                sql = sql.substring(0, sql.length() - 1);
                            if (sql.endsWith(","))
                                sql = sql.substring(0, sql.length() - 1);
                            sl += sql + "]";
                            if (slotti.length() + sl.length() > 51) {
                                vsl.add(slotti);
                                slotti = "LOTTI:" + sl;
                            } else {
                                slotti += sl;
                            }
                        }
                        vsl.add(slotti);
                        for (int nl = 0; nl < vsl.size(); nl++) {
                            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                            Env.cpclPrinter.setTone(10);
                            Env.cpclPrinter.setSpeed(5);
                            posy = 0;
                            stmp = funzStringa.riempiStringa(vsl.get(nl), 51, funzStringa.SX, ' ');
                            if (stmp.length() == 0)
                                stmp = " ";
                            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                            Env.cpclPrinter.printForm();
                        }
                    } else {
                        // riga articolo
                        Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                        Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                        Env.cpclPrinter.setTone(10);
                        Env.cpclPrinter.setSpeed(5);
                        posy = 0;
                        String riga = "";
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 24, funzStringa.SX, ' ');
                        String cau = "";
                        boolean omaggio = false;
                        if (rx.leggiStringa("causale").equals("VE"))
                            cau = "VEND";
                        else if (rx.leggiStringa("causale").equals("RN")) {
                            cau = "RESO";
                            totresi += rx.leggiDouble("qta");
                        } else if (rx.leggiStringa("causale").equals("RV")) {
                            cau = "RESO";
                            totresi += rx.leggiDouble("qta");
                        } else if (rx.leggiStringa("causale").equals("OM") || rx.leggiStringa("causale").equals("OT")) {
                            cau = "OMAG";
                            omaggio = true;
                        } else if (rx.leggiStringa("causale").equals("SM")) {
                            cau = "SC.M";
                            omaggio = true;
                        } else if (rx.leggiStringa("causale").equals("VQ"))
                            cau = "VEND";
                        if (!rx.leggiStringa("causale").equals("RN") && !rx.leggiStringa("causale").equals("RV"))
                            totqta += rx.leggiDouble("qta");
                        riga += " " + funzStringa.riempiStringa(cau, 4, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.000", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "####0.000", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                        if (rx.leggiDouble("aliquota") > 0) {
                            riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("aliquota"), "#0", Formattazione.NO_SEGNO), 3, funzStringa.DX, ' ');
                        } else {
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("codiva"), 3, funzStringa.SX, ' ');
                        }
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                        Env.cpclPrinter.printForm();
                        // righe lotti
                        String slotti = "LOTTI:";
                        ArrayList<String> vsl = new ArrayList();
                        for (int nl = 0; nl < vl.size(); nl++) {
                            Record rl = vl.get(nl);
                            String sl = "[" + rl.leggiStringa("lotto") + "=";
                            String sql = Formattazione.formatta(rl.leggiDouble("qtalotto"), "######0.000", Formattazione.NO_SEGNO);
                            while (sql.endsWith("0"))
                                sql = sql.substring(0, sql.length() - 1);
                            if (sql.endsWith(","))
                                sql = sql.substring(0, sql.length() - 1);
                            sl += sql + "]";
                            if (slotti.length() + sl.length() > 65) {
                                vsl.add(slotti);
                                slotti = "LOTTI:" + sl;
                            } else {
                                slotti += sl;
                            }
                        }
                        vsl.add(slotti);
                        for (int nl = 0; nl < vsl.size(); nl++) {
                            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                            Env.cpclPrinter.setTone(10);
                            Env.cpclPrinter.setSpeed(5);
                            posy = 0;
                            stmp = funzStringa.riempiStringa(vsl.get(nl), 65, funzStringa.SX, ' ');
                            if (stmp.length() == 0)
                                stmp = " ";
                            Log.v("JAZZTV", stmp);
                            Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                            Env.cpclPrinter.printForm();
                        }
                    }
                    if (i % Env.stampe_par1 == 0) {
                        try {
                            Thread.currentThread().sleep(5000);
                        } catch (Exception ex) {
                        }
//                        int ntentd = 0;
//                        while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntentd < 10)
//                        {
//                            Log.v("JAZZTV", "BUSY corpo ntent:" + ntentd);
//                            try
//                            {
//                                Thread.currentThread().sleep(Env.stampe_par2);
//                            } catch (Exception ex)
//                            {
//                            }
//                            ntentd++;
//                        }
                    }
                }
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.setPattern(CPCLConst.LK_CPCL_VERTICAL_PATTERN);
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                Env.cpclPrinter.setPattern(CPCLConst.LK_CPCL_DEFAULT_PATTERN);
                posy += 30;
                Env.cpclPrinter.printForm();
            }

            try {
                Thread.currentThread().sleep(7000);
            } catch (Exception ex) {
            }

            int nrighepiede = 20 + viva.size();
            if (pagtipo == 8 && imppag >= 0)
                nrighepiede++;
            else if (pagtipo == 8 && imppag < 0)
                nrighepiede++;
            else if (pagtipo == 8 && imppagscad != 0)
                nrighepiede++;
            if (ninc > 0)
                nrighepiede += 3 + vinc.size();
            else {
                if (Env.ddtstampatimbro)
                    nrighepiede += 8;
            }
            if (vcf.size() > 0)
                nrighepiede += 2 + vcf.size();
            if (cliblocco && vcliscop.size() > 0)
                nrighepiede += 3 + vcliscop.size();

            if (vscop.size() > 0) {
                nrighepiede += 7 + vscop.size();
            }

            Env.cpclPrinter.setForm(0, 200, 200, nrighepiede * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;

            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Cod.IVA              Aliq. Imponibile      IVA ", lf, 20);
            posy += 30;
            for (int iv = 0; iv < viva.size(); iv++) {
                Record riva = viva.get(iv);
                stmp = funzStringa.tagliaStringa(riva.leggiStringa("ivacod") + " " + riva.leggiStringa("ivadescr"), 21) +
                        (riva.leggiDouble("ivaaliq") > 0 ? funzStringa.riempiStringa(Formattazione.formatta(riva.leggiDouble("ivaaliq"), "##0", Formattazione.NO_SEGNO), 5, funzStringa.DX, ' ') : funzStringa.spazi(5)) +
                        funzStringa.riempiStringa(Formattazione.formatta(riva.leggiDouble("ivaimp"), "#######0.00", Formattazione.SEGNO_SX), 11, funzStringa.DX, ' ') +
                        funzStringa.riempiStringa(Formattazione.formatta(riva.leggiDouble("ivaiva"), "#####0.00", Formattazione.SEGNO_SX), 9, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
            }
            posy += 30;

            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, " Tot.imponibile     Tot. IVA             Totale", lf, 20);
            posy += 30;
            stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(10), "######0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(61, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(11), "######0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(220, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            if (cm.getDouble(12) >= 0) {
                stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(12), "#####0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
            } else {
                stmp = funzStringa.riempiStringa(Formattazione.formatta(cm.getDouble(12), "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
            }
            posy += 30;
            if (pagtipo == 8 && imppag >= 0) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "                   Tot.incassato:", lf, 20);
                stmp = funzStringa.riempiStringa(Formattazione.formatta(imppag, "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
                posy += 30;
            } else if (pagtipo == 8 && imppag < 0) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "            Uscita di cassa:", lf, 20);
                stmp = funzStringa.riempiStringa(Formattazione.formatta(imppag, "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
                posy += 30;
            } else if (pagtipo == 8 && imppagscad != 0) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "         Tot.rate incassate:", lf, 20);
                stmp = funzStringa.riempiStringa(Formattazione.formatta(imppagscad, "#####0.00", Formattazione.SEGNO_DX), 10, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(428, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 24);
                posy += 30;
            }

            stmp = "Tot.qta:" + Formattazione.formatta(totqta, "#####0.000", Formattazione.NO_SEGNO);
            if (totresi != 0)
                stmp += "      Tot.resi:" + Formattazione.formatta(totresi, "#####0.000", Formattazione.NO_SEGNO);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Pagamento: " + cm.getString(8) + "-" + pagdescr, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;

            if (ninc > 0) {
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " RICEVUTA DI PAGAMENTO", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " TIPO DOC.  N.DOC.       DATA DOC.DATA SCAD.   SALDO ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                for (int i = 0; i < vinc.size(); i++) {
                    Record rx = vinc.get(i);
                    stmp = "";
                    if (rx.leggiStringa("tipo").equals("F"))
                        stmp = "Fattura";
                    else if (rx.leggiStringa("tipo").equals("B"))
                        stmp = "DDT";
                    else if (rx.leggiStringa("tipo").equals("C"))
                        stmp = "Credito g.";
                    else if (rx.leggiStringa("tipo").equals("N"))
                        stmp = "Nota cred.";
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    if (rx.leggiIntero("numdoc") > 0)
                        stmp = rx.leggiStringa("sezdoc") + "/" + rx.leggiIntero("numdoc");
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(130, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    if (!rx.leggiStringa("datadoc").equals("") && !rx.leggiStringa("datadoc").equals("0000-00-00"))
                        stmp = (new Data(rx.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(272, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    if (!rx.leggiStringa("datascad").equals("") && !rx.leggiStringa("datascad").equals("0000-00-00"))
                        stmp = (new Data(rx.leggiStringa("datascad"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(369, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("saldo"), "#####0.00", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(467, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    posy += 30;
                }
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
            } else {
                if (Env.ddtstampatimbro) {
                    Env.cpclPrinter.printBox(0, posy, lf, posy + 30 * 7, 1);
                    Env.cpclPrinter.printAndroidFont(10, posy + 2, Typeface.MONOSPACE, true, false, false, "TIMBRO", lf, 20);
                    posy += 30 * 8;
                }
            }

            if (vcf.size() > 0) {
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " FATTURE CONSEGNATE", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                for (int i = 0; i < vcf.size(); i++) {
                    Record rx = vcf.get(i);
                    stmp = "Fattura Num." + rx.leggiIntero("numdoc");
                    if (!rx.leggiStringa("datadoc").equals("") && !rx.leggiStringa("datadoc").equals("0000-00-00"))
                        stmp += " del " + (new Data(rx.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    posy += 30;
                }
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
            }

            if (cliblocco && vcliscop.size() > 0) {
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " SCADENZE DA SALDARE", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "Data scad. Importo       Num.doc.    Data doc. ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                for (int i = 0; i < vcliscop.size(); i++) {
                    String txt = vcliscop.get(i);
                    if (txt.length() == 0)
                        txt = " ";
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, txt, lf, 18);
                    posy += 30;
                }
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
            }

            if (vscop.size() > 0) {
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " Riconosco il debito residuo come da elenco", lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " fatture che segue:", lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " ELENCO SCOPERTI DEL CLIENTE", lf, 20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " TIPO DOC.  N.DOC.       DATA DOC.DATA SCAD. RESIDUO ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                double totscop = 0;
                for (int i = 0; i < vscop.size(); i++) {
                    Record rx = vscop.get(i);
                    stmp = "";
                    if (rx.leggiStringa("tipo").equals("F"))
                        stmp = "Fattura";
                    else if (rx.leggiStringa("tipo").equals("B"))
                        stmp = "DDT";
                    else if (rx.leggiStringa("tipo").equals("C"))
                        stmp = "Credito g.";
                    else if (rx.leggiStringa("tipo").equals("N"))
                        stmp = "Nota cred.";
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    if (rx.leggiIntero("numdoc") > 0)
                        stmp = rx.leggiStringa("sezdoc") + "/" + rx.leggiIntero("numdoc");
                    if (stmp.length() == 0)
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(130, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    if (!rx.leggiStringa("datadoc").equals("") && !rx.leggiStringa("datadoc").equals("0000-00-00"))
                        stmp = (new Data(rx.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(272, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    if (!rx.leggiStringa("datascad").equals("") && !rx.leggiStringa("datascad").equals("0000-00-00"))
                        stmp = (new Data(rx.leggiStringa("datascad"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        stmp = " ";
                    double segno = 1;
                    if (rx.leggiStringa("tipoop").equals("V"))
                        segno = 1;
                    else
                        segno = -1;
                    Env.cpclPrinter.printAndroidFont(369, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(segno * rx.leggiDouble("residuoscad"), "#####0.00", Formattazione.SEGNO_SX), 9, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(467, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    posy += 30;
                    totscop += segno * rx.leggiDouble("residuoscad");
                }
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
                totscop = OpValute.arrotondaMat(totscop, 2);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE SCOPERTI DEL CLIENTE: " + Formattazione.formatta(totscop, " ######0.00", Formattazione.SEGNO_DX), lf, 24);
                posy += 30;
                posy += 30;
            }

            posy += 30;
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, " Dati compilatore            Firma destinatario", lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, Env.agenome, lf, 20);
            posy += 30;
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;

            Env.cpclPrinter.printForm();

            cm.close();
            c.close();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    public static void stampaGiacenze(Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        boolean stampaveloce = (FunzioniJBeerApp.leggiProprieta("stampeveloci").equals("S"));

        double totimp = 0;
        double totkg = 0;
        Cursor ctot = Env.db.rawQuery("SELECT artcod,artdescr,artgiacenza,artum FROM articoli WHERE artgiacenza <> 0 ORDER BY artcod", null);
        while (ctot.moveToNext()) {
            String[] pars = new String[1];
            pars[0] = ctot.getString(0);
            Cursor cltot = Env.db.rawQuery("SELECT lotcod,lotgiacenza FROM lotti WHERE lotgiacenza <> 0 AND lotartcod = ? ORDER BY lotcod DESC", pars);
            while (cltot.moveToNext()) {
                Record rx = new Record();
                double pb = leggiPrezzoBaseArticolo(ctot.getString(0), cltot.getString(0), true);
                double imp = OpValute.arrotondaMat(pb * cltot.getDouble(1), 2);
                totimp += imp;
                totkg += FunzioniJBeerApp.calcolaKgArticolo(ctot.getString(0), cltot.getDouble(1));
            }
            cltot.close();
        }
        ctot.close();

        {
            try {
                ArrayList<Record> vrighe = new ArrayList();
                HashMap<String, Double> htot = new HashMap();
                Cursor c = Env.db.rawQuery("SELECT artcod,artdescr,artgiacenza,artum FROM articoli WHERE artgiacenza <> 0 ORDER BY artcod", null);
                while (c.moveToNext()) {
                    String[] pars = new String[1];
                    pars[0] = c.getString(0);
                    Cursor cl = Env.db.rawQuery("SELECT lotcod,lotgiacenza FROM lotti WHERE lotgiacenza <> 0 AND lotartcod = ? ORDER BY lotcod DESC", pars);
                    while (cl.moveToNext()) {
                        Record rx = new Record();
                        rx.insStringa("artcod", c.getString(0));
                        rx.insStringa("artdescr", c.getString(1));
                        rx.insStringa("um", c.getString(3));
                        rx.insStringa("lotto", cl.getString(0));
                        rx.insDouble("qta", cl.getDouble(1));
                        vrighe.add(rx);
                    }
                    cl.close();
                    htot.put(c.getString(0), c.getDouble(2));
                }
                c.close();

                int posy = 0;
                int nrigheint = 6;
                if (Env.stampasedesec)
                    nrigheint += 4;
                Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.setSpeed(5);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("LISTA GIACENZE ARTICOLI", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "LISTA GIACENZE ARTICOLI", lf, 24);
                posy += 30;
                SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
                String ora = dh.format(new Date());
                String stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
                if (Env.stampasedesec) {
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                    posy += 30;
                    String[] parsdep = new String[1];
                    parsdep[0] = Env.depsede;
                    Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                    cdep.moveToFirst();
                    stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    cdep.close();
                }
                stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                         UM LOTTO       QTA ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                       UM LOTTO       QTA ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printForm();

                try {
                    Thread.currentThread().sleep(2000);
                } catch (Exception ex) {
                }
                //System.out.println("PRINTERCHECK:" + Env.cpclPrinter.printerCheck(5000));
//                try
//                {
//                    Thread.currentThread().sleep(2000);
//                } catch (Exception ex)
//                {
//                }

                for (int i = 0; i < vrighe.size(); i++) {
                    Record rx = vrighe.get(i);
                    boolean stampaart = true;
                    if (Env.tipogestlotti == 0) {
                        if (i > 0) {
                            Record rprec = vrighe.get(i - 1);
                            if (rprec.leggiStringa("artcod").equals(rx.leggiStringa("artcod"))) {
                                stampaart = false;
                            }
                        }
                    }
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                        Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                        Env.cpclPrinter.setTone(10);
                        Env.cpclPrinter.setSpeed(5);
                        posy = 0;
                        String riga = "";
                        if (stampaart) {
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 23, funzStringa.SX, ' ');
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                        } else {
                            riga += funzStringa.spazi(36);
                        }
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                        posy += 30;
                        Env.cpclPrinter.printForm();
                        if (Env.tipogestlotti == 0) {
                            if ((i < vrighe.size() - 1 && !vrighe.get(i + 1).leggiStringa("artcod").equals(rx.leggiStringa("artcod"))) || (i == vrighe.size() - 1)) {
                                Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
                                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                                Env.cpclPrinter.setTone(20);
                                posy = 0;
                                riga = funzStringa.riempiStringa("Totale " + rx.leggiStringa("artcod"), 43, funzStringa.DX, ' ');
                                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(htot.get(rx.leggiStringa("artcod")), "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 18);
                                posy += 30;
                                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, funzStringa.stringa("-", 53), lf, 18);
                                posy += 30;
                                Env.cpclPrinter.printForm();
                            }
                        }
                    } else {
                        Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                        Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                        Env.cpclPrinter.setTone(10);
                        Env.cpclPrinter.setSpeed(5);
                        posy = 0;
                        String riga = "";
                        if (stampaart) {
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 37, funzStringa.SX, ' ');
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                        } else {
                            riga += funzStringa.spazi(50);
                        }
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                        posy += 30;
                        Env.cpclPrinter.printForm();
                        if (Env.tipogestlotti == 0) {
                            if ((i < vrighe.size() - 1 && !vrighe.get(i + 1).leggiStringa("artcod").equals(rx.leggiStringa("artcod"))) || (i == vrighe.size() - 1)) {
                                Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
                                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                                Env.cpclPrinter.setTone(10);
                                posy = 0;
                                riga = funzStringa.riempiStringa("Totale " + rx.leggiStringa("artcod"), 57, funzStringa.DX, ' ');
                                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(htot.get(rx.leggiStringa("artcod")), "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 18);
                                posy += 30;
                                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, funzStringa.stringa("-", 67), lf, 18);
                                posy += 30;
                                Env.cpclPrinter.printForm();
                            }
                        }
                    }
                    int interv = Env.stampe_par1;

                    //System.out.println("interv:" + interv);
                    if (i % interv == 0 && i > 0) {
                        int ntent = 0;
                        try {
                            Thread.currentThread().sleep(5000);
                        } catch (Exception ex) {
                        }
//                        while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                        {
//                            Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                            try
//                            {
//                                Thread.currentThread().sleep(Env.stampe_par2);
//                            } catch (Exception ex)
//                            {
//                            }
//                            ntent++;
//                        }
                    }


//                    if (i % interv == 0 && i > 0)
//                    {
//                        String result = "";
//                        try
//                        {
//                            if (!(Env.cpclPrinter.printerCheck(5000) < 0))
//                            {
//                                int sts = Env.cpclPrinter.status();
//                                if (sts == CPCLConst.LK_STS_CPCL_NORMAL)
//                                    result = "Normal";
//                                if ((sts & CPCLConst.LK_STS_CPCL_BUSY) > 0)
//                                    result = result + " Busy";
//                                if ((sts & CPCLConst.LK_STS_CPCL_PAPER_EMPTY) > 0)
//                                    result = result + " Paper empty";
//                                if ((sts & CPCLConst.LK_STS_CPCL_COVER_OPEN) > 0)
//                                    result = result + " Cover open";
//                                if ((sts & CPCLConst.LK_STS_CPCL_BATTERY_LOW) > 0)
//                                    result = result + " Battery low";
//                            } else
//                            {
//                                result = "No response";
//                            }
//                        }
//                        catch (Exception e)
//                        {
//                            e.printStackTrace();
//                        }


//                        int ntent = 0;
//                        while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                        {
//                            Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                            try
//                            {
//                                Thread.currentThread().sleep(Env.stampe_par2);
//                            } catch (Exception ex)
//                            {
//                            }
//                            ntent++;
//                        }
                    //}
                }
                Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(10);
                Env.cpclPrinter.setSpeed(5);
                posy = 0;
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;

                Env.cpclPrinter.printForm();
            } catch (Exception eprint) {
                eprint.printStackTrace();
                try {
                    Env.cpclPrinter.printForm();
                } catch (Exception eprint2) {
                }
            }
        }
    }

    public static void stampaDistintaCarico(Context context, ArrayList<Record> vart) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        boolean stampaveloce = (FunzioniJBeerApp.leggiProprieta("stampeveloci").equals("S"));

        {
            try {
                int posy = 0;
                int nrigheint = 6;
                if (Env.stampasedesec)
                    nrigheint += 4;
                Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.setSpeed(5);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("DISTINTA DI CARICO ARTICOLI", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "DISTINTA DI CARICO ARTICOLI", lf, 24);
                posy += 30;
                SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
                String ora = dh.format(new Date());
                String stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
                if (Env.stampasedesec) {
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                    posy += 30;
                    String[] parsdep = new String[1];
                    parsdep[0] = Env.depsede;
                    Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                    cdep.moveToFirst();
                    stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    cdep.close();
                }
                stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                          UM COLLI    PEZZI ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                         UM COLLI    PEZZI ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printForm();

                try {
                    Thread.currentThread().sleep(2000);
                } catch (Exception ex) {
                }
                //System.out.println("PRINTERCHECK:" + Env.cpclPrinter.printerCheck(5000));
//                try
//                {
//                    Thread.currentThread().sleep(2000);
//                } catch (Exception ex)
//                {
//                }

                for (int i = 0; i < vart.size(); i++) {
                    Record rx = vart.get(i);
                    if (rx.leggiDouble("qtadistcarico") > 0 || rx.leggiIntero("collidistcarico") > 0) {
                        if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                            Env.cpclPrinter.setTone(10);
                            Env.cpclPrinter.setSpeed(5);
                            posy = 0;
                            String riga = "";
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 24, funzStringa.SX, ' ');
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                            if (rx.leggiIntero("collidistcarico") > 0)
                                riga += " " + funzStringa.riempiStringa("" + rx.leggiIntero("collidistcarico"), 5, funzStringa.DX, ' ');
                            else
                                riga += " " + funzStringa.spazi(5);
                            if (rx.leggiDouble("qtadistcarico") > 0)
                                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qtadistcarico"), "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                            else
                                riga += " " + funzStringa.spazi(8);
                            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                            posy += 30;
                            Env.cpclPrinter.printForm();
                        } else {
                            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                            Env.cpclPrinter.setTone(10);
                            Env.cpclPrinter.setSpeed(5);
                            posy = 0;
                            String riga = "";
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 39, funzStringa.SX, ' ');
                            riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                            if (rx.leggiIntero("collidistcarico") > 0)
                                riga += " " + funzStringa.riempiStringa("" + rx.leggiIntero("collidistcarico"), 5, funzStringa.DX, ' ');
                            else
                                riga += " " + funzStringa.spazi(5);
                            if (rx.leggiDouble("qtadistcarico") > 0)
                                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qtadistcarico"), "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                            else
                                riga += " " + funzStringa.spazi(8);
                            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                            posy += 30;
                            Env.cpclPrinter.printForm();
                        }
                        int interv = Env.stampe_par1;

                        //System.out.println("interv:" + interv);
                        if (i % interv == 0 && i > 0) {
                            int ntent = 0;
                            try {
                                Thread.currentThread().sleep(5000);
                            } catch (Exception ex) {
                            }
                        }
                    }
                }
                Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(10);
                Env.cpclPrinter.setSpeed(5);
                posy = 0;
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;

                Env.cpclPrinter.printForm();
            } catch (Exception eprint) {
                eprint.printStackTrace();
                try {
                    Env.cpclPrinter.printForm();
                } catch (Exception eprint2) {
                }
            }
        }
    }

    public static void stampaArticoliEsauriti(Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;


        {
            try {
                ArrayList<Record> vart = new ArrayList();
                Data dtoggi = new Data();
                dtoggi.decrementaGiorni(7);
                Cursor cursor = Env.db.rawQuery("SELECT artcod,artdescr,artum,MAX(rmmovdata) AS maxdata FROM articoli LEFT JOIN righemov ON artcod = rmartcod WHERE artgiacenza = 0 AND rmmovdata >= '" + dtoggi.formatta(Data.AAAA_MM_GG, "-") + "' ORDER BY artcod", null);
                while (cursor.moveToNext()) {
                    if (!cursor.isNull(3) && !cursor.getString(3).equals("")) {
                        Record r = new Record();
                        r.insStringa("artcod", cursor.getString(0));
                        r.insStringa("artdescr", cursor.getString(1));
                        r.insStringa("artum", cursor.getString(2));
                        r.insStringa("data", cursor.getString(3));
                        vart.add(r);
                    }
                }
                cursor.close();

                int posy = 0;
                int nrigheint = 6;
                if (Env.stampasedesec)
                    nrigheint += 4;
                Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.setSpeed(5);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("ELENCO ARTICOLI ESAURITI", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "ELENCO ARTICOLI ESAURITI", lf, 24);
                posy += 30;
                SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
                String ora = dh.format(new Date());
                String stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;
                if (Env.stampasedesec) {
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                    posy += 30;
                    String[] parsdep = new String[1];
                    parsdep[0] = Env.depsede;
                    Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                    cdep.moveToFirst();
                    stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    cdep.close();
                }
                stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                         UM ULTIMA DATA MOV ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                       UM ULTIMA DATA MOV ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
                Env.cpclPrinter.printForm();

                for (int i = 0; i < vart.size(); i++) {
                    Record rx = vart.get(i);
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                        Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                        Env.cpclPrinter.setTone(20);
                        Env.cpclPrinter.setSpeed(5);
                        posy = 0;
                        String riga = "";
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 23, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                        riga += " " + (new Data(rx.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                        posy += 30;
                        Env.cpclPrinter.printForm();
                    } else {
                        Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                        Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                        Env.cpclPrinter.setTone(20);
                        Env.cpclPrinter.setSpeed(5);
                        posy = 0;
                        String riga = "";
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 37, funzStringa.SX, ' ');
                        riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                        riga += " " + (new Data(rx.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                        Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                        posy += 30;
                        Env.cpclPrinter.printForm();
                    }
                    if (i % Env.stampe_par1 == 0) {
                        try {
                            Thread.currentThread().sleep(5000);
                        } catch (Exception ex) {
                        }
//                        int ntent = 0;
//                        while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                        {
//                            Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                            try
//                            {
//                                Thread.currentThread().sleep(Env.stampe_par2);
//                            } catch (Exception ex)
//                            {
//                            }
//                            ntent++;
//                        }
                    }
                }
                Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
                posy += 30;

                Env.cpclPrinter.printForm();
            } catch (Exception eprint) {
                eprint.printStackTrace();
                try {
                    Env.cpclPrinter.printForm();
                } catch (Exception eprint2) {
                }
            }
        }
    }

    public static void stampaInventario(Context context, int num) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        double totimp = 0;
        double totkg = 0;
        ArrayList<Record> vinv = new ArrayList();
        Cursor cinv = Env.db.rawQuery("SELECT invartcod,invqta,invval FROM inventario WHERE invnum = " + num + " ORDER BY invartcod", null);
        while (cinv.moveToNext()) {
            Record rx = new Record();
            rx.insStringa("artcod", cinv.getString(0));
            String[] parsa = new String[1];
            parsa[0] = cinv.getString(0);
            Cursor ca = Env.db.rawQuery("SELECT artdescr,artum FROM articoli WHERE artcod = ?", parsa);
            if (ca.moveToFirst()) {
                rx.insStringa("artdescr", ca.getString(0));
                rx.insStringa("um", ca.getString(1));
            } else {
                rx.insStringa("artdescr", "");
                rx.insStringa("um", "");
            }
            ca.close();
            rx.insDouble("qta", cinv.getDouble(1));
            rx.insDouble("val", cinv.getDouble(2));
            double kg = FunzioniJBeerApp.calcolaKgArticolo(cinv.getString(0), cinv.getDouble(1));
            rx.insDouble("kg", kg);
            vinv.add(rx);
            totimp += cinv.getDouble(2);
            totkg += kg;
        }
        cinv.close();

        try {
            int posy = 0;
            int nrigheint = 7;
//            if (Env.stampasedesec)
//                nrigheint += 4;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("STAMPA INVENTARIO (NON VALIDA FISCALMENTE)", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "STAMPA INVENTARIO (NON VALIDA FISCALMENTE)", lf, 24);
            posy += 30;
            String stmp = "Numero inventario:" + num;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
//            if (Env.stampasedesec)
//            {
//                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
//                posy += 30;
//                String[] parsdep = new String[1];
//                parsdep[0] = Env.depsede;
//                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
//                cdep.moveToFirst();
//                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
//                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
//                posy += 30;
//                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
//                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
//                posy += 30;
//                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
//                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
//                posy += 30;
//                cdep.close();
//            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                     UM      QTA     VALORE ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                UM        QTA       VALORE ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            for (int i = 0; i < vinv.size(); i++) {
                Record rx = vinv.get(i);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    String riga = "";
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 19, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("val"), "######0.00", Formattazione.SEGNO_SX_NEG), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    Env.cpclPrinter.printForm();
                } else {
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    String riga = "";
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 30, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.SEGNO_SX_NEG), 10, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("val"), "######0.00", Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    Env.cpclPrinter.printForm();
                }
                //if (i % Env.stampe_par1 == 0)
                if (i % 20 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 6 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Totale KG        : " +
                    funzStringa.riempiStringa(Formattazione.formValuta(totkg, 12, 3, 0), 15, funzStringa.DX, ' '), lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Totale imponibile: " +
                    funzStringa.riempiStringa(Formattazione.formValuta(totimp, 12, 2, 0), 15, funzStringa.DX, ' '), lf, 24);
            posy += 30;
            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }
    }

    public static void stampaInventario2(Context context, int num, String data) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        double totimp = 0;
        double totkg = 0;
        ArrayList<Record> vinv = new ArrayList();
        Cursor cinv = Env.db.rawQuery("SELECT invartcod,invqta,invval FROM inventario WHERE invnum = " + num + " AND invdata = '" + data + "' ORDER BY invartcod", null);
        while (cinv.moveToNext()) {
            Record rx = new Record();
            rx.insStringa("artcod", cinv.getString(0));
            String[] parsa = new String[1];
            parsa[0] = cinv.getString(0);
            Cursor ca = Env.db.rawQuery("SELECT artdescr,artum FROM articoli WHERE artcod = ?", parsa);
            if (ca.moveToFirst()) {
                rx.insStringa("artdescr", ca.getString(0));
                rx.insStringa("um", ca.getString(1));
            } else {
                rx.insStringa("artdescr", "");
                rx.insStringa("um", "");
            }
            ca.close();
            rx.insDouble("qta", cinv.getDouble(1));
            rx.insDouble("val", cinv.getDouble(2));
            double kg = FunzioniJBeerApp.calcolaKgArticolo(cinv.getString(0), cinv.getDouble(1));
            rx.insDouble("kg", kg);
            vinv.add(rx);
            totimp += cinv.getDouble(2);
            totkg += kg;
        }
        cinv.close();

        try {
            int posy = 0;
            int nrigheint = 7;
//            if (Env.stampasedesec)
//                nrigheint += 4;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("STAMPA INVENTARIO (NON VALIDA FISCALMENTE)", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "STAMPA INVENTARIO (NON VALIDA FISCALMENTE)", lf, 24);
            posy += 30;
            String stmp = "Numero inventario:" + num;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
//            if (Env.stampasedesec)
//            {
//                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
//                posy += 30;
//                String[] parsdep = new String[1];
//                parsdep[0] = Env.depsede;
//                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
//                cdep.moveToFirst();
//                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
//                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
//                posy += 30;
//                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
//                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
//                posy += 30;
//                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
//                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
//                posy += 30;
//                cdep.close();
//            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                     UM      QTA     VALORE ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                UM        QTA       VALORE ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            for (int i = 0; i < vinv.size(); i++) {
                Record rx = vinv.get(i);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    String riga = "";
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 19, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("val"), "######0.00", Formattazione.SEGNO_SX_NEG), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    Env.cpclPrinter.printForm();
                } else {
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    String riga = "";
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 30, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.SEGNO_SX_NEG), 10, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("val"), "######0.00", Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    Env.cpclPrinter.printForm();
                }
                //if (i % Env.stampe_par1 == 0)
                if (i % 20 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 6 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Totale KG        : " +
                    funzStringa.riempiStringa(Formattazione.formValuta(totkg, 12, 3, 0), 15, funzStringa.DX, ' '), lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Totale imponibile: " +
                    funzStringa.riempiStringa(Formattazione.formValuta(totimp, 12, 2, 0), 15, funzStringa.DX, ' '), lf, 24);
            posy += 30;
            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }
    }

    public static void stampaRiepilogoArticoli(Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        boolean stampacompatta = (FunzioniJBeerApp.leggiProprieta("stampariepartcompatta").equals("S"));

        try {
            TreeMap<String, Record> hart = new TreeMap();
            Cursor cmov = Env.db.rawQuery("SELECT movtipo,movnum FROM movimenti WHERE movsospeso = 'N' AND movtrasf <> 'S'", null);
            while (cmov.moveToNext()) {
                Cursor cc = Env.db.rawQuery("SELECT rmartcod,rmqta,rmcaumag,artdescr,artum FROM righemov INNER JOIN articoli ON righemov.rmartcod = articoli.artcod WHERE rmmovtipo = " + cmov.getInt(0) + " AND rmmovnum = " + cmov.getInt(1) + " AND rmartcod <> ''", null);
                while (cc.moveToNext()) {
                    String artcod = cc.getString(0);
                    double qta = cc.getDouble(1);
                    String cm = cc.getString(2);
                    if (cm.equals("VE") || cm.equals("RV") || cm.equals("RN") || cm.equals("OM") || cm.equals("OT") ||
                            cm.equals("SM") || cm.equals("TU") || cm.equals("TE") || cm.equals("SS") || cm.equals("PE") || cm.equals("RC") ||
                            cm.equals("DC") || cm.equals("DP") || cm.equals("DN") || cm.equals("VQ")) {
                        if (hart.containsKey(artcod)) {
                            Record rval = hart.get(artcod);
                            hart.remove(artcod);
                            if (cm.equals("VE") || cm.equals("VQ")) {
                                double qprec = 0;
                                if (rval.esisteCampo("VE"))
                                    qprec = rval.leggiDouble("VE");
                                rval.eliminaCampo("VE");
                                rval.insDouble("VE", qprec + qta);
                            } else if (cm.equals("DP")) {
                                double qprec = 0;
                                if (rval.esisteCampo("RE"))
                                    qprec = rval.leggiDouble("RE");
                                rval.eliminaCampo("RE");
                                rval.insDouble("RE", qprec + qta);
                            } else if (cm.equals("DN")) {
                                double qprec = 0;
                                if (rval.esisteCampo("RE"))
                                    qprec = rval.leggiDouble("RE");
                                rval.eliminaCampo("RE");
                                rval.insDouble("RE", qprec - qta);
                            } else if (cm.equals("OM") || cm.equals("OT") || cm.equals("SM")) {
                                double qprec = 0;
                                if (rval.esisteCampo("OM"))
                                    qprec = rval.leggiDouble("OM");
                                rval.eliminaCampo("OM");
                                rval.insDouble("OM", qprec + qta);
                            } else if (cm.equals("RV"))// || cm.equals("RN"))
                            {
                                double qprec = 0;
                                if (rval.esisteCampo("RV"))
                                    qprec = rval.leggiDouble("RV");
                                rval.eliminaCampo("RV");
                                rval.insDouble("RV", qprec + qta);
                            } else if (cm.equals("TU")) {
                                double qprec = 0;
                                if (rval.esisteCampo("TU"))
                                    qprec = rval.leggiDouble("TU");
                                rval.eliminaCampo("TU");
                                rval.insDouble("TU", qprec + qta);
                            } else if (cm.equals("TE")) {
                                double qprec = 0;
                                if (rval.esisteCampo("TE"))
                                    qprec = rval.leggiDouble("TE");
                                rval.eliminaCampo("TE");
                                rval.insDouble("TE", qprec + qta);
                            } else if (cm.equals("DC")) {
                                double qprec = 0;
                                if (rval.esisteCampo("DC"))
                                    qprec = rval.leggiDouble("DC");
                                rval.eliminaCampo("DC");
                                rval.insDouble("DC", qprec + qta);
                            } else if (cm.equals("SS") || cm.equals("PE") || cm.equals("RC")) {
                                double qprec = 0;
                                if (rval.esisteCampo("SS"))
                                    qprec = rval.leggiDouble("SS");
                                rval.eliminaCampo("SS");
                                rval.insDouble("SS", qprec + qta);
                            }
                            hart.put(artcod, rval);
                        } else {
                            Record rval = new Record();
                            rval.insStringa("artcod", artcod);
                            rval.insStringa("artdescr", cc.getString(3));
                            rval.insStringa("artum", cc.getString(4));
                            if (cm.equals("VE") || cm.equals("VQ")) {
                                rval.insDouble("VE", qta);
                                rval.insDouble("OM", 0);
                                rval.insDouble("RE", 0);
                                rval.insDouble("RV", 0);
                                rval.insDouble("TU", 0);
                                rval.insDouble("TE", 0);
                                rval.insDouble("DC", 0);
                                rval.insDouble("SS", 0);
                            } else if (cm.equals("DP")) {
                                rval.insDouble("VE", 0);
                                rval.insDouble("OM", 0);
                                rval.insDouble("RE", qta);
                                rval.insDouble("RV", 0);
                                rval.insDouble("TU", 0);
                                rval.insDouble("TE", 0);
                                rval.insDouble("DC", 0);
                                rval.insDouble("SS", 0);
                            } else if (cm.equals("DN")) {
                                rval.insDouble("VE", 0);
                                rval.insDouble("OM", 0);
                                rval.insDouble("RE", -qta);
                                rval.insDouble("RV", 0);
                                rval.insDouble("TU", 0);
                                rval.insDouble("TE", 0);
                                rval.insDouble("DC", 0);
                                rval.insDouble("SS", 0);
                            } else if (cm.equals("OM") || cm.equals("OT") || cm.equals("SM")) {
                                rval.insDouble("VE", 0);
                                rval.insDouble("OM", qta);
                                rval.insDouble("RE", 0);
                                rval.insDouble("RV", 0);
                                rval.insDouble("TU", 0);
                                rval.insDouble("TE", 0);
                                rval.insDouble("DC", 0);
                                rval.insDouble("SS", 0);
                            } else if (cm.equals("RV") || cm.equals("RN")) {
                                rval.insDouble("VE", 0);
                                rval.insDouble("OM", 0);
                                rval.insDouble("RE", 0);
                                rval.insDouble("RV", qta);
                                rval.insDouble("TU", 0);
                                rval.insDouble("TE", 0);
                                rval.insDouble("DC", 0);
                                rval.insDouble("SS", 0);
                            } else if (cm.equals("TU")) {
                                rval.insDouble("VE", 0);
                                rval.insDouble("OM", 0);
                                rval.insDouble("RE", 0);
                                rval.insDouble("RV", 0);
                                rval.insDouble("TU", qta);
                                rval.insDouble("TE", 0);
                                rval.insDouble("DC", 0);
                                rval.insDouble("SS", 0);
                            } else if (cm.equals("TE")) {
                                rval.insDouble("VE", 0);
                                rval.insDouble("OM", 0);
                                rval.insDouble("RE", 0);
                                rval.insDouble("RV", 0);
                                rval.insDouble("TU", 0);
                                rval.insDouble("TE", qta);
                                rval.insDouble("DC", 0);
                                rval.insDouble("SS", 0);
                            } else if (cm.equals("DC")) {
                                rval.insDouble("VE", 0);
                                rval.insDouble("OM", 0);
                                rval.insDouble("RE", 0);
                                rval.insDouble("RV", 0);
                                rval.insDouble("TU", 0);
                                rval.insDouble("TE", 0);
                                rval.insDouble("DC", qta);
                                rval.insDouble("SS", 0);
                            } else if (cm.equals("SS")) {
                                rval.insDouble("VE", 0);
                                rval.insDouble("OM", 0);
                                rval.insDouble("RE", 0);
                                rval.insDouble("RV", 0);
                                rval.insDouble("TU", 0);
                                rval.insDouble("TE", 0);
                                rval.insDouble("DC", 0);
                                rval.insDouble("SS", qta);
                            }
                            hart.put(artcod, rval);
                        }
                    }
                }
                cc.close();
            }
            cmov.close();

            int nrigheform = 6;
            if (Env.stampasedesec)
                nrigheform += 4;
            if (stampacompatta)
                nrigheform += 1;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheform * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("RIEPILOGO ARTICOLI", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "RIEPILOGO ARTICOLI", lf, 24);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            String stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (stampacompatta) {
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, "PRODOTTO      VENDITE  OMAGGI RESI  TRASF.U. TRASF.E.", lf, 18);
                Env.cpclPrinter.printAndroidFont(0, posy + 32, Typeface.MONOSPACE, true, false, false, "   INIZIALE   CARICHI  SCAR.SEDE RETTIFICHE  GIACENZA", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
                posy += 60;
            } else {
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                     UM CAUSALE         QTA ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                    UM CAUSALE         QTA ", lf, 18);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
                posy += 30;
            }
            Env.cpclPrinter.printForm();

            int n = hart.size();
            String[] vcau = {"GI", "VE", "OM", "RV", "TU", "TE", "DC", "SS", "RE", "GF"};
            String[] dcau = {"GIAC.INIZ.", "VENDITE", "OMAGGI", "RESI", "TRASF.USC.", "TRASF.ENT.", "CARICHI", "SCAR.SEDE", "RETTIFICHE", "GIAC.FINE"};
            for (int i = 0; i < n; i++) {
                String key = hart.firstKey();
                Record rval = hart.get(key);
                hart.remove(key);
                double qtavend = 0;
                qtavend = rval.leggiDouble("VE");
                double qtaom = 0;
                qtaom = rval.leggiDouble("OM");
                double qtaresa = 0;
                qtaresa = rval.leggiDouble("RV");
                double qtatvs = 0;
                qtatvs = rval.leggiDouble("TU");
                double qtatda = 0;
                qtatda = rval.leggiDouble("TE");
                double qtacar = 0;
                qtacar = rval.leggiDouble("DC");
                double qtasca = 0;
                qtasca = rval.leggiDouble("SS");
                double qtarett = 0;
                qtarett = rval.leggiDouble("RE");
                double giac = leggiGiacenzaArticolo(key);
                double giaciniz = giac + qtavend + qtaom - qtaresa + qtatvs - qtatda - qtacar + qtasca - qtarett;
                if (stampacompatta) {
                    Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    Env.cpclPrinter.setSpeed(5);
                    posy = 0;
                    String riga = "";
                    riga += funzStringa.riempiStringa(rval.leggiStringa("artcod") + " " + rval.leggiStringa("artdescr"), 14, funzStringa.SX, ' ');
                    riga += funzStringa.riempiStringa(Formattazione.formatta(qtavend, "###0.00", Formattazione.NO_SEGNO), 7, funzStringa.DX, ' ');
                    riga += funzStringa.riempiStringa(Formattazione.formatta(qtaom, "###0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                    riga += funzStringa.riempiStringa(Formattazione.formatta(qtaresa, "##0.00", Formattazione.NO_SEGNO), 5, funzStringa.DX, ' ');
                    riga += funzStringa.riempiStringa(Formattazione.formatta(qtatvs, "#####0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
                    riga += funzStringa.riempiStringa(Formattazione.formatta(qtatda, "#####0.00", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    riga = funzStringa.riempiStringa(Formattazione.formatta(giaciniz, "######0.00", Formattazione.NO_SEGNO), 11, funzStringa.DX, ' ');
                    riga += funzStringa.riempiStringa(Formattazione.formatta(qtacar, "######0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
                    riga += funzStringa.riempiStringa(Formattazione.formatta(qtasca, "######0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
                    riga += funzStringa.riempiStringa(Formattazione.formatta(qtarett, "######0.00", Formattazione.NO_SEGNO), 11, funzStringa.DX, ' ');
                    riga += funzStringa.riempiStringa(Formattazione.formatta(giac, "######0.00", Formattazione.NO_SEGNO), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    Env.cpclPrinter.printForm();
                } else {
                    boolean stampaart = true;
                    for (int j = 0; j < vcau.length; j++) {
                        double qta = 0;
                        if (vcau[j].equals("GI"))
                            qta = giaciniz;
                        else if (vcau[j].equals("GF"))
                            qta = giac;
                        else
                            qta = rval.leggiDouble(vcau[j]);
                        if (qta != 0 || vcau[j].equals("GI") || vcau[j].equals("GF")) {
                            if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                                Env.cpclPrinter.setTone(20);
                                posy = 0;
                                String riga = "";
                                if (stampaart) {
                                    riga += " " + funzStringa.riempiStringa(rval.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                                    riga += " " + funzStringa.riempiStringa(rval.leggiStringa("artdescr"), 19, funzStringa.SX, ' ');
                                    riga += " " + funzStringa.riempiStringa(rval.leggiStringa("artum"), 2, funzStringa.SX, ' ');
                                    stampaart = false;
                                } else {
                                    riga += funzStringa.spazi(32);
                                }
                                riga += " " + funzStringa.riempiStringa(dcau[j], 10, funzStringa.DX, ' ');
                                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(qta, "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                                Env.cpclPrinter.printForm();
                                if (vcau[j].equals("GF")) {
                                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                                    Env.cpclPrinter.setTone(20);
                                    posy = 0;
                                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, funzStringa.stringa("-", 53), lf, 18);
                                    Env.cpclPrinter.printForm();
                                }
                            } else {
                                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                                Env.cpclPrinter.setTone(20);
                                posy = 0;
                                String riga = "";
                                if (stampaart) {
                                    riga += " " + funzStringa.riempiStringa(rval.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                                    riga += " " + funzStringa.riempiStringa(rval.leggiStringa("artdescr"), 34, funzStringa.SX, ' ');
                                    riga += " " + funzStringa.riempiStringa(rval.leggiStringa("artum"), 2, funzStringa.SX, ' ');
                                    stampaart = false;
                                } else {
                                    riga += funzStringa.spazi(47);
                                }
                                riga += " " + funzStringa.riempiStringa(dcau[j], 10, funzStringa.DX, ' ');
                                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(qta, "###0.000", Formattazione.SEGNO_SX_NEG), 8, funzStringa.DX, ' ');
                                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                                Env.cpclPrinter.printForm();
                                if (vcau[j].equals("GF")) {
                                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                                    Env.cpclPrinter.setTone(20);
                                    posy = 0;
                                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, funzStringa.stringa("-", 67), lf, 18);
                                    Env.cpclPrinter.printForm();
                                }
                            }
                            posy += 30;
                        }
                    }
                }

                if (i % Env.stampe_par1 == 0 && i > 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;

            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }


    public static void stampaControllo(Context context) {
        try {
            // doc.rettifica carico
            int ndocrettcar = 0;
            Cursor cnum = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 11 AND movtrasf<>'S'", null);
            if (cnum.moveToFirst() && !cnum.isNull(0)) {
                ndocrettcar = cnum.getInt(0);
            }
            cnum.close();
            ArrayList<Record> vrettcar = new ArrayList();
            Cursor cm = Env.db.rawQuery(
                    "SELECT movnum,movdata FROM movimenti WHERE movtipo = 11 AND movtrasf <> 'S' ORDER BY movnum", null);
            while (cm.moveToNext()) {
                String[] parsc = new String[2];
                parsc[0] = "" + cm.getInt(0);
                parsc[1] = cm.getString(1);
                Cursor cc = Env.db.rawQuery(
                        "SELECT rmartcod,artdescr,artum,rmlotto,rmqta,rmcaumag FROM righemov LEFT JOIN articoli ON righemov.rmartcod = articoli.artcod " +
                                "WHERE rmmovtipo = 11 AND rmmovnum = ? AND rmmovdata = ? ORDER BY rmartcod,rmlotto", parsc);
                while (cc.moveToNext()) {
                    Record rx = new Record();
                    rx.insStringa("artcod", cc.getString(0));
                    rx.insStringa("artdescr", cc.getString(1));
                    rx.insStringa("um", cc.getString(2));
                    rx.insStringa("lotto", cc.getString(3));
                    rx.insDouble("qta", cc.getDouble(4));
                    String cau = "";
                    if (cc.getString(5).equals("DP"))
                        cau = "IN PIU'";
                    else
                        cau = "IN MENO";
                    rx.insStringa("cau", cau);
                    vrettcar.add(rx);
                }
                cc.close();
            }
            cm.close();

            // n.doc.trasf.vs automezzo
            int ndoctrasfvs = 0;
            cnum = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 7 AND movtrasf<>'S'", null);
            if (cnum.moveToFirst() && !cnum.isNull(0)) {
                ndoctrasfvs = cnum.getInt(0);
            }
            cnum.close();
            ArrayList<Record> vtrasfvs = new ArrayList();
            cm = Env.db.rawQuery(
                    "SELECT movnum,movdata,movnote1 FROM movimenti WHERE movtipo = 7 AND movtrasf <> 'S' ORDER BY movnum", null);
            while (cm.moveToNext()) {
                String[] parsc = new String[2];
                parsc[0] = "" + cm.getInt(0);
                parsc[1] = cm.getString(1);
                Cursor cc = Env.db.rawQuery(
                        "SELECT rmartcod,artdescr,artum,rmlotto,rmqta,rmcaumag FROM righemov LEFT JOIN articoli ON righemov.rmartcod = articoli.artcod " +
                                "WHERE rmmovtipo = 7 AND rmmovnum = ? AND rmmovdata = ? ORDER BY rmartcod,rmlotto", parsc);
                while (cc.moveToNext()) {
                    Record rx = new Record();
                    rx.insStringa("artcod", cc.getString(0));
                    rx.insStringa("artdescr", cc.getString(1));
                    rx.insStringa("um", cc.getString(2));
                    rx.insStringa("lotto", cc.getString(3));
                    rx.insDouble("qta", cc.getDouble(4));
                    rx.insStringa("dest", cm.getString(2));
                    vtrasfvs.add(rx);
                }
                cc.close();
            }
            cm.close();

            // n.doc.trasf.da automezzo
            int ndoctrasfda = 0;
            cnum = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 8 AND movtrasf<>'S' AND movnote2 <> 'CARAUTO'", null);
            if (cnum.moveToFirst() && !cnum.isNull(0)) {
                ndoctrasfda = cnum.getInt(0);
            }
            cnum.close();
            ArrayList<Record> vtrasfda = new ArrayList();
            cm = Env.db.rawQuery(
                    "SELECT movnum,movdata,movnote1 FROM movimenti WHERE movtipo = 8 AND movtrasf <> 'S' ORDER BY movnum", null);
            while (cm.moveToNext()) {
                String[] parsc = new String[2];
                parsc[0] = "" + cm.getInt(0);
                parsc[1] = cm.getString(1);
                Cursor cc = Env.db.rawQuery(
                        "SELECT rmartcod,artdescr,artum,rmlotto,rmqta,rmcaumag FROM righemov LEFT JOIN articoli ON righemov.rmartcod = articoli.artcod " +
                                "WHERE rmmovtipo = 8 AND rmmovnum = ? AND rmmovdata = ? ORDER BY rmartcod,rmlotto", parsc);
                while (cc.moveToNext()) {
                    Record rx = new Record();
                    rx.insStringa("artcod", cc.getString(0));
                    rx.insStringa("artdescr", cc.getString(1));
                    rx.insStringa("um", cc.getString(2));
                    rx.insStringa("lotto", cc.getString(3));
                    rx.insDouble("qta", cc.getDouble(4));
                    rx.insStringa("orig", cm.getString(2));
                    vtrasfda.add(rx);
                }
                cc.close();
            }
            cm.close();

            // n.scar.sede
            int ndocscasede = 0;
            cnum = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 6 AND movtrasf<>'S'", null);
            if (cnum.moveToFirst() && !cnum.isNull(0)) {
                ndocscasede = cnum.getInt(0);
            }
            cnum.close();
            ArrayList<Record> vscasede = new ArrayList();
            cm = Env.db.rawQuery(
                    "SELECT movnum,movdata FROM movimenti WHERE movtipo = 6 AND movtrasf <> 'S' ORDER BY movnum", null);
            while (cm.moveToNext()) {
                String[] parsc = new String[2];
                parsc[0] = "" + cm.getInt(0);
                parsc[1] = cm.getString(1);
                Cursor cc = Env.db.rawQuery(
                        "SELECT rmartcod,artdescr,artum,rmlotto,rmqta,rmcaumag FROM righemov LEFT JOIN articoli ON righemov.rmartcod = articoli.artcod " +
                                "WHERE rmmovtipo = 6 AND rmmovnum = ? AND rmmovdata = ? ORDER BY rmartcod,rmlotto", parsc);
                while (cc.moveToNext()) {
                    Record rx = new Record();
                    rx.insStringa("artcod", cc.getString(0));
                    rx.insStringa("artdescr", cc.getString(1));
                    rx.insStringa("um", cc.getString(2));
                    rx.insStringa("lotto", cc.getString(3));
                    rx.insDouble("qta", cc.getDouble(4));
                    String cau = "";
                    if (cc.getString(5).equals("SS"))
                        cau = "MERCE VENDIB.";
                    else if (cc.getString(5).equals("PE"))
                        cau = "MERCE ROTTA";
                    else
                        cau = "MERCE SCADUTA";
                    rx.insStringa("cau", cau);
                    vscasede.add(rx);
                }
                cc.close();
            }
            cm.close();

            int nrigheint = 15;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, 576, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("STAMPA DI CONTROLLO", 24, 576), posy, Typeface.MONOSPACE, true, false, false, "STAMPA DI CONTROLLO", 576, 24);
            posy += 30;
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, 576, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, 576, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", 576, 20);
            posy += 30;
            String[] parsdep = new String[1];
            parsdep[0] = Env.depsede;
            Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
            cdep.moveToFirst();
            stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, 576, 20);
            posy += 30;
            cdep.close();
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, 576, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, 576, 20);
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printForm();

            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.TRASFERIMENTI VS ALTRO MEZZO: " + ndoctrasfvs, 576, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO            UM LOTTO       QTA DESTINAZIONE ", 576, 18);
            Env.cpclPrinter.printBox(0, posy, 576, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();
            for (int i = 0; i < vtrasfvs.size(); i++) {
                Record rx = vtrasfvs.get(i);
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.setSpeed(5);
                posy = 0;
                String riga = "";
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 10, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("dest"), 12, funzStringa.SX, ' ');
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, 576, 18);
                Env.cpclPrinter.printForm();
                posy += 30;
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 10)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, 576, posy + 15, 1);
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printForm();

            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.TRASFERIMENTI DA ALTRO MEZZO: " + ndoctrasfda, 576, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO            UM LOTTO       QTA ORIGINE      ", 576, 18);
            Env.cpclPrinter.printBox(0, posy, 576, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();
            for (int i = 0; i < vtrasfda.size(); i++) {
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.setSpeed(5);
                posy = 0;
                Record rx = vtrasfda.get(i);
                String riga = "";
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 10, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("orig"), 12, funzStringa.SX, ' ');
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, 576, 18);
                posy += 30;
                Env.cpclPrinter.printForm();
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 10)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, 576, posy + 15, 1);
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printForm();

            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.SCARICHI A SEDE: " + ndocscasede, 576, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO           UM LOTTO        QTA CAUSALE      ", 576, 18);
            Env.cpclPrinter.printBox(0, posy, 576, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();
            for (int i = 0; i < vscasede.size(); i++) {
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.setSpeed(5);
                posy = 0;
                Record rx = vscasede.get(i);
                String riga = "";
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 9, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.000", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("cau"), 12, funzStringa.SX, ' ');
                System.out.println(riga);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, 576, 18);
                posy += 30;
                Env.cpclPrinter.printForm();
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 10)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, 576, posy + 15, 1);
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    public static void stampaOmaggi(Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            ArrayList<Record> vrighe = new ArrayList();
            Cursor c = Env.db.rawQuery(
                    "SELECT rmartcod,artdescr,artum,sum(rmqta) FROM righemov " +
                            "LEFT JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum) " +
                            " LEFT JOIN articoli ON righemov.rmartcod = articoli.artcod " +
                            "WHERE (rmcaumag='OT' OR rmcaumag='OM') AND movimenti.movtrasf='N' AND movimenti.movsospeso='N' GROUP BY rmartcod ORDER BY rmartcod", null);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", c.getString(0));
                rx.insStringa("artdescr", c.getString(1));
                rx.insStringa("um", c.getString(2));
                rx.insDouble("qta", c.getDouble(3));
                vrighe.add(rx);
            }
            c.close();

            int nrigheform = 6;
            if (Env.stampasedesec)
                nrigheform += 4;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheform * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("OMAGGI", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "OMAGGI", lf, 24);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            String stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                UM   QTA OM.", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            for (int i = 0; i < vrighe.size(); i++) {
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Record rx = vrighe.get(i);
                String riga = " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 30, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                posy += 30;
                Env.cpclPrinter.printForm();
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;

            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    public static void stampaTotali(boolean finegiornata, Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            // calcolo ddt emessi
            int numddt = 0;
            Cursor c = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 0 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                numddt = c.getInt(0);
            }
            c.close();
            int minnum = 0;
            c = Env.db.rawQuery(
                    "SELECT MIN(movnum) FROM movimenti WHERE movtipo = 0 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                minnum = c.getInt(0);
            }
            c.close();
            int maxnum = 0;
            c = Env.db.rawQuery(
                    "SELECT MAX(movnum) FROM movimenti WHERE movtipo = 0 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                maxnum = c.getInt(0);
            }
            c.close();
            // calcolo ddt di reso
            int numddtreso = 0;
            c = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 4 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                numddtreso = c.getInt(0);
            }
            c.close();
            int minnumreso = 0;
            c = Env.db.rawQuery(
                    "SELECT MIN(movnum) FROM movimenti WHERE movtipo = 4 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                minnumreso = c.getInt(0);
            }
            c.close();
            int maxnumreso = 0;
            c = Env.db.rawQuery(
                    "SELECT MAX(movnum) FROM movimenti WHERE movtipo = 4 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                maxnumreso = c.getInt(0);
            }
            c.close();
            // calcolo ddt qta
            int numddtqta = 0;
            c = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 1 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                numddtqta = c.getInt(0);
            }
            c.close();
            int minddtqta = 0;
            c = Env.db.rawQuery(
                    "SELECT MIN(movnum) FROM movimenti WHERE movtipo = 1 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                minddtqta = c.getInt(0);
            }
            c.close();
            int maxddtqta = 0;
            c = Env.db.rawQuery(
                    "SELECT MAX(movnum) FROM movimenti WHERE movtipo = 1 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                maxddtqta = c.getInt(0);
            }
            c.close();

            // calcolo fatture
            int numfatt = 0;
            c = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 2 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                numfatt = c.getInt(0);
            }
            c.close();
            int minfatt = 0;
            c = Env.db.rawQuery(
                    "SELECT MIN(movnum) FROM movimenti WHERE movtipo = 2 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                minfatt = c.getInt(0);
            }
            c.close();
            int maxfatt = 0;
            c = Env.db.rawQuery(
                    "SELECT MAX(movnum) FROM movimenti WHERE movtipo = 2 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                maxfatt = c.getInt(0);
            }
            c.close();

            // calcolo corrispettivi
            int numcorr = 0;
            c = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 3 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                numcorr = c.getInt(0);
            }
            c.close();
            int mincorr = 0;
            c = Env.db.rawQuery(
                    "SELECT MIN(movnum) FROM movimenti WHERE movtipo = 3 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                mincorr = c.getInt(0);
            }
            c.close();
            int maxcorr = 0;
            c = Env.db.rawQuery(
                    "SELECT MAX(movnum) FROM movimenti WHERE movtipo = 3 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                maxcorr = c.getInt(0);
            }
            c.close();

            // calcolo ddt annullati
            int numddtann = 0;
            ArrayList<Record> vann = new ArrayList();
            c = Env.db.rawQuery(
                    "SELECT movtipo,movnum,movdata,movkey FROM movimenti WHERE movtrasf<>'S' AND movsospeso = 'S' ORDER BY movtipo,movnum,movdata", null);
            while (c.moveToNext()) {
                numddtann++;
                Record rann = new Record();
                rann.insIntero("tipo", c.getInt(0));
                rann.insIntero("num", c.getInt(1));
                rann.insStringa("data", c.getString(2));
                vann.add(rann);
            }
            c.close();

            // calcolo DDT con omaggi
            int numddtom = 0;
            ArrayList vomdt = new ArrayList();
            ArrayList vomnum = new ArrayList();
            ArrayList vomart = new ArrayList();
            ArrayList vomdart = new ArrayList();
            ArrayList vomqta = new ArrayList();
            ArrayList vomcau = new ArrayList();
            Cursor coma = Env.db.rawQuery(
                    "SELECT movdata,movnum,rmartcod,rmqta,rmcaumag,artdescr FROM righemov INNER JOIN articoli ON righemov.rmartcod = articoli.artcod LEFT JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum) WHERE (rmcaumag = 'OT' OR rmcaumag = 'SM' OR rmcaumag = 'OM') AND movtrasf<>'S' AND movsospeso = 'N'", null);
            while (coma.moveToNext()) {
                vomdt.add(coma.getString(0));
                vomnum.add(coma.getInt(1));
                vomart.add(coma.getString(2));
                vomqta.add(coma.getDouble(3));
                vomcau.add(coma.getString(4));
                vomdart.add(coma.getString(5));
            }
            coma.close();
            HashMap<Integer, String> hddtom = new HashMap();
            for (int i = 0; i < vomnum.size(); i++) {
                if (!hddtom.containsKey((int) vomnum.get(i)))
                    hddtom.put((int) vomnum.get(i), "");
            }
            numddtom = hddtom.size();

            // calcolo DDT carico e integrazione
            int numcarint = 0;
            Cursor cci = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 5 AND movtrasf<>'S'", null);
            if (cci.moveToFirst() && !cci.isNull(0))
                numcarint = cci.getInt(0);
            cci.close();
            // calcolo scarichi a sede
            int numscasede = 0;
            Cursor css = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 6 AND movtrasf<>'S'", null);
            if (css.moveToFirst() && !css.isNull(0))
                numscasede = css.getInt(0);
            css.close();
            // calcolo trasf.a altro mezzo
            int numtrasfamezzo = 0;
            Cursor ctv = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 7 AND movtrasf<>'S'", null);
            if (ctv.moveToFirst() && !ctv.isNull(0))
                numtrasfamezzo = ctv.getInt(0);
            ctv.close();
            // calcolo trasf.da altro mezzo
            int numtrasfdamezzo = 0;
            Cursor ctd = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 8 AND movtrasf<>'S'", null);
            if (ctd.moveToFirst() && !ctd.isNull(0))
                numtrasfdamezzo = ctd.getInt(0);
            ctd.close();

            // incassi da vendite del giorno
            double totaleincassato = 0;
            double totincgiornocontanti = 0;
            double totincgiornononcontanti = 0;
            ArrayList<Record> vincg = new ArrayList();
            Cursor cincg = Env.db.rawQuery(
                    "SELECT movclicod, movdata, movnum, movacconto, clinome,movaccontotipocassa FROM movimenti LEFT JOIN clienti ON (movimenti.movclicod = clienti.clicodice) WHERE movtipo IN (0,2) AND movtrasf='N' AND movsospeso = 'N' AND movacconto <> 0", null);
            while (cincg.moveToNext()) {
                Record rincg = new Record();
                rincg.insStringa("clicod", cincg.getString(0));
                rincg.insStringa("clinome", cincg.getString(4));
                rincg.insStringa("data", cincg.getString(1));
                rincg.insIntero("num", cincg.getInt(2));
                rincg.insDouble("acconto", cincg.getDouble(3));
                rincg.insIntero("tipocassa", cincg.getInt(5));
                vincg.add(rincg);
                totaleincassato += cincg.getDouble(3);
                if (cincg.getInt(5) == 0)
                    totincgiornocontanti += cincg.getDouble(3);
                else
                    totincgiornononcontanti += cincg.getDouble(3);
            }
            cincg.close();
            // fatture incassate nel giorno e non ancora trasmesse
            Cursor cfat = Env.db.rawQuery(
                    "SELECT movclicod, movdata, movnum, movacconto, clinome FROM movimenti LEFT JOIN clienti ON (movimenti.movclicod = clienti.clicodice) WHERE movtipo = 2 AND movtrasf='N' AND movsospeso = 'N' AND movacconto = 0", null);
            while (cfat.moveToNext()) {
                // cerca su incassi
                double saldofat = 0;
                double saldofatc = 0;
                double saldofatnc = 0;
                Cursor cincf = Env.db.rawQuery(
                        "SELECT isaldo,isctipocassa FROM incassi WHERE isctipo='F' AND iscsezdoc='" + Env.sezfattura + "' AND iscdatadoc = '" + cfat.getString(1) + "' AND iscnumdoc = " + cfat.getInt(2) + " AND isctrasf = 'N'", null);
                while (cincf.moveToNext()) {
                    saldofat += cincf.getDouble(0);
                    if (cincf.getInt(1) == 0)
                        saldofatc += cincf.getDouble(0);
                    else
                        saldofatnc += cincf.getDouble(0);
                }
                cincf.close();
                if (saldofat > 0) {
                    if (saldofatc > 0) {
                        Record rincg = new Record();
                        rincg.insStringa("clicod", cfat.getString(0));
                        rincg.insStringa("clinome", cfat.getString(4));
                        rincg.insStringa("data", cfat.getString(1));
                        rincg.insIntero("num", cfat.getInt(2));
                        rincg.insDouble("acconto", saldofatc);
                        rincg.insIntero("tipocassa", 0);
                        vincg.add(rincg);
                        totaleincassato += saldofatc;
                        totincgiornocontanti += saldofatc;
                    } else {
                        Record rincg = new Record();
                        rincg.insStringa("clicod", cfat.getString(0));
                        rincg.insStringa("clinome", cfat.getString(4));
                        rincg.insStringa("data", cfat.getString(1));
                        rincg.insIntero("num", cfat.getInt(2));
                        rincg.insDouble("acconto", saldofatnc);
                        rincg.insIntero("tipocassa", 1);
                        vincg.add(rincg);
                        totaleincassato += saldofatnc;
                        totincgiornononcontanti += saldofatnc;
                    }
                }
            }
            cfat.close();

            // incassi da sospesi
            double totalesospeso = 0;
            double totincsospesicontanti = 0;
            double totincsospesinoncontanti = 0;
            ArrayList<Record> vincs = new ArrayList();
            Cursor cincs = Env.db.rawQuery(
                    "SELECT iscclicod,iscdatadoc,iscnumdoc,isaldo,isctipoop,clinome,isctipocassa FROM incassi LEFT JOIN clienti ON (incassi.iscclicod = clienti.clicodice) WHERE isctipo = 'B' ", null);
            while (cincs.moveToNext()) {
                double val = cincs.getDouble(3);
                if (!cincs.getString(4).equals("V"))
                    val = -val;
                Record rincs = new Record();
                rincs.insStringa("clicod", cincs.getString(0));
                rincs.insStringa("clinome", cincs.getString(5));
                rincs.insStringa("data", cincs.getString(1));
                rincs.insIntero("num", cincs.getInt(2));
                rincs.insDouble("saldo", val);
                rincs.insDouble("tipocassa", cincs.getInt(6));
                vincs.add(rincs);
                totalesospeso += val;
                if (cincs.getInt(6) == 0)
                    totincsospesicontanti += val;
                else
                    totincsospesinoncontanti += val;
            }
            cincs.close();

            // incassi da fatture ncred insoluti
            double totalefatture = 0;
            double totincfattcontanti = 0;
            double totincfattnoncontanti = 0;
            ArrayList<Record> vincf = new ArrayList();
            Cursor cincf = Env.db.rawQuery(
                    "SELECT iscclicod, iscdatadoc, iscnumdoc, isaldo, clinome,isctipo,iscdescr,isctipoop,iscsezdoc,isctipocassa FROM incassi LEFT JOIN clienti ON (incassi.iscclicod = clienti.clicodice) WHERE isctipo = 'F' OR isctipo = 'N' OR isctipo = 'C' ORDER BY isctipo", null);
            while (cincf.moveToNext()) {
                // controlla se fattura del giorno
                boolean incok = true;
                Cursor cfg = Env.db.rawQuery("SELECT movtipo FROM movimenti WHERE movtipo = 2 AND movdoc = '" + Env.docfattura + "' AND movsez = '" + cincf.getString(8) + "' AND " +
                        "movdata = '" + cincf.getString(1) + "' AND movnum = " + cincf.getInt(2) + " AND movtrasf = 'N'", null);
                if (cfg.moveToFirst())
                    incok = false;
                cfg.close();
                if (incok) {
                    Record rincf = new Record();
                    rincf.insStringa("tipo", cincf.getString(5));
                    rincf.insStringa("clicod", cincf.getString(0));
                    if (!cincf.getString(6).equals(""))
                        rincf.insStringa("descr", cincf.getString(6));
                    else
                        rincf.insStringa("descr", cincf.getString(4));
                    String aa = "";
                    if (cincf.getString(1).equals("0000-00-00") || cincf.getString(1).equals("") || cincf.getString(1).length() != 10)
                        rincf.insStringa("data", "");
                    else
                        rincf.insStringa("data", cincf.getString(1));
                    rincf.insIntero("num", cincf.getInt(2));
                    if (cincf.getString(7).equals("V"))
                        rincf.insDouble("saldo", cincf.getDouble(3));
                    else
                        rincf.insDouble("saldo", -cincf.getDouble(3));
                    rincf.insIntero("tipocassa", cincf.getInt(9));
                    vincf.add(rincf);
                    if (cincf.getString(7).equals("V")) {
                        totalefatture += cincf.getDouble(3);
                        if (cincf.getInt(9) == 0)
                            totincfattcontanti += cincf.getDouble(3);
                        else
                            totincfattnoncontanti += cincf.getDouble(3);
                    } else {
                        totalefatture -= cincf.getDouble(3);
                        if (cincf.getInt(9) == 0)
                            totincfattcontanti -= cincf.getDouble(3);
                        else
                            totincfattnoncontanti -= cincf.getDouble(3);
                    }
                }
            }
            cincf.close();

            // sospesi da incassare
            double totalesosp = 0;
            ArrayList<Record> vsosp = new ArrayList();
            Cursor csosp = Env.db.rawQuery(
                    "SELECT sctipo,scsezdoc,scdatadoc,scnumdoc,sctipoop,scnote,scclicod,scdestcod," +
                            "scdatascad,sctiposcad,scimportoscad,scresiduoscad,scscadid,clinome FROM scoperti LEFT JOIN clienti ON scoperti.scclicod = clienti.clicodice WHERE " +
                            "sctipo = 'B' AND scresiduoscad > 0 ORDER BY scdatadoc DESC,scnumdoc ASC", null);
            while (csosp.moveToNext()) {
                Record rsosp = new Record();
                rsosp.insStringa("clicod", csosp.getString(6));
                if (!csosp.isNull(13))
                    rsosp.insStringa("clinome", csosp.getString(13));
                else
                    rsosp.insStringa("clinome", "");
                rsosp.insStringa("data", csosp.getString(2));
                rsosp.insIntero("num", csosp.getInt(3));
                double sospdoc = csosp.getDouble(11);
                if (!csosp.getString(4).equals("V"))
                    sospdoc = -sospdoc;
                rsosp.insDouble("sospeso", sospdoc);
                vsosp.add(rsosp);
                totalesosp += sospdoc;
            }
            csosp.close();

            // totale vendite
            double totvend = calcolaTotaleVenditeSessione();

            // totale costi
            double totcosti = calcolaTotaleCostiSessione();

            // totale costi omaggi
            double totcostiom = calcolaTotaleCostiOmaggiSessione();

            // totale costi sconti merce
            double totcostism = calcolaTotaleCostiScontiMerceSessione();

            // % vendite su costi
            double percvendcosti = 0;
            if (totcosti > 0 || totcostiom > 0 || totcostism > 0)
                percvendcosti = OpValute.arrotondaMat((totvend - (totcosti + totcostiom + totcostism)) / (totcosti + totcostiom + totcostism) * 100.0, 2);

            int nrigheint = 6;
            if (Env.stampasedesec)
                nrigheint += 2;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            if (finegiornata)
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("FINE GIORNATA", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "FINE GIORNATA", lf, 24);
            else
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("RIEPILOGO TOTALE", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "RIEPILOGO TOTALE", lf, 24);
            posy += 30;

            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            String stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (Env.stampasedesec) {
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                String lp = funzStringa.tagliaStringa("Luogo di partenza:" + cdep.getString(2) + " " + cdep.getString(0) + " " + cdep.getString(1), gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, lp, lf, 20);
                posy += 30;
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printForm();

            // ddt emessi
            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.DDT EMESSI      : " + numddt + " (dal " + minnum + " al " + maxnum + ")", lf, 20);
            posy += 30;
            //posy += 30;
            Env.cpclPrinter.printForm();

            // ddt di reso
            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.DDT RESO EMESSI : " + numddtreso + " (dal " + minnumreso + " al " + maxnumreso + ")", lf, 20);
            posy += 30;
            //posy += 30;
            Env.cpclPrinter.printForm();

            // ddt qta
            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.DDT QUANTITA'   : " + numddtqta + " (dal " + minddtqta + " al " + maxddtqta + ")", lf, 20);
            posy += 30;
            //posy += 30;
            Env.cpclPrinter.printForm();

            // fatture
            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.FATTURE         : " + numfatt + " (dal " + minfatt + " al " + maxfatt + ")", lf, 20);
            posy += 30;
            //posy += 30;
            Env.cpclPrinter.printForm();

            // corrispettivi
            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.CORRISPETTIVI   : " + numcorr + " (dal " + mincorr + " al " + maxcorr + ")", lf, 20);
            posy += 30;
            //posy += 30;
            Env.cpclPrinter.printForm();

            // ddt annullati
            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.DDT ANNULLATI   : " + numddtann, lf, 20);
            posy += 30;
            Env.cpclPrinter.printForm();
            if (numddtann > 0) {
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "ELENCO DDT ANNULLATI:", lf, 20);
                posy += 30;
                Env.cpclPrinter.printForm();
                for (int i = 0; i < vann.size(); i++) {
                    Record rann = vann.get(i);
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    stmp = funzStringa.tagliaStringa(" - n." + rann.leggiIntero("num") + " " + (new Data(rann.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    Env.cpclPrinter.printForm();
                }
                posy += 30;
            }
            Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, " ", lf, 20);
            Env.cpclPrinter.printForm();

            try {
                Thread.currentThread().sleep(5000);
            } catch (Exception ex) {
            }
//			while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//			{
//				Log.v("JAZZTV", "BUSY");
//				try
//				{
//					Thread.currentThread().sleep(Env.stampe_par2);
//				} catch (Exception ex)
//				{
//				}
//			}

            // ddt con omaggi
            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.DDT CON OMAGGI  : " + numddtom, lf, 20);
            posy += 30;
            //posy += 30;
            Env.cpclPrinter.printForm();
/*
			if (numddtom > 0)
			{
				Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "ELENCO OMAGGI:", lf, 24);
				posy += 30;
				Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " DATA     NUMERO   ARTICOLO                 QTA' CAU ", lf, 18);
				Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
				posy += 30;
				for (int i = 0; i < vomnum.size(); i++)
				{
					String dt = (String) vomdt.get(i);
					stmp = (new Data(dt, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
					Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
					stmp = "" + (int) vomnum.get(i);
					Env.cpclPrinter.printAndroidFont(119, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
					stmp = funzStringa.tagliaStringa(vomart.get(i) + "-" + vomdart.get(i), 20);
					Env.cpclPrinter.printAndroidFont(206, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
					stmp = funzStringa.riempiStringa(Formattazione.formatta((double) vomqta.get(i), "####0.00", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
					Env.cpclPrinter.printAndroidFont(435, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
					stmp = funzStringa.tagliaStringa((String) vomcau.get(i), 3);
					Env.cpclPrinter.printAndroidFont(532, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
					posy += 30;
				}
				posy += 30;
			}
			posy += 30;
*/
            // ddt carico e integrazione
            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            stmp = "N.DDT INTEGR.: " + funzStringa.riempiStringa("" + numcarint, 4, funzStringa.SX, ' ') +
                    " N.SCAR.A SEDE: " + funzStringa.riempiStringa("" + numscasede, 4, funzStringa.SX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            //posy += 30;
            Env.cpclPrinter.printForm();

            // trasf.altro mezzo
            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            stmp = "N.TR.VS AUTOM: " + funzStringa.riempiStringa("" + numtrasfamezzo, 4, funzStringa.SX, ' ') +
                    " N.CAR.DA AUTO: " + funzStringa.riempiStringa("" + numtrasfdamezzo, 4, funzStringa.SX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            //posy += 30;
            Env.cpclPrinter.printForm();

            try {
                Thread.currentThread().sleep(5000);
            } catch (Exception ex) {
            }
//			while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//			{
//				Log.v("JAZZTV", "BUSY");
//				try
//				{
//					Thread.currentThread().sleep(Env.stampe_par2);
//				} catch (Exception ex)
//				{
//				}
//			}

            // incassi da vendite del giorno
            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("INCASSI DA VENDITE DEL GIORNO", 20, lf), posy, Typeface.MONOSPACE, true, false, false, "INCASSI DA VENDITE DEL GIORNO", lf, 24);
            posy += 30; // 35
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                 DATA     N.DOCUM.      EURO ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                                DATA     N.DOCUM.      EURO ", lf, 18);
            posy += 30;
            Env.cpclPrinter.printForm();
            for (int i = 0; i < vincg.size(); i++) {
                Record rincg = vincg.get(i);
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    stmp = funzStringa.tagliaStringa(rincg.leggiStringa("clicod") + "-" + rincg.leggiStringa("clinome"), 23);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = (new Data(rincg.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    Env.cpclPrinter.printAndroidFont(271, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa("" + rincg.leggiIntero("num"), 8);
                    Env.cpclPrinter.printAndroidFont(370, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(rincg.leggiDouble("acconto"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(478, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                } else {
                    stmp = funzStringa.tagliaStringa(rincg.leggiStringa("clicod") + "-" + rincg.leggiStringa("clinome"), 38);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = (new Data(rincg.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    Env.cpclPrinter.printAndroidFont(434, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa("" + rincg.leggiIntero("num"), 8);
                    Env.cpclPrinter.printAndroidFont(532, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(rincg.leggiDouble("acconto"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(630, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                }
                posy += 30;
                Env.cpclPrinter.printForm();
                if (i % 10 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//					while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//					{
//						Log.v("JAZZTV", "BUSY");
//						try
//						{
//							Thread.currentThread().sleep(Env.stampe_par2);
//						} catch (Exception ex)
//						{
//						}
//					}
                }
            }
            posy += 30;
            if (Env.incsceglicassa) {
                Env.cpclPrinter.setForm(0, 200, 200, 5 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "INCASSI DEL GIORNO CONTANTI: " + Formattazione.formatta(totincgiornocontanti, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "INCASSI DEL GIORNO CON POS : " + Formattazione.formatta(totincgiornononcontanti, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE INCASSI DEL GIORNO  : " + Formattazione.formatta(totaleincassato, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                posy += 30;
                Env.cpclPrinter.printForm();
            } else {
                Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE INCASSI DEL GIORNO: " + Formattazione.formatta(totaleincassato, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                posy += 30;
                Env.cpclPrinter.printForm();
            }

            // incassi da sospesi
            if (vincs.size() > 0) {
                Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("INCASSI DA SOSPESI", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "INCASSI DA SOSPESI", lf, 24);
                posy += 30;
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                 DATA     N.DOCUM.      EURO ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                                DATA     N.DOCUM.      EURO ", lf, 18);
                posy += 30;
                Env.cpclPrinter.printForm();
                for (int i = 0; i < vincs.size(); i++) {
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    Record rincs = vincs.get(i);
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        stmp = funzStringa.tagliaStringa(rincs.leggiStringa("clicod") + "-" + rincs.leggiStringa("clinome"), 23);
                        Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = (new Data(rincs.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                        Env.cpclPrinter.printAndroidFont(271, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.tagliaStringa("" + rincs.leggiIntero("num"), 8);
                        Env.cpclPrinter.printAndroidFont(370, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.riempiStringa(Formattazione.formatta(rincs.leggiDouble("saldo"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(478, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    } else {
                        stmp = funzStringa.tagliaStringa(rincs.leggiStringa("clicod") + "-" + rincs.leggiStringa("clinome"), 38);
                        Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = (new Data(rincs.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                        Env.cpclPrinter.printAndroidFont(434, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.tagliaStringa("" + rincs.leggiIntero("num"), 8);
                        Env.cpclPrinter.printAndroidFont(532, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.riempiStringa(Formattazione.formatta(rincs.leggiDouble("saldo"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(630, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    }
                    posy += 30;
                    Env.cpclPrinter.printForm();
                    if (i % 10 == 0) {
                        try {
                            Thread.currentThread().sleep(5000);
                        } catch (Exception ex) {
                        }
//						while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//						{
//							Log.v("JAZZTV", "BUSY");
//							try
//							{
//								Thread.currentThread().sleep(Env.stampe_par2);
//							} catch (Exception ex)
//							{
//							}
//						}
                    }
                }
                posy += 30;
                if (Env.incsceglicassa) {
                    Env.cpclPrinter.setForm(0, 200, 200, 5 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "INCASSI DA SOSPESI CONTANTI: " + Formattazione.formatta(totincsospesicontanti, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                    posy += 30;
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "INCASSI DA SOSPESI CON POS : " + Formattazione.formatta(totincsospesinoncontanti, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                    posy += 30;
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE INCASSI DA SOSPESI  : " + Formattazione.formatta(totalesospeso, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                    posy += 30;
                    posy += 30;
                    Env.cpclPrinter.printForm();
                } else {
                    Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE INCASSI DA SOSPESI: " + Formattazione.formatta(totalesospeso, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                    posy += 30;
                    posy += 30;
                    Env.cpclPrinter.printForm();
                }
            }

            // incassi da fatture nc insoluti
            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("INCASSI DA FATTURE/NOTE CRED/INSOLUTI", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "INCASSI DA FATTURE/NOTE CRED/INSOLUTI", lf, 24);
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                 DATA     N.DOCUM.      EURO ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                                DATA     N.DOCUM.      EURO ", lf, 18);
            posy += 30;
            Env.cpclPrinter.printForm();
            for (int i = 0; i < vincf.size(); i++) {
                Record rincf = vincf.get(i);
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    stmp = funzStringa.tagliaStringa(rincf.leggiStringa("clicod") + "-" + rincf.leggiStringa("descr"), 23);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    if (!rincf.leggiStringa("data").equals(""))
                        stmp = (new Data(rincf.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(271, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    if (rincf.leggiIntero("num") > 0)
                        stmp = funzStringa.tagliaStringa("" + rincf.leggiIntero("num"), 8);
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(370, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(rincf.leggiDouble("saldo"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(478, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                } else {
                    stmp = funzStringa.tagliaStringa(rincf.leggiStringa("clicod") + "-" + rincf.leggiStringa("descr"), 38);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    if (!rincf.leggiStringa("data").equals(""))
                        stmp = (new Data(rincf.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(434, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    if (rincf.leggiIntero("num") > 0)
                        stmp = funzStringa.tagliaStringa("" + rincf.leggiIntero("num"), 8);
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(532, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(rincf.leggiDouble("saldo"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(630, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                }
                posy += 30;
                Env.cpclPrinter.printForm();
                if (i % 10 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//					while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//					{
//						Log.v("JAZZTV", "BUSY");
//						try
//						{
//							Thread.currentThread().sleep(Env.stampe_par2);
//						} catch (Exception ex)
//						{
//						}
//					}
                }
            }
            posy += 30;
            if (Env.incsceglicassa) {
                Env.cpclPrinter.setForm(0, 200, 200, 5 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "FATT./NC/INSOLUTI CONTANTI: " + Formattazione.formatta(totincfattcontanti, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "FATT./NC/INSOLUTI CON POS : " + Formattazione.formatta(totincfattnoncontanti, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE FATT./NC/INSOLUTI  : " + Formattazione.formatta(totalefatture, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                posy += 30;
                Env.cpclPrinter.printForm();
            } else {
                Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE FATT./NC/INSOLUTI  : " + Formattazione.formatta(totalefatture, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                posy += 30;
                Env.cpclPrinter.printForm();
            }

            Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            double totinc = OpValute.arrotondaMat(totaleincassato + totalesospeso + totalefatture, 2);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE GENERALE INCASSI: " + Formattazione.formatta(totinc, "####0.00", Formattazione.SEGNO_DX), lf, 24);
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printForm();

            if (Env.finegstampasospdainc && vsosp.size() > 0) {
                // sospesi da incassare
                Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("RIEP.SOSPESI DA INCASSARE AL " + (new Data()).formatta(Data.GG_MM_AA, "/"), 24, lf), posy, Typeface.MONOSPACE, true, false, false, "RIEP.SOSPESI DA INCASSARE AL " + (new Data()).formatta(Data.GG_MM_AA, "/"), lf, 24);
                posy += 30;
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                 DATA     N.DOCUM.      EURO ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                                DATA     N.DOCUM.      EURO ", lf, 18);
                posy += 30;
                Env.cpclPrinter.printForm();
                for (int i = 0; i < vsosp.size(); i++) {
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    Record rsosp = vsosp.get(i);
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        stmp = funzStringa.tagliaStringa(rsosp.leggiStringa("clicod") + "-" + rsosp.leggiStringa("clinome"), 23);
                        Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = (new Data(rsosp.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                        Env.cpclPrinter.printAndroidFont(271, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.tagliaStringa("" + rsosp.leggiIntero("num"), 8);
                        Env.cpclPrinter.printAndroidFont(370, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.riempiStringa(Formattazione.formatta(rsosp.leggiDouble("sospeso"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(478, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    } else {
                        stmp = funzStringa.tagliaStringa(rsosp.leggiStringa("clicod") + "-" + rsosp.leggiStringa("clinome"), 38);
                        Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = (new Data(rsosp.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                        Env.cpclPrinter.printAndroidFont(434, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.tagliaStringa("" + rsosp.leggiIntero("num"), 8);
                        Env.cpclPrinter.printAndroidFont(532, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.riempiStringa(Formattazione.formatta(rsosp.leggiDouble("sospeso"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(630, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    }
                    posy += 30;
                    Env.cpclPrinter.printForm();
                    if (i % 10 == 0) {
                        try {
                            Thread.currentThread().sleep(5000);
                        } catch (Exception ex) {
                        }
//						while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//						{
//							Log.v("JAZZTV", "BUSY");
//							try
//							{
//								Thread.currentThread().sleep(Env.stampe_par2);
//							} catch (Exception ex)
//							{
//							}
//						}
                    }
                }
                posy += 30;
                Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE SOSPESI: " + Formattazione.formatta(totalesosp, "####0.00", Formattazione.SEGNO_DX), lf, 24);
                posy += 30;
                posy += 30;
                Env.cpclPrinter.printForm();
            }
            posy += 30;

            try {
                Thread.currentThread().sleep(5000);
            } catch (Exception ex) {
            }
//			while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//			{
//				Log.v("JAZZTV", "BUSY");
//				try
//				{
//					Thread.currentThread().sleep(Env.stampe_par2);
//				} catch (Exception ex)
//				{
//				}
//			}

            Env.cpclPrinter.setForm(0, 200, 200, 5 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE VENDITE           : " + funzStringa.riempiStringa(Formattazione.formatta(totvend, "#######0.00", Formattazione.SEGNO_SX_NEG), 15, funzStringa.DX, ' '), lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE COSTI             : " + funzStringa.riempiStringa(Formattazione.formatta(totcosti, "#######0.00", Formattazione.SEGNO_SX_NEG), 15, funzStringa.DX, ' '), lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE COSTI OMAGGI      : " + funzStringa.riempiStringa(Formattazione.formatta(totcostiom, "#######0.00", Formattazione.SEGNO_SX_NEG), 15, funzStringa.DX, ' '), lf, 20);
            posy += 30;
//			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE COSTI SCONTI MERCE: " + funzStringa.riempiStringa(Formattazione.formatta(totcostism, "#######0.00", Formattazione.SEGNO_SX_NEG), 15, funzStringa.DX, ' '), lf, 24);
//			posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE VENDITE/COSTI %   : " + funzStringa.riempiStringa(Formattazione.formatta(percvendcosti, "#######0.00", Formattazione.SEGNO_SX_NEG), 15, funzStringa.DX, ' '), lf, 20);
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }
    }

    public static void stampaTotaliStor(boolean finegiornata, Context context, String fgdata, String fgora) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            String[] parsfg = new String[2];
            parsfg[0] = fgdata;
            parsfg[1] = fgora;
            final Cursor cfg = Env.db.rawQuery(
                    "SELECT " +
                            "fgnumddt,fgminddt,fgmaxddt,fgnumddtreso,fgminddtreso,fgmaxddtreso,fgnumddtqta,fgminddtqta,fgmaxddtqta,fgnumfatt,fgminfatt,fgmaxfatt,fgnumcorrisp,fgmincorrisp," +
                            "fgmaxcorrisp,fgnumddtann,fgnumddtom,fgnumcarint,fgnumscasede,fgnumtrasfamezzo,fgnumtrasfdamezzo,fgtotincassigiorno,fgtotincassisospesi,fgtotincassiftnc," +
                            "fgsospesidainc,fgtotvendite,fgtotcosti,fgtotcostiom,fgtotcostism,fgpercvendcosti,fgincassigiornocontanti,fgincassigiornononcontanti," +
                            "fgincassisospesicontanti,fgincassisospesinoncontanti,fgincassiftnccontanti,fgincassiftncnoncontanti" +
                            " FROM storico_fineg WHERE fgdata = ? AND fgora = ?", parsfg);
            cfg.moveToFirst();
            // calcolo ddt emessi
            int numddt = cfg.getInt(0);
            int minnum = cfg.getInt(1);
            int maxnum = cfg.getInt(2);
            // calcolo ddt di reso
            int numddtreso = cfg.getInt(3);
            int minnumreso = cfg.getInt(4);
            int maxnumreso = cfg.getInt(5);
            // calcolo ddt qta
            int numddtqta = cfg.getInt(6);
            int minddtqta = cfg.getInt(7);
            int maxddtqta = cfg.getInt(8);
            // calcolo fatture
            int numfatt = cfg.getInt(9);
            int minfatt = cfg.getInt(10);
            int maxfatt = cfg.getInt(11);
            // calcolo corrispettivi
            int numcorr = cfg.getInt(12);
            int mincorr = cfg.getInt(13);
            int maxcorr = cfg.getInt(14);

            // calcolo ddt annullati
            int numddtann = cfg.getInt(15);
            ArrayList<Record> vann = new ArrayList();
            Cursor c = Env.db.rawQuery(
                    "SELECT fg1tipo,fg1num,fg1datadoc FROM storico_fineg_ddtann WHERE fg1data=? AND fg1ora=? ORDER BY fg1tipo,fg1num,fg1datadoc", parsfg);
            while (c.moveToNext()) {
                numddtann++;
                Record rann = new Record();
                rann.insIntero("tipo", c.getInt(0));
                rann.insIntero("num", c.getInt(1));
                rann.insStringa("data", c.getString(2));
                vann.add(rann);
            }
            c.close();

            // calcolo DDT con omaggi
            int numddtom = cfg.getInt(16);
            ArrayList vomdt = new ArrayList();
            ArrayList vomnum = new ArrayList();
            ArrayList vomart = new ArrayList();
            ArrayList vomdart = new ArrayList();
            ArrayList vomqta = new ArrayList();
            ArrayList vomcau = new ArrayList();
            Cursor coma = Env.db.rawQuery(
                    "SELECT fg2datadoc,fg2num,fg2artcod,fg2qta,fg2caumag,fg2artdescr FROM storico_fineg_ddtom WHERE fg2data=? AND fg2ora=? ORDER BY fg2datadoc,fg2num,fg2artcod", parsfg);
            while (coma.moveToNext()) {
                vomdt.add(coma.getString(0));
                vomnum.add(coma.getInt(1));
                vomart.add(coma.getString(2));
                vomqta.add(coma.getDouble(3));
                vomcau.add(coma.getString(4));
                vomdart.add(coma.getString(5));
            }
            coma.close();
            HashMap<Integer, String> hddtom = new HashMap();
            for (int i = 0; i < vomnum.size(); i++) {
                if (!hddtom.containsKey((int) vomnum.get(i)))
                    hddtom.put((int) vomnum.get(i), "");
            }
            numddtom = hddtom.size();

            // calcolo DDT carico e integrazione
            int numcarint = cfg.getInt(17);
            // calcolo scarichi a sede
            int numscasede = cfg.getInt(18);
            // calcolo trasf.a altro mezzo
            int numtrasfamezzo = cfg.getInt(19);
            // calcolo trasf.da altro mezzo
            int numtrasfdamezzo = cfg.getInt(20);

            // incassi da vendite del giorno
            double totaleincassato = cfg.getDouble(21);
            double totincgiornocontanti = cfg.getDouble(30);
            double totincgiornononcontanti = cfg.getDouble(31);
            ArrayList<Record> vincg = new ArrayList();
            Cursor cincg = Env.db.rawQuery(
                    "SELECT fg3clicod,fg3clinome,fg3datadoc,fg3num,fg2acconto,fg3tipocassa FROM storico_fineg_incassigiorno WHERE fg3data=? AND fg3ora=? ORDER BY fg3datadoc,fg3num", parsfg);
            while (cincg.moveToNext()) {
                Record rincg = new Record();
                rincg.insStringa("clicod", cincg.getString(0));
                rincg.insStringa("clinome", cincg.getString(1));
                rincg.insStringa("data", cincg.getString(2));
                rincg.insIntero("num", cincg.getInt(3));
                rincg.insDouble("acconto", cincg.getDouble(4));
                rincg.insIntero("tipocassa", cincg.getInt(5));
                vincg.add(rincg);
            }
            cincg.close();

            // incassi da sospesi
            double totalesospeso = cfg.getDouble(22);
            double totincsospesicontanti = cfg.getDouble(32);
            double totincsospesinoncontanti = cfg.getDouble(33);
            ArrayList<Record> vincs = new ArrayList();
            Cursor cincs = Env.db.rawQuery(
                    "SELECT fg4clicod,fg4clinome,fg4datadoc,fg4num,fg4saldo FROM storico_fineg_incassisospesi WHERE fg4data=? AND fg4ora=? ORDER BY fg4datadoc,fg4num", parsfg);
            while (cincs.moveToNext()) {
                double val = cincs.getDouble(4);
                Record rincs = new Record();
                rincs.insStringa("clicod", cincs.getString(0));
                rincs.insStringa("clinome", cincs.getString(1));
                rincs.insStringa("data", cincs.getString(2));
                rincs.insIntero("num", cincs.getInt(3));
                rincs.insDouble("saldo", val);
                vincs.add(rincs);
            }
            cincs.close();

            // incassi da fatture ncred insoluti
            double totalefatture = cfg.getDouble(23);
            double totincfatturecontanti = cfg.getDouble(34);
            double totincfatturenoncontanti = cfg.getDouble(35);
            ArrayList<Record> vincf = new ArrayList();
            Cursor cincf = Env.db.rawQuery(
                    "SELECT fg5tipo,fg5clicod,fg5descr,fg5datadoc,fg5num,fg5saldo FROM storico_fineg_incassiftnc WHERE fg5data=? AND fg5ora=? ORDER BY fg5datadoc,fg5num", parsfg);
            while (cincf.moveToNext()) {
                Record rincf = new Record();
                rincf.insStringa("tipo", cincf.getString(0));
                rincf.insStringa("clicod", cincf.getString(1));
                rincf.insStringa("descr", cincf.getString(2));
                rincf.insStringa("data", cincf.getString(3));
                rincf.insIntero("num", cincf.getInt(4));
                rincf.insDouble("saldo", cincf.getDouble(5));
                vincf.add(rincf);
            }
            cincf.close();

            // sospesi da incassare
            double totalesosp = cfg.getDouble(24);
            ArrayList<Record> vsosp = new ArrayList();
            Cursor csosp = Env.db.rawQuery(
                    "SELECT fg6clicod,fg6clinome,fg6datadoc,fg6num,fg6sospeso FROM storico_fineg_sospesidainc WHERE fg6data=? AND fg6ora=? ORDER BY fg6datadoc,fg6num", parsfg);
            while (csosp.moveToNext()) {
                Record rsosp = new Record();
                rsosp.insStringa("clicod", csosp.getString(0));
                rsosp.insStringa("clinome", csosp.getString(1));
                rsosp.insStringa("data", csosp.getString(2));
                rsosp.insIntero("num", csosp.getInt(3));
                rsosp.insDouble("sospeso", csosp.getDouble(4));
                vsosp.add(rsosp);
            }
            csosp.close();

            // totale vendite
            double totvend = cfg.getDouble(25);

            // totale costi
            double totcosti = cfg.getDouble(26);

            // totale costi omaggi
            double totcostiom = cfg.getDouble(27);

            // totale costi sconti merce
            double totcostism = cfg.getDouble(28);

            // % vendite su costi
            double percvendcosti = cfg.getDouble(29);

            int nrigheint = 6;
            if (Env.stampasedesec)
                nrigheint += 2;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            if (finegiornata)
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("FINE GIORNATA", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "FINE GIORNATA", lf, 24);
            else
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("RIEPILOGO TOTALE", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "RIEPILOGO TOTALE", lf, 24);
            posy += 30;
            String stmp = "Data:" + (new Data(fgdata, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " ora:" + fgora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (Env.stampasedesec) {
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                String lp = funzStringa.tagliaStringa("Luogo di partenza:" + cdep.getString(2) + " " + cdep.getString(0) + " " + cdep.getString(1), gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, lp, lf, 20);
                posy += 30;
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printForm();

            Env.cpclPrinter.setForm(0, 200, 200, 7 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            // ddt emessi
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.DDT EMESSI      : " + numddt + " (dal " + minnum + " al " + maxnum + ")", lf, 20);
            posy += 30;
            //posy += 30;
            // ddt di reso
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.DDT RESO EMESSI : " + numddtreso + " (dal " + minnumreso + " al " + maxnumreso + ")", lf, 20);
            posy += 30;
            //posy += 30;
            // ddt qta
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.DDT QUANTITA'   : " + numddtqta + " (dal " + minddtqta + " al " + maxddtqta + ")", lf, 20);
            posy += 30;
            //posy += 30;
            // fatture
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.FATTURE         : " + numfatt + " (dal " + minfatt + " al " + maxfatt + ")", lf, 20);
            posy += 30;
            //posy += 30;
            // corrispettivi
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.CORRISPETTIVI   : " + numcorr + " (dal " + mincorr + " al " + maxcorr + ")", lf, 20);
            posy += 30;
            //posy += 30;
            // ddt annullati
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.DDT ANNULLATI   : " + numddtann, lf, 20);
            posy += 30;
            Env.cpclPrinter.printForm();

            try {
                Thread.currentThread().sleep(5000);
            } catch (Exception ex) {
            }
//			while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//			{
//				Log.v("JAZZTV", "BUSY");
//				try
//				{
//					Thread.currentThread().sleep(Env.stampe_par2);
//				} catch (Exception ex)
//				{
//				}
//			}

            if (numddtann > 0) {
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "ELENCO DDT ANNULLATI:", lf, 20);
                posy += 30;
                Env.cpclPrinter.printForm();
                for (int i = 0; i < vann.size(); i++) {
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    posy = 0;
                    Record rann = vann.get(i);
                    stmp = funzStringa.tagliaStringa(" - n." + rann.leggiIntero("num") + " " + (new Data(rann.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"), gf20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                    posy += 30;
                    Env.cpclPrinter.printForm();
                }
                posy += 30;
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, " ", lf, 20);
                Env.cpclPrinter.printForm();
            }
            posy += 30;
//			Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
//			Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
//			Env.cpclPrinter.setTone(20);
//			Env.cpclPrinter.printForm();

            posy = 0;
            Env.cpclPrinter.setForm(0, 200, 200, 4 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            // ddt con omaggi
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "N.DDT CON OMAGGI  : " + numddtom, lf, 20);
            posy += 30;
            // ddt carico e integrazione
            stmp = "N.DDT INTEGR.: " + funzStringa.riempiStringa("" + numcarint, 4, funzStringa.SX, ' ') +
                    " N.SCAR.A SEDE: " + funzStringa.riempiStringa("" + numscasede, 4, funzStringa.SX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            // trasf.altro mezzo
            stmp = "N.TR.VS AUTOM: " + funzStringa.riempiStringa("" + numtrasfamezzo, 4, funzStringa.SX, ' ') +
                    " N.CAR.DA AUTO: " + funzStringa.riempiStringa("" + numtrasfdamezzo, 4, funzStringa.SX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printForm();

            // incassi da vendite del giorno
            posy = 0;
            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("INCASSI DA VENDITE DEL GIORNO", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "INCASSI DA VENDITE DEL GIORNO", lf, 24);
            posy += 30; // 35
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                 DATA     N.DOCUM.      EURO ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                                DATA     N.DOCUM.      EURO ", lf, 18);
            posy += 30;
            Env.cpclPrinter.printForm();
            for (int i = 0; i < vincg.size(); i++) {
                posy = 0;
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Record rincg = vincg.get(i);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    stmp = funzStringa.tagliaStringa(rincg.leggiStringa("clicod") + "-" + rincg.leggiStringa("clinome"), 23);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = (new Data(rincg.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    Env.cpclPrinter.printAndroidFont(271, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa("" + rincg.leggiIntero("num"), 8);
                    Env.cpclPrinter.printAndroidFont(370, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(rincg.leggiDouble("acconto"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(478, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                } else {
                    stmp = funzStringa.tagliaStringa(rincg.leggiStringa("clicod") + "-" + rincg.leggiStringa("clinome"), 38);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = (new Data(rincg.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    Env.cpclPrinter.printAndroidFont(434, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.tagliaStringa("" + rincg.leggiIntero("num"), 8);
                    Env.cpclPrinter.printAndroidFont(532, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(rincg.leggiDouble("acconto"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(630, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                }
                posy += 30;
                Env.cpclPrinter.printForm();
                if (i % 10 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//					while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//					{
//						Log.v("JAZZTV", "BUSY");
//						try
//						{
//							Thread.currentThread().sleep(Env.stampe_par2);
//						} catch (Exception ex)
//						{
//						}
//					}
                }
            }
            posy += 30;
            posy = 0;
            if (Env.incsceglicassa) {
                Env.cpclPrinter.setForm(0, 200, 200, 5 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "INCASSI DEL GIORNO CONTANTI: " + Formattazione.formatta(totincgiornocontanti, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "INCASSI DEL GIORNO CON POS : " + Formattazione.formatta(totincgiornononcontanti, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE INCASSI DEL GIORNO  : " + Formattazione.formatta(totaleincassato, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                posy += 30;
                Env.cpclPrinter.printForm();
            } else {
                Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE INCASSI DEL GIORNO: " + Formattazione.formatta(totaleincassato, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                posy += 30;
                Env.cpclPrinter.printForm();
            }

            // incassi da sospesi
            if (vincs.size() > 0) {
                posy = 0;
                Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("INCASSI DA SOSPESI", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "INCASSI DA SOSPESI", lf, 24);
                posy += 30;
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                 DATA     N.DOCUM.      EURO ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                                DATA     N.DOCUM.      EURO ", lf, 18);
                posy += 30;
                Env.cpclPrinter.printForm();
                for (int i = 0; i < vincs.size(); i++) {
                    posy = 0;
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    Record rincs = vincs.get(i);
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        stmp = funzStringa.tagliaStringa(rincs.leggiStringa("clicod") + "-" + rincs.leggiStringa("clinome"), 23);
                        Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = (new Data(rincs.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                        Env.cpclPrinter.printAndroidFont(271, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.tagliaStringa("" + rincs.leggiIntero("num"), 8);
                        Env.cpclPrinter.printAndroidFont(370, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.riempiStringa(Formattazione.formatta(rincs.leggiDouble("saldo"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(478, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    } else {
                        stmp = funzStringa.tagliaStringa(rincs.leggiStringa("clicod") + "-" + rincs.leggiStringa("clinome"), 38);
                        Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = (new Data(rincs.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                        Env.cpclPrinter.printAndroidFont(434, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.tagliaStringa("" + rincs.leggiIntero("num"), 8);
                        Env.cpclPrinter.printAndroidFont(532, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.riempiStringa(Formattazione.formatta(rincs.leggiDouble("saldo"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(630, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    }
                    posy += 30;
                    Env.cpclPrinter.printForm();
                    if (i % 10 == 0) {
                        try {
                            Thread.currentThread().sleep(5000);
                        } catch (Exception ex) {
                        }
//						while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//						{
//							Log.v("JAZZTV", "BUSY");
//							try
//							{
//								Thread.currentThread().sleep(Env.stampe_par2);
//							} catch (Exception ex)
//							{
//							}
//						}
                    }
                }
                posy += 30;
                posy = 0;
                if (Env.incsceglicassa) {
                    Env.cpclPrinter.setForm(0, 200, 200, 5 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "INCASSI DA SOSPESI CONTANTI: " + Formattazione.formatta(totincsospesicontanti, "####0.00", Formattazione.SEGNO_DX), lf, 24);
                    posy += 30;
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "INCASSI DA SOSPESI CON POS : " + Formattazione.formatta(totincsospesinoncontanti, "####0.00", Formattazione.SEGNO_DX), lf, 24);
                    posy += 30;
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE INCASSI DA SOSPESI  : " + Formattazione.formatta(totalesospeso, "####0.00", Formattazione.SEGNO_DX), lf, 24);
                    posy += 30;
                    posy += 30;
                    Env.cpclPrinter.printForm();
                } else {
                    Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE INCASSI DA SOSPESI: " + Formattazione.formatta(totalesospeso, "####0.00", Formattazione.SEGNO_DX), lf, 24);
                    posy += 30;
                    posy += 30;
                    Env.cpclPrinter.printForm();
                }
            }

            try {
                Thread.currentThread().sleep(5000);
            } catch (Exception ex) {
            }
//			while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//			{
//				Log.v("JAZZTV", "BUSY");
//				try
//				{
//					Thread.currentThread().sleep(Env.stampe_par2);
//				} catch (Exception ex)
//				{
//				}
//			}

            // incassi da fatture nc insoluti
            posy = 0;
            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("INCASSI DA FATTURE/NOTE CRED/INSOLUTI", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "INCASSI DA FATTURE/NOTE CRED/INSOLUTI", lf, 24);
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                 DATA     N.DOCUM.      EURO ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                                DATA     N.DOCUM.      EURO ", lf, 18);
            posy += 30;
            Env.cpclPrinter.printForm();
            for (int i = 0; i < vincf.size(); i++) {
                posy = 0;
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Record rincf = vincf.get(i);
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    stmp = funzStringa.tagliaStringa(rincf.leggiStringa("clicod") + "-" + rincf.leggiStringa("descr"), 23);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    if (!rincf.leggiStringa("data").equals(""))
                        stmp = (new Data(rincf.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(271, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    if (rincf.leggiIntero("num") > 0)
                        stmp = funzStringa.tagliaStringa("" + rincf.leggiIntero("num"), 8);
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(370, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(rincf.leggiDouble("saldo"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(478, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                } else {
                    stmp = funzStringa.tagliaStringa(rincf.leggiStringa("clicod") + "-" + rincf.leggiStringa("descr"), 38);
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    if (!rincf.leggiStringa("data").equals(""))
                        stmp = (new Data(rincf.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(434, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    if (rincf.leggiIntero("num") > 0)
                        stmp = funzStringa.tagliaStringa("" + rincf.leggiIntero("num"), 8);
                    else
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(532, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formatta(rincf.leggiDouble("saldo"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(630, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                }
                posy += 30;
                Env.cpclPrinter.printForm();
                if (i % 10 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//					while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//					{
//						Log.v("JAZZTV", "BUSY");
//						try
//						{
//							Thread.currentThread().sleep(Env.stampe_par2);
//						} catch (Exception ex)
//						{
//						}
//					}
                }
            }
            posy += 30;
            posy = 0;
            if (Env.incsceglicassa) {
                Env.cpclPrinter.setForm(0, 200, 200, 5 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "FATT./NC/INSOLUTI CONTANTI: " + Formattazione.formatta(totincfatturecontanti, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "FATT./NC/INSOLUTI CON POS : " + Formattazione.formatta(totincfatturenoncontanti, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE FATT./NC/INSOLUTI  : " + Formattazione.formatta(totalefatture, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                posy += 30;
                Env.cpclPrinter.printForm();
            } else {
                Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE FATT./NC/INSOLUTI: " + Formattazione.formatta(totalefatture, "####0.00", Formattazione.SEGNO_DX), lf, 20);
                posy += 30;
                posy += 30;
                Env.cpclPrinter.printForm();
            }

            Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            double totinc = OpValute.arrotondaMat(totaleincassato + totalesospeso + totalefatture, 2);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE GENERALE INCASSI: " + Formattazione.formatta(totinc, "####0.00", Formattazione.SEGNO_DX), lf, 24);
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printForm();

            if (Env.finegstampasospdainc && vsosp.size() > 0) {
                // sospesi da incassare
                posy = 0;
                Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.printBox(0, posy, lf, posy + 59, 1);
                Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("RIEP.SOSPESI DA INCASSARE AL " + (new Data()).formatta(Data.GG_MM_AA, "/"), 24, lf), posy, Typeface.MONOSPACE, true, false, false, "RIEP.SOSPESI DA INCASSARE AL " + (new Data()).formatta(Data.GG_MM_AA, "/"), lf, 24);
                posy += 30;
                if (Env.tipostampante == 0 || Env.tipostampante == 2)
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                 DATA     N.DOCUM.      EURO ", lf, 18);
                else
                    Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                                DATA     N.DOCUM.      EURO ", lf, 18);
                posy += 30;
                Env.cpclPrinter.printForm();
                for (int i = 0; i < vsosp.size(); i++) {
                    posy = 0;
                    Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                    Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                    Env.cpclPrinter.setTone(20);
                    Record rsosp = vsosp.get(i);
                    if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                        stmp = funzStringa.tagliaStringa(rsosp.leggiStringa("clicod") + "-" + rsosp.leggiStringa("clinome"), 23);
                        Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = (new Data(rsosp.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                        Env.cpclPrinter.printAndroidFont(271, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.tagliaStringa("" + rsosp.leggiIntero("num"), 8);
                        Env.cpclPrinter.printAndroidFont(370, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.riempiStringa(Formattazione.formatta(rsosp.leggiDouble("sospeso"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(478, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    } else {
                        stmp = funzStringa.tagliaStringa(rsosp.leggiStringa("clicod") + "-" + rsosp.leggiStringa("clinome"), 38);
                        Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = (new Data(rsosp.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                        Env.cpclPrinter.printAndroidFont(434, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.tagliaStringa("" + rsosp.leggiIntero("num"), 8);
                        Env.cpclPrinter.printAndroidFont(532, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                        stmp = funzStringa.riempiStringa(Formattazione.formatta(rsosp.leggiDouble("sospeso"), "####0.00", Formattazione.SEGNO_DX), 8, funzStringa.DX, ' ');
                        Env.cpclPrinter.printAndroidFont(630, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    }
                    posy += 30;
                    Env.cpclPrinter.printForm();
                    if (i % 10 == 0) {
                        try {
                            Thread.currentThread().sleep(5000);
                        } catch (Exception ex) {
                        }
//						while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//						{
//							Log.v("JAZZTV", "BUSY");
//							try
//							{
//								Thread.currentThread().sleep(Env.stampe_par2);
//							} catch (Exception ex)
//							{
//							}
//						}
                    }
                }
                posy += 30;
                posy = 0;
                Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE SOSPESI: " + Formattazione.formatta(totalesosp, "####0.00", Formattazione.SEGNO_DX), lf, 24);
                posy += 30;
                posy += 30;
                Env.cpclPrinter.printForm();

            }

            posy = 0;
            Env.cpclPrinter.setForm(0, 200, 200, (5) * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE VENDITE           : " + funzStringa.riempiStringa(Formattazione.formatta(totvend, "#######0.00", Formattazione.SEGNO_SX_NEG), 15, funzStringa.DX, ' '), lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE COSTI             : " + funzStringa.riempiStringa(Formattazione.formatta(totcosti, "#######0.00", Formattazione.SEGNO_SX_NEG), 15, funzStringa.DX, ' '), lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE COSTI OMAGGI      : " + funzStringa.riempiStringa(Formattazione.formatta(totcostiom, "#######0.00", Formattazione.SEGNO_SX_NEG), 15, funzStringa.DX, ' '), lf, 20);
            posy += 30;
//			Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE COSTI SCONTI MERCE: " + funzStringa.riempiStringa(Formattazione.formatta(totcostism, "#######0.00", Formattazione.SEGNO_SX_NEG), 15, funzStringa.DX, ' '), lf, 24);
//			posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "TOTALE VENDITE/COSTI %   : " + funzStringa.riempiStringa(Formattazione.formatta(percvendcosti, "#######0.00", Formattazione.SEGNO_SX_NEG), 15, funzStringa.DX, ' '), lf, 20);
            posy += 30;
            posy += 30;
            Env.cpclPrinter.printForm();
            cfg.close();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }
    }

    public static void stampaCostiVendite(Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            ArrayList<Record> vcv = calcolaCostiVenditeSessione();

            Env.cpclPrinter.setForm(0, 200, 200, 7 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);

            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("RIEPILOGO COSTI/VENDITE", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "RIEPILOGO COSTI/VENDITE", lf, 24);
            posy += 30;

            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            String stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " DATA DOC N.DOC. CLIENTE         COSTI    VENDITE   KG VEND.  % RIC ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            double totcosti = 0, totvendite = 0, totkg = 0;
            for (int i = 0; i < vcv.size(); i++) {
                Record rcv = vcv.get(i);
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {

                } else {
                    stmp = (new Data(rcv.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = "" + rcv.leggiIntero("num") + " ";
                    Env.cpclPrinter.printAndroidFont(108, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = rcv.leggiStringa("clicod");
                    if (stmp.equals(""))
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(184, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formValuta(rcv.leggiDouble("totcosti"), 12, 2, Formattazione.SEGNO_SX_NEG), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(304, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formValuta(rcv.leggiDouble("totvend"), 12, 2, Formattazione.SEGNO_SX_NEG), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(423, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formValuta(rcv.leggiDouble("totkg"), 12, 3, Formattazione.SEGNO_SX_NEG), 9, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(543, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formValuta(rcv.leggiDouble("percric"), 4, 2, Formattazione.SEGNO_SX_NEG), 7, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(652, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    posy += 30;
                }
                Env.cpclPrinter.printForm();
                totcosti += rcv.leggiDouble("totcosti");
                totvendite += rcv.leggiDouble("totvend");
                totkg += rcv.leggiDouble("totkg");
                if (i % 5 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//					while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal"))
//					{
//						Log.v("JAZZTV", "BUSY");
//						try
//						{
//							Thread.currentThread().sleep(Env.stampe_par2);
//						} catch (Exception ex)
//						{
//						}
//					}
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 4 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printLine(0, posy + 15, 576, posy + 15, 1);
            else
                Env.cpclPrinter.printLine(0, posy + 15, 740, posy + 15, 1);
            posy += 30;
            stmp = "Tot.costi:" + Formattazione.formValuta(totcosti, 12, 2, Formattazione.SEGNO_SX_NEG) + " Tot.vendite:" +
                    Formattazione.formValuta(totvendite, 12, 2, Formattazione.SEGNO_SX_NEG) + " Tot.Kg:" +
                    Formattazione.formValuta(totkg, 12, 3, Formattazione.SEGNO_SX_NEG);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
            posy += 30;
            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }
    }

    public static void stampaCostiVenditeStor(Context context, String fgdata, String fgora) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            ArrayList<Record> vcv = new ArrayList();
            String[] parsfg = new String[2];
            parsfg[0] = fgdata;
            parsfg[1] = fgora;
            Cursor c = Env.db.rawQuery(
                    "SELECT fg7num,fg7datadoc,fg7clicod,fg7totcosti,fg7totvend,fg7totkg,fg7percric FROM storico_fineg_costivendite WHERE fg7data=? AND fg7ora=? ORDER BY fg7num,fg7datadoc", parsfg);
            while (c.moveToNext()) {
                Record r = new Record();
                r.insIntero("num", c.getInt(0));
                r.insStringa("data", c.getString(1));
                r.insStringa("clicod", c.getString(2));
                r.insDouble("totcosti", c.getDouble(3));
                r.insDouble("totvend", c.getDouble(4));
                r.insDouble("totkg", c.getDouble(5));
                r.insDouble("percric", c.getDouble(6));
                vcv.add(r);
            }
            c.close();

            Env.cpclPrinter.setForm(0, 200, 200, 7 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("RIEPILOGO COSTI/VENDITE", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "RIEPILOGO COSTI/VENDITE", lf, 24);
            posy += 30;

            String stmp = "Data:" + (new Data(fgdata, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " ora:" + fgora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " DATA DOC N.DOC. CLIENTE         COSTI    VENDITE   KG VEND.  % RIC ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();
            double totcosti = 0, totvendite = 0, totkg = 0;
            for (int i = 0; i < vcv.size(); i++) {
                Record rcv = vcv.get(i);
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {

                } else {
                    stmp = (new Data(rcv.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    Env.cpclPrinter.printAndroidFont(10, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = "" + rcv.leggiIntero("num") + " ";
                    Env.cpclPrinter.printAndroidFont(108, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = rcv.leggiStringa("clicod");
                    if (stmp.equals(""))
                        stmp = " ";
                    Env.cpclPrinter.printAndroidFont(184, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formValuta(rcv.leggiDouble("totcosti"), 12, 2, Formattazione.SEGNO_SX_NEG), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(304, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formValuta(rcv.leggiDouble("totvend"), 12, 2, Formattazione.SEGNO_SX_NEG), 10, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(423, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formValuta(rcv.leggiDouble("totkg"), 12, 3, Formattazione.SEGNO_SX_NEG), 9, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(543, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    stmp = funzStringa.riempiStringa(Formattazione.formValuta(rcv.leggiDouble("percric"), 4, 2, Formattazione.SEGNO_SX_NEG), 7, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(652, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
                    posy += 30;
                }
                Env.cpclPrinter.printForm();
                totcosti += rcv.leggiDouble("totcosti");
                totvendite += rcv.leggiDouble("totvend");
                totkg += rcv.leggiDouble("totkg");
            }
            Env.cpclPrinter.setForm(0, 200, 200, 4 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printLine(0, posy + 15, 576, posy + 15, 1);
            else
                Env.cpclPrinter.printLine(0, posy + 15, 740, posy + 15, 1);
            posy += 30;
            stmp = "Tot.costi:" + Formattazione.formValuta(totcosti, 12, 2, Formattazione.SEGNO_SX_NEG) + " Tot.vendite:" +
                    Formattazione.formValuta(totvendite, 12, 2, Formattazione.SEGNO_SX_NEG) + " Tot.Kg:" +
                    Formattazione.formValuta(totkg, 12, 3, Formattazione.SEGNO_SX_NEG);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 18);
            posy += 30;
            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }
    }

    public static void elaboraFineGiornata(String fgdata, String fgora) {
        try {
            // calcolo ddt emessi
            int numddt = 0;
            Cursor c = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 0 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                numddt = c.getInt(0);
            }
            c.close();
            int minnum = 0;
            c = Env.db.rawQuery(
                    "SELECT MIN(movnum) FROM movimenti WHERE movtipo = 0 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                minnum = c.getInt(0);
            }
            c.close();
            int maxnum = 0;
            c = Env.db.rawQuery(
                    "SELECT MAX(movnum) FROM movimenti WHERE movtipo = 0 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                maxnum = c.getInt(0);
            }
            c.close();
            // calcolo ddt di reso
            int numddtreso = 0;
            c = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 4 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                numddtreso = c.getInt(0);
            }
            c.close();
            int minnumreso = 0;
            c = Env.db.rawQuery(
                    "SELECT MIN(movnum) FROM movimenti WHERE movtipo = 4 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                minnumreso = c.getInt(0);
            }
            c.close();
            int maxnumreso = 0;
            c = Env.db.rawQuery(
                    "SELECT MAX(movnum) FROM movimenti WHERE movtipo = 4 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                maxnumreso = c.getInt(0);
            }
            c.close();
            // calcolo ddt qta
            int numddtqta = 0;
            c = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 1 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                numddtqta = c.getInt(0);
            }
            c.close();
            int minddtqta = 0;
            c = Env.db.rawQuery(
                    "SELECT MIN(movnum) FROM movimenti WHERE movtipo = 1 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                minddtqta = c.getInt(0);
            }
            c.close();
            int maxddtqta = 0;
            c = Env.db.rawQuery(
                    "SELECT MAX(movnum) FROM movimenti WHERE movtipo = 1 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                maxddtqta = c.getInt(0);
            }
            c.close();

            // calcolo fatture
            int numfatt = 0;
            c = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 2 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                numfatt = c.getInt(0);
            }
            c.close();
            int minfatt = 0;
            c = Env.db.rawQuery(
                    "SELECT MIN(movnum) FROM movimenti WHERE movtipo = 2 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                minfatt = c.getInt(0);
            }
            c.close();
            int maxfatt = 0;
            c = Env.db.rawQuery(
                    "SELECT MAX(movnum) FROM movimenti WHERE movtipo = 2 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                maxfatt = c.getInt(0);
            }
            c.close();

            // calcolo corrispettivi
            int numcorr = 0;
            c = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 3 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                numcorr = c.getInt(0);
            }
            c.close();
            int mincorr = 0;
            c = Env.db.rawQuery(
                    "SELECT MIN(movnum) FROM movimenti WHERE movtipo = 3 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                mincorr = c.getInt(0);
            }
            c.close();
            int maxcorr = 0;
            c = Env.db.rawQuery(
                    "SELECT MAX(movnum) FROM movimenti WHERE movtipo = 3 AND movtrasf<>'S' AND movsospeso = 'N'", null);
            if (c.moveToFirst() && !c.isNull(0)) {
                maxcorr = c.getInt(0);
            }
            c.close();

            // calcolo ddt annullati
            int numddtann = 0;
            ArrayList<Record> vann = new ArrayList();
            SQLiteStatement st = Env.db.compileStatement("INSERT INTO storico_fineg_ddtann (fg1data,fg1ora,fg1tipo,fg1num,fg1datadoc) VALUES (?,?,?,?,?)");
            c = Env.db.rawQuery(
                    "SELECT movtipo,movnum,movdata,movkey FROM movimenti WHERE movtrasf<>'S' AND movsospeso = 'S' ORDER BY movtipo,movnum,movdata", null);
            while (c.moveToNext()) {
                numddtann++;
                st.clearBindings();
                st.bindString(1, fgdata);
                st.bindString(2, fgora);
                st.bindLong(3, c.getInt(0));
                st.bindLong(4, c.getInt(1));
                st.bindString(5, c.getString(2));
                st.execute();
            }
            c.close();
            st.close();

            // calcolo DDT con omaggi
            int numddtom = 0;
            ArrayList vomdt = new ArrayList();
            ArrayList vomnum = new ArrayList();
            ArrayList vomart = new ArrayList();
            ArrayList vomdart = new ArrayList();
            ArrayList vomqta = new ArrayList();
            ArrayList vomcau = new ArrayList();
            st = Env.db.compileStatement("INSERT INTO storico_fineg_ddtom (fg2data,fg2ora,fg2datadoc,fg2num,fg2artcod,fg2artdescr,fg2qta,fg2caumag) VALUES (?,?,?,?,?,?,?,?)");
            Cursor coma = Env.db.rawQuery(
                    "SELECT movdata,movnum,rmartcod,rmqta,rmcaumag,artdescr FROM righemov INNER JOIN articoli ON righemov.rmartcod = articoli.artcod LEFT JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum) WHERE (rmcaumag = 'OT' OR rmcaumag = 'SM' OR rmcaumag = 'OM') AND movtrasf<>'S' AND movsospeso = 'N'", null);
            while (coma.moveToNext()) {
                st.clearBindings();
                st.bindString(1, fgdata);
                st.bindString(2, fgora);
                st.bindString(3, coma.getString(0));
                st.bindLong(4, coma.getInt(1));
                st.bindString(5, coma.getString(2));
                st.bindString(6, coma.getString(5));
                st.bindDouble(7, coma.getDouble(3));
                st.bindString(8, coma.getString(4));
                st.execute();
            }
            coma.close();
            st.close();
            HashMap<Integer, String> hddtom = new HashMap();
            for (int i = 0; i < vomnum.size(); i++) {
                if (!hddtom.containsKey((int) vomnum.get(i)))
                    hddtom.put((int) vomnum.get(i), "");
            }
            numddtom = hddtom.size();

            // calcolo DDT carico e integrazione
            int numcarint = 0;
            Cursor cci = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 5 AND movtrasf<>'S'", null);
            if (cci.moveToFirst() && !cci.isNull(0))
                numcarint = cci.getInt(0);
            cci.close();
            // calcolo scarichi a sede
            int numscasede = 0;
            Cursor css = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 6 AND movtrasf<>'S'", null);
            if (css.moveToFirst() && !css.isNull(0))
                numscasede = css.getInt(0);
            css.close();
            // calcolo trasf.a altro mezzo
            int numtrasfamezzo = 0;
            Cursor ctv = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 7 AND movtrasf<>'S'", null);
            if (ctv.moveToFirst() && !ctv.isNull(0))
                numtrasfamezzo = ctv.getInt(0);
            ctv.close();
            // calcolo trasf.da altro mezzo
            int numtrasfdamezzo = 0;
            Cursor ctd = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti WHERE movtipo = 8 AND movtrasf<>'S'", null);
            if (ctd.moveToFirst() && !ctd.isNull(0))
                numtrasfdamezzo = ctd.getInt(0);
            ctd.close();

            // incassi da vendite del giorno
            double totaleincassato = 0;
            double totincgiornocontanti = 0;
            double totincgiornononcontanti = 0;
            ArrayList<Record> vincg = new ArrayList();
            st = Env.db.compileStatement("INSERT INTO storico_fineg_incassigiorno (fg3data,fg3ora,fg3clicod,fg3clinome,fg3datadoc,fg3num,fg2acconto,fg3tipocassa) VALUES (?,?,?,?,?,?,?,?)");
            Cursor cincg = Env.db.rawQuery(
                    "SELECT movclicod, movdata, movnum, movacconto, clinome,movaccontotipocassa FROM movimenti LEFT JOIN clienti ON (movimenti.movclicod = clienti.clicodice) WHERE movtipo IN (0,2) AND movtrasf='N' AND movsospeso = 'N' AND movacconto <> 0", null);
            while (cincg.moveToNext()) {
                st.clearBindings();
                st.bindString(1, fgdata);
                st.bindString(2, fgora);
                st.bindString(3, cincg.getString(0));
                st.bindString(4, cincg.getString(4));
                st.bindString(5, cincg.getString(1));
                st.bindLong(6, cincg.getInt(2));
                st.bindDouble(7, cincg.getDouble(3));
                st.bindLong(8, cincg.getInt(5));
                st.execute();
                totaleincassato += cincg.getDouble(3);
                if (cincg.getInt(5) == 0)
                    totincgiornocontanti += cincg.getDouble(3);
                else
                    totincgiornononcontanti += cincg.getDouble(3);
            }
            cincg.close();
            // fatture incassate nel giorno e non ancora trasmesse
            Cursor cfat = Env.db.rawQuery(
                    "SELECT movclicod, movdata, movnum, movacconto, clinome FROM movimenti LEFT JOIN clienti ON (movimenti.movclicod = clienti.clicodice) WHERE movtipo = 2 AND movtrasf='N' AND movsospeso = 'N' AND movacconto = 0", null);
            while (cfat.moveToNext()) {
                // cerca su incassi
                double saldofat = 0;
                double saldofatc = 0;
                double saldofatnc = 0;
                Cursor cincf = Env.db.rawQuery(
                        "SELECT isaldo,isctipocassa FROM incassi WHERE isctipo='F' AND iscsezdoc='" + Env.sezfattura + "' AND iscdatadoc = '" + cfat.getString(1) + "' AND iscnumdoc = " + cfat.getInt(2) + " AND isctrasf = 'N'", null);
                while (cincf.moveToNext()) {
                    saldofat += cincf.getDouble(0);
                    if (cincf.getInt(1) == 0)
                        saldofatc += cincf.getDouble(0);
                    else
                        saldofatnc += cincf.getDouble(0);
                }
                cincf.close();
                if (saldofat > 0) {
                    if (saldofatc > 0) {
                        st.clearBindings();
                        st.bindString(1, fgdata);
                        st.bindString(2, fgora);
                        st.bindString(3, cfat.getString(0));
                        st.bindString(4, cfat.getString(4));
                        st.bindString(5, cfat.getString(1));
                        st.bindLong(6, cfat.getInt(2));
                        st.bindDouble(7, saldofatc);
                        st.bindLong(8, 0);
                        st.execute();
                    }
                    if (saldofatnc > 0) {
                        st.clearBindings();
                        st.bindString(1, fgdata);
                        st.bindString(2, fgora);
                        st.bindString(3, cfat.getString(0));
                        st.bindString(4, cfat.getString(4));
                        st.bindString(5, cfat.getString(1));
                        st.bindLong(6, cfat.getInt(2));
                        st.bindDouble(7, saldofatnc);
                        st.bindLong(8, 1);
                        st.execute();
                    }
                    totaleincassato += saldofat;
                    totincgiornocontanti += saldofatc;
                    totincgiornononcontanti += saldofatc;
                }
            }
            cfat.close();
            st.close();

            // incassi da sospesi
            double totalesospeso = 0;
            double totincsospesicontanti = 0;
            double totincsospesinoncontanti = 0;
            ArrayList<Record> vincs = new ArrayList();
            st = Env.db.compileStatement("INSERT INTO storico_fineg_incassisospesi (fg4data,fg4ora,fg4clicod,fg4clinome,fg4datadoc,fg4num,fg4saldo,fg4tipocassa) VALUES (?,?,?,?,?,?,?,?)");
            Cursor cincs = Env.db.rawQuery(
                    "SELECT iscclicod,iscdatadoc,iscnumdoc,isaldo,isctipoop,clinome,isctipocassa FROM incassi LEFT JOIN clienti ON (incassi.iscclicod = clienti.clicodice) WHERE isctipo = 'B' ", null);
            while (cincs.moveToNext()) {
                double val = cincs.getDouble(3);
                if (!cincs.getString(4).equals("V"))
                    val = -val;
                st.clearBindings();
                st.bindString(1, fgdata);
                st.bindString(2, fgora);
                st.bindString(3, cincs.getString(0));
                st.bindString(4, cincs.getString(5));
                st.bindString(5, cincs.getString(1));
                st.bindLong(6, cincs.getInt(2));
                st.bindDouble(7, val);
                st.bindLong(8, cincs.getInt(6));
                st.execute();
                totalesospeso += val;
                if (cincs.getInt(6) == 0)
                    totincsospesicontanti += val;
                else
                    totincsospesinoncontanti += val;
            }
            cincs.close();
            st.close();

            // incassi da fatture ncred insoluti
            double totalefatture = 0;
            double totincfatturecontanti = 0;
            double totincfatturenoncontanti = 0;
            ArrayList<Record> vincf = new ArrayList();
            st = Env.db.compileStatement("INSERT INTO storico_fineg_incassiftnc (fg5data,fg5ora,fg5tipo,fg5clicod,fg5descr,fg5datadoc,fg5num,fg5saldo,fg5tipocassa) VALUES (?,?,?,?,?,?,?,?,?)");
            Cursor cincf = Env.db.rawQuery(
                    "SELECT iscclicod, iscdatadoc, iscnumdoc, isaldo, clinome,isctipo,iscdescr,isctipoop,iscsezdoc,isctipocassa FROM incassi LEFT JOIN clienti ON (incassi.iscclicod = clienti.clicodice) WHERE isctipo = 'F' OR isctipo = 'N' OR isctipo = 'C' ORDER BY isctipo", null);
            while (cincf.moveToNext()) {
                // controlla se fattura del giorno
                boolean incok = true;
                //movtipo,movdoc,movsez,movdata,movnum
                Cursor cfg = Env.db.rawQuery("SELECT movtipo FROM movimenti WHERE movtipo = 2 AND movdoc = '" + Env.docfattura + "' AND movsez = '" + cincf.getString(8) + "' AND " +
                        "movdata = '" + cincf.getString(1) + "' AND movnum = " + cincf.getInt(2) + " AND movtrasf = 'N'", null);
                if (cfg.moveToFirst())
                    incok = false;
                cfg.close();
                if (incok) {
                    st.clearBindings();
                    st.bindString(1, fgdata);
                    st.bindString(2, fgora);
                    st.bindString(3, cincf.getString(5));
                    st.bindString(4, cincf.getString(0));
                    if (!cincf.getString(6).equals(""))
                        st.bindString(5, cincf.getString(6));
                    else
                        st.bindString(5, cincf.getString(4));
                    if (cincf.getString(1).equals("0000-00-00") || cincf.getString(1).equals("") || cincf.getString(1).length() != 10)
                        st.bindString(6, "");
                    else
                        st.bindString(6, cincf.getString(1));
                    st.bindLong(7, cincf.getInt(2));
                    if (cincf.getString(7).equals("V"))
                        st.bindDouble(8, cincf.getDouble(3));
                    else
                        st.bindDouble(8, -cincf.getDouble(3));
                    st.bindLong(9, cincf.getInt(9));
                    st.execute();
                    if (cincf.getString(7).equals("V")) {
                        totalefatture += cincf.getDouble(3);
                        if (cincf.getInt(9) == 0)
                            totincfatturecontanti += cincf.getDouble(3);
                        else
                            totincfatturenoncontanti += cincf.getDouble(3);
                    } else {
                        totalefatture -= cincf.getDouble(3);
                        if (cincf.getInt(9) == 0)
                            totincfatturecontanti -= cincf.getDouble(3);
                        else
                            totincfatturenoncontanti -= cincf.getDouble(3);
                    }
                }
            }
            cincf.close();
            st.close();

            // sospesi da incassare
            double totalesosp = 0;
            ArrayList<Record> vsosp = new ArrayList();
            st = Env.db.compileStatement("INSERT INTO storico_fineg_sospesidainc (fg6data,fg6ora,fg6clicod,fg6clinome,fg6datadoc,fg6num,fg6sospeso) VALUES (?,?,?,?,?,?,?)");
            Cursor csosp = Env.db.rawQuery(
                    "SELECT sctipo,scsezdoc,scdatadoc,scnumdoc,sctipoop,scnote,scclicod,scdestcod," +
                            "scdatascad,sctiposcad,scimportoscad,scresiduoscad,scscadid,clinome FROM scoperti LEFT JOIN clienti ON scoperti.scclicod = clienti.clicodice WHERE " +
                            "sctipo = 'B' AND scresiduoscad > 0 ORDER BY scdatadoc DESC,scnumdoc ASC", null);
            while (csosp.moveToNext()) {
                st.clearBindings();
                st.bindString(1, fgdata);
                st.bindString(2, fgora);
                st.bindString(3, csosp.getString(6));
                if (!csosp.isNull(13))
                    st.bindString(4, csosp.getString(13));
                else
                    st.bindString(4, "");
                st.bindString(5, csosp.getString(2));
                st.bindLong(6, csosp.getInt(3));
                double sospdoc = csosp.getDouble(11);
                if (!csosp.getString(4).equals("V"))
                    sospdoc = -sospdoc;
                st.bindDouble(7, sospdoc);
                st.execute();
                totalesosp += sospdoc;
            }
            csosp.close();
            st.close();

            // totale vendite
            double totvend = calcolaTotaleVenditeSessione();

            // totale costi
            double totcosti = calcolaTotaleCostiSessione();

            // totale costi omaggi
            double totcostiom = calcolaTotaleCostiOmaggiSessione();

            // totale costi sconti merce
            double totcostism = calcolaTotaleCostiScontiMerceSessione();

            // % vendite su costi
            double percvendcosti = 0;
            if (totcosti > 0 || totcostiom > 0 || totcostism > 0)
                percvendcosti = OpValute.arrotondaMat((totvend - (totcosti + totcostiom + totcostism)) / (totcosti + totcostiom + totcostism) * 100.0, 2);

            // costi vendite sessione
            st = Env.db.compileStatement("INSERT INTO storico_fineg_costivendite (fg7data,fg7ora,fg7num,fg7datadoc,fg7clicod,fg7totcosti,fg7totvend,fg7totkg,fg7percric) VALUES (?,?,?,?,?,?,?,?,?)");
            ArrayList<Record> vcv = calcolaCostiVenditeSessione();
            for (int i = 0; i < vcv.size(); i++) {
                Record rcv = vcv.get(i);
                st.clearBindings();
                st.bindString(1, fgdata);
                st.bindString(2, fgora);
                st.bindLong(3, rcv.leggiIntero("num"));
                st.bindString(4, rcv.leggiStringa("data"));
                st.bindString(5, rcv.leggiStringa("clicod"));
                st.bindDouble(6, rcv.leggiDouble("totcosti"));
                st.bindDouble(7, rcv.leggiDouble("totvend"));
                st.bindDouble(8, rcv.leggiDouble("totkg"));
                st.bindDouble(9, rcv.leggiDouble("percric"));
                st.execute();
            }
            st.close();

            // dati generali
            st = Env.db.compileStatement("INSERT INTO storico_fineg (fgdata,fgora,fgnumddt,fgminddt,fgmaxddt,fgnumddtreso,fgminddtreso,fgmaxddtreso," +
                    "fgnumddtqta,fgminddtqta,fgmaxddtqta,fgnumfatt,fgminfatt,fgmaxfatt,fgnumcorrisp,fgmincorrisp,fgmaxcorrisp," +
                    "fgnumddtann,fgnumddtom,fgnumcarint,fgnumscasede,fgnumtrasfamezzo,fgnumtrasfdamezzo,fgtotincassigiorno,fgtotincassisospesi,fgtotincassiftnc," +
                    "fgsospesidainc,fgtotvendite,fgtotcosti,fgtotcostiom,fgtotcostism,fgpercvendcosti,fgincassigiornocontanti,fgincassigiornononcontanti,fgincassisospesicontanti,fgincassisospesinoncontanti,fgincassiftnccontanti,fgincassiftncnoncontanti) " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            st.clearBindings();
            st.bindString(1, fgdata);
            st.bindString(2, fgora);
            st.bindLong(3, numddt);
            st.bindLong(4, minnum);
            st.bindLong(5, maxnum);
            st.bindLong(6, numddtreso);
            st.bindLong(7, minnumreso);
            st.bindLong(8, maxnumreso);
            st.bindLong(9, numddtqta);
            st.bindLong(10, minddtqta);
            st.bindLong(11, maxddtqta);
            st.bindLong(12, numfatt);
            st.bindLong(13, minfatt);
            st.bindLong(14, maxfatt);
            st.bindLong(15, numcorr);
            st.bindLong(16, mincorr);
            st.bindLong(17, maxcorr);
            st.bindLong(18, numddtann);
            st.bindLong(19, numddtom);
            st.bindLong(20, numcarint);
            st.bindLong(21, numscasede);
            st.bindLong(22, numtrasfamezzo);
            st.bindLong(23, numtrasfdamezzo);
            st.bindDouble(24, totaleincassato);
            st.bindDouble(25, totalesospeso);
            st.bindDouble(26, totalefatture);
            st.bindDouble(27, totalesosp);
            st.bindDouble(28, totvend);
            st.bindDouble(29, totcosti);
            st.bindDouble(30, totcostiom);
            st.bindDouble(31, totcostism);
            st.bindDouble(32, percvendcosti);
            st.bindDouble(33, totincgiornocontanti);
            st.bindDouble(34, totincgiornononcontanti);
            st.bindDouble(35, totincsospesicontanti);
            st.bindDouble(36, totincsospesinoncontanti);
            st.bindDouble(37, totincfatturecontanti);
            st.bindDouble(38, totincfatturenoncontanti);
            st.execute();
            st.close();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

    }


    public static void stampaXeSede(String filtrodata, int filtronum, boolean stor, Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            ArrayList<Record> vrighe = new ArrayList();
            String qry = "SELECT mcsez,mcdata,mcnum,mcartcod,mcqta,mclotto FROM movcaricosede" + (stor ? "stor" : "") + " WHERE mcqta > 0 ";
            if (!filtrodata.equals(""))
                qry += " AND mcdata = '" + filtrodata + "'";
            if (filtronum > 0)
                qry += " AND mcnum = " + filtronum;
            qry += " ORDER BY mcdata,mcsez,mcnum,mcartcod,mclotto";
            Cursor c = Env.db.rawQuery(qry, null);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("sez", c.getString(0));
                rx.insStringa("data", c.getString(1));
                rx.insIntero("num", c.getInt(2));
                rx.insStringa("artcod", c.getString(3));
                String descr = "";
                String[] parsa = new String[1];
                parsa[0] = c.getString(3);
                Cursor ca = Env.db.rawQuery("SELECT artdescr FROM articoli WHERE artcod = ?", parsa);
                if (ca.moveToFirst()) {
                    descr = ca.getString(0);
                }
                ca.close();
                rx.insStringa("descr", descr);
                rx.insDouble("qta", c.getDouble(4));
                rx.insStringa("lotto", c.getString(5));
                vrighe.add(rx);
            }
            c.close();

            // stampa intestazione
            int nrigheformint = 11;
            if (Env.stampasedesec)
                nrigheformint += 4;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheformint * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("CARICHI DA SEDE", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "CARICHI DA SEDE", lf, 24);
            posy += 30;
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " DATA     N.DOC. ARTICOLO                 QTA  LOTTO ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " DATA     N.DOC. ARTICOLO               QTA LOTTO    PREZZO  TOTALE ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();
            double totimp = 0;
            for (int i = 0; i < vrighe.size(); i++) {
                Record rx = vrighe.get(i);
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(30);
                Env.cpclPrinter.setSpeed(5);
                posy = 0;
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    String riga = " " + (new Data(rx.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    riga += " " + funzStringa.riempiStringa("" + rx.leggiIntero("num"), 6, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("descr"), 10, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 6, funzStringa.SX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    double prz = leggiPrezzoBaseArticolo(rx.leggiStringa("artcod"), rx.leggiStringa("lotto"), true);
                    double imp = OpValute.arrotondaMat(rx.leggiDouble("qta") * prz, 2);
                    posy += 30;
                    totimp += imp;
                } else {
                    String riga = " " + (new Data(rx.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    riga += " " + funzStringa.riempiStringa("" + rx.leggiIntero("num"), 6, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("descr"), 9, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.00", Formattazione.NO_SEGNO), 7, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("lotto"), 7, funzStringa.SX, ' ');
                    double prz = leggiPrezzoBaseArticolo(rx.leggiStringa("artcod"), rx.leggiStringa("lotto"), true);
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(prz, "##0.000", Formattazione.NO_SEGNO), 7, funzStringa.DX, ' ');
                    //System.out.println(rx.leggiStringa("artcod") + " " + leggiPrezzoBaseArticolo(rx.leggiStringa("artcod"), rx.leggiStringa("lotto"), true));
                    double imp = OpValute.arrotondaMat(rx.leggiDouble("qta") * prz, 2);
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(imp, "####0.00", Formattazione.NO_SEGNO), 7, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                    totimp += imp;
                }
                Env.cpclPrinter.printForm();
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 4 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            posy = 0;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "TOTALE IMPONIBILE:" + Formattazione.formValuta(totimp, 12, 2, 0), lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    public static void stampaOrdiniInevasiSede(Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            ArrayList<Record> vrighe = new ArrayList();
            Cursor c = Env.db.rawQuery(
                    "SELECT aisez,aidata,ainum,aiartcod,aiartdescr,aiartum,aiqta FROM ordiniinevasi ORDER BY aidata,aisez,ainum,aiartcod", null);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("sez", c.getString(0));
                rx.insStringa("data", c.getString(1));
                rx.insIntero("num", c.getInt(2));
                rx.insStringa("artcod", c.getString(3));
                rx.insStringa("artdescr", c.getString(4));
                rx.insStringa("artum", c.getString(5));
                rx.insDouble("qta", c.getDouble(6));
                vrighe.add(rx);
            }
            c.close();

            // stampa intestazione
            int nrigheformint = 11;
            if (Env.stampasedesec)
                nrigheformint += 4;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheformint * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("ORDINI INEVASI DALLA SEDE", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "ORDINI INEVASI DALLA SEDE", lf, 24);
            posy += 30;
            String stmp = funzStringa.tagliaStringa(Env.intazriga1 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga2 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga3 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga4 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa(Env.intazriga5 + " ", 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " DATA     N.DOC. ARTICOLO                        QTA ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " DATA     N.DOC. ARTICOLO                                      QTA ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            for (int i = 0; i < vrighe.size(); i++) {
                Record rx = vrighe.get(i);
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(30);
                Env.cpclPrinter.setSpeed(5);
                posy = 0;
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    String riga = "";
                    if (!rx.leggiStringa("data").trim().equals("") && rx.leggiStringa("data").trim().equals("0000-00-00"))
                        riga = " " + (new Data(rx.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        riga = "         ";
                    riga += " " + funzStringa.riempiStringa("" + rx.leggiIntero("num"), 6, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 17, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                } else {
                    String riga = "";
                    if (!rx.leggiStringa("data").trim().equals("") && rx.leggiStringa("data").trim().equals("0000-00-00"))
                        riga = " " + (new Data(rx.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                    else
                        riga = "         ";
                    riga += " " + funzStringa.riempiStringa("" + rx.leggiIntero("num"), 6, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 31, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                    Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                    posy += 30;
                }
                Env.cpclPrinter.printForm();
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 3 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    public static void stampaScontiMerce(Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            ArrayList<Record> vrighe = new ArrayList();
            Cursor c = Env.db.rawQuery(
                    "SELECT rmartcod,artdescr,artum,rmqta,clicodice,clinome FROM righemov " +
                            "LEFT JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum) " +
                            "LEFT JOIN clienti ON (movimenti.movclicod = clienti.clicodice) " +
                            " LEFT JOIN articoli ON righemov.rmartcod = articoli.artcod " +
                            "WHERE rmcaumag='SM' AND movimenti.movtrasf='N' AND movimenti.movsospeso='N' ORDER BY movclicod,rmartcod", null);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("clicod", c.getString(4));
                rx.insStringa("clinome", c.getString(5));
                rx.insStringa("artcod", c.getString(0));
                rx.insStringa("artdescr", c.getString(1));
                rx.insStringa("um", c.getString(2));
                rx.insDouble("qta", c.getDouble(3));
                vrighe.add(rx);
            }
            c.close();

            int nrigheform = 6;
            if (Env.stampasedesec)
                nrigheform += 4;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheform * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("SCONTI IN MERCE", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "SCONTI IN MERCE", lf, 24);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            String stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " CLIENTE                               COD.ART.  QTA ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            for (int i = 0; i < vrighe.size(); i++) {
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Record rx = vrighe.get(i);
                String riga = " " + funzStringa.riempiStringa(rx.leggiStringa("clicod") + "-" + rx.leggiStringa("clinome"), 32, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.000", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, 576, 18);
                posy += 30;
                Env.cpclPrinter.printForm();
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 2 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    public static void stampaDistintaDenaro(Context context, HashMap<String, Double> vdati, double totinc) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            Env.cpclPrinter.setForm(0, 200, 200, 19 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            Env.cpclPrinter.setSpeed(5);
            int posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            String riga = " Pezzi da 500:";
            riga += funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("n500"), 12, 0, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            riga += " -> euro" + funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("tot500"), 12, 2, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 20);
            posy += 30;
            riga = " Pezzi da 200:";
            riga += funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("n200"), 12, 0, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            riga += " -> euro" + funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("tot200"), 12, 2, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 20);
            posy += 30;
            riga = " Pezzi da 100:";
            riga += funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("n100"), 12, 0, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            riga += " -> euro" + funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("tot100"), 12, 2, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 20);
            posy += 30;
            riga = " Pezzi da 50 :";
            riga += funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("n50"), 12, 0, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            riga += " -> euro" + funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("tot50"), 12, 2, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 20);
            posy += 30;
            riga = " Pezzi da 20 :";
            riga += funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("n20"), 12, 0, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            riga += " -> euro" + funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("tot20"), 12, 2, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 20);
            posy += 30;
            riga = " Pezzi da 10 :";
            riga += funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("n10"), 12, 0, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            riga += " -> euro" + funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("tot10"), 12, 2, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 20);
            posy += 30;
            riga = " Pezzi da 5  :";
            riga += funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("n5"), 12, 0, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            riga += " -> euro" + funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("tot5"), 12, 2, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 20);
            posy += 30;
            riga = " Monete      :" + funzStringa.spazi(12);
            riga += " -> euro" + funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("totmonete"), 12, 2, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 20);
            posy += 30;
            riga = " Assegni     :" + funzStringa.spazi(12);
            riga += " -> euro" + funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("totassegni"), 12, 2, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            riga = "                Totale DICHIARATO:" +
                    funzStringa.riempiStringa(Formattazione.formValuta(vdati.get("totdich"), 12, 2, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 20);
            posy += 30;
            posy += 30;
            riga = "                   Totale INCASSI:" +
                    funzStringa.riempiStringa(Formattazione.formValuta(totinc, 12, 2, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 20);
            posy += 30;
            posy += 30;
            double diff = OpValute.arrotondaMat(vdati.get("totdich") - totinc, 2);
            riga = "                       DIFFERENZA:" +
                    funzStringa.riempiStringa(Formattazione.formValuta(diff, 12, 2, Formattazione.SEGNO_SX_NEG), 12, funzStringa.DX, ' ');
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, riga, lf, 20);

            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }

    public static void stampaDocOrdineSede(int num, String data, Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            String[] pars = new String[3];
            pars[0] = Env.docordinesede;
            pars[1] = data;
            pars[2] = "" + num;
            Cursor cm = Env.db.rawQuery("SELECT movnote2,movnote4,movnote6,movdatacons,movfor,movdocora FROM movimenti WHERE movdoc = ? AND movdata = ? AND movnum = ?", pars);
            cm.moveToFirst();
            String oradoc = cm.getString(5);

            String forcod = "", fornome = "";
            if (!cm.getString(4).equals("")) {
                String[] parsf = new String[1];
                parsf[0] = cm.getString(4);
                Cursor cf = Env.db.rawQuery("SELECT prcodice,prdescr FROM provenienze WHERE prcodice = ?", parsf);
                if (cf.moveToFirst()) {
                    forcod = cf.getString(0);
                    fornome = cf.getString(1);
                }
                cf.close();
            }

            int totcolli = 0;
            ArrayList<Record> vrighe = new ArrayList();
            Cursor c = Env.db.rawQuery("SELECT rmartcod,rmqta,rmcolli,rmprz FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? ORDER BY rmriga", pars);
            while (c.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("artcod", c.getString(0));
                rx.insDouble("qta", c.getDouble(1));
                rx.insIntero("colli", c.getInt(2));
                totcolli += c.getInt(2);
                String[] parsa = new String[1];
                parsa[0] = c.getString(0);
                Cursor ca = Env.db.rawQuery("SELECT artdescr,artum FROM articoli WHERE artcod = ?", parsa);
                if (ca.moveToFirst()) {
                    rx.insStringa("artdescr", ca.getString(0));
                    rx.insStringa("um", ca.getString(1));
                } else {
                    rx.insStringa("artdescr", "");
                    rx.insStringa("um", "");
                }
                ca.close();
                rx.insDouble("prezzo", c.getDouble(3));
                vrighe.add(rx);
            }
            c.close();
            String dtcons = "";
            if (!cm.getString(3).equals("") && !cm.getString(3).equals("0000-00-00")) {
                dtcons = cm.getString(3);
            }
            int nrigheint = 9;
            if (Env.stampasedesec)
                nrigheint += 4;
            if (!dtcons.equals(""))
                nrigheint += 2;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, lf, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("ORDINE PRODOTTI", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "ORDINE PRODOTTI", lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Ordine n." + num, lf, 20);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            String stmp = "Data:" + (new Data(data, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " ora:" + oradoc;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, gf20);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            if (!forcod.equals("")) {
                stmp = funzStringa.tagliaStringa("Fornitore: " + fornome, gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                posy += 30;
            } else {
                stmp = funzStringa.tagliaStringa("Ordine a magazzino", gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                posy += 30;
            }
            if (!dtcons.equals("")) {
                stmp = funzStringa.tagliaStringa("Data consegna: " + (new Data(dtcons, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"), gf20);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
                posy += 30;
                posy += 30;
            }
            if (Env.tipostampante == 0 || Env.tipostampante == 2)
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                                 UM     QTA ", lf, 18);
            else
                Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " PRODOTTO                               UM COLLI       QTA   PREZZO ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();
            for (int i = 0; i < vrighe.size(); i++) {
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Record rx = vrighe.get(i);
                String riga = "";
                if (Env.tipostampante == 0 || Env.tipostampante == 2) {
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 31, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "##0.000", Formattazione.NO_SEGNO), 7, funzStringa.DX, ' ');
                } else {
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artcod"), 8, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("artdescr"), 29, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa(rx.leggiStringa("um"), 2, funzStringa.SX, ' ');
                    riga += " " + funzStringa.riempiStringa("" + rx.leggiIntero("colli"), 5, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("qta"), "####0.000", Formattazione.NO_SEGNO), 9, funzStringa.DX, ' ');
                    riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("prezzo"), "###0.000", Formattazione.NO_SEGNO), 8, funzStringa.DX, ' ');
                }
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                posy += 30;
                Env.cpclPrinter.printForm();
                if (i % Env.stampe_par1 == 0) {
                    try {
                        Thread.currentThread().sleep(5000);
                    } catch (Exception ex) {
                    }
//                    int ntent = 0;
//                    while (!FunzioniJazzTv.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20)
//                    {
//                        Log.v("JAZZTV", "BUSY ntent:" + ntent);
//                        try
//                        {
//                            Thread.currentThread().sleep(Env.stampe_par2);
//                        } catch (Exception ex)
//                        {
//                        }
//                        ntent++;
//                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 5 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Totale colli:" + totcolli, lf, 24);
            posy += 30;
            Env.cpclPrinter.printForm();

            cm.close();
            c.close();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }

//		try
//		{
//			Env.wifiPort.disconnect();
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
    }


    public static int centraTestoAndroidStampante(String txt, int grandfont, int lfoglio) {
        int pos = 0;
        if (grandfont == 24) {
            int dimtxt = (int) (14.77f * (float) txt.length());
            pos = (lfoglio - dimtxt) / 2;
        } else if (grandfont == 20) {
            int dimtxt = (int) (12.25f * (float) txt.length());
            pos = (lfoglio - dimtxt) / 2;
        } else if (grandfont == 18) {
            int dimtxt = (int) (10.87f * (float) txt.length());
            pos = (lfoglio - dimtxt) / 2;
        } else if (grandfont == 16) {
            int dimtxt = (int) (9.93f * (float) txt.length());
            pos = (lfoglio - dimtxt) / 2;
        }
        return pos;
    }

    public static void creaArtBis() {
        Env.db.execSQL("DELETE FROM artbis");

        // lettura tabella articoli base e inserimento su artbis
        Cursor cart = Env.db.rawQuery("SELECT artcod,artgiacenza FROM articoli", null);
        while (cart.moveToNext()) {
            SQLiteStatement stmt = Env.db.compileStatement("INSERT INTO artbis (artcod,artgiacenza) VALUES (?,?)");
            stmt.clearBindings();
            stmt.bindString(1, cart.getString(0));
            stmt.bindDouble(2, cart.getDouble(1));
            stmt.execute();
            stmt.close();
        }
        cart.close();
    }

    public static boolean schedaSDPresente() {
        String state = Environment.getExternalStorageState();
        return (state.equals(Environment.MEDIA_MOUNTED) || state.equals(Environment.MEDIA_MOUNTED_READ_ONLY));
    }

    public static boolean schedaSDScrivibile() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    public static String cercaCodiceCessionario(String artcod, String conscod) {
        String ris = "";
        String[] pars = new String[2];
        pars[0] = artcod;
        pars[1] = conscod;
        Cursor cursor = Env.db.rawQuery("SELECT dcclicod FROM daticessionari WHERE dcartcod = ? AND dcconscod = ?", pars);
        if (cursor.moveToFirst())
            ris = cursor.getString(0);
        cursor.close();
        return ris;
    }

    public static void annullaDocumento(int tipodoc, int numdoc, String datadoc, boolean ultimo) {
        if ((Env.tipoannullamentodoc == 0 || Env.tipoannullamentodoc == 2) && tipodoc != 20) {
            // annulla movimento con sospensione
            SQLiteStatement st = Env.db.compileStatement("UPDATE movimenti SET movsospeso = 'S' WHERE movtipo = ? AND movnum = ? AND movdata = ?");
            st.clearBindings();
            st.bindLong(1, tipodoc);
            st.bindLong(2, numdoc);
            st.bindString(3, datadoc);
            st.execute();
            st.close();

            // 1) aggiornamento giacenze articoli
            String[] parsm = new String[3];
            parsm[0] = "" + tipodoc;
            parsm[1] = "" + numdoc;
            parsm[2] = datadoc;
            Cursor cm = Env.db.rawQuery("SELECT rmartcod,rmqta,rmcaumag,rmlotto FROM righemov WHERE rmmovtipo = ? AND rmmovnum = ? AND rmmovdata = ?", parsm);
            while (cm.moveToNext()) {
                String cod = cm.getString(0);
                double qta = cm.getDouble(1);
                String cau = cm.getString(2);
                String lotto = cm.getString(3);
                // legge attuale giacenza articolo
                double giac = FunzioniJBeerApp.leggiGiacenzaArticolo(cod);
                // aggiorna giacenza articolo
                if (cau.equals("VE") || cau.equals("OM") || cau.equals("OT") || cau.equals("SM"))
                    FunzioniJBeerApp.impostaGiacenzaArticolo(cod, OpValute.arrotondaMat(giac + qta, 3));
                else if (cau.equals("RV"))
                    FunzioniJBeerApp.impostaGiacenzaArticolo(cod, OpValute.arrotondaMat(giac - qta, 3));
                else
                    FunzioniJBeerApp.impostaGiacenzaArticolo(cod, OpValute.arrotondaMat(giac, 3));
                // aggiorna giacenza lotto
                if (!lotto.equals("")) {
                    float segno = 0;
                    if (cau.equals("VE") || cau.equals("OM") || cau.equals("OT") || cau.equals("SM"))
                        segno = 1;
                    else if (cau.equals("RV"))
                        segno = -1;
                    else
                        segno = 0;
                    FunzioniJBeerApp.aggiornaGiacenzaLotto(lotto, cod, segno * qta);
                }
            }
            cm.close();

            // aggiornamento causale mov.
            st = Env.db.compileStatement("UPDATE righemov SET rmcaumag = 'AN' WHERE rmmovtipo = ? AND rmmovnum = ? AND rmmovdata = ?");
            st.clearBindings();
            st.bindLong(1, tipodoc);
            st.bindLong(2, numdoc);
            st.bindString(3, datadoc);
            st.execute();
            st.close();

            // eliminazione scoperti e incassi documento
            st = Env.db.compileStatement("DELETE FROM scoperti WHERE scsezdoc = ? AND scnumdoc = ?");
            if (tipodoc == 0)
                st.bindString(1, Env.sezbollaval);
            else if (tipodoc == 1)
                st.bindString(1, Env.sezbollaqta);
            else if (tipodoc == 4)
                st.bindString(1, Env.sezddtreso);
            else if (tipodoc == 2)
                st.bindString(1, Env.sezfattura);
            st.bindLong(2, numdoc);
            st.execute();
            st.close();
            st = Env.db.compileStatement("DELETE FROM incassi WHERE iscsezdoc = ? AND iscnumdoc = ?");
            if (tipodoc == 0)
                st.bindString(1, Env.sezbollaval);
            else if (tipodoc == 1)
                st.bindString(1, Env.sezbollaqta);
            else if (tipodoc == 4)
                st.bindString(1, Env.sezddtreso);
            else if (tipodoc == 2)
                st.bindString(1, Env.sezfattura);
            st.bindLong(2, numdoc);
            st.execute();
            st.close();

            FunzioniJBeerApp.salvaLog("Annullamento mov.n." + numdoc + " tipo " + tipodoc + " data " + datadoc);
        } else {
            // annulla movimento con eliminazione

            // aggiornamento giacenze articoli
            String[] parsm = new String[3];
            parsm[0] = "" + tipodoc;
            parsm[1] = "" + numdoc;
            parsm[2] = datadoc;
            Cursor cm = Env.db.rawQuery("SELECT rmartcod,rmqta,rmcaumag,rmlotto FROM righemov WHERE rmmovtipo = ? AND rmmovnum = ? AND rmmovdata = ?", parsm);
            while (cm.moveToNext()) {
                String cod = cm.getString(0);
                double qta = cm.getDouble(1);
                String cau = cm.getString(2);
                String lotto = cm.getString(3);
                // legge attuale giacenza articolo
                double giac = FunzioniJBeerApp.leggiGiacenzaArticolo(cod);
                // aggiorna giacenza articolo
                if (cau.equals("VE") || cau.equals("OM") || cau.equals("OT") || cau.equals("SM"))
                    FunzioniJBeerApp.impostaGiacenzaArticolo(cod, OpValute.arrotondaMat(giac + qta, 3));
                else if (cau.equals("RV"))
                    FunzioniJBeerApp.impostaGiacenzaArticolo(cod, OpValute.arrotondaMat(giac - qta, 3));
                else
                    FunzioniJBeerApp.impostaGiacenzaArticolo(cod, OpValute.arrotondaMat(giac, 3));
                // aggiorna giacenza lotto
                if (!lotto.equals("")) {
                    float segno = 0;
                    if (cau.equals("VE") || cau.equals("OM") || cau.equals("OT") || cau.equals("SM"))
                        segno = 1;
                    else if (cau.equals("RV"))
                        segno = -1;
                    else
                        segno = 0;
                    FunzioniJBeerApp.aggiornaGiacenzaLotto(lotto, cod, segno * qta);
                }
            }
            cm.close();

            SQLiteStatement st = Env.db.compileStatement("DELETE FROM movimenti WHERE movtipo = ? AND movnum = ? AND movdata = ?");
            st.clearBindings();
            st.bindLong(1, tipodoc);
            st.bindLong(2, numdoc);
            st.bindString(3, datadoc);
            st.execute();
            st.close();
            st = Env.db.compileStatement("DELETE FROM righemov WHERE rmmovtipo = ? AND rmmovnum = ? AND rmmovdata = ?");
            st.clearBindings();
            st.bindLong(1, tipodoc);
            st.bindLong(2, numdoc);
            st.bindString(3, datadoc);
            st.execute();
            st.close();
            if (tipodoc != 20) {
                st = Env.db.compileStatement("DELETE FROM ivamov WHERE immovsez = ? AND immovnum = ? AND immovdata = ?");
                st.clearBindings();
                if (tipodoc == 0)
                    st.bindString(1, Env.sezbollaval);
                else if (tipodoc == 1)
                    st.bindString(1, Env.sezbollaqta);
                else if (tipodoc == 4)
                    st.bindString(1, Env.sezddtreso);
                else if (tipodoc == 2)
                    st.bindString(1, Env.sezfattura);
                st.bindLong(2, numdoc);
                st.bindString(3, datadoc);
                st.execute();
                st.close();
                st = Env.db.compileStatement("DELETE FROM cauzmov WHERE cmmovsez = ? AND cmmovnum = ? AND cmmovdata = ?");
                st.clearBindings();
                if (tipodoc == 0)
                    st.bindString(1, Env.sezbollaval);
                else if (tipodoc == 1)
                    st.bindString(1, Env.sezbollaqta);
                else if (tipodoc == 4)
                    st.bindString(1, Env.sezddtreso);
                else if (tipodoc == 2)
                    st.bindString(1, Env.sezfattura);
                st.bindLong(2, numdoc);
                st.bindString(3, datadoc);
                st.execute();
                st.close();
                st = Env.db.compileStatement("DELETE FROM lottimov WHERE lmmovsez = ? AND lmmovnum = ? AND lmmovdata = ?");
                st.clearBindings();
                if (tipodoc == 0)
                    st.bindString(1, Env.sezbollaval);
                else if (tipodoc == 1)
                    st.bindString(1, Env.sezbollaqta);
                else if (tipodoc == 4)
                    st.bindString(1, Env.sezddtreso);
                else if (tipodoc == 2)
                    st.bindString(1, Env.sezfattura);
                st.bindLong(2, numdoc);
                st.bindString(3, datadoc);
                st.execute();
                st.close();
                st = Env.db.compileStatement("DELETE FROM scadmov WHERE smmovsez = ? AND smmovnum = ? AND smmovdata = ?");
                st.clearBindings();
                if (tipodoc == 0)
                    st.bindString(1, Env.sezbollaval);
                else if (tipodoc == 1)
                    st.bindString(1, Env.sezbollaqta);
                else if (tipodoc == 4)
                    st.bindString(1, Env.sezddtreso);
                else if (tipodoc == 2)
                    st.bindString(1, Env.sezfattura);
                st.bindLong(2, numdoc);
                st.bindString(3, datadoc);
                st.execute();
                st.close();
            }

            if (tipodoc != 20) {
                // eliminazione scoperti e incassi documento
                st = Env.db.compileStatement("DELETE FROM scoperti WHERE scsezdoc = ? AND scnumdoc = ?");
                if (tipodoc == 0)
                    st.bindString(1, Env.sezbollaval);
                else if (tipodoc == 1)
                    st.bindString(1, Env.sezbollaqta);
                else if (tipodoc == 4)
                    st.bindString(1, Env.sezddtreso);
                else if (tipodoc == 2)
                    st.bindString(1, Env.sezfattura);
                st.bindLong(2, numdoc);
                st.execute();
                st.close();
                st = Env.db.compileStatement("DELETE FROM incassi WHERE iscsezdoc = ? AND iscnumdoc = ?");
                if (tipodoc == 0)
                    st.bindString(1, Env.sezbollaval);
                else if (tipodoc == 1)
                    st.bindString(1, Env.sezbollaqta);
                else if (tipodoc == 4)
                    st.bindString(1, Env.sezddtreso);
                else if (tipodoc == 2)
                    st.bindString(1, Env.sezfattura);
                st.bindLong(2, numdoc);
                st.execute();
                st.close();
            }
            // sistemazione sezionali
            if (ultimo) {
                if (tipodoc == 0) {
                    Env.numbollaval--;
                    if (Env.sezbollaqta.equals(Env.sezbollaval))
                        Env.numbollaqta = Env.numbollaval;
                    SQLiteStatement stsez = Env.db.compileStatement(
                            "UPDATE datiterm SET numbollaval=?,numbollaqta=?");
                    stsez.clearBindings();
                    stsez.bindLong(1, Env.numbollaval);
                    stsez.bindLong(2, Env.numbollaqta);
                    stsez.execute();
                    stsez.close();
                } else if (tipodoc == 1) {
                    Env.numbollaqta--;
                    if (Env.sezbollaqta.equals(Env.sezbollaval))
                        Env.numbollaval = Env.numbollaqta;
                    SQLiteStatement stsez = Env.db.compileStatement(
                            "UPDATE datiterm SET numbollaval=?,numbollaqta=?");
                    stsez.clearBindings();
                    stsez.bindLong(1, Env.numbollaval);
                    stsez.bindLong(2, Env.numbollaqta);
                    stsez.execute();
                    stsez.close();
                } else if (tipodoc == 20) {
                    Env.numordinecli--;
                    SQLiteStatement stsez = Env.db.compileStatement(
                            "UPDATE datiterm SET numordinecli=?,numordinecli=?");
                    stsez.clearBindings();
                    stsez.bindLong(1, Env.numordinecli);
                    stsez.bindLong(2, Env.numordinecli);
                    stsez.execute();
                    stsez.close();
                } else if (tipodoc == 4) {
                    Env.numddtreso--;
                    SQLiteStatement stsez = Env.db.compileStatement(
                            "UPDATE datiterm SET numddtreso=?");
                    stsez.clearBindings();
                    stsez.bindLong(1, Env.numddtreso);
                    stsez.execute();
                    stsez.close();
                } else if (tipodoc == 2) {
                    Env.numfattura--;
                    SQLiteStatement stsez = Env.db.compileStatement(
                            "UPDATE datiterm SET numfattura=?");
                    stsez.clearBindings();
                    stsez.bindLong(1, Env.numfattura);
                    stsez.execute();
                    stsez.close();
                }
            }

            FunzioniJBeerApp.salvaLog("Annullamento mov.n." + numdoc + " tipo " + tipodoc + " data " + datadoc);
        }
    }

    public static boolean clienteVisitato(String clicod) {
        boolean ris = false;
        String[] parscli = new String[2];
        parscli[0] = clicod;
        parscli[1] = clicod;
        Cursor cm = Env.db.rawQuery("SELECT movtipo FROM movimenti WHERE (movclicod = ? OR movdestcod = ?) AND movtrasf <> 'S'", parscli);
        if (cm.moveToFirst()) {
            ris = true;
        }
        cm.close();
        return ris;
    }

    public static double dimDisplayPollici(Context context) {

        double size = 0;
        try {

            // Compute screen size

            DisplayMetrics dm = context.getResources().getDisplayMetrics();

            float screenWidth = dm.widthPixels / dm.xdpi;

            float screenHeight = dm.heightPixels / dm.ydpi;

            size = Math.sqrt(Math.pow(screenWidth, 2) +

                    Math.pow(screenHeight, 2));

        } catch (Throwable t) {

        }

        return size;

    }


    public static double leggiPrezzoBaseArticolo(String artcod, String lotto, boolean cercaSoloLotto) {
        double ris = 0;
        String[] parsp = new String[2];
        parsp[0] = artcod;
        parsp[1] = lotto;
        boolean lottopres = false;
        Cursor cp = Env.db.rawQuery("SELECT pbprezzo FROM prezzibase WHERE pbartcod = ? AND pblotcod = ?", parsp);
        if (cp.moveToFirst()) {
            lottopres = true;
            if (cp.getDouble(0) > 0)
                ris = cp.getDouble(0);
        }
        cp.close();
        if (!cercaSoloLotto && !lottopres) {
            if (ris == 0) {
                String[] parsp2 = new String[1];
                parsp2[0] = artcod;
                Cursor cp2 = Env.db.rawQuery("SELECT pbprezzo FROM prezzibase WHERE pbartcod = ? ORDER BY pbdata DESC", parsp2);
                if (cp2.moveToFirst() && cp2.getDouble(0) > 0) {
                    ris = cp2.getDouble(0);
                }
                cp2.close();
            }
            if (ris == 0) {
                String dtoggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
                String[] parslb = new String[3];
                parslb[0] = Env.listbase;
                parslb[1] = artcod;
                parslb[2] = dtoggi;
                Cursor c2 = Env.db.rawQuery("SELECT lprezzo1 FROM listini WHERE lcod = ? AND lartcod = ? AND ldataval <= ? ORDER BY ldataval DESC", parslb);
                if (c2.moveToFirst()) {
                    ris = c2.getDouble(0);
                }
                c2.close();
            }
        }
        return ris;
    }


    public static double leggiPrezzoListinoBaseArticolo(String artcod) {
        double ris = 0;
        String dtoggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
        String[] parsp2 = new String[1];
        parsp2[0] = artcod;
        Cursor cp2 = Env.db.rawQuery("SELECT pbprezzo FROM prezzibase WHERE pbartcod = ? ORDER BY pbdata DESC", parsp2);
        if (cp2.moveToFirst() && cp2.getDouble(0) > 0) {
            ris = cp2.getDouble(0);
        }
        cp2.close();
        if (ris == 0) {
            String[] parslb = new String[3];
            parslb[0] = Env.listbase;
            parslb[1] = artcod;
            parslb[2] = dtoggi;
            Cursor c2 = Env.db.rawQuery("SELECT lprezzo1 FROM listini WHERE lcod = ? AND lartcod = ? AND ldataval <= ? ORDER BY ldataval DESC", parslb);
            if (c2.moveToFirst()) {
                ris = c2.getDouble(0);
            }
            c2.close();
        }
        return ris;
    }

    public static double leggiPrezzoArticoloOrdineFor(String artcod) {
        double ris = 0;
        String dtoggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
        String[] parslb = new String[3];
        parslb[0] = Env.listbase;
        parslb[1] = artcod;
        parslb[2] = dtoggi;
        Cursor c2 = Env.db.rawQuery("SELECT lprezzo1 FROM listini WHERE lcod = ? AND lartcod = ? AND ldataval <= ? ORDER BY ldataval DESC", parslb);
        if (c2.moveToFirst()) {
            ris = c2.getDouble(0);
        }
        c2.close();
        if (ris == 0) {
            String[] parsp2 = new String[1];
            parsp2[0] = artcod;
            Cursor cp2 = Env.db.rawQuery("SELECT pbprezzo FROM prezzibase WHERE pbartcod = ? ORDER BY pbdata DESC", parsp2);
            if (cp2.moveToFirst() && cp2.getDouble(0) > 0) {
                ris = cp2.getDouble(0);
            }
            cp2.close();
        }
        return ris;
    }

    public static double leggiPrezzoArticoloOrdineForNew(String artcod) {
        double ris = 0;
        String dtoggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
        String[] parsp2 = new String[1];
        parsp2[0] = artcod;
        Cursor cp2 = Env.db.rawQuery("SELECT pbprezzo FROM prezzibase WHERE pbartcod = ? AND pbprezzo > 0 ORDER BY pbdata DESC", parsp2);
        if (cp2.moveToFirst() && cp2.getDouble(0) > 0) {
            ris = cp2.getDouble(0);
        }
        cp2.close();
        if (ris == 0) {
            String[] parslb = new String[3];
            parslb[0] = Env.listbase;
            parslb[1] = artcod;
            parslb[2] = dtoggi;
            Cursor c2 = Env.db.rawQuery("SELECT lprezzo1 FROM listini WHERE lcod = ? AND lartcod = ? AND ldataval <= ? ORDER BY ldataval DESC", parslb);
            if (c2.moveToFirst()) {
                ris = c2.getDouble(0);
            }
            c2.close();
        }
        return ris;
    }

    public static double leggiPrezzoDaPreferiti(String artcod, String clicod) {
        double ris = 0;
        String[] parspref = new String[2];
        parspref[0] = artcod;
        parspref[1] = clicod;
        Cursor c2 = Env.db.rawQuery("SELECT prefprezzo FROM preferiti WHERE prefartcod = ? AND prefclicod = ?", parspref);
        if (c2.moveToFirst()) {
            ris = c2.getDouble(0);
        }
        c2.close();
        return ris;
    }


    public static final double calcolaKgArticolo(String artcod, double qta) {
        double ris = qta;
        String[] parsa = new String[1];
        parsa[0] = artcod;
        Cursor cursor = Env.db.rawQuery("SELECT artum,artpeso FROM articoli WHERE artcod = ?", parsa);
        if (cursor.moveToFirst()) {
            if (cursor.getString(0).equalsIgnoreCase("kg"))
                ris = qta;
            else if (cursor.getDouble(1) > 0)
                ris = OpValute.arrotondaMat(qta * cursor.getDouble(1), 3);
            else
                ris = qta;
        }
        cursor.close();

        return ris;
    }

    public static final ArrayList<Record> calcolaCostiVenditeSessione() {
        ArrayList<Record> ris = new ArrayList();
        Cursor cmov = Env.db.rawQuery("SELECT movdoc,movdata,movnum,movclicod,clinome FROM movimenti INNER JOIN clienti ON movimenti.movclicod = clienti.clicodice WHERE movtipo IN (0,1,2,3,4) AND movtrasf<>'S' AND movsospeso = 'N' ORDER BY movtipo,movdata,movnum", null);
        while (cmov.moveToNext()) {
            Record rx = new Record();
            rx.insStringa("doc", cmov.getString(0));
            rx.insStringa("data", cmov.getString(1));
            rx.insIntero("num", cmov.getInt(2));
            rx.insStringa("clicod", cmov.getString(3));
            rx.insStringa("clinome", cmov.getString(4));
            double totcosti = 0, totvendite = 0, totkg = 0;
            String[] parsrm = new String[3];
            parsrm[0] = "" + cmov.getString(0);
            parsrm[1] = cmov.getString(1);
            parsrm[2] = "" + cmov.getInt(2);
            Cursor crm = Env.db.rawQuery("SELECT rmcaumag,rmqta,rmnetto,artcod,artpeso,artum,rmlotto FROM righemov INNER JOIN articoli ON righemov.rmartcod = articoli.artcod WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? AND rmcaumag IN ('VE','VQ','RV','RN') ORDER BY rmriga", parsrm);
            while (crm.moveToNext()) {
                double pb = leggiPrezzoBaseArticolo(crm.getString(3), crm.getString(6), true);
                double c = OpValute.arrotondaMat(crm.getDouble(1) * pb, 2);
                totvendite += crm.getDouble(2);
                if (crm.getString(0).equals("VE") || crm.getString(0).equals("VQ")) {
                    totcosti += c;
                } else {
                    totcosti -= c;
                }
                double kg = 0;
                if (crm.getString(5).equalsIgnoreCase("kg"))
                    kg = crm.getDouble(1);
                else if (crm.getDouble(5) > 0)
                    kg = OpValute.arrotondaMat(crm.getDouble(1) * crm.getDouble(5), 3);
                else
                    kg = crm.getDouble(1);
                if (crm.getString(0).equals("VE") || crm.getString(0).equals("VQ")) {
                    totkg += kg;
                } else {
                    totkg -= kg;
                }
            }
            crm.close();
            rx.insDouble("totvend", totvendite);
            rx.insDouble("totcosti", totcosti);
            rx.insDouble("totkg", totkg);
            double percric = 0;
            if (totcosti != 0)
                percric = OpValute.arrotondaMat((totvendite - totcosti) / totcosti * 100.0, 2);
            rx.insDouble("percric", percric);
            ris.add(rx);
        }
        cmov.close();
        return ris;
    }

    public static final double calcolaTotaleVenditeSessione() {
        double ris = 0;
//		Cursor cve = Env.db.rawQuery("SELECT SUM(movtotale) AS totvend FROM movimenti WHERE movtrasf<>'S' AND movsospeso = 'N' AND movtipo IN (0,1,2,3,4)", null);
//		if (cve.moveToFirst() && !cve.isNull(0))
//			ris = cve.getDouble(0);
//		cve.close();

        // calcolo vendite
        Cursor cve = Env.db.rawQuery("SELECT SUM(rmnetto) AS totvend FROM righemov LEFT JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum) WHERE movtrasf<>'S' AND movsospeso = 'N' AND (rmcaumag='VE')", null);
        //Cursor cve = Env.db.rawQuery("SELECT SUM(rmnetto) AS totvend FROM righemov LEFT JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovnum = movimenti.movnum)", null);
        if (cve.moveToFirst() && !cve.isNull(0))
            ris = cve.getDouble(0);
        cve.close();
        // calcolo resi
        Cursor cre = Env.db.rawQuery("SELECT SUM(rmnetto) AS totvend FROM righemov LEFT JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum) WHERE movtrasf<>'S' AND movsospeso = 'N' AND (rmcaumag='RV' OR rmcaumag='RN')", null);
        if (cre.moveToFirst() && !cre.isNull(0))
            ris -= cre.getDouble(0);
        cre.close();
        return ris;
    }

    public static final double calcolaTotaleCostiSessione() {
        double ris = 0;
        Cursor cm = Env.db.rawQuery("SELECT rmartcod,rmcaumag,rmqta,rmlotto FROM righemov LEFT JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum) WHERE movtrasf<>'S' AND movsospeso = 'N' AND (rmcaumag='VE' OR rmcaumag='VQ' OR rmcaumag='RV' OR rmcaumag='RN')", null);
        while (cm.moveToNext()) {
            double pb = leggiPrezzoBaseArticolo(cm.getString(0), cm.getString(3), true);
            double c = OpValute.arrotondaMat(cm.getDouble(2) * pb, 2);
            if (cm.getString(1).equals("VE") || cm.getString(1).equals("VQ"))
                ris += c;
            else
                ris -= c;
        }
        return ris;
    }

    public static final double calcolaTotaleCostiOmaggiSessione() {
        double ris = 0;
        Cursor cm = Env.db.rawQuery("SELECT rmartcod,rmcaumag,rmqta,rmlotto FROM righemov LEFT JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum) WHERE movtrasf<>'S' AND movsospeso = 'N' AND (rmcaumag='OM' OR rmcaumag='OT')", null);
        while (cm.moveToNext()) {
            double pb = leggiPrezzoBaseArticolo(cm.getString(0), cm.getString(3), true);
            double c = OpValute.arrotondaMat(cm.getDouble(2) * pb, 2);
            ris += c;
        }
        return ris;
    }

    public static final double calcolaTotaleCostiScontiMerceSessione() {
        double ris = 0;
        Cursor cm = Env.db.rawQuery("SELECT rmartcod,rmcaumag,rmqta,rmlotto FROM righemov LEFT JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum) WHERE movtrasf<>'S' AND movsospeso = 'N' AND (rmcaumag='SM')", null);
        while (cm.moveToNext()) {
            double pb = leggiPrezzoBaseArticolo(cm.getString(0), cm.getString(3), true);
            double c = OpValute.arrotondaMat(cm.getDouble(2) * pb, 2);
            ris += c;
        }
        return ris;
    }

    public static double[] leggiPrezzoScontiArticolo(String artcod, String clicod, String clicodpadre, String data, boolean przdapreferiti) {
        double[] ris = new double[4];
        ris[0] = 0;
        ris[1] = 0;
        ris[2] = 0;
        ris[3] = 0;
        String[] pars = new String[1];
        pars[0] = artcod;
        Cursor c = Env.db.rawQuery("SELECT artcod,artdescr,artum,artcodiva,artpezzi,artscprom,artclassecod," +
                "artsottoclassecod,artmarcacod,artscdatainizio,artscdatafine,artcpesclaltri," +
                "artcauvend,artcausm,artcauom,artcaureso,artcausost,artgiacenza,artcodsc,artpeso,artcauzcod FROM articoli WHERE artcod = ?", pars);
        if (c.moveToFirst()) {
            double prz = 0;
            int nprz = 1;
            String codlist = "";
            String codsc = c.getString(18);
            double[] sc = new double[3];
            sc[0] = 0;
            sc[1] = 0;
            sc[2] = 0;
            // sconto promozionale
            boolean esclaltri = false;
            if (c.getDouble(5) != 0) {
                // controllo data
                boolean promOk = true;
                if (!c.getString(9).equals("0000-00-00") && c.getString(9).compareTo(data) > 0)
                    promOk = false;
                if (!c.getString(10).equals("0000-00-00") && c.getString(10).compareTo(data) < 0)
                    promOk = false;
                if (promOk) {
                    if (sc[0] == 0)
                        sc[0] = c.getDouble(5);
                    else if (sc[1] == 0)
                        sc[1] = c.getDouble(5);
                    else if (sc[2] == 0)
                        sc[2] = c.getDouble(5);
                    if (c.getString(11).equals("S"))
                        esclaltri = true;
                }
            }

            if (!clicod.equals("")) {
                // dati listino cliente
                String[] pars2 = new String[1];
                pars2[0] = clicod;
                Cursor c2 = Env.db.rawQuery("SELECT clicodlist,clinprz FROM clienti WHERE clicodice = ?", pars2);
                if (c2.moveToFirst()) {
                    codlist = c2.getString(0);
                    nprz = c2.getInt(1);
                    if (nprz == 0)
                        nprz = 1;
                }
                c2.close();
            } else {
                codlist = Env.clinuovolist;
                nprz = Env.clinuovoposprz;
                if (nprz == 0)
                    nprz = 1;
            }
            // vede se esistono condizioni cliente particolari
            boolean condpers = false;
            if (!esclaltri && !clicod.equals("")) {
                boolean condes = false;
                String[] parscp = new String[2];
                parscp[0] = clicod;
                parscp[1] = artcod;
                Cursor ccp = Env.db.rawQuery("SELECT cpdatainizio,cpdatafine,cpsc1,cpsc2,cpsc3,cplistcod,cpprezzo FROM condpers WHERE cpclicod = ? AND cpartcod = ?", parscp);
                while (ccp.moveToNext()) {
                    // controllo validita
                    boolean condOk = true;
                    if (!ccp.getString(0).equals("0000-00-00") && ccp.getString(0).compareTo(data) > 0)
                        condOk = false;
                    if (!ccp.getString(1).equals("0000-00-00") && ccp.getString(1).compareTo(data) < 0)
                        condOk = false;
                    if (condOk) {
                        condes = true;
                        condpers = true;
                        if (ccp.getDouble(6) != 0)
                            prz = ccp.getDouble(6);
                        if (ccp.getDouble(2) != 0 || ccp.getDouble(3) != 0 || ccp.getDouble(4) != 0) {
                            if (!ccp.getString(5).equals("")) {
                                if (ccp.getString(5).equals(codlist)) {
                                    if (ccp.getDouble(2) != 0) {
                                        if (sc[0] == 0)
                                            sc[0] = ccp.getDouble(2);
                                        else if (sc[1] == 0)
                                            sc[1] = ccp.getDouble(2);
                                        else if (sc[2] == 0)
                                            sc[2] = ccp.getDouble(2);
                                    }
                                    if (ccp.getDouble(3) != 0) {
                                        if (sc[0] == 0)
                                            sc[0] = ccp.getDouble(3);
                                        else if (sc[1] == 0)
                                            sc[1] = ccp.getDouble(3);
                                        else if (sc[2] == 0)
                                            sc[2] = ccp.getDouble(3);
                                    }
                                    if (ccp.getDouble(4) != 0) {
                                        if (sc[0] == 0)
                                            sc[0] = ccp.getDouble(4);
                                        else if (sc[1] == 0)
                                            sc[1] = ccp.getDouble(4);
                                        else if (sc[2] == 0)
                                            sc[2] = ccp.getDouble(4);
                                    }
                                }
                            } else {
                                if (ccp.getDouble(2) != 0) {
                                    if (sc[0] == 0)
                                        sc[0] = ccp.getDouble(2);
                                    else if (sc[1] == 0)
                                        sc[1] = ccp.getDouble(2);
                                    else if (sc[2] == 0)
                                        sc[2] = ccp.getDouble(2);
                                }
                                if (ccp.getDouble(3) != 0) {
                                    if (sc[0] == 0)
                                        sc[0] = ccp.getDouble(3);
                                    else if (sc[1] == 0)
                                        sc[1] = ccp.getDouble(3);
                                    else if (sc[2] == 0)
                                        sc[2] = ccp.getDouble(3);
                                }
                                if (ccp.getDouble(4) != 0) {
                                    if (sc[0] == 0)
                                        sc[0] = ccp.getDouble(4);
                                    else if (sc[1] == 0)
                                        sc[1] = ccp.getDouble(4);
                                    else if (sc[2] == 0)
                                        sc[2] = ccp.getDouble(4);
                                }
                            }
                        }
                    }
                }
                ccp.close();

                if (!condes && !clicodpadre.equals("") && !clicodpadre.equals(clicod)) {
                    parscp = new String[2];
                    parscp[0] = clicodpadre;
                    parscp[1] = artcod;
                    ccp = Env.db.rawQuery("SELECT cpdatainizio,cpdatafine,cpsc1,cpsc2,cpsc3,cplistcod,cpprezzo FROM condpers WHERE cpclicod = ? AND cpartcod = ?", parscp);
                    while (ccp.moveToNext()) {
                        // controllo validita
                        boolean condOk = true;
                        if (!ccp.getString(0).equals("0000-00-00") && ccp.getString(0).compareTo(data) > 0)
                            condOk = false;
                        if (!ccp.getString(1).equals("0000-00-00") && ccp.getString(1).compareTo(data) < 0)
                            condOk = false;
                        if (condOk) {
                            condes = true;
                            condpers = true;
                            if (ccp.getDouble(6) != 0)
                                prz = ccp.getDouble(6);
                            if (ccp.getDouble(2) != 0 || ccp.getDouble(3) != 0 || ccp.getDouble(4) != 0) {
                                if (!ccp.getString(5).equals("")) {
                                    if (ccp.getString(5).equals(codlist)) {
                                        if (ccp.getDouble(2) != 0) {
                                            if (sc[0] == 0)
                                                sc[0] = ccp.getDouble(2);
                                            else if (sc[1] == 0)
                                                sc[1] = ccp.getDouble(2);
                                            else if (sc[2] == 0)
                                                sc[2] = ccp.getDouble(2);
                                        }
                                        if (ccp.getDouble(3) != 0) {
                                            if (sc[0] == 0)
                                                sc[0] = ccp.getDouble(3);
                                            else if (sc[1] == 0)
                                                sc[1] = ccp.getDouble(3);
                                            else if (sc[2] == 0)
                                                sc[2] = ccp.getDouble(3);
                                        }
                                        if (ccp.getDouble(4) != 0) {
                                            if (sc[0] == 0)
                                                sc[0] = ccp.getDouble(4);
                                            else if (sc[1] == 0)
                                                sc[1] = ccp.getDouble(4);
                                            else if (sc[2] == 0)
                                                sc[2] = ccp.getDouble(4);
                                        }
                                    }
                                } else {
                                    if (ccp.getDouble(2) != 0) {
                                        if (sc[0] == 0)
                                            sc[0] = ccp.getDouble(2);
                                        else if (sc[1] == 0)
                                            sc[1] = ccp.getDouble(2);
                                        else if (sc[2] == 0)
                                            sc[2] = ccp.getDouble(2);
                                    }
                                    if (ccp.getDouble(3) != 0) {
                                        if (sc[0] == 0)
                                            sc[0] = ccp.getDouble(3);
                                        else if (sc[1] == 0)
                                            sc[1] = ccp.getDouble(3);
                                        else if (sc[2] == 0)
                                            sc[2] = ccp.getDouble(3);
                                    }
                                    if (ccp.getDouble(4) != 0) {
                                        if (sc[0] == 0)
                                            sc[0] = ccp.getDouble(4);
                                        else if (sc[1] == 0)
                                            sc[1] = ccp.getDouble(4);
                                        else if (sc[2] == 0)
                                            sc[2] = ccp.getDouble(4);
                                    }
                                }
                            }
                        }
                    }
                    ccp.close();
                }
            }

            if (Env.prezzopref && !condpers && przdapreferiti) {
                String cc = clicod;
                if (!clicodpadre.equals(""))
                    cc = clicodpadre;
                double przpref = FunzioniJBeerApp.leggiPrezzoDaPreferiti(artcod, cc);
                if (przpref > 0) {
                    prz = przpref;
                    sc[0] = 0;
                    sc[1] = 0;
                    sc[2] = 0;
                    esclaltri = true;
                }
            }

            if (prz == 0) {
                // legge prezzo di listino
                if (codlist.equals("")) {
                    codlist = Env.clinuovolist;
                }
                String[] pars2 = new String[3];
                pars2[0] = codlist;
                pars2[1] = artcod;
                pars2[2] = data;
                Cursor c2 = Env.db.rawQuery("SELECT lprezzo" + nprz + " FROM listini WHERE lcod = ? AND lartcod = ? AND ldataval <= ? ORDER BY ldataval DESC", pars2);
                if (c2.moveToFirst()) {
                    prz = c2.getDouble(0);
                }
                c2.close();

                // legge sconti listino
                if (!esclaltri) {
                    String[] pars3 = new String[2];
                    pars3[0] = codlist;
                    pars3[1] = c.getString(18);
                    Cursor c3 = Env.db.rawQuery("SELECT slsc" + nprz + "_1,slsc" + nprz + "_2 FROM scontilist WHERE sllistcod = ? AND slcodsc = ?", pars3);
                    if (c3.moveToFirst()) {
                        if (c3.getDouble(0) != 0) {
                            if (sc[0] == 0)
                                sc[0] = c3.getDouble(0);
                            else if (sc[1] == 0)
                                sc[1] = c3.getDouble(0);
                            else if (sc[2] == 0)
                                sc[2] = c3.getDouble(0);
                        }
                        if (c3.getDouble(1) != 0) {
                            if (sc[0] == 0)
                                sc[0] = c3.getDouble(1);
                            else if (sc[1] == 0)
                                sc[1] = c3.getDouble(1);
                            else if (sc[2] == 0)
                                sc[2] = c3.getDouble(1);
                        }
                    }
                    c3.close();
                }

            }
            ris[0] = prz;
            ris[1] = sc[0];
            ris[2] = sc[1];
            ris[3] = sc[2];
        }
        c.close();

        return ris;
    }

    public static double[] calcoloPrezzoArticolo(String codart, String clicod, boolean consideraPreferiti) {
        double[] ris = new double[4];
        ris[0] = 0;
        ris[1] = 0;
        ris[2] = 0;
        ris[3] = 0;

        String dtoggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");

        String[] parscli = new String[1];
        parscli[0] = clicod;
        final Cursor cursor = Env.db.rawQuery(
                "SELECT clicodlist,clinprz,clicodpadre FROM clienti WHERE clicodice = ?", parscli);
        cursor.moveToFirst();
        String clicodlist = cursor.getString(0);
        int clinprz = cursor.getInt(1);
        String clicodpadre = cursor.getString(2);
        cursor.close();

        // legge dati articolo
        String[] pars = new String[1];
        pars[0] = codart;
        Cursor c = Env.db.rawQuery("SELECT artscprom,artscdatainizio,artscdatafine,artcpesclaltri,artcodsc FROM articoli WHERE artcod = ?", pars);
        if (c.moveToFirst()) {
            // *** prezzo
            int nprz = 1;
            String codlist = "";
            String codsc = c.getString(4);
            // sconto promozionale
            boolean esclaltri = false;
            if (c.getDouble(0) != 0) {
                // controllo data
                boolean promOk = true;
                if (!c.getString(1).equals("0000-00-00") && c.getString(1).compareTo(dtoggi) > 0)
                    promOk = false;
                if (!c.getString(2).equals("0000-00-00") && c.getString(2).compareTo(dtoggi) < 0)
                    promOk = false;
                if (promOk) {
                    if (ris[0] == 0)
                        ris[0] = c.getDouble(0);
                    else if (ris[1] == 0)
                        ris[1] = c.getDouble(0);
                    else if (ris[2] == 0)
                        ris[2] = c.getDouble(0);
                    if (c.getString(3).equals("S"))
                        esclaltri = true;
                }
            }

            if (!clicod.equals("")) {
                codlist = clicodlist;
                nprz = clinprz;
            } else {
                codlist = Env.clinuovolist;
                nprz = Env.clinuovoposprz;
                if (nprz == 0)
                    nprz = 1;
            }
            // vede se esistono condizioni cliente particolari
            boolean condpers = false;
            if (!esclaltri && !clicod.equals("")) {
                boolean condes = false;
                String[] pars2 = new String[2];
                pars2[0] = clicod;
                pars2[1] = codart;
                Cursor c2 = Env.db.rawQuery("SELECT cpdatainizio,cpdatafine,cpsc1,cpsc2,cpsc3,cplistcod,cpprezzo FROM condpers WHERE cpclicod = ? AND cpartcod = ?", pars2);
                while (c2.moveToNext()) {
                    // controllo validita
                    boolean condOk = true;
                    if (!c2.getString(0).equals("0000-00-00") && c2.getString(0).compareTo(dtoggi) > 0)
                        condOk = false;
                    if (!c2.getString(1).equals("0000-00-00") && c2.getString(1).compareTo(dtoggi) < 0)
                        condOk = false;
                    if (condOk) {
                        condpers = true;
                        if (c2.getDouble(6) != 0)
                            ris[3] = c2.getDouble(6);
                        if (c2.getDouble(2) != 0 || c2.getDouble(3) != 0 || c2.getDouble(4) != 0) {
                            if (!c2.getString(5).equals("")) {
                                if (c2.getString(5).equals(codlist)) {
                                    if (c2.getDouble(2) != 0) {
                                        if (ris[0] == 0)
                                            ris[0] = c2.getDouble(2);
                                        else if (ris[1] == 0)
                                            ris[1] = c2.getDouble(2);
                                        else if (ris[2] == 0)
                                            ris[2] = c2.getDouble(2);
                                    }
                                    if (c2.getDouble(3) != 0) {
                                        if (ris[0] == 0)
                                            ris[0] = c2.getDouble(3);
                                        else if (ris[1] == 0)
                                            ris[1] = c2.getDouble(3);
                                        else if (ris[2] == 0)
                                            ris[2] = c2.getDouble(3);
                                    }
                                    if (c2.getDouble(4) != 0) {
                                        if (ris[0] == 0)
                                            ris[0] = c2.getDouble(4);
                                        else if (ris[1] == 0)
                                            ris[1] = c2.getDouble(4);
                                        else if (ris[2] == 0)
                                            ris[2] = c2.getDouble(4);
                                    }
                                }
                            } else {
                                if (c2.getDouble(2) != 0) {
                                    if (ris[0] == 0)
                                        ris[0] = c2.getDouble(2);
                                    else if (ris[1] == 0)
                                        ris[1] = c2.getDouble(2);
                                    else if (ris[2] == 0)
                                        ris[2] = c2.getDouble(2);
                                }
                                if (c2.getDouble(3) != 0) {
                                    if (ris[0] == 0)
                                        ris[0] = c2.getDouble(3);
                                    else if (ris[1] == 0)
                                        ris[1] = c2.getDouble(3);
                                    else if (ris[2] == 0)
                                        ris[2] = c2.getDouble(3);
                                }
                                if (c2.getDouble(4) != 0) {
                                    if (ris[0] == 0)
                                        ris[0] = c2.getDouble(4);
                                    else if (ris[1] == 0)
                                        ris[1] = c2.getDouble(4);
                                    else if (ris[2] == 0)
                                        ris[2] = c2.getDouble(4);
                                }
                            }
                        }
                    }
                }
                c2.close();

                if (!condes && !clicodpadre.equals("") && !clicodpadre.equals(clicod)) {
                    pars2 = new String[2];
                    pars2[0] = clicodpadre;
                    pars2[1] = codart;
                    c2 = Env.db.rawQuery("SELECT cpdatainizio,cpdatafine,cpsc1,cpsc2,cpsc3,cplistcod,cpprezzo FROM condpers WHERE cpclicod = ? AND cpartcod = ?", pars2);
                    while (c2.moveToNext()) {
                        // controllo validita
                        boolean condOk = true;
                        if (!c2.getString(0).equals("0000-00-00") && c2.getString(0).compareTo(dtoggi) > 0)
                            condOk = false;
                        if (!c2.getString(1).equals("0000-00-00") && c2.getString(1).compareTo(dtoggi) < 0)
                            condOk = false;
                        if (condOk) {
                            condpers = true;
                            if (c2.getDouble(6) != 0)
                                ris[3] = c2.getDouble(6);
                            if (c2.getDouble(2) != 0 || c2.getDouble(3) != 0 || c2.getDouble(4) != 0) {
                                if (!c2.getString(5).equals("")) {
                                    if (c2.getString(5).equals(codlist)) {
                                        if (c2.getDouble(2) != 0) {
                                            if (ris[0] == 0)
                                                ris[0] = c2.getDouble(2);
                                            else if (ris[1] == 0)
                                                ris[1] = c2.getDouble(2);
                                            else if (ris[2] == 0)
                                                ris[2] = c2.getDouble(2);
                                        }
                                        if (c2.getDouble(3) != 0) {
                                            if (ris[0] == 0)
                                                ris[0] = c2.getDouble(3);
                                            else if (ris[1] == 0)
                                                ris[1] = c2.getDouble(3);
                                            else if (ris[2] == 0)
                                                ris[2] = c2.getDouble(3);
                                        }
                                        if (c2.getDouble(4) != 0) {
                                            if (ris[0] == 0)
                                                ris[0] = c2.getDouble(4);
                                            else if (ris[1] == 0)
                                                ris[1] = c2.getDouble(4);
                                            else if (ris[2] == 0)
                                                ris[2] = c2.getDouble(4);
                                        }
                                    }
                                } else {
                                    if (c2.getDouble(2) != 0) {
                                        if (ris[0] == 0)
                                            ris[0] = c2.getDouble(2);
                                        else if (ris[1] == 0)
                                            ris[1] = c2.getDouble(2);
                                        else if (ris[2] == 0)
                                            ris[2] = c2.getDouble(2);
                                    }
                                    if (c2.getDouble(3) != 0) {
                                        if (ris[0] == 0)
                                            ris[0] = c2.getDouble(3);
                                        else if (ris[1] == 0)
                                            ris[1] = c2.getDouble(3);
                                        else if (ris[2] == 0)
                                            ris[2] = c2.getDouble(3);
                                    }
                                    if (c2.getDouble(4) != 0) {
                                        if (ris[0] == 0)
                                            ris[0] = c2.getDouble(4);
                                        else if (ris[1] == 0)
                                            ris[1] = c2.getDouble(4);
                                        else if (ris[2] == 0)
                                            ris[2] = c2.getDouble(4);
                                    }
                                }
                            }
                        }
                    }
                    c2.close();
                }
            }

            if (consideraPreferiti && Env.prezzopref && !condpers) {
                String cc = clicod;
                if (!clicodpadre.equals(""))
                    cc = clicodpadre;
                double przpref = FunzioniJBeerApp.leggiPrezzoDaPreferiti(codart, cc);
                if (przpref > 0) {
                    ris[3] = przpref;
                    ris[0] = 0;
                    ris[1] = 0;
                    ris[2] = 0;
                    esclaltri = true;
                }
            }


            if (ris[3] == 0) {
                // legge prezzo di listino
                if (!codlist.equals("")) {
                    String[] parsp = new String[3];
                    parsp[0] = codlist;
                    parsp[1] = codart;
                    parsp[2] = dtoggi;
                    Cursor cp = Env.db.rawQuery("SELECT lprezzo" + nprz + " FROM listini WHERE lcod = ? AND lartcod = ? AND ldataval <= ?", parsp);
                    if (cp.moveToFirst()) {
                        ris[3] = cp.getDouble(0);
                    }
                    cp.close();

                    // legge sconti listino
                    if (!esclaltri) {
                        String[] parsc = new String[2];
                        parsc[0] = codlist;
                        parsc[1] = c.getString(4);
                        Cursor csc = Env.db.rawQuery("SELECT slsc" + nprz + "_1,slsc" + nprz + "_2 FROM scontilist WHERE sllistcod = ? AND slcodsc = ?", parsc);
                        if (csc.moveToFirst()) {
                            if (csc.getDouble(0) != 0) {
                                if (ris[0] == 0)
                                    ris[0] = csc.getDouble(0);
                                else if (ris[1] == 0)
                                    ris[1] = csc.getDouble(0);
                                else if (ris[2] == 0)
                                    ris[2] = csc.getDouble(0);
                            }
                            if (csc.getDouble(1) != 0) {
                                if (ris[0] == 0)
                                    ris[0] = csc.getDouble(1);
                                else if (ris[1] == 0)
                                    ris[1] = csc.getDouble(1);
                                else if (ris[2] == 0)
                                    ris[2] = csc.getDouble(1);
                            }
                        }
                        csc.close();
                    }
                }
            }
        }
        c.close();
        return ris;
    }

    public static Record archiviaInventario() {
        String data = (new Data()).formatta(Data.AAAA_MM_GG, "-");
        SimpleDateFormat dh = new SimpleDateFormat("HH:mm:ss");
        String ora = dh.format(new Date());
        int num = Env.numinv;

        SQLiteStatement st = Env.db.compileStatement("INSERT INTO inventario (invtrasf,invdata,invora,invnum,invartcod,invqta,invval) VALUES (?,?,?,?,?,?,?)");
        Cursor c = Env.db.rawQuery("SELECT artcod,artgiacenza FROM articoli WHERE artgiacenza > 0", null);
        while (c.moveToNext()) {
            st.clearBindings();
            st.bindString(1, "N");
            st.bindString(2, data);
            st.bindString(3, ora);
            st.bindLong(4, num);
            st.bindString(5, c.getString(0));
            st.bindDouble(6, c.getDouble(1));
            double prz = FunzioniJBeerApp.leggiPrezzoListinoBaseArticolo(c.getString(0));
            double val = OpValute.arrotondaMat(prz * c.getDouble(1), 2);
            st.bindDouble(7, val);
            st.execute();
        }
        c.close();
        st.close();

        Env.numinv++;
        SQLiteStatement stsez = Env.db.compileStatement(
                "UPDATE datiterm SET numinv=?");
        stsez.clearBindings();
        stsez.bindLong(1, Env.numinv);
        stsez.execute();
        stsez.close();

        Record ris = new Record();
        ris.insStringa("data", data);
        ris.insStringa("ora", ora);
        ris.insIntero("num", num);
        return ris;
    }

    public static void stampaElencoDocumentiEmessi(Context context) {
        int lf = 576;
        if (Env.tipostampante == 1)
            lf = 740;
        int gf16 = 58;
        if (Env.tipostampante == 1)
            gf16 = 76;
        int gf18 = 53;
        if (Env.tipostampante == 1)
            gf18 = 68;
        int gf20 = 47;
        if (Env.tipostampante == 1)
            gf20 = 61;
        int gf24 = 39;
        if (Env.tipostampante == 1)
            gf24 = 41;

        try {
            double totdocs = 0;
            double totpags = 0;
            ArrayList<Record> vrighe = new ArrayList();
            Cursor c = Env.db.rawQuery(
                    "SELECT movtipo,movdata,movnum,movdocora,movclicod,movdestcod,movnettoapag,clicodice,clinome,movacconto FROM movimenti " +
                            "INNER JOIN clienti ON movimenti.movclicod = clienti.clicodice WHERE movtrasf<>'S' AND movsospeso = 'N' ORDER BY movtipo,movdata,movnum,movdocora", null);
            while (c.moveToNext()) {
                Record rx = new Record();
                if (c.getInt(0) == 0)
                    rx.insStringa("tipo", "DDT VAL");
                else if (c.getInt(0) == 1)
                    rx.insStringa("tipo", "DDT QTA");
                else if (c.getInt(0) == 4)
                    rx.insStringa("tipo", "DDT RESO");
                rx.insStringa("data", c.getString(1));
                rx.insIntero("num", c.getInt(2));
                rx.insStringa("ora", c.getString(3));
                rx.insDouble("importo", c.getDouble(6));
                rx.insStringa("cliente", c.getString(8));
                totdocs += c.getDouble(6);
                totpags += c.getDouble(9);
                vrighe.add(rx);
            }
            c.close();

            int nrigheint = 6;
            if (Env.stampasedesec)
                nrigheint += 4;
            Env.cpclPrinter.setForm(0, 200, 200, nrigheint * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            int posy = 0;
            Env.cpclPrinter.printBox(0, posy, 576, posy + 30, 1);
            Env.cpclPrinter.printAndroidFont(centraTestoAndroidStampante("DETTAGLIO DOCUMENTI EMESSI", 24, lf), posy, Typeface.MONOSPACE, true, false, false, "DETTAGLIO DOCUMENTI EMESSI", lf, 24);
            posy += 30;
            SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
            String ora = dh.format(new Date());
            String stmp = "Data:" + (new Data()).formatta(Data.GG_MM_AAAA, "/") + " ora:" + ora;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            if (Env.stampasedesec) {
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, "Luogo di partenza:", lf, 20);
                posy += 30;
                String[] parsdep = new String[1];
                parsdep[0] = Env.depsede;
                Cursor cdep = Env.db.rawQuery("SELECT depubic1,depubic2,depdescr FROM depositi WHERE depcod = ?", parsdep);
                cdep.moveToFirst();
                stmp = funzStringa.tagliaStringa(cdep.getString(2) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(0) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                stmp = funzStringa.tagliaStringa(cdep.getString(1) + " ", 47);
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, stmp, lf, 20);
                posy += 30;
                cdep.close();
            }
            stmp = "Terminale: " + Env.pedcod + "    Targa: " + Env.targamezzo;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            stmp = funzStringa.tagliaStringa("Venditore: " + Env.agecod + ", " + Env.agenome, 47);
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, stmp, lf, 20);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy + 2, Typeface.MONOSPACE, true, false, false, " DOCUMENTO DATA    NUMERO  CLIENTE           IMPORTO ", lf, 18);
            Env.cpclPrinter.printBox(0, posy, lf, posy + 29, 1);
            posy += 30;
            Env.cpclPrinter.printForm();

            for (int i = 0; i < vrighe.size(); i++) {
                Env.cpclPrinter.setForm(0, 200, 200, 1 * 30, 1);
                Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
                Env.cpclPrinter.setTone(20);
                posy = 0;
                Record rx = vrighe.get(i);
                String riga = "";
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("tipo"), 8, funzStringa.SX, ' ');
                riga += " " + (new Data(rx.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AA, "/");
                riga += " " + funzStringa.riempiStringa("" + rx.leggiIntero("num"), 6, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(rx.leggiStringa("cliente"), 18, funzStringa.SX, ' ');
                riga += " " + funzStringa.riempiStringa(Formattazione.formatta(rx.leggiDouble("importo"), "####0.00", Formattazione.NO_SEGNO), 7, funzStringa.DX, ' ');
                Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, false, false, false, riga, lf, 18);
                posy += 30;
                Env.cpclPrinter.printForm();
                if (i % 4 == 0) {
                    int ntent = 0;
                    while (!FunzioniJBeerApp.controlloStampante(Env.cpclPrinter).contains("Normal") && ntent < 20) {
                        Log.v("jbeerapp", "BUSY ntent:" + ntent);
                        try {
                            Thread.currentThread().sleep(500);
                        } catch (Exception ex) {
                        }
                        ntent++;
                    }
                }
            }
            Env.cpclPrinter.setForm(0, 200, 200, 6 * 30, 1);
            Env.cpclPrinter.setMedia(CPCLConst.LK_CPCL_CONTINUOUS);
            Env.cpclPrinter.setTone(20);
            posy = 0;
            Env.cpclPrinter.printLine(0, posy + 15, lf, posy + 15, 1);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Totale documenti: " +
                    funzStringa.riempiStringa(Formattazione.formValuta(totdocs, 12, 2, Formattazione.SEGNO_SX_NEG), 16, funzStringa.DX, ' '), lf, 24);
            posy += 30;
            Env.cpclPrinter.printAndroidFont(0, posy, Typeface.MONOSPACE, true, false, false, "Totale incassati: " +
                    funzStringa.riempiStringa(Formattazione.formValuta(totpags, 12, 2, Formattazione.SEGNO_SX_NEG), 16, funzStringa.DX, ' '), lf, 24);
            posy += 30;
            Env.cpclPrinter.printForm();
        } catch (Exception eprint) {
            eprint.printStackTrace();
            try {
                Env.cpclPrinter.printForm();
            } catch (Exception eprint2) {
            }
        }
    }

    public static String[] backupSDCard(Context context) {
        String[] ris = new String[2];
        ris[0] = "OK";
        ris[1] = "";
        String pathsdcard = "";
        try {
            final Class surfaceControlClass = Class.forName("android.os.storage.StorageManager");
            Method method1 = surfaceControlClass.getMethod("getVolumePaths");
            method1.setAccessible(true);
            final Object object = context.getSystemService(Context.STORAGE_SERVICE);
            String[] volumes = (String[]) method1.invoke(object);
            for (int i = 0; i < volumes.length; i++) {
                Log.v("jbeerapp", "VOL:" + volumes[i]);
                if (!volumes[i].toLowerCase().contains("storage/emulated")) {
                    pathsdcard = volumes[i];
                    break;
                }
            }
        } catch (Exception exx) {
            exx.printStackTrace();
        }
        if (!pathsdcard.equals("")) {
            File sd = new File(pathsdcard + "/Android/data/jsoftware.jbeerapp/files");
            File data = Environment.getDataDirectory();
            Log.v("jbeerapp", "dir.SD:" + sd.getAbsolutePath());
            Log.v("jbeerapp", "dir.SD writable:" + sd.canWrite());
            Log.v("jbeerapp", "dir.data:" + data.getAbsolutePath());
            // pulizia backup più vecchi di 10 gg
            long tsctr = new java.util.Date().getTime();
            tsctr -= 864000000;
            File fctr = new File(pathsdcard + "/Android/data/jsoftware.jbeerapp/files");
            String[] lfctr = fctr.list();
            for (int i = 0; i < lfctr.length; i++) {
                File fbk = new File(pathsdcard + "/Android/data/jsoftware.jbeerapp/files/" + lfctr[i]);
                if (fbk.lastModified() < tsctr) {
                    fbk.delete();
                }
            }

            String dbFilePath = "/data/" + context.getPackageName() + "/databases/jbeerappbd.db";
            SimpleDateFormat dh = new SimpleDateFormat("HHmmss");
            String ora = dh.format(new Date());
            String backupDBPath = "/jbeerappbd_" + (new Data()).formatta(Data.AAAAMMGG) + "_" + ora + ".db";
            File currentDB = new File(data, dbFilePath);
            File backupDB = new File(sd, backupDBPath);
            FileChannel src = null;
            FileChannel dest = null;
            try {
                src = new FileInputStream(currentDB).getChannel();
                dest = new FileOutputStream(backupDB).getChannel();
                dest.transferFrom(src, 0, src.size());
                src.close();
                dest.close();
                ris[0] = "OK";
                ris[1] = "";
            } catch (Exception e) {
                e.printStackTrace();
                ris[0] = "ERR";
                ris[1] = e.getLocalizedMessage();
            }
        } else {
            ris[0] = "ERR";
            ris[1] = "SD card non individuata.\n" +
                    "Backup non effettuato";
        }
        return ris;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean generaPDFDocumento(String doccod, int num, String data, Context context, String nomefile) {
        boolean ris = true;
        try {
            String[] pars = new String[3];
            pars[0] = doccod;
            pars[1] = data;
            pars[2] = "" + num;
            Cursor cm = Env.db.rawQuery("SELECT movtipo,movdoc,movsez,movdata,movnum,movdocora,movclicod,movdestcod,movpagcod," +
                    "movnumcolli,movtotimp,movtotiva,movtotale,movacconto,movnrif,movcesscod,movnote1,movnote2,movdatacons,movaccisa,movnettoapag FROM movimenti WHERE movdoc = ? AND movdata = ? AND movnum = ?", pars);
            cm.moveToFirst();
            boolean accisa = (cm.getDouble(19) > 0);

            ArrayList<Record> vrighe = new ArrayList();
            Cursor c = Env.db.rawQuery("SELECT rmartcod,rmlotto,rmcaumag,rmcolli,rmpezzi,rmcontenuto,rmqta,rmprz,rmartsc1,rmartsc2," +
                    "rmscvend,rmcodiva,rmaliq,rmclisc1,rmclisc2,rmclisc3,rmlordo,rmnetto,rmnettoivato,rmaccisa FROM righemov WHERE rmmovdoc = ? AND rmmovdata = ? AND rmmovnum = ? ORDER BY rmriga", pars);
            while (c.moveToNext()) {
                Record rx = new Record();
                String artcod = c.getString(0);
                if (cm.getInt(0) == 1) {
                    String[] parscess = new String[2];
                    parscess[0] = cm.getString(15);
                    parscess[1] = artcod;
                    Cursor ccodcess = Env.db.rawQuery("SELECT dcartcodext FROM daticessionari WHERE dcclicod = ? AND dcartcod = ?", parscess);
                    if (ccodcess.moveToFirst() && !ccodcess.getString(0).equals("")) {
                        artcod = ccodcess.getString(0);
                    }
                    ccodcess.close();
                }
                rx.insStringa("artcod", artcod);
                String[] parsa = new String[1];
                parsa[0] = c.getString(0);
                Cursor ca = Env.db.rawQuery("SELECT artdescr,artum FROM articoli WHERE artcod = ?", parsa);
                if (ca.moveToFirst()) {
                    rx.insStringa("artdescr", ca.getString(0));
                    rx.insStringa("um", ca.getString(1));
                } else {
                    rx.insStringa("artdescr", "");
                    rx.insStringa("um", "");
                }
                ca.close();
                rx.insStringa("lotto", c.getString(1));
                rx.insStringa("causale", c.getString(2));
                rx.insIntero("colli", c.getInt(3));
                rx.insDouble("qta", c.getDouble(6));
                rx.insDouble("prezzo", c.getDouble(7));
                rx.insDouble("sc1", c.getDouble(8));
                rx.insDouble("sc2", c.getDouble(9));
                rx.insDouble("sc3", c.getDouble(10));
                rx.insStringa("codiva", c.getString(11));
                rx.insDouble("aliquota", c.getDouble(12));
                rx.insDouble("clisc1", c.getDouble(13));
                rx.insDouble("clisc2", c.getDouble(14));
                rx.insDouble("clisc3", c.getDouble(15));
                rx.insDouble("lordo", c.getDouble(16));
                rx.insDouble("netto", c.getDouble(17));
                rx.insDouble("accisa", c.getDouble(19));
                vrighe.add(rx);
            }
            c.close();

            ArrayList<Record> vrigheraggr = new ArrayList();
            HashMap<Integer, ArrayList<Record>> hraggrlotti = new HashMap();
            Env.ddtstamparaggrlotti = false;//fa casino con le stampe
            if (Env.ddtstamparaggrlotti) {
                for (int i = 0; i < vrighe.size(); i++) {
                    Record r = vrighe.get(i);
                    boolean trovato = false;
                    for (int j = 0; j < vrigheraggr.size(); j++) {
                        Record r2 = vrigheraggr.get(j);
                        if (r.leggiStringa("artcod").equals(r2.leggiStringa("artcod")) &&
                                r.leggiStringa("causale").equals(r2.leggiStringa("causale")) &&
                                r.leggiDouble("prezzo") == r2.leggiDouble("prezzo") &&
                                r.leggiDouble("sc1") == r2.leggiDouble("sc1") &&
                                r.leggiDouble("sc2") == r2.leggiDouble("sc2") &&
                                r.leggiDouble("sc3") == r2.leggiDouble("sc3") &&
                                r.leggiStringa("codiva").equals(r2.leggiStringa("codiva")) &&
                                r.leggiDouble("clisc1") == r2.leggiDouble("clisc1") &&
                                r.leggiDouble("clisc2") == r2.leggiDouble("clisc2") &&
                                r.leggiDouble("clisc3") == r2.leggiDouble("clisc3") &&
                                ((String) r.leggiStringa("lotto")).equals(r2.leggiStringa("lotto"))) {
                            double qtaold = r2.leggiDouble("qta");
                            int colliold = r2.leggiIntero("colli");
                            double lordoold = r2.leggiDouble("lordo");
                            double nettoold = r2.leggiDouble("netto");
                            r2.eliminaCampo("lordo");
                            r2.eliminaCampo("netto");
                            r2.eliminaCampo("qta");
                            r2.eliminaCampo("colli");
                            r2.insDouble("lordo", OpValute.arrotondaMat(r.leggiDouble("lordo") + lordoold, 2));
                            r2.insDouble("netto", OpValute.arrotondaMat(r.leggiDouble("netto") + nettoold, 2));
                            r2.insDouble("qta", OpValute.arrotondaMat(r.leggiDouble("qta") + qtaold, 3));
                            r2.insIntero("colli", r.leggiIntero("colli") + colliold);
                            int nriga = j;
                            ArrayList<Record> vl = hraggrlotti.get(new Integer(nriga));
                            Record rl = new Record();
                            rl.insStringa("lotto", r.leggiStringa("lotto"));
                            rl.insDouble("qtalotto", r.leggiDouble("qta"));
                            vl.add(rl);
                            trovato = true;
                            break;
                        }
                    }
                    if (!trovato) {
                        vrigheraggr.add(r);
                        Record rl = new Record();
                        rl.insStringa("lotto", r.leggiStringa("lotto"));
                        rl.insDouble("qtalotto", r.leggiDouble("qta"));
                        int nriga = vrigheraggr.size() - 1;
                        ArrayList<Record> vl = new ArrayList();
                        vl.add(rl);
                        hraggrlotti.put(new Integer(nriga), vl);
                    }
                }
            }

            ArrayList<Record> viva = new ArrayList();
            Cursor civa = Env.db.rawQuery("SELECT imcodiva,imaliq,imimp,imiva FROM ivamov WHERE immovdoc = ? AND immovdata = ? AND immovnum = ? ORDER BY imcodiva", pars);
            while (civa.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("ivacod", civa.getString(0));
                String[] parci = new String[1];
                parci[0] = civa.getString(0);
                Cursor cci = Env.db.rawQuery("SELECT ivadescr FROM codiva WHERE ivacod = ?", parci);
                if (cci.moveToFirst()) {
                    rx.insStringa("ivadescr", cci.getString(0));
                } else {
                    rx.insStringa("ivadescr", "");
                }
                cci.close();
                rx.insDouble("ivaaliq", civa.getDouble(1));
                rx.insDouble("ivaimp", civa.getDouble(2));
                rx.insDouble("ivaiva", civa.getDouble(3));
                viva.add(rx);
            }
            civa.close();

            ArrayList<Record> vscad = new ArrayList();
            Cursor cscad = Env.db.rawQuery("SELECT smdata,smimp FROM scadmov WHERE smmovdoc = ? AND smmovdata = ? AND smmovnum = ? ORDER BY smdata", pars);
            while (cscad.moveToNext()) {
                Record rx = new Record();
                rx.insStringa("data", cscad.getString(0));
                rx.insDouble("importo", cscad.getDouble(1));
                vscad.add(rx);
            }
            cscad.close();

            String pclicod = "";
            String pclinome = "";
            String pcliindir = "";
            String pcliloc = "";
            String pcliprov = "";
            String pclipiva = "";
            String pclispesecauz = "";
            String pclinumcontr = "";
            String pclidatacontr = "";
            String pclistampaprz = "";
            String lclicod = "";
            String lclinome = "";
            String lcliindir = "";
            String lcliloc = "";
            String lcliprov = "";
            String cesscod = "";
            String cessnome = "";
            double clisc1 = 0, clisc2 = 0, clisc3 = 0;
            String pclisconticontr = "";

            String[] parscli = new String[1];
            parscli[0] = cm.getString(6);
            Cursor ccli = Env.db.rawQuery(
                    "SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva,clispesecauz,clinumcontr,clidatacontr,clistampaprz,clisc1,clisc2,clisc3 FROM clienti WHERE clicodice = ?", parscli);
            if (ccli.moveToFirst()) {
                pclicod = ccli.getString(0);
                pclinome = ccli.getString(1);
                pcliindir = ccli.getString(2);
                pcliloc = ccli.getString(3);
                pcliprov = ccli.getString(4);
                pclipiva = ccli.getString(5);
                pclispesecauz = ccli.getString(6);
                pclinumcontr = ccli.getString(7);
                pclidatacontr = ccli.getString(8);
                pclistampaprz = ccli.getString(9);
                clisc1 = ccli.getDouble(10);
                clisc2 = ccli.getDouble(11);
                clisc3 = ccli.getDouble(12);
                if (vrighe.get(0).leggiDouble("clisc1") > 0)
                    pclisconticontr += " " + Formattazione.formValuta(vrighe.get(0).leggiDouble("clisc1"), 3, 1, 0);
                if (vrighe.get(0).leggiDouble("clisc2") > 0)
                    pclisconticontr += " " + Formattazione.formValuta(vrighe.get(0).leggiDouble("clisc2"), 3, 1, 0);
                if (vrighe.get(0).leggiDouble("clisc3") > 0)
                    pclisconticontr += " " + Formattazione.formValuta(vrighe.get(0).leggiDouble("clisc3"), 3, 1, 0);
                pclisconticontr = pclisconticontr.trim();
            }
            ccli.close();

            if (!cm.getString(7).equals("")) {
                String[] parsdest = new String[1];
                parsdest[0] = cm.getString(7);
                Cursor cdest = Env.db.rawQuery(
                        "SELECT clicodice,clinome,cliindir,cliloc,cliprov,clipiva FROM clienti WHERE clicodice = ?", parsdest);
                if (cdest.moveToFirst()) {
                    lclicod = cdest.getString(0);
                    lclinome = cdest.getString(1);
                    lcliindir = cdest.getString(2);
                    lcliloc = cdest.getString(3);
                    lcliprov = cdest.getString(4);
                }
                cdest.close();
            }

            if (!cm.getString(15).equals("")) {
                String[] parscess = new String[1];
                parscess[0] = cm.getString(15);
                Cursor ccess = Env.db.rawQuery(
                        "SELECT clicodice,clinome FROM clienti WHERE clicodice = ?", parscess);
                if (ccess.moveToFirst()) {
                    cesscod = ccess.getString(0);
                    cessnome = ccess.getString(1);
                }
                ccess.close();
            }

            String pagdescr = "";
            int pagtipo = 0;
            String[] parsp = new String[1];
            parsp[0] = cm.getString(8);
            Cursor cpag = Env.db.rawQuery(
                    "SELECT pagdescr,pagtipo FROM pagamenti WHERE pagcod = ?", parsp);
            if (cpag.moveToFirst()) {
                pagdescr = cpag.getString(0);
                pagtipo = cpag.getInt(1);
            }
            cpag.close();

            boolean stampaprezzi = false;
            if (pagtipo == 8)
                stampaprezzi = true;
            else {
                if (!pclinumcontr.equals("") && !pclinumcontr.equals("0")) {
                    stampaprezzi = false;
                } else if (pclistampaprz.equals("S") || pclistampaprz.equals(""))
                    stampaprezzi = true;
                else
                    stampaprezzi = false;
            }
            if (cm.getInt(0) == 1) {
                // ddt qta no prezzi
                stampaprezzi = false;
            }
            if (cm.getInt(0) == 2 || cm.getInt(0) == 3 || cm.getInt(0) == 20) {
                // fattura stampa prezzi si
                stampaprezzi = true;
            }

            // controllo incassi
            int ninc = 0;
            String[] parsinc = new String[1];
            parsinc[0] = cm.getString(6);
            Cursor cinc = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM incassi WHERE iscclicod = ?", parsinc);
            if (cinc.moveToFirst() && !cinc.isNull(0))
                ninc = cinc.getInt(0);
            cinc.close();

            // controllo se ci sono sospesi del giorno
            int nsosp = 0;
            Cursor csosp = Env.db.rawQuery(
                    "SELECT COUNT(*) FROM movimenti LEFT JOIN pagamenti ON movimenti.movpagcod = pagamenti.pagcod WHERE movtrasf = 'N' AND pagtipo = 8 AND movacconto <> 0 and movclicod =?", parsinc);
            if (csosp.moveToFirst() && !csosp.isNull(0))
                nsosp = csosp.getInt(0);
            csosp.close();
            ArrayList<Record> vinc = new ArrayList();
            if (ninc > 0) {
                Cursor crinc = Env.db.rawQuery(
                        "SELECT isctipo,iscsezdoc,iscnumdoc,iscdatadoc,iscdatascad,isaldo FROM incassi WHERE iscclicod = ? ORDER BY iscdatascad DESC", parsinc);
                while (crinc.moveToNext()) {
                    Record rx = new Record();
                    rx.insStringa("tipo", crinc.getString(0));
                    rx.insStringa("sezdoc", crinc.getString(1));
                    rx.insIntero("numdoc", crinc.getInt(2));
                    rx.insStringa("datadoc", crinc.getString(3));
                    rx.insStringa("datascad", crinc.getString(4));
                    rx.insDouble("saldo", crinc.getDouble(5));
                    vinc.add(rx);
                }
                crinc.close();

                crinc = Env.db.rawQuery(
                        "SELECT movtipo,movsez,movnum,movdata,movacconto FROM movimenti LEFT JOIN pagamenti ON movimenti.movpagcod = pagamenti.pagcod where movtrasf = 'N' AND pagtipo=8 AND movacconto <> 0 and movclicod =?", parsinc);
                while (crinc.moveToNext()) {
                    Record rx = new Record();
                    if (crinc.getInt(0) == 0)
                        rx.insStringa("tipo", "B");
                    else if (crinc.getInt(0) == 1)
                        rx.insStringa("tipo", "F");
                    rx.insStringa("sezdoc", crinc.getString(1));
                    rx.insIntero("numdoc", crinc.getInt(2));
                    rx.insStringa("datadoc", crinc.getString(3));
                    rx.insStringa("datascad", "");
                    rx.insDouble("saldo", crinc.getDouble(4));
                    vinc.add(rx);
                }
                crinc.close();
            }

            // consegna fatture art62
            ArrayList<Record> vcf = new ArrayList();
            String[] parscf = new String[2];
            parscf[0] = cm.getString(6);
            parscf[1] = (new Data()).formatta(Data.AAAA_MM_GG, "-");
            Cursor ccf = Env.db.rawQuery(
                    "SELECT scdatadoc,scnumdoc FROM scoperti WHERE scclicod=? " +
                            "AND sctipo = 'F' AND scdataconsfat =? ORDER BY scdatadoc DESC,scnumdoc ASC", parscf);
            while (ccf.moveToNext()) {
                Record rx = new Record();
                rx.insIntero("numdoc", ccf.getInt(1));
                rx.insStringa("datadoc", ccf.getString(0));
                vcf.add(rx);
            }
            ccf.close();

            Paint paint = new Paint();
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(0.5f);
            paint.setStyle(Paint.Style.STROKE);

            Paint paint2 = new Paint();
            paint2.setColor(Color.rgb(240, 240, 240));
            paint2.setStyle(Paint.Style.FILL_AND_STROKE);
            paint2.setStrokeWidth(0.5f);

            TextPaint textpaintetic = new TextPaint();
            textpaintetic.setColor(Color.BLACK);
            textpaintetic.setStyle(Paint.Style.FILL_AND_STROKE);
            textpaintetic.setTypeface(Typeface.SANS_SERIF);
            textpaintetic.setTextSize(6);
            textpaintetic.setTextAlign(Paint.Align.LEFT);

            TextPaint textpaintt1 = new TextPaint();
            textpaintt1.setColor(Color.BLACK);
            textpaintt1.setStyle(Paint.Style.FILL_AND_STROKE);
            textpaintt1.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
            textpaintt1.setTextSize(8);
            textpaintt1.setTextAlign(Paint.Align.LEFT);

            TextPaint textpaintt2 = new TextPaint();
            textpaintt2.setColor(Color.BLACK);
            textpaintt2.setStyle(Paint.Style.FILL_AND_STROKE);
            textpaintt2.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
            textpaintt2.setTextSize(14);
            textpaintt2.setTextAlign(Paint.Align.LEFT);

            TextPaint textpaintimptot = new TextPaint();
            textpaintimptot.setColor(Color.BLACK);
            textpaintimptot.setStyle(Paint.Style.FILL_AND_STROKE);
            textpaintimptot.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
            textpaintimptot.setTextSize(14);
            textpaintimptot.setTextAlign(Paint.Align.RIGHT);

            TextPaint textpainteticdett = new TextPaint();
            textpainteticdett.setColor(Color.BLACK);
            textpainteticdett.setStyle(Paint.Style.FILL_AND_STROKE);
            textpainteticdett.setTypeface(Typeface.SANS_SERIF);
            textpainteticdett.setTextSize(7);
            textpainteticdett.setTextAlign(Paint.Align.CENTER);

            TextPaint textpaintdett = new TextPaint();
            textpaintdett.setColor(Color.BLACK);
            textpaintdett.setStyle(Paint.Style.FILL_AND_STROKE);
            textpaintdett.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
            textpaintdett.setTextSize(6);
            textpaintdett.setTextAlign(Paint.Align.LEFT);

            TextPaint textpaintdettval = new TextPaint();
            textpaintdettval.setColor(Color.BLACK);
            textpaintdettval.setStyle(Paint.Style.FILL_AND_STROKE);
            textpaintdettval.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
            textpaintdettval.setTextSize(7);
            textpaintdettval.setTextAlign(Paint.Align.RIGHT);

            TextPaint textpaintivadescr = new TextPaint();
            textpaintivadescr.setColor(Color.BLACK);
            textpaintivadescr.setStyle(Paint.Style.FILL_AND_STROKE);
            textpaintivadescr.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
            textpaintivadescr.setTextSize(6);
            textpaintivadescr.setTextAlign(Paint.Align.LEFT);

            TextPaint textpaintivaimp = new TextPaint();
            textpaintivaimp.setColor(Color.BLACK);
            textpaintivaimp.setStyle(Paint.Style.FILL_AND_STROKE);
            textpaintivaimp.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
            textpaintivaimp.setTextSize(8);
            textpaintivaimp.setTextAlign(Paint.Align.RIGHT);

            PdfDocument document = new PdfDocument();
            int totpag = 0;
            if (Env.ddtstamparaggrlotti) {
                totpag = vrigheraggr.size() / 14;
                if (vrigheraggr.size() % 14 > 0)
                    totpag++;
            } else {
                totpag = vrighe.size() / 28;
                if (vrighe.size() % 28 > 0)
                    totpag++;
            }
            for (int npag = 1; npag <= totpag; npag++) {
                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(595, 842, npag).create();
                PdfDocument.Page page = document.startPage(pageInfo);

                // sfondo fincature e logo
                // testata
                page.getCanvas().drawRect(30, 20, 565, 236, paint);

                // logo
                if (cm.getInt(0) == 1) {
                    File flogoq = new File(context.getFilesDir() + File.separator + "logoazddtqta.png");
                    if (flogoq.exists()) {
                        try {
                            Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoazddtqta.png");
                            double wbm = (double) bm.getWidth();
                            double hbm = (double) bm.getHeight();
                            double rapp = (double) bm.getWidth() / (double) bm.getHeight();
                            if (wbm > 170) {
                                wbm = 170;
                                hbm = wbm / rapp;
                            }
                            if (hbm > 80) {
                                hbm = 80;
                                wbm = hbm * rapp;
                            }
                            page.getCanvas().drawBitmap(bm, (Rect) null, new Rect(32, 22, 32 + (int) wbm, 22 + (int) hbm), paint);
                        } catch (Exception elogo) {
                            elogo.printStackTrace();
                        }
                    } else {
                        File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                        if (flogo.exists()) {
                            try {
                                Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                                double wbm = (double) bm.getWidth();
                                double hbm = (double) bm.getHeight();
                                double rapp = (double) bm.getWidth() / (double) bm.getHeight();
                                if (wbm > 170) {
                                    wbm = 170;
                                    hbm = wbm / rapp;
                                }
                                if (hbm > 80) {
                                    hbm = 80;
                                    wbm = hbm * rapp;
                                }
                                page.getCanvas().drawBitmap(bm, (Rect) null, new Rect(32, 22, 32 + (int) wbm, 22 + (int) hbm), paint);
                            } catch (Exception elogo) {
                                elogo.printStackTrace();
                            }
                        }
                    }
                } else {
                    File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                    if (flogo.exists()) {
                        try {
                            Bitmap bm = BitmapFactory.decodeFile(context.getFilesDir() + File.separator + "logoaz.png");
                            double wbm = (double) bm.getWidth();
                            double hbm = (double) bm.getHeight();
                            double rapp = (double) bm.getWidth() / (double) bm.getHeight();
                            if (wbm > 170) {
                                wbm = 170;
                                hbm = wbm / rapp;
                            }
                            if (hbm > 80) {
                                hbm = 80;
                                wbm = hbm * rapp;
                            }
                            page.getCanvas().drawBitmap(bm, (Rect) null, new Rect(32, 22, 32 + (int) wbm, 22 + (int) hbm), paint);
                        } catch (Exception elogo) {
                            elogo.printStackTrace();
                        }
                    }
                }

                // dati azienda
                page.getCanvas().drawText(Env.intazriga1, 219, 30, textpaintetic);
                page.getCanvas().drawText(Env.intazriga2, 219, 38, textpaintetic);
                page.getCanvas().drawText(Env.intazriga3, 219, 46, textpaintetic);
                page.getCanvas().drawText(Env.intazriga4, 219, 54, textpaintetic);
                page.getCanvas().drawText(Env.intazriga5, 219, 62, textpaintetic);
                page.getCanvas().drawText(Env.intazriga6, 219, 70, textpaintetic);
                page.getCanvas().drawText(Env.intazriga7, 219, 78, textpaintetic);

                // riquadro cliente
                page.getCanvas().drawRect(219, 92, 565, 236, paint);
                page.getCanvas().save();
                page.getCanvas().clipRect(219, 92, 565, 236);
                page.getCanvas().drawText("CLIENTE / COMMITTENTE", 221, 97, textpaintetic);
                page.getCanvas().drawText("LUOGO DI DESTINAZIONE MERCE", 221, 161, textpaintetic);
                page.getCanvas().drawText(pclinome, 221, 112, textpaintt1);
                page.getCanvas().drawText(pcliindir, 221, 127, textpaintt1);
                page.getCanvas().drawText(pcliloc + " " + pcliprov, 221, 142, textpaintt1);
                page.getCanvas().drawText(lclicod, 221, 176, textpaintt1);
                page.getCanvas().drawText(lclinome, 221, 188, textpaintt1);
                page.getCanvas().drawText(lcliindir, 221, 200, textpaintt1);
                page.getCanvas().drawText(lcliloc + " " + lcliprov, 221, 212, textpaintt1);
                // cessionario
                if (!cesscod.equals("")) {
                    page.getCanvas().drawText("CESSIONARIO:" + cesscod + "-" + cessnome, 221, 227, textpaintt1);
                }
                page.getCanvas().restore();

                // tipo doc
                page.getCanvas().drawRect(30, 135, 219, 163, paint);
                page.getCanvas().drawText("Tipo documento", 32, 140, textpaintetic);
                if (cm.getInt(0) == 0) {
                    page.getCanvas().drawText("DDT TENTATA VENDITA", 32, 154, textpaintt1);
                } else if (cm.getInt(0) == 1) {
                    page.getCanvas().drawText("DDT A QUANTITA'", 32, 154, textpaintt1);
                } else if (cm.getInt(0) == 2) {
                    page.getCanvas().drawText("FATTURA", 32, 154, textpaintt1);
                } else if (cm.getInt(0) == 3) {
                    page.getCanvas().drawText("RICEVUTA FISCALE", 32, 154, textpaintt1);
                } else if (cm.getInt(0) == 4) {
                    page.getCanvas().drawText("DDT DI RESO MERCE", 32, 154, textpaintt1);
                } else if (cm.getInt(0) == 20) {
                    page.getCanvas().drawText("ORDINE CLIENTE", 32, 154, textpaintt1);
                }


                // n.doc
                page.getCanvas().drawRect(30, 163, 115, 194, paint);
                page.getCanvas().drawText("Numero documento", 32, 168, textpaintetic);
                page.getCanvas().drawText("" + num, 32, 182, textpaintt1);
                // data doc
                page.getCanvas().drawRect(115, 163, 188, 194, paint);
                page.getCanvas().drawText("Data documento", 117, 168, textpaintetic);
                page.getCanvas().drawText((new Data(data, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"), 117, 182, textpaintt1);

                // pagina
                page.getCanvas().drawRect(188, 163, 219, 194, paint);
                page.getCanvas().drawText("Pag.", 190, 168, textpaintetic);
                page.getCanvas().drawText("" + npag, 190, 182, textpaintt1);

                // p.iva - cf
                page.getCanvas().drawRect(30, 194, 154, 236, paint);
                page.getCanvas().drawText("Partita IVA/Cod.fiscale", 32, 199, textpaintetic);
                page.getCanvas().drawText(pclipiva, 32, 214, textpaintt1);

                // cod cliente
                page.getCanvas().drawRect(154, 194, 219, 236, paint);
                page.getCanvas().drawText("Codice cliente", 156, 199, textpaintetic);
                page.getCanvas().drawText(pclicod, 156, 209, textpaintt1);

                // sconti contrattuali cliente
                page.getCanvas().drawText("Sconti % cliente", 156, 219, textpaintetic);
                page.getCanvas().drawText(pclisconticontr, 156, 229, textpaintt1);

                // dicitura art.62
                if (cm.getInt(0) != 20) {
                    page.getCanvas().drawText("Assolve gli obblighi di cui all'art.62,comma1, del D.L.", 32, 110, textpaintetic);
                    page.getCanvas().drawText("24/01/2012 n.1, convertito con modificazioni, dalla legge", 32, 120, textpaintetic);
                    page.getCanvas().drawText("24/03/2012 n.27 Decorrenza immediata", 32, 130, textpaintetic);
                }

                // intestazione dettaglio
                if (accisa) {
                    // codice art
                    page.getCanvas().drawRect(30, 236, 80, 250, paint2);
                    page.getCanvas().drawRect(30, 236, 80, 250, paint);
                    page.getCanvas().drawText("Articolo", 55, 246, textpainteticdett);

                    // descr
                    page.getCanvas().drawRect(80, 236, 208, 250, paint2);
                    page.getCanvas().drawRect(80, 236, 208, 250, paint);
                    page.getCanvas().drawText("Descrizione beni", 144, 246, textpainteticdett);

                    // colli
                    page.getCanvas().drawRect(208, 236, 238, 250, paint2);
                    page.getCanvas().drawRect(208, 236, 238, 250, paint);
                    page.getCanvas().drawText("Colli", 223, 246, textpainteticdett);

                    // um
                    page.getCanvas().drawRect(238, 236, 256, 250, paint2);
                    page.getCanvas().drawRect(238, 236, 256, 250, paint);
                    page.getCanvas().drawText("U.M.", 247, 246, textpainteticdett);

                    // qta
                    page.getCanvas().drawRect(256, 236, 296, 250, paint2); // 40
                    page.getCanvas().drawRect(256, 236, 296, 250, paint);
                    page.getCanvas().drawText("Quantità", 276, 246, textpainteticdett);

                    // lotto
                    page.getCanvas().drawRect(296, 236, 326, 250, paint2); // 30
                    page.getCanvas().drawRect(296, 236, 326, 250, paint);
                    page.getCanvas().drawText("Lotto", 311, 246, textpainteticdett);

                    // prezzo
                    page.getCanvas().drawRect(326, 236, 366, 250, paint2);
                    page.getCanvas().drawRect(326, 236, 366, 250, paint);
                    page.getCanvas().drawText("Prezzo", 346, 246, textpainteticdett);

                    // sconti
                    page.getCanvas().drawRect(366, 236, 418, 250, paint2);
                    page.getCanvas().drawRect(366, 236, 418, 250, paint);
                    page.getCanvas().drawText("Sconti %", 392, 246, textpainteticdett);

                    // importo
                    page.getCanvas().drawRect(418, 236, 464, 250, paint2);
                    page.getCanvas().drawRect(418, 236, 464, 250, paint);
                    page.getCanvas().drawText("Importo", 441, 246, textpainteticdett);

                    // accisa
                    page.getCanvas().drawRect(464, 236, 504, 250, paint2);
                    page.getCanvas().drawRect(464, 236, 504, 250, paint);
                    page.getCanvas().drawText("Accisa", 484, 246, textpainteticdett);

                    // contrassegno
                    page.getCanvas().drawRect(504, 236, 544, 250, paint2);
                    page.getCanvas().drawRect(504, 236, 544, 250, paint);
                    page.getCanvas().drawText("Contrass.", 524, 246, textpainteticdett);

                    // c.iva
                    page.getCanvas().drawRect(544, 236, 565, 250, paint2);
                    page.getCanvas().drawRect(544, 236, 565, 250, paint);
                    page.getCanvas().drawText("C.IVA", 553, 246, textpainteticdett);

                    // righe corpo dettaglio
                    page.getCanvas().drawRect(30, 250, 80, 538, paint);
                    page.getCanvas().drawRect(80, 250, 208, 538, paint);
                    page.getCanvas().drawRect(208, 250, 238, 538, paint);
                    page.getCanvas().drawRect(238, 250, 256, 538, paint);
                    page.getCanvas().drawRect(256, 250, 296, 538, paint);
                    page.getCanvas().drawRect(296, 250, 326, 538, paint);
                    page.getCanvas().drawRect(326, 250, 366, 538, paint);
                    page.getCanvas().drawRect(366, 250, 418, 538, paint);
                    page.getCanvas().drawRect(418, 250, 464, 538, paint);
                    page.getCanvas().drawRect(464, 250, 504, 538, paint);
                    page.getCanvas().drawRect(504, 250, 544, 538, paint);
                    page.getCanvas().drawRect(544, 250, 565, 538, paint);
                } else {
                    // codice art
                    page.getCanvas().drawRect(30, 236, 80, 250, paint2);
                    page.getCanvas().drawRect(30, 236, 80, 250, paint);
                    page.getCanvas().drawText("Articolo", 55, 246, textpainteticdett);

                    // descr
                    page.getCanvas().drawRect(80, 236, 247, 250, paint2);
                    page.getCanvas().drawRect(80, 236, 247, 250, paint);
                    page.getCanvas().drawText("Descrizione beni", 163, 246, textpainteticdett);
                    // colli
                    page.getCanvas().drawRect(247, 236, 285, 250, paint2);
                    page.getCanvas().drawRect(247, 236, 285, 250, paint);
                    page.getCanvas().drawText("Colli", 266, 246, textpainteticdett);
                    // um
                    page.getCanvas().drawRect(285, 236, 303, 250, paint2);
                    page.getCanvas().drawRect(285, 236, 303, 250, paint);
                    page.getCanvas().drawText("U.M.", 294, 246, textpainteticdett);
                    // qta
                    page.getCanvas().drawRect(303, 236, 343, 250, paint2);
                    page.getCanvas().drawRect(303, 236, 343, 250, paint);
                    page.getCanvas().drawText("Quantità", 323, 246, textpainteticdett);
                    // lotto
                    page.getCanvas().drawRect(343, 236, 391, 250, paint2);
                    page.getCanvas().drawRect(343, 236, 391, 250, paint);
                    page.getCanvas().drawText("Lotto", 367, 246, textpainteticdett);
                    // prezzo
                    page.getCanvas().drawRect(391, 236, 439, 250, paint2);
                    page.getCanvas().drawRect(391, 236, 439, 250, paint);
                    page.getCanvas().drawText("Prezzo", 415, 246, textpainteticdett);
                    // sconti
                    page.getCanvas().drawRect(439, 236, 492, 250, paint2);
                    page.getCanvas().drawRect(439, 236, 492, 250, paint);
                    page.getCanvas().drawText("Sconti %", 465, 246, textpainteticdett);
                    // importo
                    page.getCanvas().drawRect(492, 236, 544, 250, paint2);
                    page.getCanvas().drawRect(492, 236, 544, 250, paint);
                    page.getCanvas().drawText("Importo", 518, 246, textpainteticdett);
                    // c.iva
                    page.getCanvas().drawRect(544, 236, 565, 250, paint2);
                    page.getCanvas().drawRect(544, 236, 565, 250, paint);
                    page.getCanvas().drawText("C.IVA", 553, 246, textpainteticdett);
                    // righe corpo dettaglio
                    page.getCanvas().drawRect(30, 250, 80, 538, paint);
                    page.getCanvas().drawRect(80, 250, 247, 538, paint);
                    page.getCanvas().drawRect(247, 250, 285, 538, paint);
                    page.getCanvas().drawRect(285, 250, 303, 538, paint);
                    page.getCanvas().drawRect(303, 250, 343, 538, paint);
                    page.getCanvas().drawRect(343, 250, 391, 538, paint);
                    page.getCanvas().drawRect(391, 250, 439, 538, paint);
                    page.getCanvas().drawRect(439, 250, 492, 538, paint);
                    page.getCanvas().drawRect(492, 250, 544, 538, paint);
                    page.getCanvas().drawRect(544, 250, 565, 538, paint);
//                for (int nr = 0; nr < 28; nr++)
//                {
//                    page.getCanvas().drawText("XXXXXXXXXX", 32, 260 + nr * 10, textpaintdett);
//                }
                }
                double totqta = 0;
                if (!Env.ddtstamparaggrlotti || cm.getInt(0) == 20) {
                    int ind = (npag - 1) * 28;
                    for (int i = ind; i < ind + 28 && i < vrighe.size(); i++) {
                        Record rdett = vrighe.get(i);
                        boolean omaggio = false;
                        if (rdett.leggiStringa("causale").equals("OM") || rdett.leggiStringa("causale").equals("OT") || rdett.leggiStringa("causale").equals("SM")) {
                            omaggio = true;
                        }
                        if (accisa) {
                            // codice articolo
                            page.getCanvas().save();
                            page.getCanvas().clipRect(30, 250, 80, 538);
                            page.getCanvas().drawText(rdett.leggiStringa("artcod"), 32, 260 + (i - ind) * 10, textpaintdett);
                            page.getCanvas().restore();
                            // descrizione
                            page.getCanvas().save();
                            page.getCanvas().clipRect(80, 250, 208, 538);
                            page.getCanvas().drawText(rdett.leggiStringa("artdescr"), 82, 260 + (i - ind) * 10, textpaintdett);
                            page.getCanvas().restore();
                            // colli
                            page.getCanvas().drawText("" + rdett.leggiIntero("colli"), 236, 260 + (i - ind) * 10, textpaintdettval);
                            // um
                            page.getCanvas().drawText(rdett.leggiStringa("um"), 240, 260 + (i - ind) * 10, textpaintdett);
                            // qta
                            page.getCanvas().drawText(Formattazione.formValuta(rdett.leggiDouble("qta"), 12, 3, Formattazione.SEGNO_SX_NEG), 294, 260 + (i - ind) * 10, textpaintdettval);
                            // lotto
                            page.getCanvas().drawText(rdett.leggiStringa("lotto"), 298, 260 + (i - ind) * 10, textpaintdett);
                            if (stampaprezzi && !omaggio) {
                                // prezzo
                                page.getCanvas().drawText(Formattazione.formValuta(rdett.leggiDouble("prezzo"), 12, 3, Formattazione.SEGNO_SX_NEG), 364, 260 + (i - ind) * 10, textpaintdettval);
                                // sconti
                                String ssc = "";
                                if (rdett.leggiDouble("sc1") > 0)
                                    ssc += Formattazione.formValuta(rdett.leggiDouble("sc1"), 3, 1, 0) + " ";
                                if (rdett.leggiDouble("sc2") > 0)
                                    ssc += Formattazione.formValuta(rdett.leggiDouble("sc2"), 3, 1, 0) + " ";
                                if (rdett.leggiDouble("sc3") > 0)
                                    ssc += Formattazione.formValuta(rdett.leggiDouble("sc3"), 3, 1, 0) + " ";
                                page.getCanvas().drawText(ssc, 368, 260 + (i - ind) * 10, textpaintdett);
                                // importo
                                page.getCanvas().drawText(Formattazione.formValuta(rdett.leggiDouble("netto"), 12, 2, Formattazione.SEGNO_SX_NEG), 462, 260 + (i - ind) * 10, textpaintdettval);
                                // accisa
                                page.getCanvas().drawText(Formattazione.formValuta(rdett.leggiDouble("accisa"), 12, 2, Formattazione.SEGNO_SX_NEG), 502, 260 + (i - ind) * 10, textpaintdettval);

                                // civa
                                page.getCanvas().drawText(rdett.leggiStringa("codiva"), 546, 260 + (i - ind) * 10, textpaintdett);
                            }
                            if (!rdett.leggiStringa("causale").equals("RN") && !rdett.leggiStringa("causale").equals("RV"))
                                totqta += rdett.leggiDouble("qta");
                        } else {
                            // codice articolo
                            page.getCanvas().save();
                            page.getCanvas().clipRect(30, 250, 80, 538);
                            page.getCanvas().drawText(rdett.leggiStringa("artcod"), 32, 260 + (i - ind) * 10, textpaintdett);
                            page.getCanvas().restore();
                            // descrizione
                            page.getCanvas().save();
                            page.getCanvas().clipRect(80, 250, 247, 538);
                            page.getCanvas().drawText(rdett.leggiStringa("artdescr"), 82, 260 + (i - ind) * 10, textpaintdett);
                            page.getCanvas().restore();
                            // colli
                            page.getCanvas().drawText("" + rdett.leggiIntero("colli"), 283, 260 + (i - ind) * 10, textpaintdettval);
                            // um
                            page.getCanvas().drawText(rdett.leggiStringa("um"), 287, 260 + (i - ind) * 10, textpaintdett);
                            // qta
                            page.getCanvas().drawText(Formattazione.formValuta(rdett.leggiDouble("qta"), 12, 3, Formattazione.SEGNO_SX_NEG), 341, 260 + (i - ind) * 10, textpaintdettval);
                            // lotto
                            page.getCanvas().drawText(rdett.leggiStringa("lotto"), 345, 260 + (i - ind) * 10, textpaintdett);
                            if (stampaprezzi && !omaggio) {
                                // prezzo
                                page.getCanvas().drawText(Formattazione.formValuta(rdett.leggiDouble("prezzo"), 12, 3, Formattazione.SEGNO_SX_NEG), 437, 260 + (i - ind) * 10, textpaintdettval);
                                // sconti
                                String ssc = "";
                                if (rdett.leggiDouble("sc1") > 0)
                                    ssc += Formattazione.formValuta(rdett.leggiDouble("sc1"), 3, 1, 0) + " ";
                                if (rdett.leggiDouble("sc2") > 0)
                                    ssc += Formattazione.formValuta(rdett.leggiDouble("sc2"), 3, 1, 0) + " ";
                                if (rdett.leggiDouble("sc3") > 0)
                                    ssc += Formattazione.formValuta(rdett.leggiDouble("sc3"), 3, 1, 0) + " ";
                                page.getCanvas().drawText(ssc, 441, 260 + (i - ind) * 10, textpaintdett);
                                // importo
                                page.getCanvas().drawText(Formattazione.formValuta(rdett.leggiDouble("netto"), 12, 2, Formattazione.SEGNO_SX_NEG), 542, 260 + (i - ind) * 10, textpaintdettval);
                                // civa
                                page.getCanvas().drawText(rdett.leggiStringa("codiva"), 546, 260 + (i - ind) * 10, textpaintdett);
                            }
                            if (!rdett.leggiStringa("causale").equals("RN") && !rdett.leggiStringa("causale").equals("RV"))
                                totqta += rdett.leggiDouble("qta");
                        }
                    }
                } else {
                    int ind = (npag - 1) * 14;
                    for (int i = ind; i < ind + 14 && i < vrigheraggr.size(); i++) {
                        Record rdett = vrigheraggr.get(i);
                        boolean omaggio = false;
                        if (rdett.leggiStringa("causale").equals("OM") || rdett.leggiStringa("causale").equals("OT") || rdett.leggiStringa("causale").equals("SM")) {
                            omaggio = true;
                        }
                        ArrayList<Record> vlotti = hraggrlotti.get(i);
                        // riga articolo
                        // codice articolo
                        page.getCanvas().save();
                        page.getCanvas().clipRect(30, 250, 80, 538);
                        page.getCanvas().drawText(rdett.leggiStringa("artcod"), 32, 260 + (i - ind) * 20, textpaintdett);
                        page.getCanvas().restore();
                        // descrizione
                        page.getCanvas().save();
                        page.getCanvas().clipRect(80, 250, 247, 538);
                        page.getCanvas().drawText(rdett.leggiStringa("artdescr"), 82, 260 + (i - ind) * 20, textpaintdett);
                        page.getCanvas().restore();
                        // colli
                        page.getCanvas().drawText("" + rdett.leggiIntero("colli"), 283, 260 + (i - ind) * 20, textpaintdettval);
                        // um
                        page.getCanvas().drawText(rdett.leggiStringa("um"), 287, 260 + (i - ind) * 20, textpaintdett);
                        // qta
                        page.getCanvas().drawText(Formattazione.formValuta(rdett.leggiDouble("qta"), 12, 3, Formattazione.SEGNO_SX_NEG), 341, 260 + (i - ind) * 20, textpaintdettval);
                        // lotto
                        //page.getCanvas().drawText(rdett.leggiStringa("lotto"), 345, 260 + (i - ind) * 10, textpaintdett);
                        if (stampaprezzi && !omaggio) {
                            // prezzo
                            page.getCanvas().drawText(Formattazione.formValuta(rdett.leggiDouble("prezzo"), 12, 3, Formattazione.SEGNO_SX_NEG), 437, 260 + (i - ind) * 20, textpaintdettval);
                            // sconti
                            String ssc = "";
                            if (rdett.leggiDouble("sc1") > 0)
                                ssc += Formattazione.formValuta(rdett.leggiDouble("sc1"), 3, 1, 0) + " ";
                            if (rdett.leggiDouble("sc2") > 0)
                                ssc += Formattazione.formValuta(rdett.leggiDouble("sc2"), 3, 1, 0) + " ";
                            if (rdett.leggiDouble("sc3") > 0)
                                ssc += Formattazione.formValuta(rdett.leggiDouble("sc3"), 3, 1, 0) + " ";
                            page.getCanvas().drawText(ssc, 441, 260 + (i - ind) * 20, textpaintdett);
                            // importo
                            page.getCanvas().drawText(Formattazione.formValuta(rdett.leggiDouble("netto"), 12, 2, Formattazione.SEGNO_SX_NEG), 542, 260 + (i - ind) * 20, textpaintdettval);
                            // civa
                            page.getCanvas().drawText(rdett.leggiStringa("codiva"), 546, 260 + (i - ind) * 20, textpaintdett);
                        }
                        // riga lotti
                        String sl = "LOTTI: ";
                        for (int j = 0; j < vlotti.size(); j++) {
                            Record rl = vlotti.get(j);
                            sl += "[" + rl.leggiStringa("lotto") + " qtà:" + Formattazione.formValuta(rl.leggiDouble("qtalotto"), 12, 3, 0) + "] ";
                        }
                        page.getCanvas().drawText(sl, 82, 260 + (i - ind) * 20 + 10, textpaintdett);
                        if (!rdett.leggiStringa("causale").equals("RN") && !rdett.leggiStringa("causale").equals("RV"))
                            totqta += rdett.leggiDouble("qta");
                    }
                }

                // piede
                // iva
                page.getCanvas().drawRect(30, 538, 295, 660, paint);
                page.getCanvas().drawText("Dati IVA:", 32, 543, textpaintetic);

                page.getCanvas().drawRect(30, 548, 130, 562, paint2);
                page.getCanvas().drawRect(30, 548, 130, 562, paint);
                page.getCanvas().drawText("Codice IVA", 80, 558, textpainteticdett);

                page.getCanvas().drawRect(130, 548, 160, 562, paint2);
                page.getCanvas().drawRect(130, 548, 160, 562, paint);
                page.getCanvas().drawText("Aliq.", 145, 558, textpainteticdett);

                page.getCanvas().drawRect(160, 548, 230, 562, paint2);
                page.getCanvas().drawRect(160, 548, 230, 562, paint);
                page.getCanvas().drawText("Imponibile", 195, 558, textpainteticdett);
                page.getCanvas().drawRect(160, 646, 230, 660, paint);

                page.getCanvas().drawRect(230, 548, 295, 562, paint2);
                page.getCanvas().drawRect(230, 548, 295, 562, paint);
                page.getCanvas().drawText("IVA", 262, 558, textpainteticdett);
                page.getCanvas().drawRect(230, 646, 295, 660, paint);

                if (cm.getInt(0) == 2 || cm.getInt(0) == 3) {
                    for (int iv = 0; iv < viva.size(); iv++) {
                        Record riva = viva.get(iv);
                        page.getCanvas().save();
                        page.getCanvas().clipRect(30, 562, 130, 646);
                        page.getCanvas().drawText(riva.leggiStringa("ivacod") + " " + riva.leggiStringa("ivadescr"), 32, 562 + (iv + 1) * 10, textpaintivadescr);
                        page.getCanvas().restore();
                        page.getCanvas().drawText(Formattazione.formValuta(riva.leggiDouble("ivaaliq"), 12, 0, Formattazione.SEGNO_SX_NEG), 158, 562 + (iv + 1) * 10, textpaintivaimp);
                        page.getCanvas().drawText(Formattazione.formValuta(riva.leggiDouble("ivaimp"), 12, 2, Formattazione.SEGNO_SX_NEG), 228, 562 + (iv + 1) * 10, textpaintivaimp);
                        page.getCanvas().drawText(Formattazione.formValuta(riva.leggiDouble("ivaiva"), 12, 2, Formattazione.SEGNO_SX_NEG), 293, 562 + (iv + 1) * 10, textpaintivaimp);
                    }
                }
                // totali iva
                if (stampaprezzi) {
                    page.getCanvas().drawText(Formattazione.formValuta(cm.getDouble(10), 12, 2, Formattazione.SEGNO_SX_NEG), 228, 656, textpaintivaimp);
                    page.getCanvas().drawText(Formattazione.formValuta(cm.getDouble(11), 12, 2, Formattazione.SEGNO_SX_NEG), 293, 656, textpaintivaimp);
                }

                // pagamento - dicitura art62
                page.getCanvas().drawRect(30, 660, 295, 694, paint);
                page.getCanvas().save();
                page.getCanvas().clipRect(30, 660, 295, 694);
                page.getCanvas().drawText("Pagamento", 32, 665, textpaintetic);
                page.getCanvas().drawText(cm.getString(8) + " " + pagdescr, 32, 680, textpaintt1);
                page.getCanvas().restore();

                if (cm.getInt(0) != 20) {
                    if (!pclinumcontr.equals("")) {
                        String dataContratto = "";
                        if (pclidatacontr.equals("0000-00-00") || pclidatacontr.equals(""))
                            dataContratto = "";
                        else
                            dataContratto = (new Data(pclidatacontr, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                        page.getCanvas().drawText("ART.62:Contratto: " + pclinumcontr + " del " + dataContratto, 32, 690, textpaintt1);
                    } else {
                        page.getCanvas().drawText("ART.62:contratto con validità limitata alla presente cessione", 32, 690, textpaintt1);
                    }
                }

                // totale doc
                page.getCanvas().drawRect(295, 538, 565, 630, paint2);
                page.getCanvas().drawRect(295, 538, 565, 630, paint);
                if (cm.getInt(0) == 20) {
                    page.getCanvas().drawText("Totale ordine:", 350, 560, textpaintt2);
                } else if (stampaprezzi) {
                    page.getCanvas().drawText("Totale documento:", 350, 560, textpaintt2);
                }
                if (stampaprezzi)
                    page.getCanvas().drawText(Formattazione.formValuta(cm.getDouble(20), 12, 2, Formattazione.SEGNO_SX_NEG), 560, 560, textpaintimptot);
                // totale incassato
                if (cm.getDouble(13) != 0 && stampaprezzi) {
                    page.getCanvas().drawText("Totale incassato:", 350, 585, textpaintt2);
                    page.getCanvas().drawText(Formattazione.formValuta(cm.getDouble(13), 12, 2, Formattazione.SEGNO_SX_NEG), 560, 585, textpaintimptot);
                }
                if (accisa) {
                    page.getCanvas().drawText("di cui totale accisa:", 350, 605, textpaintt2);
                    page.getCanvas().drawText(Formattazione.formValuta(cm.getDouble(19), 12, 2, Formattazione.SEGNO_SX_NEG), 560, 605, textpaintimptot);

                }
                // scadenze
                page.getCanvas().drawRect(295, 592, 565, 694, paint);
                page.getCanvas().drawText("Scadenze", 297, 602, textpaintetic);
                if (cm.getInt(0) == 2 || cm.getInt(0) == 3) {
                    for (int sc = 0; sc < vscad.size(); sc++) {
                        Record rsc = vscad.get(sc);
                        page.getCanvas().drawText((new Data(rsc.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"), 297, 602 + (sc + 1) * 15, textpaintivadescr);
                        page.getCanvas().drawText(Formattazione.formValuta(rsc.leggiDouble("importo"), 12, 2, Formattazione.SEGNO_SX_NEG), 400, 602 + (sc + 1) * 15, textpaintivaimp);
                    }
                }

                // causale trasporto
                page.getCanvas().drawRect(30, 694, 163, 721, paint);
                page.getCanvas().drawText("Causale trasporto", 32, 699, textpaintetic);
                if (cm.getInt(0) != 20) {
                    page.getCanvas().drawText("VENDITA", 32, 714, textpaintt1);
                }

                // aspetto beni
                page.getCanvas().drawRect(163, 694, 295, 721, paint);
                page.getCanvas().drawText("Aspetto beni", 165, 699, textpaintetic);
                if (cm.getInt(0) != 20) {
                    page.getCanvas().drawText("CASSE/CARTONI", 165, 714, textpaintt1);
                }

                // tot.qta
                page.getCanvas().drawRect(295, 694, 430, 721, paint);
                page.getCanvas().drawText("Totale quantità", 297, 699, textpaintetic);
                page.getCanvas().drawText(Formattazione.formatta(totqta, "#####0.000", Formattazione.NO_SEGNO), 297, 714, textpaintt1);

                // data ora trasporto
                page.getCanvas().drawRect(430, 694, 565, 721, paint);
                page.getCanvas().drawText("Data/Ora trasporto", 432, 699, textpaintetic);
                if (cm.getInt(0) != 20) {
                    page.getCanvas().drawText((new Data(cm.getString(3), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + " " + cm.getString(5), 432, 714, textpaintt1);
                }

                // vettore (firma digitalizzata)
                page.getCanvas().drawRect(30, 721, 565, 771, paint);
                page.getCanvas().save();
                page.getCanvas().clipRect(30, 721, 565, 771);
                if (cm.getInt(0) != 20) {
                    page.getCanvas().drawText("Venditore / Caricatore merce / Vettore", 32, 726, textpaintetic);
                } else {
                    page.getCanvas().drawText("Venditore", 32, 726, textpaintetic);
                }
                page.getCanvas().drawText(Env.agenome, 32, 741, textpaintt1);
                String[] parsage = new String[1];
                parsage[0] = Env.agecod;
                Cursor cage = Env.db.rawQuery("SELECT ageindirizzo,agelocalita,agepartitaiva,agecodicefiscale,agenregalbo,agetel,agefax,agemobile,ageemail FROM agenti WHERE agecod = ?", parsage);
                if (cage.moveToFirst()) {
                    page.getCanvas().drawText(cage.getString(0) + " " + cage.getString(1), 32, 751, textpaintt1);
                    page.getCanvas().drawText("Partita IVA:" + cage.getString(2) + " Cod.fiscale:" + cage.getString(3), 32, 761, textpaintt1);
                }
                cage.close();
                page.getCanvas().restore();

                // annotazioni
                page.getCanvas().drawRect(30, 771, 295, 823, paint);
                page.getCanvas().save();
                page.getCanvas().clipRect(30, 771, 295, 823);
                page.getCanvas().drawText("Annotazioni", 32, 776, textpaintetic);
                if (!cm.getString(18).equals("") && !cm.getString(18).equals("0000-00-00")) {
                    page.getCanvas().drawText("DATA PREVISTA DI CONSEGNA: " + (new Data(cm.getString(18), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"), 32, 791, textpaintt1);
                    page.getCanvas().drawText(cm.getString(16), 32, 801, textpaintt1);
                    page.getCanvas().drawText(cm.getString(17), 32, 811, textpaintt1);
                } else {
                    page.getCanvas().drawText(cm.getString(16), 32, 791, textpaintt1);
                    page.getCanvas().drawText(cm.getString(17), 32, 801, textpaintt1);
                }
                page.getCanvas().restore();

                // firma conducente (digitale)
                page.getCanvas().drawRect(295, 771, 430, 823, paint);
                if (cm.getInt(0) != 20) {
                    page.getCanvas().drawText("Firma conducente", 297, 776, textpaintetic);
                }
                // firma destinatario
                page.getCanvas().drawRect(430, 771, 565, 823, paint);
                if (cm.getInt(0) != 20) {
                    page.getCanvas().drawText("Firma destinatario", 432, 776, textpaintetic);
                }

                document.finishPage(page);
            }
            document.writeTo(new FileOutputStream(nomefile));
            document.close();

            cm.close();
            c.close();
        } catch (Exception epdf) {
            epdf.printStackTrace();
            ris = false;
        }
        return ris;
    }

    public static boolean umDecimale(String artcod) {
        boolean ris = true;
        String[] pars = new String[1];
        pars[0] = artcod;
        Cursor c = Env.db.rawQuery("SELECT artum,artflagum FROM articoli WHERE artcod = ?", pars);
        if (c.moveToFirst()) {
            if (c.getString(0).equalsIgnoreCase("kg") || c.getInt(1) != 3)
                ris = true;
            else
                ris = false;
        }
        c.close();
        return ris;
    }

    public static boolean articoloEsistente(String artcod) {
        boolean ris = false;
        String[] pars = new String[1];
        pars[0] = artcod;
        Cursor c = Env.db.rawQuery("SELECT artcod FROM articoli WHERE artcod = ?", pars);
        if (c.moveToFirst()) {
            ris = true;
        }
        c.close();
        return ris;
    }

    public static ArrayList<Record> leggiMessaggi() {
        ArrayList<Record> ris = new ArrayList();
        Data oggi = new Data();
        String dtoggi = oggi.formatta(Data.AAAA_MM_GG, "-");
        SimpleDateFormat dh = new SimpleDateFormat("HH:mm");
        String ora = dh.format(new Date());
        Cursor c = Env.db.rawQuery("SELECT msgtesto,msgora FROM messaggi WHERE (msgdatainizio = '0000-00-00' OR msgdatainizio <= '" + dtoggi + "') AND " +
                "(msgdatafine = '0000-00-00' OR msgdatafine >= '" + dtoggi + "') ORDER BY msgdatainizio,msgdatafine,msgora", null);
        while (c.moveToNext()) {
            boolean msgok = true;
            if (!c.getString(1).equals("") && !c.getString(1).equals("00:00")) {
                if (ora.compareTo(c.getString(1)) < 0) {
                    msgok = false;
                } else {
                    Cursor cvis = Env.db.rawQuery("SELECT vmsgdata FROM vismessaggi WHERE vmsgdata = '" + dtoggi + "' AND vmsgora = '" + c.getString(1) + "'", null);
                    if (cvis.moveToFirst()) {
                        msgok = false;
                    }
                    cvis.close();
                }
            } else {
                Cursor cvis = Env.db.rawQuery("SELECT vmsgdata FROM vismessaggi WHERE vmsgdata = '" + dtoggi + "' AND vmsgora = '" + c.getString(1) + "'", null);
                if (cvis.moveToFirst()) {
                    msgok = false;
                }
                cvis.close();
            }
            if (msgok) {
                Record rx = new Record();
                rx.insStringa("testo", c.getString(0));
                rx.insStringa("ora", c.getString(1));
                ris.add(rx);
            }
        }
        c.close();
        return ris;

    }

    public static Record ultimoOrdineCliente(String datainizio, String datafine) {
        Record ris = null;
        Cursor cursor = Env.db.rawQuery("SELECT movdata,movnum,movclicod,clinome,movtotimp,movtotale,movdocora FROM movimenti " +
                "INNER JOIN clienti ON movclicod = clicodice WHERE movtipo = 20 " +
                "AND movdata >= '" + datainizio + "' AND movdata <= '" + datafine + "' " +
                "ORDER BY movdata DESC,movnum DESC LIMIT 1", null);
        if (cursor.moveToFirst()) {
            ris = new Record();
            ris.insStringa("data", cursor.getString(0));
            ris.insStringa("ora", cursor.getString(6));
            ris.insIntero("numero", cursor.getInt(1));
            ris.insStringa("clicod", cursor.getString(2));
            ris.insStringa("clinome", cursor.getString(3));
            ris.insDouble("imponibile", cursor.getDouble(4));
            ris.insDouble("totale", cursor.getDouble(5));
        }
        cursor.close();
        return ris;
    }

    public static Record ultimoDDTVendita(String datainizio, String datafine) {
        Record ris = null;
        Cursor cursor = Env.db.rawQuery("SELECT movdata,movnum,movclicod,clinome,movtotimp,movtotale,movdocora " +
                "FROM movimenti INNER JOIN clienti ON movclicod = clicodice WHERE movtipo IN (0,4) " +
                "AND movdata >= '" + datainizio + "' AND movdata <= '" + datafine + "' " +
                "ORDER BY movdata DESC,movnum DESC LIMIT 1", null);
        if (cursor.moveToFirst()) {
            ris = new Record();
            ris.insStringa("data", cursor.getString(0));
            ris.insStringa("ora", cursor.getString(6));
            ris.insIntero("numero", cursor.getInt(1));
            ris.insStringa("clicod", cursor.getString(2));
            ris.insStringa("clinome", cursor.getString(3));
            ris.insDouble("imponibile", cursor.getDouble(4));
            ris.insDouble("totale", cursor.getDouble(5));
        }
        cursor.close();
        return ris;
    }

    public static Record ultimoDDTQuantita(String datainizio, String datafine) {
        Record ris = null;
        Cursor cursor = Env.db.rawQuery("SELECT movdata,movnum,movclicod,clinome,movtotimp,movtotale,movdocora FROM movimenti INNER JOIN clienti " +
                "ON movclicod = clicodice WHERE movtipo = 1 " +
                "AND movdata >= '" + datainizio + "' AND movdata <= '" + datafine + "' " +
                "ORDER BY movdata DESC,movnum DESC LIMIT 1", null);
        if (cursor.moveToFirst()) {
            ris = new Record();
            ris.insStringa("data", cursor.getString(0));
            ris.insStringa("ora", cursor.getString(6));
            ris.insIntero("numero", cursor.getInt(1));
            ris.insStringa("clicod", cursor.getString(2));
            ris.insStringa("clinome", cursor.getString(3));
            ris.insDouble("imponibile", cursor.getDouble(4));
            ris.insDouble("totale", cursor.getDouble(5));
        }
        cursor.close();
        return ris;
    }

    public static Record ultimaFattura(String datainizio, String datafine) {
        Record ris = null;
        Cursor cursor = Env.db.rawQuery("SELECT movdata,movnum,movclicod,clinome,movtotimp,movtotale,movdocora FROM movimenti " +
                "INNER JOIN clienti ON movclicod = clicodice WHERE movtipo = 2 " +
                "AND movdata >= '" + datainizio + "' AND movdata <= '" + datafine + "' " +
                " ORDER BY movdata DESC,movnum DESC LIMIT 1", null);
        if (cursor.moveToFirst()) {
            ris = new Record();
            ris.insStringa("data", cursor.getString(0));
            ris.insStringa("ora", cursor.getString(6));
            ris.insIntero("numero", cursor.getInt(1));
            ris.insStringa("clicod", cursor.getString(2));
            ris.insStringa("clinome", cursor.getString(3));
            ris.insDouble("imponibile", cursor.getDouble(4));
            ris.insDouble("totale", cursor.getDouble(5));
        }
        cursor.close();
        return ris;
    }

    public static double totaleOrdiniClienti(String datainizio, String datafine) {
        double ris = 0;
        Cursor cursor = Env.db.rawQuery("SELECT SUM(movtotimp) AS tot FROM movimenti WHERE movtipo = 20 AND movdata >= '" + datainizio + "' AND movdata <= '" + datafine + "'", null);
        if (cursor.moveToFirst() && !cursor.isNull(0)) {
            ris = cursor.getDouble(0);
        }
        cursor.close();
        return ris;
    }

    public static double totaleVenduto(String datainizio, String datafine) {
        double ris = 0;
        Cursor cursor = Env.db.rawQuery("SELECT SUM(movtotimp) AS tot FROM movimenti WHERE movtipo IN (0,2,3,4) AND movdata >= '" + datainizio + "' AND movdata <= '" + datafine + "'", null);
        if (cursor.moveToFirst() && !cursor.isNull(0)) {
            ris = cursor.getDouble(0);
        }
        cursor.close();
        return ris;
    }

    public static Record documentoImportoMax(String datainizio, String datafine) {
        Record ris = null;
        Cursor cursor = Env.db.rawQuery("SELECT movdata,movnum,movclicod,clinome,movtotimp,movtotale,movdocora,movtipo FROM movimenti " +
                "INNER JOIN clienti ON movclicod = clicodice WHERE movtipo IN (0,2,3) AND movdata >= '" + datainizio + "' AND movdata <= '" + datafine + "' ORDER BY movtotimp DESC LIMIT 1", null);
        if (cursor.moveToFirst()) {
            ris = new Record();
            if (cursor.getInt(7) == 0)
                ris.insStringa("tipo", "DDT");
            else if (cursor.getInt(7) == 2)
                ris.insStringa("tipo", "FATTURA");
            else
                ris.insStringa("tipo", "RIC.FISCALE");
            ris.insStringa("data", cursor.getString(0));
            ris.insStringa("ora", cursor.getString(6));
            ris.insIntero("numero", cursor.getInt(1));
            ris.insStringa("clicod", cursor.getString(2));
            ris.insStringa("clinome", cursor.getString(3));
            ris.insDouble("imponibile", cursor.getDouble(4));
            ris.insDouble("totale", cursor.getDouble(5));
        }
        cursor.close();
        return ris;
    }

    public static ArrayList<Record> vendutoArticoli(String datainizio, String datafine, String filtrocliente, int tipoordine) {
        ArrayList<Record> ris = new ArrayList();
        TreeMap<String, Record> hart = new TreeMap();
        Cursor c = Env.db.rawQuery("SELECT rmartcod,artdescr,artum,rmcaumag,SUM(rmqta) AS tqta,SUM(rmnetto) AS tnetto FROM righemov " +
                " INNER JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez " +
                "and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum) " +
                " INNER JOIN articoli ON rmartcod = artcod " +
                " WHERE rmmovtipo IN (0,2,3,4) AND rmmovdata >= '" + datainizio + "' AND rmmovdata <= '" + datafine + "' " +
                (!filtrocliente.equals("") ? " AND movclicod = '" + filtrocliente + "'" : "") +
                " GROUP BY rmartcod,rmcaumag", null);
        while (c.moveToNext()) {
            String artcod = c.getString(0);
            String artdescr = c.getString(1);
            String artum = c.getString(2);
            String cau = c.getString(3);
            double q = Math.abs(c.getDouble(4));
            double v = Math.abs(c.getDouble(5));
            double precq = 0, precv = 0;
            Record r = hart.get(artcod);
            if (r != null) {
                precq = r.leggiDouble("qta");
                precv = r.leggiDouble("val");
            }
            if (cau.equals("VE")) {
                precq += q;
                precv += v;
            } else if (cau.equals("OM") || cau.equals("OT") || cau.equals("SM")) {
                precq += q;
            }
            if (cau.equals("RV") || cau.equals("RN")) {
                precq -= q;
                precv -= v;
            }
            if (r != null) {
                r.eliminaCampo("qta");
                r.eliminaCampo("val");
                r.insDouble("qta", OpValute.arrotondaMat(precq, 3));
                r.insDouble("val", OpValute.arrotondaMat(precv, 2));
            } else {
                r = new Record();
                r.insStringa("artcod", artcod);
                r.insStringa("artdescr", artdescr);
                r.insStringa("artum", artum);
                r.insDouble("qta", OpValute.arrotondaMat(precq, 3));
                r.insDouble("val", OpValute.arrotondaMat(precv, 2));
                hart.put(artcod, r);
            }
        }
        c.close();
        if (tipoordine == 0) {
            // ordine per codice articolo
            while (hart.size() > 0) {
                String k = hart.firstKey();
                Record r = hart.get(k);
                ris.add(r);
                hart.remove(k);
            }
        } else if (tipoordine == 1) {
            // ordine per qta discendente
            while (hart.size() > 0) {
                String k = hart.firstKey();
                Record r = hart.get(k);
                hart.remove(k);
                boolean ins = false;
                for (int i = 0; i < ris.size(); i++) {
                    Record r2 = ris.get(i);
                    if (r2.leggiDouble("qta") <= r.leggiDouble("qta")) {
                        ris.add(i, r);
                        ins = true;
                        break;
                    }
                }
                if (!ins)
                    ris.add(r);
            }
        } else if (tipoordine == 2) {
            // ordine per valore discendente
            while (hart.size() > 0) {
                String k = hart.firstKey();
                Record r = hart.get(k);
                hart.remove(k);
                boolean ins = false;
                for (int i = 0; i < ris.size(); i++) {
                    Record r2 = ris.get(i);
                    if (r2.leggiDouble("val") <= r.leggiDouble("val")) {
                        ris.add(i, r);
                        ins = true;
                        break;
                    }
                }
                if (!ins)
                    ris.add(r);
            }
        }
        double totval = 0;
        for (Record r : ris) {
            totval += r.leggiDouble("val");
        }
        totval = OpValute.arrotondaMat(totval, 2);
        for (Record r : ris) {
            double perc = 0;
            if (totval != 0)
                perc = r.leggiDouble("val") / totval * 100.0;
            r.insDouble("perctot", OpValute.arrotondaMat(perc, 2));
        }

        return ris;
    }

    public static ArrayList<Record> ordinatoArticoli(String datainizio, String datafine, String filtrocliente, int tipoordine) {
        ArrayList<Record> ris = new ArrayList();
        TreeMap<String, Record> hart = new TreeMap();
        Cursor c = Env.db.rawQuery("SELECT rmartcod,artdescr,artum,rmcaumag,SUM(rmqta) AS tqta,SUM(rmnetto) AS tnetto, SUM(rmcolli) As tcolli FROM righemov " +
                " INNER JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum) " +
                " INNER JOIN articoli ON rmartcod = artcod " +
                " WHERE rmmovtipo = 20 AND rmmovdata >= '" + datainizio + "' AND rmmovdata <= '" + datafine + "' " +
                (!filtrocliente.equals("") ? " AND movclicod = '" + filtrocliente + "'" : "") +
                " GROUP BY rmartcod,rmcaumag", null);
        while (c.moveToNext()) {
            String artcod = c.getString(0);
            String artdescr = c.getString(1);
            String artum = c.getString(2);
            String cau = c.getString(3);
            double q = Math.abs(c.getDouble(4));
            double v = Math.abs(c.getDouble(5));
            int colli = Math.abs(c.getInt(6));
            double precq = 0, precv = 0;
            Record r = hart.get(artcod);
            if (r != null) {
                precq = r.leggiDouble("qta");
                precv = r.leggiDouble("val");
            }
            if (cau.equals("VE")) {
                precq += q;
                precv += v;
            } else if (cau.equals("OM") || cau.equals("OT") || cau.equals("SM")) {
                precq += q;
            }
            if (cau.equals("RV") || cau.equals("RN")) {
                precq -= q;
                precv -= v;
            }
            if (r != null) {
                r.eliminaCampo("qta");
                r.eliminaCampo("val");
                r.insDouble("qta", OpValute.arrotondaMat(precq, 3));
                r.insDouble("val", OpValute.arrotondaMat(precv, 2));
            } else {
                r = new Record();
                r.insStringa("artcod", artcod);
                r.insStringa("artdescr", artdescr);
                r.insStringa("artum", artum);
                r.insIntero("artcolli", colli);
                r.insDouble("qta", OpValute.arrotondaMat(q, 3));
                r.insDouble("val", OpValute.arrotondaMat(v, 2));
                hart.put(artcod, r);
            }
        }
        c.close();
        if (tipoordine == 0) {
            // ordine per codice articolo
            while (hart.size() > 0) {
                String k = hart.firstKey();
                Record r = hart.get(k);
                ris.add(r);
                hart.remove(k);
            }
        } else if (tipoordine == 1) {
            // ordine per qta discendente
            while (hart.size() > 0) {
                String k = hart.firstKey();
                Record r = hart.get(k);
                hart.remove(k);
                boolean ins = false;
                for (int i = 0; i < ris.size(); i++) {
                    Record r2 = ris.get(i);
                    if (r2.leggiDouble("qta") <= r.leggiDouble("qta")) {
                        ris.add(i, r);
                        ins = true;
                        break;
                    }
                }
                if (!ins)
                    ris.add(r);
            }
        } else if (tipoordine == 2) {
            // ordine per valore discendente
            while (hart.size() > 0) {
                String k = hart.firstKey();
                Record r = hart.get(k);
                hart.remove(k);
                boolean ins = false;
                for (int i = 0; i < ris.size(); i++) {
                    Record r2 = ris.get(i);
                    if (r2.leggiDouble("val") <= r.leggiDouble("val")) {
                        ris.add(i, r);
                        ins = true;
                        break;
                    }
                }
                if (!ins)
                    ris.add(r);
            }
        }
        double totval = 0;
        for (Record r : ris) {
            totval += r.leggiDouble("val");
        }
        totval = OpValute.arrotondaMat(totval, 2);
        for (Record r : ris) {
            double perc = 0;
            if (totval != 0)
                perc = r.leggiDouble("val") / totval * 100.0;
            r.insDouble("perctot", OpValute.arrotondaMat(perc, 2));
        }

        return ris;
    }

    public static ArrayList<Record> vendutoMarca(String datainizio, String datafine, int tipoordine) {
        ArrayList<Record> ris = new ArrayList();
        TreeMap<String, Record> hmarche = new TreeMap();
        Cursor c = Env.db.rawQuery("SELECT marcacod,marcadescr,rmcaumag,SUM(rmqta) AS tqta,SUM(rmnetto) AS tnetto FROM righemov INNER JOIN articoli ON rmartcod = artcod " +
                " LEDFT JOIN marche ON articoli.artmarcacod = marche.marcacod WHERE rmmovtipo IN (0,2,3,4) AND rmmovdata >= '" + datainizio + "' AND rmmovdata <= '" + datafine + "' GROUP BY marcacod,marcadescr,rmcaumag", null);
        while (c.moveToNext()) {
            String mcod = c.getString(0);
            String mdescr = c.getString(1);
            String cau = c.getString(2);
            double q = Math.abs(c.getDouble(3));
            double v = Math.abs(c.getDouble(4));
            double precq = 0, precv = 0;
            Record r = hmarche.get(mcod);
            if (r != null) {
                precq = r.leggiDouble("qta");
                precv = r.leggiDouble("val");
            }
            if (cau.equals("VE")) {
                precq += q;
                precv += v;
            } else if (cau.equals("OM") || cau.equals("OT") || cau.equals("SM")) {
                precq += q;
            }
            if (cau.equals("RV") || cau.equals("RN")) {
                precq -= q;
                precv -= v;
            }
            if (r != null) {
                r.eliminaCampo("qta");
                r.eliminaCampo("val");
                r.insDouble("qta", OpValute.arrotondaMat(precq, 3));
                r.insDouble("val", OpValute.arrotondaMat(precv, 2));
            } else {
                r = new Record();
                r.insStringa("marcacod", mcod);
                r.insStringa("marcadescr", mdescr);
                r.insDouble("qta", OpValute.arrotondaMat(precq, 3));
                r.insDouble("val", OpValute.arrotondaMat(precv, 2));
                hmarche.put(mcod, r);
            }
        }
        c.close();
        if (tipoordine == 0) {
            // ordine per codice marca
            while (hmarche.size() > 0) {
                String k = hmarche.firstKey();
                Record r = hmarche.get(k);
                ris.add(r);
                hmarche.remove(k);
            }
        } else if (tipoordine == 1) {
            // ordine per qta discendente
            while (hmarche.size() > 0) {
                String k = hmarche.firstKey();
                Record r = hmarche.get(k);
                hmarche.remove(k);
                boolean ins = false;
                for (int i = 0; i < ris.size(); i++) {
                    Record r2 = ris.get(i);
                    if (r2.leggiDouble("qta") <= r.leggiDouble("qta")) {
                        ris.add(i, r);
                        ins = true;
                        break;
                    }
                }
                if (!ins)
                    ris.add(r);
            }
        } else if (tipoordine == 2) {
            // ordine per valore discendente
            while (hmarche.size() > 0) {
                String k = hmarche.firstKey();
                Record r = hmarche.get(k);
                hmarche.remove(k);
                boolean ins = false;
                for (int i = 0; i < ris.size(); i++) {
                    Record r2 = ris.get(i);
                    if (r2.leggiDouble("val") <= r.leggiDouble("val")) {
                        ris.add(i, r);
                        ins = true;
                        break;
                    }
                }
                if (!ins)
                    ris.add(r);
            }
        }
        double totval = 0;
        for (Record r : ris) {
            totval += r.leggiDouble("val");
        }
        totval = OpValute.arrotondaMat(totval, 2);
        for (Record r : ris) {
            double perc = 0;
            if (totval != 0)
                perc = r.leggiDouble("val") / totval * 100.0;
            r.insDouble("perctot", OpValute.arrotondaMat(perc, 2));
        }
        return ris;
    }

    public static ArrayList<Record> vendutoSottoclasse(String datainizio, String datafine, int tipoordine) {
        ArrayList<Record> ris = new ArrayList();
        TreeMap<String, Record> hsc = new TreeMap();
        Cursor c = Env.db.rawQuery("SELECT clcod,sccod,scdescr,rmcaumag,SUM(rmqta) AS tqta,SUM(rmnetto) AS tnetto FROM righemov INNER JOIN articoli ON rmartcod = artcod " +
                " LEFT JOIN sottoclassi ON (articoli.artclassecod = sottoclassi.clcod AND articoli.artsottoclassecod = sottoclassi.sccod) WHERE rmmovtipo IN (0,2,3,4) AND rmmovdata >= '" + datainizio + "' AND rmmovdata <= '" + datafine + "' GROUP BY clcod,sccod,rmcaumag", null);
        while (c.moveToNext()) {
            String clcod = c.getString(0);
            String sccod = c.getString(1);
            String descr = c.getString(2);
            if (clcod == null) {
                clcod = "-";
                sccod = "-";
                descr = "<NON CLASSIFICATI>";
            }
            String cau = c.getString(3);
            double q = Math.abs(c.getDouble(4));
            double v = Math.abs(c.getDouble(5));
            double precq = 0, precv = 0;
            Record r = hsc.get(clcod + sccod);
            if (r != null) {
                precq = r.leggiDouble("qta");
                precv = r.leggiDouble("val");
            }
            if (cau.equals("VE")) {
                precq += q;
                precv += v;
            } else if (cau.equals("OM") || cau.equals("OT") || cau.equals("SM")) {
                precq += q;
            }
            if (cau.equals("RV") || cau.equals("RN")) {
                precq -= q;
                precv -= v;
            }
            if (r != null) {
                r.eliminaCampo("qta");
                r.eliminaCampo("val");
                r.insDouble("qta", OpValute.arrotondaMat(precq, 3));
                r.insDouble("val", OpValute.arrotondaMat(precv, 2));
            } else {
                r = new Record();
                r.insStringa("clcod", clcod);
                r.insStringa("sccod", sccod);
                r.insStringa("descr", descr);
                r.insDouble("qta", OpValute.arrotondaMat(precq, 3));
                r.insDouble("val", OpValute.arrotondaMat(precv, 2));
                hsc.put(clcod + sccod, r);
            }
        }
        c.close();
        if (tipoordine == 0) {
            // ordine per codice
            while (hsc.size() > 0) {
                String k = hsc.firstKey();
                Record r = hsc.get(k);
                ris.add(r);
                hsc.remove(k);
            }
        } else if (tipoordine == 1) {
            // ordine per qta discendente
            while (hsc.size() > 0) {
                String k = hsc.firstKey();
                Record r = hsc.get(k);
                hsc.remove(k);
                boolean ins = false;
                for (int i = 0; i < ris.size(); i++) {
                    Record r2 = ris.get(i);
                    if (r2.leggiDouble("qta") <= r.leggiDouble("qta")) {
                        ris.add(i, r);
                        ins = true;
                        break;
                    }
                }
                if (!ins)
                    ris.add(r);
            }
        } else if (tipoordine == 2) {
            // ordine per valore discendente
            while (hsc.size() > 0) {
                String k = hsc.firstKey();
                Record r = hsc.get(k);
                hsc.remove(k);
                boolean ins = false;
                for (int i = 0; i < ris.size(); i++) {
                    Record r2 = ris.get(i);
                    if (r2.leggiDouble("val") <= r.leggiDouble("val")) {
                        ris.add(i, r);
                        ins = true;
                        break;
                    }
                }
                if (!ins)
                    ris.add(r);
            }
        }
        double totval = 0;
        for (Record r : ris) {
            totval += r.leggiDouble("val");
        }
        totval = OpValute.arrotondaMat(totval, 2);
        for (Record r : ris) {
            double perc = 0;
            if (totval != 0)
                perc = r.leggiDouble("val") / totval * 100.0;
            r.insDouble("perctot", OpValute.arrotondaMat(perc, 2));
        }
        return ris;
    }

    public static ArrayList<Record> ordinatoSottoclasse(String datainizio, String datafine, int tipoordine) {
        ArrayList<Record> ris = new ArrayList();
        TreeMap<String, Record> hsc = new TreeMap();
        Cursor c = Env.db.rawQuery("SELECT clcod,sccod,scdescr,rmcaumag,SUM(rmqta) AS tqta,SUM(rmnetto) AS tnetto FROM righemov INNER JOIN articoli ON rmartcod = artcod " +
                " LEFT JOIN sottoclassi ON (articoli.artclassecod = sottoclassi.clcod AND articoli.artsottoclassecod = sottoclassi.sccod) WHERE rmmovtipo = 20 AND rmmovdata >= '" + datainizio + "' AND rmmovdata <= '" + datafine + "' GROUP BY clcod,sccod,rmcaumag", null);
        while (c.moveToNext()) {
            String clcod = c.getString(0);
            String sccod = c.getString(1);
            String descr = c.getString(2);
            if (clcod == null) {
                clcod = "-";
                sccod = "-";
                descr = "<NON CLASSIFICATI>";
            }
            String cau = c.getString(3);
            double q = Math.abs(c.getDouble(4));
            double v = Math.abs(c.getDouble(5));
            double precq = 0, precv = 0;
            Record r = hsc.get(clcod + sccod);
            if (r != null) {
                precq = r.leggiDouble("qta");
                precv = r.leggiDouble("val");
            }
            if (cau.equals("VE")) {
                precq += q;
                precv += v;
            } else if (cau.equals("OM") || cau.equals("OT") || cau.equals("SM")) {
                precq += q;
            }
            if (cau.equals("RV") || cau.equals("RN")) {
                precq -= q;
                precv -= v;
            }
            if (r != null) {
                r.eliminaCampo("qta");
                r.eliminaCampo("val");
                r.insDouble("qta", OpValute.arrotondaMat(precq, 3));
                r.insDouble("val", OpValute.arrotondaMat(precv, 2));
            } else {
                r = new Record();
                r.insStringa("clcod", clcod);
                r.insStringa("sccod", sccod);
                r.insStringa("descr", descr);
                r.insDouble("qta", OpValute.arrotondaMat(q, 3));
                r.insDouble("val", OpValute.arrotondaMat(v, 2));
                hsc.put(clcod + sccod, r);
            }
        }
        c.close();
        if (tipoordine == 0) {
            // ordine per codice
            while (hsc.size() > 0) {
                String k = hsc.firstKey();
                Record r = hsc.get(k);
                ris.add(r);
                hsc.remove(k);
            }
        } else if (tipoordine == 1) {
            // ordine per qta discendente
            while (hsc.size() > 0) {
                String k = hsc.firstKey();
                Record r = hsc.get(k);
                hsc.remove(k);
                boolean ins = false;
                for (int i = 0; i < ris.size(); i++) {
                    Record r2 = ris.get(i);
                    if (r2.leggiDouble("qta") <= r.leggiDouble("qta")) {
                        ris.add(i, r);
                        ins = true;
                        break;
                    }
                }
                if (!ins)
                    ris.add(r);
            }
        } else if (tipoordine == 2) {
            // ordine per valore discendente
            while (hsc.size() > 0) {
                String k = hsc.firstKey();
                Record r = hsc.get(k);
                hsc.remove(k);
                boolean ins = false;
                for (int i = 0; i < ris.size(); i++) {
                    Record r2 = ris.get(i);
                    if (r2.leggiDouble("val") <= r.leggiDouble("val")) {
                        ris.add(i, r);
                        ins = true;
                        break;
                    }
                }
                if (!ins)
                    ris.add(r);
            }
        }
        double totval = 0;
        for (Record r : ris) {
            totval += r.leggiDouble("val");
        }
        totval = OpValute.arrotondaMat(totval, 2);
        for (Record r : ris) {
            double perc = 0;
            if (totval != 0)
                perc = r.leggiDouble("val") / totval * 100.0;
            r.insDouble("perctot", OpValute.arrotondaMat(perc, 2));
        }
        return ris;
    }

    public static ArrayList<Record> vendutoClienti(String datainizio, String datafine, int tipoordine) {
        ArrayList<Record> ris = new ArrayList();
        TreeMap<String, Record> hcli = new TreeMap();
        Cursor c = Env.db.rawQuery("SELECT movclicod,clinome,rmcaumag,SUM(rmnetto) AS tnetto FROM righemov INNER JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum)" +
                " LEFT JOIN clienti ON movclicod = clicodice WHERE rmmovtipo IN (0,2,3,4) AND rmmovdata >= '" + datainizio + "' AND rmmovdata <= '" + datafine + "' GROUP BY movclicod,rmcaumag", null);
        while (c.moveToNext()) {
            String clicod = c.getString(0);
            String clinome = c.getString(1);
            String cau = c.getString(2);
            double v = Math.abs(c.getDouble(3));
            double precv = 0;
            Record r = hcli.get(clicod);
            if (r != null) {
                precv = r.leggiDouble("val");
            }
            if (cau.equals("VE")) {
                precv += v;
            } else if (cau.equals("OM") || cau.equals("OT") || cau.equals("SM")) {
            }
            if (cau.equals("RV") || cau.equals("RN")) {
                precv -= v;
            }
            if (r != null) {
                r.eliminaCampo("qta");
                r.eliminaCampo("val");
                r.insDouble("val", OpValute.arrotondaMat(precv, 2));
            } else {
                r = new Record();
                r.insStringa("clicod", clicod);
                r.insStringa("clinome", clinome);
                r.insDouble("val", OpValute.arrotondaMat(precv, 2));
                hcli.put(clicod, r);
            }
        }
        c.close();
        if (tipoordine == 0) {
            // ordine per codice cliente
            while (hcli.size() > 0) {
                String k = hcli.firstKey();
                Record r = hcli.get(k);
                ris.add(r);
                hcli.remove(k);
            }
        } else if (tipoordine == 1) {
            // ordine per valore discendente
            while (hcli.size() > 0) {
                String k = hcli.firstKey();
                Record r = hcli.get(k);
                hcli.remove(k);
                boolean ins = false;
                for (int i = 0; i < ris.size(); i++) {
                    Record r2 = ris.get(i);
                    if (r2.leggiDouble("val") <= r.leggiDouble("val")) {
                        ris.add(i, r);
                        ins = true;
                        break;
                    }
                }
                if (!ins)
                    ris.add(r);
            }
        }
        double totval = 0;
        for (Record r : ris) {
            totval += r.leggiDouble("val");
        }
        totval = OpValute.arrotondaMat(totval, 2);
        for (Record r : ris) {
            double perc = 0;
            if (totval != 0)
                perc = r.leggiDouble("val") / totval * 100.0;
            r.insDouble("perctot", OpValute.arrotondaMat(perc, 2));
        }

        return ris;
    }

    public static ArrayList<Record> ordinatoClienti(String datainizio, String datafine, int tipoordine) {
        ArrayList<Record> ris = new ArrayList();
        TreeMap<String, Record> hcli = new TreeMap();
        Cursor c = Env.db.rawQuery("SELECT movclicod,clinome,rmcaumag,SUM(rmnetto) AS tnetto FROM righemov INNER JOIN movimenti ON (righemov.rmmovtipo = movimenti.movtipo AND righemov.rmmovdoc = movimenti.movdoc and righemov.rmmovsez = movimenti.movsez and righemov.rmmovdata = movimenti.movdata and righemov.rmmovnum = movimenti.movnum)" +
                " LEFT JOIN clienti ON movclicod = clicodice WHERE rmmovtipo = 20 AND rmmovdata >= '" + datainizio + "' AND rmmovdata <= '" + datafine + "' GROUP BY movclicod,rmcaumag", null);
        while (c.moveToNext()) {
            String clicod = c.getString(0);
            String clinome = c.getString(1);
            String cau = c.getString(2);
            double v = Math.abs(c.getDouble(3));
            double precv = 0;
            Record r = hcli.get(clicod);
            if (r != null) {
                precv = r.leggiDouble("val");
            }
            if (cau.equals("VE")) {
                precv += v;
            } else if (cau.equals("OM") || cau.equals("OT") || cau.equals("SM")) {
            }
            if (cau.equals("RV") || cau.equals("RN")) {
                precv -= v;
            }
            if (r != null) {
                r.eliminaCampo("qta");
                r.eliminaCampo("val");
                r.insDouble("val", OpValute.arrotondaMat(precv, 2));
            } else {
                r = new Record();
                r.insStringa("clicod", clicod);
                r.insStringa("clinome", clinome);
                r.insDouble("val", OpValute.arrotondaMat(v, 2));
                hcli.put(clicod, r);
            }
        }
        c.close();
        if (tipoordine == 0) {
            // ordine per codice cliente
            while (hcli.size() > 0) {
                String k = hcli.firstKey();
                Record r = hcli.get(k);
                ris.add(r);
                hcli.remove(k);
            }
        } else if (tipoordine == 1) {
            // ordine per valore discendente
            while (hcli.size() > 0) {
                String k = hcli.firstKey();
                Record r = hcli.get(k);
                hcli.remove(k);
                boolean ins = false;
                for (int i = 0; i < ris.size(); i++) {
                    Record r2 = ris.get(i);
                    if (r2.leggiDouble("val") <= r.leggiDouble("val")) {
                        ris.add(i, r);
                        ins = true;
                        break;
                    }
                }
                if (!ins)
                    ris.add(r);
            }
        }
        double totval = 0;
        for (Record r : ris) {
            totval += r.leggiDouble("val");
        }
        totval = OpValute.arrotondaMat(totval, 2);
        for (Record r : ris) {
            double perc = 0;
            if (totval != 0)
                perc = r.leggiDouble("val") / totval * 100.0;
            r.insDouble("perctot", OpValute.arrotondaMat(perc, 2));
        }

        return ris;
    }

    public static double giacenzaArticoloJCloud(String artcod) {
        double ris = 0;
        try {
            Cursor cursor = Env.db.rawQuery(
                    "SELECT pedcod,user,pwd,serverjcloud FROM datiterm",
                    null);
            cursor.moveToFirst();
            String user = cursor.getString(1);
            String pwd = cursor.getString(2);
            String termcod = cursor.getString(0);
            String server = cursor.getString(3);
            cursor.close();
            String content = "";
            JSONObject req = new JSONObject();
            req.put("user", user);
            req.put("pwd", pwd);
            req.put("codpost", termcod);
            req.put("cmd", "GETITEMSTOCK");
            req.put("artcod", artcod);
            req.put("depcod", Env.depgiac);
            content = req.toString();
            BufferedReader in = FunzioniHTTP.inviaPostHttp(server + "/service/termservices.json",
                    content, "application/json", null, new Hashtable(), false, false, false, "", "", 1000);
            String sjsonobj = "";
            if (in != null) {
                String s = in.readLine();
                if (s != null)
                    sjsonobj += s;
                in.close();
            }
            if (!sjsonobj.equals("")) {
                JSONObject risp = new JSONObject(sjsonobj);
                if (risp.get("esito").equals("OK")) {
                    ris = ((Double) risp.get("giacenza")).doubleValue();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static ArrayList<HashMap> giacenzeJCloud() {
        ArrayList<HashMap> ris = new ArrayList();
        try {
            Cursor cursor = Env.db.rawQuery(
                    "SELECT pedcod,user,pwd,serverjcloud FROM datiterm",
                    null);
            cursor.moveToFirst();
            String user = cursor.getString(1);
            String pwd = cursor.getString(2);
            String termcod = cursor.getString(0);
            String server = cursor.getString(3);
            cursor.close();
            String content = "";
            JSONObject req = new JSONObject();
            req.put("user", user);
            req.put("pwd", pwd);
            req.put("codpost", termcod);
            req.put("cmd", "GETITEMSSTOCK");
            req.put("depcod", Env.depgiac);
            //           req.put("depcod", "sede");
            content = req.toString();
            BufferedReader in = FunzioniHTTP.inviaPostHttp(server + "/service/termservices.json",
                    content, "application/json", null, new Hashtable(), false, false, false, "", "", 1000);
            String sjsonobj = "";
            if (in != null) {
                String s = in.readLine();
                if (s != null)
                    sjsonobj += s;
                in.close();
            }
            if (!sjsonobj.equals("")) {
                //Log.v("jorders", "json giacenze:" + sjsonobj.length());
                //Log.v("jorders", sjsonobj);
                JSONObject risp = new JSONObject(sjsonobj);
                if (risp.get("esito").equals("OK")) {
                    JSONArray risp2 = risp.getJSONArray("risposta");
                    if (risp2 != null) {
                        for (int i = 0; i < risp2.length(); i++) {
                            HashMap elem = new HashMap();
                            JSONObject jsonelem = risp2.getJSONObject(i);
                            elem.put("artcod", jsonelem.get("artcod"));
                            elem.put("qta", jsonelem.get("qta"));
                            ris.add(elem);
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            ris = null;
        }
        return ris;
    }

    public static double giacenzaArticoloLocale(String artcod) {
        double ris = 0;
        try {
            Cursor cursor = Env.db.rawQuery(
                    "SELECT giacqta FROM giacart WHERE giacartcod='" + artcod + "'",
                    null);
            if (cursor.moveToFirst())
                ris = cursor.getDouble(0);
        } catch (Exception e) {
            e.printStackTrace();
            ris = 0;
        }
        return ris;
    }

    public static HashMap parametriOrdini(String user, String pwd, String termcod, String server) {
        HashMap ris = new HashMap();
        try {
            String content = "";
            JSONObject req = new JSONObject();
            req.put("user", user);
            req.put("pwd", pwd);
            req.put("codpost", termcod);
            req.put("cmd", "GETAGEORDPARS");
            content = req.toString();
            BufferedReader in = FunzioniHTTP.inviaPostHttp(server + "/service/termservices.json",
                    content, "application/json", null, new Hashtable(), false, false, false, "", "", 1000);
            String sjsonobj = "";
            if (in != null) {
                String s = in.readLine();
                if (s != null)
                    sjsonobj += s;
                in.close();
            }
            if (!sjsonobj.equals("")) {
                JSONObject risp = new JSONObject(sjsonobj);
                if (risp.get("esito").equals("OK")) {
                    JSONObject risp2 = risp.getJSONObject("risposta");
                    if (risp2 != null) {
                        HashMap elem = new HashMap();
                        elem.put("visprzsconti", (String) risp2.get("visprzsconti"));
                        elem.put("modprz", (String) risp2.get("modprz"));
                        elem.put("modsconti", (String) risp2.get("modsconti"));
                        elem.put("scontiattivi", (Integer) risp2.get("scontiattivi"));
                        if (risp2.get("minmargine") != null)
                            elem.put("minmargine", (Double) risp2.get("minmargine"));
                        else
                            elem.put("minmargine", new Double(0));
                        if (risp2.get("depgiac") != null)
                            elem.put("depgiac", (String) risp2.get("depgiac"));
                        else
                            elem.put("depgiac", "");
                        if (risp2.get("prezzodapref") != null)
                            elem.put("prezzodapref", (String) risp2.get("prezzodapref"));
                        else
                            elem.put("prezzodapref", "N");
                        ris = elem;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static void invioEMailDocumento(Activity activity, Context context, ArrayList<Record> vdocs, String email_destinatario) {
        //File fx = new File("/storage/emulated/0/Android/data/jsoftware.jbeerapp");
        File fx = new File(activity.getFilesDir() + File.separator + "tmp");
        if (!fx.exists()) {
            fx.mkdir();
        } else {
            String deleteCmd = "rm -r " + activity.getFilesDir() + File.separator + "tmp";
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(deleteCmd);
            } catch (IOException e) {
            }
            fx.mkdir();
        }

        ArrayList<String> vfiles = new ArrayList();
        boolean pdfok = true;
        for (int i = 0; i < vdocs.size(); i++) {
            Record rdoc = vdocs.get(i);
            String nomefile = rdoc.leggiStringa("doc") + "_" + rdoc.leggiIntero("num") + "_" + rdoc.leggiStringa("data") + ".pdf";
            nomefile = nomefile.replace("-", "_");
            vfiles.add(nomefile);
            boolean ok = FunzioniJBeerApp.generaPDFDocumento(rdoc.leggiStringa("doc"), rdoc.leggiIntero("num"), rdoc.leggiStringa("data"), context, fx + File.separator + nomefile);
            //boolean ok = FunzioniJBeerApp.generaPDFDocumento(rdoc.leggiStringa("doc"), rdoc.leggiIntero("num"), rdoc.leggiStringa("data"), context, "/storage/emulated/0/Android/data/jsoftware.jbeerapp/" + nomefile);
            if (!ok)
                pdfok = false;
        }
        if (pdfok) {
            String soggettomail = "";
            String testomail = "";
            ArrayList<String> testomail1 = new ArrayList<String>();

            for (int i = 0; i < vdocs.size(); i++) {
                Record rdoc = vdocs.get(i);
                if (rdoc.leggiStringa("doc").equals(Env.docbollaval)) {
                    soggettomail = "Invio DDT";
                    testomail += "In allegato il DDT n." + rdoc.leggiIntero("num") + " del " + (new Data(rdoc.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + "\r\n";
                } else if (rdoc.leggiStringa("doc").equals(Env.docbollaqta)) {
                    soggettomail = "Invio DDT";
                    testomail += "In allegato il DDT n." + rdoc.leggiIntero("num") + " del " + (new Data(rdoc.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + "\r\n";
                } else if (rdoc.leggiStringa("doc").equals(Env.docordinecli)) {
                    soggettomail = "Invio Ordine cliente";
                    testomail += "In allegato l'ordine cliente n." + rdoc.leggiIntero("num") + " del " + (new Data(rdoc.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + "\r\n";
                } else if (rdoc.leggiStringa("doc").equals(Env.docfattura)) {
                    soggettomail = "Invio Fattura";
                    testomail += "In allegato la fattura n." + rdoc.leggiIntero("num") + " del " + (new Data(rdoc.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + "\r\n";
                } else if (rdoc.leggiStringa("doc").equals(Env.doccorrisp)) {
                    soggettomail = "Invio Ricevuta fiscale";
                    testomail += "In allegato la ricevuta fiscale n." + rdoc.leggiIntero("num") + " del " + (new Data(rdoc.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + "\r\n";
                } else if (rdoc.leggiStringa("doc").equals(Env.docddtreso)) {
                    soggettomail = "Invio DDT di reso";
                    testomail += "In allegato il DDT di reso n." + rdoc.leggiIntero("num") + " del " + (new Data(rdoc.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + "\r\n";
                }
            }
            testomail += "\r\n";
            testomail += "\r\n";
            if (!Env.emailfirmariga1.equals(""))
                testomail += Env.emailfirmariga1 + "\r\n";
            if (!Env.emailfirmariga2.equals(""))
                testomail += Env.emailfirmariga2 + "\r\n";
            if (!Env.emailfirmariga3.equals(""))
                testomail += Env.emailfirmariga3 + "\r\n";
            String[] maildest = new String[1];
            if (email_destinatario != null)
                maildest[0] = email_destinatario;
            else
                maildest[0] = "";


            if (testomail != null)
                testomail1.add(testomail);
            else
                testomail1.add("");

            Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            emailIntent.putExtra(Intent.EXTRA_EMAIL, maildest);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, soggettomail);
            emailIntent.putStringArrayListExtra(Intent.EXTRA_TEXT, testomail1);
            emailIntent.setType("text/plain");

            ArrayList<Uri> uris = new ArrayList<Uri>();
            for (int i = 0; i < vfiles.size(); i++) {
                //  uris.add(Uri.parse("file:///storage/emulated/0/Android/data/jsoftware.jbeerapp/" + vfiles.get(i)));
                File newFile = new File(fx + File.separator + vfiles.get(i));
                Uri contentUri = FileProvider.getUriForFile(context, "jsoftware.jbeerapp.fileprovider", newFile);
                uris.add(contentUri);
            }
            emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            emailIntent.setType("message/rfc822");
            emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //context.startActivity(Intent.createChooser(emailIntent, "Sent mail"));

            try {
                context.startActivity(emailIntent);
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
            //activity.startActivity(emailIntent);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    activity);
            builder.setMessage("Errore durante la generazione del PDF")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });
            AlertDialog ad = builder.create();
            ad.setCancelable(false);
            ad.show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean generaPDFDocumentoCatalogo(ArrayList<Record> array, Context context) {
        boolean ris = true;
        try {

            Paint paint = new Paint();
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(0.5f);
            paint.setStyle(Paint.Style.STROKE);

            Paint paint1 = new Paint();
            paint1.setColor(Color.GRAY);
            paint1.setStrokeWidth(0.5f);
            paint1.setStyle(Paint.Style.STROKE);

            TextPaint textpaintetic = new TextPaint();
            textpaintetic.setColor(Color.BLACK);
            textpaintetic.setStyle(Paint.Style.FILL_AND_STROKE);
            textpaintetic.setTypeface(Typeface.SANS_SERIF);
            textpaintetic.setTextSize(13);
            textpaintetic.setTextAlign(Paint.Align.LEFT);

            TextPaint textpaintetic2 = new TextPaint();
            textpaintetic2.setColor(Color.BLACK);
            textpaintetic2.setStyle(Paint.Style.FILL);
            textpaintetic2.setTypeface(Typeface.SANS_SERIF);
            textpaintetic2.setTextSize(8);
            textpaintetic2.setTextAlign(Paint.Align.LEFT);

            TextPaint textpaintetic3 = new TextPaint();
            textpaintetic3.setColor(Color.BLACK);
            textpaintetic3.setStyle(Paint.Style.FILL);
            textpaintetic3.setTypeface(Typeface.create("SANS_SERIF", Typeface.BOLD));
            textpaintetic3.setTextSize(11);
            textpaintetic3.setTextAlign(Paint.Align.LEFT);

            TextPaint textpaintetic4 = new TextPaint();
            textpaintetic4.setColor(Color.BLACK);
            textpaintetic4.setStyle(Paint.Style.FILL_AND_STROKE);
            textpaintetic4.setTypeface(Typeface.SANS_SERIF);
            textpaintetic4.setTextSize(6);
            textpaintetic4.setTextAlign(Paint.Align.LEFT);

            PdfDocument document = new PdfDocument();
            int totpag = array.size() / 3;
            if (array.size() % 3 > 0)
                totpag++;


            for (int npag = 1; npag <= totpag; npag++) {
                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(595, 842, npag).create();
                PdfDocument.Page page = document.startPage(pageInfo);
                int altezzariga1 = 160;
                int altezzariga2 = 200;
                int altezzariga3 = 220;
                int altezzariga9 = 235;
                int altezzariga10 = 250;
                int altezzariga11 = 265;
                int altezzariga12 = 280;
                int altezzariga13 = 295;
                int altezzariga14 = 330;
                int altezzariga4 = 230;
                int altezzariga5 = 260;
                int altezzariga6 = 290;
                int altezzariga7 = 320;
                int altezzariga8 = 310;
                int altezzariga15 = 140;
                // sfondo fincature e logo
                // testata


                //File flogo = new File("storage/emulated/0/Android/data/jsoftware.jbeerapp/files/logoaz.png");
                File flogo = new File(context.getFilesDir() + File.separator + "logoaz.png");
                if (flogo.exists()) {
                    try {
                        Bitmap bm = BitmapFactory.decodeFile(flogo.getAbsolutePath());
                        double wbm = (double) bm.getWidth();
                        double hbm = (double) bm.getHeight();
                        double rapp = (double) bm.getWidth() / (double) bm.getHeight();
                        if (wbm > 170) {
                            wbm = 170;
                            hbm = wbm / rapp;
                        }
                        if (hbm > 80) {
                            hbm = 80;
                            wbm = hbm * rapp;
                        }
                        page.getCanvas().drawBitmap(bm, (Rect) null, new Rect(230, 22, 230 + (int) wbm, 50 + (int) hbm), paint);
                    } catch (Exception elogo) {
                        elogo.printStackTrace();
                    }
                }


                Cursor c = Env.db.rawQuery("SELECT intazriga1,intazriga2,intazriga3,intazriga4,intazriga5,intazriga6,intazriga7 FROM datiterm", null);
                String intestazione = "";
                while (c.moveToNext()) {
                    intestazione = c.getString(0) + " " + c.getString(1) + " " + c.getString(2) + " " + c.getString(3) + " " + c.getString(4) + " " + c.getString(5) + " " + c.getString(6);
                }
                c.close();
                // dati azienda
                page.getCanvas().drawText(intestazione, 30, 820, textpaintetic4);


                int ind = (npag - 1) * 3;
                for (int i = ind; i < ind + 3 && i < array.size(); i++) {


                    Record articolo = array.get(i);
                    //  File ff = context.getFileStreamPath(File.separator + articolo.leggiStringa("nomeimg"));
                    File ff = new File("storage/emulated/0/Android/data/jsoftware.jbeer/files/" + articolo.leggiStringa("nomeimg"));
                    if (ff.exists()) {
                        try {
                            //Bitmap bm = BitmapFactory.decodeFile("storage/emulated/0/Android/data/jsoftware.jbeer/files/" + articolo.leggiStringa("nomeimg"));
                            Bitmap bm = BitmapFactory.decodeFile(ff.getAbsolutePath());
                            double wbm = (double) bm.getWidth();
                            double hbm = (double) bm.getHeight();
                            double rapp = (double) bm.getWidth() / (double) bm.getHeight();
                            if (wbm > 230) {
                                wbm = 230;
                                hbm = wbm / rapp;
                            }
                            if (hbm > 140) {
                                hbm = 140;
                                wbm = hbm * rapp;
                            }
                            page.getCanvas().drawBitmap(bm, (Rect) null, new Rect(20, altezzariga15, 20 + (int) wbm, altezzariga15 + (int) hbm), paint);
                        } catch (Exception elogo) {
                            elogo.printStackTrace();
                        }
                    }
                    page.getCanvas().drawText(articolo.leggiStringa("catnomeart"), 150, altezzariga1, textpaintetic);
                    page.getCanvas().drawText(articolo.leggiStringa("prz"), 400, altezzariga1, textpaintetic);
                    page.getCanvas().drawText("Descrizione:", 150, altezzariga2, textpaintetic3);

                    String note = articolo.leggiStringa("catnote");
                    if (note.length() <= 50) {
                        page.getCanvas().drawText(note, 150, altezzariga3, textpaintetic2);
                    } else {
                        String primaparte = note.substring(0, 60);
                        int spazio = primaparte.lastIndexOf(" ");
                        page.getCanvas().drawText(note.substring(0, spazio), 150, altezzariga3, textpaintetic2); //1
                        String secondaparte = note.substring(spazio, note.length());
                        if (secondaparte.substring(0, 1).equals(" ")) {
                            secondaparte = secondaparte.substring(1, secondaparte.length());
                        }
                        if (secondaparte.length() > 60) {
                            String terzaparte = secondaparte.substring(0, 60);
                            int spazio3 = terzaparte.lastIndexOf(" ");
                            page.getCanvas().drawText(terzaparte.substring(0, spazio3), 150, altezzariga9, textpaintetic2); //2
                            String quartaparte = note.substring(spazio3 + spazio + 1, note.length());
                            if (quartaparte.substring(0, 1).equals(" ")) {
                                quartaparte = quartaparte.substring(1, quartaparte.length());
                            }
                            if (quartaparte.length() > 60) {
                                String quintaparte = quartaparte.substring(0, 60);
                                int spazio5 = quintaparte.lastIndexOf(" ");
                                page.getCanvas().drawText(quintaparte.substring(0, spazio5), 150, altezzariga10, textpaintetic2); //3
                                String sestaparte = note.substring(spazio5 + spazio3 + spazio + 2, note.length());
                                if (sestaparte.substring(0, 1).equals(" ")) {
                                    sestaparte = sestaparte.substring(1, sestaparte.length());
                                }
                                if (sestaparte.length() > 60) {
                                    String settimaparte = sestaparte.substring(0, 60);
                                    int spazio7 = settimaparte.lastIndexOf(" ");
                                    page.getCanvas().drawText(settimaparte.substring(0, spazio7), 150, altezzariga11, textpaintetic2); //4
                                    String ottavaparte = note.substring(spazio7 + spazio5 + spazio3 + spazio + 3, note.length());
                                    if (ottavaparte.substring(0, 1).equals(" ")) {
                                        ottavaparte = ottavaparte.substring(1, ottavaparte.length());
                                    }
                                    if (ottavaparte.length() > 60) {
                                        String nonaparte = ottavaparte.substring(0, 60);
                                        int spazio9 = nonaparte.lastIndexOf(" ");
                                        page.getCanvas().drawText(nonaparte.substring(0, spazio9), 150, altezzariga12, textpaintetic2); //5
                                        String decimaparte = note.substring(spazio9 + spazio7 + spazio5 + spazio3 + spazio + 4, note.length());
                                        if (decimaparte.substring(0, 1).equals(" ")) {
                                            decimaparte = decimaparte.substring(1, decimaparte.length());
                                        }
                                        if (decimaparte.length() > 60) {
                                            String undicesimaparte = decimaparte.substring(0, 60);
                                            int spazio11 = undicesimaparte.lastIndexOf(" ");
                                            page.getCanvas().drawText(undicesimaparte.substring(0, spazio11), 150, altezzariga13, textpaintetic2);
                                            String dodicesimaparte = note.substring(spazio11 + spazio9 + spazio7 + spazio5 + spazio3 + spazio + 5, note.length());
                                            if (dodicesimaparte.substring(0, 1).equals(" ")) {
                                                dodicesimaparte = dodicesimaparte.substring(1, dodicesimaparte.length());
                                            }
                                            if (dodicesimaparte.length() > 60) {
                                                String tredicesima = dodicesimaparte.substring(0, 60);
                                                int spazio13 = tredicesima.lastIndexOf(" ") + 300;
                                                page.getCanvas().drawText(tredicesima.substring(spazio11, spazio13), 150, altezzariga14, textpaintetic2);
                                                String quattordicesima = note.substring(spazio13 + spazio11 + spazio9 + spazio7 + spazio5 + spazio3 + spazio + 6, note.length());

                                            } else {
                                                page.getCanvas().drawText(dodicesimaparte, 150, altezzariga14, textpaintetic2);
                                            }
                                        } else {
                                            page.getCanvas().drawText(decimaparte, 150, altezzariga13, textpaintetic2);
                                        }
                                    } else {
                                        page.getCanvas().drawText(ottavaparte, 150, altezzariga12, textpaintetic2);
                                    }
                                } else {
                                    page.getCanvas().drawText(sestaparte, 150, altezzariga11, textpaintetic2);
                                }
                            } else {
                                page.getCanvas().drawText(quartaparte, 150, altezzariga10, textpaintetic2);
                            }
                        } else {
                            page.getCanvas().drawText(secondaparte, 150, altezzariga9, textpaintetic2);
                        }

                    }


                    page.getCanvas().drawText(articolo.leggiStringa("catstile"), 400, altezzariga2, textpaintetic2);
                    page.getCanvas().drawText(articolo.leggiStringa("catcolore"), 400, altezzariga4, textpaintetic2);
                    page.getCanvas().drawText(articolo.leggiStringa("catgrado"), 400, altezzariga5, textpaintetic2);
                    page.getCanvas().drawText("Formato: " + articolo.leggiStringa("cauz"), 400, altezzariga6, textpaintetic2);
                    page.getCanvas().drawText("Cod: " + articolo.leggiStringa("artcod"), 30, altezzariga8, textpaintetic2);
                    page.getCanvas().drawLine(30, altezzariga7, 565, altezzariga7, paint1);
                    altezzariga1 = altezzariga1 + 200;
                    altezzariga2 = altezzariga2 + 200;
                    altezzariga3 = altezzariga3 + 200;
                    altezzariga4 = altezzariga4 + 200;
                    altezzariga5 = altezzariga5 + 200;
                    altezzariga6 = altezzariga6 + 200;
                    altezzariga7 = altezzariga7 + 200;
                    altezzariga8 = altezzariga8 + 200;
                    altezzariga9 = altezzariga9 + 200;
                    altezzariga10 = altezzariga10 + 200;
                    altezzariga11 = altezzariga11 + 200;
                    altezzariga12 = altezzariga12 + 200;
                    altezzariga13 = altezzariga13 + 200;
                    altezzariga14 = altezzariga14 + 200;
                    altezzariga15 = altezzariga15 + 200;

                }
                document.finishPage(page);

            }

            File fd = new File(context.getFilesDir() + File.separator + "docs");
            if (!fd.exists())
                fd.mkdir();

            int BUFFER = 2048;
            byte data[] = new byte[BUFFER];
            FileOutputStream fosunzip = new FileOutputStream(context.getFilesDir() + File.separator + "docs" + File.separator + "catalogoBirre.pdf");
            BufferedOutputStream dest = new BufferedOutputStream(fosunzip, BUFFER);

//			document.writeTo(new FileOutputStream(nomefile));
            document.writeTo(fosunzip);
            document.close();


        } catch (Exception epdf) {
            epdf.printStackTrace();
            ris = false;
        }
        return ris;
    }

    public static int immaginiDaRicevere(String user, String pwd, String termcod, String server) {
        int ris = 0;
        try {
            String content = "";
            JSONObject req = new JSONObject();
            req.put("user", user);
            req.put("pwd", pwd);
            req.put("codpost", termcod);
            req.put("cmd", "GETIMAGES");
            content = req.toString();
            BufferedReader in = FunzioniHTTP.inviaPostHttp(server + "/service/termservices.json",
                    content, "application/json", null, new Hashtable(), false, false, false, "", "", 10000);
            String sjsonobj = "";
            if (in != null) {
                String s = in.readLine();
                if (s != null)
                    sjsonobj += s;
                in.close();
            }
            if (!sjsonobj.equals("")) {
                JSONObject risp = new JSONObject(sjsonobj);
                if (risp.get("esito").equals("OK")) {
                    JSONArray risp2 = risp.getJSONArray("risposta");
                    if (risp2 != null) {
                        JSONObject jsonelem = risp2.getJSONObject(0);
                        ris = ((Integer) jsonelem.get("num")).intValue();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static String leggiUmTassabileArticolo(String artcod) {
        String um = "";
        try {
            String[] pars = new String[1];
            pars[0] = artcod;
            Cursor cursor = Env.db.rawQuery(
                    "SELECT artaum FROM artaccisa WHERE artaartcod = ?", pars);
            if (cursor.moveToFirst())
                um = cursor.getString(0);
            cursor.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return um;
    }

    public static double leggiGradoAlcolicoArticolo(String artcod) {
        double g = 0;
        try {
            String[] pars = new String[1];
            pars[0] = artcod;
            Cursor cursor = Env.db.rawQuery(
                    "SELECT artagrado FROM artaccisa WHERE artaartcod = ?", pars);
            if (cursor.moveToFirst())
                g = cursor.getDouble(0);
            cursor.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return g;
    }

    public static double leggiGradoPlatoArticolo(String artcod, String lotto) {
        double g = 0;
        try {
            if (!Env.gestlotti) {
                String[] pars = new String[1];
                pars[0] = artcod;
                Cursor cursor = Env.db.rawQuery(
                        "SELECT artaplato FROM artaccisa WHERE artaartcod = ?", pars);
                if (cursor.moveToFirst())
                    g = cursor.getDouble(0);
                cursor.close();
            } else {
                String[] pars = new String[2];
                pars[0] = artcod;
                pars[1] = lotto;
                Cursor cursor = Env.db.rawQuery(
                        "SELECT giaclplato FROM giacartlotti WHERE giaclartcod = ? AND giacllotto = ?", pars);
                if (cursor.moveToFirst())
                    g = cursor.getDouble(0);
                cursor.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return g;
    }

    public static double calcoloAccisaAlcolici(String artcod, double qta, double gradoalc, double gradoplato,
                                               String naz, int tipoqta, String data) {
        double ris = 0;
        double contenuto = calcoloContenutoArticolo(artcod, qta);
        String[] pars = new String[1];
        pars[0] = artcod;
        try {
            String um = "";
            um = leggiUmTassabileArticolo(artcod);
            if (um.equals("")) {
                Cursor c = Env.db.rawQuery("SELECT artatipoacc FROM artaccisa WHERE artaartcod = ?", pars);
                if (c.moveToNext()) {
                    if (c.getInt(0) == 0) {
                        um = "li";
                    } else if (c.getInt(0) == 1) {
                        um = "kg";
                    } else if (c.getInt(0) == 2) {
                        um = "la";
                    } else if (c.getInt(0) == 3) {
                        um = "hlg";
                    }
                }
                c.close();
            }
            if (!um.equals("")) {
                double qtaimp = contenuto;
                if (um.equalsIgnoreCase("la") && tipoqta == LITRI_IDRATI) {
                    // trasformazione in litri anidri
                    qtaimp = OpValute.arrotondaMat(qtaimp * gradoalc / 100, 5);
                } else if (um.equalsIgnoreCase("li") && tipoqta == LITRI_ANIDRI) {
                    qtaimp = OpValute.arrotondaMat(qtaimp / gradoalc * 100, 5);
                } else if (um.equalsIgnoreCase("hlg")) {
                    qtaimp = calcoloEttogradi(qtaimp, gradoplato);
                    //qtaimp = 0;
                }
                // calcolo accisa da tabella nazioni
                boolean accnaz = false;
                {
                    boolean acctrovata = false;
                    int tipocalc = 0;
                    double accisa = 0;
                    double[] gradii = {0, 0, 0, 0, 0, 0};
                    double[] gradif = {0, 0, 0, 0, 0, 0};
                    double[] accisag = {0, 0, 0, 0, 0, 0};
                    Cursor c = Env.db.rawQuery(
                            "SELECT vatipocalc,vaaccisa,vagradiiniz1,vagradiiniz2,vagradiiniz3,vagradiiniz4,vagradiiniz5,vagradiiniz6," +
                                    "vagradifine1,vagradifine2,vagradifine3,vagradifine4,vagradifine5,vagradifine6," +
                                    "vaaccgradi1,vaaccgradi2,vaaccgradi3,vaaccgradi4,vaaccgradi5,vaaccgradi6 " +
                                    "FROM valaccisa WHERE vaartcod = '" + artcod + "' AND vanazione = '" + naz + "' AND vadataval <= '"
                                    + data + "' ORDER BY vadataval DESC", null);
                    if (c.moveToFirst()) {
                        tipocalc = c.getInt(0);
                        accisa = c.getDouble(1);
                        for (int i = 1; i <= 6; i++) {
                            gradii[i - 1] = c.getDouble(2 + i - 1);
                            gradif[i - 1] = c.getDouble(8 + i - 1);
                            accisag[i - 1] = c.getDouble(14 + i - 1);
                        }
                        acctrovata = true;
                    }
                    c.close();
                    if (!acctrovata) {
                        c = Env.db.rawQuery(
                                "SELECT vatipocalc,vaaccisa,vagradiiniz1,vagradiiniz2,vagradiiniz3,vagradiiniz4,vagradiiniz5,vagradiiniz6," +
                                        "vagradifine1,vagradifine2,vagradifine3,vagradifine4,vagradifine5,vagradifine6," +
                                        "vaaccgradi1,vaaccgradi2,vaaccgradi3,vaaccgradi4,vaaccgradi5,vaaccgradi6 " +
                                        " FROM valaccisa WHERE vaartcod = '" + artcod + "' AND vanazione = 'TT' AND vadataval <= '" + data
                                        + "' ORDER BY vadataval DESC", null);
                        if (c.moveToFirst()) {
                            tipocalc = c.getInt(0);
                            accisa = c.getDouble(1);
                            for (int i = 1; i <= 6; i++) {
                                gradii[i - 1] = c.getDouble(2 + i - 1);
                                gradif[i - 1] = c.getDouble(8 + i - 1);
                                accisag[i - 1] = c.getDouble(14 + i - 1);
                            }
                            acctrovata = true;
                        }
                        c.close();
                    }
                    if (acctrovata) {
                        // calcolo accisa
                        if (tipocalc == 0) {
                            // calcolo in base a quantita
                            ris = OpValute.arrotondaMat(qtaimp * accisa, 6);
                            accnaz = true;
                        } else {
                            // calcolo in base al grado alcolico
                            for (int j = 1; j <= 6; j++) {
                                if (gradoalc >= gradii[j - 1] && gradoalc <= gradif[j - 1]) {
                                    ris = OpValute.arrotondaMat(qtaimp * accisag[j - 1], 6);
                                    accnaz = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (!accnaz) {
                    Cursor crs = Env.db.rawQuery("SELECT artaaccisa FROM artaccisa WHERE artaartcod = ?", pars);
                    if (crs.moveToFirst())
                        ris = OpValute.arrotondaMat(qtaimp * crs.getDouble(0), 6);
                    crs.close();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ris;
    }

    public static double calcoloContenutoArticolo(String artcod, double qta) {
        double ris = 0;
        String[] pars = new String[1];
        pars[0] = artcod;
        double capacita = 1;
        try {
            Cursor c = Env.db.rawQuery("SELECT cauzcapacita FROM articoli INNER JOIN cauzioni ON articoli.artcauzcod = cauzcod WHERE artcod = ?", pars);
            if (c.moveToFirst() && c.getDouble(0) > 0) {
                capacita = c.getDouble(0);
            }
            c.close();
            ris = OpValute.arrotondaMat(qta * capacita, 3);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ris;
    }

    public static double calcoloEttolitri(double litri) {
        double hl = litri / 100;
        int dechl = 2;
        int arrotohl = 1;
        boolean pers = false;
        try {
            //pers = (leggiProprieta(azienda, "hgbirre", "calcpershg").equals("S"));
            //dechl = Formattazione.estraiIntero(leggiProprieta(azienda, "hgbirre", "dechl"));
            //arrotohl = Formattazione.estraiIntero(leggiProprieta(azienda, "hgbirre", "arrotohl"));
            pers = true;
            dechl = 2;
            arrotohl = 1;
            if (pers) {
                BigDecimal val;
                if (arrotohl == 0) {
                    val = new BigDecimal(Formattazione.formatta(hl, "#############0.0000", 0).replace(',', '.'));
                    val = val.setScale(dechl, BigDecimal.ROUND_HALF_DOWN);
                    hl = val.doubleValue();
                } else {
                    val = new BigDecimal(Formattazione.formatta(hl, "#############0.0000", 0).replace(',', '.'));
                    val = val.setScale(dechl, BigDecimal.ROUND_HALF_UP);
                    hl = val.doubleValue();
                }
            } else {
                hl = litri / 100;
                if (OpValute.arrotondaMat(hl, 3) != OpValute.arrotondaMat(hl, 4)) {
                    hl = OpValute.arrotondaMat(hl + 0.0005, 3);
                }
                BigDecimal val = new BigDecimal(Formattazione.formatta(hl, "#############0.000", 0).replace(',', '.'));
                val = val.setScale(2, BigDecimal.ROUND_HALF_DOWN);
                hl = val.doubleValue();
            }
            return hl;
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public static double calcoloEttogradi(double litri, double gradoplato) {
        double hlg = 0;
        int somma = 1;
        boolean pers = true;
        try {
            //pers = (leggiProprieta(azienda, "hgbirre", "calcpershg").equals("S"));
            //somma = Formattazione.estraiIntero(leggiProprieta(azienda, "hgbirre", "somma"));
            if (pers) {
                double gradop = gradoplato;
                gradop = arrotondaGradoPlato(gradop);
                double hl = calcoloEttolitri(litri);
                int dechg = 0;
                int arroto = 1;
                BigDecimal val;
                //dechg = Formattazione.estraiIntero(leggiProprieta(azienda, "hgbirre", "decimali"));
                dechg = 2;
                //arroto = Formattazione.estraiIntero(leggiProprieta(azienda, "hgbirre", "arroto"));
                arroto = 1;
                hlg = hl * gradop;
                if (dechg == 0) {
                    val = new BigDecimal(Formattazione.formatta(hlg, "#############0.000", 0).replace(',', '.'));
                    val = val.setScale(0, BigDecimal.ROUND_HALF_DOWN);
                    hlg = val.doubleValue();
                } else if (arroto == 0) {
                    val = new BigDecimal(Formattazione.formatta(hlg, "#############0.000", 0).replace(',', '.'));
                    val = val.setScale(dechg, BigDecimal.ROUND_HALF_DOWN);
                    hlg = val.doubleValue();
                } else {
                    val = new BigDecimal(Formattazione.formatta(hlg, "#############0.000", 0).replace(',', '.'));
                    val = val.setScale(dechg, BigDecimal.ROUND_HALF_UP);
                    hlg = val.doubleValue();
                }
            } else {
                double gradop = gradoplato;
                gradop = arrotondaGradoPlato(gradop);
                double hl = litri / 100;
                if (OpValute.arrotondaMat(hl, 3) != OpValute.arrotondaMat(hl, 4)) {
                    hl = OpValute.arrotondaMat(hl + 0.0005, 3);
                }
                BigDecimal val = new BigDecimal(Formattazione.formatta(hl, "#############0.000", 0).replace(',', '.'));
                val = val.setScale(2, BigDecimal.ROUND_HALF_DOWN);
                hl = val.doubleValue();
                System.out.println("  ettolitri:" + hl);
                hlg = hl * gradop;
                val = new BigDecimal(Formattazione.formatta(hlg, "#############0.000", 0).replace(',', '.'));
                val = val.setScale(0, BigDecimal.ROUND_HALF_DOWN);
                hlg = val.doubleValue();
                System.out.println("hlg " + hlg);
            }
            return hlg;
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public static double arrotondaGradoPlato(double gradoplato) {
        double gp = 0;
        int decpl = 1;
        int arroto = 0;
        boolean pers = false;
        try {
            //pers = (leggiProprieta(azienda, "hgbirre", "calcpershg").equals("S"));
            //decpl = Formattazione.estraiIntero(leggiProprieta(azienda, "hgbirre", "decplato"));
            //arroto = Formattazione.estraiIntero(leggiProprieta(azienda, "hgbirre", "arrotoplato"));
            pers = true;
            decpl = 1;
            arroto = 1;
            if (pers) {
                BigDecimal val;
                gp = gradoplato;
                if (decpl == 0) {
                    val = new BigDecimal(Formattazione.formatta(gp, "#############0.000", 0).replace(',', '.'));
                    val = val.setScale(0, BigDecimal.ROUND_HALF_DOWN);
                    gp = val.doubleValue();
                } else if (arroto == 0) {
                    val = new BigDecimal(Formattazione.formatta(gp, "#############0.000", 0).replace(',', '.'));
                    val = val.setScale(decpl, BigDecimal.ROUND_HALF_DOWN);
                    gp = val.doubleValue();
                } else {
                    val = new BigDecimal(Formattazione.formatta(gp, "#############0.000", 0).replace(',', '.'));
                    val = val.setScale(decpl, BigDecimal.ROUND_HALF_UP);
                    gp = val.doubleValue();
                }
            } else {
                gp = gradoplato;
                BigDecimal val = new BigDecimal(Formattazione.formatta(gp, "#############0.00", 0).replace(',', '.'));
                val = val.setScale(1, BigDecimal.ROUND_HALF_DOWN);
                gp = val.doubleValue();
            }
            return gp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public static int tipoCalcoloPrezzoArticolo(String artcod) {
        // 1 = colli x prezzo
        // 3,4 = contenuto x prezzo
        int ris = 0;
        String[] pars = new String[1];
        pars[0] = artcod;
        try {
            Cursor c = Env.db.rawQuery("SELECT artcalcprzdoc FROM articoli WHERE artcod = ?", pars);
            if (c.moveToFirst()) {
                ris = c.getInt(0);
            }
            c.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ris;
    }


    public static ArrayList<Record> vendutoArticoliStat(String filtrocliente) {
        ArrayList<Record> ris = new ArrayList();

        Cursor c = Env.db.rawQuery("SELECT startcod, startdescr,stcolli,stqta,stlitritot,sttotld2,stvalore,stprezzom FROM statistiche" +
                " WHERE stclicod = '" + filtrocliente + "'" +
                " ORDER BY startcod", null);
        while (c.moveToNext()) {
            String artcod = c.getString(0);
            String artdescr = c.getString(1);
            int colli = Math.abs(c.getInt(2));
            double qta = Math.abs(c.getDouble(3));
            double litriIdrati = Math.abs(c.getDouble(4));
            double litriAnidri = Math.abs(c.getDouble(5));
            double valore = Math.abs(c.getDouble(6));
            double prezzom = Math.abs(c.getDouble(7));

            Record r = new Record();
            r.insStringa("artcod", artcod);
            r.insStringa("artdescr", artdescr);
            r.insIntero("artcolli", colli);
            r.insDouble("artqta", qta);
            r.insDouble("artlitriidrati", litriIdrati);
            r.insDouble("artlitrianidri", litriAnidri);
            r.insDouble("artvalore", valore);
            r.insDouble("artprezzom", prezzom);
            ris.add(r);

        }
        c.close();

        return ris;
    }

}
