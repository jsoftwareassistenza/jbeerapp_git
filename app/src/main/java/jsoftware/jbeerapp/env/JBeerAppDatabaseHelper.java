package jsoftware.jbeerapp.env;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static jsoftware.jbeerapp.env.Env.db;

public class JBeerAppDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "jbeerappbd.db";

    public JBeerAppDatabaseHelper(Context context, String name,
                                  CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.disableWriteAheadLogging();
        try {
            db.execSQL("CREATE TABLE datiterm (" +
                    "numterm INTEGER DEFAULT 0," +
                    "user VARCHAR(20) DEFAULT ''," +
                    "pwd VARCHAR(20) DEFAULT ''," +
                    "activationcode VARCHAR(20) DEFAULT ''," +
                    "pin VARCHAR(255) DEFAULT ''," +
                    "serverjcloud VARCHAR(255) DEFAULT ''," +
                    "serverjcloud2 VARCHAR(255) DEFAULT ''," +
                    "depcod VARCHAR(10) DEFAULT ''," +
                    "pedcod VARCHAR(10) DEFAULT ''," +
                    "agecod VARCHAR(10) DEFAULT ''," +
                    "agenome VARCHAR(255) DEFAULT ''," +
                    "targamezzo VARCHAR(20) DEFAULT ''," +
                    "docbollaval VARCHAR(10) DEFAULT ''," +
                    "sezbollaval VARCHAR(10) DEFAULT ''," +
                    "numbollaval INTEGER DEFAULT 1," +
                    "docbollaqta VARCHAR(10) DEFAULT ''," +
                    "sezbollaqta VARCHAR(10) DEFAULT ''," +
                    "numbollaqta INTEGER DEFAULT 1," +
                    "docfattura VARCHAR(10) DEFAULT ''," +
                    "sezfattura VARCHAR(10) DEFAULT ''," +
                    "numfattura INTEGER DEFAULT 1," +
                    "doccorrisp VARCHAR(10) DEFAULT ''," +
                    "sezcorrisp VARCHAR(10) DEFAULT ''," +
                    "numcorrisp INTEGER DEFAULT 1," +
                    "docddtreso VARCHAR(10) DEFAULT ''," +
                    "sezddtreso VARCHAR(10) DEFAULT ''," +
                    "numddtreso INTEGER DEFAULT 1," +
                    "docddtcarico VARCHAR(10) DEFAULT ''," +
                    "sezddtcarico VARCHAR(10) DEFAULT ''," +
                    "numddtcarico INTEGER DEFAULT 1," +
                    "docscaricosede VARCHAR(10) DEFAULT ''," +
                    "sezscaricosede VARCHAR(10) DEFAULT ''," +
                    "numscaricosede INTEGER DEFAULT 1," +
                    "doctrasfamezzo VARCHAR(10) DEFAULT ''," +
                    "seztrasfamezzo VARCHAR(10) DEFAULT ''," +
                    "numtrasfamezzo INTEGER DEFAULT 1," +
                    "doctrasfdamezzo VARCHAR(10) DEFAULT ''," +
                    "seztrasfdamezzo VARCHAR(10) DEFAULT ''," +
                    "numtrasfdamezzo INTEGER DEFAULT 1," +
                    "docordinesede VARCHAR(10) DEFAULT ''," +
                    "sezordinesede VARCHAR(10) DEFAULT ''," +
                    "numordinesede INTEGER DEFAULT 1," +
                    "docrimfs VARCHAR(10) DEFAULT ''," +
                    "sezrimfs VARCHAR(10) DEFAULT ''," +
                    "depsede VARCHAR(10) DEFAULT ''," +
                    "cmvend VARCHAR(10) DEFAULT ''," +
                    "cmrprom VARCHAR(10) DEFAULT ''," +
                    "cmresov VARCHAR(10) DEFAULT ''," +
                    "cmresonv VARCHAR(10) DEFAULT ''," +
                    "cmsost VARCHAR(10) DEFAULT ''," +
                    "cmomaggi VARCHAR(10) DEFAULT ''," +
                    "cmsm VARCHAR(10) DEFAULT ''," +
                    "cmddtcarico VARCHAR(10) DEFAULT ''," +
                    "cmscaricosede VARCHAR(10) DEFAULT ''," +
                    "cmtrasfamezzo VARCHAR(10) DEFAULT ''," +
                    "cmtrasfdamezzo VARCHAR(10) DEFAULT ''," +
                    "cmdistrmerce VARCHAR(10) DEFAULT ''," +
                    "cmordinesede VARCHAR(10) DEFAULT ''," +
                    "cmscaduto VARCHAR(10) DEFAULT ''," +
                    "codivaomaggiimp20 VARCHAR(10) DEFAULT ''," +
                    "codivaomaggiimp10 VARCHAR(10) DEFAULT ''," +
                    "codivaomaggiimp04 VARCHAR(10) DEFAULT ''," +
                    "codivaomaggitot20 VARCHAR(10) DEFAULT ''," +
                    "codivaomaggitot10 VARCHAR(10) DEFAULT ''," +
                    "codivaomaggitot04 VARCHAR(10) DEFAULT ''," +
                    "gvcod VARCHAR(10) DEFAULT ''," +
                    "clinuovolist VARCHAR(10) DEFAULT ''," +
                    "clinuovoposprz INTEGER," +
                    "clinuovopag VARCHAR(10) DEFAULT ''," +
                    "clinuovocod VARCHAR(10) DEFAULT ''," +
                    "cliordine VARCHAR(10) DEFAULT ''," +
                    "tipoddtcar INTEGER," +
                    "btindservercs VARCHAR(20) DEFAULT ''," +
                    "gprs_indserver VARCHAR(255) DEFAULT ''," +
                    "gprs_postserver VARCHAR(255) DEFAULT ''," +
                    "autcod VARCHAR(10) DEFAULT ''," +
                    "autdescr VARCHAR(255) DEFAULT ''," +
                    "auttarga VARCHAR(10) DEFAULT ''," +
                    "tipostampante int DEFAULT 0," + // 0 = sewoo LK-P30 3p wifi, 1 = sewoo LK-P41 4p bluetooth
                    "ssidstampante VARCHAR(255) DEFAULT ''," +
                    "ipstampante VARCHAR(255) DEFAULT ''," +
                    "nomebtstampante VARCHAR(255) DEFAULT ''," +
                    "tiposcambiodati INTEGER DEFAULT 0," +
                    "ssidscambiodati VARCHAR(255) DEFAULT ''," +
                    "ipscambiodati VARCHAR(255) DEFAULT ''," +
                    "tiponuovicli INTEGER DEFAULT 0," +
                    "tipogestlotti INTEGER DEFAULT 0," +
                    "intazriga1 VARCHAR(255) DEFAULT ''," +
                    "intazriga2 VARCHAR(255) DEFAULT ''," +
                    "intazriga3 VARCHAR(255) DEFAULT ''," +
                    "intazriga4 VARCHAR(255) DEFAULT ''," +
                    "intazriga5 VARCHAR(255) DEFAULT ''," +
                    "intazriga6 VARCHAR(255) DEFAULT ''," +
                    "intazriga7 VARCHAR(255) DEFAULT ''," +
                    "visprzrif VARCHAR(1) DEFAULT 'N'," +
                    "ctrprzrif VARCHAR(1) DEFAULT 'N'," +
                    "bloccosconti VARCHAR(1) DEFAULT 'N'," +
                    "obbligoincpagtv VARCHAR(1) DEFAULT 'N'," +
                    "pwdcambiovend VARCHAR(1) DEFAULT 'N'," +
                    "tipoannullamentodoc INTEGER DEFAULT 0," +
                    "nscadblocco INTEGER DEFAULT 0," +
                    "ggtollblocco INTEGER DEFAULT 0," +
                    "impblocco Real DEFAULT 0," +
                    "nrighelogo Real DEFAULT 7," +
                    "stampasedesec VARCHAR(1) DEFAULT 'N'," +
                    "stampaddtcar_classi VARCHAR(1) DEFAULT 'N'," +
                    "stampaord VARCHAR(1) DEFAULT 'N'," +
                    "lottineg VARCHAR(1) DEFAULT 'N'," +
                    "listbase VARCHAR(10) DEFAULT ''," +
                    "trasmdopofg VARCHAR(1) DEFAULT 'N'," +
                    "inviotrasfvsmezzo VARCHAR(1) DEFAULT 'N'," +
                    "datainizioblocco VARCHAR(10) DEFAULT ''," +
                    "noxeterm VARCHAR(1) DEFAULT 'N'," +
                    "ddtstampatimbro VARCHAR(1) DEFAULT 'N'," +
                    "ddtstamparaggrlotti VARCHAR(1) DEFAULT 'N'," +
                    "ddtstampascop VARCHAR(1) DEFAULT 'N'," +
                    "ddtqtanocess VARCHAR(1) DEFAULT 'N'," +
                    "ddtdatiage INTEGER DEFAULT 0," +
                    "numinv INTEGER DEFAULT 1," +
                    "noscarsede VARCHAR(1) DEFAULT 'N'," +
                    "notrasfvs VARCHAR(1) DEFAULT 'N'," +
                    "notrasfda VARCHAR(1) DEFAULT 'N'," +
                    "noanndoc VARCHAR(1) DEFAULT 'N'," +
                    "azioneblocco INTEGER DEFAULT 0," +
                    "vendqtatot VARCHAR(1) DEFAULT 'N'," +
                    "prezzopref VARCHAR(1) DEFAULT 'N'," +
                    "noriccarichi VARCHAR(1) DEFAULT 'N'," +
                    "finegstampadatiart VARCHAR(1) DEFAULT 'N'," +
                    "finegstampaelencodoc VARCHAR(1) DEFAULT 'N'," +
                    "finegstampasospdainc VARCHAR(1) DEFAULT 'S'," +
                    "finegdistintadenaro VARCHAR(1) DEFAULT 'N'," +
                    "backupauto VARCHAR(1) DEFAULT 'N'," +
                    "tipobackupauto INTEGER DEFAULT 0," +
                    "docordinecli VARCHAR(10) DEFAULT ''," +
                    "sezordinecli VARCHAR(10) DEFAULT ''," +
                    "numordinecli INTEGER DEFAULT 1," +
                    "stampascasede VARCHAR(1) DEFAULT 'N'," +
                    "stampatrasfvs VARCHAR(1) DEFAULT 'N'," +
                    "stampatrasfda VARCHAR(1) DEFAULT 'N'," +
                    "scasedenovalvend VARCHAR(1) DEFAULT 'N'," +
                    "scasedenovalrot VARCHAR(1) DEFAULT 'N'," +
                    "scasedenovalscad VARCHAR(1) DEFAULT 'N'," +
                    "stampaordtotali VARCHAR(1) DEFAULT 'N'," +
                    "maxscvend Real DEFAULT 0," +
                    "visprzacq VARCHAR(1) DEFAULT 'N'," +
                    "incattivanote VARCHAR(1) DEFAULT 'N'," +
                    "incsceglicassa VARCHAR(1) DEFAULT 'N'," +
                    "noordsede VARCHAR(1) DEFAULT 'N'," +
                    "nosostlotti VARCHAR(1) DEFAULT 'N'," +
                    "nogestlist VARCHAR(1) DEFAULT 'N'," +
                    "nocambiovend VARCHAR(1) DEFAULT 'N'," +
                    "nocambiogirovisita VARCHAR(1) DEFAULT 'N'," +
                    "noprztrasp VARCHAR(1) DEFAULT 'N'," +
                    "noinv VARCHAR(1) DEFAULT 'N'," +
                    "nomenustampe VARCHAR(1) DEFAULT 'N'," +
                    "incnostampa VARCHAR(1) DEFAULT 'N'," +
                    "tipoterm INTEGER DEFAULT 0," +
                    "depgiac VARCHAR(10) DEFAULT ''," +
                    "sceltaemail VARCHAR(1) DEFAULT 'N'," +
                    "emailfirmariga1 VARCHAR(255) DEFAULT ''," +
                    "emailfirmariga2 VARCHAR(255) DEFAULT ''," +
                    "emailfirmariga3 VARCHAR(255) DEFAULT ''," +
                    "abbuoniattivi VARCHAR(1) DEFAULT 'N'," +
                    "backupperiodico INTEGER DEFAULT 0," +
                    "datattivazione varchar(10) DEFAULT '0000-00-00'," +
                    "versionedemo VARCHAR(1) DEFAULT 'N'," +
                    "calcaccisa VARCHAR(1) DEFAULT 'N'," +
                    "tipocalcaccisa INTEGER DEFAULT 0," +
                    "voceaccisa VARCHAR(255) DEFAULT ''," +
                    "dispart VARCHAR(1) DEFAULT 'N', " +
                    "pwdanndoc VARCHAR(1) DEFAULT 'N'," +
                    "pwdpar VARCHAR(1) DEFAULT 'N'," +
                    "pwdutilita VARCHAR(1) DEFAULT 'N'," +
                    "passcodetrasf VARCHAR(1) DEFAULT 'N'," +
                    "copieddt INTEGER DEFAULT 1," +
                    "copieddtqta INTEGER DEFAULT 1," +
                    "noannstampe VARCHAR(1) DEFAULT 'N'," +
                    "colli1 VARCHAR(1) DEFAULT 'N'," +
                    "usalistinonuovo VARCHAR(1) DEFAULT 'N'," +
                    "invioordineauto VARCHAR(1) DEFAULT 'N'," +
                    "ordinicligiacterm VARCHAR(1) DEFAULT 'N'," +
                    "nostampafinegiornata VARCHAR(1) DEFAULT 'N'," +
                    "orddepsede VARCHAR(1) DEFAULT 'N'," +
                    "gestlotti INTEGER DEFAULT 0," +
                    "moddoc VARCHAR(1) DEFAULT 'N'" +
                    ")");

            db.execSQL("CREATE TABLE clienti ("
                    + "clicodice VARCHAR(10) DEFAULT '' CONSTRAINT pkccod PRIMARY KEY,"
                    + "clinome VARCHAR(255) DEFAULT '',"
                    + "cliindir VARCHAR(255) DEFAULT '',"
                    + "clicap VARCHAR(10) DEFAULT '',"
                    + "cliloc VARCHAR(255) DEFAULT '',"
                    + "cliprov VARCHAR(2) DEFAULT '',"
                    + "clipiva VARCHAR(16) DEFAULT '',"
                    + "clicodpadre VARCHAR(10) DEFAULT '',"
                    + "clicodpag VARCHAR(10) DEFAULT '',"
                    + "clicodiva VARCHAR(3) DEFAULT '',"
                    + "clinumtel VARCHAR(20) DEFAULT '',"
                    + "clinumcell VARCHAR(20) DEFAULT '',"
                    + "clinumfax VARCHAR(20) DEFAULT '',"
                    + "clinomeemergenze VARCHAR(255) DEFAULT '',"
                    + "clitelemergenze VARCHAR(20) DEFAULT '',"
                    + "clitelammin VARCHAR(20) DEFAULT '',"
                    + "clitelcomm VARCHAR(20) DEFAULT '',"
                    + "clitelabitaz VARCHAR(20) DEFAULT '',"
                    + "clitelsedeop VARCHAR(20) DEFAULT '',"
                    + "cliemail1 VARCHAR(255) DEFAULT '',"
                    + "cliemail2 VARCHAR(255) DEFAULT '',"
                    + "cliemailpec VARCHAR(255) DEFAULT '',"
                    + "clisitoweb VARCHAR(255) DEFAULT '',"
                    + "clicodlist VARCHAR(10) DEFAULT '',"
                    + "clinprz INTEGER DEFAULT 1,"
                    + "climodprz VARCHAR(1) DEFAULT 'S',"
                    + "clistampaprz VARCHAR(1) DEFAULT 'S',"
                    + "clisc1 Real DEFAULT 0,"
                    + "clisc2 Real DEFAULT 0,"
                    + "clisc3 Real DEFAULT 0,"
                    + "climag Real DEFAULT 0,"
                    + "clifido Real DEFAULT 0,"
                    + "clibloccofido VARCHAR(1) DEFAULT 'N',"
                    + "climaxscvend Real DEFAULT 0,"
                    + "clispeseinc VARCHAR(1) DEFAULT 'N',"
                    + "clispesebol VARCHAR(1) DEFAULT 'N',"
                    + "clispesetrasp VARCHAR(1) DEFAULT 'N',"
                    + "clispesecauz VARCHAR(1) DEFAULT 'N',"
                    + "clibancacod VARCHAR(10) DEFAULT '',"
                    + "clibancadescr VARCHAR(50) DEFAULT '',"
                    + "cliabi VARCHAR(7) DEFAULT '',"
                    + "cliagenzia VARCHAR(50) DEFAULT '',"
                    + "clicab VARCHAR(7) DEFAULT '',"
                    + "climodpag VARCHAR(1) DEFAULT 'N',"
                    + "climastro VARCHAR(4) DEFAULT '',"
                    + "cliconto VARCHAR(4) DEFAULT '',"
                    + "clisottoconto VARCHAR(10) DEFAULT '',"
                    + "cliblocco VARCHAR(1) DEFAULT 'N',"
                    + "cliordine VARCHAR(1) DEFAULT 'S',"
                    + "cligpsnordsud VARCHAR(1) DEFAULT 'N',"
                    + "cligpsestovest VARCHAR(1) DEFAULT 'E',"
                    + "cligpsnordgradi Real DEFAULT 0,"
                    + "cligpsnordmin Real DEFAULT 0,"
                    + "cligpsnordsec Real DEFAULT 0,"
                    + "cligpsestgradi Real DEFAULT 0,"
                    + "cligpsestmin Real DEFAULT 0,"
                    + "cligpsestsec Real DEFAULT 0,"
                    + "clicanalecod VARCHAR(10) DEFAULT '',"
                    + "clicanaledescr VARCHAR(255) DEFAULT '',"
                    + "clicategcod VARCHAR(10) DEFAULT '',"
                    + "clicategdescr VARCHAR(255) DEFAULT '',"
                    + "clizonacod VARCHAR(10) DEFAULT '',"
                    + "clizonadescr VARCHAR(255) DEFAULT '',"
                    + "clinuovo INTEGER DEFAULT 0,"
                    + "clivalidato VARCHAR(1) DEFAULT 'S',"
                    + "cliraggrddt VARCHAR(1) DEFAULT 'S',"
                    + "clicausm VARCHAR(1) DEFAULT 'S',"
                    + "clicauom VARCHAR(1) DEFAULT 'S',"
                    + "clicaureso VARCHAR(1) DEFAULT 'S',"
                    + "clicausost VARCHAR(1) DEFAULT 'S',"
                    + "clibollaval VARCHAR(1) DEFAULT 'S',"
                    + "clibollaqta VARCHAR(1) DEFAULT 'S',"
                    + "clifattura VARCHAR(1) DEFAULT 'S',"
                    + "clicorrisp VARCHAR(1) DEFAULT 'S',"
                    + "cliddtreso VARCHAR(1) DEFAULT 'S',"
                    + "clinumcontr VARCHAR(30) DEFAULT '',"
                    + "clidatacontr VARCHAR(10) DEFAULT '',"
                    + "clinscadblocco INTEGER DEFAULT 0,"
                    + "cliimpblocco Real DEFAULT 0,"
                    + "cliggtollblocco INTEGER DEFAULT 0,"
                    + "clidtblocco VARCHAR(10) DEFAULT '0000-00-00',"
                    + "cliazioneblocco INTEGER DEFAULT 0,"
                    + "clidocpref INTEGER DEFAULT 0,"
                    + "cliordlotto VARCHAR(1) DEFAULT 'A',"
                    + "clicognomepri VARCHAR(255) DEFAULT ''," //cognome del privato
                    + "clinomepri VARCHAR(255) DEFAULT '',"   //nome del privato
                    + "clicodfisc VARCHAR(25) DEFAULT '',"
                    + "clinote VARCHAR(255) DEFAULT '',"
                    + "clitrasferito VARCHAR(1) DEFAULT 'N'," // trasferito a jazz S-N (solo per clienti nuovi)
                    + "clinumciv VARCHAR(10) DEFAULT '',"
                    + "clitipo INTEGER DEFAULT 0,"
                    + "clicodsdinuovo VARCHAR(255) DEFAULT '',"
                    + "cliinsegna VARCHAR(255) DEFAULT ''"
                    + ")");

            db.execSQL("CREATE INDEX tclicod ON clienti (clicodice)");
            db.execSQL("CREATE INDEX tclinome ON clienti (clinome)");
            db.execSQL("CREATE INDEX tclicodpadre ON clienti (clicodpadre)");

            db.execSQL("CREATE TABLE contatti ("
                    + "ccodice VARCHAR(10) DEFAULT '' CONSTRAINT pkcod PRIMARY KEY,"
                    + "ccognome varchar(255) DEFAULT '',"
                    + "cnome varchar(255) DEFAULT '',"
                    + "cdatacont varchar(10) DEFAULT '0000-00-00',"
                    + "ceventocont varchar(255) DEFAULT '',"
                    + "ctitolo varchar(255) DEFAULT '',"
                    + "cmansione varchar(255) DEFAULT '',"
                    + "cemail varchar(255) DEFAULT '',"
                    + "ctel varchar(255) DEFAULT '',"
                    + "cmobile varchar(255) DEFAULT '',"
                    + "cfax varchar(255) DEFAULT '',"
                    + "ctipologia INTEGER DEFAULT 0," //tipologia di contatto: 0 cliente, 1 fornitore, 2 altro
                    + "ctipo INTEGER DEFAULT 0," // tipo di contatto: 0 privato, 1 azienda
                    + "caziendaragsoc varchar(255) DEFAULT '',"
                    + "caziendaindir varchar(255) DEFAULT '',"
                    + "caziendanumciv varchar(10) DEFAULT '',"
                    + "caziendaloc varchar(255) DEFAULT '',"
                    + "caziendacap varchar(10) DEFAULT '',"
                    + "caziendaprov varchar(4) DEFAULT '',"
                    + "caziendanaz varchar(255) DEFAULT '',"
                    + "caziendaemail varchar(255) DEFAULT '',"
                    + "caziendasitoweb varchar(255) DEFAULT '',"
                    + "ctrasferito VARCHAR(1) DEFAULT 'N'," // trasferito a jazz S-N
                    + "ctrasformatocliente VARCHAR(1) DEFAULT 'N'," // trasformato in cliente
                    + "cnote varchar(255) DEFAULT '',"
                    + "ctermcod VARCHAR(10) DEFAULT ''"
                    + ")");

            db.execSQL("CREATE TABLE catalogo ("
                    + "catartcod varchar(255) DEFAULT '' CONSTRAINT pkcatag PRIMARY KEY,"
                    + "catstile varchar(255) DEFAULT '',"
                    + "catcolore varchar(255) DEFAULT '',"
                    + "catlistcod varchar(255) DEFAULT '',"
                    + "catgrado Real DEFAULT 0,"
                    + "catnomeart varchar(255) DEFAULT '',"
                    + "catnote TEXT)");

            db.execSQL("CREATE TABLE catalogopdf ("
                    + "catpdfevento varchar(255) DEFAULT '',"
                    + "catpdfnome varchar(255) DEFAULT '',"
                    + "catpdfdata VARCHAR(10) DEFAULT '0000-00-00'"
                    + ")");

            db.execSQL("CREATE TABLE articoli ("
                    + "artcod VARCHAR(20) DEFAULT '' CONSTRAINT pkacod PRIMARY KEY,"
                    + "artdescr VARCHAR(255) DEFAULT '',"
                    + "artum VARCHAR(3) DEFAULT '',"
                    + "artflagum INTEGER DEFAULT 0,"
                    + "artcodiva VARCHAR(3) DEFAULT '',"
                    + "artpezzi Real DEFAULT 0,"
                    + "artscprom Real DEFAULT 0,"
                    + "artclassecod VARCHAR(10) DEFAULT '',"
                    + "artsottoclassecod VARCHAR(10) DEFAULT '',"
                    + "artmarcacod VARCHAR(10) DEFAULT '',"
                    + "artscdatainizio VARCHAR(10) DEFAULT '0000-00-00',"
                    + "artscdatafine VARCHAR(10) DEFAULT '0000-00-00',"
                    + "artcpesclaltri VARCHAR(1) DEFAULT 'N',"
                    + "artcauvend VARCHAR(1) DEFAULT 'S',"
                    + "artcausm VARCHAR(1) DEFAULT 'S',"
                    + "artcauom VARCHAR(1) DEFAULT 'S',"
                    + "artcaureso VARCHAR(1) DEFAULT 'S',"
                    + "artcausost VARCHAR(1) DEFAULT 'S',"
                    + "artgiacenza Real DEFAULT 0,"
                    + "artcodsc VARCHAR(10) DEFAULT '',"
                    + "artpeso Real DEFAULT 0,"
                    + "artcauzcod VARCHAR(10) DEFAULT '',"
                    + "artraggrcod VARCHAR(10) DEFAULT '',"
                    + "artggscad INTEGER DEFAULT 0,"
                    + "artart62 INTEGER DEFAULT 0,"
                    + "arttipoord INTEGER DEFAULT 0,"
                    + "artcodbarre VARCHAR(255) DEFAULT '',"
                    + "artnotificaterm INTEGER DEFAULT 0,"
                    + "artnoord VARCHAR(1) DEFAULT 'N',"
                    + "artstato VARCHAR(1) DEFAULT 'A',"
                    + "artprzacq Real DEFAULT 0,"
                    + "artcalcprzdoc INTEGER DEFAULT 0,"
                    + "artweb_linkscheda VARCHAR(255) DEFAULT '')");
            db.execSQL("CREATE INDEX idescr ON articoli (artdescr)");
            db.execSQL("CREATE INDEX iclcod ON articoli (artclassecod)");
            db.execSQL("CREATE INDEX isccod ON articoli (artsottoclassecod)");
            db.execSQL("CREATE INDEX imarca ON articoli (artmarcacod)");
            db.execSQL("CREATE INDEX istato ON articoli (artstato)");
            db.execSQL("CREATE INDEX itipoord ON articoli (arttipoord)");
            db.execSQL("CREATE INDEX inotterm ON articoli (artnotificaterm)");

            db.execSQL("CREATE TABLE artbis ("
                    + "artcod VARCHAR(20) DEFAULT '' CONSTRAINT pkabacod PRIMARY KEY,"
                    + "artgiacenza Real DEFAULT 0)");

            db.execSQL("CREATE TABLE marche ("
                    + "marcacod VARCHAR(10) CONSTRAINT pkmcod PRIMARY KEY,"
                    + "marcadescr VARCHAR(255)" + ")");

            db.execSQL("CREATE TABLE raggrart ("
                    + "raggrcod VARCHAR(10) CONSTRAINT pkraggrcod PRIMARY KEY,"
                    + "raggrdescr VARCHAR(255)" + ")");

            db.execSQL("CREATE TABLE sottoclassi ("
                    + "clcod VARCHAR(10) DEFAULT '',"
                    + "sccod VARCHAR(10) DEFAULT '',"
                    + "scdescr VARCHAR(255) DEFAULT '',"
                    + "primary key (clcod,sccod)" + ")");

            db.execSQL("CREATE TABLE listini ("
                    + "lcod VARCHAR(10) DEFAULT '',"
                    + "lartcod VARCHAR(20) DEFAULT '',"
                    + "ldataval VARCHAR(10) DEFAULT '',"
                    + "lprezzo1 Real DEFAULT 0," + "lprezzo2 Real DEFAULT 0,"
                    + "lprezzo3 Real DEFAULT 0," + "lprezzo4 Real DEFAULT 0,"
                    + "lprezzo5 Real DEFAULT 0," + "lprezzo6 Real DEFAULT 0,"
                    + "lprezzo7 Real DEFAULT 0," + "lprezzo8 Real DEFAULT 0,"
                    + "lprezzo9 Real DEFAULT 0,"
                    + "primary key (lcod,lartcod,ldataval))");

            db.execSQL("CREATE TABLE listinifor ("
                    + "lfcod VARCHAR(10) DEFAULT '',"
                    + "lfartcod VARCHAR(20) DEFAULT '',"
                    + "lfdataval VARCHAR(10) DEFAULT '',"
                    + "lfprezzo1 Real DEFAULT 0,"
                    + "lfprezzo2 Real DEFAULT 0,"
                    + "lfprezzo3 Real DEFAULT 0,"
                    + "lfprezzo4 Real DEFAULT 0,"
                    + "lfprezzo5 Real DEFAULT 0,"
                    + "lfprezzo6 Real DEFAULT 0,"
                    + "lfprezzo7 Real DEFAULT 0,"
                    + "lfprezzo8 Real DEFAULT 0,"
                    + "lfprezzo9 Real DEFAULT 0,"
                    + "primary key (lfcod,lfartcod,lfdataval))");

            db.execSQL("CREATE TABLE scontilist (" +
                    "sllistcod VARCHAR(10) DEFAULT ''," +
                    "slcodsc VARCHAR(10) DEFAULT ''," +
                    "slsc1_1 Real DEFAULT 0," +
                    "slsc1_2 Real DEFAULT 0," +
                    "slsc2_1 Real DEFAULT 0," +
                    "slsc2_2 Real DEFAULT 0," +
                    "slsc3_1 Real DEFAULT 0," +
                    "slsc3_2 Real DEFAULT 0," +
                    "slsc4_1 Real DEFAULT 0," +
                    "slsc4_2 Real DEFAULT 0," +
                    "slsc5_1 Real DEFAULT 0," +
                    "slsc5_2 Real DEFAULT 0," +
                    "slsc6_1 Real DEFAULT 0," +
                    "slsc6_2 Real DEFAULT 0," +
                    "slsc7_1 Real DEFAULT 0," +
                    "slsc7_2 Real DEFAULT 0," +
                    "slsc8_1 Real DEFAULT 0," +
                    "slsc8_2 Real DEFAULT 0," +
                    "slsc9_1 Real DEFAULT 0," +
                    "slsc9_2 Real DEFAULT 0," +
                    "slescltesta INTEGER DEFAULT 0," +
                    "primary key (sllistcod,slcodsc)" +
                    ")");

            db.execSQL("CREATE TABLE condpers ("
                    + "cpclicod VARCHAR(10) DEFAULT '',"
                    + "cpartcod VARCHAR(20) DEFAULT '',"
                    + "cpdatainizio VARCHAR(10) DEFAULT '0000-00-00',"
                    + "cpdatafine VARCHAR(10) DEFAULT '0000-00-00',"
                    + "cpsc1 Real DEFAULT 0," + "cpsc2 Real DEFAULT 0,"
                    + "cpsc3 Real DEFAULT 0,"
                    + "cplistcod VARCHAR(10) DEFAULT '',"
                    + "cpprezzo Real DEFAULT 0,"
                    + "cpesclaltri VARCHAR(1) DEFAULT 'N',"
                    + "cpescltestata VARCHAR(1) DEFAULT 'N'"
                    + ")");
            db.execSQL("CREATE INDEX icp ON condpers (cpclicod,cpartcod,cpdatainizio,cpdatafine)");

            db.execSQL("CREATE TABLE condpersfor ("
                    + "cpfforcod VARCHAR(10) DEFAULT '',"
                    + "cpfartcod VARCHAR(20) DEFAULT '',"
                    + "cpfdatainizio VARCHAR(10) DEFAULT '0000-00-00',"
                    + "cpfdatafine VARCHAR(10) DEFAULT '0000-00-00',"
                    + "cpfsc1 Real DEFAULT 0,"
                    + "cpfsc2 Real DEFAULT 0,"
                    + "cpfsc3 Real DEFAULT 0,"
                    + "cpflistcod VARCHAR(10) DEFAULT '',"
                    + "cpfprezzo Real DEFAULT 0,"
                    + "cpfesclaltri VARCHAR(1) DEFAULT 'N',"
                    + "cpfescltestata VARCHAR(1) DEFAULT 'N'"
                    + ")");
            db.execSQL("CREATE INDEX icpf ON condpersfor (cpfforcod,cpfartcod,cpfdatainizio,cpfdatafine)");

            db.execSQL("CREATE TABLE scoperti ("
                    + "sctipo VARCHAR(1),"
                    + // [F]fattura,[N]nota credito,[B]bolla,[C]generico
                    "scsezdoc VARCHAR(10),"
                    + "scdatadoc VARCHAR(10),"
                    + "scnumdoc integer,"
                    + // [V]vendita, [R]reso
                    "sctipoop VARCHAR(1) DEFAULT '',"
                    + "scnote VARCHAR(50) DEFAULT '',"
                    + "scclicod VARCHAR(10) DEFAULT '',"
                    + "scdestcod VARCHAR(10) DEFAULT '',"
                    + "scscadid INTEGER DEFAULT -1,"
                    + "scdatascad VARCHAR(10) DEFAULT '0000-00-00',"
                    + "sctiposcad INTEGER DEFAULT 0,"
                    + "scimportoscad Real DEFAULT 0,"
                    + "scresiduoscad Real DEFAULT 0,"
                    + "scrateizzata INTEGER DEFAULT 0,"
                    + "scdataconsfat VARCHAR(10) DEFAULT '0000-00-00'" + ")");
            db.execSQL("CREATE INDEX icli ON scoperti (scclicod)");
            db.execSQL("CREATE INDEX idest ON scoperti (scdestcod)");
            db.execSQL("CREATE INDEX idoc ON scoperti (scsezdoc,scdatadoc,scnumdoc)");
            db.execSQL("CREATE INDEX itipo ON scoperti (sctipo)");

            db.execSQL("CREATE TABLE lotti ("
                    + "lotcod VARCHAR(20) DEFAULT '',"
                    + "lotartcod VARCHAR(20) DEFAULT '',"
                    + "lotdata VARCHAR(10) DEFAULT '',"
                    + "lotgiacenza Real DEFAULT 0)");
            db.execSQL("CREATE INDEX ilacod ON lotti (lotartcod)");
            db.execSQL("CREATE INDEX ilccod ON lotti (lotcod)");

            db.execSQL("CREATE TABLE codiva ("
                    + "ivacod VARCHAR(3) CONSTRAINT pkicod PRIMARY KEY,"
                    + "ivadescr VARCHAR(255) DEFAULT '',"
                    + "ivagruppo INTEGER DEFAULT 0,"
                    + "ivaaliq Real DEFAULT 0," + "ivapercind Real DEFAULT 0,"
                    + "ivagest INTEGER DEFAULT 0,"
                    + "ivacodivagest VARCHAR(3) DEFAULT '',"
                    + "ivaomaggiotot VARCHAR(1) DEFAULT 'N'" + ")");

            db.execSQL("CREATE TABLE girivisita ("
                    + "gvcod VARCHAR(10) DEFAULT '',"
                    + "gvprog INTEGER DEFAULT 0,"
                    + "gvcodcli VARCHAR(10) DEFAULT '',"
                    + "gvcoddest VARCHAR(10) DEFAULT '',"
                    + "gvcodric VARCHAR(10) DEFAULT ''" + ")");
            db.execSQL("CREATE INDEX igvcod ON girivisita (gvcod)");
            db.execSQL("CREATE INDEX igvprog ON girivisita (gvprog)");
            db.execSQL("CREATE INDEX igvcli ON girivisita (gvcodcli)");
            db.execSQL("CREATE INDEX igvdest ON girivisita (gvcoddest)");

            db.execSQL("CREATE TABLE messaggi ("
                    + "msgtesto VARCHAR(255) DEFAULT '',"
                    + "msgdatainizio VARCHAR(10) DEFAULT '0000-00-00',"
                    + "msgdatafine VARCHAR(10) DEFAULT '0000-00-00',"
                    + "msgora VARCHAR(5) DEFAULT '00:00')");

            db.execSQL("CREATE TABLE vismessaggi ("
                    + "vmsgdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "vmsgora VARCHAR(5) DEFAULT '00:00')");
            db.execSQL("CREATE INDEX ivmsg1 ON vismessaggi (vmsgdata)");

            db.execSQL("CREATE TABLE pagamenti ("
                    + "pagcod VARCHAR(10) CONSTRAINT pkpcod PRIMARY KEY,"
                    + "pagdescr VARCHAR(255) DEFAULT '',"
                    + "pagtipo INTEGER DEFAULT 0,"
                    + "pagspeseinc Real DEFAULT 0,"
                    + "pagcodivaspeseinc VARCHAR(3) DEFAULT '',"
                    + "pagnrate INTEGER DEFAULT 0,"
                    + "pagbancacod VARCHAR(10) DEFAULT '',"
                    + "pagbancadescr VARCHAR(50) DEFAULT '',"
                    + "pagabi VARCHAR(7) DEFAULT '',"
                    + "pagagenzia VARCHAR(50) DEFAULT '',"
                    + "pagcab VARCHAR(7) DEFAULT '',"
                    + "pagtipocalc VARCHAR(2) DEFAULT '',"
                    + "paggg1 INTEGER DEFAULT 0," + "paggg2 INTEGER DEFAULT 0,"
                    + "paggg3 INTEGER DEFAULT 0," + "paggg4 INTEGER DEFAULT 0,"
                    + "paggg5 INTEGER DEFAULT 0," + "paggg6 INTEGER DEFAULT 0,"
                    + "pagsconto Real DEFAULT 0"
                    + ")");

            db.execSQL("CREATE TABLE agenti ("
                    + "agecod VARCHAR(10) CONSTRAINT pkagecod PRIMARY KEY,"
                    + "agedescr VARCHAR(255) DEFAULT '',"
                    + "ageindirizzo VARCHAR(255) DEFAULT '',"
                    + "agelocalita VARCHAR(255) DEFAULT '',"
                    + "agepartitaiva VARCHAR(16) DEFAULT '',"
                    + "agecodicefiscale VARCHAR(16) DEFAULT '',"
                    + "agetel VARCHAR(255) DEFAULT '',"
                    + "agefax VARCHAR(255) DEFAULT '',"
                    + "agemobile VARCHAR(255) DEFAULT '',"
                    + "ageemail VARCHAR(255) DEFAULT '',"
                    + "agenregalbo VARCHAR(20) DEFAULT '',"
                    + "agemaxsc Real DEFAULT 0" // 0 = nessun blocco
                    + ")");

            db.execSQL("CREATE TABLE depositi ("
                    + "depcod VARCHAR(10) CONSTRAINT pkdepcod PRIMARY KEY,"
                    + "depdescr VARCHAR(255)," + "depubic1 VARCHAR(255)," + "depeventi INTEGER(1) DEFAULT 0,"
                    + "depdatai VARCHAR(10) DEFAULT '0000-00-00'," + "depdataf VARCHAR(10) DEFAULT '0000-00-00'," + "depubic2 VARCHAR(255)" + ")");

            db.execSQL("CREATE TABLE automezzi ("
                    + "amcod VARCHAR(10) CONSTRAINT pkamcod PRIMARY KEY,"
                    + "amdescr VARCHAR(255)," + "amtarga VARCHAR(255)" + ")");

            db.execSQL("CREATE TABLE banche ("
                    + "bcod VARCHAR(10) CONSTRAINT pkbancacod PRIMARY KEY,"
                    + "bdescr VARCHAR(255)," + "babi VARCHAR(7)" + ")");

            db.execSQL("CREATE TABLE cauzioni ("
                    + "cauzcod VARCHAR(10) CONSTRAINT pkczcod PRIMARY KEY,"
                    + "cauzdescr VARCHAR(255)," + "cauzdescrbreve VARCHAR(10),"
                    + "cauztipo INTEGER,"
                    + "cauzcapacita Real DEFAULT 0,"
                    + "cauztipocalc INTEGER DEFAULT 0,"
                    + "cauzc1cod VARCHAR(10)," + "cauzc2cod VARCHAR(10),"
                    + "cauzc3cod VARCHAR(10)," + "cauzc1qta Real,"
                    + "cauzc2qta Real," + "cauzc3qta Real,"
                    + "cauzprezzo Real," + "cauzposdoc INTEGER" + ")");

            db.execSQL("CREATE TABLE movimenti (" +
                    "movtipo integer DEFAULT 0," + // 0 = bolla val, 1 = bolla qta, 2 = fattura, 3 = corrisp., 4 = bolla di reso, 5 = ddt carico integrazione, 6 = scarico a sede, 7 = trasf.a mezzo, 8 = trasf.da mezzo, 10 = inventario, 11 = doc.rett.car., 20 = ordine cliente
                    "movdep VARCHAR(10) DEFAULT ''," + // deposito del movimento (da deposito evento attuale)
                    "movdoc VARCHAR(10) DEFAULT ''," +
                    "movsez VARCHAR(10) DEFAULT ''," +
                    "movdata VARCHAR(10) DEFAULT ''," +
                    "movnum integer DEFAULT 0," +
                    "movdocora VARCHAR(5) DEFAULT ''," +
                    "movtrasf VARCHAR(1) DEFAULT 'N'," + // trasferito S-N
                    "movsospeso VARCHAR(1) DEFAULT 'N'," + // sospeso (solo ddt) S-N
                    "movclicod VARCHAR(10) DEFAULT ''," +
                    "movdestcod VARCHAR(10) DEFAULT ''," +
                    "movspeseinc VARCHAR(1) DEFAULT 'N'," + // S-N
                    "movspesebolli VARCHAR(1) DEFAULT 'N'," + // S-N
                    "movspesetrasp VARCHAR(1) DEFAULT 'N'," + // S-N
                    "movspesecauz VARCHAR(1) DEFAULT 'N'," + // S-N
                    "movcodlist VARCHAR(10) DEFAULT ''," +
                    "movnprz integer DEFAULT 0," +
                    "movbancacod VARCHAR(10) DEFAULT ''," +
                    "movagenzia VARCHAR(255) DEFAULT ''," +
                    "movcab VARCHAR(7) DEFAULT ''," +
                    "movpagcod VARCHAR(10) DEFAULT ''," +
                    "movaspbeni VARCHAR(20) DEFAULT ''," +
                    "movnumcolli integer DEFAULT 0," +
                    "movpeso real DEFAULT 0," +
                    "movnote1 VARCHAR(50) DEFAULT ''," +
                    "movnote2 VARCHAR(50) DEFAULT ''," +
                    "movnote3 VARCHAR(50) DEFAULT ''," +
                    "movnote4 VARCHAR(50) DEFAULT ''," +
                    "movnote5 VARCHAR(50) DEFAULT ''," +
                    "movnote6 VARCHAR(50) DEFAULT ''," +
                    "movtotlordomerce real DEFAULT 0," +
                    "movtotsconticli real DEFAULT 0," +
                    "movtotscontiart real DEFAULT 0," +
                    "movomaggi real DEFAULT 0," +
                    "movtotmerce real DEFAULT 0," +
                    "movtotnetti real DEFAULT 0," +
                    "movtotspeseinc real DEFAULT 0," +
                    "movtotspesetrasp real DEFAULT 0," +
                    "movtotspesebol real DEFAULT 0," +
                    "movtotimp real DEFAULT 0," +
                    "movtotiva real DEFAULT 0," +
                    "movtotale real DEFAULT 0," +
                    "movaddeb real DEFAULT 0," +
                    "movaccred real DEFAULT 0," +
                    "movabbuoni real DEFAULT 0," +
                    "movnettoapag real DEFAULT 0," +
                    "movacconto real DEFAULT 0," +
                    "movnrif VARCHAR(50) DEFAULT ''," +
                    "movoperatore VARCHAR(50) DEFAULT ''," +
                    "movevaso integer DEFAULT 0," +
                    "movart62 integer DEFAULT 0," +
                    "movkey VARCHAR(255) DEFAULT ''," +
                    "movcesscod VARCHAR(10) DEFAULT ''," +
                    "movdatacons VARCHAR(10) DEFAULT ''," +
                    "movfor VARCHAR(10) DEFAULT ''," +
                    "movaccisa real DEFAULT 0," +
                    "movcalcgiac VARCHAR(1) DEFAULT 'S'," + // S documento utile per il calcola della giacenza, N quando riceviamo il carico.
                    "primary key (movtipo,movdoc,movsez,movdata,movnum))");
            db.execSQL("CREATE INDEX mtipo ON movimenti (movtipo)");
            db.execSQL("CREATE INDEX mnum ON movimenti (movnum)");
            db.execSQL("CREATE INDEX mccod ON movimenti (movclicod)");
            db.execSQL("CREATE INDEX mdcod ON movimenti (movdestcod)");
            db.execSQL("CREATE INDEX mtr ON movimenti (movtrasf)");
            db.execSQL("CREATE INDEX mccg ON movimenti (movcalcgiac)");
            db.execSQL("CREATE INDEX msosp ON movimenti (movsospeso)");


            db.execSQL("CREATE TABLE righemov ("
                    + "rmmovtipo INTEGER DEFAULT 0,"// 0 = bolla val, 1 = bolla qta, 2 = fattura, 3 = corrisp., 4 = ddt carico
                    + "rmmovdoc VARCHAR(10) DEFAULT '',"
                    + "rmmovsez VARCHAR(10) DEFAULT '',"
                    + "rmmovdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "rmmovnum INTEGER DEFAULT 0,"
                    + "rmriga INTEGER DEFAULT 0,"
                    + "rmtiporiga VARCHAR(1) DEFAULT ''," // [A]articolo, [N]non cod, [D]descrittivo
                    + "rmartcod VARCHAR(20) DEFAULT '',"
                    + "rmlotto VARCHAR(20) DEFAULT '',"
                    + "rmcaumag VARCHAR(2)DEFAULT '',"
                    + // [VE]vendita,[RV]reso vend.,[RN]reso non
                    // vend.,[RC]Rettifica Contabile,[OM]omaggio,[SM]sconto
                    // merce
                    "rmcolli INTEGER DEFAULT 0,"
                    + "rmpezzi Real DEFAULT 0,"
                    + "rmcontenuto Real DEFAULT 0,"
                    + "rmqta Real DEFAULT 0,"
                    + "rmprz Real DEFAULT 0,"
                    + "rmartsc1 Real DEFAULT 0,"
                    + "rmartsc2 Real DEFAULT 0,"
                    + "rmscvend Real DEFAULT 0,"
                    + "rmcodiva VARCHAR(3) DEFAULT '',"
                    + "rmaliq Real DEFAULT 0,"
                    + "rmclisc1 Real DEFAULT 0,"
                    + "rmclisc2 Real DEFAULT 0,"
                    + "rmclisc3 Real DEFAULT 0,"
                    + "rmlordo Real DEFAULT 0,"
                    + "rmnetto Real DEFAULT 0,"
                    + "rmnettoivato Real DEFAULT 0,"
                    + "rmesclscontitestata INTEGER DEFAULT 0,"
                    + "rmaccisa Real DEFAULT 0,"
                    + "rmgrado Real DEFAULT 0,"
                    + "rmplato Real DEFAULT 0,"
                    + "primary key (rmmovtipo,rmmovdoc,rmmovsez,rmmovdata,rmmovnum,rmriga)" + ")");
            db.execSQL("CREATE INDEX martcod ON righemov (rmartcod)");
            db.execSQL("CREATE INDEX mlotto ON righemov (rmlotto)");
            db.execSQL("CREATE INDEX rmtipo ON righemov (rmmovtipo)");
            db.execSQL("CREATE INDEX rmnum ON righemov (rmmovnum)");
            db.execSQL("CREATE INDEX rmcaumag ON righemov (rmcaumag)");

            db.execSQL("CREATE TABLE ivamov ("
                    + "immovtipo INTEGER DEFAULT 0,"// 0 = bolla val, 1 = bolla qta, 2 = fattura, 3 = corrisp., 4 = ddt carico
                    + "immovdoc VARCHAR(10) DEFAULT '',"
                    + "immovsez VARCHAR(10) DEFAULT '',"
                    + "immovdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "immovnum INTEGER DEFAULT 0,"
                    + "imriga INTEGER DEFAULT 0,"
                    + "imcodiva VARCHAR(3) DEFAULT '',"
                    + "imtipo INTEGER DEFAULT 0,"
                    + "imaliq Real DEFAULT 0,"
                    + "imimp Real DEFAULT 0,"
                    + "imiva Real DEFAULT 0,"
                    + "imivaindetr Real DEFAULT 0,"
                    + "primary key (immovtipo,immovdoc,immovsez,immovdata,immovnum,imriga)" + ")");

            db.execSQL("CREATE TABLE cauzmov ("
                    + "cmmovtipo INTEGER DEFAULT 0,"// 0 = bolla val, 1 = bolla qta, 2 = fattura, 3 = corrisp., 4 = ddt carico
                    + "cmmovdoc VARCHAR(10) DEFAULT '',"
                    + "cmmovsez VARCHAR(10) DEFAULT '',"
                    + "cmmovdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "cmmovnum INTEGER DEFAULT 0,"
                    + "cmriga INTEGER DEFAULT 0,"
                    + "cmclicod VARCHAR(10) DEFAULT '',"
                    + "cmcauzcod VARCHAR(10) DEFAULT '',"
                    + "cmcauzprezzo Real DEFAULT 0,"
                    + "cmpresi INTEGER DEFAULT 0,"
                    + "cmresi INTEGER DEFAULT 0,"
                    + "cmaddeb Real DEFAULT 0,"
                    + "cmaccred Real DEFAULT 0,"
                    + "cmins integer DEFAULT 0,"
                    + "primary key (cmmovtipo,cmmovdoc,cmmovsez,cmmovdata,cmmovnum,cmriga)" + ")");

            db.execSQL("CREATE TABLE lottimov ("
                    + "lmmovtipo INTEGER DEFAULT 0,"// 0 = bolla val, 1 = bolla qta, 2 = fattura, 3 = corrisp., 4 = ddt carico
                    + "lmmovdoc VARCHAR(10) DEFAULT '',"
                    + "lmmovsez VARCHAR(10) DEFAULT '',"
                    + "lmmovdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "lmmovnum INTEGER DEFAULT 0,"
                    + "lmriga INTEGER DEFAULT 0,"
                    + "lmlotcod VARCHAR(20) DEFAULT '',"
                    + "lmlotartcod VARCHAR(20) DEFAULT '',"
                    + "lmtipomov VARCHAR(1) DEFAULT ''," // [C]arichi - [S]scarichi
                    + "lmpezzi INTEGER DEFAULT 0,"
                    + "lmqta Real DEFAULT 0,"
                    + "primary key (lmmovtipo,lmmovdoc,lmmovsez,lmmovdata,lmmovnum,lmriga)" + ")");

            db.execSQL("CREATE TABLE scadmov ("
                    + "smmovtipo INTEGER DEFAULT 0,"// 0 = bolla val, 1 = bolla qta, 2 = fattura, 3 = corrisp., 4 = ddt carico
                    + "smmovdoc VARCHAR(10) DEFAULT '',"
                    + "smmovsez VARCHAR(10) DEFAULT '',"
                    + "smmovdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "smmovnum INTEGER DEFAULT 0,"
                    + "smriga INTEGER DEFAULT 0,"
                    + "smdata VARCHAR(10) DEFAULT '',"
                    + "smimp Real DEFAULT 0,"
                    + "primary key (smmovtipo,smmovdoc,smmovsez,smmovdata,smmovnum,smriga)" + ")");

            db.execSQL("CREATE TABLE nuovicli ("
                    + "ncmovtipo INTEGER DEFAULT 0,"// 0 = bolla val, 1 = bolla qta, 2 = fattura, 3 = corrisp., 4 = ddt carico
                    + "ncmovdoc VARCHAR(10) DEFAULT '',"
                    + "ncmovsez VARCHAR(10) DEFAULT '',"
                    + "ncmovdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "ncmovnum INTEGER DEFAULT 0,"
                    + "ncnome VARCHAR(255) DEFAULT '',"
                    + "ncind VARCHAR(255) DEFAULT '',"
                    + "ncpiva VARCHAR(16) DEFAULT '',"
                    + "primary key (ncmovtipo,ncmovdoc,ncmovsez,ncmovdata,ncmovnum)" + ")");

            db.execSQL("CREATE TABLE incassi ("
                    + "isctipo VARCHAR(1) DEFAULT '',"
                    + // [F]fattura,[N]nota credito,[B]bolla,[C]generico
                    "iscsezdoc VARCHAR(10) DEFAULT '',"
                    + "iscdatadoc VARCHAR(10) DEFAULT '0000-00-00',"
                    + "iscnumdoc INTEGER DEFAULT 0,"
                    + // [V]vendita, [R]reso
                    "isctipoop VARCHAR(1) DEFAULT '',"
                    + "iscclicod VARCHAR(10) DEFAULT '',"
                    + "iscdestcod VARCHAR(10) DEFAULT '',"
                    + "iscscadid INTEGER DEFAULT -1,"
                    + "iscdatascad VARCHAR(10) DEFAULT '0000-00-00',"
                    + "isctiposcad INTEGER DEFAULT 0,"
                    + "iscresiduoscad Real DEFAULT 0,"
                    + "isaldo Real DEFAULT 0,"
                    + "isctrasf VARCHAR(1) DEFAULT 'N',"
                    + "iscdescr VARCHAR(255) DEFAULT '',"
                    + "iscdescr2 VARCHAR(255) DEFAULT '',"
                    + "isctipocassa INTEGER DEFAULT 0," // 0 = cassa contanti, 1 = cassa non contanti
                    + "iscdatainc VARCHAR(10) DEFAULT '0000-00-00',"
                    + "iscorainc VARCHAR(5) DEFAULT '00:00'" + ")");

            db.execSQL("CREATE TABLE storico_incassi ("
                    + "sisctipo VARCHAR(1) DEFAULT '',"
                    + // [F]fattura,[N]nota credito,[B]bolla,[C]generico
                    "siscsezdoc VARCHAR(10) DEFAULT '',"
                    + "siscdatadoc VARCHAR(10) DEFAULT '0000-00-00',"
                    + "siscnumdoc INTEGER DEFAULT 0,"
                    + // [V]vendita, [R]reso
                    "sisctipoop VARCHAR(1) DEFAULT '',"
                    + "siscclicod VARCHAR(10) DEFAULT '',"
                    + "siscdestcod VARCHAR(10) DEFAULT '',"
                    + "siscscadid INTEGER DEFAULT -1,"
                    + "siscdatascad VARCHAR(10) DEFAULT '0000-00-00',"
                    + "sisctiposcad INTEGER DEFAULT 0,"
                    + "siscresiduoscad Real DEFAULT 0,"
                    + "sisaldo Real DEFAULT 0,"
                    + "siscdescr VARCHAR(255) DEFAULT '',"
                    + "siscdescr2 VARCHAR(255) DEFAULT '',"
                    + "sisctipocassa INTEGER DEFAULT 0," // 0 = cassa contanti, 1 = cassa non contanti
                    + "siscdatainc VARCHAR(10) DEFAULT '0000-00-00',"
                    + "siscorainc VARCHAR(5) DEFAULT '00:00'" + ")");

            db.execSQL("CREATE TABLE proprieta ("
                    + "pcod VARCHAR(255) CONSTRAINT pkpropcod PRIMARY KEY,"
                    + "pval VARCHAR(255) DEFAULT ''" + ")");

            db.execSQL("CREATE TABLE password ("
                    + "pwdoperatore VARCHAR(255) DEFAULT '',"
                    + "pwdpassword VARCHAR(255) DEFAULT '')");

            db.execSQL("CREATE TABLE blocchicau ("
                    + "causale INTEGER DEFAULT 0,"
                    + "agecod VARCHAR(10) DEFAULT '',"
                    + "clicod VARCHAR(10) DEFAULT '',"
                    + "artcod VARCHAR(20) DEFAULT '',"
                    + "gita VARCHAR(10) DEFAULT '',"
                    + "blocco VARCHAR(10) DEFAULT '',"
                    + "destcod VARCHAR(10) DEFAULT '')");

            db.execSQL("CREATE TABLE ddtsospchiusi ("
                    + "dscsezdoc VARCHAR(10) DEFAULT '',"
                    + "dscdatadoc VARCHAR(10) DEFAULT '',"
                    + "dscnumdoc INTEGER DEFAULT 0,"
                    + "dscimporto Real DEFAULT 0,"
                    + "dsccodcli VARCHAR(10) DEFAULT '',"
                    + "dscnomecli VARCHAR(255) DEFAULT '')");

            db.execSQL("CREATE TABLE ddtfatt ("
                    + "suffatt VARCHAR(10) DEFAULT '',"
                    + "dtfatt VARCHAR(10) DEFAULT '',"
                    + "numfatt INTEGER DEFAULT 0,"
                    + "sufddt VARCHAR(10) DEFAULT '',"
                    + "dtddt VARCHAR(10) DEFAULT '',"
                    + "numddt INTEGER DEFAULT 0)");
            db.execSQL("CREATE INDEX df1 ON ddtfatt (suffatt,dtfatt,numfatt)");
            db.execSQL("CREATE INDEX df2 ON ddtfatt (sufddt,dtddt,numddt)");

            db.execSQL("CREATE TABLE msgmov ("
                    + "msgmovtipo INTEGER DEFAULT 0,"// 0 = bolla val, 1 = bolla qta, 2 = fattura, 3 = corrisp., 4 = ddt carico
                    + "msgmovdoc VARCHAR(10) DEFAULT '',"
                    + "msgmovsez VARCHAR(10) DEFAULT '',"
                    + "msgmovdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "msgmovnum INTEGER DEFAULT 0,"
                    + "msgtipo INTEGER DEFAULT 0,"
                    + "msgtesto VARCHAR(255) DEFAULT '')");
            db.execSQL("CREATE INDEX msg1 ON msgmov (msgmovtipo,msgmovdoc,msgmovsez,msgmovdata,msgmovnum)");

            db.execSQL("CREATE TABLE movcaricosede ("
                    + "mcsez VARCHAR(10) DEFAULT '',"
                    + "mcdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "mcnum INTEGER DEFAULT 0,"
                    + "mcartcod VARCHAR(20) DEFAULT '',"
                    + "mclotto VARCHAR(20) DEFAULT '',"
                    + "mcqta Real DEFAULT 0,"
                    + "mcprezzo Real DEFAULT 0,"
                    + "mcnetto Real DEFAULT 0)");
            db.execSQL("CREATE INDEX mc1 ON movcaricosede (mcsez,mcdata,mcnum)");

            db.execSQL("CREATE TABLE movcaricosedestor ("
                    + "mcsez VARCHAR(10) DEFAULT '',"
                    + "mcdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "mcnum INTEGER DEFAULT 0,"
                    + "mcartcod VARCHAR(20) DEFAULT '',"
                    + "mclotto VARCHAR(20) DEFAULT '',"
                    + "mcqta Real DEFAULT 0,"
                    + "mcprezzo Real DEFAULT 0,"
                    + "mcnetto Real DEFAULT 0)");
            db.execSQL("CREATE INDEX mc1s ON movcaricosedestor (mcsez,mcdata,mcnum)");

            db.execSQL("CREATE TABLE movcaricati ("
                    + "mcarsez VARCHAR(10) DEFAULT '',"
                    + "mcardata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "mcarnum INTEGER DEFAULT 0)");
            db.execSQL("CREATE INDEX mcar1 ON movcaricati (mcarsez,mcardata,mcarnum)");

            db.execSQL("CREATE TABLE blocchicli ("
                    + "clicod VARCHAR(10) DEFAULT '',"
                    + "data VARCHAR(10) DEFAULT '',"
                    + "importo Real DEFAULT 0,"
                    + "trasf VARCHAR(1) DEFAULT 'N',"
                    + "primary key (clicod,data)" + ")");
            db.execSQL("CREATE INDEX blcli1 ON blocchicli (trasf)");

            db.execSQL("CREATE TABLE blocchicli_fineg ("
                    + "clicod VARCHAR(10) DEFAULT '')");

            db.execSQL("CREATE TABLE preferiti ("
                    + "prefclicod VARCHAR(10) DEFAULT '',"
                    + "prefartcod VARCHAR(20) DEFAULT '',"
                    + "prefprezzo Real DEFAULT 0,"
                    + "primary key (prefclicod,prefartcod)" + ")");

            db.execSQL("CREATE TABLE imgart ("
                    + "iartcod VARCHAR(20) DEFAULT '',"
                    + "ifileimg VARCHAR(255) DEFAULT '',"
                    + "primary key (iartcod)" + ")");

            db.execSQL("CREATE TABLE daticessionari ("
                    + "dcclicod VARCHAR(10) DEFAULT '',"
                    + "dcartcod VARCHAR(10) DEFAULT '',"
                    + "dcconscod VARCHAR(10) DEFAULT '',"
                    + "dcartcodext VARCHAR(20) DEFAULT '')");
            db.execSQL("CREATE INDEX dc1 ON daticessionari (dcclicod,dcartcod,dcconscod)");

            db.execSQL("CREATE TABLE prezzibase ("
                    + "pbartcod VARCHAR(10) DEFAULT '',"
                    + "pblotcod VARCHAR(20) DEFAULT '',"
                    + "pbdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "pbprezzo Real DEFAULT 0,"
                    + "pbprezzoacq Real DEFAULT 0,"
                    + "pbprezzoven Real DEFAULT 0)");
            db.execSQL("CREATE INDEX pb1 ON prezzibase (pbartcod)");
            db.execSQL("CREATE INDEX pb2 ON prezzibase (pbartcod,pblotcod)");
            db.execSQL("CREATE INDEX pb3 ON prezzibase (pbdata)");

            db.execSQL("CREATE TABLE cliartiva ("
                    + "clicod VARCHAR(10) DEFAULT '',"
                    + "artcod VARCHAR(10) DEFAULT '',"
                    + "ivacod VARCHAR(10) DEFAULT '')");

            db.execSQL("CREATE TABLE log (" +
                    "logcod integer," +
                    "logdata nvarchar(10)," +
                    "logora nvarchar(10)," +
                    "logop nvarchar(10)," +
                    "logdescr nvarchar(255))");

            db.execSQL("CREATE TABLE logop (" +
                    "logdata nvarchar(10)," +
                    "logora nvarchar(10)," +
                    "logdescr nvarchar(255))");

            db.execSQL("CREATE TABLE fornitori ("
                    + "forcodice VARCHAR(10) DEFAULT '' CONSTRAINT pkforcod PRIMARY KEY,"
                    + "fornome VARCHAR(255) DEFAULT '',"
                    + "forsc1 Real DEFAULT 0,"
                    + "forsc2 Real DEFAULT 0,"
                    + "forsc3 Real DEFAULT 0,"
                    + "forric1 Real DEFAULT 0,"
                    + "forric2 Real DEFAULT 0,"
                    + "forcodlist VARCHAR(10) DEFAULT '',"
                    + "forindir VARCHAR(255) DEFAULT '',"
                    + "forcap VARCHAR(10) DEFAULT '',"
                    + "forloc VARCHAR(255) DEFAULT '',"
                    + "forprov VARCHAR(2) DEFAULT '',"
                    + "forpiva VARCHAR(16) DEFAULT '',"
                    + "forcodpag VARCHAR(10) DEFAULT '',"
                    + "forcodiva VARCHAR(3) DEFAULT '',"
                    + "fornumtel VARCHAR(20) DEFAULT '',"
                    + "fornumcell VARCHAR(20) DEFAULT '',"
                    + "fornumfax VARCHAR(20) DEFAULT '')");
            db.execSQL("CREATE INDEX tforcod ON fornitori (forcodice)");
            db.execSQL("CREATE INDEX tfornome ON fornitori (fornome)");

            db.execSQL("CREATE TABLE provenienze ("
                    + "prcodice VARCHAR(10) DEFAULT '' CONSTRAINT pkforcod PRIMARY KEY,"
                    + "prforcod VARCHAR(10) DEFAULT '',"
                    + "prdescr VARCHAR(255) DEFAULT '',"
                    + "prindir VARCHAR(255) DEFAULT '',"
                    + "prcap VARCHAR(10) DEFAULT '',"
                    + "prloc VARCHAR(255) DEFAULT '',"
                    + "prprov VARCHAR(2) DEFAULT '')");
            db.execSQL("CREATE INDEX tprcod ON provenienze (prcodice)");
            db.execSQL("CREATE INDEX tprdescr ON provenienze (prdescr)");
            db.execSQL("CREATE INDEX tprforcod ON provenienze (prforcod)");

            db.execSQL("CREATE TABLE forart ("
                    + "faartcod VARCHAR(20) DEFAULT '',"
                    + "faforcod VARCHAR(10) DEFAULT '',"
                    + "facodext VARCHAR(20) DEFAULT '',"
                    + "faqtaordmin Real DEFAULT 0,"
                    + "faqtaordmax Real DEFAULT 0,"
                    + "faqtaordmult Real DEFAULT 0)");
            db.execSQL("CREATE INDEX tfacod ON forart (faartcod)");
            db.execSQL("CREATE INDEX tffcod ON forart (faforcod)");
            db.execSQL("CREATE INDEX tfextcod ON forart (facodext)");

            db.execSQL("CREATE TABLE forartpv ("
                    + "fapvforcod VARCHAR(10) DEFAULT '',"
                    + "fapvpvcod VARCHAR(10) DEFAULT '',"
                    + "fapvartcod VARCHAR(20) DEFAULT '')");
            db.execSQL("CREATE INDEX tfapvforcod ON forartpv (fapvforcod)");
            db.execSQL("CREATE INDEX tfapvpvcod ON forartpv (fapvpvcod)");

            db.execSQL("CREATE TABLE posmag ("
                    + "pmartcod VARCHAR(20) DEFAULT '',"
                    + "pmdepcod VARCHAR(10) DEFAULT '',"
                    + "pmpos1 VARCHAR(255) DEFAULT '',"
                    + "pmpos2 VARCHAR(255) DEFAULT '',"
                    + "pmpos3 VARCHAR(255) DEFAULT '',"
                    + "pmpos4 VARCHAR(255) DEFAULT '',"
                    + "pmscortamin Real DEFAULT 0,"
                    + "pmscortamax Real DEFAULT 0)");
            db.execSQL("CREATE INDEX tpmacod ON posmag (pmartcod)");
            db.execSQL("CREATE INDEX tpmdcod ON posmag (pmdepcod)");

            db.execSQL("CREATE TABLE rettgiac ("
                    + "rgtrasf VARCHAR(1) DEFAULT 'N',"
                    + "rgdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "rgora VARCHAR(5) DEFAULT '00:00',"
                    + "rgartcod VARCHAR(20) DEFAULT '',"
                    + "rglotto VARCHAR(20) DEFAULT '',"
                    + "rgqtaorig Real DEFAULT 0,"
                    + "rgqtanuova Real DEFAULT 0)");
            db.execSQL("CREATE INDEX rgtr ON rettgiac (rgtrasf)");
            db.execSQL("CREATE INDEX rgdt ON rettgiac (rgdata,rgora)");
            db.execSQL("CREATE INDEX rgacod ON rettgiac (rgartcod)");
            db.execSQL("CREATE INDEX rglcod ON rettgiac (rglotto)");

            db.execSQL("CREATE TABLE storico_ddtcarico ("
                    + "stdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "stora VARCHAR(8) DEFAULT '00:00:00',"
                    + "stnum integer DEFAULT 0,"
                    + "sttrasf VARCHAR(1) DEFAULT 'N',"
                    + "startcod VARCHAR(20) DEFAULT '',"
                    + "startdescr VARCHAR(255) DEFAULT '',"
                    + "stum VARCHAR(255) DEFAULT '',"
                    + "stlotto VARCHAR(20) DEFAULT '',"
                    + "straggrcod VARCHAR(20) DEFAULT '',"
                    + "straggrdescr VARCHAR(255) DEFAULT '',"
                    + "stqta Real DEFAULT 0,"
                    + "stprezzo Real DEFAULT 0)");
            db.execSQL("CREATE INDEX stddtc ON storico_ddtcarico (stdata,stora,stnum)");

            db.execSQL("CREATE TABLE storico_fineg ("
                    + "fgdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fgora VARCHAR(8) DEFAULT '00:00:00',"
                    + "fgnumddt integer DEFAULT 0,"
                    + "fgminddt integer DEFAULT 0,"
                    + "fgmaxddt integer DEFAULT 0,"
                    + "fgnumddtreso integer DEFAULT 0,"
                    + "fgminddtreso integer DEFAULT 0,"
                    + "fgmaxddtreso integer DEFAULT 0,"
                    + "fgnumddtqta integer DEFAULT 0,"
                    + "fgminddtqta integer DEFAULT 0,"
                    + "fgmaxddtqta integer DEFAULT 0,"
                    + "fgnumfatt integer DEFAULT 0,"
                    + "fgminfatt integer DEFAULT 0,"
                    + "fgmaxfatt integer DEFAULT 0,"
                    + "fgnumcorrisp integer DEFAULT 0,"
                    + "fgmincorrisp integer DEFAULT 0,"
                    + "fgmaxcorrisp integer DEFAULT 0,"
                    + "fgnumddtann integer DEFAULT 0,"
                    + "fgnumddtom integer DEFAULT 0,"
                    + "fgnumcarint integer DEFAULT 0,"
                    + "fgnumscasede integer DEFAULT 0,"
                    + "fgnumtrasfamezzo integer DEFAULT 0,"
                    + "fgnumtrasfdamezzo integer DEFAULT 0,"
                    + "fgtotincassigiorno Real DEFAULT 0,"
                    + "fgtotincassisospesi Real DEFAULT 0,"
                    + "fgtotincassiftnc Real DEFAULT 0,"
                    + "fgsospesidainc Real DEFAULT 0,"
                    + "fgincassigiornocontanti Real DEFAULT 0,"
                    + "fgincassigiornononcontanti Real DEFAULT 0,"
                    + "fgincassisospesicontanti Real DEFAULT 0,"
                    + "fgincassisospesinoncontanti Real DEFAULT 0,"
                    + "fgincassiftnccontanti Real DEFAULT 0,"
                    + "fgincassiftncnoncontanti Real DEFAULT 0,"
                    + "fgtotvendite Real DEFAULT 0,"
                    + "fgtotcosti Real DEFAULT 0,"
                    + "fgtotcostiom Real DEFAULT 0,"
                    + "fgtotcostism Real DEFAULT 0,"
                    + "fgpercvendcosti Real DEFAULT 0)");
            db.execSQL("CREATE INDEX fg1 ON storico_fineg (fgdata,fgora)");

            // tab.ddt annullati
            db.execSQL("CREATE TABLE storico_fineg_ddtann ("
                    + "fg1data VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg1ora VARCHAR(8) DEFAULT '00:00:00',"
                    + "fg1tipo integer DEFAULT 0,"
                    + "fg1num integer DEFAULT 0,"
                    + "fg1datadoc VARCHAR(10) DEFAULT '0000-00-00')");
            db.execSQL("CREATE INDEX fg11 ON storico_fineg_ddtann (fg1data,fg1ora)");

            // tab.ddt con omaggi
            db.execSQL("CREATE TABLE storico_fineg_ddtom ("
                    + "fg2data VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg2ora VARCHAR(8) DEFAULT '00:00:00',"
                    + "fg2datadoc VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg2num integer DEFAULT 0,"
                    + "fg2artcod VARCHAR(20) DEFAULT '',"
                    + "fg2artdescr VARCHAR(255) DEFAULT '',"
                    + "fg2qta Real DEFAULT 0,"
                    + "fg2caumag VARCHAR(10) DEFAULT '')");
            db.execSQL("CREATE INDEX fg21 ON storico_fineg_ddtom (fg2data,fg2ora)");

            // tab.incassi giorno
            db.execSQL("CREATE TABLE storico_fineg_incassigiorno ("
                    + "fg3data VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg3ora VARCHAR(8) DEFAULT '00:00:00',"
                    + "fg3clicod VARCHAR(10) DEFAULT '',"
                    + "fg3clinome VARCHAR(255) DEFAULT '',"
                    + "fg3datadoc VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg3num integer DEFAULT 0,"
                    + "fg2acconto Real DEFAULT 0)");
            db.execSQL("CREATE INDEX fg31 ON storico_fineg_incassigiorno (fg3data,fg3ora)");

            // tab.incassi da sospesi
            db.execSQL("CREATE TABLE storico_fineg_incassisospesi ("
                    + "fg4data VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg4ora VARCHAR(8) DEFAULT '00:00:00',"
                    + "fg4clicod VARCHAR(10) DEFAULT '',"
                    + "fg4clinome VARCHAR(255) DEFAULT '',"
                    + "fg4datadoc VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg4num integer DEFAULT 0,"
                    + "fg4saldo Real DEFAULT 0)");
            db.execSQL("CREATE INDEX fg41 ON storico_fineg_incassisospesi (fg4data,fg4ora)");

            // tab.incassi da fatt nc
            db.execSQL("CREATE TABLE storico_fineg_incassiftnc ("
                    + "fg5data VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg5ora VARCHAR(8) DEFAULT '00:00:00',"
                    + "fg5tipo VARCHAR(10) DEFAULT '',"
                    + "fg5clicod VARCHAR(10) DEFAULT '',"
                    + "fg5descr VARCHAR(255) DEFAULT '',"
                    + "fg5datadoc VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg5num integer DEFAULT 0,"
                    + "fg5saldo Real DEFAULT 0)");
            db.execSQL("CREATE INDEX fg51 ON storico_fineg_incassiftnc (fg5data,fg5ora)");

            // tab.sospesi da incassare
            db.execSQL("CREATE TABLE storico_fineg_sospesidainc ("
                    + "fg6data VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg6ora VARCHAR(8) DEFAULT '00:00:00',"
                    + "fg6clicod VARCHAR(10) DEFAULT '',"
                    + "fg6clinome VARCHAR(255) DEFAULT '',"
                    + "fg6datadoc VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg6num integer DEFAULT 0,"
                    + "fg6sospeso Real DEFAULT 0)");
            db.execSQL("CREATE INDEX fg61 ON storico_fineg_sospesidainc (fg6data,fg6ora)");

            // tab.costi vendite
            db.execSQL("CREATE TABLE storico_fineg_costivendite ("
                    + "fg7data VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg7ora VARCHAR(8) DEFAULT '00:00:00',"
                    + "fg7num integer DEFAULT 0,"
                    + "fg7datadoc VARCHAR(10) DEFAULT '0000-00-00',"
                    + "fg7clicod VARCHAR(10) DEFAULT '',"
                    + "fg7totcosti Real DEFAULT 0,"
                    + "fg7totvend Real DEFAULT 0,"
                    + "fg7totkg Real DEFAULT 0,"
                    + "fg7percric Real DEFAULT 0)");
            db.execSQL("CREATE INDEX fg71 ON storico_fineg_costivendite (fg7data,fg7ora)");

            db.execSQL("CREATE TABLE inventario ("
                    + "invtrasf VARCHAR(1) DEFAULT 'N',"
                    + "invdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "invora VARCHAR(8) DEFAULT '00:00:00',"
                    + "invnum INTEGER DEFAULT 0,"
                    + "invartcod VARCHAR(20) DEFAULT '',"
                    + "invqta Real DEFAULT 0,"
                    + "invval Real DEFAULT 0)");
            db.execSQL("CREATE INDEX inv1 ON inventario (invdata,invora)");
            db.execSQL("CREATE INDEX inv2 ON inventario (invtrasf)");
            db.execSQL("CREATE INDEX inv3 ON inventario (invnum)");

            db.execSQL("CREATE TABLE datelotti ("
                    + "dlartcod VARCHAR(20) DEFAULT '',"
                    + "dllotcod VARCHAR(20) DEFAULT '',"
                    + "dldata VARCHAR(10) DEFAULT '0000-00-00')");
            db.execSQL("CREATE INDEX dtl1 ON datelotti (dllotcod,dlartcod)");

            db.execSQL("CREATE TABLE listini_term ("
                    + "lttrasf VARCHAR(1) DEFAULT 'N',"
                    + "ltcod VARCHAR(10) DEFAULT '',"
                    + "ltdescr VARCHAR(255) DEFAULT '',"
                    + "ltdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "ltora VARCHAR(8) DEFAULT '00:00:00')");
            db.execSQL("CREATE INDEX lt1 ON listini_term (ltcod)");
            db.execSQL("CREATE INDEX lt2 ON listini_term (lttrasf)");

            db.execSQL("CREATE TABLE listini_term_dett ("
                    + "ltdcodlist VARCHAR(10) DEFAULT '',"
                    + "ltdartcod VARCHAR(20) DEFAULT '',"
                    + "ltdprezzo Real DEFAULT 0)");
            db.execSQL("CREATE INDEX ltd1 ON listini_term_dett (ltdcodlist)");

            db.execSQL("CREATE TABLE varanag ("
                    + "vacodice VARCHAR(10) DEFAULT '',"
                    + "vatipo INTEGER DEFAULT 0,"
                    + "vavalore VARCHAR(255) DEFAULT '',"
                    + "vatrasf VARCHAR(1) DEFAULT 'N',"
                    + "vadata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "vaora VARCHAR(5) DEFAULT '00:00',"
                    + "primary key (vacodice,vatipo,vadata,vaora)" + ")");

            db.execSQL("CREATE TABLE giacart ("
                    + "giacartcod VARCHAR(20) DEFAULT '',"
                    + "giaccolli INTEGER DEFAULT 0,"
                    + "giacqta Real DEFAULT 0,"
                    + "giacdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "giacora VARCHAR(5) DEFAULT '00:00',"
                    + "primary key (giacartcod)" + ")");

            db.execSQL("CREATE TABLE giacart_tmp ("
                    + "giacartcod VARCHAR(20) DEFAULT '',"
                    + "giaccolli INTEGER DEFAULT 0,"
                    + "giacqta Real DEFAULT 0,"
                    + "giacdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "giacora VARCHAR(5) DEFAULT '00:00',"
                    + "primary key (giacartcod)" + ")");

            db.execSQL("CREATE TABLE giacartlotti ("
                    + "giaclartcod VARCHAR(20) DEFAULT '',"
                    + "giacllotto VARCHAR(255) DEFAULT '',"
                    + "giaclcolli INTEGER DEFAULT 0,"
                    + "giaclqta Real DEFAULT 0,"
                    + "giacldata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "giaclora VARCHAR(5) DEFAULT '00:00',"
                    + "giaclplato Real DEFAULT 0,"
                    + "giaclettogradi Real DEFAULT 0,"
                    + "primary key (giaclartcod,giacllotto)" + ")");

            db.execSQL("CREATE TABLE giacartlotti_tmp ("
                    + "giaclartcod VARCHAR(20) DEFAULT '',"
                    + "giacllotto VARCHAR(255) DEFAULT '',"
                    + "giaclcolli INTEGER DEFAULT 0,"
                    + "giaclqta Real DEFAULT 0,"
                    + "giacldata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "giaclora VARCHAR(5) DEFAULT '00:00',"
                    + "primary key (giaclartcod,giacllotto)" + ")");

            db.execSQL("CREATE TABLE dispart ("
                    + "dispartcod VARCHAR(20) DEFAULT '',"
                    + "dispcolli INTEGER DEFAULT 0,"
                    + "dispqta Real DEFAULT 0,"
                    + "dispdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "primary key (dispartcod)" + ")");

            db.execSQL("CREATE TABLE artaccisa ("
                    + "artaartcod VARCHAR(20) DEFAULT '',"
                    + "artarea6 INTEGER DEFAULT 0,"
                    + "artagrado Real DEFAULT 0,"
                    + "artaplato Real DEFAULT 0,"
                    + "artata16 VARCHAR(255) DEFAULT '',"
                    + "artaaccisa Real DEFAULT 0,"
                    + "artatipoacc INTEGER DEFAULT 0,"
                    + "artacontrassegno Real DEFAULT 0,"
                    + "artacn INTEGER DEFAULT 0,"
                    + "artanc VARCHAR(255) DEFAULT '',"
                    + "artaum VARCHAR(255) DEFAULT '',"
                    + "primary key (artaartcod)" + ")");

            db.execSQL("CREATE TABLE valaccisa ("
                    + "vaartcod VARCHAR(20) DEFAULT '',"
                    + "vanazione VARCHAR(255) DEFAULT '',"
                    + "vadataval VARCHAR(10) DEFAULT '0000-00-00',"
                    + "vaaccisa Real DEFAULT 0,"
                    + "vagradiiniz1 Real DEFAULT 0,"
                    + "vagradifine1 Real DEFAULT 0,"
                    + "vaaccgradi1 Real DEFAULT 0,"
                    + "vagradiiniz2 Real DEFAULT 0,"
                    + "vagradifine2 Real DEFAULT 0,"
                    + "vaaccgradi2 Real DEFAULT 0,"
                    + "vagradiiniz3 Real DEFAULT 0,"
                    + "vagradifine3 Real DEFAULT 0,"
                    + "vaaccgradi3 Real DEFAULT 0,"
                    + "vagradiiniz4 Real DEFAULT 0,"
                    + "vagradifine4 Real DEFAULT 0,"
                    + "vaaccgradi4 Real DEFAULT 0,"
                    + "vagradiiniz5 Real DEFAULT 0,"
                    + "vagradifine5 Real DEFAULT 0,"
                    + "vaaccgradi5 Real DEFAULT 0,"
                    + "vagradiiniz6 Real DEFAULT 0,"
                    + "vagradifine6 Real DEFAULT 0,"
                    + "vaaccgradi6 Real DEFAULT 0,"
                    + "vatipocalc INTEGER DEFAULT 0,"
                    + "primary key (vaartcod,vanazione,vadataval)" + ")");

            db.execSQL("CREATE TABLE eventi_venduto ("
                    + "evvevento varchar(255) DEFAULT '',"
                    + "evvartcod varchar(255) DEFAULT '',"
                    + "evvartdescr varchar(255) DEFAULT '',"
                    + "evvartum varchar(255) DEFAULT '',"
                    + "evvqta Real DEFAULT 0,"
                    + "evvvalore Real DEFAULT 0)");
            db.execSQL("CREATE INDEX evv1 ON eventi_venduto (evvevento)");

            db.execSQL("CREATE TABLE eventi_merceinviata ("
                    + "evievento varchar(255) DEFAULT '',"
                    + "eviartcod varchar(255) DEFAULT '',"
                    + "eviartdescr varchar(255) DEFAULT '',"
                    + "eviartum varchar(255) DEFAULT '',"
                    + "eviqta Real DEFAULT 0)");
            db.execSQL("CREATE INDEX evi1 ON eventi_merceinviata (evievento)");

            db.execSQL("CREATE TABLE eventi_mercetornata ("
                    + "evtevento varchar(255) DEFAULT '',"
                    + "evtartcod varchar(255) DEFAULT '',"
                    + "evtartdescr varchar(255) DEFAULT '',"
                    + "evtartum varchar(255) DEFAULT '',"
                    + "evtqta Real DEFAULT 0)");
            db.execSQL("CREATE INDEX evt1 ON eventi_mercetornata (evtevento)");

            db.execSQL("CREATE TABLE eventi_contatti ("
                    + "evcevento varchar(255) DEFAULT '',"
                    + "evcnome varchar(255) DEFAULT '',"
                    + "evccognome varchar(255) DEFAULT '',"
                    + "evcemail varchar(255) DEFAULT '',"
                    + "evctel varchar(255) DEFAULT '',"
                    + "evcmobile varchar(255) DEFAULT '',"
                    + "evcaziendaragsoc varchar(255) DEFAULT '',"
                    + "evcaziendaindir varchar(255) DEFAULT '',"
                    + "evcaziendaloc varchar(255) DEFAULT '',"
                    + "evcaziendaprov varchar(255) DEFAULT '',"
                    + "evcaziendaemail varchar(255) DEFAULT '',"
                    + "evcaziendasitoweb varchar(255) DEFAULT '',"
                    + "evccod varchar(255) DEFAULT '',"
                    + "evcnote varchar(255) DEFAULT '',"
                    + "evctrasfcliente varchar(255) DEFAULT '')");
            db.execSQL("CREATE INDEX evc1 ON eventi_contatti (evcevento)");

            db.execSQL("CREATE TABLE forord ("
                    + "foforcod VARCHAR(10) DEFAULT '',"
                    + "fopvcod VARCHAR(10) DEFAULT '',"
                    + "foggord1 integer DEFAULT 0,"
                    + "foggord2 integer DEFAULT 0,"
                    + "foggord3 integer DEFAULT 0,"
                    + "foggord4 integer DEFAULT 0,"
                    + "foggord5 integer DEFAULT 0,"
                    + "foggord6 integer DEFAULT 0,"
                    + "foggord7 integer DEFAULT 0,"
                    + "foqtamin Real DEFAULT 0,"
                    + "foggcons1 integer DEFAULT 0,"
                    + "foggcons2 integer DEFAULT 0,"
                    + "foggcons3 integer DEFAULT 0,"
                    + "foggcons4 integer DEFAULT 0,"
                    + "foggcons5 integer DEFAULT 0,"
                    + "foggcons6 integer DEFAULT 0,"
                    + "foggcons7 integer DEFAULT 0,"
                    + "foggappr1 integer DEFAULT 0,"
                    + "foggappr2 integer DEFAULT 0,"
                    + "foggappr3 integer DEFAULT 0,"
                    + "foggappr4 integer DEFAULT 0,"
                    + "foggappr5 integer DEFAULT 0,"
                    + "foggappr6 integer DEFAULT 0,"
                    + "foggappr7 integer DEFAULT 0)");
            db.execSQL("CREATE INDEX tfoforcod ON forord (foforcod)");
            db.execSQL("CREATE INDEX tfopvcod ON forord (fopvcod)");

            db.execSQL("CREATE TABLE statistiche ("
                    + "stclicod varchar(10) DEFAULT '',"
                    + "startcod varchar(10) DEFAULT '',"
                    + "startdescr varchar(255) DEFAULT '',"
                    + "stcolli integer DEFAULT 0,"
                    + "stqta Real DEFAULT 0,"
                    + "stvalore Real DEFAULT 0,"
                    + "stprezzom Real DEFAULT 0,"
                    + "stlitritot Real DEFAULT 0,"
                    + "sttotld2 Real DEFAULT 0,"
                    + "sttotld3 Real DEFAULT 0" +
                    ")");
            db.execSQL("CREATE INDEX tstclicod ON statistiche (stclicod)");


            db.execSQL("CREATE TABLE datitermreiniz ( " +
                    "numterm INTEGER DEFAULT 0," +
                    "user VARCHAR(20) DEFAULT ''," +
                    "pwd VARCHAR(20) DEFAULT ''," +
                    "activationcode VARCHAR(20) DEFAULT ''," +
                    "pin VARCHAR(255) DEFAULT ''," +
                    "serverjcloud VARCHAR(255) DEFAULT ''," +
                    "serverjcloud2 VARCHAR(255) DEFAULT ''," +
                    "versionedemo VARCHAR(1) DEFAULT 'N'," +
                    "pedcod VARCHAR(10) DEFAULT ''" +
                    ")");

            db.execSQL("CREATE TABLE dispartlotti ("
                    + "dlartcod VARCHAR(20) DEFAULT '',"
                    + "dllotto VARCHAR(20) DEFAULT '',"
                    + "dlcolli INTEGER DEFAULT 0,"
                    + "dlqta Real DEFAULT 0,"
                    + "dldata VARCHAR(10) DEFAULT '0000-00-00'"
                    + ")");

        } catch (Exception edb) {
            //edb.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.v("jbeerapp", "upgrade db versione " + newVersion);
        if (newVersion > oldVersion) {
            try {
                db.execSQL("ALTER TABLE righemov ADD COLUMN rmaccisa Real DEFAULT 0");
            } catch (Exception edb) {
                //edb.printStackTrace();
            }
            try {
                db.execSQL("ALTER TABLE righemov ADD COLUMN rmgrado Real DEFAULT 0");
            } catch (Exception edb) {
                //edb.printStackTrace();
            }
            try {
                db.execSQL("ALTER TABLE righemov ADD COLUMN rmplato Real DEFAULT 0");
            } catch (Exception edb) {
                //edb.printStackTrace();
            }
            try {
                db.execSQL("ALTER TABLE datiterm ADD COLUMN calcaccisa VARCHAR(1) DEFAULT 'N'");
            } catch (Exception edb) {
                //edb.printStackTrace();
            }
            try {
                db.execSQL("ALTER TABLE datiterm ADD COLUMN tipocalcaccisa INTEGER DEFAULT 0");
            } catch (Exception edb) {
                //edb.printStackTrace();
            }
            try {
                db.execSQL("ALTER TABLE datiterm ADD COLUMN voceaccisa VARCHAR(255) DEFAULT ''");
            } catch (Exception edb) {
                //edb.printStackTrace();
            }
            try {
                db.execSQL("ALTER TABLE movimenti ADD COLUMN movaccisa real DEFAULT 0");
            } catch (Exception edb) {
                //edb.printStackTrace();
            }
            try {
                db.execSQL("CREATE TABLE artaccisa ("
                        + "artaartcod VARCHAR(20) DEFAULT '',"
                        + "artarea6 INTEGER DEFAULT 0,"
                        + "artagrado Real DEFAULT 0,"
                        + "artaplato Real DEFAULT 0,"
                        + "artata16 VARCHAR(255) DEFAULT '',"
                        + "artaaccisa Real DEFAULT 0,"
                        + "artatipoacc INTEGER DEFAULT 0,"
                        + "artacontrassegno Real DEFAULT 0,"
                        + "artacn INTEGER DEFAULT 0,"
                        + "artanc VARCHAR(255) DEFAULT '',"
                        + "artaum VARCHAR(255) DEFAULT '',"
                        + "primary key (artaartcod)" + ")");
            } catch (Exception edb) {
                //edb.printStackTrace();
            }
        }

        try {
            db.execSQL("CREATE TABLE dispart ("
                    + "dispartcod VARCHAR(20) DEFAULT '',"
                    + "dispcolli INTEGER DEFAULT 0,"
                    + "dispqta Real DEFAULT 0,"
                    + "dispdata VARCHAR(10) DEFAULT '0000-00-00',"
                    + "primary key (dispartcod)" + ")");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("CREATE TABLE valaccisa ("
                    + "vaartcod VARCHAR(20) DEFAULT '',"
                    + "vanazione VARCHAR(255) DEFAULT '',"
                    + "vadataval VARCHAR(10) DEFAULT '0000-00-00',"
                    + "vaaccisa Real DEFAULT 0,"
                    + "vagradiiniz1 Real DEFAULT 0,"
                    + "vagradifine1 Real DEFAULT 0,"
                    + "vaaccgradi1 Real DEFAULT 0,"
                    + "vagradiiniz2 Real DEFAULT 0,"
                    + "vagradifine2 Real DEFAULT 0,"
                    + "vaaccgradi2 Real DEFAULT 0,"
                    + "vagradiiniz3 Real DEFAULT 0,"
                    + "vagradifine3 Real DEFAULT 0,"
                    + "vaaccgradi3 Real DEFAULT 0,"
                    + "vagradiiniz4 Real DEFAULT 0,"
                    + "vagradifine4 Real DEFAULT 0,"
                    + "vaaccgradi4 Real DEFAULT 0,"
                    + "vagradiiniz5 Real DEFAULT 0,"
                    + "vagradifine5 Real DEFAULT 0,"
                    + "vaaccgradi5 Real DEFAULT 0,"
                    + "vagradiiniz6 Real DEFAULT 0,"
                    + "vagradifine6 Real DEFAULT 0,"
                    + "vaaccgradi6 Real DEFAULT 0,"
                    + "vatipocalc INTEGER DEFAULT 0,"
                    + "primary key (vaartcod,vanazione,vadataval)" + ")");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }

        try {
            db.execSQL("ALTER TABLE cauzioni ADD COLUMN cauzcapacita real DEFAULT 0");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE cauzioni ADD COLUMN cauztipocalc INTEGER DEFAULT 0");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE articoli ADD COLUMN artcalcprzdoc INTEGER DEFAULT 0");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN dispart VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN orddepsede VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN pwdanndoc VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN pwdpar VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN pwdutilita VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN passcodetrasf VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN copieddt INTEGER DEFAULT 1");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN copieddtqta INTEGER DEFAULT 1");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN finegstampasospdainc VARCHAR(1) DEFAULT 'S'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN noannstampe VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN colli1 VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN usalistinonuovo VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN invioordineauto VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN ordinicligiacterm VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN nostampafinegiornata VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN orddepsede VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN finegdistintadenaro VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE clienti ADD COLUMN cliinsegna VARCHAR(255) DEFAULT ''");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE clienti ADD COLUMN clicodsdinuovo INTEGER DEFAULT 0");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN gestlotti INTEGER DEFAULT 0");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }

        try {
            db.execSQL("CREATE TABLE statistiche ("
                    + "stclicod varchar(10) DEFAULT '',"
                    + "startcod varchar(10) DEFAULT '',"
                    + "startdescr varchar(255) DEFAULT '',"
                    + "stcolli integer DEFAULT 0,"
                    + "stqta Real DEFAULT 0,"
                    + "stvalore Real DEFAULT 0,"
                    + "stprezzom Real DEFAULT 0,"
                    + "stlitritot Real DEFAULT 0,"
                    + "sttotld2 Real DEFAULT 0,"
                    + "sttotld3 Real DEFAULT 0" +
                    ")");
            db.execSQL("CREATE INDEX tstclicod ON statistiche (stclicod)");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }

/*        try {
            db.execSQL("ALTER TABLE righemov DROP PRIMARY KEY, ADD PRIMARY KEY (rmmovtipo,rmmovdoc,rmmovsez,rmmovdata,rmmovnum,rmriga,rmlotto)");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }*/
        try {
            db.execSQL("ALTER TABLE giacartlotti ADD COLUMN giaclplato Real DEFAULT 0");
            db.execSQL("ALTER TABLE giacartlotti ADD COLUMN giaclettogradi Real DEFAULT 0");

        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN serverjcloud2 VARCHAR(255) DEFAULT 'http://j-cloud.it:8080/J-Cloud'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("CREATE TABLE datitermreiniz ( " +
                    "numterm INTEGER DEFAULT 0," +
                    "user VARCHAR(20) DEFAULT ''," +
                    "pwd VARCHAR(20) DEFAULT ''," +
                    "activationcode VARCHAR(20) DEFAULT ''," +
                    "pin VARCHAR(255) DEFAULT ''," +
                    "serverjcloud VARCHAR(255) DEFAULT ''," +
                    "serverjcloud2 VARCHAR(255) DEFAULT ''," +
                    "versionedemo VARCHAR(1) DEFAULT 'N'," +
                    "pedcod VARCHAR(10) DEFAULT ''" +
                    ")");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE datitermreiniz ADD COLUMN serverjcloud2 VARCHAR(255) DEFAULT 'http://j-cloud.it:8080/J-Cloud'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("CREATE INDEX mccg ON movimenti (movcalcgiac)");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
        try {
            db.execSQL("ALTER TABLE movimenti ADD COLUMN movcalcgiac VARCHAR(1) DEFAULT 'S'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }

        try {
        db.execSQL("CREATE TABLE dispartlotti ("
                + "dlartcod VARCHAR(20) DEFAULT '',"
                + "dllotto VARCHAR(20) DEFAULT '',"
                + "dlcolli INTEGER DEFAULT 0,"
                + "dlqta Real DEFAULT 0,"
                + "dldata VARCHAR(10) DEFAULT '0000-00-00'"
                + ")");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }

        try {
            db.execSQL("ALTER TABLE datiterm ADD COLUMN moddoc VARCHAR(1) DEFAULT 'N'");
        } catch (Exception edb) {
            //edb.printStackTrace();
        }
    }

}
