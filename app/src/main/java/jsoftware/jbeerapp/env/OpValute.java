package jsoftware.jbeerapp.env;

import java.math.BigDecimal;

public class OpValute
{
    public static double convertiValuta(double imp, double cambio)
    {
        BigDecimal val = new BigDecimal(imp);
        val = val.setScale(16, BigDecimal.ROUND_HALF_UP);
        BigDecimal cmb = new BigDecimal(cambio);
        cmb = cmb.setScale(16, BigDecimal.ROUND_HALF_UP);
        val = val.divide(cmb, BigDecimal.ROUND_HALF_UP);
        return val.doubleValue();    
    }

    public static BigDecimal convertiValuta(BigDecimal imp, BigDecimal cambio)
    {
        imp = imp.divide(cambio, BigDecimal.ROUND_HALF_UP);
        return imp;
    }

    public static double convertiEuroLira(double imp)
    {
        BigDecimal val = new BigDecimal(imp);
        val = val.setScale(16, BigDecimal.ROUND_HALF_UP);
        BigDecimal cmb = new BigDecimal("0.0005164568990895");
        cmb = cmb.setScale(16, BigDecimal.ROUND_HALF_UP);
        val = val.divide(cmb, BigDecimal.ROUND_HALF_UP);
        return val.doubleValue();    
    }

    public static BigDecimal convertiEuroLira(BigDecimal imp)
    {
        BigDecimal cmb = new BigDecimal("0.0005164568990895");
        cmb = cmb.setScale(16, BigDecimal.ROUND_HALF_UP);
        imp = imp.divide(cmb, BigDecimal.ROUND_HALF_UP);
        return imp;
    }

    public static double convertiLiraEuro(double imp)
    {
        BigDecimal val = new BigDecimal(imp);
        val = val.setScale(16, BigDecimal.ROUND_HALF_UP);
        BigDecimal cmb = new BigDecimal("1936.27");
        cmb = cmb.setScale(16, BigDecimal.ROUND_HALF_UP);
        val = val.divide(cmb, BigDecimal.ROUND_HALF_UP);
        return val.doubleValue();    
    }

    public static BigDecimal convertiLiraEuro(BigDecimal imp)
    {
        BigDecimal cmb = new BigDecimal("1936.27");
        cmb = cmb.setScale(16, BigDecimal.ROUND_HALF_UP);
        imp = imp.divide(cmb, BigDecimal.ROUND_HALF_UP);
        return imp;
    }

    public static double convertiAziendaEsercizio(double impAz, int vaz, int ves)
    {
        if (vaz == ves)
            return impAz;
        else if (vaz == 1 && ves == 2) // lira -> euro
            return convertiLiraEuro(impAz);
        else
            return convertiEuroLira(impAz);
    }

    public static BigDecimal convertiAziendaEsercizio(BigDecimal impAz, int vaz, int ves)
    {
        if (vaz == ves)
            return impAz;
        else if (vaz == 1 && ves == 2) // lira -> euro
            return convertiLiraEuro(impAz);
        else
            return convertiEuroLira(impAz);
    }

    public static double arrotonda(double imp, double arrot)
    {
        if (imp != 0 && !Double.isInfinite(imp) && !Double.isNaN(imp))
        {
            if (arrot != 0)
            {
                double val = OpValute.arrotondaMat(imp / arrot, 8);
                if(val == (double) ((int) val))
                    return imp;
                else
                    return (double) ((((int) val) * arrot) + arrot);
            }
            else
                return imp;
        }
        else
            return 0;
    }

    public static double arrotondaEccesso(double imp, double arrot)
    {
        if (imp != 0 && !Double.isInfinite(imp) && !Double.isNaN(imp))
        {
            if (arrot != 0)
            {
                double val = imp / arrot;
                if(val == (double) ((int) val))
                    return imp;
                else
                    return (double) ((((int) val) * arrot) + arrot);
            }
            else
                return imp;
        }
        else
            return 0;
    }

    public static double arrotondaMatematico(double imp, double arrot)
    {
        if (imp != 0 && !Double.isInfinite(imp) && !Double.isNaN(imp))
        {
            if (arrot != 0)
            {
                double val = imp / arrot;
                if(val == (double) ((int) val))
                    return imp;
                else
                {
                    if ((imp - ((int) val) * arrot) > arrot / 2)
                        return (double) ((((int) val) * arrot) + arrot);
                    else
                        return (double) ((((int) val) * arrot));
                }
            }
            else
                return imp;
        }
        else
            return 0;
    }

    public static double arrotondaMat(double imp, int dec)
    {
        boolean negativo = false;
        if (imp != 0 && !Double.isInfinite(imp) && !Double.isNaN(imp))
        {
            if (imp < 0)
            {
                negativo = true;
                imp = Math.abs(imp);
            }
            BigDecimal val = new BigDecimal(imp + 0.000000001);
            val = val.setScale(dec, BigDecimal.ROUND_HALF_UP);
            double ris = val.doubleValue();
            if (negativo)
                ris = -ris;
            return ris;
        }
        else
            return 0;
    }

    public static double arrotondaDifetto(double imp, int dec)
    {
        if (imp != 0 && !Double.isInfinite(imp) && !Double.isNaN(imp))
        {
            BigDecimal val = new BigDecimal(imp + 0.0000001);
            val = val.setScale(dec, BigDecimal.ROUND_DOWN);
            return OpValute.arrotondaMat(val.doubleValue(), dec);
        }
        else
            return 0;
    }

    public static double arrotondaEccesso(double imp, int dec)
    {
        if (imp != 0 && !Double.isInfinite(imp) && !Double.isNaN(imp))
        {
            BigDecimal val = new BigDecimal(imp);
            val = val.setScale(dec, BigDecimal.ROUND_UP);
            return val.doubleValue();
        }
        else
            return 0;
    }
/*
    public static final boolean ugualeValuta(double impv1, int valu1, double impv2, int valu2, double cambio)
    {
        
    }
*/
    
    public static final double convValuta(double impv, int valuid, int valuconv, double cambio, int decvaluconv)
    {
        if (valuid == valuconv)
            return arrotondaMat(impv, decvaluconv);
        else if (valuid == 2 && valuconv == 1)
        {
            return arrotondaMat(impv * 1936.27, decvaluconv);
        }
        else if (valuid == 1 && valuconv == 2)
        {
            return arrotondaMat(impv / 1936.27, decvaluconv);
        }
        else
        {
            return arrotondaMat(impv * cambio, decvaluconv);
        }
    }

    public static final double convValutaTriang(double impv, int valuid, int valuconv, double cambio, double cambioInEuro, int decvaluconv)
    {
        if (valuid == valuconv)
            return arrotondaMat(impv, decvaluconv);
        else if (valuid == 2 && valuconv == 1)
        {
            // da euro a lira
            return arrotondaMat(impv * 1936.27, decvaluconv);
        }
        else if (valuid == 1 && valuconv == 2)
        {
            // da lira a euro
            return arrotondaMat(impv / 1936.27, decvaluconv);
        }
        else if (valuid == 2)
        {
            // da euro ad altra valuta
            return arrotondaMat(impv / cambioInEuro, decvaluconv);
        }
        else if (valuconv == 1)
        {
            // conv. a lire italiane
            return arrotondaMat(impv * cambioInEuro * 1936.27, decvaluconv);
        }
        else if (valuconv == 2)
        {
            // conv. a euro
            return arrotondaMat(impv * cambioInEuro, decvaluconv);
        }
        else if (valuid == 1)
        {
            // da lira ad altra valuta
            return arrotondaMat((impv / 1936.27) / cambioInEuro, decvaluconv);
        }
        else
        {
            // valute generiche
            return arrotondaMat(impv * cambio, decvaluconv);
        }
    }

    public static final double convAzEs(double impaz, int valuaz, int values, int decvalues)
    {
        if (valuaz == 2 && values == 1)
            return arrotondaMat(impaz * 1936.27, decvalues);
        else if (valuaz == 1 && values == 2)
            return arrotondaMat(impaz / 1936.27, decvalues);
        else
            return arrotondaMat(impaz, decvalues);
    }
}