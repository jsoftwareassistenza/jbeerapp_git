package jsoftware.jbeerapp.env;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by pgx71 on 16/12/2014.
 */
public class ListImageButton extends androidx.appcompat.widget.AppCompatImageButton
{
    public ListImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setFocusable(false);
    }

    @Override
    public void setPressed(boolean pressed) {
        if (pressed && ((View) getParent()).isPressed()) {
            return;
        }
        super.setPressed(pressed);
    }
}