package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FragmentDocumentoCarrello;

public class ListaDocDettAdapter extends ArrayAdapter<Record> {

    private final Context context;
    private final ArrayList<Record> values;
    public FragmentDocumentoCarrello padre;

    public ListaDocDettAdapter(Context context, ArrayList<Record> values, FragmentDocumentoCarrello padre) {
        super(context, R.layout.lvdoc_dett_item, values);
        this.context = context;
        this.values = values;
        this.padre = padre;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.lvdoc_dett_item, parent, false);
        TextView tvart = (TextView) rowView.findViewById(R.id.lvdocdett_art);
        TextView tvcau = (TextView) rowView.findViewById(R.id.lvdocdett_cau);
        TextView tvqta = (TextView) rowView.findViewById(R.id.lvdocdett_qta);
        TextView tvprz = (TextView) rowView.findViewById(R.id.lvdocdett_prezzo);
        TextView tvpercsc1 = (TextView) rowView.findViewById(R.id.lvdocdett_percsc1);
        TextView tvpercsc2 = (TextView) rowView.findViewById(R.id.lvdocdett_percsc2);
        TextView tvpercsc3 = (TextView) rowView.findViewById(R.id.lvdocdett_percsc3);
        TextView tvimp = (TextView) rowView.findViewById(R.id.lvdocdett_imp);
        TextView tvlotto = (TextView) rowView.findViewById(R.id.lvdocdett_lotto);

        Record rec = values.get(position);
        String artcod = rec.leggiStringa("rmartcod");
        String artdescr = rec.leggiStringa("rmartdescr");
        String caumag = rec.leggiStringa("rmcaumag");
        String codiva = rec.leggiStringa("rmcodiva");
        if (padre.fdoc.fcli.sptipodoc.getSelectedItemPosition() == 6) {
            if (codiva.equals(Env.codivaomaggiimp04) || codiva.equals(Env.codivaomaggiimp10) || codiva.equals(Env.codivaomaggiimp20) ||
                    codiva.equals(Env.codivaomaggitot04) || codiva.equals(Env.codivaomaggitot10) || codiva.equals(Env.codivaomaggitot20)) {
                caumag += "\n(omaggio)";
            }
        }
        double qta = rec.leggiDouble("rmqta");
        double prz = rec.leggiDouble("rmprz");
        double sc1 = rec.leggiDouble("rmartsc1");
        double sc2 = rec.leggiDouble("rmartsc2");
        double sc3 = rec.leggiDouble("rmscvend");
        double netto = rec.leggiDouble("rmnetto");
        String lotto = rec.leggiStringa("rmlotto");
        String cess = "";
        if (!rec.leggiStringa("cesscod").equals("")) {
            cess = " (cessionario " + rec.leggiStringa("cesscod");
            String[] pars = new String[1];
            pars[0] = rec.leggiStringa("cesscod");
            Cursor cc = Env.db.rawQuery("SELECT clinome FROM clienti WHERE clicodice = ?", pars);
            if (cc.moveToFirst()) {
                cess += "-" + cc.getString(0);
            }
            cc.close();
            cess += ")";
        }
        //tvart.setText(artcod + " " + artdescr + cess);
        if (Env.gestlotti) {
            tvart.setText(artdescr);
            tvlotto.setText("Lotto: " + lotto);
        } else {
            tvart.setText(artdescr);
            tvlotto.setVisibility(View.GONE);
        }
        tvcau.setText(caumag);
        tvqta.setText(Formattazione.formValuta(qta, 12, 0, 0));
        tvprz.setText(Formattazione.formValuta(prz, 12, 2, 0) + "€");
        String ss = "";
        if (sc1 > 0)
            tvpercsc1.setText(Formattazione.formValuta(sc1, 3, 1, 0) + "%");

        else
            tvpercsc1.setVisibility(View.GONE);
        //tvpercsc1.setText("");
        if (sc2 > 0)
            tvpercsc2.setText(Formattazione.formValuta(sc2, 3, 1, 0) + "%");
        else
            tvpercsc2.setVisibility(View.GONE);
        //tvpercsc2.setText("");
        if (sc3 > 0)
            tvpercsc3.setText(Formattazione.formValuta(sc3, 3, 1, 0) + "%");
        else
            tvpercsc3.setVisibility(View.GONE);
        //tvpercsc3.setText("");
        tvimp.setText(Formattazione.formValuta(netto, 12, 2, 0) + "€");
        if (netto == 0)
            tvimp.setTextColor(Color.RED);
        else
            tvimp.setTextColor(Color.BLACK);
/*		if (!lotto.equals(""))
			tvlotto.setText(lotto);
		else
			tvlotto.setText("");*/
        return rowView;
    }
}
