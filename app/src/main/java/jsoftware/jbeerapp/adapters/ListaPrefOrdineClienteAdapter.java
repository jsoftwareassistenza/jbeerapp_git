package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;

public class ListaPrefOrdineClienteAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;

	public ListaPrefOrdineClienteAdapter(Context context, ArrayList<Record> values) {
		super(context, R.layout.prefoc_item, values);
		this.context = context;
		this.values = values;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.prefoc_item, parent, false);
		TextView tvart = (TextView) rowView.findViewById(R.id.prefoc_art);
		TextView tvgiac = (TextView) rowView.findViewById(R.id.prefoc_artgiac);
		TextView tvprezzo = (TextView) rowView.findViewById(R.id.prefoc_prezzo);
        TextView tvprezzocli = (TextView) rowView.findViewById(R.id.prefoc_prezzocli);
		TextView tvqtavend = (TextView) rowView.findViewById(R.id.prefoc_qtavend);
        TextView tvcollivend = (TextView) rowView.findViewById(R.id.prefoc_collivend);
		ImageView img = (ImageView) rowView.findViewById(R.id.prefoc_icona);
		Record rec = values.get(position);
		String artcod = rec.leggiStringa("artcod");
		String artdescr = rec.leggiStringa("artdescr");
		double prz = rec.leggiDouble("prezzonetto");
        double przcli = rec.leggiDouble("prezzocli");

        int collivend = rec.leggiIntero("collivend");
        int collism = rec.leggiIntero("collism");
        int colliomimp = rec.leggiIntero("colliomimp");
        int colliomtot = rec.leggiIntero("colliomtot");
        int colliom = collism + colliomimp + colliomtot;

        double qtavend = rec.leggiDouble("qtavend");
        double qtasm = rec.leggiDouble("qtasm");
        double qtaomimp = rec.leggiDouble("qtaomimp");
        double qtaomtot = rec.leggiDouble("qtaomtot");
        double qtaom = qtasm + qtaomimp + qtaomtot;

		double giac = rec.leggiDouble("giac");
		tvart.setText(artcod + " " + artdescr);
		if (Env.dispart) {
			tvgiac.setText("Disp " + Formattazione.formValuta(giac, 12, 0, Formattazione.SEGNO_SX_NEG));
		} else {
			tvgiac.setText("Giac " + Formattazione.formValuta(giac, 12, 0, Formattazione.SEGNO_SX_NEG));
		}

        if (giac <= 0)
        {
            tvgiac.setTextColor(Color.RED);
        }
        else
        {
            tvgiac.setTextColor(Color.rgb(0, 100, 0));
        }
		tvprezzo.setText("Prezzo\n" + Formattazione.formValuta(prz, 12, 2, Formattazione.SEGNO_SX_NEG));
        tvprezzocli.setText("Prz. Cliente\n" + Formattazione.formValuta(przcli, 12, 2, Formattazione.SEGNO_SX_NEG));
        if (collivend > 0)
            tvcollivend.setText("Colli\n" + collivend);
        else
            tvcollivend.setText("");
		if (qtavend > 0)
			tvqtavend.setText("Qta\n" + Formattazione.formValuta(qtavend, 12, 1, Formattazione.NO_SEGNO));
		else
			tvqtavend.setText("");
		int not = rec.leggiIntero("notifica");
		if (not == 1)
			img.setImageResource(R.drawable.ic_announcement_black_48dp);
		else if (not == 2)
			img.setImageResource(R.drawable.ic_visibility_black_48dp);
		else if (not == 3)
			img.setImageResource(R.drawable.ic_trending_up_black_48dp);
		else if (not == 4)
			img.setImageResource(R.drawable.ic_trending_down_black_48dp);
		else
			img.setImageResource(R.drawable.ic_grade_black_48dp);
		//img.setImageResource(android.R.color.transparent);
		return rowView;
	}	
}
