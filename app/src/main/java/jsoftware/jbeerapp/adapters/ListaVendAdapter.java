package jsoftware.jbeerapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;

public class ListaVendAdapter extends ArrayAdapter<Record> {

    private final Context context;
    private final ArrayList<Record> values;

    public ListaVendAdapter(Context context, ArrayList<Record> values) {
        super(context, R.layout.listavend_item, values);
        this.context = context;
        this.values = values;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.listavend_item, parent, false);
        TextView tvart = (TextView) rowView.findViewById(R.id.listavend_art);
        //TextView tvlotto = (TextView) rowView.findViewById(R.id.listavend_lotto);
        TextView tvgiac = (TextView) rowView.findViewById(R.id.listavend_giac);
        TextView tvqtavend = (TextView) rowView.findViewById(R.id.listavend_qtavend);
        //TextView tvqtaom = (TextView) rowView.findViewById(R.id.listavend_qtaom);
        TextView tvprezzo = (TextView) rowView.findViewById(R.id.listavend_prezzo);
        Record rec = values.get(position);
        String artcod = rec.leggiStringa("artcod");
        String artdescr = rec.leggiStringa("artdescr");
        double giac = rec.leggiDouble("giac");
        double qtavend = rec.leggiDouble("qtavend");
        double qtasm = rec.leggiDouble("qtasm");
        double qtaomimp = rec.leggiDouble("qtaomimp");
        double qtaomtot = rec.leggiDouble("qtaomtot");
        double qtaom = qtasm + qtaomimp + qtaomtot;
        double prz = rec.leggiDouble("prezzonetto");
        tvart.setText(artcod + " " + artdescr);
        //tvlotto.setText(lotto);
        if (giac <= 0) {
            //tvgiac.setTextColor(Color.rgb(200,0,0));
            tvgiac.setTextColor(Color.RED);
        } else {
            int newColor = Color.parseColor("#0091ea"); //shareblue
            tvgiac.setTextColor(newColor);
            //tvgiac.setTextColor(R.);
            //tvgiac.setTextColor(Color.BLUE);
        }
        if (Env.dispart) {
            tvgiac.setText("Disp " + Formattazione.formValuta(giac, 12, 0, Formattazione.SEGNO_SX_NEG));
        } else {
            tvgiac.setText("Giac " + Formattazione.formValuta(giac, 12, 0, Formattazione.SEGNO_SX_NEG));
        }
        if (qtavend > 0)
            tvqtavend.setText("Q.tà " + Formattazione.formValuta(qtavend, 12, 0, Formattazione.NO_SEGNO));
        else if (qtasm > 0)
            tvqtavend.setText("Q.tà " + Formattazione.formValuta(qtasm, 12, 0, Formattazione.NO_SEGNO));
        else if (qtaomimp > 0)
            tvqtavend.setText("Q.tà " + Formattazione.formValuta(qtaomimp, 12, 0, Formattazione.NO_SEGNO));
        else if (qtaomtot > 0)
            tvqtavend.setText("Q.tà " + Formattazione.formValuta(qtaomtot, 12, 0, Formattazione.NO_SEGNO));
        else
            tvqtavend.setText("Q.tà");
/*        if (qtaom > 0)
            tvqtaom.setText(Formattazione.formValuta(qtaom, 12, 3, Formattazione.NO_SEGNO));
        else
            tvqtaom.setText("");*/
        tvprezzo.setText("€ " + Formattazione.formValuta(prz, 12, 2, Formattazione.SEGNO_SX_NEG));
        return rowView;
    }
}
