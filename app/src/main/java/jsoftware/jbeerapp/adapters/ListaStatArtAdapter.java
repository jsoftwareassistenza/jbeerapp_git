package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;

import static jsoftware.jbeerapp.env.Formattazione.SEGNO_SX_NEG;

public class ListaStatArtAdapter extends ArrayAdapter<Record> {

    private final Context context;
    private final ArrayList<Record> values;

    public ListaStatArtAdapter(Context context, ArrayList<Record> values) {
        super(context, R.layout.statart_item, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.statart_item, parent, false);
        TextView tvart = (TextView) rowView.findViewById(R.id.statart_art);
        TextView tvum = (TextView) rowView.findViewById(R.id.statart_um);
        TextView tvqta = (TextView) rowView.findViewById(R.id.statart_qta);
        TextView tvval = (TextView) rowView.findViewById(R.id.statart_valore);
        // TextView tvperctot = (TextView) rowView.findViewById(R.id.statart_perctot);
        Record rec = values.get(position);
        String artcod = (String) rec.leggiStringa("artcod");
        String artdescr = (String) rec.leggiStringa("artdescr");
        String artum = (String) rec.leggiStringa("artum");
        double qta = rec.leggiDouble("qta");
        double val = rec.leggiDouble("val");
        double perctot = rec.leggiDouble("perctot");
        tvart.setText(artcod + " " + artdescr);
        tvum.setText(artum);
        tvqta.setText(Formattazione.formValuta(qta, 12, 3, SEGNO_SX_NEG));
        tvval.setText(Formattazione.formValuta(val, 12, 2, SEGNO_SX_NEG));
        //tvperctot.setText(Formattazione.formValuta(perctot, 12, 2, SEGNO_SX_NEG));
        return rowView;
    }
}
