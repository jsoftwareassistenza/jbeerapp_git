package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FormFatturato;

import static jsoftware.jbeerapp.env.Formattazione.SEGNO_SX_NEG;

public class ListaFatturatoAdapter extends ArrayAdapter<Record> {

    private final Context context;
    private final ArrayList<Record> values;
    public FormFatturato padre;

    public ListaFatturatoAdapter(Context context, ArrayList<Record> values, FormFatturato padre) {
        super(context, R.layout.listafatturato_item, values);
        this.context = context;
        this.values = values;
        this.padre = padre;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.listafatturato_item, parent, false);
        TextView tvart = (TextView) rowView.findViewById(R.id.listafatturato_art);
        TextView tvvalore = (TextView) rowView.findViewById(R.id.listafatturato_valore);
        TextView tvprz = (TextView) rowView.findViewById(R.id.listafatturato_prz);
        Record rec = values.get(position);
        String artcod = (String) rec.leggiStringa("artcod");
        String artdescr = (String) rec.leggiStringa("artdescr");
        double valore = rec.leggiDouble("artvalore");
        double przm = rec.leggiDouble("artprezzom");
        tvart.setText(artcod + " " + artdescr);
        tvvalore.setText(Formattazione.formValuta(valore, 12, 2, SEGNO_SX_NEG));
        tvprz.setText(Formattazione.formValuta(przm, 12, 2, SEGNO_SX_NEG));
        //tvperctot.setText(Formattazione.formValuta(perctot, 12, 2, SEGNO_SX_NEG));
        return rowView;

    }
}
