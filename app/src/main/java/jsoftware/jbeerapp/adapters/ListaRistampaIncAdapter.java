package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FormRistampaIncassi;

public class ListaRistampaIncAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;
    public FormRistampaIncassi padre;

	public ListaRistampaIncAdapter(Context context, ArrayList<Record> values, FormRistampaIncassi padre) {
		super(context, R.layout.ristampainc_lista_item, values);
		this.context = context;
		this.values = values;
        this.padre = padre;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.ristampainc_lista_item, parent, false);
        TextView tvcli = (TextView) rowView.findViewById(R.id.ristampainc_lista_cliente);
        TextView tvdoc = (TextView) rowView.findViewById(R.id.ristampainc_lista_doc);
		TextView tvsaldo = (TextView) rowView.findViewById(R.id.ristampainc_lista_saldo);
		TextView tvdtinc = (TextView) rowView.findViewById(R.id.ristampainc_lista_datainc);
		TextView tvorainc = (TextView) rowView.findViewById(R.id.ristampainc_lista_orainc);
		Record rec = values.get(position);
        tvcli.setText(rec.leggiStringa("cliente"));
        tvdoc.setText(rec.leggiStringa("doc"));
		tvsaldo.setText(Formattazione.formValuta(rec.leggiDouble("saldo"), 12, 2, 0));
		tvdtinc.setText((new Data(rec.leggiStringa("datainc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"));
		tvorainc.setText(rec.leggiStringa("orainc"));
		return rowView;
	}	
}
