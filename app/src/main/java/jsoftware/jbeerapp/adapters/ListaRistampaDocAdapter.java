package jsoftware.jbeerapp.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FormRistampaDoc;

import android.graphics.PorterDuff.Mode;

public class ListaRistampaDocAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;
	public FormRistampaDoc padre;

	public ListaRistampaDocAdapter(Context context, ArrayList<Record> values, FormRistampaDoc padre) {
		super(context, R.layout.ristampadoc_item, values);
		this.context = context;
		this.values = values;
		this.padre = padre;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.ristampadoc_item, parent, false);
		//TextView tvtipodoc = (TextView) rowView.findViewById(R.id.ristampadoc_tipodoc);
		TextView tvndoc = (TextView) rowView.findViewById(R.id.ristampadoc_numdoc);
		TextView tvdatadoc = (TextView) rowView.findViewById(R.id.ristampadoc_datadoc);
		TextView tvcliente = (TextView) rowView.findViewById(R.id.ristampadoc_cliente);
		TextView tvinviato = (TextView) rowView.findViewById(R.id.textView15);
		ImageView imgtipodoc =  (ImageView) rowView.findViewById(R.id.ristampadoc_imgtipodoc);
		ImageButton imgtrasf = (ImageButton) rowView.findViewById(R.id.ristampadoc_trasf);
        //cbtrasf.setEnabled(false);

		final int pos = position;
		imgtrasf.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (pos != -1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(padre);
                    builder.setMessage("Confermi il cambiamento stato per il Reinvio?")
                            .setPositiveButton("SI",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            Record rec = values.get(pos);
											SQLiteStatement stmt = Env.db.compileStatement("UPDATE movimenti SET movtrasf='N'  WHERE movdoc = ? AND movnum = ?");
											stmt.clearBindings();
											stmt.bindString(1, rec.leggiStringa("doc"));
											stmt.bindDouble(2, rec.leggiIntero("numdoc"));
											stmt.execute();
											stmt.close();
											rec.eliminaCampo("trasf");
											rec.insStringa("trasf","N");
                                            notifyDataSetChanged();

                                        }
                                    })
                            .setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                        }
                                    });
                    AlertDialog ad = builder.create();
                    ad.show();

                }
            }
        });
		imgtrasf.setFocusable(false);
		Record rec = values.get(position);
		//tvtipodoc.setText(rec.leggiStringa("doc"));
		tvndoc.setText("" + rec.leggiIntero("numdoc"));
		tvdatadoc.setText((new Data(rec.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"));

		tvcliente.setText(rec.leggiStringa("cliente"));
        if (rec.leggiStringa("trasf").equals("S")) {
			imgtrasf.setImageResource(R.drawable.ic_done_black_24dp);
			int newColor = Color.parseColor("#4CAF50");//verde
			imgtrasf.setColorFilter(newColor);

		}else {
			//imgtrasf.setVisibility(View.GONE);
			tvinviato.setText("Da Inviare");
			tvinviato.setTextColor(Color.RED);
			imgtrasf.setVisibility(View.GONE);
		}


		imgtipodoc.setImageResource(R.drawable.rounded_lista_doc);
		int newColor = Color.parseColor("#ffc107"); //amber
		imgtipodoc.setColorFilter(newColor);
		return rowView;

	}


}
