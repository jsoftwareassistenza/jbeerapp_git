package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.ListImageButton;
import jsoftware.jbeerapp.env.funzStringa;
import jsoftware.jbeerapp.forms.FormClientiGiro;

public class ListaClientiGiroAdapter extends ArrayAdapter<HashMap>
{

	private final Context context;
	private final ArrayList<HashMap> values;
    public FormClientiGiro padre;

	public ListaClientiGiroAdapter(Context context, ArrayList<HashMap> values, FormClientiGiro padre) {
		super(context, R.layout.lvcligiro_item, values);
		this.context = context;
		this.values = values;
        this.padre = padre;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.lvcligiro_item, parent, false);
		TextView textView1 = (TextView) rowView.findViewById(R.id.lvcligiro_label1);
		TextView textView2 = (TextView) rowView.findViewById(R.id.lvcligiro_label2);
		TextView textView3 = (TextView) rowView.findViewById(R.id.lvcligiro_label3);
		TextView textView4 = (TextView) rowView.findViewById(R.id.lvcligiro_label4);
		TextView textView5 = (TextView) rowView.findViewById(R.id.lvcligiro_label5);
		TextView textViewVis = (TextView) rowView.findViewById(R.id.lvcligiro_labelvis);
        LinearLayout l1 = (LinearLayout) rowView.findViewById(R.id.lvcligiro_l1);
        ListImageButton bgps = (ListImageButton) rowView.findViewById(R.id.lvcligiro_buttonGps);
		HashMap rec = values.get(position);
		String clicod = (String) rec.get("codice");
		String clinome = (String) rec.get("nome");
		String cliind = (String) rec.get("indirizzo");
		String cliloc = (String) rec.get("localita");
		String vis = (String) rec.get("visitato");
		double scop = (double) rec.get("scoperti");
		boolean bloccato = ((String) rec.get("bloccato")).equals("S");
		double impscaduto = (double) rec.get("impscaduto");
		final String geoloc = (String) rec.get("geouri");
        final double gpsn = ((Double) rec.get("gpsn")).doubleValue();
        final double gpse = ((Double) rec.get("gpse")).doubleValue();
		textView1.setText(clicod + " " + clinome);
		textView2.setText(cliind);
		textView3.setText(cliloc);
		textView5.setText(Formattazione.formValuta(scop, 12, 2, 1) + (bloccato?" BLOCCATO\n(scaduto=" + Formattazione.formValuta(impscaduto, 12, 2, 0) + ")":""));
		if (vis.equals("S"))
		{
			textViewVis.setText("");
			textViewVis.setBackgroundResource(R.drawable.greencircle);
		}
		else
			textViewVis.setText("");
		if (scop != 0)
		{
			textView5.setTextColor(Color.RED);
		}
		else
		{
			textView5.setTextColor(Color.rgb(0, 100, 0));
		}
        if (!geoloc.equals(""))
            bgps.setEnabled(true);
        else
        {
            bgps.setVisibility(View.INVISIBLE);
//            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
//            l1.setLayoutParams(param);
        }
        bgps.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + funzStringa.sostituisci(Formattazione.formatta(gpsn, "#######0.000000", 0), ',', '.') + "," +
                        funzStringa.sostituisci(Formattazione.formatta(gpse, "#######0.000000", 0), ',', '.'));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                padre.startActivity(mapIntent);
            }
        });
		return rowView;
	}	
}
