package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.forms.FormVisAvvisi;

public class ListaAvvisiAdapter extends ArrayAdapter<String>
{

	private final Context context;
	private final ArrayList<String> values;
    public FormVisAvvisi padre;

	public ListaAvvisiAdapter(Context context, ArrayList<String> values, FormVisAvvisi padre) {
		super(context, R.layout.lvavvisi_item, values);
		this.context = context;
		this.values = values;
        this.padre = padre;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.lvavvisi_item, parent, false);
		TextView tvtesto = (TextView) rowView.findViewById(R.id.lvavvisi_testo);
		String txt = values.get(position);
		tvtesto.setText(txt);
		return rowView;
	}	
}
