package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;

import static jsoftware.jbeerapp.env.Formattazione.SEGNO_SX_NEG;

public class ListaStatProdAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;

	public ListaStatProdAdapter(Context context, ArrayList<Record> values) {
		super(context, R.layout.statprod_item, values);
		this.context = context;
		this.values = values;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.statprod_item, parent, false);
		TextView tvart = (TextView) rowView.findViewById(R.id.statprod_art);
        TextView tvcolli = (TextView) rowView.findViewById(R.id.statprod_colli);
        TextView tvqta = (TextView) rowView.findViewById(R.id.statprod_qta);
        TextView tvanidri = (TextView) rowView.findViewById(R.id.statprod_anidri);
		TextView tvidrati = (TextView) rowView.findViewById(R.id.statprod_idrati);
		Record rec = values.get(position);
		String artcod = (String) rec.leggiStringa("artcod");
		String artdescr = (String) rec.leggiStringa("artdescr");
		int colli = (int) rec.leggiIntero("artcolli");
		double qta = rec.leggiDouble("artqta");
        double anidri = rec.leggiDouble("artlitrianidri");
        double idrati = rec.leggiDouble("artlitriidrati");
		tvart.setText(artcod + " " + artdescr);
		tvcolli.setText("" + colli);
        tvqta.setText(Formattazione.formValuta(qta, 12, 2, SEGNO_SX_NEG));
		tvanidri.setText(Formattazione.formValuta(anidri, 12, 2, SEGNO_SX_NEG));
		tvidrati.setText(Formattazione.formValuta(idrati, 12, 2, SEGNO_SX_NEG));
        //tvperctot.setText(Formattazione.formValuta(perctot, 12, 2, SEGNO_SX_NEG));
		return rowView;
	}	
}
