package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;

public class ListaVisGiacAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;

	public ListaVisGiacAdapter(Context context, ArrayList<Record> values) {
		super(context, R.layout.visgiac_item, values);
		this.context = context;
		this.values = values;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.visgiac_item, parent, false);
		TextView tvart = (TextView) rowView.findViewById(R.id.visgiac_art);
        TextView tvum = (TextView) rowView.findViewById(R.id.visgiac_um);
        TextView tvgiac = (TextView) rowView.findViewById(R.id.visgiac_giac);
		Record rec = values.get(position);
		String artcod = rec.leggiStringa("artcod");
		String artdescr = rec.leggiStringa("artdescr");
		String artum = rec.leggiStringa("artum");
        double giac = rec.leggiDouble("giac");
		tvart.setText(artcod + " " + artdescr);
        tvum.setText(artum);
        tvgiac.setText(Formattazione.formValuta(giac, 12, 3, Formattazione.SEGNO_SX_NEG));
		return rowView;
	}	
}
