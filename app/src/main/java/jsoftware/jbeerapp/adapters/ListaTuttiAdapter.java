package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FragmentRubricaTutti;

public class ListaTuttiAdapter extends ArrayAdapter<Record> {

    private final Context context;
    private final ArrayList<Record> values;
    public FragmentRubricaTutti padre;

    public ListaTuttiAdapter(Context context, ArrayList<Record> values, FragmentRubricaTutti padre) {
        super(context, R.layout.lvcli_item, values);
        this.context = context;
        this.values = values;
        this.padre = padre;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.lvcli_item, parent, false);
        TextView textView1 = (TextView) rowView.findViewById(R.id.lvcliconta_nome);
        CheckBox cbsel = (CheckBox) rowView.findViewById(R.id.checkBox_rubrica);
        final Record rec = values.get(position);
        if (rec.leggiStringa("tipo") == "contatto") {
            String clicognome = (String) rec.leggiStringa("ccognome");
            String clinome = (String) rec.leggiStringa("cnome");
            String ragsoc = (String) rec.leggiStringa("caziendaragsoc");
            if (clicognome.equals("")) {
                textView1.setText(ragsoc);
            } else {
                textView1.setText(clicognome + " " + clinome);
            }

        } else {
            String cliazienda = (String) rec.leggiStringa("clinome");
            String clinomep = (String) rec.leggiStringa("clinomepri");
            String clicognomep = (String) rec.leggiStringa("clicognomepri");
            //String clinome = (String) rec.leggiStringa("cnome");
            if (cliazienda.equals("")) {
                textView1.setText(clicognomep + " " + clinomep);
            } else {
                textView1.setText(cliazienda);
            }

        }
        cbsel.setChecked(rec.esisteCampo("sel"));
        cbsel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                             @Override
                                             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                 if (isChecked) {
                                                     if (rec.esisteCampo("sel"))
                                                         rec.eliminaCampo("sel");
                                                     rec.insStringa("sel", "S");
                                                 } else {
                                                     if (rec.esisteCampo("sel"))
                                                         rec.eliminaCampo("sel");
                                                 }
                                                 //aggiornadestinatariemail();
                                             }
                                         }
        );


        return rowView;
    }
}
