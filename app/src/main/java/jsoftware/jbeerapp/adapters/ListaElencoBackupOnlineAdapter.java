package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FormElencoBackupOnline;
import jsoftware.jbeerapp.forms.FormRipristino;

public class ListaElencoBackupOnlineAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;
    public FormElencoBackupOnline padre;
	public FormRipristino padre1;

	public ListaElencoBackupOnlineAdapter(Context context, ArrayList<Record> values, FormElencoBackupOnline padre) {
		super(context, R.layout.listafileselbackuponline_item, values);
		this.context = context;
		this.values = values;
        this.padre = padre;
	}

	public ListaElencoBackupOnlineAdapter(Context context, ArrayList<Record> values, FormRipristino padre1) {
		super(context, R.layout.listafileselbackuponline_item, values);
		this.context = context;
		this.values = values;
		this.padre1 = padre1;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.listafileselbackuponline_item, parent, false);
		TextView tvnome = (TextView) rowView.findViewById(R.id.listafileselbackuponline_nome);
		TextView tvdata = (TextView) rowView.findViewById(R.id.listafileselbackuponline_data);
		TextView tvora = (TextView) rowView.findViewById(R.id.listafileselbackuponline_ora);
		Record rec = values.get(position);
		tvnome.setText(rec.leggiStringa("nomefile"));
		tvdata.setText((new Data(rec.leggiStringa("dataarrivo"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"));
		tvora.setText(rec.leggiStringa("oraarrivo"));
		return rowView;
	}	
}
