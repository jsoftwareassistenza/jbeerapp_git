package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FormCatalogo;

public class ListaContAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;
    public FormCatalogo padre;

	public ListaContAdapter(Context context, ArrayList<Record> values, FormCatalogo padre) {
		super(context, R.layout.lvcont_item, values);
		this.context = context;
		this.values = values;
		this.padre = padre;


	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.lvcont_item, parent, false);
		TextView textView1 = (TextView) rowView.findViewById(R.id.nome);
		CheckBox selc = (CheckBox)rowView.findViewById(R.id.radiookc) ;
		Record rec = values.get(position);
		String nome = (String) rec.leggiStringa("nome");
		String email= (String) rec.leggiStringa("mail");
		if(rec.leggiIntero("ch")==0){
			selc.setChecked(false);
		}else{
			selc.setChecked(true);
		}
		CheckBox ckselc = (CheckBox) rowView.findViewById(R.id.radiookc);

		ckselc.setOnClickListener( new View.OnClickListener() {
			public void onClick(View v) {



				CheckBox cbc = (CheckBox) v ;

				if(cbc.isChecked())
				{
					values.get(position).eliminaCampo("ch");
					values.get(position).insIntero("ch",1);
				}
				else
				{
					values.get(position).eliminaCampo("ch");
					values.get(position).insIntero("ch",0);
				}
				notifyDataSetChanged();

			}
		});

		textView1.setText(nome);
		return rowView;

		}


}
