package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;

public class ListaStatEventiContattiAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;

	public ListaStatEventiContattiAdapter(Context context, ArrayList<Record> values) {
		super(context, R.layout.stateventicontatti_item, values);
		this.context = context;
		this.values = values;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.stateventicontatti_item, parent, false);
		TextView tvnome = (TextView) rowView.findViewById(R.id.stateventicont_nome);
        TextView tvazienda = (TextView) rowView.findViewById(R.id.stateventicont_azienda);
        TextView tvemail = (TextView) rowView.findViewById(R.id.stateventicont_email);
        TextView tvtel = (TextView) rowView.findViewById(R.id.stateventicont_tel);
        TextView tvmobile = (TextView) rowView.findViewById(R.id.stateventicont_mobile);
		Record rec = values.get(position);
		String nome = (String) rec.leggiStringa("nome");
        String cognome = (String) rec.leggiStringa("cognome");
        String azienda = (String) rec.leggiStringa("azienda");
        String email = (String) rec.leggiStringa("email");
        String tel = (String) rec.leggiStringa("tel");
        String mobile = (String) rec.leggiStringa("mobile");
        String emailazienda = (String) rec.leggiStringa("emailazienda");
		tvnome.setText(nome + " " + cognome);
        tvazienda.setText("azienda: " + azienda);
        if (!email.equals(""))
            tvemail.setText(email);
        else
            tvemail.setText(emailazienda);
        tvtel.setText("tel.fisso: " + tel);
        tvmobile.setText("mobile: " + mobile);
		return rowView;
	}	
}
