package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;

/**
 * Created by pgx71 on 21/11/2016.
 */

public class SpinnerAdapter extends ArrayAdapter<String>
{
    private ArrayList<String> elems;

    public SpinnerAdapter(Context context, int textViewResourceId, ArrayList<String> objects)
    {
        super(context, textViewResourceId, objects);
        this.elems = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        return super.getDropDownView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.spinner_item, parent, false);
        TextView label=(TextView)row.findViewById(R.id.ba1);
        label.setText(elems.get(position));

        //ImageView icon = (ImageView)row.findViewById(R.id.icon);

        //icon.setImageResource(R.drawable.dark_ic_action_about);

        return row;
    }}
