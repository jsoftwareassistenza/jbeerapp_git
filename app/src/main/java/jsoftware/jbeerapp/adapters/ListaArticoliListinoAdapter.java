package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FragmentDettaglioClienteListino;
import jsoftware.jbeerapp.forms.FragmentRubricaContatti;

public class ListaArticoliListinoAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;

	public ListaArticoliListinoAdapter(Context context, ArrayList<Record> values) {
		super(context, R.layout.lvlisart_item, values);
		this.context = context;
		this.values = values;
	}

    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.lvlisart_item, parent, false);
		TextView tvnomeArt = (TextView) rowView.findViewById(R.id.tvlisart_nomeart);
		TextView tvprzlistino = (TextView) rowView.findViewById(R.id.tvlisart_prezzolist);
		//TextView tvprznetto = (TextView) rowView.findViewById(R.id.tvlistart_prezzonet);
		TextView tvdata = (TextView) rowView.findViewById(R.id.tvlistart_data);
		Record rec = values.get(position);
		String nomeArticolo = (String) rec.leggiStringa("artdescr");
		double prezzoListino = rec.leggiDouble("przlistino");
       // double prezzoNetto = rec.leggiDouble("przlistino");
        String dataListino = new Data(rec.leggiStringa("dataval"), Data.AAAA_MM_GG).formatta(Data.GG_MM_AAAA,"/");
        tvnomeArt.setText(nomeArticolo);
        tvprzlistino.setText("€ " + Formattazione.formValuta(prezzoListino, 12, 2, 0));
      //  tvprznetto.setText("€ " + Formattazione.formValuta(prezzoNetto, 12, 2, 0));
        tvdata.setText(dataListino);
		return rowView;
	}	
}
