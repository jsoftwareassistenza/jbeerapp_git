package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FormScoperti;

public class ListaScopertiAdapter extends ArrayAdapter<Record> {

    private final Context context;
    private final ArrayList<Record> values;
    public FormScoperti padre;

    public ListaScopertiAdapter(Context context, ArrayList<Record> values, FormScoperti padre) {
        super(context, R.layout.listascoperti_item, values);
        this.context = context;
        this.values = values;
        this.padre = padre;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.listascoperti_item, parent, false);
        TextView tvtipo = (TextView) rowView.findViewById(R.id.listascoperti_tipo);
        TextView tvdoc = (TextView) rowView.findViewById(R.id.listascoperti_doc);
        TextView tvimp = (TextView) rowView.findViewById(R.id.listascoperti_imp);


        final Record rec = values.get(position);

        tvtipo.setText("Tipo " + rec.leggiStringa("tipo"));
        String dd = "";
        if (rec.leggiIntero("rateizzata") == 1) {
            dd = "RATEIZZAZIONE";
        } else if (rec.leggiIntero("numdoc") > 0) {
            dd = rec.leggiStringa("sezdoc") + "/" + rec.leggiIntero("numdoc") + " del " + (new Data(rec.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
        }
        tvdoc.setText("Documento: " + dd);
        if (rec.leggiStringa("tipoop").equals("V"))
            tvimp.setText("Importo: " + Formattazione.formatta(rec.leggiDouble("residuoscad"), "#######0.00", Formattazione.SEGNO_DX));
        else
            tvimp.setText("Importo: " + Formattazione.formatta(-rec.leggiDouble("residuoscad"), "#######0.00", Formattazione.SEGNO_DX));

        return rowView;
    }
}
