package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Record;

public class ListaRicArtAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;
	
	public ListaRicArtAdapter(Context context, ArrayList<Record> values) {
		super(context, R.layout.ricart_item, values);
		this.context = context;
		this.values = values;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.ricart_item, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.lvricart_label1);
		ImageView img = (ImageView) rowView.findViewById(R.id.lvricart_icona);
		Record rec = values.get(position);
		String artcod = (String) rec.leggiStringa("codice");
		String artdescr = (String) rec.leggiStringa("descr");
		String artum = (String) rec.leggiStringa("um");
		int not = rec.leggiIntero("notifica");
		if (not == 1)
			img.setImageResource(R.drawable.ic_announcement_black_48dp);
		else if (not == 2)
			img.setImageResource(R.drawable.ic_grade_black_48dp);
		else if (not == 3)
			img.setImageResource(R.drawable.ic_trending_up_black_48dp);
		else if (not == 4)
			img.setImageResource(R.drawable.ic_trending_down_black_48dp);
		else
			img.setImageResource(android.R.color.transparent);
		textView.setText(artcod + " " + artdescr);
		return rowView;
	}	
}
