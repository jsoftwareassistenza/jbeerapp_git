package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;

public class ListaStatEventiMerceInviataAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;

	public ListaStatEventiMerceInviataAdapter(Context context, ArrayList<Record> values) {
		super(context, R.layout.stateventimerceinviata_item, values);
		this.context = context;
		this.values = values;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.stateventimerceinviata_item, parent, false);
		TextView tvart = (TextView) rowView.findViewById(R.id.stateventimi_art);
        TextView tvum = (TextView) rowView.findViewById(R.id.stateventimi_um);
        TextView tvqta = (TextView) rowView.findViewById(R.id.stateventimi_qta);
		Record rec = values.get(position);
		String artcod = (String) rec.leggiStringa("artcod");
		String artdescr = (String) rec.leggiStringa("artdescr");
		String artum = (String) rec.leggiStringa("artum");
		double qta = rec.leggiDouble("qta");
		tvart.setText(artcod + " " + artdescr);
        tvum.setText(artum);
        tvqta.setText(Formattazione.formValuta(qta, 12, 0, Formattazione.SEGNO_SX_NEG));
		return rowView;
	}	
}
