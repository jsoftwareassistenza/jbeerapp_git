package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;

import static jsoftware.jbeerapp.env.Formattazione.SEGNO_SX_NEG;

public class ListaStatCliAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;

	public ListaStatCliAdapter(Context context, ArrayList<Record> values) {
		super(context, R.layout.statcli_item, values);
		this.context = context;
		this.values = values;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.statcli_item, parent, false);
		TextView tvcli = (TextView) rowView.findViewById(R.id.statcli_cli);
        TextView tvval = (TextView) rowView.findViewById(R.id.statcli_valore);
        //TextView tvperctot = (TextView) rowView.findViewById(R.id.statcli_perctot);
		Record rec = values.get(position);
		String ccod = (String) rec.leggiStringa("clicod");
		String cdescr = (String) rec.leggiStringa("clinome");
        double val = rec.leggiDouble("val");
        double perctot = rec.leggiDouble("perctot");
		tvcli.setText(ccod + " " + cdescr);
        tvval.setText(Formattazione.formValuta(val, 12, 2, SEGNO_SX_NEG));
        //tvperctot.setText(Formattazione.formValuta(perctot, 12, 2, SEGNO_SX_NEG));
		return rowView;
	}	
}
