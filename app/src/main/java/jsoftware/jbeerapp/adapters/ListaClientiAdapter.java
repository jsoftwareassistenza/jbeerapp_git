package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.forms.FormRicercaClienti;
import jsoftware.jbeerapp.forms.FormRubrica;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FragmentRubricaClienti;

public class ListaClientiAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;
    public FragmentRubricaClienti padre;
	
	public ListaClientiAdapter(Context context, ArrayList<Record> values,  FragmentRubricaClienti padre) {
	super(context, R.layout.lvcli_item, values);
	this.context = context;
	this.values = values;
	this.padre = padre;
}


    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.lvcli_item, parent, false);
		TextView textView1 = (TextView) rowView.findViewById(R.id.lvcliconta_nome);
		CheckBox cbsel = (CheckBox) rowView.findViewById(R.id.checkBox_rubrica);
		/*TextView textView2 = (TextView) rowView.findViewById(R.id.lvcli_label2);
		TextView textView3 = (TextView) rowView.findViewById(R.id.lvcli_label3);*/
        //LinearLayout l1 = (LinearLayout) rowView.findViewById(R.id.lvcli_l1);
		final Record rec = values.get(position);
		String cliazienda = (String) rec.leggiStringa("clinome");
		String clinome = (String) rec.leggiStringa("clinomepri");
		String clicognome = (String) rec.leggiStringa("clicognomepri");
/*		String cliind = (String) rec.get("indirizzo");
		String cliloc = (String) rec.get("localita");
        final String geoloc = (String) rec.get("geouri");*/
		if (cliazienda.equals("")) {
			textView1.setText(clicognome + " " + clinome);
		} else {
			textView1.setText(cliazienda);
		}
/*		textView2.setText(cliind);
		textView3.setText(cliloc);*/
		cbsel.setChecked(rec.esisteCampo("sel"));
		cbsel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					if (rec.esisteCampo("sel"))
						rec.eliminaCampo("sel");
					rec.insStringa("sel", "S");
				} else {
					if (rec.esisteCampo("sel"))
						rec.eliminaCampo("sel");
				}
				//aggiornadestinatariemail();
			}
		}
        );
		return rowView;
	}	
}
