package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;

public class ListaVendQtaTotAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;

	public ListaVendQtaTotAdapter(Context context, ArrayList<Record> values) {
		super(context, R.layout.listavend_qtatot_item, values);
		this.context = context;
		this.values = values;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.listavend_qtatot_item, parent, false);
		TextView tvart = (TextView) rowView.findViewById(R.id.listavendqt_art);
		TextView tvgiac = (TextView) rowView.findViewById(R.id.listavendqt_giac);
		TextView tvqtavend = (TextView) rowView.findViewById(R.id.listavendqt_qtavend);
        TextView tvqtaom = (TextView) rowView.findViewById(R.id.listavendqt_qtaom);
        TextView tvprezzo = (TextView) rowView.findViewById(R.id.listavendqt_prezzo);
		Record rec = values.get(position);
		String artcod = rec.leggiStringa("artcod");
		String artdescr = rec.leggiStringa("artdescr");
		double giac = rec.leggiDouble("giac");
        double qtavend = rec.leggiDouble("qtavend");
        double qtasm = rec.leggiDouble("qtasm");
        double qtaomimp = rec.leggiDouble("qtaomimp");
        double qtaomtot = rec.leggiDouble("qtaomtot");
        double qtaom = qtasm + qtaomimp + qtaomtot;
        double prz = rec.leggiDouble("prezzonetto");
		tvart.setText(artcod + " " + artdescr);
		tvgiac.setText(Formattazione.formValuta(giac, 12, 2, Formattazione.SEGNO_SX_NEG));
		if (qtavend > 0)
			tvqtavend.setText(Formattazione.formValuta(qtavend, 12, 2, Formattazione.NO_SEGNO));
		else
			tvqtavend.setText("");
        if (qtaom > 0)
            tvqtaom.setText(Formattazione.formValuta(qtaom, 12, 3, Formattazione.NO_SEGNO));
        else
            tvqtaom.setText("");
        tvprezzo.setText(Formattazione.formValuta(prz, 12, 3, Formattazione.SEGNO_SX_NEG));
		return rowView;
	}	
}
