package jsoftware.jbeerapp.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FormCatalogo;
import jsoftware.jbeerapp.forms.FormRubrica;

public class ListaCatalogoAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;
	public FormCatalogo padre;

	public ListaCatalogoAdapter(Context context, ArrayList<Record> values, FormCatalogo padre) {
		super(context, R.layout.lvcatag_item, values);
		this.context = context;
		this.values = values;
		this.padre = padre;


	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = inflater.inflate(R.layout.lvcatag_item, parent, false);
		TextView textView1 = (TextView) rowView.findViewById(R.id.art_descr);
		TextView textView2 = (TextView) rowView.findViewById(R.id.art_stile);
		TextView textView3 = (TextView) rowView.findViewById(R.id.art_colore);
		TextView textView4 = (TextView) rowView.findViewById(R.id.art_grado);
		TextView textView5 = (TextView) rowView.findViewById(R.id.art_prezzo);
		TextView textView6 = (TextView) rowView.findViewById(R.id.art_giac);
		ImageView img= (ImageView)rowView.findViewById(R.id.img) ;
		CheckBox sel = (CheckBox)rowView.findViewById(R.id.radiook) ;
		Record rec = values.get(position);
		String artdescr = (String) rec.leggiStringa("catnomeart");
		String artstile = (String) rec.leggiStringa("catstile");
		String artcolore = (String) rec.leggiStringa("catcolore");
		String artgrado = (String) rec.leggiStringa("catgrado");
		String artprz = (String) rec.leggiStringa("prz");
		String artgiac = (String) rec.leggiStringa("giac");
		String nomeimg=(String) rec.leggiStringa("nomeimg");

		boolean checkati;
		if((Integer)rec.leggiIntero("ch")==0){
			checkati=false;
		}else{
			checkati=true;
		}
		textView1.setText(artdescr);
		textView2.setText(artstile);
		textView3.setText(artcolore);
		textView4.setText(artgrado);
		textView5.setText(artprz);
		textView6.setText(artgiac);
		if (checkati){
			sel.setChecked(true);
		}else{
			sel.setChecked(false);
		}

        if (!nomeimg.equals(""))
        {
            File fimg = padre.getBaseContext().getFileStreamPath(nomeimg);
            if (fimg.exists())
            {
                try
                {
                    Bitmap myBitmap = BitmapFactory.decodeFile(fimg.getAbsolutePath());
                    img.setImageBitmap(myBitmap);
                } catch (Exception eimg)
                {
                    eimg.printStackTrace();
                }
            }
        }

		CheckBox cksel = (CheckBox) rowView.findViewById(R.id.radiook);

		cksel.setOnClickListener( new View.OnClickListener() {
			public void onClick(View v) {



				CheckBox cb = (CheckBox) v ;

				if(cb.isChecked())
				{
					values.get(position).eliminaCampo("ch");
					values.get(position).insIntero("ch",1);
				}
				else
				{
					values.get(position).eliminaCampo("ch");
					values.get(position).insIntero("ch",0);
				}
				notifyDataSetChanged();

			}
		});


		return rowView;

	}


}
