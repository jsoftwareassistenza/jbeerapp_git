package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FormRegIncassi;

public class ListaRegIncScopAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;
    public FormRegIncassi padre;

	public ListaRegIncScopAdapter(Context context, ArrayList<Record> values, FormRegIncassi padre) {
		super(context, R.layout.listaregincscop_item, values);
		this.context = context;
		this.values = values;
        this.padre = padre;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.listaregincscop_item, parent, false);
        CheckBox cbsel = (CheckBox) rowView.findViewById(R.id.listaregincscop_sel);
        TextView tvtipo = (TextView) rowView.findViewById(R.id.listaregincscop_tipo);
		TextView tvdoc = (TextView) rowView.findViewById(R.id.listaregincscop_doc);
		TextView tvimp = (TextView) rowView.findViewById(R.id.listaregincscop_imp);
		TextView tvsaldo = (TextView) rowView.findViewById(R.id.listaregincscop_saldo);

		final Record rec = values.get(position);

		tvtipo.setText(rec.leggiStringa("tipo"));
		String dd = "";
		if (rec.leggiIntero("rateizzata") == 1)
		{
			dd = "RATEIZZAZIONE";
		}
		else if (rec.leggiIntero("numdoc") > 0)
		{
			dd = rec.leggiStringa("sezdoc") + "/" + rec.leggiIntero("numdoc") + " del " + (new Data(rec.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
		}
		tvdoc.setText(dd);
		if (rec.leggiStringa("tipoop").equals("V"))
			tvimp.setText(Formattazione.formatta(rec.leggiDouble("residuoscad"), "#######0.00", Formattazione.SEGNO_DX));
		else
			tvimp.setText(Formattazione.formatta(-rec.leggiDouble("residuoscad"), "#######0.00", Formattazione.SEGNO_DX));
		if (rec.leggiStringa("tipoop").equals("V"))
			tvsaldo.setText(Formattazione.formatta(rec.leggiDouble("saldo"), "#######0.00", Formattazione.SEGNO_DX));
		else
			tvsaldo.setText(Formattazione.formatta(-rec.leggiDouble("saldo"), "#######0.00", Formattazione.SEGNO_DX));

        cbsel.setChecked(rec.esisteCampo("sel"));
        cbsel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
               @Override
               public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                   if (isChecked)
                   {
                       if (rec.esisteCampo("sel"))
                           rec.eliminaCampo("sel");
                       rec.insStringa("sel", "S");
                   }
                   else
                   {
                       if (rec.esisteCampo("sel"))
                           rec.eliminaCampo("sel");
                   }
                   padre.aggiornaTotaleIncassoMultiplo();
               }
           }
        );

        return rowView;
	}	
}
