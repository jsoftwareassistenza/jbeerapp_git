package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FormMessaggi;

public class ListaMsgAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;
    public FormMessaggi padre;

	public ListaMsgAdapter(Context context, ArrayList<Record> values, FormMessaggi padre) {
		super(context, R.layout.lmsg_dett_item, values);
		this.context = context;
		this.values = values;
        this.padre = padre;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.lmsg_dett_item, parent, false);
		TextView tvtxt = (TextView) rowView.findViewById(R.id.lmsgdett_txt);
		Record rec = values.get(position);
		String testo = rec.leggiStringa("testo");
		tvtxt.setText(testo);
		return rowView;
	}	
}
