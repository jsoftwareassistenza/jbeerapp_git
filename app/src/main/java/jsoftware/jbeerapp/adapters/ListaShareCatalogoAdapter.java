package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Data;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FormDettaglioCliente;
import jsoftware.jbeerapp.forms.FormRubrica;
import jsoftware.jbeerapp.forms.FragmentRubricaContatti;

public class ListaShareCatalogoAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;
    public FormRubrica padre;
	public FormDettaglioCliente padre1;

	public ListaShareCatalogoAdapter(Context context, ArrayList<Record> values, FormRubrica padre) {
		super(context, R.layout.lvsharecatag_item, values);
		this.context = context;
		this.values = values;
		this.padre = padre;
	}

	public ListaShareCatalogoAdapter(Context context, ArrayList<Record> values, FormDettaglioCliente padre1) {
		super(context, R.layout.lvsharecatag_item, values);
		this.context = context;
		this.values = values;
		this.padre = padre;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.lvsharecatag_item, parent, false);
		TextView tvnome = (TextView) rowView.findViewById(R.id.lvcatalogo_nome);
		TextView tvdata = (TextView) rowView.findViewById(R.id.lvcatalogo_data);
		CheckBox cbsel = (CheckBox) rowView.findViewById(R.id.checkBox_sharecatag);
		final Record rec = values.get(position);
		String catalogoNome = (String) rec.leggiStringa("catpdfnome");
		String dataDb = (String) rec.leggiStringa("catpdfdata");
		String data = (new Data(dataDb, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
		tvnome.setText(catalogoNome);
		tvdata.setText(data);
		cbsel.setChecked(rec.esisteCampo("sel"));
		cbsel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				 @Override
				 public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isChecked) {
						 if (rec.esisteCampo("sel"))
							 rec.eliminaCampo("sel");
						 rec.insStringa("sel", "S");
					 } else {
						 if (rec.esisteCampo("sel"))
							 rec.eliminaCampo("sel");
					 }
					 //aggiornadestinatariemail();
				 }
			 }
		);

		return rowView;
	}	
}
