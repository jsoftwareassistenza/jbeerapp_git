package jsoftware.jbeerapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;
import jsoftware.jbeerapp.forms.FormVisDettaglioDoc;

public class ListaVisDocDettAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;
    public FormVisDettaglioDoc padre;

	public ListaVisDocDettAdapter(Context context, ArrayList<Record> values, FormVisDettaglioDoc padre) {
		super(context, R.layout.lvisdoc_dett_item, values);
		this.context = context;
		this.values = values;
        this.padre = padre;
	}	
	
	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.lvisdoc_dett_item, parent, false);
		TextView tvart = (TextView) rowView.findViewById(R.id.lvisdocdett_art);
		TextView tvqta = (TextView) rowView.findViewById(R.id.lvisdocdett_qta);
		TextView tvprz = (TextView) rowView.findViewById(R.id.lvisdocdett_prezzo);
		TextView tvpercsc1 = (TextView) rowView.findViewById(R.id.lvisdocdett_percsc1);
		TextView tvpercsc2 = (TextView) rowView.findViewById(R.id.lvisdocdett_percsc2);
		TextView tvpercsc3 = (TextView) rowView.findViewById(R.id.lvisdocdett_percsc3);
		TextView tvimp = (TextView) rowView.findViewById(R.id.lvisdocdett_imp);
		TextView tvlotto = (TextView) rowView.findViewById(R.id.lvisdocdett_lotto);

		Record rec = values.get(position);
		String artcod = rec.leggiStringa("artcod");
		String artdescr = rec.leggiStringa("artdescr");
		double qta = rec.leggiDouble("qta");
		double prz = rec.leggiDouble("prezzo");
		double sc1 = rec.leggiDouble("sc1");
		double sc2 = rec.leggiDouble("sc2");
		double sc3 = rec.leggiDouble("sc3");
		double netto = rec.leggiDouble("netto");
		String lotto = rec.leggiStringa("lotto");
		tvart.setText(artcod + " " + artdescr);
		tvqta.setText(Formattazione.formValuta(qta, 12, 0, 0));
        tvprz.setText(Formattazione.formValuta(prz, 12, 2, 2));
		String ss = "";
		if (sc1 > 0)
			tvpercsc1.setText(Formattazione.formValuta(sc1, 3, 1, 0));
		else
			tvpercsc1.setText("");
		if (sc2 > 0)
			tvpercsc2.setText(Formattazione.formValuta(sc2, 3, 1, 0));
		else
			tvpercsc2.setText("");
		if (sc3 > 0)
			tvpercsc3.setText(Formattazione.formValuta(sc3, 3, 1, 0));
		else
			tvpercsc3.setText("");
		tvimp.setText(Formattazione.formValuta(netto, 12, 2, 2));
		if (netto == 0)
			tvimp.setTextColor(Color.RED);
		else
			tvimp.setTextColor(Color.BLACK);
		if (!lotto.equals(""))
			tvlotto.setText(lotto);
		else
			tvlotto.setText("");
		return rowView;
	}	
}
