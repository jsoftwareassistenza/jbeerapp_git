package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Env;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;

public class ListaPrefAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;

	public ListaPrefAdapter(Context context, ArrayList<Record> values) {
		super(context, R.layout.pref_item, values);
		this.context = context;
		this.values = values;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.pref_item, parent, false);
		TextView tvart = (TextView) rowView.findViewById(R.id.pref_art);
		TextView tvprezzo = (TextView) rowView.findViewById(R.id.pref_prezzo);
		TextView tvgiac = (TextView) rowView.findViewById(R.id.pref_giac);
		TextView tvqtavend = (TextView) rowView.findViewById(R.id.pref_qtavend);
        TextView tvqtaom = (TextView) rowView.findViewById(R.id.pref_qtaom);
		TextView tvlotto = (TextView) rowView.findViewById(R.id.pref_lotto);
		ImageView img = (ImageView) rowView.findViewById(R.id.pref_icona);
		Record rec = values.get(position);
		String artcod = rec.leggiStringa("artcod");
		String artdescr = rec.leggiStringa("artdescr");
		String lotto = rec.leggiStringa("lotto");
		double prz = rec.leggiDouble("prezzonetto");
		double giac = rec.leggiDouble("giac");
        double qtavend = rec.leggiDouble("qtavend");
        double qtasm = rec.leggiDouble("qtasm");
        double qtaomimp = rec.leggiDouble("qtaomimp");
        double qtaomtot = rec.leggiDouble("qtaomtot");
        double qtaom = qtasm + qtaomimp + qtaomtot;
		tvart.setText(artcod + " " + artdescr);
		if(!lotto.equals("")) {
			tvlotto.setText("Lotto " + lotto);
		}else{
			tvlotto.setVisibility(View.GONE);
		}
		tvprezzo.setText("€ " + Formattazione.formValuta(prz, 12, 2, Formattazione.SEGNO_SX_NEG));

		if(giac <= 0){
			//tvgiac.setTextColor(Color.rgb(200,0,0));
			tvgiac.setTextColor(Color.RED);
		}else {
			int newColor = Color.parseColor("#0091ea"); //shareblue
			tvgiac.setTextColor(newColor);

		}
		if (Env.dispart) {
			tvgiac.setText("Disp " + Formattazione.formValuta(giac, 12, 0, Formattazione.SEGNO_SX_NEG));
		} else {
			tvgiac.setText("Giac " +Formattazione.formValuta(giac, 12, 0, Formattazione.SEGNO_SX_NEG));
		}

		if (qtavend > 0)
			tvqtavend.setText("Q.tà " + Formattazione.formValuta(qtavend, 12, 1, Formattazione.NO_SEGNO));
		else
			tvqtavend.setText("Q.tà");
        if (qtaom > 0)
            tvqtaom.setText(Formattazione.formValuta(qtaom, 12, 2, Formattazione.NO_SEGNO));
        else
            tvqtaom.setText("");
		int not = rec.leggiIntero("notifica");
		if (not == 1)
			img.setImageResource(R.drawable.ic_announcement_black_48dp);
		else if (not == 2)
			img.setImageResource(R.drawable.ic_visibility_black_48dp);
		else if (not == 3)
			img.setImageResource(R.drawable.ic_trending_up_black_48dp);
		else if (not == 4)
			img.setImageResource(R.drawable.ic_trending_down_black_48dp);
		else
			/*img.setImageResource(android.R.color.transparent);*/
			img.setImageResource(R.drawable.ic_grade_black_48dp);
		return rowView;
	}	
}
