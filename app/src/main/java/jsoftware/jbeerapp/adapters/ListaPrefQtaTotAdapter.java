package jsoftware.jbeerapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jsoftware.jbeerapp.R;
import jsoftware.jbeerapp.env.Formattazione;
import jsoftware.jbeerapp.env.Record;

public class ListaPrefQtaTotAdapter extends ArrayAdapter<Record>
{

	private final Context context;
	private final ArrayList<Record> values;

	public ListaPrefQtaTotAdapter(Context context, ArrayList<Record> values) {
		super(context, R.layout.pref_item, values);
		this.context = context;
		this.values = values;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.pref_qtatot_item, parent, false);
		TextView tvart = (TextView) rowView.findViewById(R.id.prefqt_art);
		TextView tvprezzo = (TextView) rowView.findViewById(R.id.prefqt_prezzo);
		TextView tvgiac = (TextView) rowView.findViewById(R.id.prefqt_giac);
		TextView tvqtavend = (TextView) rowView.findViewById(R.id.prefqt_qtavend);
        TextView tvqtaom = (TextView) rowView.findViewById(R.id.prefqt_qtaom);
		ImageView img = (ImageView) rowView.findViewById(R.id.prefqt_icona);
		Record rec = values.get(position);
		String artcod = rec.leggiStringa("artcod");
		String artdescr = rec.leggiStringa("artdescr");
		double prz = rec.leggiDouble("prezzonetto");
		double giac = rec.leggiDouble("giac");
		double qtavend = rec.leggiDouble("qtavend");
        double qtasm = rec.leggiDouble("qtasm");
        double qtaomimp = rec.leggiDouble("qtaomimp");
        double qtaomtot = rec.leggiDouble("qtaomtot");
        double qtaom = qtasm + qtaomimp + qtaomtot;
		tvart.setText(artcod + " " + artdescr);
		tvprezzo.setText(Formattazione.formValuta(prz, 12, 3, Formattazione.SEGNO_SX_NEG));
		tvgiac.setText(Formattazione.formValuta(giac, 12, 3, Formattazione.SEGNO_SX_NEG));
		if (qtavend > 0)
			tvqtavend.setText(Formattazione.formValuta(qtavend, 12, 3, Formattazione.NO_SEGNO));
		else
			tvqtavend.setText("");
        if (qtaom > 0)
            tvqtaom.setText(Formattazione.formValuta(qtaom, 12, 3, Formattazione.NO_SEGNO));
        else
            tvqtaom.setText("");
		int not = rec.leggiIntero("notifica");
		if (not == 1)
			img.setImageResource(R.drawable.ic_announcement_black_48dp);
		else if (not == 2)
			img.setImageResource(R.drawable.ic_grade_black_48dp);
		else if (not == 3)
			img.setImageResource(R.drawable.ic_trending_up_black_48dp);
		else if (not == 4)
			img.setImageResource(R.drawable.ic_trending_down_black_48dp);
		else
			img.setImageResource(android.R.color.transparent);
		return rowView;
	}	
}
